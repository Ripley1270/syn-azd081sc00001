﻿-- This SQL updates or creates a WebDiary install.
-- Version 0.01
-- 18 NOV 2010

DECLARE @description0 varchar(255);
DECLARE @description1 varchar(255);
DECLARE @description2 varchar(255);
DECLARE @description3 varchar(255);
SET @description0 = N'Production';
SET @description1 = N'UST';
SET @description2 = N'Staging';
SET @description3 = N'Developer';
IF EXISTS (select * from dbo.ENVIRONMENTS)
	BEGIN
		UPDATE dbo.ENVIRONMENTS
		SET description = @description0
		WHERE environment_id = 0
		UPDATE dbo.ENVIRONMENTS
		SET description = @description1
		WHERE environment_id = 1
		UPDATE dbo.ENVIRONMENTS
		SET description = @description2
		WHERE environment_id = 2
		UPDATE dbo.ENVIRONMENTS
		SET description = @description3
		WHERE environment_id = 3
	END
ELSE
	BEGIN
		INSERT INTO dbo.ENVIRONMENTS (description)
		VALUES (@description0)
		INSERT INTO dbo.ENVIRONMENTS (description)
		VALUES (@description1)
		INSERT INTO dbo.ENVIRONMENTS (description)
		VALUES (@description2)
		INSERT INTO dbo.ENVIRONMENTS (description)
		VALUES (@description3)
	END

DECLARE @majorVersion int;
DECLARE @minorVersion int;
DECLARE @microVersion int;
DECLARE @revision int;
DECLARE @buildNumber int;
DECLARE @surveyToolDB varchar(255);
DECLARE @environment int;
DECLARE @portalUrl varchar(255);
DECLARE @serviceUrl varchar(255);

-- These are populated by installer --
SET @portalUrl = '[portal_url]';
SET @serviceUrl = '[service_url]';

-- MODIFY THESE VALUES --
SET @majorVersion = $majorVersion;
SET @minorVersion = $minorVersion;
SET @microVersion = $microVersion;

-- Increment revision when new tables or columns are added to the schema. --
SET @revision = 60; 
SET @buildNumber = $buildNumber;
SET @surveyToolDB = N'Data Source=np-joes.phtstudy.com;Initial Catalog=Checkbox;User ID=admin;Password=admin';
SET @environment = $environment; -- 0 - production; 1 - UST; 2 - Staging; 3 - Developer --

-- EXECUTE SQL --
IF EXISTS (select * from dbo.VERSIONS)
	BEGIN
		UPDATE dbo.VERSIONS
		SET Major_version = @majorVersion, Minor_version = @minorVersion, Micro_version = @microVersion, Revision = @revision, Build_number = @buildNumber,  Modification_date = GETUTCDATE(), Survey_tool_database = @surveyToolDB, environment = @environment, portal_url = @portalUrl, service_url = @serviceUrl
		WHERE Version_id = 1
	END
ELSE
	BEGIN
		INSERT INTO dbo.VERSIONS (Major_version, Minor_version, Micro_version, Revision, Build_number, Creation_date, Survey_tool_database, environment, portal_url, service_url)
		VALUES (@majorVersion, 
				@minorVersion,
				@microVersion, 
				@revision,
				@buildNumber,
				GETUTCDATE(),
				@surveyToolDB,
				@environment,
				@portalUrl,
				@serviceUrl)
	END
