
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[customGetSiteCodeFromkrDOM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[customGetSiteCodeFromkrDOM]
GO

CREATE procedure [dbo].[customGetSiteCodeFromkrDOM](
@krDOM varchar(256))
as
set nocount on
SELECT sitecode FROM lookup_domain with (nolock) WHERE krDOM = @krDOM

GO

GRANT EXECUTE ON customGetSiteCodeFromkrDOM TO pht_server_read_only

IF NOT EXISTS (SELECT * FROM [allowed_clin_procs] WHERE [proc_name] = 'customGetSiteCodeFromkrDOM') BEGIN
INSERT [allowed_clin_procs] ([proc_name]) VALUES ('customGetSiteCodeFromkrDOM')
END
