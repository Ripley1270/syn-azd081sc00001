-- ==========================================================================================
-- Creation date: 13 November 2015
-- Description: get diary by either @sigId or all of @krpt, @krsu, @origStartts, @origSignningts
-- Stored procedure: LPA_getDiary    
-- ==========================================================================================

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'LPA_getDiary' AND TYPE = 'P')
   DROP PROCEDURE [dbo].[LPA_getDiary]
GO

CREATE PROCEDURE [dbo].[LPA_getDiary]
	@sigId VARCHAR(36) = NULL,
	@krpt VARCHAR(36) = NULL,
	@krsu VARCHAR(36) = NULL, 
	@origStartts DATETIME = NULL,
	@origSigningts DATETIME = NULL
AS

SET NOCOUNT ON;
	
DECLARE @OpID NUMERIC(18,0);
IF((ISNULL(@sigId,'') != '') OR (@krpt IS NOT NULL AND  @krsu IS NOT NULL AND @origstartts IS NOT NULL AND @origsigningts IS NOT NULL))
BEGIN 
	SELECT @OpID = MIN(OL.OpID)
	FROM OpsLog ol WITH (NOLOCK) 
	JOIN lookup_su su WITH (NOLOCK)
	ON su.sigorig = ol.sigorig 
	WHERE ol.sigid = @sigId
	 OR (ol.krpt = @krpt
			AND su.krsu = @krsu
			AND ol.startts = @origStartts
			AND ol.signingts = @origSigningts);
	
	SELECT xml 
	FROM opslog_xml WITH (NOLOCK) 
	WHERE opid = @OpID;
END
ELSE
BEGIN
	DECLARE @message VARCHAR(36);
	--used if any one of @krpt, @krsu, @origStartts, @origSigningts is null 
	SET @message = 'required parameters missing';
	SELECT @message;
END
 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'LPA_getDiary' AND TYPE = 'P')
BEGIN
	DECLARE @name as varchar(50)
	DECLARE @version as numeric(18,2)

	SELECT @name = 'LPA_getDiary', @version = 1.00
	EXEC [dbo].[PDE_UpdateSQLVersion] @name, @version
   
END

GRANT EXECUTE ON [dbo].[LPA_getDiary] TO pht_server_read_only 
GO

IF NOT EXISTS (SELECT * FROM allowed_clin_procs WHERE proc_name='LPA_getDiary')
	INSERT INTO allowed_clin_procs (proc_name) values ('LPA_getDiary');
GO

