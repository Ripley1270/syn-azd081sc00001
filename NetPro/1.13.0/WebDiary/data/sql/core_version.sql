﻿-- This SQL updates or creates a WebDiary install.
-- Version 0.01
-- 18 NOV 2010

DECLARE @majorVersion int;
DECLARE @minorVersion int;
DECLARE @revisionVersion int;
DECLARE @buildNumber int;
DECLARE @study_id int;



-- MODIFY THESE VALUES --
SET @majorVersion = majorVersion;
SET @minorVersion = minorVersion;
SET @revisionVersion = revisionVersion;
SET @buildNumber = buildNumber;
SET @study_id = study_id;


-- EXECUTE SQL --
IF EXISTS (select * from dbo.ORIGINAL_CORE_VERSION where study_id = @study_id)
	BEGIN
		UPDATE dbo.ORIGINAL_CORE_VERSION
		SET Major_version = @majorVersion, Minor_version = @minorVersion, Micro_version = @revisionVersion, Build_number = @buildNumber,  Modification_date = GETUTCDATE()
		WHERE study_id = @study_id
	END
ELSE
	BEGIN
		INSERT INTO dbo.ORIGINAL_CORE_VERSION (study_id, Major_version, Minor_version, Micro_version, Build_number, Creation_date)
		VALUES (@study_id,
                @majorVersion,
				@minorVersion,
				@revisionVersion, 
				@buildNumber,
				GETUTCDATE()) 
	END
