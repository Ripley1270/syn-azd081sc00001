-- Creating the domain user --
IF NOT EXISTS (SELECT loginname from master.dbo.syslogins 
    where name = '<domain>\<domain_user>')
    BEGIN
    CREATE LOGIN [<domain>\<domain_user>] FROM WINDOWS
    WITH DEFAULT_DATABASE = WebDiary
    END
GO

-- Swithching to our database --
use [WebDiary]

-- Creating the user in the database
IF NOT EXISTS (SELECT name FROM sys.database_principals WHERE
    name = '<domain_user>_user')
    CREATE USER <domain_user>_user FOR LOGIN [<domain>\<domain_user>]
    WITH DEFAULT_SCHEMA = <domain_user>_user
    GO

-- Creating the database schema --
IF NOT EXISTS(SELECT * FROM sys.schemas WHERE name = '<domain_user>_user')
    Exec('CREATE SCHEMA <domain_user>_user AUTHORIZATION <domain_user>_user')
    GO

GRANT SELECT, INSERT, DELETE, UPDATE, EXECUTE TO [<domain_user>_user]
-- Permissions needed for sql dependency starter
GRANT CREATE SERVICE, CREATE QUEUE, CREATE PROCEDURE, VIEW DEFINITION TO [<domain_user>_user]
-- Permissions needed for sql dependency subscriber
GRANT SUBSCRIBE QUERY NOTIFICATIONS TO [<domain_user>_user]
GRANT RECEIVE ON QueryNotificationErrorsQueue TO [<domain_user>_user]
-- Permissions needed for both sql dependency starter and subscriber
GRANT REFERENCES on CONTRACT::[http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification] TO [<domain_user>_user]
