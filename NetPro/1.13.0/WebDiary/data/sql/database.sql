﻿-- Creates the WebDiary database and WebDiary user.
-- Version 0.04
-- 17 Apr 2011

USE master

-- Cleaning up before we start
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'WebDiary')
ALTER DATABASE [WebDiary]
SET OFFLINE WITH ROLLBACK IMMEDIATE
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'WebDiary')
ALTER DATABASE [WebDiary]
SET ONLINE

-- Creating a database
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'WebDiary')
CREATE DATABASE [WebDiary]
GO

-- Ensuring that Service Broker is enabled 
IF (SELECT IS_BROKER_ENABLED FROM sys.databases WHERE name = N'WebDiary') = 0
	ALTER DATABASE [WebDiary] SET ENABLE_BROKER