param([string]$baseFilePath, [string]$databaseName, [string]$assignmentFileName, [string]$editSubjectFileName, [string]$offStudyFileName)

##Args:
##baseFilePath - file path for the study.xml 
##databaseName - database name - used to update study.name attribute
##assignmentFileName - assignment survey xml file name
##editSubjectFileName - edit subject survey xml file name
##offStudyFileName - offstudy survey xml file name
$xml = [xml](Get-Content $baseFilePath -Encoding UTF8)


## Update name attribute
$xml.study.name = $databaseName

## Get handle to each survey node
$assignmentNode = $xml.study.surveys.survey | Where-Object {$_.form_type -eq "Assignment" }
$editSubjectNode = $xml.study.surveys.survey | Where-Object {$_.form_type -eq "Edit Subject" }
$offStudyNode = $xml.study.surveys.survey | Where-Object {$_.form_type -eq "Off Study" }

## Update fileName in xml to study specific files
$assignmentNode.filename = $assignmentFileName
$editSubjectNode.filename = $editSubjectFileName
$offStudyNode.fileName = $offStudyFileName


$xml.save($baseFilePath)