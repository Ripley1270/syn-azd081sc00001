Version info for AstraZeneca D081SC00001 NetPro

LABEL		DATE		CHANGES
======		=======		=======
8.0.1       15MAY19     [kmccracken]
                        TCO ONEECOA-120274
                        - corrected erroneous study.xml version attribute info at top of file

8.0.0       15MAY19     [kmccracken]
                        TCO ONEECOA-120274
                        - changed text of VistConfirmation top radio button from 'Re-screening' to 'Restart Assessment Window'

7.0.0       15APR19     [kmccracken]
                        - TCO ONEECOA-150009
                          Added ru-US

6.0.0       29DEC18     [tegan]
                        - TCO ONEECOA-103724
                          Adding deDE, nlBE, frBE, itIT, zhCN, esES, zhUS

5.0.0       07DEC18     [xzhao]
                        - TCO-RAF: ONEECOA-126525
                          Adding 1 planned language - Spanish (Chile)

4.2.0       27NOV18     [ykadam]
                        - change in language list to
                         Batch 3: skSK, nlNL, frFR, ptBR

4.1.0       27NOV18     [ykadam]
                        - change in language list to
                         Batch 3: skSK, nlNL, frFR, ptBR, esCL

4.0.0       27NOV18     [ykadam]
                        - ONEECOA-103724
                          Language batch 4: deDE, nlBE, frBE, itIT, zhCN, esES

3.0.0       14Nov18     [xzhao]
                        - Adding languages batch 2: TCO-RAF: ONEECOA-113959, Enhancement: ONEECOA-113962
                            cs_CZ	Czech (Czech Republic)
                            en_GB	English (United Kingdom)
                            es_US	Spanish (United States)
                            ja_JP	Japanese (Japan)
                            tr_TR	Turkish (Turkey)

2.1.0       07Nov18     [ykadam]
                        - Changed the file name.

2.0.0       07Nov18     [ykadam]
                        - Added languages for esUS, enAU, enCA, frCA

1.21.0		10Oct018	[schan]
						- Added Korean as a language in Add Subject

1.20.0		07Oct018	[schan]
						- ONEECOA-111779: SR-Data is not displayed for Site User,Active Ingredients,Single-Dose Strength columns in Other Medication Worksheet report

1.19.0      	25Sep2018   ivetma
		ONEECOA-110658 NetPRO - missing last response value on Page 9/2 in Other Analgesic Resolution

1.18.0      	22Sep2018   ivetma
		ONEECOA-110488 NP - Incorrect text in Other Medication Resolution

1.17.0      	22Sep2018   ivetma
		ONEECOA-109916 HH - variable %x is not correctly displayed in SW- Other Medication Resolution Web Page (10)

1.16.0      	21Sep2018   ivetma
		ONEECOA-109676 NP - incorrect response value on Page 9 in Other Analgesic Resolution
		ONEECOA-109631 NP - incorrect time format for Other Medication Record

1.15.0      	17Sep2018   ivetma
						ONEECOA-107868 NP - incorrect dynamic text in Other medication Resolution
1.14.0      	16Sep2018   ivetma
						                    ONEECOA-107868 NP - incorrect dynamic text in Other medication Resolution

1.13.0          11Sep2018       [jpresto]
                                - ONEECOA-107291: NP - Incorrect End Use of Handheld Reasons

1.12.0		07Sep2018	[schan]
						Fixed:
						- ONEECOA-107001: SWE - Other Analgesic Resolution and Subject Analgesic List are available
						- ONEECOA-105786: HH - Incorrect Subject ID format

1.11.0      06Sep2018   jpresto
							- Finished implementing SWE Visit Confirmation Diary (custom widgets, branching, phase transitions, scheduling, custom variables)
							- ONEECOA-105983: SWE - Shows error when trying to complete Visit Confirmation in SWE

1.9.0		05Sep2018	tegan
							- ONEECOA-105532: Correctly adding PT.DOB to assignment form

1.8.0		21Aug2018	tegan
							- ONEECOA-105526: Change subject assignement number range
							- ONEECOA-105477: Alter study med form names

1.7.0      	16Aug2018   ivetma
							- Med module tweaks


1.3.0      	30July2018   ivetma

							- Adding medication module
1.0.0      	20July2018   tegan
							- Initial build
