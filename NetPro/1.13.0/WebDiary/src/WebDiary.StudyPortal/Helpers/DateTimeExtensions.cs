﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Threading;

namespace WebDiary.StudyPortal.Helpers
{
    /// <summary>
    /// Contains DateTime Extension methods
    /// </summary>
    public static class DateTimeExtensions
    {
        public static string ToPreferredShortMonthName(this DateTime dateTime, string format)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            switch (cultureInfo.Name)
            {
                case "cs-CZ":
                    CultureInfo newCultureInfo = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
                    //Set AbbreviatedMonthNames, AbbreviatedMonthGenitiveNames to preferred short month name  
                    newCultureInfo.DateTimeFormat.AbbreviatedMonthNames = GetPreferredShortMonthName(cultureInfo.Name);
                    newCultureInfo.DateTimeFormat.AbbreviatedMonthGenitiveNames = newCultureInfo.DateTimeFormat.AbbreviatedMonthNames;
                    Thread.CurrentThread.CurrentCulture = newCultureInfo;
                    break;

                default:
                    break;
            }

            return dateTime.ToString(format);
        }

        public static string ToPreferredShortMonthName(this DateTime dateTime, string format, IFormatProvider provider)
        {
            CultureInfo cultureInfo = (CultureInfo)provider;

            switch (cultureInfo.Name)
            {
                case "cs-CZ":
                    CultureInfo newCultureInfo = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
                    //Set AbbreviatedMonthNames, AbbreviatedMonthGenitiveNames to preferred short month name  
                    newCultureInfo.DateTimeFormat.AbbreviatedMonthNames = GetPreferredShortMonthName(cultureInfo.Name);
                    newCultureInfo.DateTimeFormat.AbbreviatedMonthGenitiveNames = newCultureInfo.DateTimeFormat.AbbreviatedMonthNames;

                    Thread.CurrentThread.CurrentCulture = newCultureInfo;
                    break;

                default:
                    break;
            }

            return dateTime.ToString(format, CultureInfo.CurrentCulture);
        }

        public static string[] GetPreferredShortMonthName(string culture)
        {
            string[] preferrefShortMonthName = new string[] { };
            switch (culture)
            {
                //Specify preferred short month name for cs-CZ
                case "cs-CZ":
                    preferrefShortMonthName = new string[] { "led", "ún", "břez", 
                                                      "dub", "květ", "červ", 
                                                      "červen", "srp", "září", 
                                                      "řij", "list", "pros", "" };
                    break;

                default:
                    break;
            }

            if (preferrefShortMonthName.Length == 0)
                throw new Exception("Preferred Short Month Names were not populated");

            return preferrefShortMonthName;
        }
    }
}