﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using System.Globalization;

namespace WebDiary.StudyPortal.Helpers
{
    public class OriginalCoreVersionHelper
    {
        public static string OriginalCoreVersionShort(int studyId)
        {
            using (WebDiaryContext context = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                OriginalCoreVersionRepository coreVersionRep = new OriginalCoreVersionRepository(context);
                OriginalCoreVersion coreVersion = coreVersionRep.FindByStudyId(studyId);

                if (coreVersion != null)
                    return String.Format(CultureInfo.InvariantCulture, "{0}" + "." + "{1}",
                    coreVersion.MajorVersion, coreVersion.MinorVersion);
                else
                    return string.Empty;

            }
        }

        public static string OriginalCoreVersionFullBuild(int studyId)
        {
            using (WebDiaryContext context = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                OriginalCoreVersionRepository coreVersionRep = new OriginalCoreVersionRepository(context);
                OriginalCoreVersion coreVersion = coreVersionRep.FindByStudyId(studyId);

                if (coreVersion != null)
                    return String.Format(CultureInfo.InvariantCulture, "{0}" + "." + "{1}" + "." + "{2}" + ". Bld. " + "{3}",
                        coreVersion.MajorVersion, coreVersion.MinorVersion, coreVersion.MicroVersion, coreVersion.BuildNumber);
                else
                    return string.Empty;

            }
        }
    }
}