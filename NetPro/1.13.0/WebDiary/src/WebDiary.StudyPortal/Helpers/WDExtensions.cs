﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebDiary.StudyPortal.Helpers
{
    public static class WDExtensions
    {
        public static string TrimLastCharacter(this String str)
        {
            if (String.IsNullOrEmpty(str))
            {
                return str;
            }
            else
            {
                return str.TrimEnd(str[str.Length - 1]);
            }
        }
    }
}