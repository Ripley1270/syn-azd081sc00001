﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebDiary.SWAPI.SWData;
using System.Collections;
using WebDiary.SWAPI;
using System.Globalization;
using System.Xml.Linq;
using WebDiary.Core.Helpers;
using WebDiary.StudyPortal.MedicationModule;

namespace WebDiary.StudyPortal
{
    public class PDETools : PDECommonTools
    {

        //Diary KRSU's
        public const String KRSU_ASSIGN = "Assignment";
        public const String KRSU_Deactivate = "EndWebProUse";
        public const String KRSU_VISITCONFIRMATION = "VisitConfirmation";
        public const String KRSU_MED_SELECTION = MedModuleConfig.MED_SELECTION_KRSU;
        public const String KRSU_OTHER_MED_RESOLUTION = MedModuleConfig.OTHER_MED_RESOLUTION_KRSU;


        //Constants
        public const String EMPTY_STRING = " ";
        public const String STD_YES = "1";
        public const String STD_NO = "0";
        public const String STD_NO_VALID_DATA = "-1";

        public const String DEFAULT_PROTOCOL = "1";

        //Phases
        public const String PHASE_PRESCREENING = "10";
        public const String PHASE_SCREENING = "100";
        public const String PHASE_RESCREENING_1 = "200";
        public const String PHASE_RESCREENING_2 = "210";
        public const String PHASE_RESCREENING_3 = "220";
        public const String PHASE_RESCREENING_4 = "230";
        public const String PHASE_RESCREENING_5 = "240";
        public const String PHASE_RESCREENING_6 = "250";
        public const String PHASE_RESCREENING_7 = "260";
        public const String PHASE_RESCREENING_8 = "270";
        public const String PHASE_RESCREENING_9 = "280";
        public const String PHASE_RESCREENING_10 = "290";
        public const String PHASE_TREATMENT = "300";
        public const String PHASE_FOLLOWUP_1 = "400";
        public const String PHASE_FOLLOWUP_2 = "500";
        public const String PHASE_PRETERMINATION = "998";
        public const String PHASE_DEACTIVATION = "999";

        //Languages
        public const String LANG_ENGLISH = "en_US";
        public const String SW_DATE_FORMAT = "dd MMM yyyy";

        //Visits
        public const String VISIT_RESCREENING = "10";
        public const String VISIT_START_TREATMENT = "20";
        public const String VISIT_TREATMENT_DISCONTINUATION = "30";
        public const String VISIT_PROGRESSION_AND_TREATMENT_DISCONTINUATION = "40";
        public const String VISIT_PROGRESSION = "50";
        public const String VISIT_STUDY_DISCONTINUATION = "60";
    }
}