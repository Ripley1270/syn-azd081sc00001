﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;

using WebDiary.Core.Entities;
using WebDiary.Controls;
using WebDiary.Core.Helpers;
using WebDiary.Core.Repositories;
using WebDiary.SWAPI;
using WebDiary.SWAPI.SWData;
using WebDiary.WDAPI;

using log4net;
using System.Globalization;
using WebDiary.Core.Errors;
using WebDiary.Core.Constants;
using System.Web.Services;
using WebDiary.StudyPortal.Helpers;
using WebDiary.StudyPortal.MedicationModule;

namespace WebDiary.StudyPortal
{
    public partial class SiteAccess : BasePage
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(SiteAccess));
        private const string QUESTIONNAIRE_LIST = "questionnaire_list";
        private const string EDIT_SUBJECT = "edit_subject";
        private const string OFF_STUDY = "off_study";
        private const string SSA_BASE_URL = "ssa_base_url";
        private string site_language;
        private const string SUBJECTLIST_PARAM = "requestType=subject";

        public SiteAccess()
        {
            try
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    db.ExecuteCommand("EXECUTE dbo.DeleteExpiredSessions");
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                logger.Error(err, ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool isAuthorizedBrowser = false;
            try
            {
                HttpBrowserCapabilities browser = Request.Browser;
                isAuthorizedBrowser = AuthorizedBrowser.CheckAuthorizedBrowser(StudyData.study.Name, browser.Browser, browser.MajorVersion);
                if (!isAuthorizedBrowser)
                {
                    authBrowser.Value = "false";
                    lblUnauthorizedMessage.Visible = true;
                    string browserNameVersion = browser.Browser;

                    if (string.Equals(browserNameVersion, "Edge", StringComparison.OrdinalIgnoreCase))
                        browserNameVersion = browserNameVersion + "HTML" + " " + browser.MajorVersion;
                    else
                        browserNameVersion = browserNameVersion + " " + browser.MajorVersion;
                    lblUnauthorizedMessage.Text = String.Format(CultureInfo.CurrentCulture, Resources.Resource.UnauthorizedBrowserMsg.ToString(CultureInfo.CurrentCulture), browserNameVersion);

                    List<string> authorizedBrowser = AuthorizedBrowser.GetAuthorizedBrowser2(StudyData.study.Name);
                    string browserList = AuthorizedBrowser.GenerateBrowserList(authorizedBrowser);
                    authBrowserArea.InnerHtml = browserList;

                    logger.Info(String.Format(CultureInfo.InvariantCulture, "Use of {0} is not supported. Browser Name: {1}, Major version:{2}, userAgent: {3}", browserNameVersion, browser.Browser, browser.MajorVersion, Request.UserAgent));
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }

            if (Session["SubmitResult"] != null)
            {
                if (!string.IsNullOrEmpty(Session["SubmitResult"].ToString()))
                {
                    notificationScriptID.Text = "<script type=\"text/javascript\">notification.text = '" + Session["SubmitResult"].ToString() + "';</script>";
                    Session["SubmitResult"] = null;
                }
            }

            site_language = SessionHelper.GetSiteLanguageKey();            
            
            if (!IsPostBack)
            {   
                //In case we back out of survey frame
                Session[WDConstants.FramePostUrl] = null;
                Session[WDConstants.FrameRedirectUrl] = null;
                Session[WDConstants.FrameLeaveUrl] = null;
                                
                //Check is session is still active.
                //Check NetPRO session
                if (Session[WDConstants.VarRequestUrl] != null)
                {
                    //Check StudyWork Session
                    if (!SessionHelper.CheckSwSession(Session[WDConstants.VarRequestUrl] as String))
                    {
                        Response.Redirect(Session[SSA_BASE_URL].ToString() + SWAPIConstants.SessionTimeoutPage, false);
                        return;
                    }
                }
                else
                {
                    // Construct SW Return URL
                    string returnUrl = null;

                    try
                    {
                        using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                        {
                            returnUrl = StudyData.study.SsaBaseUrl + SWAPIConstants.SessionTimeoutPage;
                            Response.Redirect(returnUrl, false);
                        }
                    }
                    catch (Exception ex)
                    {

                        string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                        logger.Error(err, ex);
                        Session[WDError.ErrorMessageKey] = err;
                        Response.Redirect(WDError.Path, true);
                    }                    
                    return;
                }

                try
                {
		            //Load languages
                    //Stream studyStream = null;
                    //try
                    //{
                    //    studyStream = File.Open(Server.MapPath("studyconfig/study.xml"), FileMode.Open);
                    //    XDocument xdoc = XDocument.Load(studyStream);
                    //    XElement sls = xdoc.Element("study").Element("SiteLanguages");
                    //    var slsList = sls.Elements("SiteLanguage");
                    //    if (slsList.Count() > 1)
                    //    {
                    //        foreach (var slsItem in slsList)
                    //        {
                    //            selectLanguageDdl.Items.Add(new ListItem(slsItem.Element("LanguageDisplayName").Value, slsItem.Element("LanguageCode").Value));
                    //        }
                    //    }
                    //    else
                    //        changeLanguageBlock.Visible = false;
                    //}
                    //catch (IOException ex)
                    //{
                    //    string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorProcessingUrRequest, ex.Message);
                    //    logger.Error(err);
                    //    Session[WDError.ErrorMessageKey] = err;
                    //    Response.Redirect(WDError.Path, true);
                    //}
                    //finally
                    //{
                    //    studyStream.Close();
                    //}
                    

                    //Get Subject information and Site Gateway questionnaires.
                    using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                    {
                        //PDE Disable timetravel in production
                        VersionsRepository vr = new VersionsRepository(db);
                        Versions ver = vr.GetSingleOrDefault();
                        if (ver.Environment == 0)
                        {
                            DTPanel.Visible = false;
                        }
                        else
                        {
                            DTPanel.Visible = true;
                        }

                        Study study = StudyData.study;
                        bool lfStudy = !string.IsNullOrEmpty(study.StudyVersions.Lf_Hash);
                        PanelLF.Visible = !string.IsNullOrEmpty(StudyData.version.LFHash);

                        Session[SSA_BASE_URL] = study.SsaBaseUrl;

                        string krPT = Session[WDConstants.VarKrpt].ToString();

                        WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);

                        SubjectData sdata = wd.GetSubjectData(Session[WDConstants.VarKrpt].ToString(), 
                                                                            new ArrayList {SWAPIConstants.ColumnPatientId, 
                                                                            SWAPIConstants.ColumnPatientKrPhase,
                                                                            SWAPIConstants.ColumnPatientPhaseDate});
                        if (sdata != null)
                        {
                            patientIDLbl.Text = sdata.data.Patientid;
                            Session[WDConstants.VarPtPatientId] = sdata.data.Patientid;
                            
                            //Get current phase and phase date.
                            Session[WDConstants.VarSuPhaseAtEntry] = sdata.data.Krphase;

                            //Fix krphasedate if necessary.
                            if (sdata.data.Phasedate.Contains('.'))
                            {
                                string[] str = sdata.data.Phasedate.Split('.');
                                 string strnew = DateTimeHelper.ChangeFormat(str[0], SWAPIConstants.SwWeirdDateTimeFormat, SWAPIConstants.SwDateTimeFormat);

                                 Session[WDConstants.VarSuPhaseStartDate] = strnew;
                            }
                            else
                            {
                                Session[WDConstants.VarSuPhaseStartDate] = sdata.data.Phasedate;
                            }
                        }
                        else
                        {
                            string err = Resources.Resource.ErrorSWDB;
                            logger.Error(err);
                            Session[WDError.ErrorMessageKey] = err;
                            Response.Redirect(WDError.Path, true);
                            return;
                        }

                        SubjectRepository subjectRep = new SubjectRepository(db);
                        Subject subject = subjectRep.FindByKrptAndStudy(krPT, study.Id);
                        ClinData clinData;
                        string sigprev;
                        if (subject.Email.Contains(WDConstants.NoEmailToken) && !lfStudy) // LogPad 5.x combo study
                        {
                            //Get date from the Subject's Assignment report.
                            clinData = wd.GetClinDataByNumberRange(krPT, SWAPIConstants.DefaultAssignmentSu, -1,
                                                       new ArrayList { SWAPIConstants.TableColumnAssignmentSuSigOrig, SWAPIConstants.TableColumnAssignmentSuSigIg });

                            patientInfoBlock.Visible = false;
                        }
                        else
                        {
                            clinData = wd.GetClinDataByNumberRange(krPT, SWAPIConstants.DefaultAssignmentSu, -1,
                            new ArrayList {SWAPIConstants.TableColumnAssignmentSuSigOrig, SWAPIConstants.TableColumnAssignmentSuSigIg,
                                SWAPIConstants.TableColumnSubjectEmailId, SWAPIConstants.TableColumnSubjectLanguage});

                            Session[WDConstants.VarSubjectEmailId] = (string)(clinData.datatable[0])[2];
                            preferredLangValueLbl.Text = (string)(wd.GetLanguageNameFromCode((clinData.datatable[0])[3]));
                            Session[WDConstants.VarLanguage] = preferredLangValueLbl.Text;
                        }
                        Session[WDConstants.VarSuSigOrig] = (string)(clinData.datatable[0])[0];
                        sigprev = (string)(clinData.datatable[0])[1];
                        Session[WDConstants.VarSuSigPrev] = sigprev == null ? (string)Session[WDConstants.VarSuSigOrig] : sigprev;

                        string lfStudyUrl = study.WebDiaryStudyBaseUrl.Trim().ToString(CultureInfo.InvariantCulture);
                        int startPosition = lfStudyUrl.IndexOf("//") + "//".Length;
                        int endPosition = lfStudyUrl.IndexOf(".");
                        string lfMainStudyUrl = lfStudyUrl.Substring(startPosition, endPosition - startPosition);
                        LFStudyName.Text = lfMainStudyUrl.ToString(CultureInfo.InvariantCulture);

                        //Set Subject LogPad LF Info
                        LFStudyUrlValueLabel.Text = (study.WebDiaryStudyBaseUrl.EndsWith("/", StringComparison.OrdinalIgnoreCase) ? study.WebDiaryStudyBaseUrl : study.WebDiaryStudyBaseUrl + "/") + "LF/";
                        if (subject.Pin.HasValue)
                            LFPinValueLabel.Text = subject.Pin.ToString();

                        //QR code
                        if (lfStudy)
                        {
                            string subjectLanguage = WDAPIObject.JavaLocaleToDotNetCulture((string)(clinData.datatable[0])[3]); 
                            string imgFile = QRCodeHelper.GetQRCode(lfMainStudyUrl, subject.Pin.Value, subjectLanguage);
                            System.Web.UI.WebControls.Image qrcodeImg = new System.Web.UI.WebControls.Image();
                            qrcodeImg.ImageUrl = imgFile;
                            qrcode.Controls.Add(qrcodeImg);
                        }

                        DateTime currentDT = DateTime.Now.Date;

                        //Get Subject's time zone id.
                        if (subject != null)
                        {
                            Session[WDConstants.VarSuTimeZoneValue] = subject.TimeZone.ToString(CultureInfo.InvariantCulture);
                            Session[DateTimeHelper.TimeZoneId] = subject.TimeZone;
                            int pos = subject.Email.IndexOf('@');
                            string hiddenEmail = new string('*', pos);
                            hiddenEmail += subject.Email.Substring(pos);
                            emailAddrValueLbl.Text = hiddenEmail;
                            //PDE - set time to timetravel widget
                            txtTimeTravel.Text = DateTimeHelper.GetLocalTime().ToString(SWAPIConstants.DateTimeTravelFormat, CultureInfo.InvariantCulture);
                            currentDT = SurveyScheduler.GetSubjectLocalTime(subject);
                        }

                        //PDE - list of surveys to hide
                        List<String> hiddenSurveyList = new List<String>();
                        //Flush the hidden survey list and re-calculate
                        hiddenSurveyList.Clear();

                        /*
                         *  PDE - Add custom schedule logic to hide diaries
                         *  
                         *  Add KRSU values to the hidden survey list that you do not want to appear on the gateway
                         *  
                         *  ex: hiddenSurveyList.Add(PDETools.KRSU_BRFreq);
                         */

                        //MedModule Initialize Other Record Drop Down
                        //DropDownList otherRecordList = MedModuleUtils.initOtherRecordList(wd, Session, questionnnaireHeaderPnl, questionnnaireFooterPnl);
                        
                        /*if (!otherRecordList.Visible)
                        {
                            hiddenSurveyList.Add(MedModuleConfig.OTHER_MED_RESOLUTION_KRSU);
                        }*/

                        initOtherMedDropDown(wd, subject, hiddenSurveyList);

                        if (!wd.IsSubjectEnabled(krPT) || sdata.data.Krphase.Equals(PDETools.PHASE_PRETERMINATION))
                        {
                            hiddenSurveyList.Add(PDETools.KRSU_MED_SELECTION);
                        }

                        //Scheduling for Visit COnfirmation
                        if (sdata.data.Krphase.Equals(PDETools.PHASE_DEACTIVATION) ||
                            sdata.data.Krphase.Equals(PDETools.PHASE_PRETERMINATION) ||
                            sdata.data.Krphase.Equals(PDETools.PHASE_PRESCREENING))
                        {
                            hiddenSurveyList.Add(PDETools.KRSU_VISITCONFIRMATION);
                        }
                    
                            //Get questionnaires.
                            var siteQuestionnaireList = from s in db.StudySurveys
                                                        where s.StudyId == study.Id &&
                                                        s.FormType == SWAPIConstants.FormTypeSiteSubmit &&
                                                        hiddenSurveyList.Contains(s.KRSU) == false
                                                        orderby s.SortOrder
                                                        select new
                                                        {
                                                            DisplayName = s.DisplayName, //PDE - if study includes non-english site languages, this must be updated to read from a resource file (resx) Ex: (String)GetGlobalResourceObject("PDEResource", s.KRSU)
                                                            ActivationURL = s.ActivationURL,
                                                            TriggeredPhase = s.TriggeredPhase
                                                        };

                            //Bind questionnaires to data grid.
                            if(siteQuestionnaireList.Count() == 0)
                                NoQuestionnairesLabel.Visible = true;
                            questionnaireGridView.DataSource = siteQuestionnaireList;
                            questionnaireGridView.DataBind();
                            if (!isAuthorizedBrowser)
                                questionnaireGridView.Enabled = false;

                            ActivationEmailTable.Visible = !subject.IsApproved;

                        if (!wd.IsSubjectEnabled(krPT))
                        {
                            endStudyPnl.Visible = false;
                            ActivationEmailTable.Visible = false;
                            ParticipationEndedLabel.Visible = true;

                            if (siteQuestionnaireList.Count() == 0)
                            {
                            NoQuestionnairesLabel.Visible = true;
                        }
                        }

                        //if Lf_Hash != null, activation is on Logpad App, which doesn't use email
                        if (lfStudy)
                        {
                            ActivationEmailTable.Visible = false;
                            emailAddrRow.Visible = false;
                        }
                        
                        var languages = wd.GetLanguages();
                        if (languages != null)
                        {
                            int counter = 0;
                            int selectedIndex = 0;
                            string defaultLanguage = "en-US";
                            if (!string.IsNullOrEmpty(Session[site_language] as string))
                            {
                                defaultLanguage = Session[site_language].ToString();
                            }
                            else if (Request.Cookies[site_language] != null && !string.IsNullOrEmpty(Request.Cookies[site_language].Value))
                            {
                                defaultLanguage = Request.Cookies[site_language].Value;
                            }                            

                            foreach (var item in languages.list)
                            {
                                SiteLanguagesList.Items.Add(new ListItem(item.decode, item.code));
                                if (WDAPIObject.JavaLocaleToDotNetCulture(item.code).Contains(defaultLanguage))
                                    selectedIndex = counter;
                                counter++;
                            }
                            
                            SiteLanguagesList.SelectedIndex = selectedIndex;
                        }

                        //Only if Developer mode show screen shot checkbox
                        if (StudyData.version.ProductEnvironment == WebDiary.Core.Helpers.Version.EnvironmentDeveloper)
                        {
                            pnlScreenShotContainer.Visible = true;
                            string screenShot = Session[WDConstants.VarScreenShot] as string ?? string.Empty;
                            if (!string.IsNullOrEmpty(screenShot) && (screenShot == "true"))
                                chkScreenShot.Checked = true;
                        }
                        else
                            pnlScreenShotContainer.Visible = false;

                        //Try to send setup code to StudyWorks again
                        if (lfStudy && subject.Pin.HasValue)
                        {
                            try
                            {
                                SimpleResponse setupCodeResponse = wd.GetData("LPA_getSetupCode", new System.Collections.ArrayList() { krPT });

                                try
                                {
                                    if (string.IsNullOrEmpty(setupCodeResponse.response))
                                    {
                                        SimpleResponse swapiResponse = wd.GetData("LPA_submitSetupCode", new System.Collections.ArrayList() { krPT, subject.Pin.Value });
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logger.Info(String.Format(CultureInfo.InvariantCulture, "Error occured while sending setup code to StudyWorks. But the error will be supressed to allow normal process of NetPRO. {0}", ex.Message), ex);
                                }
                            }
                            catch (Exception ex)
                            {
                                logger.Info(String.Format(CultureInfo.InvariantCulture, "Error occured while checking setup code in StudyWorks. But the error will be supressed to allow normal process of NetPRO. {0}", ex.Message), ex);
                            }

                        }                            
                    }
                }
                catch (NetProException npex)
                {
                    string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorQRCode, npex.Message);
                    logger.Error(err, npex);
                    Session[WDError.ErrorMessageKey] = err;
                    Response.Redirect(WDError.Path, true);
                    return;
                }
                catch (Exception ex)
                {
                    string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                    logger.Error(err, ex);
                    Session[WDError.ErrorMessageKey] = err;
                    Response.Redirect(WDError.Path, true);
                    return;
                }
            }
        }

        protected void QuestionnaireGridView_RowCommand(Object sender, CommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "launch")
                {
                    WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);
                    SurveyRepository surveyRep = new SurveyRepository(db);
                    string url = e.CommandArgument.ToString().Trim();
                    StudySurveys survey = surveyRep.FindByActivationUrl(url);
                    SurveyData surveyData = SurveyDataHelper.CreateSurveyData(survey.Id, survey.TriggeredPhase);
                    surveyData.Sigid = "NP." + WDAPIObject.GenerateSignatureId();
                    NameValueCollection queryStrings = SurveyDataHelper.SaveSurveyData(surveyData, db);
                    
                    if (!survey.IsExternal)
                    {
                        

                        String subjectID = System.Web.HttpContext.Current.Session[WDConstants.VarPtPatientId].ToString();
                        queryStrings.Add("subject_id", subjectID);
                        String siteID = System.Web.HttpContext.Current.Session[WDConstants.VarSiteCode].ToString();
                        queryStrings.Add("site_id", siteID);

                        if (survey.FormType.Equals(SWAPIConstants.FormTypeSiteSubmit))
                        {
                            String siteUser = System.Web.HttpContext.Current.Session[WDConstants.VarUserId].ToString();
                            queryStrings.Add("siteUser", siteUser);
                        }

                        /* PDE - SWAPI call example
                         * //Get SWAPI hook
                         * StudyRepository sr = new StudyRepository(db);
                         * Study study = sr.FindByName(Session[WDConstants.VarProtocol].ToString());
                         * SubjectRepository subjRep = new SubjectRepository(db);
                         * Subject subject = subjRep.FindByKrptAndStudy(surveyData.Krpt, study.Id);
                         * WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
                         * 
                         * 
                         * String krpt = subject.KrPT.ToString();
                         * //Setup arguments for stored procedure
                         * ArrayList PDEArgs = PDETools.setupSwapiArgs(krpt, Session[WDConstants.VarSuTimeZoneValue].ToString());
                         * //Run stored procedure and get results
                         * String launchSurveyData = wd.GetData("PDE_NetProBPInfoSQLV1", PDEArgs).response;
                         */

                        //Get SWAPI hook
                         StudyRepository sr = new StudyRepository(db);
                         Study study = sr.FindByName(Session[WDConstants.VarProtocol].ToString());
                         SubjectRepository subjRep = new SubjectRepository(db);
                         Subject subject = subjRep.FindByKrptAndStudy(surveyData.Krpt, study.Id);
                         WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);

                         String krpt = subject.KrPT.ToString();
                         //Setup arguments for stored procedurev
                         ArrayList PDEArgs = PDETools.setupSwapiArgs(krpt, Session[WDConstants.VarSuTimeZoneValue].ToString());
                         DateTime currentDT = SurveyScheduler.GetSubjectLocalTime(subject);
                         queryStrings.Add("currentDT", currentDT.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture));

                        // PDETools.getStringValue

                        /*
                        *  PDE - pass in hidden items to a survey here
                        */
                        Boolean launchSurvey = true;

                        //[schan]
                        if (survey.KRSU.Equals(PDETools.KRSU_MED_SELECTION))
                        {
                            MedicationSelection.onOpen(wd, queryStrings, PDEArgs, this);
                        } 
                        else if (survey.KRSU.Equals(PDETools.KRSU_OTHER_MED_RESOLUTION))
                        {
                            if (OtherMedDropDown.SelectedIndex == 0)
                            {
                                otherMedSelectionError.Visible = true;
                                launchSurvey = false;
                            }
                            else
                            {
                                String MED_ID1N = OtherMedDropDown.SelectedValue;

                                queryStrings = OtherMedResolution.onOpen(wd, queryStrings, PDEArgs, this, MED_ID1N);
                            }
                        }
                        else if (survey.KRSU.Equals(PDETools.KRSU_VISITCONFIRMATION))
                        {
                            String phaseAtEntry = System.Web.HttpContext.Current.Session[WDConstants.VarSuPhaseAtEntry].ToString();
                            queryStrings.Add("phase", phaseAtEntry);

                            String siteUser = System.Web.HttpContext.Current.Session[WDConstants.VarUserId].ToString();
                            queryStrings.Add("siteName", siteUser);

                            //Execute Stored Proc to return historical data needed
                            String launchSurveyData = wd.GetData("PDE_NetPro_LaunchSurveyDataSQLV1", PDEArgs).response;

                            //Parse stored proc results
                            DateTime? randomizationDT = PDETools.getDateTimeValue("RNDMDT1D", launchSurveyData);
                            DateTime? lastBPISF = PDETools.getDateTimeValue("lastBPIReportDT", launchSurveyData);
                            DateTime? PRCDAT1D = PDETools.getDateTimeValue("PRCDAT1D", launchSurveyData);
                            String minDate;

                            //max date for randomization date picker
                            queryStrings.Add("maxDate", currentDT.ToString());

                            //min date for randomization date picker
                            if (lastBPISF.HasValue && PRCDAT1D.HasValue)
                            {
                                //Check which is more recent, BPISF or Practice Diary reportDate
                                if (lastBPISF.Value.CompareTo(PRCDAT1D.Value) > 0)
                                {
                                    minDate = lastBPISF.Value.ToString();
                                }
                                else
                                {
                                    minDate = PRCDAT1D.Value.ToString();
                                }
                            }
                            //Only BPI-SF done, use that as the min
                            else if (lastBPISF.HasValue)
                            {
                                minDate = lastBPISF.Value.ToString();
                            }
                            //Only Practice Diary done, use that as the min
                            else if (PRCDAT1D.HasValue)
                            {
                                minDate = PRCDAT1D.Value.ToString();
                            }
                            //fallback - shouldn't get here, but set min to current day so code doesn't crash if we are missing a BPISF and Practice Diary date for some reason
                            else
                            {
                                minDate = currentDT.ToString();
                            }

                            queryStrings.Add("minDate", minDate);
                            

                            //If randomization date exists, pass in study day to checkbox so it can be read on submission
                            if (randomizationDT.HasValue)
                            {
                                int studyDay = (currentDT.Date - randomizationDT.Value).Days + 1;
                                queryStrings.Add("studyDay", studyDay.ToString());
                            }

                        }

                        //TODO Add to PDE Template
                        String[] jsCSSFiles = null;
                        String inlineJS = null;

                        if (jsCSSFiles != null)
                        {
                            PDECommonTools.setOnLoadJS_CSSFile(Session, jsCSSFiles);
                        }

                        if (inlineJS != null)
                        {
                            PDECommonTools.setOnLoadInlineJavascript(Session, inlineJS);
                        }

                        if (launchSurvey)
                        {
                            url += "&" + queryStrings.ToString();
                            HttpContext.Current.Session[WDConstants.FrameRedirectUrl] = url;
                            Response.Redirect("survey.aspx?psd_id=" + surveyData.Id, true);
                        }
                    }
                    else
                        ExternalLauncher.LaunchExternalSurvey(Request, Response, survey, surveyData);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                logger.Error(err, ex);
                Session[WDError.ErrorMessageKey] = err;
                Response.Redirect(WDError.Path, true);
            }
        }

        protected void EditSubjectBtn_Click(Object sender, EventArgs e)
        {
            try
            {
                WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);
                
                SurveyRepository surveyRep = new SurveyRepository(db);
                var editSurvey = surveyRep.FindByStudyIdAndFormType(StudyData.study.Id, SWAPIConstants.FormTypeEditSubject);
                StudySurveys editSubject = editSurvey.FirstOrDefault();
                if (editSubject == null)
                {
                    logger.Fatal("Edit survey is not found in the database. NetPRO is not configured correctly.");
                    Session[WDError.ErrorMessageKey] = String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>");
                    Response.Redirect(WDError.Path, false);
                    return;
                }
                SurveyData surveyData = SurveyDataHelper.CreateSurveyData(editSubject.Id, editSubject.TriggeredPhase);
                surveyData.Sigorig = Session[WDConstants.VarSuSigOrig].ToString();
                surveyData.SigPrev = Session[WDConstants.VarSuSigPrev].ToString();
                NameValueCollection queryStrings = SurveyDataHelper.SaveSurveyData(surveyData, db);
                
                if (emailChk.Checked)
                {
                    queryStrings.Add(WDConstants.VarSubjectEmailId, (string)Session[WDConstants.VarSubjectEmailId]);
                }

                if (languageChk.Checked)
                {
                    queryStrings.Add(WDConstants.VarLanguage, (string)Session[WDConstants.VarLanguage]);
                }

                string url = editSubject.ActivationURL;
                url += "&" + queryStrings.ToString();
                HttpContext.Current.Session[WDConstants.FrameRedirectUrl] = url;
                Response.Redirect("survey.aspx?psd_id=" + surveyData.Id, true);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                logger.Error(err, ex);
                Session[WDError.ErrorMessageKey] = err;
                Response.Redirect(WDError.Path, true);
            }
        }

        protected void EndStudyParticipationBtn_Click(Object sender, EventArgs e)
        {
            try
            {
                WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);
                
                SurveyRepository surveyRep = new SurveyRepository(db);
                var offStudySurvey = surveyRep.FindByStudyIdAndFormType(StudyData.study.Id, SWAPIConstants.FormTypeOffStudy);
                StudySurveys offStudy = offStudySurvey.FirstOrDefault();
                if (offStudy == null)
                {
                    logger.Fatal("End NetPRO use survey is not found in the database. NetPRO is not configured correctly.");
                    Session[WDError.ErrorMessageKey] = String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>");
                    Response.Redirect(WDError.Path, false);
                    return;
                }
                
                SurveyData surveyData = SurveyDataHelper.CreateSurveyData(offStudy.Id, offStudy.TriggeredPhase);
                NameValueCollection queryStrings = SurveyDataHelper.SaveSurveyData(surveyData, db);

                string url = offStudy.ActivationURL;
                url += "&" + queryStrings.ToString();

                //Add last_report_date for End Study Participation Survey
                string krPT = Session[WDConstants.VarKrpt] as string ?? string.Empty;
                string daysFromToday = string.Empty;
                if (!string.IsNullOrEmpty(krPT))
                {
                  daysFromToday = GetLastReportDate(krPT, StudyData.study.Id);
                    if (!string.IsNullOrEmpty(daysFromToday))
                        url += "&last_report_date=" + daysFromToday;
                }
                HttpContext.Current.Session[WDConstants.FrameRedirectUrl] = url;
                Response.Redirect("survey.aspx?psd_id=" + surveyData.Id, true);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string err = String.Format(CultureInfo.InvariantCulture, Resources.Resource.ErrorDB, ex.Message);
                logger.Error(err, ex);
                Session[WDError.ErrorMessageKey] = err;
                Response.Redirect(WDError.Path, true);
            }
        }

        protected void DataSummariesLnk_Click(object sender, EventArgs e)
        {
            //Check is session is still active.
            if (!SessionHelper.CheckSwSession(Session[WDConstants.VarRequestUrl] as String))
            {
                Response.Redirect(Session[SSA_BASE_URL].ToString() + "/ssa", false);
                return;
            }
            string test1 = Session[WDConstants.VarSwReturnUrl] as string;
            logger.Debug(test1);
            Response.Redirect(test1, true);
        }

        protected void ChooseSubjectLnk_Click(object sender, EventArgs e)
        {
            //Check if session is still active.
            if (!SessionHelper.CheckSwSession(Session[WDConstants.VarRequestUrl] as String))
            {
                Response.Redirect(Session[SSA_BASE_URL].ToString() + "/ssa", false);
                return;
            }

            //Redirect to choose another subject
            Response.Redirect(Session[WDConstants.VarSwReturnUrl].ToString() + "&" + SUBJECTLIST_PARAM, true);
        }

        [WebMethod]
        public static bool SendActivationEmail()
        {
            try
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    SurveyEmailScheduleRepository scheduleRep = new SurveyEmailScheduleRepository(db);
                    SubjectRepository subjectRep = new SubjectRepository(db);
                    Study study = StudyData.study;
                    Subject subject = subjectRep.FindByKrptAndStudy(HttpContext.Current.Session[WDConstants.VarKrpt].ToString(), study.Id);
                    var schedule = scheduleRep.FindBySurveyIdAndSubjectId((int)WebDiary.Core.Constants.WDConstants.EmailType.Activation, subject.Id);
                    WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
                    int surveyId = (int)WebDiary.Core.Constants.WDConstants.EmailType.Activation;
                    if (schedule != null)
                    {
                        logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject GUID: {0}, surveyId: {1}, subject krpt: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, scheduled date time in UTC: {6}, logging time stamp in UTC: {7}. Sending activation email failed, a record for activation email already exists in SURVEY_EMAIL_SCHEDULE table, scheduling service might be down.", subject.Id.ToString(), surveyId, subject.KrPT, StudyData.study.Name, StudyData.study.Id, "activation", schedule.ScheduleDT, DateTime.UtcNow));
                    }
                    else if (!wd.IsSubjectEnabled(subject.KrPT))
                        logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject GUID: {0}, subject krpt: {1}, study name: {2}, study ID in NPDB: {3}, email info: {4}, logging time stamp in UTC: {5}. Sending activation email failed, subject is not enabled.", subject.Id.ToString(), subject.KrPT, StudyData.study.Name, StudyData.study.Id, "activation", DateTime.UtcNow));
                    else
                    {
                        schedule = new SurveyEmailSchedule
                        {
                            SurveyId = surveyId,
                            SubjectId = subject.Id,
                            ScheduleDT = DateTime.UtcNow,
                        };
                        db.SurveyEmailSchedule.InsertOnSubmit(schedule);
                        //New token and expiration
                        subject.ActivationToken = Guid.NewGuid();
                        subject.ActivationTokenExpirationDate = DateTime.UtcNow.AddDays(study.TokenExpiration);
                        db.SubmitChanges();
                        logger.Info(String.Format(CultureInfo.InvariantCulture, "SUCCESS: subject GUID: {0}, surveyId: {1}, subject krpt: {2}, StudyWorks timeZone: {3}, study name: {4}, study ID in NPDB: {5}, email info: {6}, scheduled date time in UTC: {7}, logging time stamp in UTC: {8}. Entered a record for activation email to SURVEY_EMAIL_SCHEDULE table successfully.", subject.Id.ToString(), surveyId, subject.KrPT, subject.TimeZone, StudyData.study.Name, StudyData.study.Id, "activation", schedule.ScheduleDT, DateTime.UtcNow));
                        return true;
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string krpt = (HttpContext.Current.Session[WDConstants.VarKrpt].ToString());
                int surveyId = (int)WebDiary.Core.Constants.WDConstants.EmailType.Activation;
                logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject krpt: {0}, surveyId: {1}, study name: {2}, study ID in NPDB: {3}, email info: {4}, logging time stamp in UTC: {5}, error message: {6}. Sending activation email failed, failed in entering a record for activation email to SURVEY_EMAIL_SCHEDULE table.", krpt, surveyId, StudyData.study.Name, StudyData.study.Id, "activation", DateTime.UtcNow, ex.Message), ex);
            }
            catch (Exception ex)
            {
                string krpt = (HttpContext.Current.Session[WDConstants.VarKrpt].ToString());
                int surveyId = (int)WebDiary.Core.Constants.WDConstants.EmailType.Activation;
                logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject krpt: {0}, surveyId: {1}, study name: {2}, study ID in NPDB: {3}, email info: {4}, logging time stamp in UTC: {5}, error message: {6}. Sending activation email failed, failed in entering a record for activation email to SURVEY_EMAIL_SCHEDULE table.", krpt, surveyId, StudyData.study.Name, StudyData.study.Id, "activation", DateTime.UtcNow, ex.Message), ex);
            }
            return false;
        }

        protected void SiteLanguages_IndexChanged(object sender, EventArgs e)
        {
            Session[site_language] = Response.Cookies[site_language].Value = WDAPIObject.JavaLocaleToDotNetCulture(SiteLanguagesList.SelectedItem.Value);
            Response.Redirect(Request.Url.ToString(), true);
        }

        protected void DTApply_Click(object sender, EventArgs e)
        {
            using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
            {
                DateTime DTtravel = DateTime.Parse(txtTimeTravel.Text, CultureInfo.InvariantCulture);
                SubjectRepository subRep = new SubjectRepository(db);
                Subject subject = subRep.FindByKrptAndStudy(HttpContext.Current.Session[WDConstants.VarKrpt].ToString(), StudyData.study.Id);
                subject.TimeTravelOffset = (int)(TimeSpan.FromTicks((DateTimeHelper.GetUtcTime(DTtravel, subject.StudyId, subject.TimeZone)).Ticks - DateTime.UtcNow.Ticks)).TotalMinutes;
                db.SubmitChanges();
                SurveyScheduler.ScheduleEmailAllSurveys(subject, StudyData.study, SurveyScheduler.ScheduleState.TimeTravel);
                Session[DateTimeHelper.TimeTravelOffset] = subject.TimeTravelOffset;
            }
            Response.Redirect(Request.Url.ToString(), true);
        }

        protected string GetLastReportDate(string krPT, int studyId)
        {
            string krSU = string.Empty;
            string lastReportStartDate = string.Empty;
            ClinData clinData;
            HashSet<DateTime> lastReportStartDateSet = new HashSet<DateTime>();

           try
           {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    Study study = StudyData.study;
                    WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);

                    var surveyList = db.StudySurveys.Where(s => s.StudyId == studyId && (s.FormType == SWAPIConstants.FormTypeSubjectSubmit || s.FormType == SWAPIConstants.FormTypeSubjectAssignment || s.FormType == SWAPIConstants.FormTypeSiteSubmit));

                    foreach (var survey in surveyList)
                    {
                        krSU = survey.KRSU;
                        clinData = wd.GetClinDataByNumberRange(krPT, krSU, -1, new ArrayList { SWAPIConstants.SuReportStartDate });
                        if (clinData.datatable.Count() > 0)
                        {
                            lastReportStartDate = (string)(clinData.datatable[0])[0];
                            lastReportStartDateSet.Add(Convert.ToDateTime(lastReportStartDate, CultureInfo.InvariantCulture));
                        }
                    }

                    //If no report start date is retrieved, get enrolldate of subject  
                    if (lastReportStartDateSet.Count() == 0)
                    {
                        SubjectData sdata = wd.GetSubjectData(krPT, new ArrayList {SWAPIConstants.ColumnPatientEnrollDate});
                        if (sdata != null)
                            lastReportStartDateSet.Add(Convert.ToDateTime(sdata.data.enrolldate, CultureInfo.InvariantCulture)); 
                    }                    
                }

                TimeSpan daysFromToday = lastReportStartDateSet.Max().Date.Subtract(DateTime.Now.Date);
                return daysFromToday.TotalDays.ToString(CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                string err = "A problem occurred while trying to get last report start date from Study Works: " + ex.Message;
                logger.Error(err, ex);
            }
           return string.Empty;
        }

        protected void chkScreenShot_CheckedChanged(object sender, EventArgs e)
        {
            if (chkScreenShot.Checked)
                Session[WDConstants.VarScreenShot] = "true";
            else
                Session[WDConstants.VarScreenShot] = null;
            Response.Redirect(Request.Url.ToString(), true);
        }


        //[Med Module] Other Med Drop Down change callback
        protected void onOtherMedChanged(object sender, EventArgs e)
        {
            //If no other med is selected, hide error message
            if (OtherMedDropDown.SelectedIndex != 0)
            {
                otherMedSelectionError.Visible = false;
    }
}

        //[Med Module] Initialize Other Med Drop Down
        protected void initOtherMedDropDown(WDAPIObject wd, Subject subject, List<String> hiddenSurveyList)
        {
            ArrayList PDEArgs = new ArrayList();
            String krpt = subject.KrPT.ToString();
            PDEArgs.Add(HttpContext.Current.Server.UrlEncode(krpt));
            //Add device ID as NetPRO
            PDEArgs.Add("NetPRO");
            //Add current local date time
            DateTime now = DateTimeHelper.GetLocalTime();
            PDEArgs.Add(now.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.CurrentCulture));
            //Add local timezone offset from UTC in milliseconds 
            PDEArgs.Add(Session[WDConstants.VarSuTimeZoneValue]);
            //Other Med Code
            PDEArgs.Add(MedModuleConfig.OTHER_MED_CODE);

            String otherRecordResponse = wd.GetData(MedModuleConfig.OTHER_RECORD_LIST_SQL, PDEArgs).response;
            XDocument xmlDoc = XDocument.Parse(otherRecordResponse);
            XElement otherMedsEl = xmlDoc.Element("othermeds");
            IEnumerable<XElement> medEls = otherMedsEl.Elements("med");
            int otherMedCount = medEls.Count<XElement>();
            

            //If there is no "Other Med"
            if (otherMedCount == 0)
            {

                if (!wd.IsSubjectEnabled(krpt))
                {
                    OtherMedDropDown.Visible = false;
                    hiddenSurveyList.Add(MedModuleConfig.OTHER_MED_RESOLUTION_KRSU);
                }

                //Add 1st item to display "<-- Choose Other Medication Record -->"
                OtherMedDropDown.Items.Add(new ListItem(Resources.PDEResource.DropDownNoRecords, ""));
            }
            //Else there are "Other Med(s)"
            else
            {
                //OtherMedDropDown.Visible = true;

                //Add 1st item to display "<-- Choose Other Medication Record -->"
                OtherMedDropDown.Items.Add(new ListItem(Resources.PDEResource.DropDownChooseRecord, ""));

                //Add each other med to drop down list
                foreach (XElement otherMed in medEls)
                {
                    String MEDTKN1S = otherMed.Attribute("MEDTKN1S").Value;
                    DateTime dt = DateTime.Parse(MEDTKN1S);
                    OtherMedDropDown.Items.Add(new ListItem(dt.ToString(Resources.PDEResource.OtherMedGatewayDTFormat, CultureInfo.CurrentCulture), 
                                                            otherMed.Attribute("MED_ID1N").Value));
                }
            }            
        }

    }
}
