﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebDiary.SWAPI.SWData;
using System.Collections;
using WebDiary.SWAPI;
using System.Globalization;
using System.Xml.Linq;
using WebDiary.Core.Helpers;
using System.Web.SessionState;

namespace WebDiary.StudyPortal
{
    public class PDECommonTools
    {

        //Creates a clinfield that can be added to a clinRecord (form)
        //IT - column in the database 
        //val - value to be added
        public static ClinField generateClinField(String IT, String val)
        {
            ClinField newClinField = new ClinField();
            newClinField.krit = IT;
            newClinField.value = val;
            newClinField.edit = false;

            return newClinField;
        }


        //Sets up parameters for making a swapi call to execute a stored procedure
        public static ArrayList setupSwapiArgs(String krpt, String tz)
        {
            ArrayList PDEArgs = new ArrayList();
            PDEArgs.Add(HttpContext.Current.Server.UrlEncode(krpt));
            //Add device ID as NetPRO
            PDEArgs.Add("NetPRO");
            //Add current local date time
            DateTime now = DateTimeHelper.GetLocalTime();
            PDEArgs.Add(now.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture));

            PDEArgs.Add(tz);

            return PDEArgs;
        }


        //If study uses PDE_NetPro_LastDiaryTime.sql, pulls last diary time out of result
        public static DateTime? getLastDiaryTS(String krsu, String lastDiaryDataXML)
        {
            XElement xmlDoc = XElement.Parse(lastDiaryDataXML);
            DateTime? retVal = null;


            IEnumerable<XElement> elements = from el in xmlDoc.Elements("Diary")
                                             where el.Attribute("KRSU").Value.Equals(krsu)
                                             select el;

            if (elements.Count() != 0)
            {
                XAttribute attr = ((XElement)elements.FirstOrDefault()).Attribute("lastTS");
                if (attr != null)
                {
                    retVal = DateTime.Parse(attr.Value, CultureInfo.InvariantCulture);
                }
            }

            return retVal;
        }


        //Used to parse XML attributes for string values
        public static String getStringValue(String key, String sqlDataXML)
        {
            XElement xmlDoc = XElement.Parse(sqlDataXML);
            String retVal = "";

            try
            {
                retVal = (String)xmlDoc.Attribute(key);
            }
            catch (Exception e)
            {
                retVal = "";
            }

            return retVal;
        }


        //Used to parse XML attributes for datetime values
        public static DateTime? getDateTimeValue(String key, String sqlDataXML)
        {
            XElement xmlDoc = XElement.Parse(sqlDataXML);
            DateTime? retVal = null;

            try
            {
                retVal = DateTime.Parse((String)xmlDoc.Attribute(key), CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                retVal = null;
            }

            return retVal;
        }


        //Get report start date for clinRecord
        public static string getReportStartDate(PDEClinRecord clinRecord)
        {
            ClinField clinReportstartDate = clinRecord.clinRecord.sysvars.Find(item => item.sysvar == "SU.ReportStartDate");
            String reportStartDate = clinReportstartDate.value;

            return reportStartDate;
        }


        //TODO Add to PDE Template
        /*
         * Set an Array of JS and CSS filenames to be loaded when the survey starts
         * 
         * session = the active session of the user
         * filepaths = array of JS and CSS files names that are to be loaded. Files may not be reload on browser if a cache is already exist
         * forceLoadFilepaths = array of JS and CSS files names that are to be loaded. Files are always loaded regardless if files are cache in the browser
         * 
         */
        public static void setOnLoadJS_CSSFile(HttpSessionState session, String[] filepaths = null, String[] forceLoadFilepaths = null)
        {
            String JSResult = "";
            String sessionKey = "customJS_CSSFile";

            if (filepaths != null)
            {
                foreach (String filepath in filepaths)
                {
                    JSResult += "loadjscssfile(\"" + filepath + "\"); ";
                }
            }

            if (forceLoadFilepaths != null)
            {
                foreach (String filepath in forceLoadFilepaths)
                {
                    //Append report start time to create a unique file name, that triggers the file to be loaded when
                    //the survey start regardless if the file is already cache in the browser
                    JSResult += "loadjscssfile(\"" + filepath + "?" + session["reportStart"] + "\"); ";
                }
            }

            if (filepaths == null && forceLoadFilepaths == null)
            {
                session.Remove(sessionKey);
            }
            else
            {
                session["customJS_CSSFile"] = JSResult;
            }
        }

        //TODO Add to PDE Template
        /*
         * Get JS to be loaded that would load specify JS and CSS files
         * 
         * session = the active session of the user
         */
        public static String getOnLoadJS_CSSFile(HttpSessionState session)
        {
            return (String)session["customJS_CSSFile"];
        }

        //TODO Add to PDE Template
        /*
         * Set inline javascript code to be loaded when the survey starts
         * 
         * session = the active session of the user
         * js = javascript string to be loaded. If js is null, it clears previously set javascript code
         */
        public static void setOnLoadInlineJavascript(HttpSessionState session, String js)
        {
            String sessionKey = "customInlineJS";

            if (js == null)
            {
                session.Remove(sessionKey);
            }
            else
            {
                session[sessionKey] = js;
            }
            
        }

        //TODO Add to PDE Template
        /*
         * Get inline javascript code to be loaded when the survey starts
         * 
         * session = the active session of the user
         */
        public static String getOnLoadInlineJavascript(HttpSessionState session)
        {
            String js = (String)session["customInlineJS"];
            String result = null;

            if (js != null)
            {
                result = "var jsonScript = document.createElement('script'); jsonScript.setAttribute('type','text/javascript'); jsonScript.text = '" +
                            js + ";';document.getElementsByTagName(\"head\")[0].appendChild(jsonScript);";
            }

            return result;
        }
    }
}