﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.Resources;
using System.Globalization;
using System.Reflection;
using System.Diagnostics;

namespace WebDiary.StudyPortal.PDECommon
{
    public class PDEResourceUtils
    {       
        //TODO
        public static ResourceSet getResourceSet(String resourceClassName, CultureInfo culture = null)
        {
            ResourceManager resourceMgr = new ResourceManager(resourceClassName, Assembly.GetExecutingAssembly());

            if (culture == null)
            {
                culture = CultureInfo.CurrentUICulture;
            }

            ResourceSet resourceSet = resourceMgr.GetResourceSet(culture, false, false); 

            return resourceSet;
        }

        public static void createResourceJS(String resourceClassName, String jsNamespace = null, CultureInfo culture = null)
        {
            
            //TODO
            
        }

        public static void updateJSResource(String resourceClassName, String jsNamespace = null, CultureInfo culture = null)
        {
            DateTime appStartTime = Process.GetCurrentProcess().StartTime;
            //TODO
        }


    }
}