﻿<%@ Page Title="" Language="C#" MasterPageFile="~/includes/masters/Site.Master" AutoEventWireup="true" CodeBehind="settings.aspx.cs" Inherits="WebDiary.StudyPortal.Settings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
<div class="pht_np_sub_pgHeader">	
        <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, SettingPageHeader%>" />
</div>

<div class="form-wrapper">
    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, emailLbl %>" CssClass="fieldname"></asp:Label> <asp:Label ID="lblEmail" runat="server" CssClass="fieldvalue"></asp:Label><br />
    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, PswrdLabel%>" CssClass="fieldname"></asp:Label> <span class="fieldvalue">**********</span>&nbsp;&nbsp;<asp:LinkButton
        ID="lnkResetPwd" runat="server" 
        Text="<%$ Resources:Resource, resetpassLnk%>" onclick="LnkResetPwd_Click"></asp:LinkButton><br />
</div>
<br /><br />
    <asp:PlaceHolder ID="PlaceholderUserNotice" runat="server" Visible="false">
        <div class="phtUserNoticeConfirm" style="width:auto">
            <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, TZChangeConfirmMsg%>"></asp:Literal>
        </div>
    </asp:PlaceHolder>
<asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, currentTZLabel %>" CssClass="fieldname"></asp:Label> <span id="currentTZ"><asp:Literal ID="litCurrentTimezone" runat="server"></asp:Literal></span><br /><br />

<div id="map-wrapper">
    <div id="right-col"></div>
    <div id="left-col"><b><asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, chooseTZ%>" /></b></div>
    <div class="clear"></div>
    <asp:DropDownList  runat="server" ID="selectTimeZoneList" OnSelectedIndexChanged="TimeZoneList_IndexChanged" AutoPostBack="true"/>
</div>
<div id="info" class="pht_np_sub_pgHeader"></div>
</asp:Content>
