﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WebDiary.StudyPortal
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEmailScheduler" in both code and config file together.
    [ServiceContract]
    public interface IEmailScheduler
    {
        [OperationContract]
        void SendEmail(int surveyId, Guid subjectId);
    }
}
