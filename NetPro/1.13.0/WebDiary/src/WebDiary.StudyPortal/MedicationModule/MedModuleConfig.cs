﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebDiary.StudyPortal.MedicationModule
{
    public class MedModuleConfig
    {
        public const int MED_CODE_LENGTH = 8;
        public const int MAX_NUM_SUBJECT_MEDS = 9;
        public const String OTHER_MED_CODE = "99999999";
        public const String NO_RESOLUTION_MED_CODE = "99999998";

        //FORM ID(KRSU)
        public const String MED_SELECTION_KRSU = "SubjectMed";
        public const String OTHER_MED_RESOLUTION_KRSU = "OtherMedResolve";

        //Sync SQL
        public const String SUBJECT_MED_SYNC_SQL = "PDE_NetPro_GetSubjectMedList";
        public const String MASTER_MED_SYNC_SQL = "PDE_NetPro_GetMasterMedList";
        public const String WORK_LIST_SYNC_SQL = "PDE_NetPro_GetWorkList";
        public const String OTHER_RECORD_LIST_SQL = "PDE_NetPro_GetOtherRecordList";
        public const String INSERT_WORK_MED_SQL = "PDE_NetPro_InsertNewWorkMed";
        public const String INSERT_WORK_MED_INGREDIENTS_SQL = "PDE_NetPro_InsertNewWorkMedIngredients";
        public const String OTHER_MED_RESOLVE_SQL = "PDE_NetPro_OtherMedResolve";

        /*
         *  Files
         */
        //Default directory for Med Module related JS and CSS files
        public const String JS_DIR = "/medModule/";

        //Master Medication List containing all master medications. It is dynamically generated and saved to localhost
        public const String MASTER_MED_FILE_NAME = JS_DIR + "masterMedicationList.js";
        public const String MASTER_MED_CONFIG_FILE_NAME = JS_DIR + "masterMedication.config";

        //Master Work List List containing all work medications. It is dynamically generated and saved to localhost
        public const String WORK_LIST_FILE_NAME = JS_DIR + "masterWorkList.js";

        //Require javascript files for med module related questionnaire
        public const String MED_MODULE_CONFIG_FILE_NAME = JS_DIR + "MedModuleConfig.js";
        public const String MEDICATION_MODULE_FILE_NAME = JS_DIR + "MedicationModule.js";
        public const String MEDICATION_SELECTION_FILE_NAME = JS_DIR + "MedicationSelection.js";
        public const String MED_MODULE_UTIL_FILE_NAME = JS_DIR + "MedModuleUtils.js";
        public const String OTHER_MED_RESOLUTION_FILE_NAME = JS_DIR + "OtherMedResolution.js";
        public const String WORK_INGREDIENTS_TABLE_FILE_NAME = JS_DIR + "workIngredientsTable.js";
        public const String BOOTSTRAP_TABLE_JS_FILE_NAME = JS_DIR + "bootstrap-table.min.js";
        public const String BOOTSTRAP_TABLE_CSS_FILE_NAME = "bootstrap-table.min.css";
        public const String OTHER_MED_RESOLVE_CSS_FILE_NAME = "OtherMedResolve.css";

        //This is for the attribute for the med code that is being return by the sync sql
        public const String MASTER_MED_ATTR_MEDCODE = "MEDCOD1C";

        public const String WORK_MED_ATTR_WORKCODE = "WRKCOD1C";

        //Javascript names
        public const String JS_SUBJECT_MEDS = "SUBJECT_MEDS";
        public const String JS_MASTER_MEDS = "MASTER_MEDS";
        public const String JS_WORK_MEDS = "WORK_MEDS";

        public const String EMPTY_STRING = " ";
        public const String IG_SubjectMed = "SubjectMed";
        public const String IG_OtherMedsResolve = "OtherMedResolve";
        public const String IG_OtherMedWorksheet = "OtherMedWorksheet";

        //Checkbox Alias
        public const String ALIAS_SUBJECT_MED_LIST = "SubjectMed.0.SubjectMedListResponse";

        public const String MedSource_NoResolution = "NN";
        public const String MedSource_SubjList = "SC";
        public const String MedSource_MasterList = "MC";
        public const String MedSource_WorkList = "WC";
        public const String MedSource_NewWorkMed = "WE";
    }
}