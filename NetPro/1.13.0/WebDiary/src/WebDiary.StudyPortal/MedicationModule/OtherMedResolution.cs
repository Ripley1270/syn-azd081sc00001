﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using WebDiary.WDAPI;
using System.Collections;
using WebDiary.Controls;
using WebDiary.Core.Helpers;
using Newtonsoft.Json.Linq;
using WebDiary.SWAPI;
using System.Globalization;
using WebDiary.Core.Entities;
using WebDiary.StudyPortal.PDECommon;
using System.Xml.Linq;
using System.Runtime.CompilerServices;
using log4net;
using WebDiary.Core.Constants;

namespace WebDiary.StudyPortal.MedicationModule
{
    public class OtherMedResolution
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(OtherMedResolution));

        public static NameValueCollection onOpen(WDAPIObject webDiary, NameValueCollection queryStrings, ArrayList PDEArgs, BasePage page, String MED_ID1N)
        {
            //Get subject Meds
            String subjectMeds = MedModuleUtils.getSubjectMeds(webDiary, queryStrings, PDEArgs);

            String result = "var " + MedModuleConfig.JS_SUBJECT_MEDS + " = " + subjectMeds;

            //Add subject meds into javascript to be loaded
            PDECommonTools.setOnLoadInlineJavascript(page.Session, result);

            //Update Master Med List
            MedModuleUtils.updateMasterMedList(webDiary, page.Server, page, (ArrayList)PDEArgs.Clone());

            //Update Work List
            MedModuleUtils.updateWorkList(webDiary, page.Server, page);

            XElement otherMedResolveResult = getOtherMedResolveResult(webDiary, (ArrayList)PDEArgs.Clone(), MED_ID1N);
            String MEDTKN1S = otherMedResolveResult.Attribute("MEDTKN1S").Value;

            //Add JS files to be loaded
            String[] loadFileList = { MedModuleConfig.MED_MODULE_CONFIG_FILE_NAME,
                                        //MedModuleConfig.MEDICATION_MODULE_FILE_NAME, 
                                        MedModuleConfig.MEDICATION_SELECTION_FILE_NAME, 
                                        MedModuleConfig.MED_MODULE_UTIL_FILE_NAME, 
                                        MedModuleConfig.OTHER_MED_RESOLUTION_FILE_NAME,
                                        MedModuleConfig.WORK_INGREDIENTS_TABLE_FILE_NAME,
                                        MedModuleConfig.BOOTSTRAP_TABLE_JS_FILE_NAME,                                        
                                        MedModuleConfig.BOOTSTRAP_TABLE_CSS_FILE_NAME,
                                        MedModuleConfig.OTHER_MED_RESOLVE_CSS_FILE_NAME
                                     };

            String[] forceLoadFileList = { MedModuleConfig.MASTER_MED_FILE_NAME, 
                                             MedModuleConfig.WORK_LIST_FILE_NAME };
            PDECommonTools.setOnLoadJS_CSSFile(page.Session, loadFileList, forceLoadFileList);

            queryStrings.Add("medCodeLength", MedModuleConfig.MED_CODE_LENGTH.ToString());
            queryStrings.Add("maxNumMeds", MedModuleConfig.MAX_NUM_SUBJECT_MEDS.ToString());

            queryStrings.Add("MED_ID1N", MED_ID1N);

            //record date display in report
            DateTime dt = DateTime.Parse(MEDTKN1S);
            String otherMedDateTime = dt.ToString(Resources.PDEResource.OtherMedSurveyDTFormat, CultureInfo.CurrentCulture);


            //asking Sam why this is needed
            //queryStrings.Add("otherMedDateTime", HttpUtility.UrlPathEncode(otherMedDateTime));
            queryStrings.Add("otherMedDateTime", otherMedDateTime);

            return queryStrings;
        }

        public static XElement getOtherMedResolveResult(WDAPIObject webDiary, ArrayList PDEArgs, String MED_ID1N)
        {
            PDEArgs.Add(MED_ID1N);
            String syncResponse = webDiary.GetData(MedModuleConfig.OTHER_MED_RESOLVE_SQL, PDEArgs).response;
            XDocument xmlDoc = XDocument.Parse(syncResponse);
            XElement syncEl = xmlDoc.Element("sync");

            return syncEl;
        }

        public static void onSaved(PDEClinRecord pdeClinRecord, SurveyResponseData surveyResponseData, WDAPIObject webDiary, Subject subject)
        {
            //Update Subject med List
            if (surveyResponseData.Answers.ContainsKey(MedModuleConfig.ALIAS_SUBJECT_MED_LIST))
            {
                String reportDate = PDETools.getReportStartDate(pdeClinRecord);
                String subjectMedListResponse = surveyResponseData.Answers[MedModuleConfig.ALIAS_SUBJECT_MED_LIST];

                JArray subjectMedJArray = JArray.Parse(subjectMedListResponse);

                foreach (JObject subjectMed in subjectMedJArray.Children<JObject>())
                {
                    subjectMed.Add("SBJEFF1S", reportDate);
                }

                PDEJsonUtils.saveJArrayToClinRecord(pdeClinRecord, subjectMedJArray, MedModuleConfig.IG_SubjectMed);

                String[] aliasTokens = MedModuleConfig.ALIAS_SUBJECT_MED_LIST.Split('.');
                pdeClinRecord.removeIT(aliasTokens[0], aliasTokens[2], aliasTokens[1]);
            }
            
            pdeClinRecord.addHiddenItem("MED_ID1N", MedModuleConfig.IG_OtherMedsResolve, surveyResponseData, "0", "MED_ID1N");

            pdeClinRecord.addHiddenItem("MEDTKN1S", MedModuleConfig.IG_OtherMedsResolve, surveyResponseData, "0", "MEDTKN1S");

            //Determine source of OTHCOD1C
            String OTHRES1L = MedModuleConfig.MedSource_NewWorkMed;

            if (surveyResponseData.Answers.ContainsKey(MedModuleConfig.IG_OtherMedsResolve + ".0.OTHNOT1B") &&
                surveyResponseData.Answers[MedModuleConfig.IG_OtherMedsResolve + ".0.OTHNOT1B"].Equals(PDETools.STD_YES))
            {
                OTHRES1L = MedModuleConfig.MedSource_NoResolution;
                pdeClinRecord.addIT("OTHCOD1C", MedModuleConfig.IG_OtherMedsResolve, MedModuleConfig.NO_RESOLUTION_MED_CODE);
            }
            else if (surveyResponseData.Answers.ContainsKey(MedModuleConfig.IG_OtherMedsResolve + ".0.OnSubjectList") &&
                surveyResponseData.Answers[MedModuleConfig.IG_OtherMedsResolve + ".0.OnSubjectList"].Equals(PDETools.STD_YES))
            {                
                OTHRES1L = MedModuleConfig.MedSource_SubjList;
            }
            else if (surveyResponseData.Answers.ContainsKey(MedModuleConfig.IG_OtherMedsResolve + ".0.OnMasterList") &&
                surveyResponseData.Answers[MedModuleConfig.IG_OtherMedsResolve + ".0.OnMasterList"].Equals(PDETools.STD_YES))
            {                
                OTHRES1L = MedModuleConfig.MedSource_MasterList;
            }
            else if (surveyResponseData.Answers.ContainsKey(MedModuleConfig.IG_OtherMedsResolve + ".0.OnWorkList") &&
                surveyResponseData.Answers[MedModuleConfig.IG_OtherMedsResolve + ".0.OnWorkList"].Equals(PDETools.STD_YES))
            {
                OTHRES1L = MedModuleConfig.MedSource_WorkList;
            }

            pdeClinRecord.addIT("OTHRES1L", MedModuleConfig.IG_OtherMedsResolve, OTHRES1L);

            //Remove unnecessary variables
            pdeClinRecord.removeIT(MedModuleConfig.IG_OtherMedsResolve, "OnSubjectList");
            pdeClinRecord.removeIT(MedModuleConfig.IG_OtherMedsResolve, "OnMasterList");
            pdeClinRecord.removeIT(MedModuleConfig.IG_OtherMedsResolve, "OnWorkList");
            pdeClinRecord.removeIT(MedModuleConfig.IG_OtherMedWorksheet, "WorkIngredientResponse");            

            //If other med resolution is new work med
            if (OTHRES1L.Equals(MedModuleConfig.MedSource_NewWorkMed))
            {
                //Add new work med
                addWorkMed(pdeClinRecord, surveyResponseData, webDiary, subject);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static void addWorkMed(PDEClinRecord pdeClinRecord, SurveyResponseData surveyResponseData, WDAPIObject webDiary, Subject subject)
        {
            //WRKNAM2C
            String WRKNAM2C = MedModuleUtils.getRemoveResponse(pdeClinRecord, surveyResponseData, MedModuleConfig.IG_OtherMedWorksheet + ".0.WRKNAM2C");
            String WRKNAM2CPart1 = "";
            String WRKNAM2CPart2 = "";
            
            if (WRKNAM2C != null && WRKNAM2C.Length > 0)
            {
                //if WRKNAM2C is longer than 36 characters, split WRKNAM2C into two values
                if (WRKNAM2C.Length > 36)
                {
                    WRKNAM2CPart1 = WRKNAM2C.Substring(0, 36);
                    WRKNAM2CPart2 = WRKNAM2C.Substring(36);
                }
                else
                {
                    WRKNAM2CPart1 = WRKNAM2C;
                }
            }

            //WRKENT1S
            String WRKENT1S = PDETools.getReportStartDate(pdeClinRecord);

            //MEDUNT1L
            String MEDUNT1L = MedModuleUtils.getRemoveResponse(pdeClinRecord, surveyResponseData, MedModuleConfig.IG_OtherMedWorksheet + ".0.MEDUNT1L");

            //MEDRTE1L
            String MEDRTE1L = MedModuleUtils.getRemoveResponse(pdeClinRecord, surveyResponseData, MedModuleConfig.IG_OtherMedWorksheet + ".0.MEDRTE1L");

            //MEDFRM1L
            String MEDFRM1L = MedModuleUtils.getRemoveResponse(pdeClinRecord, surveyResponseData, MedModuleConfig.IG_OtherMedWorksheet + ".0.MEDFRM1L");

            //MEDFRM2L
            String MEDFRM2L = surveyResponseData.Answers[MedModuleConfig.IG_OtherMedsResolve + ".0.MEDFRM2L"];

            //Build SWAPI argument list
            ArrayList PDEArgs = new ArrayList();
            PDEArgs.Add(WRKNAM2CPart1);
            PDEArgs.Add(WRKNAM2CPart2);
            PDEArgs.Add(WRKENT1S);
            PDEArgs.Add(MEDUNT1L);
            PDEArgs.Add(MEDRTE1L);
            PDEArgs.Add(MEDFRM1L);
            PDEArgs.Add(MEDFRM2L);

            PDEArgs.Add(surveyResponseData.HiddenItems["siteUser"]);

            DateTime now = DateTimeHelper.GetLocalTime();
            PDEArgs.Add(now.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture));
            PDEArgs.Add(subject.TimeZone);

            String workMedResult = webDiary.GetData(MedModuleConfig.INSERT_WORK_MED_SQL, PDEArgs).response;
            try
            {
                XDocument xmlDoc = XDocument.Parse(workMedResult);
                XElement sync = xmlDoc.Element("sync");
                String WRKCOD1C = (String) sync.Attribute("WRKCOD1C");
                String WorkRowID = (String)sync.Attribute("WorkRowID");

                //If WRKCOD1C is missing
                if (WRKCOD1C == null)
                {
                    throw new System.NullReferenceException("WRKCOD1C is null. Unable to save work ingredients");
                }
                //Else save active ingredients
                else
                {
                    String workIngredientResponse = MedModuleUtils.getRemoveResponse(pdeClinRecord, surveyResponseData, MedModuleConfig.IG_OtherMedWorksheet + ".0.WorkIngredientResponse");

                    if (workIngredientResponse == null)
                    {
                        throw new System.NullReferenceException("workIngredientResponse is null. Unable to save work ingredients");
                    }
                    else
                    {
                        JArray workIngredientJArray = JArray.Parse(workIngredientResponse);
                        int ORDWRK1N = 0;
                        
                        //Save each work ingredient variables to PDEClinRecord
                        foreach (JObject workIngredient in workIngredientJArray.Children<JObject>())
                        {
                            JProperty INGWRK1CProp = workIngredient.Property("INGWRK1C");
                            JProperty DOSWRK1NProp = workIngredient.Property("DOSWRK1N");

                            if (INGWRK1CProp != null && DOSWRK1NProp != null)
                            {
                                //Build SWAPI argument list
                                PDEArgs = new ArrayList();
                                String INGWRK1C = (String) INGWRK1CProp.Value;
                                String INGWRK1CPart1 = "";
                                String INGWRK1CPart2 = "";

                                if (INGWRK1C != null && INGWRK1C.Length > 0)
                                {
                                    //if WRKNAM2C is longer than 36 characters, split WRKNAM2C into two values
                                    if (INGWRK1C.Length > 36)
                                    {
                                        INGWRK1CPart1 = INGWRK1C.Substring(0, 36);
                                        INGWRK1CPart2 = INGWRK1C.Substring(36);
                                    }
                                    else
                                    {
                                        INGWRK1CPart1 = INGWRK1C;
                                    }
                                }

                                //Send Ingredients to SWAPI
                                PDEArgs.Add(WRKCOD1C);
                                PDEArgs.Add(INGWRK1CPart1);
                                PDEArgs.Add(INGWRK1CPart2);
                                PDEArgs.Add(DOSWRK1NProp.Value.ToString());
                                PDEArgs.Add(ORDWRK1N);
                                PDEArgs.Add(now.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture));
                                PDEArgs.Add(subject.TimeZone);
                                String result = webDiary.GetData(MedModuleConfig.INSERT_WORK_MED_INGREDIENTS_SQL, PDEArgs).response;
                            }

                            ORDWRK1N++;
                        }

                        pdeClinRecord.addIT("OTHCOD1C", MedModuleConfig.IG_OtherMedsResolve, WRKCOD1C);
                    }

                }
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }

        
    }
}