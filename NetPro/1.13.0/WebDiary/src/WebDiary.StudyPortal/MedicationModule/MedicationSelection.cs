﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebDiary.WDAPI;
using System.Collections.Specialized;
using System.Collections;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.SessionState;
using WebDiary.Controls;
using WebDiary.Core.Helpers;
using WebDiary.Core.Constants;
using WebDiary.StudyPortal.PDECommon;

namespace WebDiary.StudyPortal.MedicationModule
{
    public class MedicationSelection
    {
        public static NameValueCollection onOpen(WDAPIObject webDiary, NameValueCollection queryStrings, ArrayList PDEArgs, BasePage page)
        {   
            //Get subject Meds
            String subjectMeds = MedModuleUtils.getSubjectMeds(webDiary, queryStrings, PDEArgs);

            String result = "var " + MedModuleConfig.JS_SUBJECT_MEDS + " = " + subjectMeds;

            //Add subject meds into javascript to be loaded
            PDECommonTools.setOnLoadInlineJavascript(page.Session, result);

            //Update Master Med List and save it to local server
            MedModuleUtils.updateMasterMedList(webDiary, page.Server, page, PDEArgs);

            //Add JS files to be loaded
            String[] loadFileList = { MedModuleConfig.MED_MODULE_CONFIG_FILE_NAME,
                                        //MedModuleConfig.MEDICATION_MODULE_FILE_NAME, 
                                        MedModuleConfig.MEDICATION_SELECTION_FILE_NAME, 
                                        MedModuleConfig.MED_MODULE_UTIL_FILE_NAME};

            String[] forceLoadFileList = { MedModuleConfig.MASTER_MED_FILE_NAME};

            PDECommonTools.setOnLoadJS_CSSFile(page.Session, loadFileList, forceLoadFileList);

            queryStrings.Add("medCodeLength", MedModuleConfig.MED_CODE_LENGTH.ToString());
            queryStrings.Add("maxNumMeds", MedModuleConfig.MAX_NUM_SUBJECT_MEDS.ToString());

            return queryStrings;
        }

        public static void onSaved(PDEClinRecord pdeClinRecord, SurveyResponseData surveyResponseData)
        {
            //Update Subject med List
            if (surveyResponseData.Answers.ContainsKey(MedModuleConfig.ALIAS_SUBJECT_MED_LIST))
            {
                String reportDate = PDETools.getReportStartDate(pdeClinRecord);
                String subjectMedListResponse = surveyResponseData.Answers[MedModuleConfig.ALIAS_SUBJECT_MED_LIST];

                JArray subjectMedJArray = JArray.Parse(subjectMedListResponse);

                foreach(JObject subjectMed in subjectMedJArray.Children<JObject>()){
                    subjectMed.Add("SBJEFF1S", reportDate);
                }

                PDEJsonUtils.saveJArrayToClinRecord(pdeClinRecord, subjectMedJArray, MedModuleConfig.IG_SubjectMed);

                String[] aliasTokens = MedModuleConfig.ALIAS_SUBJECT_MED_LIST.Split('.');
                pdeClinRecord.removeIT(aliasTokens[0], aliasTokens[2], aliasTokens[1]);
            }
        }
    }
}