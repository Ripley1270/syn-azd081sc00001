﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;
using WebDiary.WDAPI;
using WebDiary.StudyPortal;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using Newtonsoft.Json;
using WebDiary.Core.Entities;
using System.Web.SessionState;
using WebDiary.Core.Helpers;
using WebDiary.SWAPI;
using System.Globalization;
using WebDiary.Core.Constants;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using System.Xml;
using WebDiary.Controls;

namespace WebDiary.StudyPortal.MedicationModule
{
    public class MedModuleUtils
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(MedModuleUtils));

        public static JObject parseXmlELtoJSONObject(XElement el)
        {
            JObject jObj = new JObject();

            foreach (XAttribute attr in el.Attributes())
            {
                jObj.Add(attr.Name.ToString(), new JValue(attr.Value));
            }

            return jObj;
        }

        //Update master med list to the latest before the survey is launched
        //Return true is master med list is Updated
        public static Boolean updateMasterMedList(WDAPIObject webDiary, HttpServerUtility Server, BasePage page, ArrayList PDEArgs)
        {
            //Check to see if master medication list and work medication list need to be generated
            DateTime masterMedModTime;
            Boolean updateMasterMedFile = false;
            String lastSyncVersion = null;
            String serverPath = Server.MapPath("~/includes/scripts");
            String listPath = serverPath + "\\" + MedModuleConfig.MASTER_MED_FILE_NAME;
            String configPath = serverPath + "\\" + MedModuleConfig.MASTER_MED_CONFIG_FILE_NAME;

            if (File.Exists(listPath) && File.Exists(configPath))
            {
                masterMedModTime = File.GetLastWriteTimeUtc(listPath);

                using (StreamReader configFile = new StreamReader(configPath))
                {
                    String config = configFile.ReadToEnd();

                    XDocument configDoc = XDocument.Parse(config);
                    XElement configurationEl = configDoc.Element("configurations");
                    lastSyncVersion = configurationEl.Element("syncVersion").Value;
                }
            }
            else
            {
                masterMedModTime = new DateTime(1900, 01, 01, 01, 01, 0, 0); //file doesn't exist, send date in past so medlist is returned
                updateMasterMedFile = true;
            }

            //Get Master Meds List from StudyWorks
            PDEArgs.Add(masterMedModTime.ToString());

            if (lastSyncVersion != null)
            {
                //Last sync version is used to check if there is a change to the SQL which trigger 
                //the master med list to be updated
                PDEArgs.Add(lastSyncVersion);
            }

            String masterListResult = webDiary.GetData(MedModuleConfig.MASTER_MED_SYNC_SQL, PDEArgs).response;

            XDocument mastermedsDoc = XDocument.Parse(masterListResult);
            XElement masterMedsEl = mastermedsDoc.Element("mastermeds");
            XAttribute syncVersionAttr = masterMedsEl.Attribute("syncVersion");
            String syncVersion = null;

            if (syncVersionAttr != null)
            {
                syncVersion = syncVersionAttr.Value;
            }

            //If results return "updateList" as 1(Yes)
            if (masterMedsEl.Attribute("updateList").Value.Equals(PDETools.STD_YES))
            {
                updateMasterMedFile = true;
            }

            if (updateMasterMedFile)
            {
                //Build JSON object
                JObject masterMedObj = new JObject();

                //Build JSON object from individual med element
                foreach (XElement med in masterMedsEl.Elements("med"))
                {
                    String medCode = med.Attribute(MedModuleConfig.MASTER_MED_ATTR_MEDCODE).Value;
                    //Create JSON object for each med
                    JObject medObj = parseXmlELtoJSONObject(med);
                    JArray ingredients = new JArray();
                    //Build ingredient list for each med
                    foreach (XElement ingredient in med.Elements("ing"))
                    {
                        JObject ingObj = parseXmlELtoJSONObject(ingredient);
                        ingredients.Add(ingObj);
                    }

                    medObj.Add("ings", ingredients);

                    masterMedObj.Add(medCode, medObj);
                }

                String result = masterMedObj.ToString();

                //Write result to master med list file
                using (StreamWriter file = new StreamWriter(listPath, false))
                {
                    file.Write("var " + MedModuleConfig.JS_MASTER_MEDS + " = ");
                    file.Write(result);
                    file.WriteLine(";");
                    file.Close();
                }

                //Update config file
                using (XmlWriter writer = XmlWriter.Create(configPath))
                {
                    writer.WriteStartElement("configurations");
                    writer.WriteElementString("syncVersion", syncVersion);
                    writer.WriteEndElement();
                    writer.Flush();
                    writer.Close();
                }
            }

            //Save report start time to be used for loading the master med list
            page.Session["reportStart"] = DateTime.UtcNow.ToString();

            return updateMasterMedFile;
        }

        public static String getSubjectMeds(WDAPIObject webDiary, NameValueCollection queryStrings, ArrayList PDEArgs)
        {
            String syncResponse = webDiary.GetData(MedModuleConfig.SUBJECT_MED_SYNC_SQL, PDEArgs).response;
            XDocument xmlDoc = XDocument.Parse(syncResponse);
            XElement subjectMeds = xmlDoc.Element("subjectmeds");
            int numSubjectMeds = 0;

            JArray jarray = new JArray();

            //Convert xml output to json
            foreach (XElement subjectMed in subjectMeds.Elements("subjectmed"))
            {
                JObject jObj = MedModuleUtils.parseXmlELtoJSONObject(subjectMed);
                jarray.Add(jObj);

                numSubjectMeds++;
            }

            //Add numSubjectMeds to hidden item
            queryStrings.Add("numSubjectMeds", numSubjectMeds.ToString());

            String json = jarray.ToString();
            String result = JsonConvert.SerializeObject(jarray, new JsonSerializerSettings() { Formatting = Newtonsoft.Json.Formatting.None });

            return result;
        }

        public static Boolean updateWorkList(WDAPIObject webDiary, HttpServerUtility Server, BasePage page)
        {
            //Check to see if master medication list and work medication list need to be generated
            DateTime workModTime;
            Boolean updateWorkFile = false;
            String serverPath = Server.MapPath("~/includes/scripts");

            if (File.Exists(serverPath + "\\" + MedModuleConfig.WORK_LIST_FILE_NAME))
            {
                workModTime = File.GetLastWriteTime(serverPath + "\\" + MedModuleConfig.WORK_LIST_FILE_NAME);
            }
            else
            {
                workModTime = new DateTime(1900, 01, 01, 01, 01, 0, 0); //file doesn't exist, send date in past so medlist is returned
                updateWorkFile = true;
            }

            //Get Master Meds List from StudyWorks
            ArrayList PDEArgs = new ArrayList();
            PDEArgs.Add(workModTime.ToString());
            String workListResult = webDiary.GetData(MedModuleConfig.WORK_LIST_SYNC_SQL, PDEArgs).response;

            XDocument xmlDoc = XDocument.Parse(workListResult);
            XElement worksEl = xmlDoc.Element("workmeds");

            //If results return "updateList" as 1(Yes)
            if (worksEl.Attribute("updateList").Value.Equals(PDETools.STD_YES))
            {
                updateWorkFile = true;
            }

            if (updateWorkFile)
            {
                //Build JSON object
                JObject workMedsObj = new JObject();

                //Build JSON object from individual med element
                foreach (XElement med in worksEl.Elements("med"))
                {
                    String medCode = med.Attribute(MedModuleConfig.WORK_MED_ATTR_WORKCODE).Value;
                    //Create JSON object for each med
                    JObject medObj = parseXmlELtoJSONObject(med);
                    JArray ingredients = new JArray();
                    //Build ingredient list for each med
                    foreach (XElement ingredient in med.Elements("ing"))
                    {
                        JObject ingObj = parseXmlELtoJSONObject(ingredient);
                        ingredients.Add(ingObj);
                    }
                    //Add ingredients to work med
                    medObj.Add("ings", ingredients);

                    //Add work med to work meds object
                    workMedsObj.Add(medCode, medObj);
                }

                String result = workMedsObj.ToString();

                //Write result to file
                using (StreamWriter file = new StreamWriter(serverPath + "\\" + MedModuleConfig.WORK_LIST_FILE_NAME, false))
                {
                    file.Write("var " + MedModuleConfig.JS_WORK_MEDS + " = ");
                    file.Write(result);
                    file.WriteLine(";");
                }

            }

            //Save report start time to be used for loading the master med list
            page.Session["reportStart"] = DateTime.UtcNow.ToString();

            return updateWorkFile;
        }

        //Get value from survey response and remove it from PDEClinRecord
        public static String getRemoveResponse(PDEClinRecord pdeClinRecord, SurveyResponseData surveyResponseData, String alias)
        {
            String value = null;

            if (surveyResponseData.Answers.ContainsKey(alias))
            {
                value = surveyResponseData.Answers[alias];

                String[] aliasTokens = alias.Split('.');
                pdeClinRecord.removeIT(aliasTokens[0], aliasTokens[2], aliasTokens[1]);
            }

            return value;
        }

        /*public static DropDownList initOtherRecordList(WDAPIObject wd, HttpSessionState session, Panel quesHeaderPnl, Panel quesFooterPnl)
        {
            ArrayList PDEArgs = new ArrayList();
            String krpt = session[WDConstants.VarKrpt].ToString();
            PDEArgs.Add(HttpContext.Current.Server.UrlEncode(krpt));
            //Add device ID as NetPRO
            PDEArgs.Add("NetPRO");
            //Add current local date time
            DateTime now = DateTimeHelper.GetLocalTime();
            PDEArgs.Add(now.ToString(SWAPIConstants.SwDateTimeFormat, CultureInfo.CurrentCulture));
            //Add local timezone offset from UTC in milliseconds 
            PDEArgs.Add(session[WDConstants.VarSuTimeZoneValue]);

            String otherRecordResponse = wd.GetData(MedModuleConfig.OTHER_RECORD_LIST_SQL, PDEArgs).response;
            XDocument xmlDoc = XDocument.Parse(otherRecordResponse);
            XElement otherMedsEl = xmlDoc.Element("othermeds");
            IEnumerable medEls = otherMedsEl.Elements("med");
            int otherMedCount = 0;
            Boolean hideDropDown = false;

            DropDownList otherRecordList = new DropDownList();
            otherRecordList.ID = "OtherRecordList";
            otherRecordList.AutoPostBack = true;
            otherRecordList.SelectedIndexChanged += new EventHandler(otherRecordListIndexChanged);
            
            //Add drop down to header panel
            quesHeaderPnl.Controls.Add(otherRecordList);

            Label errorSelectionLabel = new Label();
            errorSelectionLabel.ID = "selectError";
            errorSelectionLabel.Visible = false;
            errorSelectionLabel.CssClass = "nb-errors";

            //Add error label to footer panel
            quesFooterPnl.Controls.Add(errorSelectionLabel);

            //Set drop down list
            if (!hideDropDown)
            {
                otherRecordList.Visible = true;

                foreach (XElement otherMed in medEls)
                {
                    String MEDTKN1S = otherMed.Attribute("MEDTKN1S").Value;
                    String MED_ID1N = otherMed.Attribute("MED_ID1N").Value;

                    ListItem otherMedItem = new ListItem(MEDTKN1S, MED_ID1N);

                    otherRecordList.Items.Add(otherMedItem);

                    otherMedCount++;
                }

                if (otherMedCount == 0)
                {
                    otherRecordList.Items.Insert(0, new ListItem(Resources.PDEResource.DropDownNoRecords, ""));
                }
                else
                {
                    otherRecordList.Items.Insert(0, new ListItem(Resources.PDEResource.DropDownChooseRecord, ""));//ListItem("<-- Choose Other Medication Record -->", ""));
                }
            }
            else
            {
                otherRecordList.Visible = false;
            }

            return otherRecordList;
        }*/

        /*public static void otherRecordListIndexChanged(object sender, EventArgs e)
        {
            if (sender.GetType() == typeof(DropDownList))
            {
                DropDownList dropDown = (DropDownList) sender;
                Page page = dropDown.Page;
                Label selectError = (Label)page.FindControl("selectionError");

                if (dropDown.SelectedIndex == 0)
                {
                    selectError.Visible = false;
                }
                else
                {
                    selectError.Visible = true;
                }
            }

        }*/

        
    }
}