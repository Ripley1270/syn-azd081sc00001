﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebDiary.Core.Entities;
using WebDiary.SWAPI;
using System.Globalization;
using WebDiary.Core.Helpers;
using WebDiary.Core.Errors;
using WebDiary.Core.Constants;
using System.Collections.Specialized;

namespace WebDiary.StudyPortal
{
    public sealed class SurveyDataHelper
    {
        private SurveyDataHelper() { }

        public static SurveyData CreateSurveyData(int surveyId, string triggeredPhase)
        {
            DateTime nowUtc = DateTimeHelper.GetUtcTime();
            SurveyData surveyData = new SurveyData();
            surveyData.Id = Guid.NewGuid();
            surveyData.ExpiryDate = DateTime.UtcNow.AddDays(1);
            surveyData.Krpt = HttpContext.Current.Session[WDConstants.VarKrpt].ToString();
            surveyData.Krdom = HttpContext.Current.Session[WDConstants.VarKrDom].ToString();
            surveyData.StudyId = StudyData.study.Id;
            surveyData.SurveyId = surveyId;
            surveyData.PrintedName = HttpContext.Current.Session[WDConstants.VarPrintedName] as String;
            surveyData.SuReportDate = nowUtc;
            if (HttpContext.Current.Session[WDConstants.VarSuTimeZoneValue] != null) //not set for assignment
                surveyData.SuTzValue = Int32.Parse(HttpContext.Current.Session[WDConstants.VarSuTimeZoneValue].ToString(), CultureInfo.InvariantCulture);
            surveyData.PatientId = HttpContext.Current.Session[WDConstants.VarPtPatientId] as String; //null for assignment
            surveyData.SurveyStartDate = nowUtc;
            surveyData.ReturnUrl = HttpContext.Current.Session[WDConstants.VarReturnUrl] as String;
            surveyData.Uid = HttpContext.Current.Session[WDConstants.VarUserId] as String;

            if (!String.IsNullOrEmpty(triggeredPhase))
            {
                surveyData.Krphase = triggeredPhase;
                surveyData.SuPhaseAtEntry = triggeredPhase;

                surveyData.SuPhaseStartDate = DateTimeHelper.GetUtcTime();
            }
            else
            {
                surveyData.SuPhaseAtEntry = HttpContext.Current.Session[WDConstants.VarSuPhaseAtEntry].ToString();
                DateTime SUPhaseStartDate = DateTime.MinValue;
                if (!DateTime.TryParseExact(HttpContext.Current.Session[WDConstants.VarSuPhaseStartDate].ToString(), SWAPIConstants.SwDateTimeFormat, CultureInfo.InvariantCulture,
                                        DateTimeStyles.None, out SUPhaseStartDate))
                {
                    throw new WebDiary.Core.Errors.WDAPIException(WDErrors.CannotFormatDateTime,
                        "The datetime format for \"" + WDConstants.VarSuPhaseStartDate + "\" is incorrect. Should be \"" + SWAPIConstants.SwDateTimeFormat + "\"");
                }
                //Need to send the phase start date in utc
                surveyData.SuPhaseStartDate = DateTimeHelper.GetUtcTime(SUPhaseStartDate, surveyData.StudyId, Int32.Parse(HttpContext.Current.Session[DateTimeHelper.TimeZoneId].ToString(), CultureInfo.InvariantCulture));
            }
                
            return surveyData;
        }

        public static NameValueCollection SaveSurveyData(SurveyData surveyData, WebDiaryContext db)
        {
            NameValueCollection queryStrings = System.Web.HttpUtility.ParseQueryString(string.Empty);
            queryStrings.Add(WDConstants.VarPsdId, surveyData.Id.ToString());
            if (surveyData.Uid != null)
                queryStrings.Add(WDConstants.VarUserId, surveyData.Uid);

            if (SessionHelper.IsSiteSession()) // site gateway
            {
                queryStrings.Add(WDConstants.Language, HttpContext.Current.Session[SessionHelper.GetSiteLanguageKey()].ToString());
            }
            else if (HttpContext.Current.Session[WDConstants.Language] != null) // patient gateway
            {                
                queryStrings.Add(WDConstants.Language, HttpContext.Current.Session[WDConstants.Language].ToString());
            }
            db.SurveyData.InsertOnSubmit(surveyData);
            db.SubmitChanges();
            return queryStrings;
        }
    }
}