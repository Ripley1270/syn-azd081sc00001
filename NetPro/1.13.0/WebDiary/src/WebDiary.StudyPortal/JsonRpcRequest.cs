﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net;
using System.Text;
using System.Globalization;
using WebDiary.SWAPI.SWData;
using WebDiary.SWAPI;

namespace WebDiary.StudyPortal
{
    public class JsonRpcRequest
    {
        private int requestId = 1;
        private Uri uri;

        public Uri ServiceUri
        {
            get { return uri; }
            set { uri = value; }
        }

        private string method;

        public string Method
        {
            get { return method; }
            set { method = value; }
        }

        private JArray parameters;

        public JArray Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }
        

        public JsonRpcRequest(Uri serviceUri, string method, JArray parameters)
        {
            this.uri = serviceUri;
            this.method = method;
            this.parameters = parameters;
        }

        public JObject GetResponse()
        {
            HttpWebRequest request = null;
            JObject response = null;
            string data = string.Empty;

            request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "POST";
            request.ContentType = "application/json-rpc";

            // create service data
            var serviceData = new JObject(
                new JProperty("jsonrpc", "2.0"),
                new JProperty("id", requestId++),
                new JProperty("method", this.method),
                new JProperty("params", this.parameters));

            //create data string.
            data = serviceData.ToString();
            request.ContentLength = data.Length;

            using (Stream writeStream = request.GetRequestStream())
            {
                byte[] bytes = Encoding.UTF8.GetBytes(data);
                writeStream.Write(bytes, 0, bytes.Length);
            }

            using (HttpWebResponse httpResponse = (HttpWebResponse)request.GetResponse())
            {
                using (Stream responseStream = httpResponse.GetResponseStream())
                {
                    using (StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8))
                    {
                        string responseStr = readStream.ReadToEnd();
                        response = JObject.Parse(responseStr);
                        if (response.Property("error") != null)
                        {
                            SWAPIError error = new SWAPIError { error_message = (string)response["error"]["message"], extra_info = (string)response["error"]["extra_info"] };
                            if (response["error"]["code"] != null)
                                error.error_code = (int)response["error"]["code"];
                            if (string.Compare(this.method, "registerDevice", true, CultureInfo.InvariantCulture) == 0)
                            {
                                string deviceInfo = string.Empty;
                                deviceInfo = ParseDeviceInfo(this.parameters);
                                throw new SWAPIException(error, error.error_message + "; extra info:" + error.extra_info + "; error occured on calling StudyWorks registerDevice. " + deviceInfo);
                            }
                            else
                                throw new SWAPIException(error, error.error_message + "; extra info:" + error.extra_info + "; request:" + data);
                        }
                    }
                }
            }

            return response;
        }

        /// <summary>
        /// Parse device info in registerDevice call 
        /// so that user name and password can not be logged in errors table
        /// </summary>
        /// <param name="deviceInfo">Device Info</param>
        /// <returns>Parsed Device Info</returns>
        public string ParseDeviceInfo(JArray deviceInfo)
        {
            string deviceType = string.Empty;
            string deviceTypeID = string.Empty;
            string deviceSerialNumber = string.Empty;
            string deviceModel = string.Empty;
            StringBuilder parsedInfo = new StringBuilder();

            try
            {
                JObject deviceObject = new JObject();

                foreach (JToken jt in deviceInfo)
                {
                    if (jt.Type == JTokenType.Object)
                    {
                        deviceObject = JObject.FromObject(jt);

                        if (deviceObject.HasValues)
                        {
                            deviceType = deviceObject["deviceType"].Value<string>();
                            deviceTypeID = deviceObject["deviceTypeID"].Value<string>();
                            deviceSerialNumber = deviceObject["deviceSerialNumber"].Value<string>();
                            deviceModel = deviceObject["model"].Value<string>();
                        }

                        break;
                    }
                }
            }
            catch(Exception ex)
            {
                return string.Empty;
            }

            if (!string.IsNullOrEmpty(deviceType))
                parsedInfo.Append("DeviceType: " + deviceType);

            if (!string.IsNullOrEmpty(deviceTypeID))
                parsedInfo.Append(" DeviceTypeID: " + deviceTypeID);

            if (!string.IsNullOrEmpty(deviceSerialNumber))
                parsedInfo.Append(" DeviceSerialNumber: " + deviceSerialNumber);

            if (!string.IsNullOrEmpty(deviceModel))
                parsedInfo.Append(" DeviceModel: " + deviceModel);

            return parsedInfo.ToString(); 
        }
    }
}