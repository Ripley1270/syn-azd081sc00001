﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="activation-affidavit.aspx.cs" Inherits="WebDiary.StudyPortal.mobile.activation_affidavit" %>
<!DOCTYPE html> 
<html>
<head runat="server">
    <title></title>
</head>
<body>
    <div id="container" runat="server" data-role="page">
        <div data-role="header">
            <h2><%= Resources.Resource.Affidavit%></h2>
        </div>
        <div data-role="content">
            <p><%= Resources.Resource.EmailAffidavit %></p>
            <ul>
                <li><%= Resources.Resource.EmailAffidavitLI1 %></li>
                <li><%= Resources.Resource.EmailAffidavitLI2 %></li>
                <li><%= Resources.Resource.EmailAffidavitLI3 %></li>
                <li><%= Resources.Resource.EmailAffidavitLI4 %></li>
                <li><%= Resources.Resource.EmailAffidavitLI5 %></li>
                <li><%= Resources.Resource.EmailAffidavitLI6 %></li>
            </ul>
        </div>
    </div> 
</body>
</html>
