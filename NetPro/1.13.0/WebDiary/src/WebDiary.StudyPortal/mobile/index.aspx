﻿<%@ Page Title="NetPRO" Language="C#" MasterPageFile="~/mobile/mobile.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebDiary.StudyPortal.Mobile.Index" %>

<asp:Content ID="mainContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="center-wrapper">
    <input type="hidden" id="authBrowser" value="" runat="server" />
        <div id="unauthMsgArea" class="server-error">
            <asp:Label ID="lblUnauthorizedMessage" Visible="false" runat="server">
            </asp:Label>
        </div>
        <div id="authBrowserArea" class="server-error" runat="server">
        </div> 
            <form id="loginForm" runat="server">
                <span id="timeoutMessage"><%= Resources.Resource.SessionExpiredLogin %></span>
                <span><%= Resources.Resource.LoginMessageBody %></span><br /><br />

                <div id="lfError" class="error"></div>

                <div class="form-row">
                    <label for="txtEmail"><%= Resources.Resource.emailLbl %></label>
                    <input type="text" autocomplete="off" maxlength="256" id="txtEmail" name="txtEmail" class="required email" />
                </div>
                <div class="form-row">
                    <label for="txtPassword"><%= Resources.Resource.PswrdLabel %></label>
                    <input type="password" id="txtPassword" name="txtPassword" autocomplete="off" maxlength="64" oncopy="return false;" onpaste="return false;" oncut="return false;" class="required" />
                </div>
                <asp:Literal ID="litSurveyRef" runat="server"></asp:Literal>
                <br />
                <input type="submit" value="<%= Resources.Resource.LoginBtnText %>" />                
            </form><br />

            <div class="button-row">
                <div style="margin-bottom:10px;"><a href="forgot-password.aspx"><%= Resources.Resource.ForgotPassword %></a></div>
                <div><a href="forgot-email-dialog.aspx" data-rel="dialog"><%= Resources.Resource.ForgotEmail %></a></div>
            </div>
        <script type="text/javascript">
            $(document).ready(function() {
                var authBrowser = $('#authBrowser').val();
                if (authBrowser == "false") {
                    $("#loginForm input").prop("disabled", true);
                }
            });
        </script>
    </div>
</asp:Content>