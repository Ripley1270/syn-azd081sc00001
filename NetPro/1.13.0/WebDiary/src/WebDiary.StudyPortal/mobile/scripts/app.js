﻿var isTimeout = false;
var sleepCheckTimer, logoutAttempt = null;
var subject = { email: '', krpt: '' };
var lastCheck = 0;
var timeoutPage = '';

$.ajaxSetup({
    cache: false,
    headers: {
        "Cache-Control": "no-cache"
    },
    error: function (x, status, error) {
        if (x.status === 403) {
            logout(true);
        }
        else if (status.length < 1 || navigator.onLine === false) {
            if(logoutAttempt === null)
                alert(resources.noConnection);
        }
    }
});

function hideKeyboard() {
    document.activeElement.blur();
    $("input").blur();
}

function sleepCheck () {
    var now = new Date().getTime();
    var diff = now - lastCheck;
    if (diff > sTimeout) {
        lastCheck = now;
        logout(true);
    }
}

function startSleepCheck() {
    startAutoLock();
    sleepCheckTimer = setInterval(function () { sleepCheck(); }, 1000);
}

function stopSleepCheck() {
    clearInterval(sleepCheckTimer);
}

function startAutoLock() {
    lastCheck = new Date().getTime();
}

// notification settings
var notification = {
    type: 'alert',
    text: '',
    speed: 1000, // opening & closing animation speed
    timeout: 3000, // delay for closing event. Set false for sticky notifications
    elm: null,
    init: function () {
        if (!this.elm) {
            $('body').append('<div class="notification" id="noteBar"></div>');
            this.elm = $(document.getElementById('noteBar'));

            //position banner
            this.elm.css('height', ($('[data-role=header]').height() - 10) + 'px');
            this.elm.css('line-height', ($('[data-role=header]').height() - 10) + 'px');
            this.elm.css('top', -(this.elm.height() + 35));

            this.elm.addClass('note_text');
            if (this.text.length > 0)
                this.show(this.text);
        }
    },
    show: function (text) {
        this.text = text;
        this.elm.text(this.text);

        if (this.text.length > 0) {
            this.elm.animate({ top: 0 }, this.speed, 'swing', function () {
                if (notification.timeout) {
                    setTimeout(function () {
                        notification.elm.animate({ top: -(notification.elm.height() + 35) }, notification.speed, 'swing');
                    }, notification.timeout);
                }
            });
        }
    }
};

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x === c_name) {
            return unescape(y);
        }
    }
}

function adjustStyle(width) {
    width = parseInt(width);
    if (width < 701) {
        $("#dynamicStyle").attr("href", "css/narrow.css");
    } else if ((width >= 701) && (width < 900)) {
        $("#dynamicStyle").attr("href", "css/medium.css");
    } else {
        $("#dynamicStyle").attr("href", "css/wide.css");
    }
}

setCookie('test', 'none', 1);

// if getCookie succeeds, cookies are enabled, since the cookie was successfully created.
if (getCookie('test')) {
    // Delete Cookie.
    setCookie('test', null, -1);
}
// if the Get_Cookie test fails, cookies are not enabled.
else {
    if (location.href.indexOf('wd-error.aspx') < 0)
        location.href = 'wd-error.aspx?nocookie=1';
}

$(document).bind('pageinit', function (event) {
    if (getCookie('timeout')) {
        isTimeout = true;
    }
    adjustStyle($(this).width());
    $("div.container").bind("tap", function (event) {
        hideKeyboard();
    });

    $('div.container input, div.container a, div.container select, div.container textarea').bind('tap', function (event) {
        event.stopPropagation();
    });
});

$(document).bind('pageload', function (event) {
    if ($('form#loginForm').length > 0) {
        validateLoginForm();
    }
    if ($('form#changePasswordForm').length > 0) {
        validateChangePasswordForm();
    }
    if ($('form#forgotPasswordForm').length > 0) {
        validateForgotPasswordForm();
    }
    if ($('form#resetPasswordForm').length > 0) {
        validateResetPasswordForm();
    }
});

var skip = 0, take = 10, updateTimer = null;

function populateHistoryList() {
    $('#loadMoreBtn').hide();
    $.getJSON("completed-diaries.ashx", { "skip": skip, "take": take }).done(function (json) {
        $.each(json.diaries, function (key, val) {
            $('#historyList').append('<li>' + val.dateReport + '&nbsp;&nbsp;&nbsp;&nbsp;' + val.su + '</li>');
        });
        $('#historyList').listview("refresh");
        if ($('#historyList').children('li').length - 1 < json.count) {
            $('#loadMoreBtn').show();
        }
        skip += take;
    });
    startAutoLock();
}

function populateQuestionaireList() {
    $.mobile.showPageLoadingMsg();
    $.get("questionaires.ashx").done(function (data) {
        $('#questionaireWrapper').html(data);
        $('#questionaireList').listview();
        $.mobile.hidePageLoadingMsg();
    });
}

$(document).bind("pagechangefailed", function (event, data) {
    try {
        if (navigator.onLine) {
            var pageid = $.mobile.activePage.attr('id');
            if (pageid === "home" || pageid === "settings" || pageid === "password" || pageid === "timezone") {
                $.get('check-session.ashx').done(function (data) {
                    if (data === '0') logout();
                });
            }
        } else {
            alert(resources.noConnection);
        }
    }
    catch (ex) { }
});

$(document).bind("pageshow", function (event, ui) {
    notification.init();
    var pageid = $.mobile.activePage.attr('id');
    var prevPageId = ui.prevPage.attr('id');
    if (pageid === "home") {
        if (prevPageId === "home")
            location.href = location.pathname.substring(0, location.pathname.lastIndexOf('/') + 1) + 'home.aspx';
        //populate questionaire list
        populateQuestionaireList();
        updateTimer = window.setInterval(function () {
            var now = new Date();
            if (now.getSeconds() === 0) { //sets the timer’s interval to 600000 milliseconds (or 60 seconds)
                populateQuestionaireList();
            }
        }, 1000);

        //populate history list
        skip = 0;
        $('#historyList li[data-role!="list-divider"]').remove();
        populateHistoryList();
    } else {
        updateTimer = window.clearInterval(updateTimer);
        $('#surveyErrMsg').hide();
    }

    if (pageid === "home" || pageid === "settings" || pageid === "password" || pageid === "timezone") {
        stopSleepCheck();
        startSleepCheck();
        $.get('check-session.ashx').done(function (data) {
            if (data === '0') logout();
        });
    }

    if (pageid === "login") {
        if (isTimeout) {
            $('#timeoutMessage').show();
        } else {
            $('#timeoutMessage').hide();
        }
        if (prevPageId === "login")
            location.href = location.pathname.substring(0, location.pathname.lastIndexOf('/') + 1);
    }
});

$(document).bind('scrollstart', function (event) {
    var pageid = $.mobile.activePage.attr('id');
    if (pageid === "home" || pageid === "settings" || pageid === "password" || pageid === "timezone")
        startAutoLock();
});

$(document).bind('scrollstop', function (event) {
    var pageid = $.mobile.activePage.attr('id');
    if (pageid === "home" || pageid === "settings" || pageid === "password" || pageid === "timezone")
        startAutoLock();
});

$(document).bind('swipe', function (event) {
    var pageid = $.mobile.activePage.attr('id');
    if (pageid === "home" || pageid === "settings" || pageid === "password" || pageid === "timezone")
        startAutoLock();
});

$(document).bind('tap', function (event) {
    var pageid = $.mobile.activePage.attr('id');
    if (pageid === "home" || pageid === "settings" || pageid === "password" || pageid === "timezone")
        startAutoLock();
});

$(document).bind('taphold', function (event) {
    var pageid = $.mobile.activePage.attr('id');
    if (pageid === "home" || pageid === "settings" || pageid === "password" || pageid === "timezone")
        startAutoLock();
});

$(window).on("orientationchange", function (event) {
    var pageid = $.mobile.activePage.attr('id');
    if (pageid === "home" || pageid === "settings" || pageid === "password" || pageid === "timezone")
        startAutoLock();
});

$(document).bind("pagechange", function (e, data) {
    $('.error').html('');
    $('input[type="password"]').val('');
});

function validateLoginForm() {
    $("form#loginForm").validate({
        onkeyup: false,
        messages: {
            txtEmail: {
                required: resources.requiredEmail,
                email: resources.invalidEmail
            },
            txtPassword: resources.requiredPassword
        },
        submitHandler: function (form) {
            $.mobile.showPageLoadingMsg();
            $.ajax({
                url: "login.ashx",
                type: "post",
                data: $("form#loginForm").serialize()
            }).done(function (data) {
                if (data.indexOf("passed") >= 0) {
                    subject.email = $('#txtEmail').val();
                    $('#txtEmail').val('');
                    hideKeyboard();
                    if (timeoutPage.length > 0)
                        $.mobile.changePage(timeoutPage, { transition: "slide" });
                    else
                        $.mobile.changePage("home.aspx", { transition: "slide" });
                    $('div#lfError').html('');
                    startSleepCheck();
                }
                else if ((data.indexOf('surveyref') >= 0) && (/IEMobile|windows phone/i.test(navigator.userAgent))) {
                    location.href = location.pathname + data;
                }
                else if (data.indexOf('surveyref') >= 0) {
                    location.href = location.origin + location.pathname + data;
                }
                else if (data.indexOf('reset-password.aspx') >= 0) {
                    $('div#lfError').html('');
                    $.mobile.changePage(data, { transition: "slide", reloadPage: true });
                } else {
                    $('#txtPassword').val('');
                    $('div#lfError').html(data);
                }
                $.mobile.hidePageLoadingMsg();
            });
        }
    });
}

function validateChangePasswordForm() {
    $("form#changePasswordForm").validate({
        onkeyup: false,
        rules: {
            txtConfirmNewPassword: {
                equalTo: '#txtNewPassword'
            }
        },
        messages: {
            txtOldPassword: resources.requiredOldPassword,
            txtNewPassword: resources.requiredNewPassword,
            txtConfirmNewPassword: {
                required: resources.requiredConfirmPassword,
                equalTo: resources.passwordMismatch
            }
        },
        submitHandler: function (form) {
            $.mobile.showPageLoadingMsg();
            $.ajax({
                url: "change-password.ashx",
                type: "post",
                data: $("form#changePasswordForm").serialize()
            }).done(function (data) {
                if (data.indexOf("passed") >= 0) {
                    hideKeyboard();
                    $.mobile.changePage("settings.aspx", { transition: "slide", reverse: true });
                    $('div#cpfError').html('');                    
                    notification.show(resources.changesSavedMsg);
                } else
                    $('div#cpfError').html(data);

                $.mobile.hidePageLoadingMsg();
            });
        }
    });
}

function validateForgotPasswordForm() {
    $("form#forgotPasswordForm").validate({
        onkeyup: false,
        messages: {
            fpfEmail: {
                required: resources.requiredEmail,
                email: resources.invalidEmail
            }
        },
        submitHandler: function (form) {
            $.mobile.showPageLoadingMsg();
            $.ajax({
                url: "forgot-password.ashx",
                type: "post",
                data: $("form#forgotPasswordForm").serialize()
            }).done(function (data) {
                if (data.indexOf("passed") >= 0) {
                    hideKeyboard();
                    $('div#pnlSuccess').show();
                    $('div#pnlForm').hide();
                    $('.error').html('');
                } else
                    $('.error').html(data);

                $.mobile.hidePageLoadingMsg();
            });
        },
        showErrors: function (errorMap, errorList) {
            $("div#fpError").html('');
            this.defaultShowErrors();
        }
    });
}

function validateResetPasswordForm() {
    $("form#resetPasswordForm").validate({
        onkeyup: false,
        rules: {
            rpfConfirmPassword: {
                equalTo: '#rpfPassword'
            }
        },
        messages: {
            rpfEmail: {
                required: resources.requiredEmail,
                email: resources.invalidEmail
            },
            rpfPassword: resources.requiredPassword,
            rpfConfirmPassword: {
                required: resources.requiredConfirmPassword,
                equalTo: resources.passwordMismatch
            }
        },
        submitHandler: function (form) {
            $.mobile.showPageLoadingMsg();
            $.ajax({
                url: "reset-password.ashx",
                type: "post",
                data: $("form#resetPasswordForm").serialize()
            }).done(function (data) {
                if (data.indexOf("passed") >= 0) {
                    hideKeyboard();
                    $('div#pnlSuccess').show();
                    $('div#pnlForm').hide();
                    $('div#rpfErrorMsg').html('');
                    setTimeout(function () { $.mobile.changePage("../mobile/", {}); }, 5000);
                } else
                    $('div#rpfErrorMsg').html(data);

                $.mobile.hidePageLoadingMsg();
            });
        },
        showErrors: function (errorMap, errorList) {
            $("div#rpfErrorMsg").html('');
            this.defaultShowErrors();
        }
    });
}

function logout(timeout) {
    if (typeof timeout === 'undefined') {
        timeoutPage = '';
        isTimeout = false;
    } else {
        timeoutPage = location.pathname;
        isTimeout = timeout;
    }

    stopSleepCheck();
    window.clearInterval(updateTimer);

    $.get("logout.ashx").done(function (data) {
        if (data.indexOf("loggedout") >= 0) {
            subject.email = '';
            clearTimeout(logoutAttempt);
            logoutAttempt = null;
            $.mobile.changePage("../mobile/", { transition: "slide", reverse: true });
        }
    }).fail(function () {
        logoutAttempt = setTimeout(function () { logout(timeout); }, 2000); 
    });
}

function changeTimeZone() {
    $.mobile.showPageLoadingMsg();
    $.ajax({
        url: "change-timezone.ashx",
        type: "post",
        data: $("#timezoneForm").serialize()
    }).done(function (data) {
        if (data.indexOf("passed") >= 0) {
            $.mobile.changePage("settings.aspx", { transition: "slide", reverse: true });
            notification.show(resources.changesSavedMsg);
        } else
            $('.error').html(data);

        $.mobile.hidePageLoadingMsg();
    });
}

function applyTimeTravel(datetimeValue) {
    $('#btnApply').attr('disabled', 'disabled');
    $.mobile.showPageLoadingMsg();
    $.ajax({
        url: "time-travel-handler.ashx",
        type: "post",
        data: { datetime: datetimeValue },
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8'
    }).done(function (data) {
        if (data.indexOf("passed") >= 0) {
            populateQuestionaireList();
            $('#btnApply').removeAttr('disabled');
        }
    });
}