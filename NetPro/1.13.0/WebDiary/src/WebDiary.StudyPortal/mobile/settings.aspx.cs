﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

using WebDiary.Core.Helpers;
using WebDiary.Core.Constants;
using WebDiary.SWAPI.SWData;
using WebDiary.WDAPI;
using WebDiary.SWAPI;

namespace WebDiary.StudyPortal.Mobile
{
    public partial class Settings : WebDiary.Controls.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblEmail.Text = Session["userName"].ToString();

            // Get subject number
            WDAPIObject wd = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);
            string patientId = Session[WDConstants.VarPtPatientId] as string ?? string.Empty;
            if (string.IsNullOrEmpty(patientId))
            {
                SubjectData sdata = wd.GetSubjectData(Session[WDConstants.VarKrpt].ToString(), new ArrayList { SWAPIConstants.ColumnPatientId });
                patientId = sdata.data.Patientid;
            }
            patientIDLbl.Text = patientId;
        }
    }
}