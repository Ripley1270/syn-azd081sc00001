﻿<%@ Page Title="" Language="C#" MasterPageFile="~/mobile/mobile.Master" AutoEventWireup="true" CodeBehind="reset-password.aspx.cs" Inherits="WebDiary.StudyPortal.Mobile.ResetPassword" %>
        <asp:Content ID="mainContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
            <div class="center-wrapper">
                <div class="server-error"><asp:Literal ID="litServerError" runat="server"></asp:Literal></div>
                <input type="hidden" id="authBrowser" value="" runat="server" />
                <div id="unauthMsgArea" class="server-error">
                    <asp:Label ID="lblUnauthorizedMessage" Visible="false" runat="server">
                    </asp:Label>
                </div>
                <div id="authBrowserArea" class="server-error" runat="server"></div>
                <asp:Panel ID="pnlForm" runat="server">
                    <p>
                        <asp:Label id="lblResetPswrdMsgBody" runat="server" 
                            Text="<%$ Resources:Resource, confirmEmailAndSetPswd%>">
                        </asp:Label>
                    </p>
                    <form id="resetPasswordForm">
                        <div id="rpfErrorMsg" class="error"></div>
                        <div class="form-row">
                            <label for="rpfEmail"><%= Resources.Resource.emailLbl %></label>
                            <input type="text" autocomplete="off" maxlength="256" id="rpfEmail" name="rpfEmail" class="required email" />
                        </div>
                        <div class="form-row">
                            <label for="rpfPassword"><%= Resources.Resource.ResetNewPswrdLbl%></label>
                            <input type="password" id="rpfPassword" name="rpfPassword" autocomplete="off" maxlength="64" oncopy="return false;" onpaste="return false;" oncut="return false;" class="required" />
                        </div>
                        <div class="form-row">
                            <label for="rpfConfirmPassword"><%= Resources.Resource.ConfirmPasswordLbl%></label>
                            <input type="password" id="rpfConfirmPassword" name="rpfConfirmPassword" autocomplete="off" maxlength="64" oncopy="return false;" onpaste="return false;" oncut="return false;" class="required" />
                        </div><br />               
                        <div class="form-row">
                            <input type="submit" value="<%= Resources.Resource.Submit %>" />
                        </div>
                        <asp:Literal ID="litToken" runat="server"></asp:Literal>
                    </form>
                </asp:Panel>
                <div id="pnlSuccess" style="display:none">
                    <p>
                        <asp:Literal ID="litConfirmMsg" runat="server"></asp:Literal>
                    </p>
                </div>
                <script type="text/javascript">
                    $(document).ready(function () {
                        var authBrowser = $('#authBrowser').val();
                        if (authBrowser == "false") {
                            $("#resetPasswordForm input").prop("disabled", true);
                        }
                    });
                </script>
            </div>
         </asp:Content>
