﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="forgot-email-dialog.aspx.cs" Inherits="WebDiary.StudyPortal.mobile.forgot_email_dialog" %>

<!DOCTYPE html> 
<html>
<head runat="server">
    <title></title>
</head>
<body>
    <div data-role="page">
    <div data-role="header">
    <h2><%= Resources.Resource.ForgotEmail %></h2>
    </div>
    <div data-role="content" class="body">
    <p><%= Resources.Resource.ForgotEmailMSG %></p>
    </div>
</div>
</body>
</html>
