﻿using System;
using System.Linq;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Globalization;
using log4net;
using WebDiary.Core.Errors;
using WebDiary.SWAPI;
using WebDiary.StudyPortal.Api.Models;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;

namespace WebDiary.StudyPortal.Api.Controllers
{
    public class TimeZonesController: ApiController
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(TimeZonesController));
        private WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString);

        //GET api/v1/TimeZones
        //only utc is supported. Other timezones are not supported yet
        public HttpResponseMessage Get(string id)
        {
            try
            {
                if (string.Equals(id, "utc", StringComparison.OrdinalIgnoreCase))
                {
                    TimeZoneModel timeZoneResult = new TimeZoneModel();

                    UTCRepository utcr = new UTCRepository(db);
                    DateTime returnedDateTime = utcr.GetUTC().ToList().Single();

                    DateTime utc = DateTime.SpecifyKind(returnedDateTime, DateTimeKind.Utc);
                    timeZoneResult.UTC = utc.ToString(SWAPIConstants.UTCDateTimeFormat, CultureInfo.InvariantCulture);

                    logger.Info("Current UTC: " + timeZoneResult.UTC); 

                    HttpResponseMessage responseMsg = Request.CreateResponse<TimeZoneModel>(HttpStatusCode.OK, timeZoneResult);
                    return responseMsg;
                }
                else
                {
                    HttpResponseException responseException = new HttpResponseException(HttpStatusCode.NotFound);
                    throw responseException;
                }
            }
            catch (SqlException sqlEx)
            {
                logger.Error(sqlEx.Message, sqlEx);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                responseException.Response.Headers.Add("X-Error-Code", SWISStatusCodes.ServerError);
                throw responseException;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                db.Dispose();
            base.Dispose(disposing);
        }
    }
}