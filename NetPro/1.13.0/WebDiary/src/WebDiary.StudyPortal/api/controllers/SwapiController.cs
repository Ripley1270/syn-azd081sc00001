﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebDiary.StudyPortal.Api.Models;
using WebDiary.WDAPI;
using WebDiary.Core.Helpers;
using log4net;
namespace WebDiary.StudyPortal.Api.Controllers
{
    public class SwapiController : ApiController
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(SwapiController));

        // POST /api/v1/SWAPI/GetDataClin
        [HttpPost]
        [Authorize]
        public HttpResponseMessage GetDataClin([FromBody] StoredProcModel storedproc)
        {
            try
            {
                // Perform a SWAPI call
                WDAPIObject wdapi = new WDAPIObject(StudyData.study.StudyWorksUsername, StudyData.study.StudyWorksPassword, StudyData.study.Name, StudyData.study.StudyWorksBaseUrl);
                return Request.CreateResponse(HttpStatusCode.OK, wdapi.GetData(storedproc.StoredProc, storedproc.Params).response);
            }
            catch (SWAPI.SWAPIException ex)
            {
                logger.Error(ex.Message, ex);
                HttpResponseException responseException = new HttpResponseException(HttpStatusCode.InternalServerError);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}