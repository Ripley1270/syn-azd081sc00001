﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using WebDiary.StudyPortal.Api.Models;
using WebDiary.Core.Errors;
using WebDiary.Core.Helpers;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using System.Threading;
using log4net;
using WebDiary.SWAPI.SWData;
using WebDiary.SWAPI;
using WebDiary.WDAPI;
using Newtonsoft.Json.Linq;

namespace WebDiary.StudyPortal.Api.Helpers
{
    public class RegisterDevice
    {
        /// <summary>
        /// Register a device for SitePad App
        /// </summary>
        /// <param name="deviceData">device information to register with StudyWorks</param>
        /// <returns>StudyWorks deviceId</returns>
        public static int Register(DeviceData deviceData)
        {
            Uri uri = new Uri(StudyData.study.StudyWorksBaseUrl.TrimEnd('/') + "/Voltron/DeviceJsonRpcService");

            JsonRpcRequest request = new JsonRpcRequest(uri, "registerDevice", new JArray(
                    StudyData.study.StudyWorksUsername,
                    StudyData.study.StudyWorksPassword,
                    StudyData.study.Name,
                    new JObject(
                        new JProperty("deviceUsername", deviceData.deviceUsername),
                        new JProperty("devicePassword", deviceData.devicePassword),
                        new JProperty("siteCode", deviceData.siteCode),
                        new JProperty("adminDom", deviceData.adminDom),
                        new JProperty("deviceSerialNumber", deviceData.deviceSerialNumber),
                        new JProperty("deviceType", deviceData.deviceType),
                        new JProperty("deviceTypeID", deviceData.deviceTypeId),
                        new JProperty("model", deviceData.model)
                    )
                ));

            JObject response = request.GetResponse();

            return (int)response["result"];
        }
    }
}