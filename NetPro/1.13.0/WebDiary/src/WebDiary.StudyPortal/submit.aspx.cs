﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Globalization;
using System.Text.RegularExpressions;

using WebDiary.SurveyToolDBAdapters;
using System.Text;
using WebDiary.SWAPI.SWData;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Helpers;
using WebDiary.Core.Errors;
using WebDiary.WDAPI;
using WebDiary.SWAPI;
using WebDiary.SurveyToolDBAdapters.Checkbox_Entities;

using log4net;
using WebDiary.Controls;
using WebDiary.Core.Constants;
using WebDiary.Membership.User;
using WebDiary.Membership.Provider;
using Newtonsoft.Json;
using System.Collections.Specialized;
using WebDiary.StudyPortal.MedicationModule;

namespace WebDiary.StudyPortal
{
    public partial class Submit : BasePage
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(Submit));
        protected const string ReturnUrl = "submit_return_url";
        private string psdIdForEQ5D = string.Empty; 

        protected void Page_Load(object sender, EventArgs e)
        {
            psdIdForEQ5D = Request.QueryString[WDConstants.VarPsdId] as string ?? string.Empty;

            if (!IsPostBack)
            {
                SubmitData();
            }
            Cancel.Click += new EventHandler(BtnCancel_Click);
            Retry.Click += new EventHandler(BtnRetry_Click);
        }

        protected void SubmitData(bool retry = false)
        {
            int responseId;
            string returnUrl = String.Empty;

            ClinRecord baseClinRecord = null;
            PDEClinRecord pdeClinRecord = null;
            StudySurveys survey = null;
            SurveyResponseData surveyResponseData;
            SurveyData surveyData = null;

            //Abort flag
            Boolean abortSurvey = false;

            try
            {
                responseId = Convert.ToInt32(WebDiary.Core.Helpers.HttpHelper.GetQueryString("id", "0"), CultureInfo.InvariantCulture);

                using (WebDiaryContext wdDb = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    if (Request.QueryString[WDConstants.VarPsdId] != null)
                    {
                        //EuroQOL
                        surveyData = (new SurveyDataRepository(wdDb)).FindById(new Guid(Request.QueryString[WDConstants.VarPsdId].ToString()));

                        if (retry)
                        {
                            //Get EQ5D data from session
                            Dictionary<string, string> answers = new Dictionary<string, string>();
                            answers = HttpContext.Current.Session[psdIdForEQ5D] as Dictionary<string, string>;
                            surveyResponseData = new SurveyResponseData();
                            surveyResponseData.Answers = answers;
                        }
                        else
                        {
                            surveyResponseData = EQData.ResponseData();
                            //Add EQ5D data to session to retrieve it if first submission fails and data needs to be resubmitted later 
                            HttpContext.Current.Session[psdIdForEQ5D] = surveyResponseData.Answers;
                        }

                    }
                    else
                    {
                        //Checkbox
                        surveyResponseData = CheckboxDBAdapter.getSurveyData(responseId);
                        surveyData = (new SurveyDataRepository(wdDb)).FindById(new Guid(surveyResponseData.HiddenItems[WDConstants.PsdId]));
                    }
                    if (surveyData != null)
                    {
                        //[custom] Set phase base of schedule type
                        String newPhase = string.Empty;

                        if (surveyResponseData.Answers.ContainsKey("VisitConfirmation.0.VST1L"))
                        {
                            String visit = surveyResponseData.Answers["VisitConfirmation.0.VST1L"];
                            String currentPhase = surveyData.SuPhaseAtEntry;

                            if (currentPhase.Equals(PDETools.PHASE_SCREENING) && visit.Equals(PDETools.VISIT_RESCREENING))
                            {
                                newPhase = PDETools.PHASE_RESCREENING_1;
                            }
                            else if (currentPhase.Equals(PDETools.PHASE_RESCREENING_1) && visit.Equals(PDETools.VISIT_RESCREENING))
                            {
                                newPhase = PDETools.PHASE_RESCREENING_2;
                            }
                            else if (currentPhase.Equals(PDETools.PHASE_RESCREENING_2) && visit.Equals(PDETools.VISIT_RESCREENING))
                            {
                                newPhase = PDETools.PHASE_RESCREENING_3;
                            }
                            else if (currentPhase.Equals(PDETools.PHASE_RESCREENING_3) && visit.Equals(PDETools.VISIT_RESCREENING))
                            {
                                newPhase = PDETools.PHASE_RESCREENING_4;
                            }
                            else if (currentPhase.Equals(PDETools.PHASE_RESCREENING_4) && visit.Equals(PDETools.VISIT_RESCREENING))
                            {
                                newPhase = PDETools.PHASE_RESCREENING_5;
                            }
                            else if (currentPhase.Equals(PDETools.PHASE_RESCREENING_5) && visit.Equals(PDETools.VISIT_RESCREENING))
                            {
                                newPhase = PDETools.PHASE_RESCREENING_6;
                            }
                            else if (currentPhase.Equals(PDETools.PHASE_RESCREENING_6) && visit.Equals(PDETools.VISIT_RESCREENING))
                            {
                                newPhase = PDETools.PHASE_RESCREENING_7;
                            }
                            else if (currentPhase.Equals(PDETools.PHASE_RESCREENING_7) && visit.Equals(PDETools.VISIT_RESCREENING))
                            {
                                newPhase = PDETools.PHASE_RESCREENING_8;
                            }
                            else if (currentPhase.Equals(PDETools.PHASE_RESCREENING_8) && visit.Equals(PDETools.VISIT_RESCREENING))
                            {
                                newPhase = PDETools.PHASE_RESCREENING_9;
                            }
                            else if (currentPhase.Equals(PDETools.PHASE_RESCREENING_9) && visit.Equals(PDETools.VISIT_RESCREENING))
                            {
                                newPhase = PDETools.PHASE_RESCREENING_10;
                            }
                            else if (visit.Equals(PDETools.VISIT_START_TREATMENT))
                            {
                                newPhase = PDETools.PHASE_TREATMENT;
                            }
                            else if (visit.Equals(PDETools.VISIT_STUDY_DISCONTINUATION))
                            {
                                newPhase = PDETools.PHASE_PRETERMINATION;
                            }
                            else if (visit.Equals(PDETools.VISIT_TREATMENT_DISCONTINUATION))
                            {
                                newPhase = PDETools.PHASE_FOLLOWUP_1;
                            }
                            else if (visit.Equals(PDETools.VISIT_PROGRESSION_AND_TREATMENT_DISCONTINUATION))
                            {
                                newPhase = PDETools.PHASE_FOLLOWUP_2;
                            }
                            else if (visit.Equals(PDETools.VISIT_PROGRESSION))
                            {
                                newPhase = PDETools.PHASE_FOLLOWUP_2;
                            }
                        }


                        ViewState["sdataid"] = surveyData.Id;
                        survey = new SurveyRepository(wdDb).FindById(surveyData.SurveyId);
                        returnUrl = surveyData.ReturnUrl;
                        baseClinRecord = WDAPIObject.GenerateClinicalRecord(surveyResponseData, surveyData, newPhase);

                        pdeClinRecord = new PDEClinRecord(baseClinRecord);

                        StudyRepository studyRep = new StudyRepository(wdDb);
                        Study study = studyRep.FindById(Convert.ToInt32(surveyData.StudyId, CultureInfo.InvariantCulture));

                        //PDE - if assignment set Protocol to PT.Custom10 and PT.Custom9
                        //TODO - if multi protocol study, protocol must be set
                        String protocol = PDETools.DEFAULT_PROTOCOL;

                        //TODO Add to the template
                        //Query to be append to the redirect URL
                        NameValueCollection PDERedirectQuery = System.Web.HttpUtility.ParseQueryString(newPhase);
                        //PDE Resource ID to be display in the instruction page
                        String PDEResourceInstructions = null;

                        if (pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSubjectAssignment))
                        {
                            //PT.Custom10 - subject's current protocol
                            //Commented out for 1.13.0 - core added protocol to assignment form
                            pdeClinRecord.addSysVar("PT.Custom10", protocol);
                            //Pt.Custom9 - subject's initial protocol
                            pdeClinRecord.addSysVar("PT.Custom9", protocol);
                        }

                        //Add protocol to netpro forms
                        if (!pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeEditSubject) &&
                            !pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSubjectAssignment))
                        {

                            //Pull PT.Custom10 and set that as protocol
                            WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
                            SubjectData sdata = wd.GetSubjectData(Session[WDConstants.VarKrpt].ToString(),
                                                                                new ArrayList { SWAPIConstants.ColumnPatientCustom10 });
                            protocol = sdata.data.custom10;

                            pdeClinRecord.addIT("Protocol", "Protocol", protocol);
                        }


                        /*
                         *  PDE - add custom variables to form here
                         *  
                         * 
                         * Ex:
                         *       if (pdeClinRecord.clinRecord.krsu.Equals(PDETools.KRSU_VISIT_CONFIRMATION))
                         *       {
                         *           //pdeClinRecord.addIT(<it>, <ig>, <value>);
                         *           pdeClinRecord.addIT("VST1L", "Visit", "1");
                         *       }
                         */

                        WDAPIObject wdapi = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword,
                            study.Name, study.StudyWorksBaseUrl);
                        SubjectRepository subjectRep = new SubjectRepository(wdDb);
                        Subject subj = subjectRep.FindByKrptAndStudy(surveyData.Krpt, surveyData.StudyId);

                        if (pdeClinRecord.clinRecord.krsu.Equals(PDETools.KRSU_MED_SELECTION))
                        {
                            MedicationSelection.onSaved(pdeClinRecord, surveyResponseData);
                        }

                        if (pdeClinRecord.clinRecord.krsu.Equals(PDETools.KRSU_OTHER_MED_RESOLUTION))
                        {
                            OtherMedResolution.onSaved(pdeClinRecord, surveyResponseData, wdapi, subj);
                        }                        
                        
                        //[MedModule] Display med instruction text on Assignment
                        if (pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSubjectAssignment))
                        {
                            PDEResourceInstructions = "MedInstructions";
                        }

                        //Visit Confirmation add values
                        if (pdeClinRecord.clinRecord.krsu.Equals(PDETools.KRSU_VISITCONFIRMATION))
                        {
                            String visit = "";

                            if (surveyResponseData.Answers.ContainsKey("VisitConfirmation.0.VST1L"))
                            {
                                visit = surveyResponseData.Answers["VisitConfirmation.0.VST1L"];
                            }

                            ClinField reportDate = pdeClinRecord.clinRecord.sysvars.Find(delegate(ClinField field) { return field.sysvar.Equals("SU.ReportDate"); });

                            if (visit.Equals(PDETools.VISIT_START_TREATMENT))
                            {
                                if (surveyResponseData.Answers.ContainsKey("VisitConfirmation.0.RNDMDT1D"))
                                {
                                    String RNDMDT1D = surveyResponseData.Answers["VisitConfirmation.0.RNDMDT1D"];
                                    if (RNDMDT1D != null && reportDate != null)
                                    {
                                        int diffInDays = (DateTime.Parse(reportDate.value).Date - DateTime.Parse(RNDMDT1D).Date).Days + 1;
                                        pdeClinRecord.addIT("STYDAY1D", "VisitConfirmation", RNDMDT1D);
                                        pdeClinRecord.addIT("STYDAY1N", "VisitConfirmation", diffInDays.ToString());
                                    }
                                }
                            }
                            else if (surveyResponseData.HiddenItems.ContainsKey("studyDay"))
                            {
                                String studyDay = surveyResponseData.HiddenItems["studyDay"];

                                if (studyDay != null)
                                {
                                    pdeClinRecord.addIT("STYDAY1N", "VisitConfirmation", studyDay);
                                }
                            }


                            if (visit.Equals(PDETools.VISIT_RESCREENING))
                            {
                                if (reportDate != null)
                                {
                                    pdeClinRecord.addIT("RESCRN1D", "VisitConfirmation", reportDate.value);
                                }
                            }

                        }
                        else if (surveyResponseData.HiddenItems.ContainsKey("studyDay"))
                        {
                            String studyDay = surveyResponseData.HiddenItems["studyDay"];

                            if (studyDay != null)
                            {
                                pdeClinRecord.addIT("STYDAY1N", "VisitConfirmation", studyDay);
                            }
                        }

                        //TODO move this up in the template
                        //StudyRepository studyRep = new StudyRepository(wdDb);
                        //Study study = studyRep.FindById(Convert.ToInt32(surveyData.StudyId, CultureInfo.InvariantCulture));

                        if (study != null)
                        {
                            if (String.IsNullOrEmpty(returnUrl))
                            {
                                returnUrl = study.WebDiaryStudyBaseUrl;
                                if (!returnUrl.EndsWith("/", StringComparison.Ordinal))
                                    returnUrl += "/";
                            }

                            //Check Session
                            //determine if the request is for patient access or site access
                            if (pdeClinRecord.clinRecord.formtype.Equals("Subject Submit")) //Patient gateway
                            {
                                if (Session["login"] == null)
                                {
                                    //We should delete submitted response from checkbox db
                                    redirectScript.Text = "<script type=\"text/javascript\">top.location.href = '" + returnUrl + "';</script>";
                                    return;
                                }
                                else
                                {
                                    var isApproved = (from subject in wdDb.Subjects
                                                      where subject.KrPT == (string)Session[WDConstants.VarKrpt]
                                                      select subject.IsApproved).SingleOrDefault();

                                    if (!isApproved)
                                    {
                                        this.litResult.Text = Resources.Resource.SubmitErrorInactive;
                                        Session[ReturnUrl] = returnUrl = "login.aspx";
                                        Cancel.Text = Resources.Resource.SubmitContinue;
                                        DivPanel2.Visible = false;
                                        return;
                                    }

                                }
                            }
                            else //site gateway
                            {
                                string ssa_url = Session[WDConstants.VarSwReturnUrl] as string;
                                if (string.IsNullOrEmpty(ssa_url))
                                {
                                    if (!study.SsaBaseUrl.EndsWith("/", StringComparison.Ordinal))
                                        ssa_url = study.SsaBaseUrl + "/ssa";
                                    else
                                        ssa_url = study.SsaBaseUrl + "ssa";
                                }

                                if (!string.IsNullOrEmpty(Session[WDConstants.VarRequestUrl] as string))
                                {
                                    if (!SessionHelper.CheckSwSession(Session[WDConstants.VarRequestUrl] as string))
                                    {
                                        //Code to delete submitted response from checkbox db
                                        redirectScript.Text = "<script type=\"text/javascript\">top.location.href = '" + ssa_url + "';</script>";
                                        return;
                                    }
                                }
                                else
                                {
                                    //Code to delete submitted response from checkbox db
                                    redirectScript.Text = "<script type=\"text/javascript\">top.location.href = '" + ssa_url + "';</script>";
                                    return;
                                }
                            }

                            //TODO move this up in the template
                            //WDAPIObject wdapi = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword,
                            //    study.Name, study.StudyWorksBaseUrl);

                            if (string.IsNullOrEmpty(pdeClinRecord.clinRecord.ptsigorig))
                            {
                                foreach (var field in pdeClinRecord.clinRecord.sysvars)
                                {
                                    if (field.sysvar.StartsWith("pt.", StringComparison.OrdinalIgnoreCase))
                                    {
                                        SubjectData sdata = wdapi.GetSubjectData(pdeClinRecord.clinRecord.krpt,
                                                                                        new ArrayList {SWAPIConstants.ColumnPatientSigOrig, 
                                                                                        SWAPIConstants.ColumnPatientSigId});
                                        if (sdata != null)
                                        {
                                            pdeClinRecord.clinRecord.ptsigorig = sdata.data.Sigorig;
                                            pdeClinRecord.clinRecord.ptsigprev = sdata.data.Sigid;
                                        }
                                        break;
                                    }
                                }
                            }

                            bool isSubjectOrStudyEnabled = true;
                            SubjectStudyEnabled result = SubjectStudyEnabled.Error;

                            // Do not call IsSubjectAndStudyEnabled if FormType is SubjectAssignment
                            if (!pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSubjectAssignment))
                            {
                                result = wdapi.IsSubjectAndStudyEnabled(pdeClinRecord.clinRecord.krpt, study); 
                                if (result == SubjectStudyEnabled.SubjectDisabled || result == SubjectStudyEnabled.StudyDisabled || result == SubjectStudyEnabled.Error)
                                {
                                    isSubjectOrStudyEnabled = false;
                                }
                            }

                            if (pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSubjectAssignment) || isSubjectOrStudyEnabled)
                            {
                                // Set NetProInformation IG
                                VersionsRepository verResp = new VersionsRepository(wdDb);
                                Versions npVer = verResp.FindById(1);
                                StudyVersions studyVer = study.StudyVersions;
                                List<ClinField> NetPROInfo = new List<ClinField>(5);

                                ClinField NPVersion = new ClinField();
                                NPVersion.krit = "NPVersion";
                                NPVersion.value = (npVer.MicroVersion == 0) ? npVer.MajorVersion + "." + npVer.MinorVersion : npVer.MajorVersion + "." + npVer.MinorVersion + "." + npVer.MicroVersion;
                                NPVersion.edit = false;

                                ClinField NPStudyVersion = new ClinField();
                                NPStudyVersion.krit = "NPStudyVersion";
                                NPStudyVersion.value = studyVer.MajorVersion + "." + studyVer.MinorVersion;
                                NPStudyVersion.edit = false;

                                ClinField OSVersion = new ClinField();
                                OSVersion.krit = "OSVersion";
                                if (Request.Browser.Platform != "Unknown")
                                    OSVersion.value = Request.Browser.Platform;
                                else
                                {
                                    string pattern = @"\([^()]*\)";
                                    MatchCollection matches = Regex.Matches(Request.UserAgent, pattern);
                                    OSVersion.value = matches[0].Value.Trim('(', ')');
                                }
                                OSVersion.edit = false;

                                ClinField browserType = new ClinField();
                                browserType.krit = "BrowserName";
                                browserType.value = Request.Browser.Type;
                                browserType.edit = false;

                                ClinField browserVersion = new ClinField();
                                browserVersion.krit = "BrowserVersion";
                                browserVersion.value = Request.Browser.Version;
                                browserVersion.edit = false;

                                NetPROInfo.Add(NPVersion);
                                NetPROInfo.Add(NPStudyVersion);
                                NetPROInfo.Add(OSVersion);
                                NetPROInfo.Add(browserType);
                                NetPROInfo.Add(browserVersion);

                                Dictionary<string, List<ClinField>> ig = new Dictionary<string, List<ClinField>>();
                                ig.Add("0", NetPROInfo);
                                pdeClinRecord.clinRecord.iglist.Add("NetProInformation", ig);

                                //Set krUSER.
                                if (!String.IsNullOrEmpty(surveyData.Uid) && !String.IsNullOrEmpty(pdeClinRecord.clinRecord.krUserPw))
                                {
                                    pdeClinRecord.clinRecord.kruser = surveyData.Uid;

                                    wdapi.UserName = surveyData.Uid;
                                    wdapi.Password = pdeClinRecord.clinRecord.krUserPw;
                                }
                                else
                                {
                                    pdeClinRecord.clinRecord.kruser = study.StudyWorksUsername;
                                }

                                //PDE Don't submit if aborted
                                if (!abortSurvey)
                                {
                                    wdapi.SubmitClinReportRRInk(pdeClinRecord.clinRecord);
                                }

                                if (pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSubjectAssignment))
                                    this.litResult.Text = Resources.Resource.SubmitAssignmentSucc;
                                else if (pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeOffStudy))
                                    this.litResult.Text = Resources.Resource.SubmitOffStudySucc;
                                else if (pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSubjectSubmit))
                                    this.litResult.Text = Resources.Resource.SubmitDiarySucc;
                                else
                                    this.litResult.Text = Resources.Resource.SubmitSucc;

                                wdDb.SurveyData.DeleteOnSubmit(surveyData);
                                wdDb.SubmitChanges();

                                if (pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSubjectAssignment) == false)
                                {
                                    //TODO move this up in the template
                                    //SubjectRepository subjectRep = new SubjectRepository(wdDb);

                                    Subject subject = subjectRep.FindByKrptAndStudy(surveyData.Krpt, surveyData.StudyId);
                                    SurveyScheduler.ScheduleSurveyEmail(survey, subject, study, SurveyScheduler.ScheduleState.Submit);
                                }

                                //Delete pending emails for a terminated subject
                                if (pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeOffStudy) && !wdapi.IsSubjectEnabled(surveyData.Krpt))
                                    SurveyScheduler.DeletePendingEmail(surveyData.Krpt, surveyData.StudyId);

                                Session[WDConstants.ResponseId] = responseId;
                                Session[WDConstants.PsdId] = surveyData.Id;

                                if (!string.IsNullOrEmpty(psdIdForEQ5D) && Session[psdIdForEQ5D] != null)
                                    Session.Remove(psdIdForEQ5D);

                                //TODO Add to the template
                                String PDERedirectQueryString = String.Empty;

                                //If PDE Resource instruction is set, add it to Query String
                                if (PDEResourceInstructions != null)
                                {
                                    PDERedirectQuery.Add("displayPDEResource", PDEResourceInstructions);
                                }

                                //TODO Add to the template
                                if (PDERedirectQuery.Count > 0)
                                {
                                    PDERedirectQueryString = "?";
                                    PDERedirectQueryString += PDERedirectQuery.ToString();
                                }

                                if (!pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSubjectAssignment))
                                    Session["SubmitResult"] = litResult.Text;
                                Page.Form.Style.Add("display", "none");
                                if (Session["isMobile"] != null)
                                    //TODO add to the PDE Template
                                    redirectScript.Text = "<script type=\"text/javascript\">top.location.href = '" + study.WebDiaryStudyBaseUrl.TrimEnd('/') + "/mobile/home.aspx" + PDERedirectQueryString.ToString() + "';</script>";
                                else if (pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSubjectAssignment))
                                {
                                    //Do not show Whats Next Instruction and direct to Study Works
                                    if (DoNotShowInstruction())
                                        redirectScript.Text = "<script type=\"text/javascript\">top.location.href = '" + returnUrl + "';</script>";
                                    else
                                    {
                                        Session[ReturnUrl] = returnUrl;
                                        //Populate so it can be used if SW session is expired
                                        Session[WDConstants.VarSSABASEURL] = study.SsaBaseUrl;
                                        redirectScript.Text = "<script type=\"text/javascript\">top.location.href = 'instruction.aspx" + PDERedirectQueryString.ToString() + "';</script>";
                                    }
                                }
                                else
                                    redirectScript.Text = "<script type=\"text/javascript\">top.location.href = '" + returnUrl + PDERedirectQueryString.ToString() + "';</script>";
                                return;
                            }
                            else if (result == SubjectStudyEnabled.StudyDisabled || result == SubjectStudyEnabled.SubjectDisabled)
                            {
                                this.litResult.Text = Resources.Resource.SubmitErrorInactive;
                                Session[ReturnUrl] = returnUrl = "login.aspx";
                                Cancel.Text = Resources.Resource.SubmitContinue;
                                DivPanel2.Visible = false;
                                return;
                            }
                            else if (result == SubjectStudyEnabled.Error)
                            {
                                Session[ReturnUrl] = returnUrl;
                                return;
                            }
                            else
                            {
                                this.litResult.Text = Resources.Resource.SubmitErrorInactive;
                                returnUrl = "logout.aspx";
                                Cancel.Text = Resources.Resource.SubmitContinue;
                                DivPanel2.Visible = false;
                            }
                        }
                        else
                        {
                            this.litResult.Text = Resources.Resource.SubmitErrorSending;
                            logger.Error("The Study ID was not found in the WebDiary STUDIES table.");
                        }
                    }
                    else
                    {
                        // check if user submit the same response
                        if ((Session[WDConstants.PsdId] != null && Session[WDConstants.PsdId].ToString() == Request.QueryString[WDConstants.VarPsdId]) || (Session[WDConstants.ResponseId] != null && (int)Session[WDConstants.ResponseId] == responseId))
                        {
                            litResult.Text = Resources.Resource.SubmitTwice;
                            Cancel.Text = Resources.Resource.SubmitContinue;
                            DivPanel2.Visible = false;
                            return;
                        }
                        else
                        {
                            this.litResult.Text = Resources.Resource.SubmitErrorSending;
                            logger.Error("The survey data ID was not found in the PRODUCT_SURVEY_DATA table");
                        }
                    }
                }
            }
            catch (SWAPIException swex)
            {
                logger.Error(swex.Message, swex);

                //10391 is duplicate error returned from StudyWorks if sigid already exists in opslog table
                if ((String.Compare(swex.Error.error_message, "10391", StringComparison.InvariantCulture) == 0) &&
                    (pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSubjectSubmit) || pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSiteSubmit)))
                {
                    try
                    {
                        //capture data
                        NetProDuplicateReport npdr = new NetProDuplicateReport();
                        //get report start date
                        ClinField clinReportstartDate = new ClinField();
                        clinReportstartDate = pdeClinRecord.clinRecord.sysvars.Find(item => item.sysvar == "SU.ReportStartDate");
                        string reportStartDate = string.Empty;
                        reportStartDate = HttpUtility.UrlDecode(clinReportstartDate.value);

                        npdr.Krpt = pdeClinRecord.clinRecord.krpt;
                        npdr.StudyName = survey.Study.Name;
                        npdr.Krsu = HttpUtility.UrlDecode(pdeClinRecord.clinRecord.krsu);
                        npdr.SurveyId = surveyData.SurveyId;
                        npdr.ReportStartDateUtc = surveyData.SurveyStartDate;
                        npdr.ReportStartDate = reportStartDate;
                        npdr.Sigid = pdeClinRecord.clinRecord.sigid;
                        npdr.ClinRecord = ToJSON(pdeClinRecord.clinRecord);

                        using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                        {
                            db.NetProDuplicateReport.InsertOnSubmit(npdr);
                            db.SubmitChanges();

                            SurveyData surveyData2 = (new SurveyDataRepository(db)).FindById(surveyData.Id);
                            if (surveyData2 != null)
                            {
                                db.SurveyData.DeleteOnSubmit(surveyData2);
                                db.SubmitChanges();
                            }
                        }
                        //end of capture data

                        DivPanel1.Visible = false;
                        DivPanel2.Visible = false;

                        string resultText = string.Empty;
                        if (pdeClinRecord.clinRecord.formtype.Equals(SWAPIConstants.FormTypeSubjectSubmit))
                            resultText = Resources.Resource.SubmitDiarySucc;
                        else
                            resultText = Resources.Resource.SubmitSucc;

                        //TODO Add to PDE Template
                        //[PDE] Clear existing custom JS, CSS and inline javascript code
                        PDECommonTools.setOnLoadJS_CSSFile(Session, null);
                        PDECommonTools.setOnLoadInlineJavascript(Session, null);

                        Session["SubmitResult"] = resultText;
                        if (Session["isMobile"] != null)
                            redirectScript.Text = "<script type=\"text/javascript\">top.location.href = '" + survey.Study.WebDiaryStudyBaseUrl.TrimEnd('/') + "/mobile/home.aspx';</script>";
                        else
                            redirectScript.Text = "<script type=\"text/javascript\">top.location.href = '" + returnUrl + "';</script>";
                        return;
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message, ex);
                        this.litResult.Text = Resources.Resource.SubmitErrorSending;
                        DivPanel1.Visible = true;
                        DivPanel2.Visible = true;

                        if (Session[ReturnUrl] == null && string.IsNullOrEmpty(returnUrl))
                        {
                            Session[WDError.ErrorMessageKey] = Resources.Resource.ErrUnexpectedErr;
                            redirectScript.Text = "<script type=\"text/javascript\">top.location.href = 'wd-error.aspx';</script>";
                        }
                    }
                }
                else
                {
                    this.litResult.Text = Resources.Resource.SubmitErrorSending;
                    if (Session[ReturnUrl] == null && string.IsNullOrEmpty(returnUrl))
                    {
                        Session[WDError.ErrorMessageKey] = Resources.Resource.ErrUnexpectedErr;
                        redirectScript.Text = "<script type=\"text/javascript\">top.location.href = 'wd-error.aspx';</script>";
                    }
                }
            }
            catch (System.Net.WebException webex)
            {
                litResult.Text = Resources.Resource.ServerDown;
                logger.Error(webex.Message, webex);
            }
            catch (Exception ex)
            {
                this.litResult.Text = Resources.Resource.SubmitErrorSending;
                logger.Error(ex.Message, ex);
                if (Session[ReturnUrl] == null && string.IsNullOrEmpty(returnUrl))
                {
                    Session[WDError.ErrorMessageKey] = Resources.Resource.ErrUnexpectedErr;
                    redirectScript.Text = "<script type=\"text/javascript\">top.location.href = 'wd-error.aspx';</script>";
                }
            }
            if (!string.IsNullOrEmpty(returnUrl))
                Session[ReturnUrl] = returnUrl;
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            // delete questionnaire data from PRODUCT_SURVEY_DATA table
            if (ViewState["sdataid"] != null)
            {
                using (WebDiaryContext wdDb = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    SurveyData surveyData = (new SurveyDataRepository(wdDb)).FindById(new Guid(ViewState["sdataid"].ToString()));
                    if (surveyData != null)
                    {
                        wdDb.SurveyData.DeleteOnSubmit(surveyData);
                        wdDb.SubmitChanges();
                    }
                }
            }

            // remove session psdIdForEQ5D saved for EQ5D
            if (!string.IsNullOrEmpty(psdIdForEQ5D) && Session[psdIdForEQ5D] != null)
                Session.Remove(psdIdForEQ5D);

            if (Session[ReturnUrl] == null)
            {
                Session[WDError.ErrorMessageKey] = Resources.Resource.ErrUnexpectedErr;
                redirectScript.Text = "<script type=\"text/javascript\">top.location.href = 'wd-error.aspx';</script>";
            }
            else
                redirectScript.Text = "<script type=\"text/javascript\">top.location.href = '" + Session[ReturnUrl].ToString() + "';</script>";
        }

        protected void BtnRetry_Click(object sender, EventArgs e)
        {
            SubmitData(true);
        }

        protected bool DoNotShowInstruction()
        {
            string siteUserId = Session[WDConstants.VarUserId] as string ?? string.Empty;
            string hashedSiteUserId = CryptoHelper.EncodeUserName(siteUserId);

            if ((Request.Cookies[hashedSiteUserId] != null) && !string.IsNullOrEmpty(Request.Cookies[hashedSiteUserId].Value))
            {
                if (Request.Cookies[hashedSiteUserId].Value.Equals("true", StringComparison.OrdinalIgnoreCase))
                    return true;
            }
            return false;
        }

        protected static string ToJSON(Object obj)
        {
            string jsonStr = null;

            jsonStr = JsonConvert.SerializeObject(obj);

            return jsonStr;
        }
    }
}
