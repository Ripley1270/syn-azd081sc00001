﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

using System.Net;
using System.Net.Configuration;
using System.IO;
using System.Text;
using System.Configuration;

using System.Net.Mail;
using log4net;

using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using WebDiary.Controls;
using WebDiary.Core.Helpers;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Email;
using WebDiary.Core.Constants;
using WebDiary.WDAPI;
using System.Globalization;
using WebDiary.StudyPortal.Helpers;


namespace WebDiary.StudyPortal
{
    public partial class ForgotPassword : WebDiary.Controls.BasePage
    {
        private static readonly ILog logger = log4net.LogManager.GetLogger(typeof(ForgotPassword));

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                HttpBrowserCapabilities browser = Request.Browser;
                bool isAuthorizedBrowser = AuthorizedBrowser.CheckAuthorizedBrowser(StudyData.study.Name, browser.Browser, browser.MajorVersion);
                if (!isAuthorizedBrowser)
                {
                    lblUnauthorizedMessage.Visible = true;
                    string browserNameVersion = browser.Browser;

                    if (string.Equals(browserNameVersion, "Edge", StringComparison.OrdinalIgnoreCase))
                        browserNameVersion = browserNameVersion + "HTML" + " " + browser.MajorVersion;
                    else
                        browserNameVersion = browserNameVersion + " " + browser.MajorVersion;
                    lblUnauthorizedMessage.Text = String.Format(CultureInfo.CurrentCulture, Resources.Resource.UnauthorizedBrowserMsg.ToString(CultureInfo.CurrentCulture), browserNameVersion);

                    List<string> authorizedBrowser = AuthorizedBrowser.GetAuthorizedBrowser2(StudyData.study.Name);
                    string browserList = AuthorizedBrowser.GenerateBrowserList(authorizedBrowser);
                    authBrowserArea.InnerHtml = browserList;

                    txtEmail.Enabled = false;
                    btnSubmit.Enabled = false;

                    logger.Info(String.Format(CultureInfo.InvariantCulture, "Use of {0} is not supported. Browser Name: {1}, Major version:{2}, userAgent: {3}", browserNameVersion, browser.Browser, browser.MajorVersion, Request.UserAgent));
                    return;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
            }

            btnSubmit.Click += new EventHandler(btnSubmit_Click);
        }

        void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate();

                if (!Page.IsValid)
                {
                    //Validation on the page is already visible so disabling the validation summary
                    vsErrors.Visible = false;
                    return;
                }
                vsErrors.Visible = true;

                WebDiaryProvider provider = System.Web.Security.Membership.Provider as WebDiaryProvider;

                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    StudyRepository sr = new StudyRepository(db);
                    Study study = StudyData.study;

                    WebDiaryUser user = (WebDiaryUser)provider.GetUser(txtEmail.Text.Trim(), false);

                    //Check if user exists and if so, check to see if they're assigned to this specific study.
                    if (user == null || study == null || user.StudyId != study.Id)
                    {
                        Page.Validators.Add(new ValidationError(Resources.Resource.ForgotPasswordNoEmail));
                        return;
                    }

                    string language = WDAPIObject.JavaLocaleToDotNetCulture(user.Language);
                    UICulture = Culture = language;
                    Session[WDConstants.Language] = Response.Cookies[WDConstants.Language].Value = language;
                    if (user.IsApproved)
                    {

                        study = sr.FindById(user.StudyId);
                        WDAPIObject wd = new WDAPIObject(study.StudyWorksUsername, study.StudyWorksPassword, study.Name, study.StudyWorksBaseUrl);
                        SubjectStudyEnabled result = wd.IsSubjectAndStudyEnabled(user.KrPT, study);
                        if (result == SubjectStudyEnabled.Enabled)
                        {
                            SubjectRepository subjectRep = new SubjectRepository(db);
                            Subject sub = subjectRep.FindByKrptAndStudy(user.KrPT, user.StudyId);
                            Session[DateTimeHelper.TimeTravelOffset] = sub.TimeTravelOffset;
                            HttpContext.Current.Session[DateTimeHelper.TimeZoneId] = sub.TimeZone;

                            Guid token = provider.CreateForgotPasswordRequest((Guid)user.ProviderUserKey);

                            if (token != Guid.Empty)
                            {

                                String resetUrl = String.Format(CultureInfo.InvariantCulture, "{0}/reset-password.aspx?token={1}",
                                                                StudyData.study.WebDiaryStudyBaseUrl.TrimEnd('/'),
                                                                token);

                                string body = EmailHelper.GetForgotPasswordEmail(resetUrl, txtEmail.Text.Trim());
                                Message message = new Message(ConfigurationManager.AppSettings["FROM_ADDRESS"], user.Email, Resources.Resource.EmailAcctForgotPswrdSubject.ToString(), body.ToString());
                                try
                                {
                                    message.Send();
                                    logger.Info(String.Format(CultureInfo.InvariantCulture, "SUCCESS: subject GUID: {0}, subject krpt: {1}, StudyWorks timezone: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, logging date time in UTC: {6}. Email was sent to the email server successfully.", sub.Id.ToString(), sub.KrPT, sub.TimeZone, StudyData.study.Name, StudyData.study.Id, "reset password request due to forgot password", DateTime.UtcNow));
                                }
                                catch (SmtpException ex)
                                {
                                    logger.Error(String.Format(CultureInfo.InvariantCulture, "FAIL: subject GUID: {0}, subject krpt: {1}, StudyWorks timezone: {2}, study name: {3}, study ID in NPDB: {4}, email info: {5}, logging date time in UTC: {6}, error message: {7}. Sending email failed in StudyPortal.", sub.Id.ToString(), sub.KrPT, sub.TimeZone, StudyData.study.Name, StudyData.study.Id, "reset password request due to forgot password", DateTime.UtcNow, ex.Message), ex);
                                    Page.Validators.Add(new ValidationError(Resources.Resource.ForgotPasswordSendingEmailFailed));
                                    return;
                                }

                                pnlForm.Visible = false;

                                pnlSuccess.Visible = true;
                            }
                            else
                            {
                                Page.Validators.Add(new ValidationError(Resources.Resource.ErrorProcessingUrRequest));
                            }
                        }
                        else if (result == SubjectStudyEnabled.Error)
                        {
                            Page.Validators.Add(new ValidationError(String.Format(CultureInfo.InvariantCulture, Resources.Resource.AppErrHasOccurred, "<a href=\"https://mystudy.phtstudy.com/ssa/pages/contact_us/\">", "</a>")));
                        }
                        else
                        {
                            Page.Validators.Add(new ValidationError(Resources.Resource.ForgotPswrdAccInactive));
                        }
                    }
                    else
                    {
                        Page.Validators.Add(new ValidationError(Resources.Resource.AcctRetrievePswdforNotActivated));
                    }
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message, ex);
                Page.Validators.Add(new ValidationError(Resources.Resource.ErrUnexpectedErr));
            }
        }        
    }
}
