﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;

namespace WebDiary.StudyPortal
{
    public class Ping : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("1");
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}