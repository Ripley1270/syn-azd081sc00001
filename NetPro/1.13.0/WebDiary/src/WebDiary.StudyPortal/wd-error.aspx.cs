﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebDiary.Controls;

namespace WebDiary.StudyPortal
{
    public partial class WDError : BasePage
    {
        public const string Path = "~/wd-error.aspx";
        public const string ErrorMessageKey = "error_message";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["nocookie"] != null)
                {
                    Session[ErrorMessageKey] = Resources.Resource.BrowserCookiesDisabled;
                    Response.Redirect("~/wd-error.aspx", true);
                }
                errorMessageLbl.Text = Session[ErrorMessageKey].ToString();
            }
        }
    }
}