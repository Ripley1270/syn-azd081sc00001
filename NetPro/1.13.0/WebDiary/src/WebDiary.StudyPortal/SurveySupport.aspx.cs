﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

using WebDiary.Core.Helpers;
using WebDiary.SWAPI;
using WebDiary.SWAPI.SWData;
using WebDiary.Membership.Provider;
using WebDiary.Membership.User;
using WebDiary.Core.Entities;
using WebDiary.Core.Repositories;
using WebDiary.Core.Constants;
using WebDiary.Controls;

namespace WebDiary.StudyPortal
{
    public partial class SurveySupport : BasePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            char[] traillers = { '/' };
            int survey_id = 0;
            string returnUrl = "";
            string appUrl = "";
            string loginPage = Session["isMobile"] != null? "/mobile/timeout-redirect.aspx" : "/login.aspx";

            returnUrl = StudyData.study.WebDiaryStudyBaseUrl.TrimEnd(traillers) + loginPage;
            appUrl = StudyData.study.WebDiaryStudyBaseUrl.TrimEnd(traillers); 

            if (string.IsNullOrEmpty(Request.QueryString["survey_id"]) || !int.TryParse(Request.QueryString["survey_id"], out survey_id))
            {
                Response.Write("top.location.href = '" + returnUrl + "';");
            }

            if (Session[Request.QueryString[WDConstants.VarPsdId]] == null)
            {
                using (WebDiaryContext db = new WebDiaryContext(WebDiaryContext.ConnectionString))
                {
                    SurveyRepository svr = new SurveyRepository(db);
                    StudySurveys survey = svr.FindById(survey_id);

                    //determine if the request is for patient access or site access
                    if (survey.FormType.Equals("Subject Submit")) //Patient gateway
                    {
                        if (Session["login"] == null)
                        {
                            Response.Write("top.location.href = '" + returnUrl + "';");
                        }
                    }
                    else //site gateway
                    {
                        returnUrl = Request.QueryString["ssa_base_url"] + SWAPIConstants.SessionTimeoutPage;

                        if (!string.IsNullOrEmpty(Session[WDConstants.VarRequestUrl] as string))
                        {
                            if (!SessionHelper.CheckSwSession(Session[WDConstants.VarRequestUrl] as string))
                            {
                                Response.Write("top.location.href = '" + returnUrl + "';");
                            }
                        }
                        else
                        {
                            Response.Write("top.location.href = '" + returnUrl + "';");
                        }
                    }
                }
            }

            if (SessionHelper.IsSiteSession())
                returnUrl = appUrl + "/logout.aspx?ssa_url=" + Server.UrlEncode(Request.QueryString["ssa_base_url"] + SWAPIConstants.SessionTimeoutPage);

            Response.Write(" secCheck = true; window.onbeforeunload = function () { if (isReloaded) return '" + Resources.Resource.RefreshMessage + "'; };");

            //TODO Add to PDE Template
            //[PDE] Add logic to account for ?

            //Add support to load custom javascript objects and function
            //See /includes/script/PDELoader.js for unminify source
            Response.Write("function PDELoader(a,b,c){$(document).ready(function(){var d=function(k){var l,m=window,n=k.split('.');for(l=0;l<n.length;l++)if(m=m[n[l]],'undefined'==typeof m)return!1;return!0},e=function(){var k;for(k=0;k<a.length;k++)if(!d(a[k]))return c&&$(h).addClass('PDELoader'),void setTimeout(e,100);if('function'==typeof b)try{b(),PDECommonUtils.enableNextButton(),c&&$(h).removeClass('PDELoader')}catch(m){console.error('PDELoader - Failed to execute function: '+m)}else console.error('PDELoader - Invalid function: '+b)},f=document.getElementsByTagName('script'),g=f[f.length-1],h=g.parentNode;$('#ResponseView_LayoutTemplate__nextZone__nextBtn').attr('disabled','disabled'),Array.isArray(a)||(a=[a]),a.push('PDECommonUtils'),e()})}");

            Response.Write("function loadjscssfile(filename){ var questionMarkIndex = filename.lastIndexOf('?'), filetype = (questionMarkIndex == -1)? filename.substr(filename.lastIndexOf('.') + 1).toLowerCase(): filename.substring(filename.lastIndexOf('.') + 1, filename.lastIndexOf('?')).toLowerCase(); if(filetype==\"js\"){var fileref=document.createElement('script'); fileref.setAttribute(\"type\",\"text/javascript\"); fileref.setAttribute(\"src\", '" + appUrl + "/includes/scripts/' + filename);} else if (filetype==\"css\"){var fileref=document.createElement(\"link\"); fileref.setAttribute(\"rel\",\"stylesheet\"); fileref.setAttribute(\"type\",\"text/css\"); fileref.setAttribute(\"href\",'" + appUrl + "/includes/css/' + filename);} if(typeof fileref!=\"undefined\")document.getElementsByTagName(\"head\")[0].appendChild(fileref);}");

            Response.Write("loadjscssfile(\"app.js\", \"js\"); var sTimeout = " + Session.Timeout * 60000 + "; ");

            //TODO Add to PDE Template
            //[PDE] Add custom JS and CS files
            String customJS_CSFiles = PDECommonTools.getOnLoadJS_CSSFile(Session);

            if (customJS_CSFiles != null)
            {
                Response.Write(customJS_CSFiles);
            }

            Response.Write("var lastCheck = new Date().getTime(); function sleepCheck () { var now = new Date().getTime(); var diff = now - lastCheck; if (diff > sTimeout) {isReloaded = false; top.location.href = '" + returnUrl + "'; } }; setInterval(function(){sleepCheck();}, 2000);");

            Response.Write("$(function() { $('body').attr('dir','" + TextDirection + "'); $('div[align=\"left\"]').attr('align',''); });");

            Response.Write(" notOnlineMessage = '" + Resources.Resource.NotOnlineMessage + "';");

            //TODO Add to PDE Template
            //[PDE] Add inline JS
            String customInlineJavascript = PDECommonTools.getOnLoadInlineJavascript(Session);

            if (customInlineJavascript != null)
            {
                Response.Write(customInlineJavascript);
            }

            Response.End();
        }

        [System.Web.Services.WebMethod]
        public static string EQBackupSwap()
        {
            if (HttpContext.Current.Session[WDConstants.EQBackup] != null && HttpContext.Current.Session[WDConstants.EQBackup].ToString() == "0")
            {
                HttpContext.Current.Session[WDConstants.EQBackup] = "1";
                return "0";
            }
            else
            {
                HttpContext.Current.Session[WDConstants.EQBackup] = "0";
                return "1";
            }
        }
    }
}
