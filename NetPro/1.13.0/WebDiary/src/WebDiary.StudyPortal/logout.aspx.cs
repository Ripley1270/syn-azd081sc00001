﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using WebDiary.Controls;
using WebDiary.Core.Constants;

namespace WebDiary.StudyPortal
{
    public partial class LogOff : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            // session timeout out on site survey page
            if (Request.QueryString["ssa_url"] != null)
            {
                Session[WDConstants.VarRequestUrl] = null;
                Response.Redirect(Server.UrlDecode(Request.QueryString["ssa_url"]));
            }
            else
            {
                //Delete cookie so that the login screen doesn't show session timeout message.
                if (Request.Cookies[WDConstants.UserStatusCookie] != null)
                {
                    HttpCookie userStatusCookie = new HttpCookie(WDConstants.UserStatusCookie);
                    userStatusCookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(userStatusCookie);
                }

                Response.Redirect("login.aspx");
            }
        }
    }
}
