﻿<%@ Page Title="" Language="C#" MasterPageFile="~/includes/masters/Site.Master" AutoEventWireup="true" CodeBehind="wd_error.aspx.cs" Inherits="WebDiary.StudyPortal.WDError" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:Panel ID="Panel1" CssClass="pht_error_outer_div" runat="server">
        <asp:Label ID="Label1" CssClass="pht_error_heading phtBold" runat="server" Text="<%$ Resources:Resource, ErrUnexpectedErr%>"></asp:Label>
        <asp:Panel ID="Panel2" CssClass="pht_error_inner_div" runat="server">
            <asp:Label ID="errorMessageLbl" CssClass="pht_error_message" runat="server"></asp:Label>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
