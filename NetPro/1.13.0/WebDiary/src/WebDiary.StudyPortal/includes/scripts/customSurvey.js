﻿
/*
*   Handlerbar Templates
*/
Templates = {};

Templates.question = [
    "<span id={{questionID}} class='Question'>{{{questionText}}}</span><br/>"
].join("\n");

Templates.radioAnswer = [
    "<input id={{className}}a{{answerValue}} class='{{className}}' type='radio' style='display:inline;' name={{answerName}} value={{answerValue}}>",
    "<label for={{className}}a{{answerValue}}>{{answerText}}</label>",
    "</br>"
].join("\n");

Templates.checkboxAnswer = [
    "<input id={{className}}a{{answerValue}} class='{{className}}' type='checkbox' style='display:inline;' name={{answerName}} value={{answerValue}}>",
    "<label for={{className}}a{{answerValue}}>{{answerText}}</label>",
    "</br>"
].join("\n");

Templates.textBox = [
    "<input id={{id}} class='{{className}}' type='text' name={{answerName}} maxlength={{maxLength}} ><br/>"
].join("\n");

Templates.dropDown = [
    "<br/>",
    "<select id={{id}} class={{className}} name={{answerName}}>",
    "<option value='-1'> </option>",
    "</select>"
].join("\n");


function dateDiffInDays(date1, date2, useAbsolute) {
    var date1Copy = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate(), 0, 0, 0, 0),
        date2Copy = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate(), 0, 0, 0, 0),
        date1Offset = (date1Copy.getTimezoneOffset() * 60 * 1000),
        date2Offset = (date2Copy.getTimezoneOffset() * 60 * 1000),
        msDiff;

    if (!useAbsolute) {
        msDiff = date1Copy - date2Copy + (date2Offset - date1Offset);
    } else {
        msDiff = Math.abs(date1Copy - date2Copy + (date2Offset - date1Offset));
    }

    return Math.floor(msDiff / 1000 / 60 / 60 / 24);
}

function generateRandomizationDatePicker() {
    var maxDate = new Date(PDECommonUtils.urlDecode(getParam('maxDate'))),
        minDate = new Date(PDECommonUtils.urlDecode(getParam('minDate'))),
        StartDateName = randDate.name,
        StartDateID = randDate.id,
        tempVal = randDate.value;

    //Move textArea outside of the matrix
    $(randDate).remove();
    $('#dateContainer').closest('table').find('div[id^="custom-control-"]').append('<input name= "' + StartDateName + '" type="text" id= "' + StartDateID + '" class="randDate" />');
    $('#' + StartDateID).datepicker({ minDate: minDate, maxDate: maxDate, defaultDate: maxDate, dateFormat: 'dd M yy', showOtherMonths: true });

    $('#' + StartDateID).attr('readOnly', 'true');

    //copy current value into new date picker
    $('#' + StartDateID).val(tempVal);


    PDECommonUtils.AddNextButtionValidation(randomizationDateEditCheck);
}

function randomizationDateEditCheck() {
    var validated = true,
        dateVal = $('.randDate').val(),
        reportDate = new Date(PDECommonUtils.urlDecode(getParam('maxDate'))),
        diffInDays = 0;

    if (dateVal.length == 0) {

        //Set Error message to screen
        if ($("#dobError").length <= 0) {
            $('.randDate').after('<div id="dobError" class="Error" style="text-decoration:none;">' + myCustomText.errorText + '</div>');
        } else {
            $('#dobError').show();
        }

        validated = false;

    } else {
        $('#dobError').hide();

        //we have a date, calculate days since randomzation for branching logic
        diffInDays = dateDiffInDays(reportDate, new Date(dateVal)) + 1;

        if (diffInDays >= 27) {
            randInPast.value = '1';
        }
        else {
            randInPast.value = '0';
        }
    }


    return validated;
};


function generateVisitDropDownQuestion() {
    var className = "A1",
        nameQ1 = visitList.name,
        nameID = visitList.id,
        tempVal = visitList.value || $('#' + className).val(),

        dropdownTemplate = Handlebars.compile(Templates.dropDown),

        answers = [
            { key: 'reScreening', value: 10},
            { key: 'startTreatment', value: 20},
            { key: 'treatmentDisc', value: 30},
            { key: 'progTreatmentDisc', value: 40},
            { key: 'progression', value: 50},
            { key: 'studyDiscontinuation', value: 60}
        ],
        answerHTML = "",
        currentPhase = $('#phase').val();

    $(visitList).remove();

    switch(currentPhase) {
        case '100':
        case '200':
        case '210':
        case '220':
        case '230':
        case '240':
        case '250':
        case '260':
        case '270':
        case '280':
            answers = _.filter(answers, function (answer) {
                return answer.value == 10 || answer.value == 20 || answer.value == 60;
            });
            break;
        case '290':
            answers = _.filter(answers, function (answer) {
                return answer.value == 20 || answer.value == 60;
            });
            break;
        case '300':
            answers = _.filter(answers, function (answer) {
                return answer.value == 30 || answer.value == 40 || answer.value == 60;
            });
            break;
        case '400':
            answers = _.filter(answers, function (answer) {
                return answer.value == 50 ||  answer.value == 60;
            });
            break;
        case '500':
            answers = _.filter(answers, function (answer) {
                return answer.value == 60;
            });
            break;
    }

    answerHTML = dropdownTemplate({ id: className, className: className, answerName: nameQ1 });
    $('#visitContainer').html(answerHTML);

    for (var i = 0; i < answers.length; i++) {
        $('#' + className).append($('<option/>', {
            value: answers[i].value,
            text: myCustomText[answers[i].key]
        }));
    }

    //Set default value
    try {
        if (tempVal != '' && tempVal != -1) {
            $('#' + className).val(tempVal);
        }
    }
    catch (err) {

    }

}

