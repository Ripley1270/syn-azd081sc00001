var ingredientText = {
    orderIngredients: "Order of ingredients",
    activeIngredients: "Active Ingredients",
    singleDoseStength: "Single-Dose Strength",
    order1: "1",
    order2: "2",
    order3: "3",
    order4: "4",
    missingFirstIngredient: "Please enter at least 1 active ingredient",
    missingIngredient: "Active ingredient is required for entered single-dose strength.",
    missingDose: "Single-Dose Strength is required for entered active ingredient(s)."
};

var MAX_INGREDIENTS = 4,
    WORK_INGREDIENT_TABLE_ID = "workIngredientTable",
    //Characters that are considered as invalid by SWAPI
    INVALID_CHARS = ['%', '&'];

function activateWorkIngredientTable(){
    var workIngredientColumns = getWorkIngredientColumns(),
        workIngredientData = getWorkIngredientData(),
		workIngredientTable = $('#' + WORK_INGREDIENT_TABLE_ID),
        tempValue;
        
    //WorkIngredientResponse        

	tempValue =  $('#' + WorkIngredientResponse.id).val();

	$(WorkIngredientResponse).remove();

	//Create table
    workIngredientTable.bootstrapTable({
        columns: workIngredientColumns,
        data: workIngredientData
    });

	$('#' + WorkIngredientResponse.id).val(tempValue);

	initializeWorkIngredientResponses(tempValue);

	workIngredientTable.append('<input id="' + WorkIngredientResponse.id + '" name="' +
                                WorkIngredientResponse.name + '" type="hidden"/>');

    saveWorkIngredientResponses();

    initializeWorkIngredientEventHandlers(workIngredientTable);

	PDECommonUtils.AddNextButtionValidation(function () {
        var table = workIngredientTable,
            tableContainer = $('.bootstrap-table'),
            isValid = true,
            INGWRK1C,
            DOSWRK1N,
            checkDose = false,
            checkRow = false,
            row,
            missingFirstIngredient,
            missingIngredient,
            missingDoseStrength,
            errorMessage,
            maxRowPopulated = 0,
            i,
            precision = function (num) {
                var splitNum = num.split('.');

                if (splitNum.length > 1) {
                    return splitNum[1].length;
                }
                else {
                    return 0;
                }
            };

        //Remove all highlighted input
        $('input').removeClass('pdeerror');

        $('div.pdeerror').remove();
        
        for(i = MAX_INGREDIENTS - 1; i >=0; i--){
            row = table.find('tr[data-index="' + i + '"]');

            INGWRK1C = row.find('input#INGWRK1C' + i).val();

            DOSWRK1N = row.find('input#DOSWRK1N' + i).val();

            if ((INGWRK1C !== '' || DOSWRK1N !== '') && maxRowPopulated === 0){
                maxRowPopulated = i;
            }

            //if value in row check it, or if first row always check since we need at least 1 ingredient
            if (INGWRK1C === '' || DOSWRK1N === '') {
                //If the 1st row is empty
                if (INGWRK1C === '' && DOSWRK1N === '' && i === 0){
                    missingFirstIngredient = true;
                }

                if(i <= maxRowPopulated){
                    if(INGWRK1C === ''){
                        missingIngredient = true;
                        isValid = false;
                        row.find('input#INGWRK1C' + i).addClass('pdeerror');
                    }
                    
                    if(DOSWRK1N === ''){
                        missingDoseStrength = true;
                        isValid = false;
                        row.find('input#DOSWRK1N' + i).addClass('pdeerror');
                    }
                }
            }

            if(DOSWRK1N !== ''){
                if (isNaN(DOSWRK1N) ||
                    Number(DOSWRK1N) > 9999999999 ||
                    Number(DOSWRK1N) < 0 ||
                    precision(DOSWRK1N) > 4) 
                {
                    missingDoseStrength = true;
                    isValid = false;
                    row.find('input#DOSWRK1N' + i).addClass('pdeerror');
                }    
            }
        }

	    if (isValid) {
	        saveWorkIngredientResponses();
	    }else {
            if(missingFirstIngredient){
                errorMessage = ingredientText.missingFirstIngredient;
            }
            else if(missingIngredient){
                errorMessage = ingredientText.missingIngredient;
            }
            else {
                errorMessage = ingredientText.missingDose;
            }

            tableContainer.append('<div class="pdeerror"><br>' + errorMessage + '</div>');
        }

	    return isValid;
	}); 

	PDECommonUtils.AddBackButtionValidation(function(){
		saveWorkIngredientResponses();
	});
}

function getWorkIngredientColumns(){
    var result = [
        {   
            field: 'order',
            title: ingredientText.orderIngredients
        },
		{   
            field: 'INGWRK1C',
            title: ingredientText.activeIngredients
        },
		{   
            field: 'DOSWRK1N',
            title: ingredientText.singleDoseStength
        }
    ];

    return result;
}

function getWorkIngredientData() {
    var result = [],
        row,
        i;

    //Initialize each row
    for(i = 0; i < MAX_INGREDIENTS; i++) {
        row = {};
        row.id = i;
        //Add order number
        row.order = ingredientText['order' +  (i + 1)];

        row.INGWRK1C = getINGWRK1CInputHtml('INGWRK1C' + i);

        row.DOSWRK1N = getDOSWRK1NInputHtml('DOSWRK1N' + i);

        result.push(row);
    }

    return result;
}

function getINGWRK1CInputHtml(id, index) {
    var result = '<input disabled id="' + id + '" type="text" class="ingredientInput" maxlength="40">';

    return result;
}

function getDOSWRK1NInputHtml(id) {
    var result = '<input disabled id="' + id + '" type="number" class="ingredientInput" min="0" max="9999999999" step="0.0001">';

    return result;
}

function saveWorkIngredientResponses() {
    var table = $('#' + WORK_INGREDIENT_TABLE_ID),
        isTablePopulated = false,
        responses = [],
        INGWRK1C,
        DOSWRK1N,
        i,
        row,
        response,        
		JSONResponses;

    for(i = MAX_INGREDIENTS - 1; i >= 0; i--){
        row = table.find('tr[data-index="' + i + '"]');

        response = {};

        INGWRK1C = row.find('input#INGWRK1C' + i);

        if(INGWRK1C.length > 0){            
            response.INGWRK1C = INGWRK1C.val();

            //Replace invalid characters with empty string
            $.each(INVALID_CHARS, function(index, char){
                response.INGWRK1C.replace(new RegExp(char, 'g'), '');
            });
        }

        DOSWRK1N = row.find('input#DOSWRK1N' + i);

        if(DOSWRK1N.length > 0){
            response.DOSWRK1N = DOSWRK1N.val(); 
        }
        
        if((response.DOSWRK1N !== '' && response.INGWRK1C !== '') || isTablePopulated){       
            responses.unshift(response);

            if(!isTablePopulated){
                isTablePopulated = true;
            }
        }
    }

    //Save json 
    JSONResponses = JSON.stringify(responses);
    $('#' + WorkIngredientResponse.id).val(JSONResponses);
}

function initializeWorkIngredientResponses(tempValue){
    var currentResponses = tempValue,
        table = $('#' + WORK_INGREDIENT_TABLE_ID),
        firstRow = table.find('tr[data-index="' + 0 + '"]');

    if (currentResponses !== '' && currentResponses !== "" && currentResponses !== null) {
        currentResponses = JSON.parse(tempValue);
    }

    //Enable inputs for the 1st row
    firstRow.find('input').prop('disabled', false);

    $.each(currentResponses, function(index, response){
        var row = table.find('tr[data-index="' + index + '"]'),
            nextRow = table.find('tr[data-index="' + (index + 1) + '"]');
       
		if(typeof response !== 'undefined'){
			if(typeof response.INGWRK1C !== 'undefined'){
                row.find('input#INGWRK1C' + index).val(response.INGWRK1C);
                
                if(nextRow.length > 0){
                    nextRow.find('input').prop('disabled', false);    
                }
			}

			if(typeof response.DOSWRK1N !== 'undefined'){
                row.find('input#DOSWRK1N' + index).val(response.DOSWRK1N);
                
                if(nextRow.length > 0){
                    nextRow.find('input').prop('disabled', false);    
                }
			}
		}
	});
}

function initializeWorkIngredientEventHandlers(workIngredientTable) {
    var onTableChange = function(){
        var $this = $(this),
            i,
            row,
            maxRowPopulated = -1,
            INGWRK1C,
            DOSWRK1N;

        //Identify the last populated rows
        for(i = MAX_INGREDIENTS - 1; i >=0; i--){
            row = workIngredientTable.find('tr[data-index="' + i + '"]');// This is bad, don't know why

            INGWRK1C = row.find('input#INGWRK1C' + i).val();

            DOSWRK1N = row.find('input#DOSWRK1N' + i).val();

            if(INGWRK1C !== '' || DOSWRK1N !== ''){
                maxRowPopulated = i;
                break;
            }
        }

        //Disable/Enable rows
        for(i = 0; i < MAX_INGREDIENTS; i++){
            row = workIngredientTable.find('tr[data-index="' + i + '"]');

            //if rows should be enable
            if(i <= maxRowPopulated + 1){
                //For each input in the row
                $.each(row.find('.ingredientInput'), function(index, input){
                    //If input is set to disable
                    if($(input).attr('disabled')){
                        //enable input
                        $(input).removeAttr('disabled');
                    }
                });
            }
            else {
                //For each input in the row
                $.each(row.find('.ingredientInput'), function(index, input){
                    //If input is set to enable
                    if(!$(input).attr('disabled')){
                        //disable input
                        $(input).attr('disabled', 'disabled');
                    }
                });
            }
        }
    };

    onTableChange();

    workIngredientTable.find('input.ingredientInput').on('input', onTableChange);

    //Filter out invalid characters
    $('.ingredientInput').keypress(function(event) {
        var isValidChar = true;
        
        $.each(INVALID_CHARS, function(index, char){
            if(event.which === char.charCodeAt(0)){
                isValidChar = false;
            }
            return isValidChar;
        });

        return isValidChar;
    });
}