﻿function generateUpdateMedListControl() {
    /*var subjMedList = parseSubjectMedList();
    var subjAliasList = parseSubjectAliasList();
    var numMeds = subjMedList.length;
    var medCodeLength = getParam('medCodeLength');

    var nameQ1 = updateMedList.name;
    var nameID = updateMedList.id;
    var tempVal = updateMedList.value;
    $(updateMedList).remove();

    var aliasQ2 = updateAliasList.name;
    var aliasID = updateAliasList.id;
    var aliasTempVal = updateAliasList.value;
    $(updateAliasList).remove();*/

    var orgSubjectMasterMedList = MedModuleUtils.getSubjectMasterMedList(),
        subjectMedListName = SubjectMedListResponse.name,
        subjectMedListID = SubjectMedListResponse.id,
        subjectMedListValue = SubjectMedListResponse.value;

    $(SubjectMedListResponse).remove();

    $('div[id^="custom-control-"]').first().append('<div id="customContainer" class="customContainer"><div id="medListContainer">');

    if (tempVal == '') {
        for (var i = 0; i < numMeds; i++) {
            $('#medListContainer').append('<div id="s' + subjMedList[i].medNum + '"><input class="subjAlias" maxlength="25" type="text" value="' + subjAliasList[i] + '"/><input class="medClass" id="' + subjMedList[i].medNum + '" type="radio" name="medlist" value="' + subjMedList[i].medIndicationDisplayName + '">' + subjMedList[i].medIndicationDisplayName + '<br/></div>');
        }
    } else {
        var medSplit = tempVal.split("|");
        var aliasSplit = aliasTempVal.split("|");
        for (var j = 0; j < medSplit.length - 1; j++) {
            var medCodeStr = $.trim(medSplit[j].substring(0, medCodeLength));
            $('#medListContainer').append('<div id="s' + medCodeStr + '"><input class="subjAlias" maxlength="25" type="text" value="' + aliasSplit[j] + '"/><input class="medClass" id="' + medCodeStr + '" type="radio" name="medlist" value="' + medSplit[j] + '">' + medSplit[j] + '<br/></div>');
        }
    }

    $('#medListContainer').append('</div></div>');
    
    $('#customContainer').append('<br/><br/><table><tr><td><input id="deleteBtn" type="button" value="Delete"><input style="margin-left: 10px;" id="addBtn" type="button" value="Add Item"><input id="newMedTxtbox" type="textbox"/></td></tr></table><input id="valueHolder" name="' + nameQ1 + '" type="hidden"/><input id="aliasHolder" name="' + aliasQ2 + '" type="hidden"/><div class="Error" id="errorMessage"></div>');
    
    var allMedString = "";
    var allAliasString = "";

    $(".medClass").each(function () {
        var $this = $(this);
        //Build up a string with all med values
        allMedString = allMedString + $this.val() + "|";
    });

    $(".subjAlias").each(function () {
        var $this = $(this);
        //Build up a string with all med values
        allAliasString = allAliasString + $this.val() + "|";
    });

    tempVal = allMedString;
    aliasTempVal = allAliasString;

    $('#valueHolder').val(tempVal);
    $('#aliasHolder').val(aliasTempVal);

    //Bind delete button handler
    $("#deleteBtn").click(function () {
        var allMedString = "";
        var allAliasString = "";

        $(".medClass").each(function () {
            var $this = $(this);
            if ($this.is(":checked")) {
                $('#s' + $this.attr("id")).remove();
            } else {
                //Build up a string with all values
                allMedString = allMedString + $this.val() + "|";
            }
        });

        $(".subjAlias").each(function () {
            var $this = $(this);
            //Build up a string with all med values
            allAliasString = allAliasString + $this.val() + "|";
        });

        tempVal = allMedString;
        aliasTempVal = allAliasString;
        $('#valueHolder').val(tempVal);
        $('#aliasHolder').val(aliasTempVal);
    });


    //Bind Add button handler
    $("#addBtn").click(function () {
        var validCode = validateMedCode();
        if (validCode) {
            var medCode = $('#newMedTxtbox').val();
            var indicationCode = $('#indicationDrop').val();
            var retVal = lookUpMedCodeObjectAlias(medCode, "");

            if (retVal.medNum == medCode) {
                //check if med is already in the list

                if (isCurrentMed(medCode)) {
                    $('#errorMessage').text(StudyText.text3);
                }
                else {
                    if (retVal.medDisabled == "1") {
                        $('#errorMessage').text(StudyText.text5);
                    }
                    else if (retVal.medExcluded == "0") {
                        $('#errorMessage').text(StudyText.text6);
                    }
                    else {
                        var newMed = $('<span id="s' + medCode + indicationCode + '"><input class="subjAlias" type="text" maxlength="25" /><input class="medClass" id="' + medCode + indicationCode + '" type="radio"    name="medlist" value="' + retVal.medIndicationDisplayName + '"/>' + retVal.medIndicationDisplayName + '<input class="subjIndication" type="hidden" value="' + indicationCode + '"><br></span>');

                        $('#medListContainer').append(newMed);
                        $('#errorMessage').text(""); //clear error text
                    }
                }
            }
            else {
                $('#errorMessage').text(StudyText.text2);
            }
            var allMedString = "";
            var allAliasString = "";
            var allIndicationString = "";

            $(".medClass").each(function () {
                var $this = $(this);
                //Build up a string with all values
                allMedString = allMedString + $this.val() + "|";
            });

            $(".subjAlias").each(function () {
                var $this = $(this);
                //Build up a string with all med values
                allAliasString = allAliasString + $this.val() + "|";
            });

            tempVal = allMedString;
            aliasTempVal = allAliasString;
            $('#valueHolder').val(tempVal);
            $('#aliasHolder').val(aliasTempVal);
        }
    });

    //Checks if the medication code is valid
    function validateMedCode() {
        var retVal = false;
        var medCode = $('#newMedTxtbox').val();
        var medCodeLength = getParam('medCodeLength');
        var maxNumMeds = $('#maxNumMeds').val();
        var numMeds = $('input:radio').length;

        if (numMeds < maxNumMeds) {
            if (medCode.length == medCodeLength) {
                retVal = true;
            }
            else {
                $('#errorMessage').text(StudyText.text2);
            }
        }
        else {
            $('#errorMessage').text(StudyText.text1);
        }

        return retVal;
    }

    //Determines if medication code is already in the list
    function isCurrentMed(newMedCode) {
        var retVal = false,
            currentMedList = $('#valueHolder').val(),
            medCodeLength = getParam('medCodeLength'),
            medSplit = tempVal.split("|"),
            tmpMed,
            tmpIndication;

        for (var j = 0; j < medSplit.length - 1; j++) {
            tmpMed = $.trim(medSplit[j].substring(0, medCodeLength));

            //Considered same med if medcode and indication are both the same
            if (tmpMed == newMedCode) {
                retVal = true;
            }
        }

        return retVal;
    }
}

//Find Medication in master medication javascript array
function lookUpMedCodeObjectAlias(code, alias) {
    var retVal = "";

    //TODO change masterArray into object with each medcode code as key

    if (code in masterArray) {
        retVal = {
            medNum: masterArray[code][0],
            medName: masterArray[code][1],
            medSubjName: masterArray[code][2],
            medDose: masterArray[code][3],
            medAdmin: masterArray[code][4],
            medRoute: masterArray[code][5],
            medDisabled: masterArray[code][6],
            medExcluded: masterArray[code][7],
            medMEDFLG2B: masterArray[code][8],
            medAlias: alias,
            medDisplayName: masterArray[code][0] + " - " + masterArray[code][1] + "; " + masterArray[code][3] + "; " + decodeMasterMedForm(masterArray[code][4]) + "; " + decodeMasterMedUnit(masterArray[code][5]),
            medAlisDisplayName: masterArray[code][0] + " - " + masterArray[code][1] + "; " + alias + "; " + masterArray[code][3] + "; " + decodeMasterMedForm(masterArray[code][4]) + "; " + decodeMasterMedUnit(masterArray[code][5]),
            medIndicationDisplayName: masterArray[code][0] + " - " + masterArray[code][1] + "; " + masterArray[code][3] + "; " + decodeMasterMedForm(masterArray[code][4]) + "; " + decodeMasterMedUnit(masterArray[code][5])
        };
    }

    return retVal;
}

function parseSubjectMedList() {
    var delimitedList = $('#delimitedMedList').val();
    var currentNumMeds = $('#numMeds').val();
    var splitList = delimitedList.split('|');

    var subjMedList = [];

    //Build Medication meta arrays
    for (var i = 1; i <= currentNumMeds * 2; i += 2) {
        var med = lookUpMedCodeObjectAlias(splitList[i], splitList[i + 1]);
        subjMedList.push(med);
    }

    return subjMedList;
}

function parseSubjectAliasList() {
    var delimitedList = $('#delimitedMedList').val();
    var currentNumMeds = $('#numMeds').val();
    var splitList = delimitedList.split('|');

    var subjAliasList = [];

    //Build Alias meta arrays
    for (var i = 2; i <= currentNumMeds * 2; i += 2) {
        //subjAliasList.push(decodeURIComponent(splitList[i]));
        subjAliasList.push(splitList[i]);
    }

    return subjAliasList;
}

//[schan] Get Subject Master Med list that contain the combination of master med and subject med for each subject med
function getSubjectMasterMedList() {
    var delimitedList = $('#delimitedMedList').val();
    var currentNumMeds = $('#numMeds').val();
    var splitList = delimitedList.split('|');

    var subjAliasList = [];

    //Build Alias meta arrays
    for (var i = 2; i <= currentNumMeds * 2; i += 2) {
        //subjAliasList.push(decodeURIComponent(splitList[i]));
        subjAliasList.push(splitList[i]);
    }

    return subjAliasList;
}


//[schan] Get Subject Med list that contain Med Code and Alias for each Subject Med
function getSubjectMedList() {

    return orgSubjectMeds;
}

//[schan] Get Subject Med list that contain Med Code and Alias for each Subject Med
function getSubjectMedList() {

    return orgSubjectMeds;
}