//TODO Update text to resource strings

var MED_MODULE_CONFIG = MED_MODULE_CONFIG || {
	//This needs to be updated for each study. The MUST match the responses in OtherResolveMed survey in Page 8
    MED_FORM: 	[
                    {
                        VALUE: "1",
                        TEXT: "pill",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "2",
                        TEXT: "powder",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "3",
                        TEXT: "liquid",
                        MAX_AMT: 99
                    },
                    {
                        VALUE: "4",
                        TEXT: "inhaler",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "5",
                        TEXT: "film",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "6",
                        TEXT: "spray",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "7",
                        TEXT: "ointment",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "8",
                        TEXT: "patch",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "9",
                        TEXT: "suppository",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "10",
                        TEXT: "injection",
                        MAX_AMT: 99
                    },
                    {
                        VALUE: "11",
                        TEXT: "drop",
                        MAX_AMT: 9
                    }
               ],
    MED_FORMS: 	[
                    {
                        VALUE: "1",
                        TEXT: "pill(s)",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "2",
                        TEXT: "packet(s)",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "3",
                        TEXT: "mLs",
                        MAX_AMT: 99
                    },
                    {
                        VALUE: "4",
                        TEXT: "5 mLs - teaspoon(s)",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "5",
                        TEXT: "15 mLs - tablespoon(s)",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "6",
                        TEXT: "drop(s)",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "7",
                        TEXT: "film(s)",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "8",
                        TEXT: "spray(s)",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "9",
                        TEXT: "ug",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "10",
                        TEXT: "mg",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "11",
                        TEXT: "g",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "12",
                        TEXT: "fingertip(s)",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "13",
                        TEXT: "patch(s)",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "14",
                        TEXT: "suppository(s)",
                        MAX_AMT: 9
                    },
                    {
                        VALUE: "15",
                        TEXT: "puff(s)",
                        MAX_AMT: 9
                    }
               ],
	//This needs to be updated for each study. The MUST match the responses in OtherResolveMed survey in Page 8
    MED_ROUTE: [
                    {
                        VALUE: "1",
                        TEXT: "Oral"
                    },
                    {
                        VALUE: "2",
                        TEXT: "Nasal"
                    },
                    {
                        VALUE: "3",
                        TEXT: "Optic"
                    },
                    {
                        VALUE: "4",
                        TEXT: "Aural"
                    },
                    {
                        VALUE: "5",
                        TEXT: "Transdermal"
                    },
                    {
                        VALUE: "6",
                        TEXT: "Rectal"
                    },
                    {
                        VALUE: "7",
                        TEXT: "Injectable (IM)"
                    },
                    {
                        VALUE: "8",
                        TEXT: "Injectable (IV)"
                    },
                    {
                        VALUE: "9",
                        TEXT: "Injectable (SC)"
                    },
                    {
                        VALUE: "10",
                        TEXT: "Injectable"
                    }
				],
        DOSE_UNIT: [
                    {
                        VALUE: "1",
                        TEXT: "ug"
                    },
                    {
                        VALUE: "2",
                        TEXT: "mg"
                    },
                    {
                        VALUE: "3",
                        TEXT: "g"
                    },
                    {
                        VALUE: "4",
                        TEXT: "ug/mL"
                    },
                    {
                        VALUE: "5",
                        TEXT: "mg/mL"
                    },
                    {
                        VALUE: "6",
                        TEXT: "g/mL"
                    },
                    {
                        VALUE: "7",
                        TEXT: "mg/5 mL"
                    },
                    {
                        VALUE: "8",
                        TEXT: "g/5 mL"
                    },
                    {
                        VALUE: "9",
                        TEXT: "g/5 mL"
                    },
                    {
                        VALUE: "10",
                        TEXT: "ug/15 mL"
                    },
                    {
                        VALUE: "11",
                        TEXT: "mg/15 mL"
                    },
                    {
                        VALUE: "12",
                        TEXT: "g/15 mL"
                    },
                    {
                        VALUE: "13",
                        TEXT: "IU"
                    }
				]
};