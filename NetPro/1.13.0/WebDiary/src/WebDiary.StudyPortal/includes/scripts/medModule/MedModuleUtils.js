﻿
var MedModuleUtils = MedModuleUtils || {};

//Get Subject Med list that contain Med Code and Alias for each Subject Med
MedModuleUtils.getSubjectMedList = function () {
    var subjectMeds = $.extend(true, [], SUBJECT_MEDS); //Clone subject meds

    return subjectMeds;
};

//Get Master Meds
MedModuleUtils.getMasterMeds = function () {
    var masterMeds = $.extend(true, {}, MASTER_MEDS); //Clone master meds

    return masterMeds;
};

//Get Work Meds
MedModuleUtils.getWorkMeds = function () {
    var workMeds = $.extend(true, {}, WORK_MEDS); //Clone master meds

    return workMeds;
};

MedModuleUtils.getSubjectMedByMedCode = function (medCode) {
    var subjectMeds = MedModuleUtils.getSubjectMedList(),
        tempSubjectMed,
        subjectMed;

    if (subjectMeds) {
        for (var i = 0; i < subjectMeds.length; i++) {
            tempSubjectMed = subjectMeds[i];

            if (tempSubjectMed.MEDCOD1C == medCode) {
                subjectMed = tempSubjectMed;
                break;
            }
        }
    }

    return subjectMed;
};

MedModuleUtils.getMasterMedByMedCode = function (medCode) {
    var masterMeds = MedModuleUtils.getMasterMeds(),
        masterMed;

    if (masterMeds) {
        masterMed = masterMeds[medCode];
    }

    return masterMed;
};

MedModuleUtils.getWorkMedByWorkCode = function (workCode) {
    var workMeds = MedModuleUtils.getWorkMeds(),
        workMed;

    if (workMeds) {
        workMed = workMeds[workCode];
    }

    return workMed;
};

//Get Subject Master Med list that contain the combination of master med and subject med for each subject med
MedModuleUtils.getSubjectMasterMedList = function (subjectMeds) {
    var subjectMasterMedList = [];

    if (typeof subjectMeds === 'undefined') {
        subjectMeds = MedModuleUtils.getSubjectMedList();
    }

    $.each(subjectMeds, function (index, subjectMed) {
        var masterMed = MedModuleUtils.getMasterMedByMedCode(subjectMed.MEDCOD1C),
        //Create Subject master med by merging subject med and master med
            subjectMasterMed = $.extend(true, subjectMed, masterMed);

        subjectMasterMedList.push(subjectMasterMed);
    });

    return subjectMasterMedList;
};

MedModuleUtils.getWorkMedDisplayName = function (med) {
    var result = '';

    if (med) {
        result += med['WRKCOD1C'];
        result += ' - ';
        result += med['WRKNAM2C'];
        result += '; ';
        result += MedModuleUtils.getDisplayWorkIngredientName(med);
        result += '; ';
        result += MedModuleUtils.getDisplayWorkDoseName(med);
        result += ' ';
        result += MedModuleUtils.decodeMasterMedRoute(med['MEDRTE1L']);
        result += '; ';
        result += MedModuleUtils.decodeMasterMedForm(med['MEDFRM1L']);
    }

    return result;
};

MedModuleUtils.getDisplayMedNameDoseUnit = function (med) {
    var result = '',
        i;

    if (med) {
        for (i = 0; i < med.ings.length; i++) {
            if (i !== 0) {
                result += ' / ';
            }

            result += med.ings[i].INGNAM1C;
        }

        result += '; ';

        for (i = 0; i < med.ings.length; i++) {
            if (i !== 0) {
                result += ' / ';
            }
            result += med.ings[i].INGDOS1N;            
        }

        result += ' ';
        if(typeof med.ings[0].INGUNT1L != 'undefined'){
            result += MedModuleUtils.decodeMasterDoseUnit(med.ings[0].INGUNT1L);
        }   
        
    }

    return result;
};

MedModuleUtils.getDisplayWorkIngredientName = function (med) {
    var result = '',
        i;

    if (med) {
        for (i = 0; i < med.ings.length; i++) {
            if (i !== 0) {
                result += ' / ';
            }

            result += med.ings[i].INGWRK1C;
        }
    }

    return result;
};

MedModuleUtils.getDisplayWorkDoseName = function (med) {
    var result = '',
        i;

    if (med) {
        for (i = 0; i < med.ings.length; i++) {
            if (i !== 0) {
                result += ' / ';
            }

            result += med.ings[i].DOSWRK1N;
        }
    }

    return result;
};

MedModuleUtils.getMasterMedDisplayName = function (med) {
    var result = '';

    if (med) {
        result += med['MEDCOD1C'];
        result += ' - ';
        result += MedModuleUtils.getDisplayMedNameDoseUnit(med);
        result += '; ';
        result += MedModuleUtils.decodeMasterMedRoute(med['MEDRTE1L']);
        result += '; ';
        result += MedModuleUtils.decodeMasterMedForm(med['MEDFRM1L']);
    }

    return result;
};

MedModuleUtils.getSubjectMasterMedDisplayName = function (med) {
    var result = '';

    if (med) {
        result += med['MEDCOD1C'];
        result += ' - ';
        result += MedModuleUtils.getDisplayMedNameAliasDoseUnit(med);
        result += '; ';
        result += MedModuleUtils.decodeMasterMedRoute(med['MEDRTE1L']);
        result += '; ';
        result += MedModuleUtils.decodeMasterMedForm(med['MEDFRM1L']);
    }

    return result;
};

MedModuleUtils.getDisplayMedNameDoseUnit = function (med) {
    var result = '',
        i;

    if (med) {
        for (i = 0; i < med.ings.length; i++) {
            if (i !== 0) {
                result += ' / ';
            }

            result += med.ings[i].INGNAM1C;
        }

        result += '; ';

        for (i = 0; i < med.ings.length; i++) {
            if (i !== 0) {
                result += ' / ';
            }
            result += med.ings[i].INGDOS1N;            
        }

        result += ' ';
        if(typeof med.ings[0].INGUNT1L != 'undefined'){
            result += MedModuleUtils.decodeMasterDoseUnit(med.ings[0].INGUNT1L);
        }  
    }

    return result;
};

MedModuleUtils.getDisplayMedNameAliasDoseUnit = function (med) {
    var result = '',
        i;

    if (med) {
        for (i = 0; i < med.ings.length; i++) {
            if (i !== 0) {
                result += ' / ';
            }

            result += med.ings[i].INGNAM1C;
        }

        result += '; [';
        result += med['SBJALS1C'];
        result += ']; ';

        for (i = 0; i < med.ings.length; i++) {
            if (i !== 0) {
                result += ' / ';
            }
            result += med.ings[i].INGDOS1N;            
        }

        result += ' ';
        if(typeof med.ings[0].INGUNT1L != 'undefined'){
            result += MedModuleUtils.decodeMasterDoseUnit(med.ings[0].INGUNT1L);
        }   
    }

    return result;
};

MedModuleUtils.getMedRouteList = function () {
    return $.extend(true, [], MED_MODULE_CONFIG.MED_ROUTE);
};

//Med Route(MEDRTE1L)
MedModuleUtils.decodeMasterMedRoute = function (code) {
    var retVal = "",
        medRouteList = MedModuleUtils.getMedRouteList(),
        medRoute;

    for (var i = 0; i < medRouteList.length; i++) {
        medRoute = medRouteList[i];

        if (medRoute.VALUE == code) {
            retVal = medRoute.TEXT;
            break;
        }
    }

    return retVal;
};

MedModuleUtils.getMedFormList = function () {
    return $.extend(true, [], MED_MODULE_CONFIG.MED_FORM);
};

//Med Form(MEDFRM1L)
MedModuleUtils.decodeMasterMedForm = function (code) {
    var retVal = "",
        medFormList = MedModuleUtils.getMedFormList(),
        medForm;

    for (var i = 0; i < medFormList.length; i++) {
        medForm = medFormList[i];

        if (medForm.VALUE == code) {
            retVal = medForm.TEXT;
            break;
        }
    }

    return retVal;
};

MedModuleUtils.getMedFormsList = function () {
    return $.extend(true, [], MED_MODULE_CONFIG.MED_FORMS);
};

MedModuleUtils.decodeMasterMedForms = function (code) {
    var retVal = "",
        medFormsList = MedModuleUtils.getMedFormsList(),
        medForms;

    for (var i = 0; i < medFormsList.length; i++) {
        medForms = medFormsList[i];

        if (medForms.VALUE == code) {
            retVal = medForms.TEXT;
            break;
        }
    }

    return retVal;
};

MedModuleUtils.encodeMasterMedForms = function (label) {
    var retVal = "",
        medFormsList = MedModuleUtils.getMedFormsList(),
        medForms;

    for (var i = 0; i < medFormsList.length; i++) {
        medForms = medFormsList[i];

        if (medForms.TEXT == label) {
            retVal = medForms.VALUE;
            break;
        }
    }

    return retVal;
};

MedModuleUtils.getMedFormsMaxAmt = function (code) {
    var retVal = "",
        medFormList = MedModuleUtils.getMedFormsList(),
        medForm;

    for (var i = 0; i < medFormList.length; i++) {
        medForm = medFormList[i];

        if (medForm.VALUE == code) {
            retVal = medForm.MAX_AMT;
            break;
        }
    }

    return retVal;
};

MedModuleUtils.encodeMasterMedForm = function (label) {
    var retVal = "",
        medFormList = MedModuleUtils.getMedFormList(),
        medForm;

    for (var i = 0; i < medFormList.length; i++) {
        medForm = medFormList[i];

        if (medForm.TEXT == label) {
            retVal = medForm.VALUE;
            break;
        }
    }

    return retVal;
};

MedModuleUtils.getDoseUnitList = function () {
    return $.extend(true, [], MED_MODULE_CONFIG.DOSE_UNIT);
};

MedModuleUtils.decodeMasterDoseUnit = function (code) {
    var retVal = "",
        doseUnitList = MedModuleUtils.getDoseUnitList(),
        doseUnit;

    for (var i = 0; i < doseUnitList.length; i++) {
        doseUnit = doseUnitList[i];

        if (doseUnit.VALUE == code) {
            retVal = doseUnit.TEXT;
            break;
        }
    }

    return retVal;
};

MedModuleUtils.getMedCodeLength = function () {
    return getParam('medCodeLength');
};

MedModuleUtils.isCorrectMedCodeLength = function (medCode) {
    var expectedMedCodeLength = MedModuleUtils.getMedCodeLength(),
        actualMedCodeLength = medCode.length;

    return (expectedMedCodeLength == actualMedCodeLength);
};

MedModuleUtils.decodeHtml = function (html) {
    return $('<div>').html(html).text();
};

MedModuleUtils.displayOtherMedDateTime = function() {
    var otherMedDateTime = getParam('otherMedDateTime'),
        otherMedDateTimeSpan = $('span#otherMedDateTime');

    if(otherMedDateTimeSpan.length > 0 && otherMedDateTime){
        //Double decode URL
        otherMedDateTimeSpan.text(decodeURIComponent(decodeURIComponent(otherMedDateTime)));
    }        
};

//On load
$(document).ready(function () {
    MedModuleUtils.displayOtherMedDateTime();
});