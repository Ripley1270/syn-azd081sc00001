﻿namespace Checkbox.Analytics.Filters.Configuration
{
    using Checkbox.Analytics.Filters;
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class ProfileFilterData : ResponseFilterData
    {
        protected override Checkbox.Analytics.Filters.Filter CreateFilterObject()
        {
            return new ProfileFilter();
        }

        protected override DBCommandWrapper GetCreateCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Filter_InsertProfile");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("ProfileField", DbType.String, base.PropertyName);
            return storedProcCommandWrapper;
        }

        protected override DBCommandWrapper GetDeleteCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Filter_DeleteProfile");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, base.ID.Value);
            return storedProcCommandWrapper;
        }

        protected override DBCommandWrapper GetLoadConfigurationCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Filter_GetProfile");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, base.ID);
            return storedProcCommandWrapper;
        }

        public override string DataTableName
        {
            get
            {
                return "ProfileFilterData";
            }
        }

        public override string IdentityColumnName
        {
            get
            {
                return "FilterID";
            }
        }

        protected override string PropertyFieldName
        {
            get
            {
                return "ProfileField";
            }
        }
    }
}

