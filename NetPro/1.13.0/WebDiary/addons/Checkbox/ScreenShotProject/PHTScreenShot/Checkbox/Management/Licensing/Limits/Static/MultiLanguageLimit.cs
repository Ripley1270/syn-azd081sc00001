﻿namespace Checkbox.Management.Licensing.Limits.Static
{
    using Checkbox.Management.Licensing.Limits;
    using System;

    public class MultiLanguageLimit : StaticLicenseLimit
    {
        public override string LimitName
        {
            get
            {
                return "AllowMultiLanguage";
            }
        }
    }
}

