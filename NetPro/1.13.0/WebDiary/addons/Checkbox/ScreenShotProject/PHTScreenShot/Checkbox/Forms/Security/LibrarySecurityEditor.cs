﻿namespace Checkbox.Forms.Security
{
    using Checkbox.Forms;
    using Checkbox.Security;
    using System;

    public class LibrarySecurityEditor : AccessControllablePDOSecurityEditor
    {
        public LibrarySecurityEditor(LibraryTemplate library) : base(library)
        {
        }

        protected override string RequiredEditorPermission
        {
            get
            {
                return "Library.Edit";
            }
        }
    }
}

