﻿namespace Checkbox.Forms.PageLayout.Configuration
{
    using Checkbox.Common;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;

    [Serializable]
    public abstract class PageLayoutTemplateData : AbstractPersistedDomainObject
    {
        private Dictionary<string, LayoutTemplateExtendedProperty> _extendedProperties;
        private DataSet _layoutData;

        protected PageLayoutTemplateData()
        {
            this.InitializeData();
        }

        protected void AddExtendedProperty(LayoutTemplateExtendedProperty property)
        {
            this._extendedProperties[property.Name] = property;
        }

        public void AddItemToLayout(int itemID, string zoneName)
        {
            if (Utilities.IsNotNullOrEmpty(zoneName) && (itemID > 0))
            {
                DataRow[] rowArray = this.ItemZoneMapTable.Select("ItemID = " + itemID, null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    foreach (DataRow row in rowArray)
                    {
                        row["ZoneName"] = zoneName;
                    }
                }
                else
                {
                    DataRow row2 = this.ItemZoneMapTable.NewRow();
                    row2["ItemID"] = itemID;
                    row2["ZoneName"] = zoneName;
                    if (base.ID.HasValue)
                    {
                        row2["LayoutTemplateID"] = base.ID.Value;
                    }
                    this.ItemZoneMapTable.Rows.Add(row2);
                }
            }
        }

        protected void ClearExtendedProperties()
        {
            this._extendedProperties.Clear();
        }

        protected override void Create(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_LayoutTemplate_Insert");
            storedProcCommandWrapper.AddInParameter("LayoutTemplateTypeName", DbType.String, this.TypeName);
            storedProcCommandWrapper.AddOutParameter("LayoutTemplateID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("LayoutTemplateID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("No ID was returned from ckbx_LayoutTemplate_Insert");
            }
            base.ID = new int?((int) parameterValue);
        }

        public abstract object CreateTemplate(string languageCode);
        public override void Delete(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_LayoutTemplate_Delete");
            storedProcCommandWrapper.AddInParameter("LayoutTemplateID", DbType.Int32, base.ID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public virtual ReadOnlyCollection<string> GetExtendedPropertyNames()
        {
            List<string> list = new List<string>();
            foreach (LayoutTemplateExtendedProperty property in this._extendedProperties.Values)
            {
                list.Add(property.Name);
            }
            return new ReadOnlyCollection<string>(list);
        }

        public Dictionary<int, string> GetItemZoneMappings()
        {
            DataRow[] rowArray;
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            if (base.ID.HasValue)
            {
                rowArray = this.ItemZoneMapTable.Select("LayoutTemplateID = " + base.ID, null, DataViewRowState.CurrentRows);
            }
            else
            {
                rowArray = this.ItemZoneMapTable.Select("LayoutTemplateID IS NULL", null, DataViewRowState.CurrentRows);
            }
            foreach (DataRow row in rowArray)
            {
                dictionary[(int) row["ItemID"]] = (string) row["ZoneName"];
            }
            return dictionary;
        }

        private DBCommandWrapper GetMappingDeleteCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_LayoutTemplate_DeleteItemZone");
            storedProcCommandWrapper.AddInParameter("LayoutTemplateID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        private DBCommandWrapper GetMappingInsertCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_LayoutTemplate_UpSertItemZone");
            storedProcCommandWrapper.AddInParameter("LayoutTemplateID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ZoneName", DbType.String, "ZoneName", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        private DBCommandWrapper GetMappingUpdateCommand(Database db)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_LayoutTemplate_UpSertItemZone");
            storedProcCommandWrapper.AddInParameter("LayoutTemplateID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, "ItemID", DataRowVersion.Current);
            storedProcCommandWrapper.AddInParameter("ZoneName", DbType.String, "ZoneName", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        public virtual double? GetPropertyMaxValue(string propertyName)
        {
            if (this._extendedProperties.ContainsKey(propertyName))
            {
                return this._extendedProperties[propertyName].MaxValue;
            }
            return 0.0;
        }

        public virtual double? GetPropertyMinValue(string propertyName)
        {
            if (this._extendedProperties.ContainsKey(propertyName))
            {
                return this._extendedProperties[propertyName].MinValue;
            }
            return 0.0;
        }

        public virtual LayoutTemplateExtendedPropertyType GetPropertyType(string propertyName)
        {
            if (this._extendedProperties.ContainsKey(propertyName))
            {
                return this._extendedProperties[propertyName].Type;
            }
            return LayoutTemplateExtendedPropertyType.String;
        }

        public virtual object GetPropertyValue(string propertyName)
        {
            if (!this._extendedProperties.ContainsKey(propertyName))
            {
                return null;
            }
            LayoutTemplateExtendedProperty property = this._extendedProperties[propertyName];
            if ((property.Value != null) && (property.Value.ToString().ToLower() == "true"))
            {
                return "Yes";
            }
            if ((property.Value != null) && (property.Value.ToString().ToLower() == "false"))
            {
                return "No";
            }
            return property.Value;
        }

        public virtual ReadOnlyCollection<string> GetPropertyValueList(string propertyName)
        {
            if (this._extendedProperties.ContainsKey(propertyName) && (this._extendedProperties[propertyName].PossibleValues != null))
            {
                return new ReadOnlyCollection<string>(this._extendedProperties[propertyName].PossibleValues);
            }
            return new ReadOnlyCollection<string>(new string[0]);
        }

        private void InitializeData()
        {
            this._layoutData = new DataSet();
            DataTable table = new DataTable {
                TableName = ItemZoneMapTableName
            };
            table.Columns.Add("LayoutTemplateID", typeof(int));
            table.Columns.Add("ItemID", typeof(int));
            table.Columns.Add("ZoneName", typeof(string));
            this._layoutData.Tables.Add(table);
            this._extendedProperties = new Dictionary<string, LayoutTemplateExtendedProperty>();
        }

        protected override void Load(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("Unable to load AbstractLayoutTemplate; DataRow cannot be null.");
            }
            if (this.DataTableName == data.Table.TableName)
            {
                this.LoadFromDataRow(data);
            }
        }

        public override void Load(DataSet data)
        {
            base.Load(data);
            this.ItemZoneMapTable.Clear();
            if (data.Tables.Contains(ItemZoneMapTableName))
            {
                foreach (DataRow row in data.Tables[ItemZoneMapTableName].Select("LayoutTemplateID = " + base.ID, null, DataViewRowState.CurrentRows))
                {
                    this.ItemZoneMapTable.ImportRow(row);
                }
            }
        }

        public void RemoveItemFromLayout(int itemID)
        {
            foreach (DataRow row in this.ItemZoneMapTable.Select("ItemID = " + itemID, null, DataViewRowState.CurrentRows))
            {
                row.Delete();
            }
        }

        protected void SaveMappings(IDbTransaction t)
        {
            Database db = DatabaseFactory.CreateDatabase();
            db.UpdateDataSet(this._layoutData, ItemZoneMapTableName, this.GetMappingInsertCommand(db), this.GetMappingUpdateCommand(db), this.GetMappingDeleteCommand(db), t);
        }

        internal void SetIdentity(int id)
        {
            base.ID = new int?(id);
        }

        public virtual void SetPropertyValue(string propertyName, object value)
        {
            if (this._extendedProperties.ContainsKey(propertyName))
            {
                this._extendedProperties[propertyName].Value = value;
            }
        }

        protected override void Update(IDbTransaction t)
        {
        }

        public virtual bool AllowManualLayout
        {
            get
            {
                return true;
            }
        }

        protected override DBCommandWrapper ConfigurationDataSetCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_LayoutTemplate_Get");
                storedProcCommandWrapper.AddInParameter("LayoutTemplateID", DbType.Int32, base.ID.Value);
                return storedProcCommandWrapper;
            }
        }

        protected ReadOnlyCollection<LayoutTemplateExtendedProperty> ExtendedProperties
        {
            get
            {
                List<LayoutTemplateExtendedProperty> list = new List<LayoutTemplateExtendedProperty>();
                foreach (LayoutTemplateExtendedProperty property in this._extendedProperties.Values)
                {
                    list.Add(property);
                }
                return new ReadOnlyCollection<LayoutTemplateExtendedProperty>(list);
            }
        }

        private DataTable ItemZoneMapTable
        {
            get
            {
                return this._layoutData.Tables[ItemZoneMapTableName];
            }
        }

        protected static string ItemZoneMapTableName
        {
            get
            {
                return "ItemZoneMap";
            }
        }

        public override string ParentDataTableName
        {
            get
            {
                return "LayoutTemplates";
            }
        }

        public abstract string TypeName { get; }
    }
}

