﻿namespace Checkbox.Forms.PageLayout.Configuration
{
    using Prezza.Framework.Common;
    using System;
    using System.Reflection;

    internal static class PageLayoutConfigurationFactory
    {
        private static PageLayoutTemplateData CreateObject(Type type)
        {
            ArgumentValidation.CheckForNullReference(type, "type");
            ValidateTypeIsPageLayoutTemplateData(type);
            ConstructorInfo constructor = type.GetConstructor(new Type[0]);
            if (constructor == null)
            {
                throw new Exception("ItemData does not have a constructor: " + type.FullName);
            }
            object obj2 = null;
            try
            {
                obj2 = constructor.Invoke(null);
            }
            catch (MethodAccessException exception)
            {
                throw new Exception(exception.Message, exception);
            }
            catch (TargetInvocationException exception2)
            {
                throw new Exception(exception2.Message, exception2);
            }
            catch (TargetParameterCountException exception3)
            {
                throw new Exception(exception3.Message, exception3);
            }
            return (PageLayoutTemplateData) obj2;
        }

        public static PageLayoutTemplateData CreatePageLayoutTemplateData(string typeName)
        {
            return CreateObject(GetType(typeName));
        }

        private static Type GetType(string typeName)
        {
            Type type;
            ArgumentValidation.CheckForEmptyString(typeName, "typeName");
            try
            {
                type = Type.GetType(typeName, true, false);
            }
            catch (TypeLoadException exception)
            {
                throw new Exception("A type-loading error occurred.  Type was: " + typeName, exception);
            }
            return type;
        }

        private static void ValidateTypeIsPageLayoutTemplateData(Type type)
        {
            ArgumentValidation.CheckForNullReference(type, "type");
            if (!typeof(PageLayoutTemplateData).IsAssignableFrom(type))
            {
                throw new Exception("Type mismatch between PageLayoutTemplateData type [" + typeof(PageLayoutTemplateData).AssemblyQualifiedName + "] and requested type [" + type.AssemblyQualifiedName);
            }
        }
    }
}

