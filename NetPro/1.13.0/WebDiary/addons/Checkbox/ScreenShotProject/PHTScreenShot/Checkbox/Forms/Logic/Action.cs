﻿namespace Checkbox.Forms.Logic
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class Action
    {
        private readonly object _receiver;

        protected Action() : this(null)
        {
        }

        protected Action(object receiver)
        {
            this._receiver = receiver;
        }

        public abstract void Execute(bool directive);

        internal int? Identity { get; set; }

        protected object Receiver
        {
            get
            {
                return this._receiver;
            }
        }
    }
}

