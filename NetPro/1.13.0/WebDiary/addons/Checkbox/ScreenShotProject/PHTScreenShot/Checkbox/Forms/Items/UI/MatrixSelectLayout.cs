﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public abstract class MatrixSelectLayout : SelectLayout
    {
        protected MatrixSelectLayout()
        {
            base.ItemPosition = "center";
        }

        public override int? Columns
        {
            get
            {
                return 1;
            }
            set
            {
            }
        }

        public override Layout LayoutDirection
        {
            get
            {
                return Layout.Horizontal;
            }
            set
            {
            }
        }

        public override bool ShowNumberLabels
        {
            get
            {
                return false;
            }
            set
            {
            }
        }
    }
}

