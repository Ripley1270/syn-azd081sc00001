﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Forms.Items;
    using System;
    using System.Collections.Generic;

    public class TextAnswerValidator : Validator<TextItem>
    {
        private string _errorMessage;

        private List<Validator<string>> GetFormatValidatorList(TextItem input)
        {
            List<Validator<string>> list = new List<Validator<string>>();
            switch (input.Format)
            {
                case AnswerFormat.Email:
                    list.Add(new EmailValidator());
                    return list;

                case AnswerFormat.Integer:
                    list.Add(new IntegerValidator());
                    return list;

                case AnswerFormat.Numeric:
                    list.Add(new NumericValidator());
                    return list;

                case AnswerFormat.Decimal:
                    list.Add(new DecimalValidator());
                    return list;

                case AnswerFormat.Money:
                    list.Add(new USCurrencyValidator());
                    return list;

                case AnswerFormat.Phone:
                    list.Add(new USPhoneValidator());
                    return list;

                case AnswerFormat.SSN:
                    list.Add(new SocialSecurityValidator());
                    return list;

                case AnswerFormat.URL:
                    list.Add(new UrlValidator());
                    return list;

                case AnswerFormat.Postal:
                    list.Add(new USZipCodeValidator());
                    list.Add(new CanadaZipCodeValidator());
                    return list;

                case AnswerFormat.Alpha:
                    list.Add(new AlphaValidator());
                    return list;

                case AnswerFormat.Lowercase:
                    list.Add(new LowerCaseAlphaValidator());
                    return list;

                case AnswerFormat.Uppercase:
                    list.Add(new UpperCaseAlphaValidator());
                    return list;

                case AnswerFormat.Date:
                    list.Add(new DateValidator());
                    return list;

                case AnswerFormat.Date_USA:
                    list.Add(new USADateValidator());
                    return list;

                case AnswerFormat.Date_ROTW:
                    list.Add(new ROTWDateValidator());
                    return list;

                case AnswerFormat.AlphaNumeric:
                    list.Add(new AlphaNumericValidator());
                    return list;

                case AnswerFormat.Custom:
                    list.Add(new CustomFormatValidator(input.CustomFormatId));
                    return list;
            }
            return list;
        }

        public override string GetMessage(string languageCode)
        {
            return this._errorMessage;
        }

        public override bool Validate(TextItem input)
        {
            if (!this.ValidateRequired(input))
            {
                return false;
            }
            if (input.HasAnswer)
            {
                List<Validator<string>> formatValidatorList = this.GetFormatValidatorList(input);
                bool flag = false;
                string answer = input.GetAnswer();
                foreach (Validator<string> validator in formatValidatorList)
                {
                    if (validator.Validate(answer))
                    {
                        flag = true;
                        break;
                    }
                    this.ErrorMessage = validator.GetMessage(input.LanguageCode);
                }
                if ((formatValidatorList.Count > 0) && !flag)
                {
                    return false;
                }
                if (!this.ValidateLength(input))
                {
                    return false;
                }
            }
            return true;
        }

        private bool ValidateLength(TextItem item)
        {
            if (((((item.Format == AnswerFormat.None) || (item.Format == AnswerFormat.Alpha)) || ((item.Format == AnswerFormat.AlphaNumeric) || (item.Format == AnswerFormat.Email))) || ((item.Format == AnswerFormat.Uppercase) || (item.Format == AnswerFormat.Lowercase))) || (item.Format == AnswerFormat.URL))
            {
                AnswerLengthValidator validator = new AnswerLengthValidator();
                if (!validator.Validate(item))
                {
                    this.ErrorMessage = validator.GetMessage(item.LanguageCode);
                    return false;
                }
            }
            return true;
        }

        private bool ValidateRequired(TextItem item)
        {
            if (item.Required)
            {
                RequiredItemValidator validator = new RequiredItemValidator();
                if (!validator.Validate(item))
                {
                    this.ErrorMessage = validator.GetMessage(item.LanguageCode);
                    return false;
                }
            }
            return true;
        }

        protected string ErrorMessage
        {
            get
            {
                return this._errorMessage;
            }
            set
            {
                this._errorMessage = value;
            }
        }
    }
}

