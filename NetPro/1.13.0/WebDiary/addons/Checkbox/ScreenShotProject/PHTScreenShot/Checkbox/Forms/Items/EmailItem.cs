﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Globalization.Text;
    using Checkbox.Messaging.Email;
    using Checkbox.Styles;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Specialized;
    using System.Data;
    using System.Net.Mime;
    using System.Text;

    [Serializable]
    public class EmailItem : ResponseItem
    {
        private string _bcc;
        private string _body;
        private string _from;
        private string _messageFormat;
        private int _myPage;
        private bool _sendOnlyOnce;
        private int? _styleTemplateID;
        private string _subject;
        private string _to;
        private const string MESSAGE_BODY_TOKEN = "[[MESSAGE_BODY]]";

        protected virtual bool CheckEmailSent()
        {
            if (base.Response != null)
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_GetEmailSent");
                storedProcCommandWrapper.AddInParameter("ResponseID", DbType.Int32, base.Response.ID);
                storedProcCommandWrapper.AddInParameter("EmailItemID", DbType.Int32, base.ID);
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        return reader.Read();
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            return false;
        }

        public override void Configure(ItemData configuration, string languageCode)
        {
            ArgumentValidation.CheckExpectedType(configuration, typeof(EmailItemData));
            EmailItemData data = (EmailItemData) configuration;
            this._from = data.From;
            this._messageFormat = data.MessageFormat;
            this._styleTemplateID = data.StyleTemplateID;
            this._to = data.To;
            this._bcc = data.BCC;
            this._subject = TextManager.GetText(data.SubjectTextID, languageCode);
            this._body = TextManager.GetText(data.BodyTextID, languageCode);
            this._sendOnlyOnce = data.SendOnce;
            this._myPage = -1;
            base.Configure(data, languageCode);
        }

        protected virtual string GetBodyText()
        {
            if (base.Response != null)
            {
                return this.GetPipedText("Body", this._body);
            }
            return this.Body;
        }

        protected override NameValueCollection GetInstanceDataValuesForXmlSerialization()
        {
            NameValueCollection instanceDataValuesForXmlSerialization = base.GetInstanceDataValuesForXmlSerialization();
            instanceDataValuesForXmlSerialization["From"] = this.From;
            instanceDataValuesForXmlSerialization["To"] = this.To;
            instanceDataValuesForXmlSerialization["Bcc"] = this.BCC;
            instanceDataValuesForXmlSerialization["Subject"] = this.Subject;
            instanceDataValuesForXmlSerialization["Body"] = this.Body;
            return instanceDataValuesForXmlSerialization;
        }

        protected virtual string GetMessageBody()
        {
            return this.GetMessageTemplate().Replace("[[MESSAGE_BODY]]", this.GetBodyText());
        }

        protected virtual string GetMessageTemplate()
        {
            if (!this.MessageFormat.Equals("Html", StringComparison.InvariantCultureIgnoreCase) && !this.MessageFormat.Equals("SurveyHtml", StringComparison.InvariantCultureIgnoreCase))
            {
                return "[[MESSAGE_BODY]]";
            }
            StyleTemplate styleTemplate = null;
            if (this.StyleTemplateID.HasValue)
            {
                styleTemplate = StyleTemplateManager.GetStyleTemplate(this.StyleTemplateID.Value);
            }
            StringBuilder builder = new StringBuilder();
            builder.Append("<html>");
            builder.Append("<head>");
            if (styleTemplate != null)
            {
                builder.Append("<style type=\"text/css\">");
                builder.Append(styleTemplate.GetCss());
                builder.Append("</style>");
            }
            builder.Append("</head>");
            builder.Append("<body>");
            if (styleTemplate != null)
            {
                builder.Append(TextManager.GetText(styleTemplate.HeaderTextID, base.LanguageCode));
            }
            builder.Append("[[MESSAGE_BODY]]");
            if (styleTemplate != null)
            {
                builder.Append(TextManager.GetText(styleTemplate.FooterTextID, base.LanguageCode));
            }
            builder.Append("</body>");
            builder.Append("</html>");
            return builder.ToString();
        }

        protected override NameValueCollection GetMetaDataValuesForXmlSerialization()
        {
            NameValueCollection metaDataValuesForXmlSerialization = base.GetMetaDataValuesForXmlSerialization();
            metaDataValuesForXmlSerialization["MessageFormat"] = this.MessageFormat;
            metaDataValuesForXmlSerialization["StyleTemplateID"] = this.StyleTemplateID.HasValue ? this.StyleTemplateID.ToString() : null;
            metaDataValuesForXmlSerialization["SendOnce"] = this._sendOnlyOnce.ToString();
            return metaDataValuesForXmlSerialization;
        }

        protected virtual void MarkEmailSent()
        {
            if (this._sendOnlyOnce && (base.Response != null))
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Response_MarkEmailSent");
                storedProcCommandWrapper.AddInParameter("ResponseID", DbType.Int32, base.Response.ID);
                storedProcCommandWrapper.AddInParameter("EmailItemID", DbType.Int32, base.ID);
                storedProcCommandWrapper.AddInParameter("EmailDate", DbType.DateTime, DateTime.Now);
                database.ExecuteNonQuery(storedProcCommandWrapper);
            }
        }

        protected override void OnPageLoad()
        {
            base.OnPageLoad();
            try
            {
                if (base.Response != null)
                {
                    this._myPage = base.Response.CurrentPage.Position;
                    base.Response.PageChanged += new Response.ResponsePageChangedHandler(this.Response_PageChanged);
                }
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "UIProcess");
            }
        }

        protected void Response_PageChanged(object sender, ResponsePageChangedEventArgs e)
        {
            if ((e.NewPage == this._myPage) && (e.NewPage != e.PreviousPage))
            {
                try
                {
                    this.SendEmail();
                    if (this._sendOnlyOnce)
                    {
                        base.Response.PageChanged -= new Response.ResponsePageChangedHandler(this.Response_PageChanged);
                    }
                }
                catch (Exception exception)
                {
                    ExceptionPolicy.HandleException(exception, "BusinessProtected");
                }
            }
        }

        protected void SendEmail()
        {
            if (this.OkToSend)
            {
                EmailMessage message = new EmailMessage {
                    From = this.From,
                    To = this.To,
                    Subject = this.Subject,
                    Body = this.GetMessageBody()
                };
                if ((this.MessageFormat == null) || (this.MessageFormat.ToLower() != "html"))
                {
                    message.Format = MailFormat.Text;
                }
                foreach (string str in message.To.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    message.To = str.Trim();
                    if ((!string.IsNullOrEmpty(message.To) && message.To.Contains("@")) && message.To.Contains("."))
                    {
                        try
                        {
                            EmailGateway.Send(message);
                        }
                        catch (Exception exception)
                        {
                            ExceptionPolicy.HandleException(exception, "BusinessPublic");
                        }
                    }
                }
                if (this.BCC != null)
                {
                    foreach (string str2 in this.BCC.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        message.To = str2.Trim();
                        if ((!string.IsNullOrEmpty(message.To) && message.To.Contains("@")) && message.To.Contains("."))
                        {
                            try
                            {
                                EmailGateway.Send(message);
                            }
                            catch (Exception exception2)
                            {
                                ExceptionPolicy.HandleException(exception2, "BusinessPublic");
                            }
                        }
                    }
                }
                this.MarkEmailSent();
            }
        }

        public virtual string BCC
        {
            get
            {
                return this.GetPipedText("Bcc", this._bcc);
            }
        }

        public virtual string Body
        {
            get
            {
                return this._body;
            }
        }

        public virtual string From
        {
            get
            {
                string str = this.GetPipedText("From", this._from) ?? string.Empty;
                string[] strArray = str.Split(new char[] { ',', ';' });
                if (strArray.Length > 0)
                {
                    foreach (string str2 in strArray)
                    {
                        if (Utilities.IsNotNullOrEmpty(str2))
                        {
                            return str2;
                        }
                    }
                }
                return str;
            }
        }

        public virtual ContentType MessageContentType
        {
            get
            {
                ContentType type = new ContentType();
                if ("Html".Equals(this.MessageFormat, StringComparison.InvariantCultureIgnoreCase))
                {
                    type.MediaType = "text/html";
                }
                else if ("Xml".Equals(this.MessageFormat, StringComparison.InvariantCultureIgnoreCase))
                {
                    type.MediaType = "text/xml";
                }
                else
                {
                    type.MediaType = "text/plain";
                }
                type.Parameters["charset"] = "utf-8";
                return type;
            }
        }

        public virtual string MessageFormat
        {
            get
            {
                return this._messageFormat;
            }
        }

        protected virtual bool OkToSend
        {
            get
            {
                if (base.Excluded || (base.Response == null))
                {
                    return false;
                }
                return (!this._sendOnlyOnce || !this.CheckEmailSent());
            }
        }

        public virtual int? StyleTemplateID
        {
            get
            {
                return this._styleTemplateID;
            }
        }

        public virtual string Subject
        {
            get
            {
                return this.GetPipedText("Subject", this._subject);
            }
        }

        public virtual string To
        {
            get
            {
                return this.GetPipedText("To", this._to);
            }
        }

        public override bool Visible
        {
            get
            {
                return (base.Response == null);
            }
        }
    }
}

