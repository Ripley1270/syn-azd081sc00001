﻿namespace Checkbox.Forms.PageLayout.Configuration
{
    using System;

    public enum LayoutTemplateExtendedPropertyType
    {
        Color = 4,
        Integer = 2,
        Numeric = 3,
        String = 1
    }
}

