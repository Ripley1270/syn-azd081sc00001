﻿namespace Checkbox.Analytics.Items.UI
{
    using System;

    [Serializable]
    public class TotalScoreItemAppearanceData : AnalysisItemAppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "ANALYSIS_TOTAL_SCORE";
            }
        }
    }
}

