﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class HorizontalLineItemData : ResponseItemData
    {
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new Exception("DataID must be set to create item data.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertHR");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("Width", DbType.Int32, this.LineWidth);
            storedProcCommandWrapper.AddInParameter("Unit", DbType.String, this.WidthUnit);
            storedProcCommandWrapper.AddInParameter("Color", DbType.String, this.Color);
            storedProcCommandWrapper.AddInParameter("Thickness", DbType.Int32, this.Thickness);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new HorizontalLine();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetHR");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, this.DataTableName);
            return dataSet;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            try
            {
                this.LineWidth = DbUtility.GetValueFromDataRow<int?>(data, "Width", 100);
                this.WidthUnit = DbUtility.GetValueFromDataRow<string>(data, "Unit", "Percent");
                this.Thickness = DbUtility.GetValueFromDataRow<int?>(data, "Thickness", 1);
                this.Color = DbUtility.GetValueFromDataRow<string>(data, "Color", string.Empty);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new Exception("DataID must be set to update item.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateHR");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("Width", DbType.Int32, this.LineWidth);
            storedProcCommandWrapper.AddInParameter("Unit", DbType.String, this.WidthUnit);
            storedProcCommandWrapper.AddInParameter("Color", DbType.String, this.Color);
            storedProcCommandWrapper.AddInParameter("Thickness", DbType.Int32, this.Thickness);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public string Color { get; set; }

        public override string DataTableName
        {
            get
            {
                return "HorizontalLineItemData";
            }
        }

        public int? LineWidth { get; set; }

        public int? Thickness { get; set; }

        public string WidthUnit { get; set; }
    }
}

