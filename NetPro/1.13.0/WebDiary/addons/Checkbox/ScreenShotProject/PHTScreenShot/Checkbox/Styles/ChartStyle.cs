﻿namespace Checkbox.Styles
{
    using System;
    using System.Runtime.CompilerServices;

    public class ChartStyle
    {
        public ChartStyle(int id, int appearanceID, string name, string createdBy, bool editable, bool isPublic)
        {
            this.ID = id;
            this.AppearanceID = appearanceID;
            this.Name = name;
            this.CreatedBy = createdBy;
            this.Editable = editable;
            this.Public = isPublic;
        }

        public int AppearanceID { get; private set; }

        public string CreatedBy { get; private set; }

        public bool Editable { get; private set; }

        public int ID { get; private set; }

        public string Name { get; private set; }

        public bool Public { get; private set; }
    }
}

