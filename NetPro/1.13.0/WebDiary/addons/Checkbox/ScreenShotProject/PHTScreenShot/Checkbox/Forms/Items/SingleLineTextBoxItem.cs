﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Validation;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Specialized;

    [Serializable]
    public class SingleLineTextBoxItem : TextItem
    {
        private double? _maxNumericValue;
        private double? _minNumericValue;

        public override void Configure(ItemData configuration, string languageCode)
        {
            base.Configure(configuration, languageCode);
            ArgumentValidation.CheckExpectedType(configuration, typeof(SingleLineTextItemData));
            SingleLineTextItemData data = (SingleLineTextItemData) configuration;
            this._minNumericValue = data.MinValue;
            this._maxNumericValue = data.MaxValue;
        }

        protected override NameValueCollection GetMetaDataValuesForXmlSerialization()
        {
            NameValueCollection metaDataValuesForXmlSerialization = base.GetMetaDataValuesForXmlSerialization();
            metaDataValuesForXmlSerialization["minNumericValue"] = this.MinNumericValue.HasValue ? this.MinNumericValue.ToString() : null;
            metaDataValuesForXmlSerialization["maxNumericValue"] = this.MaxNumericValue.HasValue ? this.MaxNumericValue.ToString() : null;
            return metaDataValuesForXmlSerialization;
        }

        protected override bool ValidateAnswers()
        {
            SingleLineTextAnswerValidator validator = new SingleLineTextAnswerValidator();
            if (!validator.Validate(this))
            {
                base.ValidationErrors.Add(validator.GetMessage(base.LanguageCode));
                return false;
            }
            return true;
        }

        public double? MaxNumericValue
        {
            get
            {
                return this._maxNumericValue;
            }
        }

        public double? MinNumericValue
        {
            get
            {
                return this._minNumericValue;
            }
        }
    }
}

