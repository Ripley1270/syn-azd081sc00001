﻿namespace Checkbox.Analytics.Items.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class ExportItemData : AnalysisItemData
    {
        private List<int> _sourceItemIds;

        protected ExportItemData()
        {
        }

        public override void AddSourceItem(int sourceItemID)
        {
            if (!this.SourceItemIDs.Contains(sourceItemID))
            {
                this.SourceItemIDs.Add(sourceItemID);
            }
        }

        public override string DataTableName
        {
            get
            {
                return "ExportItemData";
            }
        }

        public virtual bool IncludeHidden { get; set; }

        public virtual bool IncludeOpenEnded { get; set; }

        public virtual bool MergeSelectMany { get; set; }

        public override List<int> SourceItemIDs
        {
            get
            {
                if (this._sourceItemIds == null)
                {
                    this._sourceItemIds = new List<int>();
                }
                return this._sourceItemIds;
            }
        }
    }
}

