﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Globalization.Text;
    using System;

    public class CanadaZipCodeValidator : RegularExpressionValidator
    {
        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/usPostal", languageCode);
        }

        public override bool Validate(string input)
        {
            string str = "[a-ceghj-npr-tv-zA-CEGHJ-NPR-TV-Z]";
            string str2 = "[0-9]";
            base._regex = "^" + str + str2 + str + str2 + str + str2 + "$";
            return base.Validate(input.Replace(" ", ""));
        }
    }
}

