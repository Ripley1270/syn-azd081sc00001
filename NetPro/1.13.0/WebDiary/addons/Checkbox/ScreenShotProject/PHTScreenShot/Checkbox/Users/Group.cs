﻿namespace Checkbox.Users
{
    using Checkbox.Common;
    using Checkbox.Globalization.Text;
    using Checkbox.Management;
    using Checkbox.Security;
    using Checkbox.Users.Data;
    using Checkbox.Users.Security;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Security.Principal;

    [Serializable]
    public class Group : AccessControllablePersistedDomainObject, IAccessPermissible
    {
        private static GroupCache _groupCache = new GroupCache();
        private Dictionary<string, string> _pendingDeletes;
        private Dictionary<string, string> _pendingInserts;
        private static object _syncObject = new object();
        private Dictionary<string, string> _userIdentities;
        public static int DEFAULT_ID = -1;

        protected Group(string name) : this(name, string.Empty, DEFAULT_ID, new IIdentity[0])
        {
        }

        protected Group(string name, string description, IIdentity[] userIdentities) : this(name, description, DEFAULT_ID, userIdentities)
        {
        }

        protected Group(string name, string description, int id, IIdentity[] userIdentities) : base(new string[] { "Group.Edit", "Group.View", "Group.ManageUsers" }, new string[] { "Group.Edit", "Group.View", "Group.ManageUsers", "Group.Create", "Group.Delete" })
        {
            this._pendingInserts = new Dictionary<string, string>();
            this._pendingDeletes = new Dictionary<string, string>();
            this.Name = name;
            this.Description = description;
            base.ID = new int?(id);
            this._userIdentities = new Dictionary<string, string>(userIdentities.Length);
            foreach (IIdentity identity in userIdentities)
            {
                this._userIdentities.Add(identity.Name, identity.Name);
            }
        }

        private static void AddMembersToGroup(Database db, int groupID, Dictionary<string, string> members, IDbTransaction transaction)
        {
            foreach (string str in members.Keys)
            {
                DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Group_AddMember");
                storedProcCommandWrapper.AddInParameter("GroupID", DbType.Int32, groupID);
                storedProcCommandWrapper.AddInParameter("MemberID", DbType.String, str);
                db.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            }
        }

        public void AddUser(IIdentity identity)
        {
            this.AddUser(identity.Name);
        }

        public void AddUser(IPrincipal principal)
        {
            this.AddUser(principal.Identity);
        }

        public void AddUser(string uniqueName)
        {
            if (!this._userIdentities.ContainsKey(uniqueName))
            {
                this._userIdentities.Add(uniqueName, uniqueName);
                this._pendingInserts.Add(uniqueName, uniqueName);
            }
        }

        public static void Commit(Group group, ExtendedPrincipal userContext)
        {
            if (group.ID > 0)
            {
                if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(userContext, group, "Group.Edit"))
                {
                    throw new AuthorizationException();
                }
                DoUpdate(group);
                group._pendingInserts = new Dictionary<string, string>();
                group._pendingDeletes = new Dictionary<string, string>();
            }
            else
            {
                if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(userContext, "Group.Create"))
                {
                    throw new AuthorizationException();
                }
                if (userContext != null)
                {
                    DoInsert(group, userContext.Identity.Name);
                }
                group._pendingInserts = new Dictionary<string, string>();
                group._pendingDeletes = new Dictionary<string, string>();
            }
            if (ApplicationManager.AppSettings.CacheVolatileDataInApplication)
            {
                lock (_syncObject)
                {
                    _groupCache.IdentityName = string.Empty;
                    _groupCache.AddGroup(group);
                }
            }
        }

        public static Group CopyGroup(Group group, string languageCode, ExtendedPrincipal currentPrincipal)
        {
            int? nullable;
            int num = 1;
            string groupName = string.Concat(new object[] { group.Name, " ", TextManager.GetText("/common/duplicate", languageCode), " ", num });
        Label_008B:
            nullable = null;
            if (IsDuplicateName(nullable, groupName))
            {
                num++;
                groupName = string.Concat(new object[] { group.Name, " ", TextManager.GetText("/common/duplicate", languageCode), " ", num });
                goto Label_008B;
            }
            Group group2 = new Group(groupName, group.Description, group.GetUserIdentities());
            Commit(group2, currentPrincipal);
            return group2;
        }

        protected override void Create(IDbTransaction t)
        {
            throw new NotImplementedException();
        }

        public static Group CreateGroup(string name, string description)
        {
            return new Group(name) { Description = description };
        }

        public static void DeleteGroup(int id, ExtendedPrincipal userContext)
        {
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(userContext, GetGroup(id), "Group.Delete"))
            {
                throw new AuthorizationException();
            }
            if (id == 1)
            {
                throw new Exception("Deleting the Everyone Group is not supported.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            using (IDbConnection connection = database.GetConnection())
            {
                connection.Open();
                IDbTransaction transaction = connection.BeginTransaction();
                try
                {
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Group_Delete");
                    storedProcCommandWrapper.AddInParameter("GroupID", DbType.Int32, id);
                    database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        private static void DoInsert(Group group, string creatorUniqueIdentifier)
        {
            Database db = DatabaseFactory.CreateDatabase();
            using (IDbConnection connection = db.GetConnection())
            {
                connection.Open();
                IDbTransaction transaction = connection.BeginTransaction();
                try
                {
                    DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Group_Create");
                    storedProcCommandWrapper.AddInParameter("GroupName", DbType.String, group.Name);
                    storedProcCommandWrapper.AddInParameter("Description", DbType.String, group.Description);
                    storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, creatorUniqueIdentifier);
                    storedProcCommandWrapper.AddOutParameter("GroupID", DbType.Int32, 4);
                    storedProcCommandWrapper.AddOutParameter("GroupACLID", DbType.Int32, 4);
                    storedProcCommandWrapper.AddOutParameter("GroupDefaultPolicyID", DbType.Int32, 4);
                    db.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                    object parameterValue = storedProcCommandWrapper.GetParameterValue("GroupID");
                    object obj3 = storedProcCommandWrapper.GetParameterValue("GroupACLID");
                    object obj4 = storedProcCommandWrapper.GetParameterValue("GroupDefaultPolicyID");
                    if ((parameterValue == null) || (parameterValue == DBNull.Value))
                    {
                        throw new DataException("Error creating new Group");
                    }
                    if ((obj3 == null) || (obj3 == DBNull.Value))
                    {
                        throw new DataException("Error creating ACL for Group");
                    }
                    if ((obj4 == null) || (obj4 == DBNull.Value))
                    {
                        throw new DataException("Error creating Default Policy for Group");
                    }
                    group.ID = new int?((int) parameterValue);
                    group.SetAclID((int) obj3);
                    group.SetDefaultPolicyID((int) obj4);
                    group.CreatedBy = creatorUniqueIdentifier;
                    AddMembersToGroup(db, group.ID.Value, group._userIdentities, transaction);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        private static void DoUpdate(Group group)
        {
            Database db = DatabaseFactory.CreateDatabase();
            using (IDbConnection connection = db.GetConnection())
            {
                connection.Open();
                IDbTransaction transaction = connection.BeginTransaction();
                try
                {
                    AddMembersToGroup(db, group.ID.Value, group._pendingInserts, transaction);
                    foreach (string str in group._pendingDeletes.Keys)
                    {
                        DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Group_RemoveMember");
                        storedProcCommandWrapper.AddInParameter("GroupID", DbType.Int32, group.ID);
                        storedProcCommandWrapper.AddInParameter("MemberID", DbType.String, str);
                        db.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                    }
                    UpdateQuery query = new UpdateQuery("ckbx_Group");
                    query.AddValueParameter("GroupName", "'" + group.Name.Replace("'", "''") + "'");
                    query.AddValueParameter("Description", "'" + group.Description.Replace("'", "''") + "'");
                    query.AddValueParameter("CreatedBy", "'" + group.CreatedBy.Replace("'", "''") + "'");
                    query.AddCriterion(new QueryCriterion(new SelectParameter("GroupID"), CriteriaOperator.EqualTo, new LiteralParameter(group.ID)));
                    DBCommandWrapper sqlStringCommandWrapper = db.GetSqlStringCommandWrapper(query.ToString());
                    db.ExecuteNonQuery(sqlStringCommandWrapper, transaction);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public override DataSet GetConfigurationDataSet()
        {
            throw new NotImplementedException();
        }

        public override SecurityEditor GetEditor()
        {
            return new GroupSecurityEditor(this);
        }

        public static Group GetGroup(int id)
        {
            Group groupToAdd = null;
            if (ApplicationManager.AppSettings.CacheVolatileDataInApplication)
            {
                groupToAdd = _groupCache.GetGroup(id);
                if (groupToAdd != null)
                {
                    return groupToAdd;
                }
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Group_GetGroup");
            storedProcCommandWrapper.AddInParameter("GroupID", DbType.Int32, id);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        groupToAdd = new Group(DbUtility.GetValueFromDataReader<string>(reader, "GroupName", string.Empty)) {
                            ID = new int?(id),
                            Description = DbUtility.GetValueFromDataReader<string>(reader, "Description", string.Empty),
                            AclID = new int?(DbUtility.GetValueFromDataReader<int>(reader, "AclID", 0)),
                            CreatedBy = DbUtility.GetValueFromDataReader<string>(reader, "CreatedBy", string.Empty),
                            DefaultPolicyID = new int?(DbUtility.GetValueFromDataReader<int>(reader, "DefaultPolicy", 0))
                        };
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            if ((groupToAdd != null) && ApplicationManager.AppSettings.CacheVolatileDataInApplication)
            {
                lock (_syncObject)
                {
                    _groupCache.AddGroup(groupToAdd);
                }
            }
            return groupToAdd;
        }

        public static Group GetGroup(string groupName)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Group_GetIdFromName");
            storedProcCommandWrapper.AddInParameter("GroupName", DbType.String, groupName);
            storedProcCommandWrapper.AddOutParameter("GroupId", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("GroupID");
            if ((parameterValue != null) && (parameterValue != DBNull.Value))
            {
                return GetGroup((int) parameterValue);
            }
            return null;
        }

        public static List<int> GetGroupMembershipIds(IIdentity userIdentity)
        {
            if (userIdentity == null)
            {
                return new List<int>();
            }
            if (ApplicationManager.AppSettings.CacheVolatileDataInApplication && userIdentity.Name.Equals(_groupCache.IdentityName, StringComparison.InvariantCultureIgnoreCase))
            {
                return _groupCache.GroupMemberships;
            }
            List<int> list = new List<int>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Group_GetIDMemberships");
            storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, userIdentity.Name);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        int item = DbUtility.GetValueFromDataReader<int>(reader, "GroupId", -1);
                        if ((item > 0) && !list.Contains(item))
                        {
                            list.Add(item);
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            if (ApplicationManager.AppSettings.CacheVolatileDataInApplication)
            {
                lock (_syncObject)
                {
                    _groupCache.IdentityName = userIdentity.Name;
                    _groupCache.GroupMemberships = list;
                }
            }
            return list;
        }

        public static List<Group> GetGroupMemberships(IIdentity userIdentity)
        {
            List<int> groupMembershipIds = GetGroupMembershipIds(userIdentity);
            List<Group> list2 = new List<Group>();
            foreach (int num in groupMembershipIds)
            {
                list2.Add(GetGroup(num));
            }
            list2.Sort((g1, g2) => g1.Name.CompareTo(g2.Name));
            return list2;
        }

        public static List<Group> GetGroupMemberships(IPrincipal userPrincipal)
        {
            return GetGroupMemberships(userPrincipal.Identity);
        }

        public static Group[] GetGroups(ExtendedPrincipal currentPrincipal, PermissionJoin permissionJoinType, params string[] permissions)
        {
            DataSet set = GetGroupsDataSet(currentPrincipal, "GroupName", false, permissionJoinType, permissions);
            ArrayList list = new ArrayList();
            if ((set.Tables.Count > 0) && set.Tables[0].Columns.Contains("GroupID"))
            {
                for (int i = 0; i < set.Tables[0].Rows.Count; i++)
                {
                    object obj2 = set.Tables[0].Rows[i]["GroupID"];
                    if (obj2 != DBNull.Value)
                    {
                        Group group = GetGroup(Convert.ToInt32(obj2));
                        list.Add(group);
                    }
                }
            }
            return (Group[]) list.ToArray(typeof(Group));
        }

        public static int GetGroupsCount(ExtendedPrincipal currentPrincipal, string filterField, string filterValue, PermissionJoin permissionJoinType, params string[] permissions)
        {
            int num;
            SelectQuery query = QueryFactory.GetAvailableGroupsCountQuery(currentPrincipal, filterField, filterValue, permissionJoinType, permissions);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return Convert.ToInt32(reader["RecordCount"]);
                    }
                    num = 0;
                }
                finally
                {
                    reader.Close();
                }
            }
            return num;
        }

        public static int GetGroupsCount(ExtendedPrincipal currentPrincipal, string filterField, string filterValue, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            int num;
            SelectQuery query = QueryFactory.GetAvailableGroupsCountQuery(currentPrincipal, filterField, filterValue, filterCollection, permissionJoinType, permissions);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return Convert.ToInt32(reader["RecordCount"]);
                    }
                    num = 0;
                }
                finally
                {
                    reader.Close();
                }
            }
            return num;
        }

        public static DataSet GetGroupsDataSet(ExtendedPrincipal currentPrincipal, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = QueryFactory.GetAvailableGroupsQuery(currentPrincipal, permissionJoinType, permissions);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            return database.ExecuteDataSet(sqlStringCommandWrapper);
        }

        public static DataSet GetGroupsDataSet(ExtendedPrincipal currentPrincipal, string sortField, bool sortDescending, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = QueryFactory.GetAvailableGroupsQuery(currentPrincipal, sortField, !sortDescending, permissionJoinType, permissions);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            return database.ExecuteDataSet(sqlStringCommandWrapper);
        }

        public static DataSet GetGroupsDataSet(ExtendedPrincipal currentPrincipal, int pageNumber, int resultsPerPage, string filterField, string filterValue, string sortField, bool sortDescending, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = QueryFactory.GetAvailableGroupsQuery(currentPrincipal, resultsPerPage, pageNumber, filterField, filterValue, sortField, !sortDescending, permissionJoinType, permissions);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            return database.ExecuteDataSet(sqlStringCommandWrapper);
        }

        public static DataSet GetGroupsDataSet(ExtendedPrincipal currentPrincipal, int pageNumber, int resultsPerPage, string filterField, string filterValue, string sortField, bool sortDescending, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = QueryFactory.GetAvailableGroupsQuery(currentPrincipal, resultsPerPage, pageNumber, filterField, filterValue, sortField, !sortDescending, filterCollection, permissionJoinType, permissions);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            return database.ExecuteDataSet(sqlStringCommandWrapper);
        }

        public static int GetMembersCount(int groupID)
        {
            SelectQuery query = new SelectQuery("ckbx_GroupMembers");
            query.AddCountParameter("MemberUniqueIdentifier", "MemberCount");
            query.AddCriterion(new QueryCriterion(new SelectParameter("GroupID"), CriteriaOperator.EqualTo, new LiteralParameter(groupID.ToString())));
            int num = 0;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    try
                    {
                        if (reader.Read())
                        {
                            num = (int) reader["MemberCount"];
                        }
                    }
                    catch (Exception exception)
                    {
                        if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                        {
                            throw;
                        }
                    }
                    return num;
                }
                finally
                {
                    reader.Close();
                }
            }
            return num;
        }

        public string[] GetUserIdentifiers()
        {
            if ((this._userIdentities == null) || (this._userIdentities.Count == 0))
            {
                this.LoadMembers();
            }
            List<string> list = new List<string>();
            if (this._userIdentities != null)
            {
                foreach (string str in this._userIdentities.Keys)
                {
                    list.Add(str);
                }
            }
            return list.ToArray();
        }

        public IIdentity[] GetUserIdentities()
        {
            if ((this._userIdentities == null) || (this._userIdentities.Count == 0))
            {
                this.LoadMembers();
            }
            ArrayList list = new ArrayList();
            if (this._userIdentities != null)
            {
                foreach (string str in this._userIdentities.Keys)
                {
                    IIdentity userIdentity = UserManager.GetUserIdentity(str);
                    if (userIdentity != null)
                    {
                        list.Add(userIdentity);
                    }
                }
            }
            return (IIdentity[]) list.ToArray(typeof(IIdentity));
        }

        public IPrincipal[] GetUsers()
        {
            if ((this._userIdentities == null) || (this._userIdentities.Count == 0))
            {
                this.LoadMembers();
            }
            ArrayList list = new ArrayList();
            if (this._userIdentities != null)
            {
                foreach (string str in this._userIdentities.Keys)
                {
                    IPrincipal user = UserManager.GetUser(str);
                    if (user != null)
                    {
                        list.Add(user);
                    }
                }
            }
            return (IPrincipal[]) list.ToArray(typeof(IPrincipal));
        }

        public static bool IsDuplicateName(int? groupId, string groupName)
        {
            if (groupName != null)
            {
                groupName = groupName.Trim();
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Group_IsNameUnique");
            storedProcCommandWrapper.AddInParameter("GroupName", DbType.String, groupName);
            storedProcCommandWrapper.AddInParameter("GroupId", DbType.Int32, groupId.GetValueOrDefault());
            return Convert.ToBoolean(Convert.ToInt32(database.ExecuteScalar(storedProcCommandWrapper)));
        }

        private static bool IsNotAGroupMember(Database db, int groupID, string member, IDbTransaction transaction)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Group_IsAMember");
            storedProcCommandWrapper.AddInParameter("GroupID", DbType.Int32, groupID);
            storedProcCommandWrapper.AddInParameter("MemberID", DbType.String, member);
            object obj2 = db.ExecuteScalar(storedProcCommandWrapper, transaction);
            return "0".Equals(obj2.ToString());
        }

        private void LoadMembers()
        {
            this._userIdentities = new Dictionary<string, string>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Group_GetMembers");
            storedProcCommandWrapper.AddInParameter("GroupID", DbType.Int32, base.ID);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        if (((reader[0] != null) && (reader[0] != DBNull.Value)) && (reader[0] is string))
                        {
                            string str = (string) reader[0];
                            this._userIdentities[str] = str;
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
        }

        public void RemoveUser(IIdentity identity)
        {
            this.RemoveUser(identity.Name);
        }

        public void RemoveUser(IPrincipal principal)
        {
            this.RemoveUser(principal.Identity);
        }

        public void RemoveUser(string uniqueName)
        {
            this._userIdentities.Remove(uniqueName);
            this._pendingDeletes.Add(uniqueName, uniqueName);
        }

        protected void SetAclID(int aclId)
        {
            base.AclID = new int?(aclId);
        }

        protected void SetDefaultPolicyID(int defaultPolicyId)
        {
            base.DefaultPolicyID = new int?(defaultPolicyId);
        }

        protected override void Update(IDbTransaction t)
        {
            throw new NotImplementedException();
        }

        public virtual string AclEntryIdentifier
        {
            get
            {
                return base.ID.ToString();
            }
        }

        public virtual string AclTypeIdentifier
        {
            get
            {
                return "Checkbox.Users.Group";
            }
        }

        public string CreatedBy { get; set; }

        public override string DataTableName
        {
            get
            {
                return "UserGroupData";
            }
        }

        public string Description { get; set; }

        public override string DomainDBIdentityColumnName
        {
            get
            {
                return "GroupID";
            }
        }

        public override string DomainDBTableName
        {
            get
            {
                return "ckbx_Group";
            }
        }

        public static EveryoneGroup Everyone
        {
            get
            {
                EveryoneGroup group = new EveryoneGroup();
                Group group2 = GetGroup(1);
                group.SetAclID(group2.AclID.Value);
                group.ID = 1;
                group.SetDefaultPolicyID(group2.DefaultPolicyID.Value);
                group.Name = group2.Name;
                group.Description = group2.Description;
                return group;
            }
        }

        public override string IdentityColumnName
        {
            get
            {
                return "GroupId";
            }
        }

        public string Name { get; set; }

        public string PermissibleEntityName
        {
            get
            {
                return this.Name;
            }
        }
    }
}

