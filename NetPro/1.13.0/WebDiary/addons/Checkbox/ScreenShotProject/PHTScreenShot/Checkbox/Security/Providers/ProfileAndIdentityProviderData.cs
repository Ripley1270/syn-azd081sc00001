﻿namespace Checkbox.Security.Providers
{
    using Prezza.Framework.Common;
    using Prezza.Framework.Security.Providers;
    using System;
    using System.Runtime.CompilerServices;
    using System.Xml;

    internal class ProfileAndIdentityProviderData : DbProfileProviderData
    {
        public ProfileAndIdentityProviderData(string providerName) : base(providerName, typeof(ProfileAndIdentityProvider).AssemblyQualifiedName)
        {
            ArgumentValidation.CheckForEmptyString(providerName, "provderName");
        }

        public override void LoadFromXml(XmlNode node)
        {
            base.LoadFromXml(node);
            XmlNode node2 = node.SelectSingleNode("identityTable");
            if (node2 == null)
            {
                throw new Exception("Unable to find identityTable node for provider: " + node.Name);
            }
            this.IdentityProfileTable = XmlUtility.GetAttributeText(node2, "identityProfileTable", true);
        }

        public string IdentityProfileTable { get; private set; }
    }
}

