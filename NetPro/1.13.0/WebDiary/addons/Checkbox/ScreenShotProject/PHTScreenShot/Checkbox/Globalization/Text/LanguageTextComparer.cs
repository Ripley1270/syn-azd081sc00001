﻿namespace Checkbox.Globalization.Text
{
    using System;
    using System.Collections.Generic;

    public class LanguageTextComparer : IComparer<string>
    {
        private string _languageCode;

        public LanguageTextComparer(string languageCode)
        {
            this._languageCode = languageCode;
        }

        public int Compare(string x, string y)
        {
            string text = TextManager.GetText("/languageText/" + x, this._languageCode);
            string strB = TextManager.GetText("/languageText/" + y, this._languageCode);
            if ((text == null) || (text.Trim() == string.Empty))
            {
                text = x;
            }
            if ((strB == null) || (strB.Trim() == string.Empty))
            {
                strB = y;
            }
            return string.Compare(text, strB);
        }
    }
}

