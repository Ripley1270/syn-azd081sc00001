﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Management;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Specialized;

    [Serializable]
    public class RedirectItem : ResponseItem
    {
        private bool _autoRedirect;
        private bool _restartSurvey;
        private string _url;
        private string _urlText;

        public override void Configure(ItemData configuration, string languageCode)
        {
            ArgumentValidation.CheckExpectedType(configuration, typeof(RedirectItemData));
            base.Configure(configuration, languageCode);
            RedirectItemData data = (RedirectItemData) configuration;
            this._restartSurvey = data.RestartSurvey;
            if (data.RestartSurvey)
            {
                this._url = string.Format("{0}/survey.aspx?s={1}ResponseTemplateGUID&ForceNew=true", ApplicationManager.ApplicationRoot, ApplicationManager.AppSettings.PipePrefix);
            }
            else
            {
                this._url = data.URL;
            }
            this._autoRedirect = data.RedirectAutomatically;
            this._urlText = this.GetText(data.URLTextID);
        }

        protected override NameValueCollection GetInstanceDataValuesForXmlSerialization()
        {
            NameValueCollection instanceDataValuesForXmlSerialization = base.GetInstanceDataValuesForXmlSerialization();
            instanceDataValuesForXmlSerialization["redirectUrl"] = this.URL;
            instanceDataValuesForXmlSerialization["isRedirectAutomatic"] = this.RedirectAutomatically.ToString();
            instanceDataValuesForXmlSerialization["isRestartSurvey"] = this.RestartSurvey.ToString();
            instanceDataValuesForXmlSerialization["linkText"] = this.LinkText;
            return instanceDataValuesForXmlSerialization;
        }

        public virtual string LinkText
        {
            get
            {
                return this.GetPipedText("UrlText", this._urlText);
            }
        }

        public virtual bool RedirectAutomatically
        {
            get
            {
                return this._autoRedirect;
            }
        }

        public virtual bool RestartSurvey
        {
            get
            {
                return this._restartSurvey;
            }
        }

        public virtual string URL
        {
            get
            {
                return this.GetPipedText("Url", this._url).Trim();
            }
        }
    }
}

