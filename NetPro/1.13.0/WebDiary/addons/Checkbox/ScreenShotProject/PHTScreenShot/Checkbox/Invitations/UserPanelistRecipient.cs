﻿namespace Checkbox.Invitations
{
    using Checkbox.Panels;
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class UserPanelistRecipient : Recipient
    {
        public UserPanelistRecipient(Panelist panelist) : base(panelist)
        {
        }

        protected override void InsertRecipient()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_InsertRecipient");
            storedProcCommandWrapper.AddInParameter("InvitationID", DbType.Int32, base.InvitationID);
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.PanelID);
            storedProcCommandWrapper.AddInParameter("EmailToAddress", DbType.String, base.EmailToAddress);
            storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, base.UniqueIdentifier);
            storedProcCommandWrapper.AddInParameter("GUID", DbType.Guid, base.GUID);
            storedProcCommandWrapper.AddInParameter("LastSent", DbType.DateTime, base.LastSent);
            storedProcCommandWrapper.AddInParameter("Success", DbType.Boolean, base.SuccessfullySent);
            storedProcCommandWrapper.AddInParameter("Error", DbType.String, base.Error);
            storedProcCommandWrapper.AddInParameter("LastBatchMessageId", DbType.Int64, base.BatchMessageId);
            storedProcCommandWrapper.AddOutParameter("RecipientID", DbType.Int64, 8);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("RecipientID");
            if (parameterValue == DBNull.Value)
            {
                throw new Exception("Unable to insert recipient into database.");
            }
            base.ID = new long?((long) Convert.ToInt32(parameterValue));
        }
    }
}

