﻿namespace Checkbox.Panels
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class Panelist : IComparable<Panelist>
    {
        private Dictionary<string, string> _propertiesDictionary;

        public int CompareTo(Panelist other)
        {
            return string.Compare(this.Email, other.Email);
        }

        public string ComputeHashKey(int panelId)
        {
            string str = panelId.ToString();
            if (this.ContainsProperty("UniqueIdentifier"))
            {
                return (str + "__" + this.GetProperty("UniqueIdentifier"));
            }
            return (str + "__" + this.Email);
        }

        public virtual bool ContainsProperty(string propertyName)
        {
            return this.PropertiesDictionary.ContainsKey(propertyName);
        }

        public virtual string GetProperty(string propertyName)
        {
            if (this.ContainsProperty(propertyName))
            {
                return this.PropertiesDictionary[propertyName];
            }
            return string.Empty;
        }

        public virtual void SetProperty(string propertyName, string propertyValue)
        {
            this.PropertiesDictionary[propertyName] = propertyValue;
        }

        public string Email
        {
            get
            {
                return this.GetProperty("Email");
            }
            set
            {
                this.SetProperty("Email", value);
            }
        }

        protected Dictionary<string, string> PropertiesDictionary
        {
            get
            {
                if (this._propertiesDictionary == null)
                {
                    this._propertiesDictionary = new Dictionary<string, string>();
                }
                return this._propertiesDictionary;
            }
        }
    }
}

