﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using System;

    public class SelectManyValidator : SelectItemValidator
    {
        protected override bool ValidateRequired(SelectItem input)
        {
            int count = input.SelectedOptions.Count;
            int? minToSelect = ((SelectMany) input).MinToSelect;
            int? maxToSelect = ((SelectMany) input).MaxToSelect;
            if (minToSelect.HasValue && (minToSelect.Value > count))
            {
                if (minToSelect.Value == 1)
                {
                    base.ErrorMessage = TextManager.GetText("/validationMessages/selectMany/minErrorSingular", input.LanguageCode);
                }
                else
                {
                    base.ErrorMessage = TextManager.GetText("/validationMessages/selectMany/minError", input.LanguageCode).Replace("{min}", minToSelect.ToString());
                }
                return false;
            }
            if (maxToSelect.HasValue && (maxToSelect.Value < count))
            {
                base.ErrorMessage = TextManager.GetText("/validationMessages/selectMany/maxError", input.LanguageCode).Replace("{max}", maxToSelect.ToString());
                return false;
            }
            return true;
        }
    }
}

