﻿namespace Checkbox.Forms
{
    using System;

    public class TemplateLoadError : TemplateException
    {
        public TemplateLoadError(int templateID) : base(templateID)
        {
        }

        public TemplateLoadError(string templateGuid) : base(templateGuid)
        {
        }
    }
}

