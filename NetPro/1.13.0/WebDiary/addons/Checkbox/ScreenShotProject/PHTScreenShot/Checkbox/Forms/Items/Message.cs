﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Specialized;

    [Serializable]
    public class Message : ResponseItem
    {
        private string _text;

        public override void Configure(ItemData configuration, string languageCode)
        {
            ArgumentValidation.CheckExpectedType(configuration, typeof(MessageItemData));
            base.Configure(configuration, languageCode);
            MessageItemData data = (MessageItemData) configuration;
            this._text = this.GetText(data.TextID);
        }

        protected override NameValueCollection GetInstanceDataValuesForXmlSerialization()
        {
            NameValueCollection instanceDataValuesForXmlSerialization = base.GetInstanceDataValuesForXmlSerialization();
            instanceDataValuesForXmlSerialization["text"] = this.Text;
            return instanceDataValuesForXmlSerialization;
        }

        public virtual string Text
        {
            get
            {
                return this.GetPipedText("Message", this._text);
            }
        }
    }
}

