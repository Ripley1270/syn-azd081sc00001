﻿namespace Checkbox.Analytics.Items.Configuration
{
    using Checkbox.Analytics.Items;
    using Checkbox.Forms.Items;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class AverageScoreItemData : AnalysisItemData
    {
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new Exception("Unable to save analysis item.  DataID is <= 0.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertAverageScore");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ScoreOption", DbType.String, this.AverageScoreCalculation.ToString());
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateDataTables(t);
        }

        protected override Item CreateItem()
        {
            return new AverageScoreItem();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID.Value <= 0))
            {
                throw new Exception("Unable to load average score item data.  No id was specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetAverageScore");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName, AnalysisItemData.SourceItemsTableName, AnalysisItemData.ResponseTemplatesTableName });
            return dataSet;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data != null)
            {
                base.LoadFromDataRow(data);
                if (data["ScoreOption"] != DBNull.Value)
                {
                    this.AverageScoreCalculation = (Checkbox.Analytics.Items.Configuration.AverageScoreCalculation) Enum.Parse(typeof(Checkbox.Analytics.Items.Configuration.AverageScoreCalculation), (string) data["ScoreOption"]);
                }
                else
                {
                    this.AverageScoreCalculation = Checkbox.Analytics.Items.Configuration.AverageScoreCalculation.Aggregate;
                }
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new Exception("Unable to save analysis item.  DataID is <= 0.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateAverageScore");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ScoreOption", DbType.String, this.AverageScoreCalculation.ToString());
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateDataTables(t);
        }

        public virtual Checkbox.Analytics.Items.Configuration.AverageScoreCalculation AverageScoreCalculation { get; set; }

        public override string DataTableName
        {
            get
            {
                return "AverageScoreItemData";
            }
        }
    }
}

