﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Checkbox.Common.Captcha;
    using Checkbox.Common.Captcha.Image;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Validation;
    using Checkbox.Management;
    using Prezza.Framework.Caching;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Xml;

    [Serializable]
    public class CaptchaItem : LabelledItem
    {
        private string _code;
        private CodeType _codeType;
        private bool _enableSound;
        private ImageFormatEnum _imageFormat;
        private int _imageHeight;
        private int _imageID;
        private int _imageWidth;
        private int _maxCodeLength;
        private int _minCodeLength;
        private List<TextStyleEnum> _textStyles;

        public override void Configure(ItemData configuration, string languageCode)
        {
            base.Configure(configuration, languageCode);
            this._minCodeLength = ((CaptchaItemData) configuration).MinCodeLength;
            this._maxCodeLength = ((CaptchaItemData) configuration).MaxCodeLength;
            this._codeType = ((CaptchaItemData) configuration).CodeType;
            this._textStyles = ((CaptchaItemData) configuration).TextStyles;
            this._imageFormat = ((CaptchaItemData) configuration).ImageFormat;
            this._imageHeight = ((CaptchaItemData) configuration).ImageHeight;
            this._imageWidth = ((CaptchaItemData) configuration).ImageWidth;
            this._enableSound = ((CaptchaItemData) configuration).EnableSound;
        }

        private string GenerateCode()
        {
            return CaptchaGenerator.GenerateCode(this._codeType, this._minCodeLength, this._maxCodeLength);
        }

        private void GenerateCodeAndImage(bool forceRegen)
        {
            if ((forceRegen || !this.GetImageID().HasValue) || (this.GetCode() == null))
            {
                DbUtility.DeleteTempImages();
                this._code = this.GenerateCode();
                this._imageID = this.GenerateImage(this._code);
                this.StoreData(this._imageID, this._code);
            }
        }

        private int GenerateImage(string code)
        {
            return CaptchaGenerator.GenerateAndStoreImage(code, this._imageFormat, this._imageWidth, this._imageHeight, this._textStyles);
        }

        private string GetCacheKey()
        {
            if (base.Response == null)
            {
                return string.Empty;
            }
            return string.Concat(new object[] { base.Response.ID, "__ ", base.ID, "__CODE" });
        }

        private string GetCode()
        {
            string cacheKey = this.GetCacheKey();
            if (Utilities.IsNotNullOrEmpty(cacheKey))
            {
                CacheManager cacheManager = CacheFactory.GetCacheManager();
                if (cacheManager.Contains(cacheKey) && (cacheManager[cacheKey] != null))
                {
                    string[] strArray = cacheManager[cacheKey].ToString().Split(new string[] { "__" }, StringSplitOptions.RemoveEmptyEntries);
                    if (strArray.Length > 1)
                    {
                        return strArray[1];
                    }
                    return null;
                }
            }
            return this._code;
        }

        private int? GetImageID()
        {
            string cacheKey = this.GetCacheKey();
            if (Utilities.IsNotNullOrEmpty(cacheKey))
            {
                CacheManager cacheManager = CacheFactory.GetCacheManager();
                if (cacheManager.Contains(cacheKey) && (cacheManager[cacheKey] != null))
                {
                    string[] strArray = cacheManager[cacheKey].ToString().Split(new string[] { "__" }, StringSplitOptions.RemoveEmptyEntries);
                    if (strArray.Length > 1)
                    {
                        return new int?(Convert.ToInt32(strArray[0]));
                    }
                    return null;
                }
            }
            return new int?(this._imageID);
        }

        protected override NameValueCollection GetInstanceDataValuesForXmlSerialization()
        {
            NameValueCollection instanceDataValuesForXmlSerialization = base.GetInstanceDataValuesForXmlSerialization();
            instanceDataValuesForXmlSerialization["imageUrl"] = this.ImageUrl;
            instanceDataValuesForXmlSerialization["code"] = this.Code;
            return instanceDataValuesForXmlSerialization;
        }

        protected override NameValueCollection GetMetaDataValuesForXmlSerialization()
        {
            NameValueCollection metaDataValuesForXmlSerialization = base.GetMetaDataValuesForXmlSerialization();
            metaDataValuesForXmlSerialization["minCodeLength"] = this._minCodeLength.ToString();
            metaDataValuesForXmlSerialization["maxCodeLength"] = this._maxCodeLength.ToString();
            metaDataValuesForXmlSerialization["codeType"] = this._codeType.ToString();
            metaDataValuesForXmlSerialization["imageFormat"] = this._imageFormat.ToString();
            metaDataValuesForXmlSerialization["imageHeight"] = this._imageHeight.ToString();
            metaDataValuesForXmlSerialization["imageWidth"] = this._imageWidth.ToString();
            metaDataValuesForXmlSerialization["enableSound"] = this._enableSound.ToString();
            return metaDataValuesForXmlSerialization;
        }

        private void StoreData(int imageID, string code)
        {
            string cacheKey = this.GetCacheKey();
            if (Utilities.IsNotNullOrEmpty(cacheKey))
            {
                CacheFactory.GetCacheManager().Add(cacheKey, imageID + "__" + code);
            }
        }

        protected override bool ValidateAnswers()
        {
            CaptchaItemValidator validator = new CaptchaItemValidator();
            if (!validator.Validate(this))
            {
                base.ValidationErrors.Add(validator.GetMessage(base.LanguageCode));
                this.GenerateCodeAndImage(true);
                return false;
            }
            return true;
        }

        public override void WriteXmlMetaData(XmlWriter writer)
        {
            base.WriteXmlMetaData(writer);
            writer.WriteStartElement("textFormats");
            foreach (TextStyleEnum enum2 in this._textStyles)
            {
                writer.WriteElementString("textFormat", enum2.ToString());
            }
            writer.WriteEndElement();
        }

        public string Code
        {
            get
            {
                this.GenerateCodeAndImage(false);
                return this.GetCode();
            }
        }

        public bool EnableSound
        {
            get
            {
                return this._enableSound;
            }
        }

        public string ImageUrl
        {
            get
            {
                this.GenerateCodeAndImage(false);
                int? imageID = this.GetImageID();
                if (imageID.HasValue)
                {
                    return (ApplicationManager.ApplicationRoot + "/ViewImage.aspx?ImageID=" + imageID);
                }
                return string.Empty;
            }
        }
    }
}

