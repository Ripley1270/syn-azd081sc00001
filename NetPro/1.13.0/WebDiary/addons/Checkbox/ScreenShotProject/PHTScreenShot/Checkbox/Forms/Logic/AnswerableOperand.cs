﻿namespace Checkbox.Forms.Logic
{
    using Checkbox.Forms.Items;
    using System;

    [Serializable]
    public class AnswerableOperand : Operand
    {
        private IAnswerable _answerableItem;

        public AnswerableOperand(IAnswerable item)
        {
            this._answerableItem = item;
        }

        protected override object ProtectedGetValue(RuleEventTrigger trigger)
        {
            if (this.AnswerableItem.HasAnswer)
            {
                return this.AnswerableItem.GetAnswer();
            }
            return null;
        }

        public IAnswerable AnswerableItem
        {
            get
            {
                return this._answerableItem;
            }
        }
    }
}

