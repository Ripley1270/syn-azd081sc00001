﻿namespace Checkbox.Forms.Items.Configuration
{
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class ResponseItemData : ItemData
    {
        protected ResponseItemData()
        {
        }

        protected override ItemData Copy()
        {
            ResponseItemData data = (ResponseItemData) base.Copy();
            if (data != null)
            {
                data.IsRequired = this.IsRequired;
            }
            return data;
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            return new DataSet();
        }

        public virtual bool IsRequired { get; set; }
    }
}

