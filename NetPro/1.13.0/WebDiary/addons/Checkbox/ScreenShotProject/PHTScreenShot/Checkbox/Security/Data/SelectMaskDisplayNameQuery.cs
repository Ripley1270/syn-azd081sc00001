﻿namespace Checkbox.Security.Data
{
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using System;

    internal static class SelectMaskDisplayNameQuery
    {
        internal static SelectQuery GetQuery(string maskName)
        {
            SelectQuery query = new SelectQuery("ckbx_PermissionMask");
            query.AddParameter("MaskDisplayName", string.Empty, string.Empty);
            query.AddCriterion(new QueryCriterion(new SelectParameter("MaskName"), CriteriaOperator.EqualTo, new LiteralParameter("'" + maskName + "'")));
            return query;
        }
    }
}

