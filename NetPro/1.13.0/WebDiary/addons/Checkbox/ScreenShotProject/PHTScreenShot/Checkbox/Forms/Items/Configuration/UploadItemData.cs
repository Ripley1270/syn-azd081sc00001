﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms.Items;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class UploadItemData : LabelledItemData
    {
        private void AddParams(DBCommandWrapper command)
        {
            command.AddInParameter("ItemID", DbType.Int32, base.ID);
            command.AddInParameter("TextID", DbType.String, base.TextID);
            command.AddInParameter("SubTextID", DbType.String, base.SubTextID);
            command.AddInParameter("IsRequired", DbType.Boolean, base.IsRequired);
            command.AddInParameter("AllowedFileTypes", DbType.String, this.AllowedFileTypesCSV);
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to create configuration data data for File Upload Item with null or negative id.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertFileUpload");
            this.AddParams(storedProcCommandWrapper);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new UploadItem();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to load concrete configuration data for File Upload Item with null or negative id.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetFileUpload");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet concreteConfigurationDataSet = base.GetConcreteConfigurationDataSet();
            database.LoadDataSet(storedProcCommandWrapper, concreteConfigurationDataSet, this.DataTableName);
            return concreteConfigurationDataSet;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("Unable to load Upload Item data from a null data row.");
            }
            if (data["IsRequired"] != DBNull.Value)
            {
                this.IsRequired = Convert.ToInt32(data["IsRequired"]) == 1;
            }
            if (data["AllowedFileTypes"] != DBNull.Value)
            {
                this.AllowedFileTypes = new List<string>(((string) data["AllowedFileTypes"]).Split(new char[] { ',' }));
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to update configuration data data for File Upload Item with null or negative id.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateFileUpload");
            this.AddParams(storedProcCommandWrapper);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public List<string> AllowedFileTypes { get; set; }

        public string AllowedFileTypesCSV
        {
            get
            {
                return Utilities.ListToDelimitedString(',', this.AllowedFileTypes);
            }
        }

        public override string DataTableName
        {
            get
            {
                return "UploadItemData";
            }
        }

        public int FileID { get; set; }

        public int FileSize { get; set; }

        public override bool ItemIsIAnswerable
        {
            get
            {
                return true;
            }
        }

        public override string TextIDPrefix
        {
            get
            {
                return "uploadItemData";
            }
        }
    }
}

