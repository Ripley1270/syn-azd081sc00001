﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Globalization.Text;
    using Prezza.Framework.ExceptionHandling;
    using System;

    public class AlphaNumericValidator : RegularExpressionValidator
    {
        public AlphaNumericValidator()
        {
            base._regex = @"^[\d\w\s]+$".Normalize();
        }

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/alphaNumeric", languageCode);
        }

        public override bool Validate(string input)
        {
            if ((base._regex == null) || (base._regex == string.Empty))
            {
                return true;
            }
            try
            {
                return this.RegularExpression.IsMatch(input.Normalize());
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return false;
            }
        }
    }
}

