﻿namespace Checkbox.Security.Data
{
    using Checkbox.Security;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using System;

    internal static class SelectRolesWithPermissionQuery
    {
        internal static SelectQuery GetQuery(string permission)
        {
            SelectQuery query = new SelectQuery("ckbx_Role");
            query.AddSelectDistinctCriterion();
            query.AddParameter("UniqueIdentifier", string.Empty, "ckbx_IdentityRoles");
            query.AddTableJoin("ckbx_RolePermissions", QueryJoinType.Inner, "RoleID", "ckbx_Role", "RoleID");
            query.AddTableJoin("ckbx_IdentityRoles", QueryJoinType.Inner, "RoleID", "ckbx_Role", "RoleID");
            query.AddTableJoin("ckbx_Permission", QueryJoinType.Inner, "PermissionID", "ckbx_RolePermissions", "PermissionID");
            query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", string.Empty, "ckbx_Permission"), CriteriaOperator.EqualTo, new LiteralParameter("'" + permission + "'")));
            return query;
        }

        internal static SelectQuery GetQuery(PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = new SelectQuery("ckbx_Role");
            query.AddSelectDistinctCriterion();
            query.AddParameter("UniqueIdentifier", string.Empty, "ckbx_IdentityRoles");
            query.AddTableJoin("ckbx_RolePermissions", QueryJoinType.Inner, "RoleID", "ckbx_Role", "RoleID");
            query.AddTableJoin("ckbx_IdentityRoles", QueryJoinType.Inner, "RoleID", "ckbx_Role", "RoleID");
            query.AddTableJoin("ckbx_Permission", QueryJoinType.Inner, "PermissionID", "ckbx_RolePermissions", "PermissionID");
            if (permissionJoinType == PermissionJoin.Any)
            {
                string paramValue = string.Empty;
                for (int i = 0; i < permissions.Length; i++)
                {
                    if (i > 0)
                    {
                        paramValue = paramValue + ",";
                    }
                    paramValue = paramValue + "'" + permissions[i] + "'";
                }
                query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", string.Empty, "ckbx_Permission"), CriteriaOperator.In, new LiteralParameter(paramValue, string.Empty, true)));
                return query;
            }
            query.CriteriaJoinType = CriteriaJoinType.And;
            foreach (string str2 in permissions)
            {
                query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", string.Empty, "ckbx_Permission"), CriteriaOperator.EqualTo, new LiteralParameter("'" + str2 + "'")));
            }
            return query;
        }
    }
}

