﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Common;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class CustomFormatValidator : RegularExpressionValidator
    {
        private string _formatId;

        public CustomFormatValidator(string format)
        {
            this._formatId = format;
            base._regex = this.GetRegularExpression(format);
        }

        public static Dictionary<string, string> GetFormats(string languageCode)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_CustomValidators_GetFormats");
            DataSet set = database.ExecuteDataSet(storedProcCommandWrapper);
            if ((set.Tables.Count == 1) && (set.Tables[0].Rows != null))
            {
                foreach (DataRow row in set.Tables[0].Rows)
                {
                    string str = DbUtility.GetValueFromDataRow<string>(row, "FormatId", string.Empty);
                    if (Utilities.IsNotNullOrEmpty(str))
                    {
                        string text = TextManager.GetText(string.Format("{0}/Name", str), languageCode);
                        if (Utilities.IsNotNullOrEmpty(text))
                        {
                            dictionary.Add(str, text);
                        }
                    }
                }
            }
            return dictionary;
        }

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText(string.Format("/validationMessages{0}", this._formatId), languageCode);
        }

        private string GetRegularExpression(string format)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_CustomValidators_GetExpression");
            storedProcCommandWrapper.AddInParameter("FormatId", DbType.String, format);
            object obj2 = database.ExecuteScalar(storedProcCommandWrapper);
            if (obj2 != null)
            {
                return (string) obj2;
            }
            return string.Empty;
        }

        public override bool Validate(string input)
        {
            if (Utilities.IsNullOrEmpty(base._regex))
            {
                return true;
            }
            try
            {
                return this.RegularExpression.IsMatch(input.Normalize());
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return false;
            }
        }
    }
}

