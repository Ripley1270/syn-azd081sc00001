﻿namespace Checkbox.Forms.Security
{
    using Checkbox.Forms;
    using Checkbox.Security;
    using System;

    public class FormFolderSecurityEditor : AccessControllablePDOSecurityEditor
    {
        public FormFolderSecurityEditor(FormFolder folder) : base(folder)
        {
        }

        protected override string RequiredEditorPermission
        {
            get
            {
                return "FormFolder.FullControl";
            }
        }
    }
}

