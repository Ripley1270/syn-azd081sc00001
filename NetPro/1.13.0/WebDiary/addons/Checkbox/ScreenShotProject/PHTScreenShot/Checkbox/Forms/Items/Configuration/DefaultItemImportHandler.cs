﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items.UI;
    using Checkbox.Globalization.Text;
    using System;
    using System.Data;

    public class DefaultItemImportHandler : IItemImportHandler
    {
        protected static ItemData CreateItemFromDataRow(DataRow row)
        {
            try
            {
                if (((row == null) || (row["ItemName"] == DBNull.Value)) || (((row["ItemDataClassName"] == DBNull.Value) || (row["ItemDataAssemblyName"] == DBNull.Value)) || (row["ItemID"] == DBNull.Value)))
                {
                    return null;
                }
                string str = (string) row["ItemName"];
                string str2 = (string) row["ItemDataClassName"];
                string str3 = (string) row["ItemDataAssemblyName"];
                int num = (int) row["ItemID"];
                ItemData data = new ItemConfigurationFactory().CreateItemData(str2 + ", " + str3);
                data.ItemTypeName = str;
                if (num > 0)
                {
                    data.ID = new int?(-1 * num);
                }
                else
                {
                    data.ID = new int?(num);
                }
                return data;
            }
            catch
            {
                return null;
            }
        }

        public virtual ItemData ImportItem(int itemID, DataSet ds, bool doNotNegateRelationalIDs)
        {
            if (ds.Tables.Contains("Items"))
            {
                DataRow[] rowArray = ds.Tables["Items"].Select("ItemID = " + itemID, null, DataViewRowState.CurrentRows);
                if (rowArray.Length > 0)
                {
                    ItemData itemData = CreateItemFromDataRow(rowArray[0]);
                    if (itemData != null)
                    {
                        itemData.Import(ds, doNotNegateRelationalIDs);
                        if (rowArray[0]["ItemName"] != DBNull.Value)
                        {
                            DataTable itemTypeList = ItemConfigurationManager.GetItemTypeList();
                            if (itemTypeList != null)
                            {
                                DataRow[] rowArray2 = itemTypeList.Select("ItemName = '" + ((string) rowArray[0]["ItemName"]) + "'", null, DataViewRowState.CurrentRows);
                                if ((rowArray2.Length > 0) && (rowArray2[0]["ItemTypeID"] != DBNull.Value))
                                {
                                    itemData.ItemTypeID = (int) rowArray2[0]["ItemTypeID"];
                                    SaveImportedItem(itemData, ds);
                                    return itemData;
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        protected static void SaveImportedItem(ItemData itemData, DataSet importData)
        {
            int oldItemID = itemData.ID.Value;
            itemData.Save();
            SaveImportedItemText(itemData, importData, oldItemID);
            SaveImportedItemAppearance(itemData, importData, oldItemID);
        }

        protected static void SaveImportedItemAppearance(ItemData itemData, DataSet importData, int oldItemID)
        {
            if (importData.Tables.Contains("AppearanceData") && importData.Tables["AppearanceData"].Columns.Contains(itemData.IdentityColumnName))
            {
                foreach (DataRow row in importData.Tables["AppearanceData"].Select(itemData.IdentityColumnName + " = " + oldItemID, null, DataViewRowState.CurrentRows))
                {
                    if ((row["AppearanceID"] != DBNull.Value) && (row["AppearanceCode"] != DBNull.Value))
                    {
                        int appearanceID = (int) row["AppearanceID"];
                        string appearanceCode = (string) row["AppearanceCode"];
                        AppearanceData data = AppearanceDataManager.GetAppearanceData(appearanceID, appearanceCode, importData);
                        if (data != null)
                        {
                            data.Save(itemData.ID.Value);
                        }
                    }
                }
            }
        }

        protected static void SaveImportedItemText(ItemData itemData, DataSet importData, int oldItemID)
        {
            ItemTextDecorator decorator = itemData.CreateTextDecorator(TextManager.DefaultLanguage);
            if (decorator != null)
            {
                decorator.ImportText(importData, oldItemID);
            }
        }
    }
}

