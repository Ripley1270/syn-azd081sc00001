﻿namespace Checkbox.Analytics.Items.Configuration
{
    using Checkbox.Analytics.Items;
    using Checkbox.Forms.Items;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class StatisticsItemData : AnalysisItemData
    {
        public StatisticsItemData()
        {
            this.ReportOption = StatisticsItemReportingOption.All;
        }

        protected override void Create(IDbTransaction transactionContext)
        {
            base.Create(transactionContext);
            this.UpSert(transactionContext);
        }

        protected override Item CreateItem()
        {
            return new StatisticsItem();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to load data for item with no id!");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_Statistics_Get");
            storedProcCommandWrapper.AddInParameter("ItemId", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName, AnalysisItemData.SourceItemsTableName, AnalysisItemData.ResponseTemplatesTableName });
            return dataSet;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            base.UseAliases = DbUtility.GetValueFromDataRow<bool>(data, "UseAliases", false);
            string str = DbUtility.GetValueFromDataRow<string>(data, "ReportOption", string.Empty);
            if (!string.IsNullOrEmpty(str))
            {
                try
                {
                    this.ReportOption = (StatisticsItemReportingOption) Enum.Parse(typeof(StatisticsItemReportingOption), str);
                }
                catch
                {
                    this.ReportOption = StatisticsItemReportingOption.All;
                }
            }
        }

        protected override void Update(IDbTransaction transactionContext)
        {
            base.Update(transactionContext);
            this.UpSert(transactionContext);
        }

        private void UpSert(IDbTransaction transactionContext)
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("ID not valid!");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_Statistics_UpSert");
            storedProcCommandWrapper.AddInParameter("ItemId", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ReportOption", DbType.String, this.ReportOption);
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            database.ExecuteNonQuery(storedProcCommandWrapper, transactionContext);
            this.UpdateDataTables(transactionContext);
        }

        public StatisticsItemReportingOption ReportOption { get; set; }
    }
}

