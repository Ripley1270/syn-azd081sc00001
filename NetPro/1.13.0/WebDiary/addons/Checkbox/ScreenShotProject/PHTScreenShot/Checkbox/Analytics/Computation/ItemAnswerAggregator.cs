﻿namespace Checkbox.Analytics.Computation
{
    using Checkbox.Analytics.Data;
    using Checkbox.Forms.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class ItemAnswerAggregator
    {
        private Dictionary<long, ItemAnswer> _answerDictionary;
        private List<string> _groupNames;
        private Dictionary<int, List<long>> _itemAnswerDictionary;
        private Dictionary<int, GroupedReportItemData> _itemDictionary;
        private Dictionary<string, List<int>> _itemGroups;
        private Dictionary<int, List<int>> _itemOptionDictionary;
        private Dictionary<int, Dictionary<long, int>> _itemResponses;
        private Dictionary<int, List<long>> _optionAnswerDictionary;
        private Dictionary<int, GroupedReportOptionData> _optionDictionary;
        private Dictionary<int, Dictionary<long, int>> _optionResponses;
        private Dictionary<long, List<long>> _responseAnswerDictionary;

        public ItemAnswerAggregator()
        {
            this.InitializeAnswerData();
        }

        private void AddAnswer(ItemAnswer answerData)
        {
            this.AnswerDictionary[answerData.AnswerId] = answerData;
            this.AddResponseAnswer(answerData.ResponseId, answerData.AnswerId);
            this.AddItemResponse(answerData.ItemId, answerData.OptionId, answerData.ResponseId);
            this.AddItemAnswer(answerData.ItemId, answerData.OptionId, answerData.AnswerId);
        }

        public virtual void AddAnswer(long answerId, long responseID, int itemID, int? optionID)
        {
            this.AddAnswer(answerId, responseID, itemID, optionID, null);
        }

        public virtual void AddAnswer(long answerId, long responseID, int itemID, string answer)
        {
            this.AddAnswer(answerId, responseID, itemID, null, answer);
        }

        public virtual void AddAnswer(long answerId, long responseID, int itemID, int? optionID, string answer)
        {
            this.AddAnswer(answerId, responseID, null, itemID, optionID, answer);
        }

        public virtual void AddAnswer(long answerId, long responseID, Guid? responseGuid, int itemID, int? optionID, string answer)
        {
            ItemAnswer answerData = new ItemAnswer {
                AnswerId = answerId,
                ResponseId = responseID,
                ResponseGuid = responseGuid,
                ItemId = itemID,
                OptionId = optionID,
                AnswerText = answer
            };
            this.AddAnswer(answerData);
        }

        public virtual void AddAnswerGroup(string groupName)
        {
            this.GroupNames.Add(groupName);
            this.ItemGroups[groupName] = new List<int>();
        }

        public virtual void AddItem(int itemID, string itemText, string itemType)
        {
            this.AddItem(itemID, itemText, itemType, null);
        }

        public virtual void AddItem(int itemID, string itemText, string itemType, string groupName)
        {
            GroupedReportItemData data = new GroupedReportItemData {
                ItemId = itemID,
                GroupName = groupName,
                Position = this.ItemDictionary.Count + 1,
                ItemType = itemType
            };
            data.SetText("[LANGUAGE]", itemText);
            this.ItemDictionary[itemID] = data;
        }

        private void AddItemAnswer(int itemId, int? optionId, long answerId)
        {
            if (!this.ItemAnswers.ContainsKey(itemId))
            {
                this.ItemAnswers[itemId] = new List<long>();
            }
            this.ItemAnswers[itemId].Add(answerId);
            if (optionId.HasValue)
            {
                if (!this.OptionAnswers.ContainsKey(optionId.Value))
                {
                    this.OptionAnswers[optionId.Value] = new List<long>();
                }
                this.OptionAnswers[optionId.Value].Add(answerId);
            }
        }

        public virtual void AddItemOption(int itemID, int optionID, string optionText)
        {
            this.AddItemOption(itemID, optionID, optionText, null, false);
        }

        public virtual void AddItemOption(int itemID, int optionID, string optionText, double? points, bool isOther)
        {
            int num;
            if (this.ItemOptionDictionary.ContainsKey(itemID))
            {
                num = this.ItemOptionDictionary[itemID].Count + 1;
            }
            else
            {
                this.ItemOptionDictionary[itemID] = new List<int>();
                num = 1;
            }
            GroupedReportOptionData data2 = new GroupedReportOptionData {
                ItemId = itemID,
                OptionId = optionID
            };
            double? nullable = points;
            data2.Points = nullable.HasValue ? nullable.GetValueOrDefault() : 0.0;
            data2.Position = num;
            data2.IsOther = isOther;
            GroupedReportOptionData data = data2;
            data.SetText("[LANGUAGE]", optionText);
            this.ItemOptionDictionary[itemID].Add(optionID);
            this.OptionDictionary[optionID] = data;
        }

        private void AddItemResponse(int itemId, int? optionId, long responseId)
        {
            if (!this.ItemResponses.ContainsKey(itemId))
            {
                this.ItemResponses[itemId] = new Dictionary<long, int>();
            }
            if (!this.ItemResponses[itemId].ContainsKey(responseId))
            {
                this.ItemResponses[itemId][responseId] = 1;
            }
            else
            {
                Dictionary<long, int> dictionary;
                long num;
                (dictionary = this.ItemResponses[itemId])[num = responseId] = dictionary[num] + 1;
            }
            if (optionId.HasValue)
            {
                if (!this.OptionResponses.ContainsKey(optionId.Value))
                {
                    this.OptionResponses[optionId.Value] = new Dictionary<long, int>();
                }
                if (!this.OptionResponses[optionId.Value].ContainsKey(responseId))
                {
                    this.OptionResponses[optionId.Value][responseId] = 1;
                }
                else
                {
                    Dictionary<long, int> dictionary2;
                    long num2;
                    (dictionary2 = this.OptionResponses[optionId.Value])[num2 = responseId] = dictionary2[num2] + 1;
                }
            }
        }

        private void AddResponseAnswer(long responseId, long answerId)
        {
            if (!this.ResponseAnswerDictionary.ContainsKey(responseId))
            {
                this.ResponseAnswerDictionary[responseId] = new List<long>();
            }
            this.ResponseAnswerDictionary[responseId].Add(answerId);
        }

        public DataTable GetAggregatedAnswerData()
        {
            if (this.AggregatedDataTable.Select(null, null, DataViewRowState.CurrentRows).Length == 0)
            {
                this.PopulateAggregatedAnswerDataProtected();
            }
            return this.AggregatedDataTable;
        }

        public virtual List<string> GetAnswerTexts(int itemID)
        {
            return (from ans in this.AnswerDictionary.Values
                where ans.ItemId == itemID
                select ans.AnswerText).Distinct<string>(StringComparer.CurrentCultureIgnoreCase).ToList<string>();
        }

        public virtual int GetGroupCount()
        {
            return this.GroupNames.Count;
        }

        public virtual string GetGroupName(int position)
        {
            if ((position > 0) && ((position - 1) < this.GroupNames.Count))
            {
                return this.GroupNames[position - 1];
            }
            return string.Empty;
        }

        public virtual string GetGroupNameForItem(int itemID)
        {
            if (!this.ItemDictionary.ContainsKey(itemID))
            {
                return string.Empty;
            }
            return (this.ItemDictionary[itemID].GroupName ?? string.Empty);
        }

        public virtual int? GetGroupPosition(string groupName)
        {
            int index = this.GroupNames.IndexOf(groupName);
            if (index > 0)
            {
                return new int?(index);
            }
            return null;
        }

        public virtual int GetItemAnswerCount(int itemId)
        {
            if (this.ItemAnswers.ContainsKey(itemId))
            {
                return this.ItemAnswers[itemId].Count;
            }
            return 0;
        }

        public virtual int GetItemAnswerCount(int itemId, string answerText)
        {
            return (from ans in this.AnswerDictionary.Values
                where (ans.ItemId == itemId) && answerText.Equals(ans.AnswerText, StringComparison.InvariantCultureIgnoreCase)
                select ans).Count<ItemAnswer>();
        }

        public virtual int GetItemID(int optionID)
        {
            if (this.OptionDictionary.ContainsKey(optionID))
            {
                return this.OptionDictionary[optionID].ItemId;
            }
            return -1;
        }

        public virtual List<int> GetItemIDs()
        {
            return new List<int>(this.ItemDictionary.Keys);
        }

        public virtual List<int> GetItemIDs(string groupName)
        {
            if (this.ItemGroups.ContainsKey(groupName))
            {
                return this.ItemGroups[groupName];
            }
            return new List<int>();
        }

        public virtual int GetItemPosition(int itemID)
        {
            if (this.ItemDictionary.ContainsKey(itemID))
            {
                return this.ItemDictionary[itemID].Position;
            }
            return -1;
        }

        public virtual string GetItemText(int itemID)
        {
            if (this.ItemDictionary.ContainsKey(itemID))
            {
                return this.ItemDictionary[itemID].GetText("[LANGUAGE]");
            }
            return string.Empty;
        }

        public virtual string GetItemType(int itemID)
        {
            if (this.ItemDictionary.ContainsKey(itemID))
            {
                return this.ItemDictionary[itemID].ItemType;
            }
            return string.Empty;
        }

        public virtual int GetOptionAnswerCount(int optionId)
        {
            if (this.OptionAnswers.ContainsKey(optionId))
            {
                return this.OptionAnswers[optionId].Count;
            }
            return 0;
        }

        public virtual List<int> GetOptionIDs(int itemID)
        {
            if (this.ItemOptionDictionary.ContainsKey(itemID))
            {
                return this.ItemOptionDictionary[itemID];
            }
            return new List<int>();
        }

        public virtual bool GetOptionIsOther(int optionID)
        {
            return (this.OptionDictionary.ContainsKey(optionID) && this.OptionDictionary[optionID].IsOther);
        }

        public virtual double GetOptionPoints(int optionID)
        {
            if (this.OptionDictionary.ContainsKey(optionID))
            {
                return this.OptionDictionary[optionID].Points;
            }
            return 0.0;
        }

        public virtual int GetOptionPosition(int optionID)
        {
            if (this.OptionDictionary.ContainsKey(optionID))
            {
                return this.OptionDictionary[optionID].Position;
            }
            return -1;
        }

        public virtual string GetOptionText(int optionID)
        {
            if (this.OptionDictionary.ContainsKey(optionID))
            {
                return this.OptionDictionary[optionID].GetText("[LANGUAGE]");
            }
            return string.Empty;
        }

        public virtual int GetResponseCount(int? itemId)
        {
            if (!itemId.HasValue)
            {
                return this.ResponseAnswerDictionary.Count;
            }
            if (this.ItemResponses.ContainsKey(itemId.Value))
            {
                return this.ItemResponses[itemId.Value].Count;
            }
            return 0;
        }

        public virtual List<long> GetResponseIDs()
        {
            return new List<long>(this.ResponseAnswerDictionary.Keys);
        }

        private void InitializeAnswerData()
        {
            this.ResponseAnswerDictionary.Clear();
            this.AnswerDictionary.Clear();
            this.ItemDictionary.Clear();
            this.ItemOptionDictionary.Clear();
            this.OptionDictionary.Clear();
            this.ItemGroups.Clear();
            this.GroupNames.Clear();
            this.OptionAnswers.Clear();
            this.OptionResponses.Clear();
            this.ItemAnswers.Clear();
            this.ItemResponses.Clear();
            DataTable table = new DataTable {
                TableName = AggregatedDataTableName
            };
            this.AggregatedDataTable = table;
            this.AggregatedDataTable.Columns.Add("ResponseID", typeof(long));
            this.AggregatedDataTable.Columns.Add("ResponseGuid", typeof(Guid));
            this.AggregatedDataTable.Columns.Add("GroupName", typeof(string));
            this.AggregatedDataTable.Columns.Add("GroupPosition", typeof(int));
            this.AggregatedDataTable.Columns.Add("ItemPosition", typeof(int));
            this.AggregatedDataTable.Columns.Add("OptionPosition", typeof(int));
            this.AggregatedDataTable.Columns.Add("ItemID", typeof(int));
            this.AggregatedDataTable.Columns.Add("OptionID", typeof(int));
            this.AggregatedDataTable.Columns.Add("AnswerText", typeof(string));
            this.AggregatedDataTable.Columns.Add("ItemText", typeof(string));
            this.AggregatedDataTable.Columns.Add("OptionText", typeof(string));
            this.AggregatedDataTable.Columns.Add("Points", typeof(double));
            this.AggregatedDataTable.Columns.Add("OptionIsOther", typeof(bool));
        }

        protected List<ItemAnswer> ListResponseAnswers(long responseId)
        {
            List<ItemAnswer> list = new List<ItemAnswer>();
            if (this.ResponseAnswerDictionary.ContainsKey(responseId))
            {
                List<long> list2 = this.ResponseAnswerDictionary[responseId];
                foreach (long num in list2)
                {
                    if (this.AnswerDictionary.ContainsKey(num))
                    {
                        list.Add(this.AnswerDictionary[num]);
                    }
                }
            }
            return list;
        }

        protected virtual void PopulateAggregatedAnswerDataProtected()
        {
            this.AggregatedDataTable.Clear();
            List<long> responseIDs = this.GetResponseIDs();
            responseIDs.Sort();
            foreach (long num in responseIDs)
            {
                List<long> list2 = this.ResponseAnswerDictionary[num];
                foreach (long num2 in list2)
                {
                    if (this.AnswerDictionary.ContainsKey(num2))
                    {
                        ItemAnswer answer = this.AnswerDictionary[num2];
                        object[] values = new object[] { num, DBNull.Value, DBNull.Value, DBNull.Value, this.GetItemPosition(answer.ItemId), DBNull.Value, answer.ItemId, DBNull.Value, answer.AnswerText, this.GetItemText(answer.ItemId), DBNull.Value, DBNull.Value, false };
                        if (answer.ResponseGuid.HasValue)
                        {
                            values[1] = answer.ResponseGuid.Value;
                        }
                        string groupName = this.GetGroupName(answer.ItemId);
                        int? groupPosition = this.GetGroupPosition(groupName);
                        if (!string.IsNullOrEmpty(groupName))
                        {
                            values[2] = groupName;
                        }
                        if (groupPosition.HasValue)
                        {
                            values[3] = groupPosition.Value;
                        }
                        if (answer.OptionId.HasValue)
                        {
                            values[5] = this.GetOptionPosition(answer.OptionId.Value);
                            values[7] = answer.OptionId.Value;
                            values[10] = this.GetOptionText(answer.OptionId.Value);
                            values[11] = this.GetOptionPoints(answer.OptionId.Value);
                            values[12] = this.GetOptionIsOther(answer.OptionId.Value);
                        }
                        this.AggregatedDataTable.Rows.Add(values);
                    }
                }
            }
        }

        protected virtual void UpdateAnswer(long answerId, string answer)
        {
            if (this.AnswerDictionary.ContainsKey(answerId))
            {
                this.AnswerDictionary[answerId].AnswerText = answer;
            }
        }

        protected DataTable AggregatedDataTable { get; private set; }

        private static string AggregatedDataTableName
        {
            get
            {
                return "AggregatedData";
            }
        }

        public Dictionary<long, ItemAnswer> AnswerDictionary
        {
            get
            {
                if (this._answerDictionary == null)
                {
                    this._answerDictionary = new Dictionary<long, ItemAnswer>();
                }
                return this._answerDictionary;
            }
        }

        protected List<string> GroupNames
        {
            get
            {
                if (this._groupNames == null)
                {
                    this._groupNames = new List<string>();
                }
                return this._groupNames;
            }
        }

        public Dictionary<int, List<long>> ItemAnswers
        {
            get
            {
                if (this._itemAnswerDictionary == null)
                {
                    this._itemAnswerDictionary = new Dictionary<int, List<long>>();
                }
                return this._itemAnswerDictionary;
            }
        }

        protected Dictionary<int, GroupedReportItemData> ItemDictionary
        {
            get
            {
                if (this._itemDictionary == null)
                {
                    this._itemDictionary = new Dictionary<int, GroupedReportItemData>();
                }
                return this._itemDictionary;
            }
        }

        public Dictionary<string, List<int>> ItemGroups
        {
            get
            {
                if (this._itemGroups == null)
                {
                    this._itemGroups = new Dictionary<string, List<int>>();
                }
                return this._itemGroups;
            }
        }

        public Dictionary<int, List<int>> ItemOptionDictionary
        {
            get
            {
                if (this._itemOptionDictionary == null)
                {
                    this._itemOptionDictionary = new Dictionary<int, List<int>>();
                }
                return this._itemOptionDictionary;
            }
        }

        public Dictionary<int, Dictionary<long, int>> ItemResponses
        {
            get
            {
                if (this._itemResponses == null)
                {
                    this._itemResponses = new Dictionary<int, Dictionary<long, int>>();
                }
                return this._itemResponses;
            }
        }

        public Dictionary<int, List<long>> OptionAnswers
        {
            get
            {
                if (this._optionAnswerDictionary == null)
                {
                    this._optionAnswerDictionary = new Dictionary<int, List<long>>();
                }
                return this._optionAnswerDictionary;
            }
        }

        protected Dictionary<int, GroupedReportOptionData> OptionDictionary
        {
            get
            {
                if (this._optionDictionary == null)
                {
                    this._optionDictionary = new Dictionary<int, GroupedReportOptionData>();
                }
                return this._optionDictionary;
            }
        }

        public Dictionary<int, Dictionary<long, int>> OptionResponses
        {
            get
            {
                if (this._optionResponses == null)
                {
                    this._optionResponses = new Dictionary<int, Dictionary<long, int>>();
                }
                return this._optionResponses;
            }
        }

        public Dictionary<long, List<long>> ResponseAnswerDictionary
        {
            get
            {
                if (this._responseAnswerDictionary == null)
                {
                    this._responseAnswerDictionary = new Dictionary<long, List<long>>();
                }
                return this._responseAnswerDictionary;
            }
        }

        protected class GroupedReportItemData : LightweightItemMetaData
        {
            public string GroupName { get; set; }

            public int Position { get; set; }
        }

        protected class GroupedReportOptionData : LightweightOptionMetaData
        {
            public string GroupName { get; set; }

            public int Position { get; set; }
        }
    }
}

