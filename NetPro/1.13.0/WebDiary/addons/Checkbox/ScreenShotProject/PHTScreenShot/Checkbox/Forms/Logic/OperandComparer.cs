﻿namespace Checkbox.Forms.Logic
{
    using System;

    [Serializable]
    public static class OperandComparer
    {
        public static bool Compare(Operand left, LogicalOperator operation, Operand right, RuleEventTrigger trigger)
        {
            if (left != null)
            {
                switch (operation)
                {
                    case LogicalOperator.Equal:
                        return (left.CompareTo(right, trigger) == 0);

                    case LogicalOperator.NotEqual:
                        return (left.CompareTo(right, trigger) != 0);

                    case LogicalOperator.GreaterThan:
                        return (left.CompareTo(right, trigger) > 0);

                    case LogicalOperator.GreaterThanEqual:
                        return (left.CompareTo(right, trigger) >= 0);

                    case LogicalOperator.LessThan:
                        return (left.CompareTo(right, trigger) < 0);

                    case LogicalOperator.LessThanEqual:
                        return (left.CompareTo(right, trigger) <= 0);

                    case LogicalOperator.Contains:
                        if (!left.GetValue(trigger).HasValue || !right.GetValue(trigger).HasValue)
                        {
                            return false;
                        }
                        return left.GetValue(trigger).Contains(right.GetValue(trigger));

                    case LogicalOperator.DoesNotContain:
                        return ((!left.GetValue(trigger).HasValue || !right.GetValue(trigger).HasValue) || !left.GetValue(trigger).Contains(right.GetValue(trigger)));

                    case LogicalOperator.Answered:
                        return left.GetValue(trigger).HasValue;

                    case LogicalOperator.NotAnswered:
                        return !left.GetValue(trigger).HasValue;

                    case LogicalOperator.IsNull:
                        return !left.GetValue(trigger).HasValue;

                    case LogicalOperator.IsNotNull:
                        return left.GetValue(trigger).HasValue;
                }
            }
            return false;
        }
    }
}

