﻿namespace Checkbox.Analytics.Computation
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class MatrixReportDataCalculator : FrequencyDataCalculator
    {
        private List<int> _ratingScaleItems = new List<int>();
        private List<int> _sumTotalItems = new List<int>();

        public void AddRatingScaleItem(int itemID)
        {
            if (!this._ratingScaleItems.Contains(itemID))
            {
                this._ratingScaleItems.Add(itemID);
            }
        }

        public void AddSumTotalItem(int itemID)
        {
            if (!this._sumTotalItems.Contains(itemID))
            {
                this._sumTotalItems.Add(itemID);
            }
        }

        //private DataTable CalculateRatingScaleAverages(ItemAnswerAggregator aggregator)
        //{
        //    DataTable aggregatedAnswerData = aggregator.GetAggregatedAnswerData();
        //    DataTable table2 = new DataTable();
        //    table2.Columns.Add("RatingScaleItemID", typeof(int));
        //    table2.Columns.Add("AverageScore", typeof(decimal));
        //    foreach (int num in this._ratingScaleItems)
        //    {
        //        decimal d = 0M;
        //        decimal num3 = 0M;
        //        foreach (DataRow row in aggregatedAnswerData.Select("ItemID = " + num + "AND OptionIsOther = 0", null, DataViewRowState.CurrentRows))
        //        {
        //            if (row["Points"] != DBNull.Value)
        //            {
        //                d = decimal.op_Increment(d);
        //                num3 += Convert.ToDecimal(row["Points"]);
        //            }
        //        }
        //        DataRow row2 = table2.NewRow();
        //        row2["RatingScaleItemID"] = num;
        //        if (d > 0M)
        //        {
        //            row2["AverageScore"] = num3 / d;
        //        }
        //        else
        //        {
        //            row2["AverageScore"] = 0;
        //        }
        //        table2.Rows.Add(row2);
        //    }
        //    table2.AcceptChanges();
        //    return table2;
        //}

        private DataTable CalculateRatingScaleAverages(ItemAnswerAggregator aggregator)
        {
            DataTable aggregatedAnswerData = aggregator.GetAggregatedAnswerData();
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("RatingScaleItemID", typeof(int));
            dataTable.Columns.Add("AverageScore", typeof(decimal));
            foreach (int _ratingScaleItem in this._ratingScaleItems)
            {
                decimal num = new decimal(0);
                decimal num1 = new decimal(0);
                DataRow[] dataRowArray = aggregatedAnswerData.Select(string.Concat("ItemID = ", _ratingScaleItem, "AND OptionIsOther = 0"), null, DataViewRowState.CurrentRows);
                DataRow[] dataRowArray1 = dataRowArray;
                for (int i = 0; i < (int)dataRowArray1.Length; i++)
                {
                    DataRow dataRow = dataRowArray1[i];
                    if (dataRow["Points"] != DBNull.Value)
                    {
                        num = num++;
                        num1 = num1 + Convert.ToDecimal(dataRow["Points"]);
                    }
                }
                DataRow dataRow1 = dataTable.NewRow();
                dataRow1["RatingScaleItemID"] = _ratingScaleItem;
                if (num <= new decimal(0))
                {
                    dataRow1["AverageScore"] = 0;
                }
                else
                {
                    dataRow1["AverageScore"] = num1 / num;
                }
                dataTable.Rows.Add(dataRow1);
            }
            dataTable.AcceptChanges();
            return dataTable;
        }

        //private DataTable CalculateSumTotalAverages(ItemAnswerAggregator aggregator)
        //{
        //    DataTable aggregatedAnswerData = aggregator.GetAggregatedAnswerData();
        //    DataTable table2 = new DataTable();
        //    table2.Columns.Add("SumTotalItemID", typeof(int));
        //    table2.Columns.Add("AverageValue", typeof(decimal));
        //    foreach (int num in this._sumTotalItems)
        //    {
        //        decimal d = 0M;
        //        decimal num3 = 0M;
        //        foreach (DataRow row in aggregatedAnswerData.Select("ItemID = " + num, null, DataViewRowState.CurrentRows))
        //        {
        //            if (row["AnswerText"] != DBNull.Value)
        //            {
        //                try
        //                {
        //                    num3 += Convert.ToDecimal(row["AnswerText"]);
        //                    d = decimal.op_Increment(d);
        //                }
        //                catch (Exception)
        //                {
        //                }
        //            }
        //        }
        //        DataRow row2 = table2.NewRow();
        //        row2["SumTotalItemID"] = num;
        //        if (d > 0M)
        //        {
        //            row2["AverageValue"] = num3 / d;
        //        }
        //        else
        //        {
        //            row2["AverageValue"] = 0;
        //        }
        //        table2.Rows.Add(row2);
        //    }
        //    table2.AcceptChanges();
        //    return table2;
        //}
        private DataTable CalculateSumTotalAverages(ItemAnswerAggregator aggregator)
        {
            DataTable aggregatedAnswerData = aggregator.GetAggregatedAnswerData();
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("SumTotalItemID", typeof(int));
            dataTable.Columns.Add("AverageValue", typeof(decimal));
            foreach (int _sumTotalItem in this._sumTotalItems)
            {
                decimal num = new decimal(0);
                decimal num1 = new decimal(0);
                DataRow[] dataRowArray = aggregatedAnswerData.Select(string.Concat("ItemID = ", _sumTotalItem), null, DataViewRowState.CurrentRows);
                DataRow[] dataRowArray1 = dataRowArray;
                for (int i = 0; i < (int)dataRowArray1.Length; i++)
                {
                    DataRow dataRow = dataRowArray1[i];
                    if (dataRow["AnswerText"] != DBNull.Value)
                    {
                        try
                        {
                            num1 = num1 + Convert.ToDecimal(dataRow["AnswerText"]);
                            num = num++;
                        }
                        catch (Exception exception)
                        {
                        }
                    }
                }
                DataRow dataRow1 = dataTable.NewRow();
                dataRow1["SumTotalItemID"] = _sumTotalItem;
                if (num <= new decimal(0))
                {
                    dataRow1["AverageValue"] = 0;
                }
                else
                {
                    dataRow1["AverageValue"] = num1 / num;
                }
                dataTable.Rows.Add(dataRow1);
            }
            dataTable.AcceptChanges();
            return dataTable;
        }

        public override DataSet GetData(ItemAnswerAggregator aggregator)
        {
            DataSet data = base.GetData(aggregator);
            data.Merge(this.CalculateRatingScaleAverages(aggregator));
            data.Merge(this.CalculateSumTotalAverages(aggregator));
            data.Tables[2].TableName = this.RatingScaleAveragesTableName;
            data.Tables[3].TableName = this.SumTotalAveragesTableName;
            return data;
        }

        public string RatingScaleAveragesTableName
        {
            get
            {
                return "RatingScaleItemAverages";
            }
        }

        public string SumTotalAveragesTableName
        {
            get
            {
                return "SumTotalItemAverages";
            }
        }
    }
}

