﻿namespace Checkbox.Forms.Data
{
    using Checkbox.Common;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LightweightOptionMetaData
    {
        private Dictionary<string, string> _textDictionary;

        public string GetText(string languageCode)
        {
            if (this.TextDictionary.ContainsKey(languageCode))
            {
                return this.TextDictionary[languageCode];
            }
            return string.Empty;
        }

        public string GetText(bool preferAlias, string languageCode)
        {
            if (preferAlias && Utilities.IsNotNullOrEmpty(this.Alias))
            {
                return this.Alias;
            }
            return this.GetText(languageCode);
        }

        public void SetText(string languageCode, string text)
        {
            this.TextDictionary[languageCode] = text;
        }

        public string Alias { get; set; }

        public bool IsOther { get; set; }

        public int ItemId { get; set; }

        public int OptionId { get; set; }

        public double Points { get; set; }

        public int Position { get; set; }

        private Dictionary<string, string> TextDictionary
        {
            get
            {
                if (this._textDictionary == null)
                {
                    this._textDictionary = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
                }
                return this._textDictionary;
            }
        }
    }
}

