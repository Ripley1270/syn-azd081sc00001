﻿using Checkbox.Analytics.Filters.Configuration;
using Checkbox.Analytics.Items;
using Checkbox.Common;
using Checkbox.Forms.Data;
using Checkbox.Forms.Items;
using Checkbox.Forms.Items.Configuration;
using Checkbox.Forms.Logic;
using Checkbox.Forms.Logic.Configuration;
using Checkbox.Forms.Piping.Tokens;
using Checkbox.Forms.Security;
using Checkbox.Globalization.Text;
using Checkbox.Progress;
using Prezza.Framework.Common;
using Prezza.Framework.Data;
using Prezza.Framework.ExceptionHandling;
using Prezza.Framework.Security;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Checkbox.Forms
{
    [Serializable]
    public class ResponseTemplate : Template
    {
        private string _editLanguage;
        private ResponseTemplateFilterCollection _filters;
        private Dictionary<int, Coordinate> _matrixItemCoordinate;
        private Dictionary<int, int> _matrixItemParent;
        private Dictionary<int, int> _newItemIDs;
        private Dictionary<int, int> _newOptionIDs;
        private int _reportSecurityType;
        private readonly Dictionary<string, ItemToken> _responsePipes;
        private int _securityType;
        private ArrayList _supportedLanguages;
        private PHTScreenShotSession _screenShotSession;

        internal ResponseTemplate()
            : this(string.Empty)
        {
        }

        internal ResponseTemplate(string createdBy)
            : base(new string[] { "Form.Administer", "Form.Edit", "Form.Fill", "Analysis.ViewResponses", "Analysis.Analyze" }, new string[] { "Form.Administer", "Form.Create", "Form.Delete", "Form.Edit", "Form.Fill", "Analysis.Create", "Analysis.Responses.View", "Analysis.Responses.Export", "Analysis.Responses.Edit", "Analysis.ManageFilters" })
        {
            this.CreatedBy = createdBy;
            this._responsePipes = new Dictionary<string, ItemToken>();
            this.SetDefaultValues();
        }

        private void AddCommonSprocParams(DBCommandWrapper command)
        {
            if (command != null)
            {
                command.AddInParameter("TemplateName", DbType.String, this.Name);
                command.AddInParameter("NameTextID", DbType.String, this.NameTextID);
                command.AddInParameter("TitleTextID", DbType.String, this.TitleTextID);
                command.AddInParameter("DescriptionTextID", DbType.String, this.DescriptionTextID);
                command.AddInParameter("IsActive", DbType.Boolean, this.IsActive);
                command.AddInParameter("ActivationStart", DbType.DateTime, this.ActivationStartDate);
                command.AddInParameter("ActivationEnd", DbType.DateTime, this.ActivationEndDate);
                command.AddInParameter("MaxTotalResponses", DbType.Int32, this.MaxTotalResponses);
                command.AddInParameter("MaxResponsesPerUser", DbType.Int32, this.MaxResponsesPerUser);
                command.AddInParameter("AllowContinue", DbType.Boolean, this.AllowContinue);
                command.AddInParameter("ShowSaveAndQuit", DbType.Boolean, this.ShowSaveAndQuit);
                command.AddInParameter("AllowEdit", DbType.Boolean, this.AllowEdit);
                command.AddInParameter("AnonymizeResponses", DbType.Boolean, this.AnonymizeResponses);
                command.AddInParameter("DisableBackButton", DbType.Boolean, this.DisableBackButton);
                command.AddInParameter("RandomizeItemsInPages", DbType.Boolean, this.RandomizeItemsInPages);
                command.AddInParameter("ShowValidationMessage", DbType.Boolean, this.ShowValidationMessage);
                command.AddInParameter("RequiredFieldsAlert", DbType.Boolean, this.ShowValidationErrorAlert);
                command.AddInParameter("MobileCompatible", DbType.Boolean, this.MobileCompatible);
                command.AddInParameter("ButtonContinueTextID", DbType.String, this.ContinueButtonTextID);
                command.AddInParameter("ButtonBackTextID", DbType.String, this.BackButtonTextID);
                command.AddInParameter("GuestPassword", DbType.String, this.Password);
                command.AddInParameter("StyleTemplateID", DbType.Int32, this.StyleTemplateID);
                command.AddInParameter("ShowPageNumbers", DbType.Boolean, this.ShowPageNumbers);
                command.AddInParameter("EnableDynamicPageNumbers", DbType.Boolean, this.EnableDynamicPageNumbers);
                command.AddInParameter("EnableDynamicItemNumbers", DbType.Boolean, this.EnableDynamicItemNumbers);
                command.AddInParameter("ShowProgressBar", DbType.Boolean, this.ShowProgressBar);
                command.AddInParameter("ShowItemNumbers", DbType.Boolean, this.ShowItemNumbers);
                command.AddInParameter("ShowTitle", DbType.Boolean, this.ShowTitle);
                command.AddInParameter("CompletionType", DbType.Int32, this.CompletionType);
                command.AddInParameter("AllowSurveyEditWhileActive", DbType.Boolean, this.AllowSurveyEditWhileActive);
                command.AddInParameter("IsPoll", DbType.Boolean, this.IsPoll);
                command.AddInParameter("ChartStyleID", DbType.Int32, this.ChartStyleID);
                command.AddInParameter("Height", DbType.Int32, this.Height);
                command.AddInParameter("Width", DbType.Int32, this.Width);
                command.AddInParameter("BorderWidth", DbType.Int32, this.BorderWidth);
                command.AddInParameter("BorderColor", DbType.String, this.BorderColor);
                command.AddInParameter("BorderStyle", DbType.String, this.BorderStyle);
                string str = string.Empty;
                foreach (string str2 in this._supportedLanguages)
                {
                    if (str == string.Empty)
                    {
                        str = str2;
                    }
                    else
                    {
                        str = str + ";" + str2;
                    }
                }
                command.AddInParameter("SupportedLanguages", DbType.String, str);
                command.AddInParameter("DefaultLanguage", DbType.String, this.DefaultLanguage);
                command.AddInParameter("LanguageSource", DbType.String, this.LanguageSource);
                command.AddInParameter("LanguageSourceToken", DbType.String, this.LanguageSourceToken);
                command.AddInParameter("SecurityType", DbType.Int32, this._securityType);
                command.AddInParameter("ReportSecurityType", DbType.Int32, this._reportSecurityType);
                command.AddInParameter("EnableScoring", DbType.Boolean, this.EnableScoring);
            }
        }

        public void AddFilter(FilterData filter)
        {
            this.FilterCollection.AddFilter(filter);
        }

        public void AddResponsePipe(string name, int itemID)
        {
            this.AddResponsePipe(name, itemID, null);
        }

        private void AddResponsePipe(string name, int itemID, IDbTransaction t)
        {
            if ((!string.IsNullOrEmpty(name) && !this._responsePipes.ContainsKey(name)) && (itemID > 0))
            {
                this._responsePipes[name] = new ItemToken(name, itemID);
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_AddResponsePipe");
                storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, base.ID.Value);
                storedProcCommandWrapper.AddInParameter("PipeName", DbType.String, name);
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, itemID);
                if (t == null)
                {
                    database.ExecuteNonQuery(storedProcCommandWrapper);
                }
                else
                {
                    database.ExecuteNonQuery(storedProcCommandWrapper, t);
                }
            }
        }

        public void AddSupportedLanguage(string language)
        {
            if (!this._supportedLanguages.Contains(language))
            {
                this._supportedLanguages.Add(language);
            }
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (!base.ID.HasValue || (base.ID.Value <= 0))
            {
                throw new Exception("Unable to create response template with no DataID.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_Insert");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, base.ID);
            this.AddCommonSprocParams(storedProcCommandWrapper);
            this.GUID = Guid.NewGuid();
            storedProcCommandWrapper.AddInParameter("GUID", DbType.Guid, this.GUID);
            storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, this.CreatedBy);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateResponsePipes(t);
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            base.CreateDataRelationsInternal(ds);
            this.CreateRuleDataService().CreateDataRelations(ds);
            string name = "ResponsePipes_" + DataTableName;
            if (ds.Tables.Contains("ResponsePipes") && !ds.Relations.Contains(name))
            {
                DataRelation relation = new DataRelation(name, ds.Tables[DataTableName].Columns["TemplateID"], ds.Tables["ResponsePipes"].Columns[this.IdentityColumnName]);
                ds.Relations.Add(relation);
            }
        }

        public override Policy CreatePolicy(string[] permissions)
        {
            return new FormPolicy(permissions);
        }

        public Response CreateResponse(string languageCode)
        {
            Response response = new Response
            {
                ResponseTemplateID = base.ID.Value,
                ResponseTemplateGuid = this.GUID,
                AnonymizeResponses = this.AnonymizeResponses
            };
            response.RulesEngine = new RulesEngine(base._templateData, response);
            foreach (TemplatePage page in this.TemplatePages)
            {
                foreach (ItemData data in page.Items)
                {
                    Item item = data.CreateItem(languageCode);
                    response.Items[data.ID.Value] = item;
                    if (item is ResponseItem)
                    {
                        ((ResponseItem)item).Response = response;
                    }
                    else if (item is AnalysisItem)
                    {
                        ((AnalysisItem)item).RunMode = true;
                    }
                }
            }
            ResponseTemplateManager.CacheResponseTemplate(this);
            for (int i = 0; i < this.TemplatePages.Count; i++)
            {
                TemplatePage page2 = this.TemplatePages[i];
                bool randomize = this.RandomizeItemsInPages && (i < (this.TemplatePages.Count - 1));
                ResponsePage page3 = new ResponsePage(page2.Identity.Value, page2.Position, randomize, page2.LayoutTemplateID);
                foreach (int num2 in this.GetPageItemDataIDs(page2.Identity.Value))
                {
                    page3.AddItemID(num2);
                }
                page3.Parent = response;
                response.Pages.Add(page3);
            }
            foreach (ItemToken token in this._responsePipes.Values)
            {
                response.ResponsePipes.Add(token);
            }
            response.AllowBack = !this.DisableBackButton;
            response.PreviousButtonTextID = this.BackButtonTextID;
            response.NextButtonTextID = this.ContinueButtonTextID;
            response.SaveProgressButtonTextID = this.SaveProgressButtonTextID;
            response.FinishButtonTextID = this.FinishButtonTextID;
            response.TitleTextID = this.TitleTextID;
            response.DescriptionTextID = this.DescriptionTextID;
            response.TemplateName = this.Name;
            response.ShowItemNumbers = this.ShowItemNumbers;
            response.UseDynamicItemNumbers = this.EnableDynamicItemNumbers;
            response.UseDynamicPageNumbers = this.EnableDynamicPageNumbers;
            response.ScoringEnabled = this.EnableScoring;
            return response;
        }

        public RuleDataService CreateRuleDataService()
        {
            return new RuleDataService(this, base._templateData);
        }

        public void DeleteFilter(FilterData filter)
        {
            this.FilterCollection.DeleteFilter(filter);
        }

        public void DeleteFilter(int filterID)
        {
            this.FilterCollection.DeleteFilter(filterID);
        }

        public override void DeleteItemFromPage(TemplatePage page, ItemData item)
        {
            foreach (ItemToken token in this.ResponsePipes)
            {
                if (item.ID.HasValue && (token.ItemID == item.ID.Value))
                {
                    this.DeleteResponsePipe(token.TokenName);
                }
            }
            base.DeleteItemFromPage(page, item);
        }

        public override void DeletePage(TemplatePage page)
        {
            RuleDataService service = this.CreateRuleDataService();
            service.DeleteRules(page);
            service.DeleteSubscriberExpressions(page);
            service.DeletePageTargetExpressions(page);
            base.DeletePage(page);
        }

        public void DeleteResponsePipe(string name)
        {
            this.DeleteResponsePipe(name, null);
        }

        private void DeleteResponsePipe(string name, IDbTransaction t)
        {
            if ((name != null) && this._responsePipes.ContainsKey(name))
            {
                this._responsePipes.Remove(name);
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_DeleteResponsePipe");
                storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, base.ID.Value);
                storedProcCommandWrapper.AddInParameter("PipeName", DbType.String, name);
                if (t == null)
                {
                    database.ExecuteNonQuery(storedProcCommandWrapper);
                }
                else
                {
                    database.ExecuteNonQuery(storedProcCommandWrapper, t);
                }
            }
        }

        public List<LightweightItemMetaData> GetAnswerableQuestions(int? pageNumber, bool skipFreshnessCheck)
        {
            List<LightweightItemMetaData> list = new List<LightweightItemMetaData>();
            skipFreshnessCheck = skipFreshnessCheck || !ResponseTemplateManager.CheckTemplateUpdated(base.ID.Value, base.LastModified);
            foreach (TemplatePage page in this.TemplatePages)
            {
                if (pageNumber.HasValue)
                {
                    int? nullable2 = pageNumber;
                    int position = page.Position;
                    if (!((nullable2.GetValueOrDefault() > position) && nullable2.HasValue))
                    {
                        continue;
                    }
                }
                List<int> itemIds = page.ItemIds;
                for (int i = 0; i < itemIds.Count; i++)
                {
                    LightweightItemMetaData itemData = SurveyMetaDataProxy.GetItemData(itemIds[i], skipFreshnessCheck);
                    if (itemData != null)
                    {
                        itemData.PagePosition = page.Position;
                        itemData.ItemPosition = i + 1;
                        SurveyMetaDataProxy.AddItemToCache(itemData);
                        if (itemData.IsAnswerable && (itemData.Children.Count == 0))
                        {
                            list.Add(itemData);
                        }
                        if (itemData.Children.Count > 0)
                        {
                            foreach (int num2 in itemData.Children)
                            {
                                LightweightItemMetaData item = SurveyMetaDataProxy.GetItemData(num2, true);
                                if (item != null)
                                {
                                    item.PagePosition = itemData.PagePosition;
                                    item.ItemPosition = itemData.ItemPosition;
                                    if (item.IsAnswerable)
                                    {
                                        list.Add(item);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return list;
        }

        private static List<int> GetChildItemOptionIDs(int itemID, DataTable optionsTable)
        {
            List<int> list = new List<int>();
            if (optionsTable != null)
            {
                DataRow[] rowArray = optionsTable.Select("ItemID = " + itemID, "Position ASC", DataViewRowState.CurrentRows);
                if (rowArray.Length == 0)
                {
                    rowArray = optionsTable.Select("ItemID = " + (-1 * itemID), "Position ASC", DataViewRowState.CurrentRows);
                }
                foreach (DataRow row in rowArray)
                {
                    if (row["OptionID"] != DBNull.Value)
                    {
                        list.Add((int)row["OptionID"]);
                    }
                }
            }
            return list;
        }

        private static List<ItemData> GetChildItems(ItemData parentItemData)
        {
            List<ItemData> list = new List<ItemData>();
            if (parentItemData is ICompositeItemData)
            {
                foreach (ItemData data in ((ICompositeItemData)parentItemData).GetChildItemDatas())
                {
                    list.Add(data);
                    list.AddRange(GetChildItems(data));
                }
            }
            return list;
        }

        public override DataSet GetConfigurationDataSet()
        {
            ArrayList list = new ArrayList();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_GetTableNames");
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        list.Add(reader["TableName"]);
                    }
                }
                catch (Exception exception)
                {
                    if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                    {
                        throw;
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            string calledStoredProcedure = string.Empty;
            this._screenShotSession = new PHTScreenShotSession();
            if (this._screenShotSession.ScreenShotMode)
                calledStoredProcedure = "PHTScreenShotRTGetResponseTemplate";
            else
                calledStoredProcedure = "ckbx_RT_GetResponseTemplate";

            DataSet dataSet = new DataSet();
            //DBCommandWrapper command = database.GetStoredProcCommandWrapper("ckbx_RT_GetResponseTemplate");
            DBCommandWrapper command = database.GetStoredProcCommandWrapper(calledStoredProcedure);
            command.AddInParameter("ResponseTemplateID", DbType.Int32, base.ID);
            database.LoadDataSet(command, dataSet, (string[])list.ToArray(typeof(string)));
            if (((dataSet.Tables.Count == 0) || !dataSet.Tables.Contains(DataTableName)) || (dataSet.Tables[DataTableName].Rows.Count <= 0))
            {
                throw new TemplateDoesNotExist(base.ID.Value);
            }
            this.SetConfigurationDataSetColumnMappings(dataSet);
            base.MergeTextData(dataSet, TextManager.GetTextData(this.NameTextID), "name");
            base.MergeTextData(dataSet, TextManager.GetTextData(this.DescriptionTextID), "description");
            base.MergeTextData(dataSet, TextManager.GetTextData(this.TitleTextID), "title");
            base.MergeTextData(dataSet, TextManager.GetTextData(this.BackButtonTextID), "back");
            base.MergeTextData(dataSet, TextManager.GetTextData(this.ContinueButtonTextID), "continue");
            base.MergeTextData(dataSet, TextManager.GetTextData(this.FinishButtonTextID), "finish");
            base.MergeTextData(dataSet, TextManager.GetTextData(this.SaveProgressButtonTextID), "saveProgress");
            if (dataSet.Tables.Contains(this.TemplateTextTableName) && !dataSet.Tables[this.TemplateTextTableName].Columns.Contains("ComputedTextID"))
            {
                dataSet.Tables[this.TemplateTextTableName].Columns.Add("ComputedTextID", typeof(string));
                dataSet.Tables[this.TemplateTextTableName].Columns["ComputedTextID"].Expression = "Iif(TemplateID IS NULL OR TextIDSuffix IS NULL, '', '/" + this.TextIDPrefix + "/' + TemplateID + '/' + TextIDSuffix)";
            }
            return dataSet;
        }

        protected override DBCommandWrapper GetDeletePageCommand()
        {
            DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_RT_DeletePage");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("PageID", DbType.Int32, "PageID", DataRowVersion.Current);
            return storedProcCommandWrapper;
        }

        public override SecurityEditor GetEditor()
        {
            return new FormSecurityEditor(this);
        }

        public ReadOnlyCollection<FilterData> GetFilterDataObjects()
        {
            return new ReadOnlyCollection<FilterData>(this.FilterCollection.GetFilterDataObjects());
        }

        private static Dictionary<int, List<int>> GetItemOptionIDs(int itemID, DataSet ds)
        {
            Dictionary<int, List<int>> dictionary = new Dictionary<int, List<int>>();
            dictionary[itemID] = GetChildItemOptionIDs(itemID, ds.Tables["ItemOptions"]);
            if (ds.Tables.Contains("ItemPositions"))
            {
                foreach (DataRow row in ds.Tables["ItemPositions"].Select("MatrixID = " + itemID, null, DataViewRowState.CurrentRows))
                {
                    if (row["ItemID"] != DBNull.Value)
                    {
                        dictionary[Math.Abs((int)row["ItemID"])] = GetChildItemOptionIDs((int)row["ItemID"], ds.Tables["MatrixItemOptions"]);
                    }
                }
            }
            return dictionary;
        }

        //public LightweightItemMetaData GetLightweightItem(int itemId, bool skipFreshnessCheck)
        //{
        //    TemplatePage page = this.TemplatePages.FirstOrDefault<TemplatePage>(page => page.ItemIds.Contains(itemId));

        //    if (page == null)
        //    {
        //        return null;
        //    }
        //    LightweightItemMetaData itemData = SurveyMetaDataProxy.GetItemData(itemId, false);
        //    itemData.PagePosition = page.Position;
        //    itemData.ItemPosition = page.ItemIds.IndexOf(itemId) + 1;
        //    return itemData;
        //}

        public LightweightItemMetaData GetLightweightItem(int itemId, bool skipFreshnessCheck)
        {
            TemplatePage templatePage = this.TemplatePages.FirstOrDefault<TemplatePage>((TemplatePage page) => page.ItemIds.Contains(itemId));
            if (templatePage != null)
            {
                LightweightItemMetaData itemData = SurveyMetaDataProxy.GetItemData(itemId, false);
                itemData.PagePosition = templatePage.Position;
                itemData.ItemPosition = templatePage.ItemIds.IndexOf(itemId) + 1;
                return itemData;
            }
            else
            {
                return null;
            }
        }

        private static int? GetParentMatrixID(int itemID, DataSet ds)
        {
            if (ds.Tables.Contains("ItemPositions"))
            {
                DataRow[] rowArray = ds.Tables["ItemPositions"].Select("ItemID = " + itemID, null, DataViewRowState.CurrentRows);
                if ((rowArray.Length > 0) && (rowArray[0]["MatrixID"] != DBNull.Value))
                {
                    return new int?((int)rowArray[0]["MatrixID"]);
                }
            }
            return null;
        }

        private static Coordinate GetParentMatrixPosition(int itemID, DataSet ds)
        {
            if (ds.Tables.Contains("ItemPositions"))
            {
                DataRow[] rowArray = ds.Tables["ItemPositions"].Select("ItemID = " + itemID, null, DataViewRowState.CurrentRows);
                if (((rowArray.Length > 0) && (rowArray[0]["Row"] != DBNull.Value)) && (rowArray[0]["Column"] != DBNull.Value))
                {
                    return new Coordinate((int)rowArray[0]["Row"], (int)rowArray[0]["Column"]);
                }
            }
            return null;
        }

        public override void Import(DataSet ds)
        {
            this.Import(ds, string.Empty, string.Empty);
        }

        public void Import(DataSet ds, string progressKey, string progressLanguage)
        {
            this._matrixItemCoordinate = new Dictionary<int, Coordinate>();
            this._matrixItemParent = new Dictionary<int, int>();
            this._newItemIDs = new Dictionary<int, int>();
            this._newOptionIDs = new Dictionary<int, int>();
            this.LoadResponsePipes(ds, progressKey, progressLanguage, 5, 5);
            foreach (ItemToken token in this._responsePipes.Values)
            {
                int? parentMatrixID = GetParentMatrixID(token.ItemID, ds);
                Coordinate parentMatrixPosition = GetParentMatrixPosition(token.ItemID, ds);
                if (parentMatrixID.HasValue && (parentMatrixPosition != null))
                {
                    this._matrixItemParent[token.ItemID] = parentMatrixID.Value;
                    this._matrixItemCoordinate[token.ItemID] = parentMatrixPosition;
                }
            }
            this.StoreMatrixParentData(ItemRulesTableName, ds, "ItemID");
            this.StoreMatrixParentData(ItemOperandTableName, ds, "ItemID");
            this.StoreMatrixParentData(ValueOperandTableName, ds, "ItemID");
            base.Import(ds, progressKey, progressLanguage);
            ProgressProvider.SetProgress(progressKey, TextManager.GetText("/controlText/templateImport/importingRules", progressLanguage), 90);
            this.ImportTemplateRules(ds);
            this.UpdateImportedItemIDs();
            ProgressProvider.SetProgress(progressKey, TextManager.GetText("/controlText/templateImport/savingRules", progressLanguage), 0x5f);
            this.CreateRuleDataService().Save();
        }

        protected override ItemData ImportItem(int itemID, DataSet ds)
        {
            Dictionary<int, List<int>> itemOptionIDs = GetItemOptionIDs(itemID, ds);
            ItemData itemData = base.ImportItem(itemID, ds);
            if (itemData != null)
            {
                if (itemData is MatrixItemData)
                {
                    foreach (int num in this._matrixItemParent.Keys)
                    {
                        if (this._matrixItemParent[num] == Math.Abs(itemID))
                        {
                            Coordinate coordinate = this._matrixItemCoordinate[num];
                            ItemData itemAt = ((MatrixItemData)itemData).GetItemAt(coordinate.X, coordinate.Y);
                            if (itemAt != null)
                            {
                                this.UpdateTokenSourceIDs(num, itemAt.ID.Value);
                                this._newItemIDs[Math.Abs(num)] = itemAt.ID.Value;
                                if (itemOptionIDs.ContainsKey(num))
                                {
                                    this.StoreNewItemOptionIDs(itemAt, itemOptionIDs[num]);
                                }
                            }
                        }
                    }
                    this._newItemIDs[Math.Abs(itemID)] = itemData.ID.Value;
                    return itemData;
                }
                this.UpdateTokenSourceIDs(itemID, itemData.ID.Value);
                this._newItemIDs[Math.Abs(itemID)] = itemData.ID.Value;
                if (itemOptionIDs.ContainsKey(itemID))
                {
                    this.StoreNewItemOptionIDs(itemData, itemOptionIDs[itemID]);
                }
            }
            return itemData;
        }

        private static void ImportTableData(DataSet ds, string sourceTableName, DataTable destinationTable)
        {
            ImportTableData(ds, sourceTableName, destinationTable, null);
        }

        private static void ImportTableData(DataSet ds, string sourceTableName, DataTable destinationTable, string sourceSortExpression)
        {
            if (((ds != null) && ds.Tables.Contains(sourceTableName)) && (destinationTable != null))
            {
                DataTable table = ds.Tables[sourceTableName];
                destinationTable.Clear();
                foreach (DataRow row in table.Select(null, sourceSortExpression, DataViewRowState.CurrentRows))
                {
                    DataRow row2 = destinationTable.NewRow();
                    foreach (DataColumn column in destinationTable.Columns)
                    {
                        if (table.Columns.Contains(column.ColumnName))
                        {
                            row2[column.ColumnName] = row[column.ColumnName];
                        }
                    }
                    destinationTable.Rows.Add(row2);
                }
            }
        }

        protected override void ImportTemplateData(DataSet data)
        {
            DataRow responseTemplateDR = data.Tables[this.TemplateDataTableName].Rows[0];
            this.LoadTemplateData(responseTemplateDR, true);
        }

        protected virtual void ImportTemplateRules(DataSet ds)
        {
            ImportTableData(ds, OperandTableName, this.OperandTable);
            ImportTableData(ds, ItemOperandTableName, this.ItemOperandTable);
            ImportTableData(ds, ValueOperandTableName, this.ValueOperandTable);
            ImportTableData(ds, ProfileOperandTableName, this.ProfileOperandTable);
            ImportTableData(ds, ResponseOperandTableName, this.ResponseOperandTable);
            ImportTableData(ds, ExpressionTableName, this.ExpressionTable, "Depth ASC");
            ImportTableData(ds, RuleTableName, this.RuleTable, "RuleId DESC");
            ImportTableData(ds, ActionTableName, this.ActionTable);
            ImportTableData(ds, BranchActionTableName, this.BranchActionTable);
            ImportTableData(ds, RuleActionsTableName, this.RuleActionsTable);
            ImportTableData(ds, PageRulesTableName, this.PageRulesTable);
            ImportTableData(ds, ItemRulesTableName, this.ItemRulesTable);
        }

        internal void InitializeAccess(Policy defaultPolicy, AccessControlList acl)
        {
            if (base.ID.HasValue && (base.ID > 0))
            {
                throw new Exception("Access can only be initialized for a new response template.");
            }
            ArgumentValidation.CheckExpectedType(defaultPolicy, typeof(FormPolicy));
            base.SetAccess(defaultPolicy, acl);
        }

        protected override void InitializeData()
        {
            base.InitializeData();
            this.InitializeRuleData();
        }

        protected virtual void InitializeRuleData()
        {
            DataTable table = new DataTable();
            table.Columns.Add("RuleID", typeof(int));
            table.Columns.Add("ExpressionID", typeof(int));
            table.TableName = RuleTableName;
            DataTable table2 = new DataTable();
            table2.Columns.Add("ItemID", typeof(int));
            table2.Columns.Add("RuleID", typeof(int));
            table2.TableName = ItemRulesTableName;
            DataTable table3 = new DataTable();
            table3.Columns.Add("PageID", typeof(int));
            table3.Columns.Add("RuleID", typeof(int));
            table3.Columns.Add("EventTrigger", typeof(string));
            table3.TableName = PageRulesTableName;
            DataTable table4 = new DataTable();
            table4.Columns.Add("ExpressionID", typeof(int));
            table4.Columns.Add("Operator", typeof(int));
            table4.Columns.Add("LeftOperand", typeof(int));
            table4.Columns.Add("RightOperand", typeof(int));
            table4.Columns.Add("Parent", typeof(int));
            table4.Columns.Add("Depth", typeof(int));
            table4.Columns.Add("Lineage", typeof(string));
            table4.Columns.Add("Root", typeof(int));
            table4.Columns.Add("ChildRelation", typeof(string));
            table4.TableName = ExpressionTableName;
            DataTable table5 = new DataTable();
            table5.Columns.Add("ActionID", typeof(int));
            table5.Columns.Add("ActionTypeName", typeof(string));
            table5.Columns.Add("ActionAssembly", typeof(string));
            table5.TableName = ActionTableName;
            DataTable table6 = new DataTable();
            table6.Columns.Add("RuleID", typeof(int));
            table6.Columns.Add("ActionID", typeof(int));
            table6.TableName = RuleActionsTableName;
            DataTable table7 = new DataTable();
            table7.Columns.Add("ActionID", typeof(int));
            table7.Columns.Add("GoToPageID", typeof(int));
            table7.TableName = BranchActionTableName;
            DataTable table8 = new DataTable();
            table8.Columns.Add("OperandID", typeof(int));
            table8.Columns.Add("TypeName", typeof(string));
            table8.Columns.Add("TypeAssembly", typeof(string));
            table8.TableName = OperandTableName;
            DataTable table9 = new DataTable();
            table9.Columns.Add("OperandID", typeof(int));
            table9.Columns.Add("ItemID", typeof(int));
            table9.Columns.Add("ParentItemID", typeof(int));
            table9.TableName = ItemOperandTableName;
            DataTable table10 = new DataTable();
            table10.Columns.Add("OperandID", typeof(int));
            table10.Columns.Add("ItemID", typeof(int));
            table10.Columns.Add("OptionID", typeof(int));
            table10.Columns.Add("AnswerValue", typeof(string));
            table10.TableName = ValueOperandTableName;
            DataTable table11 = new DataTable();
            table11.Columns.Add("OperandID", typeof(int));
            table11.Columns.Add("ProfileKey", typeof(string));
            table11.TableName = ProfileOperandTableName;
            DataTable table12 = new DataTable();
            table12.Columns.Add("OperandID", typeof(int));
            table12.Columns.Add("ResponseKey", typeof(string));
            table12.TableName = ResponseOperandTableName;
            base._templateData.Tables.Add(table);
            base._templateData.Tables.Add(table2);
            base._templateData.Tables.Add(table3);
            base._templateData.Tables.Add(table4);
            base._templateData.Tables.Add(table5);
            base._templateData.Tables.Add(table7);
            base._templateData.Tables.Add(table6);
            base._templateData.Tables.Add(table8);
            base._templateData.Tables.Add(table9);
            base._templateData.Tables.Add(table10);
            base._templateData.Tables.Add(table11);
            base._templateData.Tables.Add(table12);
        }

        protected override void InitializeTemplateData()
        {
            base.InitializeTemplateData();
            DataTable table = new DataTable();
            table.Columns.Add("ResponseTemplateID", typeof(int));
            table.Columns.Add("PipeName", typeof(string));
            table.Columns.Add("ItemID", typeof(int));
            table.TableName = "ResponsePipes";
            base._templateData.Tables.Add(table);
        }

        private void LoadResponsePipes(DataSet data, string progressKey, string progressLanguage, int stepMagnitude, int stepCompletePercent)
        {
            this._responsePipes.Clear();
            if (data.Tables.Contains("ResponsePipes"))
            {
                string text = TextManager.GetText("/controlText/templateImport/loadingPipes", progressLanguage);
                int count = data.Tables["ResponsePipes"].Rows.Count;
                int currentCounterItem = 1;
                foreach (DataRow row in data.Tables["ResponsePipes"].Rows)
                {
                    ProgressProvider.SetProgressCounter(progressKey, text, currentCounterItem, count, stepMagnitude, stepCompletePercent);
                    if ((row["PipeName"] != DBNull.Value) && (row["ItemID"] != DBNull.Value))
                    {
                        ItemToken token = new ItemToken((string)row["PipeName"], Convert.ToInt32(row["ItemID"]));
                        this._responsePipes[token.TokenName] = token;
                    }
                    currentCounterItem++;
                }
            }
        }

        protected override void LoadTemplateData(DataSet data)
        {
            if (!data.Tables.Contains(this.TemplateDataTableName))
            {
                throw new Exception("DataSet does not contain ResponseTemplate table.");
            }
            if (data.Tables[this.TemplateDataTableName].Rows.Count != 1)
            {
                throw new Exception("ResponseTemplate table has 0 rows or more than 1 row.");
            }
            DataRow responseTemplateDR = data.Tables[this.TemplateDataTableName].Rows[0];
            this.LoadTemplateData(responseTemplateDR, false);
            this.LoadResponsePipes(data, string.Empty, string.Empty, -1, -1);
        }

        protected void LoadTemplateData(DataRow responseTemplateDR, bool isImport)
        {
            if (!isImport)
            {
                base.AclID = DbUtility.GetValueFromDataRow<int?>(responseTemplateDR, "AclID", null);
                base.DefaultPolicyID = DbUtility.GetValueFromDataRow<int?>(responseTemplateDR, "DefaultPolicy", null);
                base.ID = DbUtility.GetValueFromDataRow<int?>(responseTemplateDR, "ResponseTemplateID", null);
                this.GUID = DbUtility.GetValueFromDataRow<Guid>(responseTemplateDR, "GUID", Guid.Empty);
                this.CreatedBy = DbUtility.GetValueFromDataRow<string>(responseTemplateDR, "CreatedBy", string.Empty);
                base.CreatedDate = DbUtility.GetValueFromDataRow<DateTime?>(responseTemplateDR, "CreatedDate", null);
                base.LastModified = DbUtility.GetValueFromDataRow<DateTime?>(responseTemplateDR, "ModifiedDate", null);
                this.StyleTemplateID = DbUtility.GetValueFromDataRow<int?>(responseTemplateDR, "StyleTemplateID", null);
                this.IsActive = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "IsActive", false);
                this._securityType = DbUtility.GetValueFromDataRow<int>(responseTemplateDR, "SecurityType", 1);
                this._reportSecurityType = DbUtility.GetValueFromDataRow<int>(responseTemplateDR, "ReportSecurityType", 3);
            }
            this.Name = DbUtility.GetValueFromDataRow<string>(responseTemplateDR, "TemplateName", string.Empty);
            this.ActivationStartDate = DbUtility.GetValueFromDataRow<DateTime?>(responseTemplateDR, "ActivationStart", null);
            this.ActivationEndDate = DbUtility.GetValueFromDataRow<DateTime?>(responseTemplateDR, "ActivationEnd", null);
            this.MaxTotalResponses = DbUtility.GetValueFromDataRow<int?>(responseTemplateDR, "MaxTotalResponses", null);
            this.MaxResponsesPerUser = DbUtility.GetValueFromDataRow<int?>(responseTemplateDR, "MaxResponsesPerUser", null);
            this.AllowContinue = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "AllowContinue", false);
            this.AllowEdit = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "AllowEdit", false);
            this.AnonymizeResponses = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "AnonymizeResponses", false);
            this.MobileCompatible = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "MobileCompatible", false);
            this.ShowItemNumbers = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "ShowItemNumbers", false);
            this.ShowPageNumbers = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "ShowPageNumbers", false);
            this.EnableDynamicPageNumbers = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "EnableDynamicPageNumbers", false);
            this.EnableDynamicItemNumbers = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "EnableDynamicItemNumbers", false);
            this.ShowProgressBar = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "ShowProgressBar", false);
            this.ShowTitle = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "ShowTitle", false);
            this.DefaultLanguage = DbUtility.GetValueFromDataRow<string>(responseTemplateDR, "DefaultLanguage", TextManager.DefaultLanguage);
            this._supportedLanguages = new ArrayList(DbUtility.GetValueFromDataRow<string>(responseTemplateDR, "SupportedLanguages", string.Empty).Split(new char[] { ';' }));
            this.LanguageSource = DbUtility.GetValueFromDataRow<string>(responseTemplateDR, "LanguageSource", string.Empty);
            this.LanguageSourceToken = DbUtility.GetValueFromDataRow<string>(responseTemplateDR, "LanguageSourceToken", string.Empty);
            this.EnableScoring = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "EnableScoring", false);
            this.IsDeleted = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "Deleted", false);
            this.RandomizeItemsInPages = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "RandomizeItemsInPages", false);
            this.ShowValidationMessage = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "ShowValidationMessage", false);
            this.ShowValidationErrorAlert = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "RequiredFieldsAlert", false);
            this.CompletionType = DbUtility.GetValueFromDataRow<int>(responseTemplateDR, "CompletionType", -1);
            this.DisableBackButton = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "DisableBackButton", false);
            this.IsPoll = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "IsPoll", false);
            this.ChartStyleID = DbUtility.GetValueFromDataRow<int?>(responseTemplateDR, "ChartStyleID", null);
            this.Height = DbUtility.GetValueFromDataRow<int?>(responseTemplateDR, "Height", null);
            this.Width = DbUtility.GetValueFromDataRow<int?>(responseTemplateDR, "Width", null);
            this.BorderWidth = DbUtility.GetValueFromDataRow<int?>(responseTemplateDR, "BorderWidth", null);
            this.BorderColor = DbUtility.GetValueFromDataRow<string>(responseTemplateDR, "BorderColor", string.Empty);
            this.BorderStyle = DbUtility.GetValueFromDataRow<string>(responseTemplateDR, "BorderStyle", string.Empty);
            this.AllowSurveyEditWhileActive = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "AllowSurveyEditWhileActive", false);
            this.ShowSaveAndQuit = DbUtility.GetValueFromDataRow<bool>(responseTemplateDR, "ShowSaveAndQuit", false);
            this.Password = DbUtility.GetValueFromDataRow<string>(responseTemplateDR, "GuestPassword", null);
        }

        protected override DataSet MergeDataForExport()
        {
            DataSet set = base.MergeDataForExport();
            if (!set.Tables.Contains("MatrixItemOptions"))
            {
                DataTable table = new DataTable
                {
                    TableName = "MatrixItemOptions"
                };
                table.Columns.Add("OptionID", typeof(int));
                table.Columns.Add("ItemID", typeof(int));
                table.Columns.Add("TextID", typeof(string));
                table.Columns.Add("Alias", typeof(string));
                table.Columns.Add("IsDefault", typeof(bool));
                table.Columns.Add("Position", typeof(int));
                table.Columns.Add("IsOther", typeof(bool));
                table.Columns.Add("Points", typeof(int));
                set.Tables.Add(table);
            }
            foreach (ItemData data in base.Items)
            {
                if (data is MatrixItemData)
                {
                    foreach (ItemData data2 in ((MatrixItemData)data).GetChildItemDatas())
                    {
                        if (data2 is SelectItemData)
                        {
                            foreach (ListOptionData data3 in ((SelectItemData)data2).Options)
                            {
                                if (set.Tables["MatrixItemOptions"].Select("OptionID = " + data3.OptionID, null, DataViewRowState.CurrentRows).Length == 0)
                                {
                                    DataRow row = set.Tables["MatrixItemOptions"].NewRow();
                                    row["OptionID"] = data3.OptionID;
                                    row["ItemID"] = data2.ID.Value;
                                    row["TextID"] = data3.TextID;
                                    row["Alias"] = data3.Alias;
                                    row["IsDefault"] = data3.IsDefault;
                                    row["Position"] = data3.Position;
                                    row["IsOther"] = data3.IsOther;
                                    row["Points"] = data3.Points;
                                    set.Tables["MatrixItemOptions"].Rows.Add(row);
                                }
                            }
                        }
                    }
                }
            }
            set.Tables["MatrixItemOptions"].AcceptChanges();
            return set;
        }

        public override void MoveItemToPage(ItemData item, TemplatePage fromPage, TemplatePage toPage)
        {
            RuleDataService service = this.CreateRuleDataService();
            service.HandleSubscriberMoved(item, toPage.Position);
            if (fromPage.Position < toPage.Position)
            {
                service.NotifySubscribingExpressionsOfPublisherItemMoved(item, toPage.Position);
            }
            base.MoveItemToPage(item, fromPage, toPage);
        }

        public override void MovePage(TemplatePage page, int position)
        {
            RuleDataService service = this.CreateRuleDataService();
            service.HandleSubscriberMoved(page, position);
            if (page.Position < position)
            {
                foreach (ItemData data in page.Items)
                {
                    service.NotifySubscribingExpressionsOfPublisherItemMoved(data, position);
                }
            }
            base.MovePage(page, position);
        }

        protected override void OnAbort(object sender, EventArgs e)
        {
            base.OnAbort(sender, e);
            base._templateData.RejectChanges();
        }

        protected override void OnItemRemoved(ItemData item)
        {
            this.CreateRuleDataService().DeleteSubscriberExpressions(item);
            base.OnItemRemoved(item);
        }

        protected override void OnItemSaved(ItemData item, IDbTransaction t)
        {
            base.OnItemSaved(item, t);
            this.ValidateItemRules(t);
        }

        protected override void OnPageRemoved(TemplatePage page)
        {
            this.CreateRuleDataService().DeleteSubscriberExpressions(page);
            base.OnPageRemoved(page);
        }

        public void RejectChanges()
        {
            try
            {
                base._templateData.RejectChanges();
            }
            catch (ConstraintException)
            {
            }
        }

        public void RemoveSupportedLanguage(string language)
        {
            if (string.Compare(language, this.DefaultLanguage, true) != 0)
            {
                this._supportedLanguages.Remove(language);
            }
        }

        public void SaveFilters()
        {
            if (this.FilterCollection != null)
            {
                using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
                {
                    try
                    {
                        connection.Open();
                        IDbTransaction t = connection.BeginTransaction();
                        try
                        {
                            this.FilterCollection.Save(t);
                            t.Commit();
                            this._filters = null;
                        }
                        catch (Exception)
                        {
                            t.Rollback();
                            throw;
                        }
                    }
                    catch (Exception exception)
                    {
                        if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                        {
                            throw;
                        }
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        protected override void SavePageData(IDbTransaction transaction)
        {
            this.CreateRuleDataService().Save(transaction);
            base.SavePageData(transaction);
        }

        protected override void SavePageItemData(IDbTransaction transaction)
        {
            this.CreateRuleDataService().Save(transaction);
            base.SavePageItemData(transaction);
        }

        private void SetDefaultValues()
        {
            try
            {
                base.ID = null;
                base.CreatedDate = new DateTime?(DateTime.Now);
                base.LastModified = new DateTime?(DateTime.Now);
                this.Name = string.Empty;
                this.IsActive = false;
                this.MaxTotalResponses = null;
                this.MaxResponsesPerUser = null;
                this.AllowContinue = false;
                this.ShowSaveAndQuit = false;
                this.AllowEdit = false;
                this.AnonymizeResponses = false;
                this.AllowSurveyEditWhileActive = true;
                this.MobileCompatible = false;
                this.Password = string.Empty;
                this.StyleTemplateID = null;
                this._supportedLanguages = new ArrayList();
                this.DefaultLanguage = string.Empty;
                this._securityType = 1;
                this._reportSecurityType = 3;
                base.AclID = null;
                base.DefaultPolicyID = -1;
                this.EnableScoring = false;
                this.IsDeleted = false;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPrivate"))
                {
                    throw;
                }
            }
        }

        private void StoreMatrixParentData(string tableName, DataSet ds, string itemIDField)
        {
            if (((ds != null) && ds.Tables.Contains(tableName)) && ds.Tables[tableName].Columns.Contains(itemIDField))
            {
                foreach (DataRow row in ds.Tables[tableName].Select(null, null, DataViewRowState.CurrentRows))
                {
                    if (row[itemIDField] != DBNull.Value)
                    {
                        int? parentMatrixID = GetParentMatrixID((int)row[itemIDField], ds);
                        Coordinate parentMatrixPosition = GetParentMatrixPosition((int)row[itemIDField], ds);
                        if (parentMatrixID.HasValue && (parentMatrixPosition != null))
                        {
                            this._matrixItemParent[(int)row[itemIDField]] = parentMatrixID.Value;
                            this._matrixItemCoordinate[(int)row[itemIDField]] = parentMatrixPosition;
                        }
                    }
                }
            }
        }

        private void StoreNewItemOptionIDs(ItemData itemData, IList<int> oldOptionIDs)
        {
            if (itemData is SelectItemData)
            {
                for (int i = 0; i < ((SelectItemData)itemData).Options.Count; i++)
                {
                    if (i < oldOptionIDs.Count)
                    {
                        this._newOptionIDs[Math.Abs(oldOptionIDs[i])] = ((SelectItemData)itemData).Options[i].OptionID;
                    }
                }
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_Update");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, base.ID);
            this.AddCommonSprocParams(storedProcCommandWrapper);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateResponsePipes(t);
        }

        private void UpdateImportedItemIDs()
        {
            List<int> list = new List<int>(this._newItemIDs.Keys);
            list.Sort();
            for (int i = list.Count - 1; i >= 0; i--)
            {
                int oldID = list[i];
                UpdateImportedItemIDs(this.ItemRulesTable, "ItemID", oldID, this._newItemIDs[oldID]);
                UpdateImportedItemIDs(this.ItemOperandTable, "ItemID", oldID, this._newItemIDs[oldID]);
                UpdateImportedItemIDs(this.ItemOperandTable, "ParentItemID", oldID, this._newItemIDs[oldID]);
                UpdateImportedItemIDs(this.ValueOperandTable, "ItemID", oldID, this._newItemIDs[oldID]);
            }
            List<int> list2 = new List<int>(this._newOptionIDs.Keys);
            for (int j = list2.Count - 1; j >= 0; j--)
            {
                int num4 = list2[j];
                UpdateImportedItemIDs(this.ValueOperandTable, "OptionID", num4, this._newOptionIDs[num4]);
            }
        }

        private static void UpdateImportedItemIDs(DataTable table, string columnName, int oldID, int newID)
        {
            if ((table != null) && table.Columns.Contains(columnName))
            {
                foreach (DataRow row in table.Select(columnName + " = " + oldID))
                {
                    row[columnName] = newID;
                }
            }
        }

        private void UpdateResponsePipes(IDbTransaction t)
        {
            foreach (ItemToken token in this.ResponsePipes)
            {
                this.DeleteResponsePipe(token.TokenName, t);
                this.AddResponsePipe(token.TokenName, token.ItemID, t);
            }
        }

        private void UpdateTokenSourceIDs(int oldID, int newID)
        {
            foreach (ItemToken token in this._responsePipes.Values)
            {
                if (token.ItemID == Math.Abs(oldID))
                {
                    token.ItemID = newID;
                }
            }
        }

        protected override void ValidateDataSetForImport(DataSet ds)
        {
            base.ValidateDataSetForImport(ds);
            if (ds.Tables[this.PageDataTableName].Rows.Count < 3)
            {
                throw new Exception("XML file must contain at least 3 Pages nodes. (Hidden Items page with position 0, Survey page with position 1, Completion Events page with position >= 2)");
            }
        }

        public virtual void ValidateItemRules()
        {
            this.ValidateItemRules(null);
        }

        public virtual void ValidateItemRules(IDbTransaction t)
        {
            List<ItemData> list = new List<ItemData>();
            foreach (TemplatePage page in this.TemplatePages)
            {
                foreach (ItemData data in page.Items)
                {
                    list.Add(data);
                    list.AddRange(GetChildItems(data));
                }
            }
            RuleDataService service = this.CreateRuleDataService();
            if (service.DeleteInvalidSubscriberExpressions(new ReadOnlyCollection<ItemData>(list)))
            {
                if (t != null)
                {
                    service.Save(t);
                }
                else
                {
                    service.Save();
                }
            }
        }

        internal virtual DataTable ActionTable
        {
            get
            {
                if ((base._templateData != null) && base._templateData.Tables.Contains(ActionTableName))
                {
                    return base._templateData.Tables[ActionTableName];
                }
                return null;
            }
        }

        private static string ActionTableName
        {
            get
            {
                return "Action";
            }
        }

        public DateTime? ActivationEndDate { get; set; }

        public DateTime? ActivationStartDate { get; set; }

        public bool AllowContinue { get; set; }

        public bool AllowEdit { get; set; }

        public bool AllowSurveyEditWhileActive { get; set; }

        public bool AnonymizeResponses { get; set; }

        public string BackButtonText
        {
            get
            {
                string text = TextManager.GetText(this.BackButtonTextID, this._editLanguage);
                if (Utilities.IsNullOrEmpty(text))
                {
                    text = TextManager.GetText("/pageText/responseTemplate.cs/backDefaultText", "en-US");
                }
                return text;
            }
            set
            {
                TextManager.SetText(this.BackButtonTextID, this._editLanguage, value);
            }
        }

        public string BackButtonTextID
        {
            get
            {
                if (base.ID > 0)
                {
                    return string.Concat(new object[] { "/", this.TextIDPrefix, "/", base.ID, "/back" });
                }
                return string.Empty;
            }
        }

        public string BorderColor { get; set; }

        public string BorderStyle { get; set; }

        public int? BorderWidth { get; set; }

        internal virtual DataTable BranchActionTable
        {
            get
            {
                if ((base._templateData != null) && base._templateData.Tables.Contains(BranchActionTableName))
                {
                    return base._templateData.Tables[BranchActionTableName];
                }
                return null;
            }
        }

        private static string BranchActionTableName
        {
            get
            {
                return "BranchAction";
            }
        }

        public int? ChartStyleID { get; set; }

        public int CompletionType { get; set; }

        public string ContinueButtonText
        {
            get
            {
                string text = TextManager.GetText(this.ContinueButtonTextID, this._editLanguage);
                if (Utilities.IsNullOrEmpty(text))
                {
                    text = TextManager.GetText("/pageText/responseTemplate.cs/nextDefaultText", "en-US");
                }
                return text;
            }
            set
            {
                TextManager.SetText(this.ContinueButtonTextID, this._editLanguage, value);
            }
        }

        public string ContinueButtonTextID
        {
            get
            {
                if (base.ID > 0)
                {
                    return string.Concat(new object[] { "/", this.TextIDPrefix, "/", base.ID, "/continue" });
                }
                return string.Empty;
            }
        }

        public string CreatedBy { get; private set; }

        internal static string DataTableName
        {
            get
            {
                return "TemplateData";
            }
        }

        public string DefaultLanguage { get; set; }

        public string Description
        {
            get
            {
                return TextManager.GetText(this.DescriptionTextID, this._editLanguage);
            }
            set
            {
                TextManager.SetText(this.DescriptionTextID, this._editLanguage, value);
            }
        }

        private string DescriptionTextID
        {
            get
            {
                if (base.ID > 0)
                {
                    return string.Concat(new object[] { "/", this.TextIDPrefix, "/", base.ID, "/description" });
                }
                return string.Empty;
            }
        }

        public bool DisableBackButton { get; set; }

        public string EditLanguage
        {
            get
            {
                if (Utilities.IsNullOrEmpty(this._editLanguage) || !this._supportedLanguages.Contains(this._editLanguage))
                {
                    this._editLanguage = this.DefaultLanguage;
                    if (Utilities.IsNullOrEmpty(this._editLanguage))
                    {
                        this._editLanguage = TextManager.DefaultLanguage;
                    }
                }
                return this._editLanguage;
            }
            set
            {
                this._editLanguage = value;
            }
        }

        public bool EnableDynamicItemNumbers { get; set; }

        public bool EnableDynamicPageNumbers { get; set; }

        public bool EnableScoring { get; set; }

        internal virtual DataTable ExpressionTable
        {
            get
            {
                if ((base._templateData != null) && base._templateData.Tables.Contains(ExpressionTableName))
                {
                    return base._templateData.Tables[ExpressionTableName];
                }
                return null;
            }
        }

        private static string ExpressionTableName
        {
            get
            {
                return "Expression";
            }
        }

        private ResponseTemplateFilterCollection FilterCollection
        {
            get
            {
                if (this._filters == null)
                {
                    try
                    {
                        ResponseTemplateFilterCollection filters = new ResponseTemplateFilterCollection
                        {
                            ParentID = base.ID.Value
                        };
                        this._filters = filters;
                        this._filters.Load(base.ID.Value);
                    }
                    catch
                    {
                        this._filters = null;
                        throw;
                    }
                }
                return this._filters;
            }
        }

        public string FinishButtonText
        {
            get
            {
                string text = TextManager.GetText(this.FinishButtonTextID, this._editLanguage);
                if (Utilities.IsNullOrEmpty(text))
                {
                    text = TextManager.GetText("/pageText/responseTemplate.cs/finishDefaultText", "en-US");
                }
                return text;
            }
            set
            {
                TextManager.SetText(this.FinishButtonTextID, this._editLanguage, value);
            }
        }

        public string FinishButtonTextID
        {
            get
            {
                if (base.ID > 0)
                {
                    return string.Concat(new object[] { "/", this.TextIDPrefix, "/", base.ID, "/finish" });
                }
                return string.Empty;
            }
        }

        public Guid GUID { get; private set; }

        public int? Height { get; set; }

        internal int? Identity
        {
            set
            {
                base.ID = value;
            }
        }

        public override string IdentityColumnName
        {
            get
            {
                return "ResponseTemplateID";
            }
        }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; private set; }

        public bool IsPoll { get; set; }

        internal virtual DataTable ItemOperandTable
        {
            get
            {
                if ((base._templateData != null) && base._templateData.Tables.Contains(ItemOperandTableName))
                {
                    return base._templateData.Tables[ItemOperandTableName];
                }
                return null;
            }
        }

        private static string ItemOperandTableName
        {
            get
            {
                return "ItemOperand";
            }
        }

        internal virtual DataTable ItemRulesTable
        {
            get
            {
                if ((base._templateData != null) && base._templateData.Tables.Contains(ItemRulesTableName))
                {
                    return base._templateData.Tables[ItemRulesTableName];
                }
                return null;
            }
        }

        private static string ItemRulesTableName
        {
            get
            {
                return "ItemRules";
            }
        }

        public string LanguageSource { get; set; }

        public string LanguageSourceToken { get; set; }

        public string LocalizedName
        {
            get
            {
                string text = string.Empty;
                if (Utilities.IsNotNullOrEmpty(this._editLanguage))
                {
                    text = TextManager.GetText(this.NameTextID, this._editLanguage);
                }
                else if (Utilities.IsNotNullOrEmpty(this.DefaultLanguage))
                {
                    text = TextManager.GetText(this.NameTextID, this.DefaultLanguage);
                }
                if (Utilities.IsNullOrEmpty(text))
                {
                    text = this.Name;
                }
                return text;
            }
            set
            {
                if (Utilities.IsNotNullOrEmpty(this._editLanguage))
                {
                    TextManager.SetText(this.NameTextID, this._editLanguage, value);
                }
                else if (Utilities.IsNotNullOrEmpty(this.DefaultLanguage))
                {
                    TextManager.SetText(this.NameTextID, this.DefaultLanguage, value);
                }
                else
                {
                    TextManager.SetText(this.NameTextID, TextManager.DefaultLanguage, value);
                }
            }
        }

        public int? MaxResponsesPerUser { get; set; }

        public int? MaxTotalResponses { get; set; }

        public bool MobileCompatible { get; set; }

        public string Name { get; set; }

        private string NameTextID
        {
            get
            {
                if (base.ID > 0)
                {
                    return string.Concat(new object[] { "/", this.TextIDPrefix, "/", base.ID, "/name" });
                }
                return string.Empty;
            }
        }

        internal virtual DataTable OperandTable
        {
            get
            {
                if ((base._templateData != null) && base._templateData.Tables.Contains(OperandTableName))
                {
                    return base._templateData.Tables[OperandTableName];
                }
                return null;
            }
        }

        private static string OperandTableName
        {
            get
            {
                return "Operand";
            }
        }

        internal virtual DataTable PageRulesTable
        {
            get
            {
                if ((base._templateData != null) && base._templateData.Tables.Contains(PageRulesTableName))
                {
                    return base._templateData.Tables[PageRulesTableName];
                }
                return null;
            }
        }

        private static string PageRulesTableName
        {
            get
            {
                return "PageRules";
            }
        }

        internal virtual DataTable PageTable
        {
            get
            {
                return this.PageDataTable;
            }
        }

        public string Password { get; set; }

        internal virtual DataTable ProfileOperandTable
        {
            get
            {
                if ((base._templateData != null) && base._templateData.Tables.Contains(ProfileOperandTableName))
                {
                    return base._templateData.Tables[ProfileOperandTableName];
                }
                return null;
            }
        }

        private static string ProfileOperandTableName
        {
            get
            {
                return "ProfileOperand";
            }
        }

        public bool RandomizeItemsInPages { get; set; }

        public Checkbox.Forms.ReportSecurityType ReportSecurityType
        {
            get
            {
                return (Checkbox.Forms.ReportSecurityType)Enum.ToObject(typeof(Checkbox.Forms.ReportSecurityType), this._reportSecurityType);
            }
            set
            {
                this._reportSecurityType = Convert.ToInt32(value);
            }
        }

        internal virtual DataTable ResponseOperandTable
        {
            get
            {
                if ((base._templateData != null) && base._templateData.Tables.Contains(ResponseOperandTableName))
                {
                    return base._templateData.Tables[ResponseOperandTableName];
                }
                return null;
            }
        }

        private static string ResponseOperandTableName
        {
            get
            {
                return "ResponseOperand";
            }
        }

        public ReadOnlyCollection<ItemToken> ResponsePipes
        {
            get
            {
                List<ItemToken> list = new List<ItemToken>();
                foreach (ItemToken token in this._responsePipes.Values)
                {
                    list.Add(token);
                }
                return new ReadOnlyCollection<ItemToken>(list);
            }
        }

        internal virtual DataTable RuleActionsTable
        {
            get
            {
                if ((base._templateData != null) && base._templateData.Tables.Contains(RuleActionsTableName))
                {
                    return base._templateData.Tables[RuleActionsTableName];
                }
                return null;
            }
        }

        private static string RuleActionsTableName
        {
            get
            {
                return "RuleActions";
            }
        }

        internal virtual DataTable RuleTable
        {
            get
            {
                if ((base._templateData != null) && base._templateData.Tables.Contains(RuleTableName))
                {
                    return base._templateData.Tables[RuleTableName];
                }
                return null;
            }
        }

        private static string RuleTableName
        {
            get
            {
                return "Rule";
            }
        }

        public string SaveAndQuitButtonText
        {
            get
            {
                string text = TextManager.GetText(this.SaveProgressButtonTextID, this._editLanguage);
                if (Utilities.IsNullOrEmpty(text))
                {
                    text = TextManager.GetText("/pageText/responseTemplate.cs/saveDefaultText", "en-US");
                }
                return text;
            }
            set
            {
                TextManager.SetText(this.SaveProgressButtonTextID, this._editLanguage, value);
            }
        }

        public string SaveProgressButtonTextID
        {
            get
            {
                if (base.ID > 0)
                {
                    return string.Concat(new object[] { "/", this.TextIDPrefix, "/", base.ID, "/saveProgress" });
                }
                return string.Empty;
            }
        }

        public Checkbox.Forms.SecurityType SecurityType
        {
            get
            {
                return (Checkbox.Forms.SecurityType)Enum.ToObject(typeof(Checkbox.Forms.SecurityType), this._securityType);
            }
            set
            {
                this._securityType = Convert.ToInt32(value);
            }
        }

        public bool ShowItemNumbers { get; set; }

        public bool ShowPageNumbers { get; set; }

        public bool ShowProgressBar { get; set; }

        public bool ShowSaveAndQuit { get; set; }

        public bool ShowTitle { get; set; }

        public bool ShowValidationErrorAlert { get; set; }

        public bool ShowValidationMessage { get; set; }

        public int? StyleTemplateID { get; set; }

        public string[] SupportedLanguages
        {
            get
            {
                return (string[])this._supportedLanguages.ToArray(typeof(string));
            }
            set
            {
                this._supportedLanguages = new ArrayList(value);
            }
        }

        public string Title
        {
            get
            {
                return TextManager.GetText(this.TitleTextID, this._editLanguage);
            }
            set
            {
                TextManager.SetText(this.TitleTextID, this._editLanguage, value);
            }
        }

        public string TitleTextID
        {
            get
            {
                if (base.ID > 0)
                {
                    return string.Concat(new object[] { "/", this.TextIDPrefix, "/", base.ID, "/title" });
                }
                return string.Empty;
            }
        }

        internal virtual DataTable ValueOperandTable
        {
            get
            {
                if ((base._templateData != null) && base._templateData.Tables.Contains(ValueOperandTableName))
                {
                    return base._templateData.Tables[ValueOperandTableName];
                }
                return null;
            }
        }

        private static string ValueOperandTableName
        {
            get
            {
                return "ValueOperand";
            }
        }

        public int? Width { get; set; }
    }
}

