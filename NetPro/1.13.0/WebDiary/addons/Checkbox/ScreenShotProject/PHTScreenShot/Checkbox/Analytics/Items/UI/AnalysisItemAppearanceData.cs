﻿namespace Checkbox.Analytics.Items.UI
{
    using Checkbox.Forms.Items.UI;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Text;

    [Serializable]
    public abstract class AnalysisItemAppearanceData : AppearanceData
    {
        protected AnalysisItemAppearanceData()
        {
            this.SetDefaultValues();
        }

        private void AddCommonSprocParams(DBCommandWrapper command)
        {
            command.AddInParameter("AppearanceID", DbType.Int32, base.ID.Value);
            command.AddInParameter("Precision", DbType.Int32, this.Precision);
            command.AddInParameter("Explosion", DbType.Int32, this.Explosion);
            command.AddInParameter("ShowDataLabels", DbType.Boolean, this.ShowDataLabels);
            command.AddInParameter("ShowDLZeroValues", DbType.Boolean, this.ShowDataLabelZeroValues);
            command.AddInParameter("ShowDLValues", DbType.Boolean, this.ShowDataLabelValues);
            command.AddInParameter("Separator", DbType.String, this.Separator);
            command.AddInParameter("ShowXTitle", DbType.Boolean, this.ShowDataLabelsXAxisTitle);
            command.AddInParameter("Width", DbType.Int32, this.GraphWidth);
            command.AddInParameter("Height", DbType.Int32, this.GraphHeight);
            command.AddInParameter("ShowLegend", DbType.Boolean, this.ShowLegend);
            command.AddInParameter("LegendWidth", DbType.Int32, this.LegendWidth);
            command.AddInParameter("ShowTitle", DbType.Boolean, this.ShowTitle);
            command.AddInParameter("ShowResponseCount", DbType.Boolean, this.ShowResponseCountInTitle);
            command.AddInParameter("TitleColor", DbType.String, this.TitleColor);
            command.AddInParameter("BGColor", DbType.String, this.BackgroundColor);
            command.AddInParameter("BGGradient", DbType.Boolean, this.BackgroundGradient);
            command.AddInParameter("ShowHeader", DbType.Boolean, this.ShowHeader);
            command.AddInParameter("Spacing", DbType.Int32, this.GraphSpacing);
            command.AddInParameter("ShowPercent", DbType.Boolean, this.ShowPercent);
            command.AddInParameter("ForeColor", DbType.String, this.ForegroundColor);
            command.AddInParameter("Color", DbType.String, this.Color);
            command.AddInParameter("PieGraphColors", DbType.String, this.PieGraphColors);
            command.AddInParameter("MaxColWidth", DbType.Int32, this.MaxColumnWidth);
            command.AddInParameter("TitleFont", DbType.String, this.TitleFont);
            command.AddInParameter("LegendFont", DbType.String, this.LegendFont);
            command.AddInParameter("ShowAnswerCount", DbType.Boolean, this.ShowAnswerCount);
            command.AddInParameter("ShowPercentageInLegend", DbType.Boolean, this.ShowPercentageInLegend);
        }

        protected override void Create(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_CreateAI");
            storedProcCommandWrapper.AddInParameter("AppearanceCode", DbType.String, this.AppearanceCode);
            storedProcCommandWrapper.AddInParameter("GraphType", DbType.String, this.GraphType.ToString());
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
            storedProcCommandWrapper.AddOutParameter("AppearanceID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("AppearanceID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save appearance data.");
            }
            base.ID = new int?((int) parameterValue);
            this.CreateAnalysisItemAppearanceData(transaction);
        }

        protected virtual void CreateAnalysisItemAppearanceData(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_CreateAI_Spec");
            this.AddCommonSprocParams(storedProcCommandWrapper);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            base.CreateDataRelationsInternal(ds);
            string name = this.ParentDataTableName + "_" + this.DataTableName;
            if ((!ds.Relations.Contains(name) && ds.Tables.Contains(this.ParentDataTableName)) && ds.Tables.Contains(this.DataTableName))
            {
                ds.Relations.Add(new DataRelation(name, ds.Tables[this.ParentDataTableName].Columns["AppearanceID"], ds.Tables[this.DataTableName].Columns[this.IdentityColumnName]));
            }
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID.Value <= 0))
            {
                throw new Exception("Unable to get appearance data with no ID.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_GetAI");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, this.DataTableName);
            return dataSet;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            this.Precision = DbUtility.GetValueFromDataRow<int?>(data, "Precision", null);
            this.Explosion = DbUtility.GetValueFromDataRow<int?>(data, "Explosion", null);
            this.ShowDataLabels = DbUtility.GetValueFromDataRow<bool>(data, "ShowDataLabels", false);
            this.ShowDataLabelZeroValues = DbUtility.GetValueFromDataRow<bool>(data, "ShowDLZeroValues", false);
            this.ShowDataLabelValues = DbUtility.GetValueFromDataRow<bool>(data, "ShowDLValues", false);
            this.Separator = DbUtility.GetValueFromDataRow<string>(data, "Separator", string.Empty);
            this.ShowDataLabelsXAxisTitle = DbUtility.GetValueFromDataRow<bool>(data, "ShowXTitle", false);
            this.GraphHeight = DbUtility.GetValueFromDataRow<int?>(data, "Height", null);
            this.GraphWidth = DbUtility.GetValueFromDataRow<int?>(data, "Width", null);
            this.ShowLegend = DbUtility.GetValueFromDataRow<bool>(data, "ShowLegend", false);
            this.LegendWidth = DbUtility.GetValueFromDataRow<int?>(data, "LegendWidth", null);
            this.ShowTitle = DbUtility.GetValueFromDataRow<bool>(data, "ShowTitle", false);
            this.ShowResponseCountInTitle = DbUtility.GetValueFromDataRow<bool>(data, "ShowResponseCount", false);
            this.TitleColor = DbUtility.GetValueFromDataRow<string>(data, "TitleColor", string.Empty);
            this.BackgroundColor = DbUtility.GetValueFromDataRow<string>(data, "BGColor", string.Empty);
            this.BackgroundGradient = DbUtility.GetValueFromDataRow<bool>(data, "BGGradient", false);
            this.ShowHeader = DbUtility.GetValueFromDataRow<bool>(data, "ShowHeader", false);
            this.GraphSpacing = DbUtility.GetValueFromDataRow<int?>(data, "Spacing", null);
            this.ShowPercent = DbUtility.GetValueFromDataRow<bool>(data, "ShowPercent", false);
            this.ForegroundColor = DbUtility.GetValueFromDataRow<string>(data, "ForeColor", string.Empty);
            this.Color = DbUtility.GetValueFromDataRow<string>(data, "Color", string.Empty);
            this.PieGraphColors = DbUtility.GetValueFromDataRow<string>(data, "PieGraphColors", string.Empty);
            this.MaxColumnWidth = DbUtility.GetValueFromDataRow<int?>(data, "MaxColWidth", null);
            this.TitleFont = DbUtility.GetValueFromDataRow<string>(data, "TitleFont", string.Empty);
            this.LegendFont = DbUtility.GetValueFromDataRow<string>(data, "LegendFont", string.Empty);
            this.ShowAnswerCount = DbUtility.GetValueFromDataRow<bool>(data, "ShowAnswerCount", false);
            this.ShowPercentageInLegend = DbUtility.GetValueFromDataRow<bool>(data, "ShowPercentageInLegend", false);
        }

        protected virtual void SetDefaultValues()
        {
            this.Precision = 1;
            this.ShowTitle = true;
            this.ShowResponseCountInTitle = true;
            this.TitleColor = "Black";
            this.GraphWidth = 600;
            this.GraphHeight = 400;
            this.ShowLegend = true;
            this.LegendWidth = 200;
            this.BackgroundColor = "White";
            this.BackgroundGradient = false;
            this.ShowHeader = true;
            this.GraphSpacing = 20;
            this.ShowPercent = true;
            this.ShowDataLabelZeroValues = false;
            this.Explosion = 10;
            this.PieGraphColors = SupportedPieGraphColors;
            this.ShowDataLabels = true;
            this.ShowDataLabelValues = true;
            this.ShowDataLabelsXAxisTitle = true;
            this.Separator = string.Empty;
            this.ForegroundColor = "Brown";
            this.Color = "Black";
            this.MaxColumnWidth = 30;
            this.TitleFont = "Arial";
            this.LegendFont = "Arial";
            this.ShowAnswerCount = true;
            this.ShowPercentageInLegend = false;
        }

        protected override void Update(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_UpdateAI");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("AppearanceCode", DbType.String, this.AppearanceCode);
            storedProcCommandWrapper.AddInParameter("GraphType", DbType.String, this.GraphType.ToString());
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            this.UpdateAnalysisItemAppearanceData(transaction);
        }

        protected virtual void UpdateAnalysisItemAppearanceData(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_UpdateAI_Spec");
            this.AddCommonSprocParams(storedProcCommandWrapper);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
        }

        public virtual string BackgroundColor { get; set; }

        public virtual bool BackgroundGradient { get; set; }

        public virtual string Color { get; set; }

        public override string DataTableName
        {
            get
            {
                return "AnalysisItemAppearanceData";
            }
        }

        public virtual int? Explosion { get; set; }

        public virtual string ForegroundColor { get; set; }

        public virtual int? GraphHeight
        {
            get
            {
                return this.Height;
            }
            set
            {
                this.Height = value;
            }
        }

        public virtual int? GraphSpacing { get; set; }

        public virtual int? GraphWidth
        {
            get
            {
                return this.Width;
            }
            set
            {
                this.Width = value;
            }
        }

        public string LegendFont { get; set; }

        public virtual int? LegendWidth { get; set; }

        public virtual int? MaxColumnWidth { get; set; }

        public string PieGraphColors { get; set; }

        public int? Precision { get; set; }

        public virtual string Separator { get; set; }

        public bool ShowAnswerCount { get; set; }

        public virtual bool ShowDataLabels { get; set; }

        public virtual bool ShowDataLabelsXAxisTitle { get; set; }

        public virtual bool ShowDataLabelValues { get; set; }

        public virtual bool ShowDataLabelZeroValues { get; set; }

        public virtual bool ShowHeader { get; set; }

        public virtual bool ShowLegend { get; set; }

        public virtual bool ShowPercent { get; set; }

        public bool ShowPercentageInLegend { get; set; }

        public virtual bool ShowResponseCountInTitle { get; set; }

        public virtual bool ShowTitle { get; set; }

        private static string SupportedPieGraphColors
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("Red");
                builder.Append(",");
                builder.Append("Orange");
                builder.Append(",");
                builder.Append("Yellow");
                builder.Append(",");
                builder.Append("Green");
                builder.Append(",");
                builder.Append("Blue");
                builder.Append(",");
                builder.Append("Purple");
                builder.Append(",");
                builder.Append("Red");
                builder.Append(",");
                builder.Append("Pink");
                builder.Append(",");
                builder.Append("Teal");
                builder.Append(",");
                builder.Append("Maroon");
                builder.Append(",");
                builder.Append("Aqua");
                builder.Append(",");
                builder.Append("Gold");
                builder.Append(",");
                builder.Append("Lime");
                builder.Append(",");
                builder.Append("MidnightBlue");
                builder.Append(",");
                builder.Append("YellowGreen");
                builder.Append(",");
                builder.Append("DarkGreen");
                return builder.ToString();
            }
        }

        public virtual string TitleColor { get; set; }

        public string TitleFont { get; set; }
    }
}

