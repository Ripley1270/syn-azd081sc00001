﻿namespace Checkbox.Configuration.Install
{
    using Checkbox.Styles;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Xml;

    [Serializable]
    public class UpgradeInstaller : ApplicationInstaller
    {
        private Dictionary<string, string> _isoMappingTable;
        private List<string> _postProcessingScripts;
        private List<string> _preProcessingScripts;
        private string _upgradeProductName;

        public UpgradeInstaller(string rootFolder) : this(rootFolder, false)
        {
        }

        public UpgradeInstaller(string rootFolder, bool forceNew) : base(rootFolder, forceNew, "sqlserver")
        {
        }

        private string ConvertISOCode(string languageCode)
        {
            if (this._isoMappingTable.ContainsKey(languageCode))
            {
                return this._isoMappingTable[languageCode];
            }
            if (languageCode.Length != 5)
            {
                return (languageCode + "-" + languageCode.ToUpper());
            }
            return languageCode;
        }

        private void ConvertText(string languageCode, string textID, string xmlSnippet)
        {
            if (((xmlSnippet != null) && (textID != null)) && (textID.Trim() != string.Empty))
            {
                try
                {
                    XmlDocument document = new XmlDocument();
                    document.LoadXml(xmlSnippet);
                    XmlNodeList list = document.SelectNodes("/MultiLanguageText/Text");
                    if (list != null)
                    {
                        if (list.Count == 0)
                        {
                            Database database = DatabaseFactory.CreateDatabase();
                            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper("DELETE FROM ckbx_Text WHERE TextID = @TextID AND LanguageCode = @LanguageCode");
                            sqlStringCommandWrapper.AddInParameter("TextID", DbType.String, textID);
                            sqlStringCommandWrapper.AddInParameter("LanguageCode", DbType.String, languageCode);
                            database.ExecuteNonQuery(sqlStringCommandWrapper);
                        }
                        else
                        {
                            bool flag = false;
                            foreach (XmlNode node in list)
                            {
                                XmlAttribute attribute = node.Attributes["Language"];
                                if (attribute != null)
                                {
                                    string strA = this.ConvertISOCode(attribute.Value);
                                    string innerText = node.InnerText;
                                    if ((strA != null) && (strA.Trim() != string.Empty))
                                    {
                                        if (string.Compare(strA, languageCode, true) == 0)
                                        {
                                            flag = true;
                                        }
                                        Database database2 = DatabaseFactory.CreateDatabase();
                                        DBCommandWrapper storedProcCommandWrapper = database2.GetStoredProcCommandWrapper("ckbx_Text_Set");
                                        storedProcCommandWrapper.AddInParameter("TextID", DbType.String, textID);
                                        storedProcCommandWrapper.AddInParameter("LanguageCode", DbType.String, strA);
                                        storedProcCommandWrapper.AddInParameter("TextValue", DbType.String, innerText);
                                        database2.ExecuteNonQuery(storedProcCommandWrapper);
                                    }
                                }
                            }
                            if (!flag)
                            {
                                Database database3 = DatabaseFactory.CreateDatabase();
                                DBCommandWrapper command = database3.GetSqlStringCommandWrapper("DELETE FROM ckbx_Text WHERE TextID = @TextID AND LanguageCode = @LanguageCode");
                                command.AddInParameter("TextID", DbType.String, textID);
                                command.AddInParameter("LanguageCode", DbType.String, languageCode);
                                database3.ExecuteNonQuery(command);
                            }
                        }
                    }
                }
                catch
                {
                }
            }
        }

        public bool DoUpgrade(out string status)
        {
            if (!this.ExecuteUpgradeScripts(out status))
            {
                return false;
            }
            this.ExecuteInCodeUpgradeOperations();
            if (!this.ExecutePostUpgradeScripts(out status))
            {
                return false;
            }
            return true;
        }

        private void ExecuteInCodeUpgradeOperations()
        {
            UpgradeStyleTemplateCss();
            this.InternationalizeUpgradedInstallation();
        }

        private bool ExecutePostUpgradeScripts(out string status)
        {
            status = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(base.ConnectionString))
                {
                    status = "No connection string was specified.";
                    return false;
                }
                foreach (string str in this._postProcessingScripts)
                {
                    if (!str.Trim().StartsWith("EXEC") && !File.Exists(str))
                    {
                        status = "A required database installation script was not found:  " + str;
                        return false;
                    }
                }
                foreach (string str2 in this._postProcessingScripts)
                {
                    if (!this.RunDBScriptFile(str2, out status))
                    {
                        return false;
                    }
                }
            }
            catch (Exception exception)
            {
                status = "An error occurred while upgrading the database.  The error was:  " + exception.Message;
                return false;
            }
            return true;
        }

        private bool ExecuteUpgradeScripts(out string status)
        {
            status = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(base.ConnectionString))
                {
                    status = "No connection string was specified.";
                    return false;
                }
                if (this.PreviousVersion != "3.0.10")
                {
                    string path = string.Concat(new object[] { base.RootFolder, Path.DirectorySeparatorChar, "Install", Path.DirectorySeparatorChar, "InstallScripts", Path.DirectorySeparatorChar, "Upgrade", Path.DirectorySeparatorChar, "Patch-", this.PreviousVersion, "-to-3.0.10.sql" });
                    if (!File.Exists(path))
                    {
                        throw new Exception("Unable to find patch files: " + path);
                    }
                    if (!this.RunDBScriptFile(path, out status))
                    {
                        return false;
                    }
                }
                foreach (string str2 in this._preProcessingScripts)
                {
                    if (!str2.Trim().StartsWith("EXEC") && !File.Exists(str2))
                    {
                        status = "A required database installation script was not found:  " + str2;
                        return false;
                    }
                }
                foreach (string str3 in this._preProcessingScripts)
                {
                    if (!this.RunDBScriptFile(str3, out status))
                    {
                        return false;
                    }
                }
            }
            catch (Exception exception)
            {
                status = "An error occurred while upgrading the database.  The error was:  " + exception.Message;
                return false;
            }
            return true;
        }

        private void InternationalizeUpgradedInstallation()
        {
            if (IsMLInstallation())
            {
                this.UpgradeMultiLanguageSurveyText();
            }
            UpgradeSurveySupportedLanguages();
        }

        private static bool IsMLInstallation()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper("SELECT COUNT(FormID) FROM rz_Form WHERE EnableMultipleLanguages = 1");
            return (((int) database.ExecuteScalar(sqlStringCommandWrapper)) > 0);
        }

        public override void LoadInstallFilesAndScripts()
        {
            base.LoadInstallFilesAndScripts();
            XmlDocument document = new XmlDocument();
            document.Load(string.Concat(new object[] { base.RootFolder, Path.DirectorySeparatorChar, "Install", Path.DirectorySeparatorChar, "UpgradeInfo.xml" }));
            this._upgradeProductName = XmlUtility.GetNodeText(document.SelectSingleNode("/upgradeInfo/properties/name"), true);
            this._preProcessingScripts = new List<string>();
            XmlNodeList list = document.SelectNodes("/upgradeInfo/databaseScripts/preProcessing/databaseScript");
            if (list != null)
            {
                foreach (XmlNode node in list)
                {
                    if (node.InnerText.Trim().StartsWith("EXEC"))
                    {
                        this._preProcessingScripts.Add(node.InnerText.Trim());
                    }
                    else
                    {
                        this._preProcessingScripts.Add(base.RootFolder + Path.DirectorySeparatorChar + node.InnerText.Trim());
                    }
                }
            }
            this._postProcessingScripts = new List<string>();
            XmlNodeList list2 = document.SelectNodes("/upgradeInfo/databaseScripts/postProcessing/databaseScript");
            if (list2 != null)
            {
                foreach (XmlNode node2 in list2)
                {
                    if (node2.InnerText.Trim().StartsWith("EXEC"))
                    {
                        this._postProcessingScripts.Add(node2.InnerText.Trim());
                    }
                    else
                    {
                        this._postProcessingScripts.Add(base.RootFolder + Path.DirectorySeparatorChar + node2.InnerText.Trim());
                    }
                }
            }
            this.LoadIsoMappingTable();
        }

        private void LoadIsoMappingTable()
        {
            XmlDocument document = new XmlDocument();
            document.Load(string.Concat(new object[] { base.RootFolder, Path.DirectorySeparatorChar, "Install", Path.DirectorySeparatorChar, "UpgradeInfo.xml" }));
            this._isoMappingTable = new Dictionary<string, string>();
            XmlNodeList list = document.SelectNodes("/upgradeInfo/SupportedLanguages/Language");
            if (list != null)
            {
                foreach (XmlNode node in list)
                {
                    string nodeText = XmlUtility.GetNodeText(node.SelectSingleNode("ISOCode639"));
                    string str2 = XmlUtility.GetNodeText(node.SelectSingleNode("ISOCode639-3166"));
                    if ((nodeText.Trim() != string.Empty) && (str2.Trim() != string.Empty))
                    {
                        this._isoMappingTable.Add(nodeText, str2);
                    }
                }
            }
        }

        public override bool RunDBScriptFile(string sqlScript, out string status)
        {
            if (sqlScript.Trim().StartsWith("EXEC"))
            {
                Database database = DatabaseFactory.CreateDatabase();
                IDbConnection connection = database.GetConnection();
                try
                {
                    sqlScript = sqlScript.Replace("[[PREVIOUS_VERSION]]", this.PreviousVersion);
                    sqlScript = sqlScript.Replace("[[PRODUCTID]]", base.InstalledProductID.ToString());
                    connection.Open();
                    DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(sqlScript);
                    database.ExecuteNonQuery(sqlStringCommandWrapper);
                    status = "Success.";
                    return true;
                }
                catch
                {
                    status = "An error occurred executing a required script: " + sqlScript;
                    return false;
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Dispose();
                    }
                }
            }
            return base.RunDBScriptFile(sqlScript, out status);
        }

        public bool TestForExistingUpgrade(out string status)
        {
            if (string.IsNullOrEmpty(base.ConnectionString))
            {
                status = "No connection string was specified.";
                return true;
            }
            status = string.Empty;
            bool flag = false;
            using (SqlConnection connection = new SqlConnection(base.ConnectionString))
            {
                try
                {
                    try
                    {
                        connection.Open();
                        string str = "select ProductID from ckbx_Product_Info where ProductName = N'" + this._upgradeProductName + "' ";
                        SqlCommand command = connection.CreateCommand();
                        command.CommandText = str;
                        using (IDataReader reader = command.ExecuteReader())
                        {
                            if (reader != null)
                            {
                                try
                                {
                                    if (reader.Read())
                                    {
                                        status = "The upgrade procedure has already completed for this installation.";
                                        flag = true;
                                    }
                                }
                                finally
                                {
                                    reader.Close();
                                }
                            }
                            return flag;
                        }
                    }
                    catch
                    {
                    }
                    return flag;
                }
                finally
                {
                    connection.Close();
                }
            }
            return flag;
        }

        private void UpdateResponseLanguages()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper("SELECT ResponseID, Language FROM ckbx_Response WHERE Language IS NOT NULL AND LEN(Language) = 2");
            DataSet set = database.ExecuteDataSet(sqlStringCommandWrapper);
            if (set.Tables.Count > 0)
            {
                foreach (DataRow row in set.Tables[0].Select(null, null, DataViewRowState.CurrentRows))
                {
                    long num = DbUtility.GetValueFromDataRow<long>(row, "ResponseID", -1L);
                    string languageCode = DbUtility.GetValueFromDataRow<string>(row, "Language", null);
                    languageCode = this.ConvertISOCode(languageCode);
                    DBCommandWrapper command = database.GetSqlStringCommandWrapper("UPDATE ckbx_Response SET Language = @Language WHERE ResponseID = @ResponseID");
                    command.AddInParameter("Language", DbType.String, languageCode);
                    command.AddInParameter("ResponseID", DbType.Int64, num);
                    database.ExecuteNonQuery(command);
                }
            }
        }

        private void UpdateResponseTemplateLanguages()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper("SELECT ResponseTemplateID, DefaultLanguage, SupportedLanguages FROM ckbx_ResponseTemplate");
            DataSet set = database.ExecuteDataSet(sqlStringCommandWrapper);
            if (set.Tables.Count > 0)
            {
                foreach (DataRow row in set.Tables[0].Select(null, null, DataViewRowState.CurrentRows))
                {
                    int num = DbUtility.GetValueFromDataRow<int>(row, "ResponseTemplateID", -1);
                    string languageCode = DbUtility.GetValueFromDataRow<string>(row, "DefaultLanguage", string.Empty);
                    string str2 = DbUtility.GetValueFromDataRow<string>(row, "SupportedLanguages", null);
                    if ((languageCode != null) && (languageCode.Length == 2))
                    {
                        languageCode = this.ConvertISOCode(languageCode);
                        DBCommandWrapper wrapper2 = database.GetSqlStringCommandWrapper("UPDATE ckbx_ResponseTemplate SET DefaultLanguage = @DefaultLanguage WHERE ResponseTemplateID = @ResponseTemplateID");
                        wrapper2.AddInParameter("DefaultLanguage", DbType.String, languageCode);
                        wrapper2.AddInParameter("ResponseTemplateID", DbType.Int32, num);
                        database.ExecuteNonQuery(wrapper2);
                    }
                    string str3 = languageCode;
                    if (str2 != null)
                    {
                        foreach (string str4 in str2.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            string str5 = this.ConvertISOCode(str4);
                            if ((str3 != null) && !str3.ToLower().Contains(str5.ToLower()))
                            {
                                str3 = str3 + ";" + str5;
                            }
                        }
                    }
                    DBCommandWrapper command = database.GetSqlStringCommandWrapper("UPDATE ckbx_ResponseTemplate SET SupportedLanguages = @SupportedLanguages WHERE ResponseTemplateID = @ResponseTemplateID");
                    command.AddInParameter("SupportedLanguages", DbType.String, str3);
                    command.AddInParameter("ResponseTemplateID", DbType.Int32, num);
                    database.ExecuteNonQuery(command);
                }
            }
        }

        private void UpdateTexts()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper("SELECT * FROM ckbx_Text WHERE TextValue LIKE '<MultiLanguageText>%</MultiLanguageText>'");
            DataSet set = database.ExecuteDataSet(sqlStringCommandWrapper);
            if (set.Tables.Count > 0)
            {
                foreach (DataRow row in set.Tables[0].Select(null, null, DataViewRowState.CurrentRows))
                {
                    this.ConvertText(DbUtility.GetValueFromDataRow<string>(row, "LanguageCode", null), DbUtility.GetValueFromDataRow<string>(row, "TextID", null), DbUtility.GetValueFromDataRow<string>(row, "TextValue", null));
                }
            }
        }

        private void UpgradeMultiLanguageSurveyText()
        {
            this.UpdateTexts();
            this.UpdateResponseTemplateLanguages();
            this.UpdateResponseLanguages();
        }

        private static void UpgradeStyleTemplateCss()
        {
            try
            {
                Database database = DatabaseFactory.CreateDatabase();
                using (IDbConnection connection = database.GetConnection())
                {
                    connection.Open();
                    DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper("SELECT TemplateID, Css FROM rz_StyleTemplate");
                    using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(1) && (((string) reader[1]) != string.Empty))
                            {
                                StyleTemplate styleTemplate = StyleTemplateManager.GetStyleTemplate((int) reader[0]);
                                XmlDocument xmlDoc = new XmlDocument();
                                xmlDoc.LoadXml((string) reader[1]);
                                styleTemplate.LoadCssClasses(xmlDoc);
                                styleTemplate.Save();
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "UIProcess"))
                {
                    throw;
                }
            }
        }

        private static void UpgradeSurveySupportedLanguages()
        {
        }

        protected override string InstallType
        {
            get
            {
                return "upgrade";
            }
        }

        public string PreviousVersion { get; set; }
    }
}

