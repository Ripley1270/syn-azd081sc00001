﻿namespace Checkbox.Security.Data
{
    using Checkbox.Security;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using System;

    internal static class SelectACLWithGroupMembershipPermission
    {
        internal static SelectQuery GetQuery(string entityTable, string entityAclIDColumn, string entryIdentifier, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = new SelectQuery(entityTable);
            query.AddParameter(entityAclIDColumn, string.Empty, entityTable);
            query.AddTableJoin("ckbx_AccessControlEntries", QueryJoinType.Inner, "AclID", entityTable, entityAclIDColumn);
            query.AddTableJoin("ckbx_AccessControlEntry", QueryJoinType.Inner, "EntryID", "ckbx_AccessControlEntries", "EntryID");
            CriteriaCollection joinCriteria = new CriteriaCollection(CriteriaJoinType.And);
            joinCriteria.AddCriterion(new QueryCriterion(new LiteralParameter("CONVERT(nvarchar, g.GroupID)"), CriteriaOperator.EqualTo, new SelectParameter("EntryIdentifier", string.Empty, "ckbx_AccessControlEntry")));
            QueryTableJoin join = new QueryTableJoin("ckbx_Group", QueryJoinType.Inner, joinCriteria, "g");
            query.AddTableJoin(join);
            query.AddTableJoin("ckbx_GroupMembers", QueryJoinType.Inner, "GroupID", "g", "GroupID");
            query.AddTableJoin("ckbx_PolicyPermissions", QueryJoinType.Inner, "PolicyID", "ckbx_AccessControlEntry", "PolicyID");
            query.AddTableJoin("ckbx_Permission", QueryJoinType.Inner, "PermissionID", "ckbx_PolicyPermissions", "PermissionID");
            query.AddCriterion(new QueryCriterion(new SelectParameter("EntryType", string.Empty, "ckbx_AccessControlEntry"), CriteriaOperator.EqualTo, new LiteralParameter("'Checkbox.Users.Group'")));
            query.AddCriterion(new QueryCriterion(new SelectParameter("MemberUniqueIdentifier", string.Empty, "ckbx_GroupMembers"), CriteriaOperator.EqualTo, new LiteralParameter("'" + entryIdentifier + "'")));
            string paramValue = string.Empty;
            for (int i = 0; i < permissions.Length; i++)
            {
                if (i > 0)
                {
                    paramValue = paramValue + ",";
                }
                paramValue = paramValue + "'" + permissions[i] + "'";
            }
            query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", string.Empty, "ckbx_Permission"), CriteriaOperator.In, new LiteralParameter(paramValue, string.Empty, true)));
            return query;
        }
    }
}

