﻿namespace Checkbox.Forms.Items.Configuration
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections.ObjectModel;

    [Serializable]
    public abstract class TabularItemData : LabelledItemData, ICompositeItemData
    {
        protected TabularItemData()
        {
        }

        public abstract ReadOnlyCollection<int> GetChildItemDataIDs();
        public abstract ReadOnlyCollection<ItemData> GetChildItemDatas();
        public abstract ItemData GetItemAt(int row, int column);
        public abstract Coordinate GetItemCoordinate(ItemData item);

        public abstract int ColumnCount { get; }

        public abstract int RowCount { get; }
    }
}

