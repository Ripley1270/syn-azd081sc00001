﻿namespace Checkbox.Globalization.Configuration
{
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections;
    using System.Runtime.CompilerServices;
    using System.Xml;

    public class GlobalizationConfiguration : ConfigurationBase, IXmlConfigurationBase
    {
        private Hashtable _textProviders;

        public GlobalizationConfiguration() : this(string.Empty)
        {
        }

        public GlobalizationConfiguration(string sectionName) : base(sectionName)
        {
            try
            {
                this._textProviders = new Hashtable();
                this.DefaultTextProvider = string.Empty;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        public ProviderData GetTextProviderConfig(string providerName)
        {
            try
            {
                ArgumentValidation.CheckForEmptyString(providerName, "providerName");
                if (!this._textProviders.Contains(providerName))
                {
                    throw new Exception("No configuration for specified text provider exists.  Specified provider was: " + providerName);
                }
                return (ProviderData) this._textProviders[providerName];
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return null;
            }
        }

        public void LoadFromXml(XmlNode node)
        {
            try
            {
                ArgumentValidation.CheckForNullReference(node, "node");
                this.DefaultLanguage = XmlUtility.GetNodeText(node.SelectSingleNode("/globalizationConfiguration/defaultLanguage"), true);
                this._textProviders = new Hashtable();
                foreach (XmlNode node2 in node.SelectNodes("/globalizationConfiguration/providers/provider[@type='text']"))
                {
                    string attributeText = XmlUtility.GetAttributeText(node2, "name");
                    string filePath = XmlUtility.GetAttributeText(node2, "filePath");
                    string typeName = XmlUtility.GetAttributeText(node2, "configDataType");
                    bool attributeBool = XmlUtility.GetAttributeBool(node2, "default");
                    if (attributeText == string.Empty)
                    {
                        throw new Exception("A text provider was defined in the globalization configuration file, but the name was not specified: " + attributeText);
                    }
                    if (filePath == string.Empty)
                    {
                        throw new Exception("A text provider was defined in the globalization configuration file, but the provider's configuration file path was not specified: " + attributeText);
                    }
                    if (typeName == string.Empty)
                    {
                        throw new Exception("A text provider was defined in the globalization configuration file, but the provider's configuration object type name was not specified: " + attributeText);
                    }
                    object[] extraParams = new object[] { attributeText };
                    ProviderData data = (ProviderData) ConfigurationManager.GetConfiguration(filePath, typeName, extraParams);
                    if (data == null)
                    {
                        throw new Exception("Authentication provider [" + attributeText + "] was specified but it's configuration could not be loaded: " + attributeText);
                    }
                    this._textProviders.Add(data.Name, data);
                    if (attributeBool)
                    {
                        this.DefaultTextProvider = data.Name;
                    }
                }
                this.ApplicationLanguages = new ArrayList();
                foreach (XmlNode node3 in node.SelectNodes("/globalizationConfiguration/applicationLanguages/language"))
                {
                    if ((node3.InnerText != null) && (node3.InnerText.Trim() != string.Empty))
                    {
                        this.ApplicationLanguages.Add(node3.InnerText);
                    }
                }
                this.SurveyLanguages = new ArrayList();
                foreach (XmlNode node4 in node.SelectNodes("/globalizationConfiguration/surveyLanguages/language"))
                {
                    if ((node4.InnerText != null) && (node4.InnerText.Trim() != string.Empty))
                    {
                        this.SurveyLanguages.Add(node4.InnerText);
                    }
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        public ArrayList ApplicationLanguages { get; private set; }

        public string DefaultLanguage { get; private set; }

        public string DefaultTextProvider { get; set; }

        public ArrayList SurveyLanguages { get; private set; }
    }
}

