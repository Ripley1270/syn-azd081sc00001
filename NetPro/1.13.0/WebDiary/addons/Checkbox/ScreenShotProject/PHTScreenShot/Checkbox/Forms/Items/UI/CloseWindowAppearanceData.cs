﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class CloseWindowAppearanceData : AppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "CLOSE_WINDOW";
            }
        }
    }
}

