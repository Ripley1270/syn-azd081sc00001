﻿namespace Checkbox.Forms.Logic
{
    using System;

    public interface IOperandComparer
    {
        bool Compare(Operand left, LogicalOperator operation, Operand right);
    }
}

