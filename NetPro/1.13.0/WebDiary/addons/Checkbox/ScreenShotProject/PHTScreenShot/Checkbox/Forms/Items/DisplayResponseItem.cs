﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Globalization.Text;
    using Checkbox.Management;
    using Checkbox.Security;
    using System;
    using System.Collections.Specialized;
    using System.Text;

    [Serializable]
    public class DisplayResponseItem : ResponseItem
    {
        private bool _displayInline;
        private string _editorText;
        private string _linkText;
        private string _linkUrl;

        public override void Configure(ItemData configuration, string languageCode)
        {
            base.Configure(configuration, languageCode);
            this._displayInline = ((DisplayResponseItemData) configuration).DisplayInlineResponse;
            this._editorText = TextManager.GetText("/itemText/displayResponseItem/editorText", languageCode);
            this._linkText = this.GetText(((DisplayResponseItemData) configuration).LinkTextID);
            if ((this._editorText == null) || (this._editorText.Trim() == string.Empty))
            {
                this._editorText = TextManager.GetText("/itemText/displayResponseItem/editorText", TextManager.DefaultLanguage);
            }
        }

        protected override NameValueCollection GetInstanceDataValuesForXmlSerialization()
        {
            NameValueCollection instanceDataValuesForXmlSerialization = base.GetInstanceDataValuesForXmlSerialization();
            instanceDataValuesForXmlSerialization["LinkText"] = this.GetLinkText();
            instanceDataValuesForXmlSerialization["LinkUrl"] = this.GetLinkUrl();
            instanceDataValuesForXmlSerialization["ResponseHtml"] = this.GetResponseHtml();
            return instanceDataValuesForXmlSerialization;
        }

        private string GetLinkText()
        {
            return this.GetPipedText("LinkText", this._linkText);
        }

        private string GetLinkUrl()
        {
            if (Utilities.IsNullOrEmpty(this._linkUrl))
            {
                if (base.Response == null)
                {
                    return "#";
                }
                StringBuilder builder = new StringBuilder();
                Ticketing.CreateTicket(base.Response.GUID.Value, DateTime.Now.AddMinutes((double) ApplicationManager.AppSettings.ViewReportTicketDuration));
                builder.Append("javascript:void window.open('");
                builder.Append(ApplicationManager.ApplicationRoot);
                builder.Append("/Analytics/Data/ViewResponseDetails.aspx?responseGuid=");
                builder.Append(base.Response.GUID);
                builder.Append("&noChrome=true', 'Response', 'menubar=no,toolbar=no,scrollbars=yes')");
                this._linkUrl = builder.ToString();
            }
            return this._linkUrl;
        }

        protected override NameValueCollection GetMetaDataValuesForXmlSerialization()
        {
            NameValueCollection metaDataValuesForXmlSerialization = base.GetMetaDataValuesForXmlSerialization();
            metaDataValuesForXmlSerialization["DisplayInForm"] = this.DisplayInlineResponse.ToString();
            return metaDataValuesForXmlSerialization;
        }

        public string GetResponseHtml()
        {
            StringBuilder builder = new StringBuilder();
            if (!this._displayInline)
            {
                builder.Append("<br /><a href=\"");
                builder.Append(this.GetLinkUrl());
                builder.Append("\">");
                builder.Append(this.GetLinkText());
                builder.Append("</a>");
            }
            else if (base.Response == null)
            {
                builder.Append("<br /><div style=\"padding:5px;font-family:Verdana,Arial;font-size:10px;color:black;\">");
                builder.Append(this._editorText);
                builder.Append("</div><br />");
            }
            else
            {
                builder.Append(this.GetText("/itemText/displayResponseItem/surveyTitle"));
                builder.Append(" ");
                builder.Append(base.Response.Title);
                builder.AppendLine("<br />");
                if ((base.Response.Description != null) && (base.Response.Description != string.Empty))
                {
                    builder.Append(base.Response.Description);
                    builder.AppendLine("<br />");
                }
                builder.Append(this.GetText("/itemText/displayResponseItem/responseGUID"));
                builder.Append(" ");
                builder.Append(base.Response.GUID.ToString());
                builder.AppendLine("<br />");
                builder.Append(this.GetText("/itemText/displayResponseItem/responseStarted"));
                builder.Append(" ");
                builder.Append(base.Response.DateCreated.ToString());
                builder.AppendLine("<br />");
                builder.Append(this.GetText("/itemText/displayResponseItem/responseCompleted"));
                builder.Append(" ");
                builder.Append(base.Response.LastModified.ToString());
                builder.AppendLine("<br /><br />");
            }
            return builder.ToString();
        }

        public bool DisplayInlineResponse
        {
            get
            {
                return this._displayInline;
            }
        }
    }
}

