﻿namespace Checkbox.Configuration.Install
{
    using System;

    [Serializable]
    public class InstallFileInfo
    {
        private string _destinationPath;
        private string _sourcePath;

        internal InstallFileInfo(string sourcePath, string destinationPath)
        {
            this._sourcePath = sourcePath;
            this._destinationPath = destinationPath;
        }

        public string DestinationPath
        {
            get
            {
                return this._destinationPath;
            }
        }

        public string SourcePath
        {
            get
            {
                return this._sourcePath;
            }
        }
    }
}

