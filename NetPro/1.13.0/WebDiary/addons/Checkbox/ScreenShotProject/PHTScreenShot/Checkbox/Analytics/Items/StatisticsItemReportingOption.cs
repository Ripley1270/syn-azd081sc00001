﻿namespace Checkbox.Analytics.Items
{
    using System;

    [Serializable]
    public enum StatisticsItemReportingOption
    {
        Responses,
        Mean,
        Median,
        Mode,
        StdDeviation,
        All
    }
}

