﻿namespace Checkbox.Forms.Logic
{
    using System;

    [Flags]
    public enum OperandDataType
    {
        Currency = 0x10,
        Date = 8,
        Double = 4,
        Integer = 2,
        Option = 0x20,
        Other = 0x40,
        String = 1
    }
}

