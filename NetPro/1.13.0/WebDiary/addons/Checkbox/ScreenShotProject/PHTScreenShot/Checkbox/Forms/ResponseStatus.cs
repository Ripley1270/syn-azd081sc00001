﻿namespace Checkbox.Forms
{
    using System;

    public enum ResponseStatus
    {
        COMPLETED = 2,
        DELETED = 4,
        EDITING = 3,
        INPROGRESS = 1
    }
}

