﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Logic;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class OperandData : AbstractPersistedDomainObject
    {
        private static readonly object EventValidationFailed = new object();

        public event EventHandler ValidationFailed
        {
            add
            {
                base.Events.AddHandler(EventValidationFailed, value);
            }
            remove
            {
                base.Events.RemoveHandler(EventValidationFailed, value);
            }
        }

        protected OperandData(ResponseTemplate context)
        {
            this.Context = context;
        }

        protected override void Create(IDbTransaction t)
        {
            DataRow row = this.Context.OperandTable.NewRow();
            row["TypeName"] = base.GetType().FullName;
            row["TypeAssembly"] = base.GetType().Assembly.GetName().Name;
            this.Context.OperandTable.Rows.Add(row);
            base.ID = new int?((int) row["OperandID"]);
        }

        public abstract Operand CreateOperand(Response context, string languageCode);
        public override void Delete(IDbTransaction transaction)
        {
            if (base.ID.HasValue)
            {
                DataRow[] rowArray = this.Context.OperandTable.Select(this.IdentityColumnName + "=" + base.ID.Value);
                if (rowArray.Length > 0)
                {
                    rowArray[0].Delete();
                }
            }
        }

        protected override void Load(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            if (this.DataTableName == data.Table.TableName)
            {
                this.LoadFromDataRow(data);
            }
            else
            {
                bool flag1 = data.Table.TableName == this.ParentDataTableName;
            }
        }

        protected virtual void OnValidationFailed()
        {
            EventHandler handler = (EventHandler) base.Events[EventValidationFailed];
            if (handler != null)
            {
                handler(this, null);
            }
        }

        protected override void Update(IDbTransaction t)
        {
        }

        public abstract void Validate();

        protected override DBCommandWrapper ConfigurationDataSetCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Rules_GetOperand");
                storedProcCommandWrapper.AddInParameter("OperandID", DbType.Int32, base.ID);
                return storedProcCommandWrapper;
            }
        }

        public ResponseTemplate Context { get; set; }

        public override string IdentityColumnName
        {
            get
            {
                return "OperandID";
            }
        }

        public abstract string OperandTypeName { get; }

        public override string ParentDataTableName
        {
            get
            {
                return "Operand";
            }
        }

        internal int SetID
        {
            set
            {
                base.ID = new int?(value);
            }
        }

        public virtual Array SupportedLogicalOperators
        {
            get
            {
                return Enum.GetNames(typeof(LogicalOperator));
            }
        }
    }
}

