﻿namespace Checkbox.Analytics.Items.UI
{
    using System;

    [Serializable]
    public class AverageScoreItemAppearanceData : AnalysisItemAppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "ANALYSIS_AVERAGE_SCORE";
            }
        }
    }
}

