﻿namespace Checkbox.Security.IdentityFilters
{
    using Prezza.Framework.Common;
    using System;

    public abstract class IdentityFilter
    {
        private readonly string _filterProperty;
        private readonly IdentityFilterPropertyType _filterPropertyType;
        private readonly IdentityFilterType _filterType;

        protected IdentityFilter(IdentityFilterType filterType, IdentityFilterPropertyType filterPropertyType, string filterProperty)
        {
            ArgumentValidation.CheckForEmptyString(filterProperty, "_filterProperty");
            this._filterType = filterType;
            this._filterPropertyType = filterPropertyType;
            this._filterProperty = filterProperty;
        }

        public IdentityFilterType FilterType
        {
            get
            {
                return this._filterType;
            }
        }

        public string PropertyName
        {
            get
            {
                return this._filterProperty;
            }
        }

        public IdentityFilterPropertyType PropertyType
        {
            get
            {
                return this._filterPropertyType;
            }
        }
    }
}

