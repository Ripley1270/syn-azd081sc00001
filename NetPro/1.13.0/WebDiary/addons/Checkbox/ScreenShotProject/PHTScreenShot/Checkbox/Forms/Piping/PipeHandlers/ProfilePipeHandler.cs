﻿namespace Checkbox.Forms.Piping.PipeHandlers
{
    using Checkbox.Forms.Piping.Tokens;
    using Checkbox.Users;
    using System;
    using System.Security.Principal;

    public class ProfilePipeHandler : PipeHandler
    {
        public override string GetTokenValue(Token token, object context)
        {
            string cleanName = token.CleanName;
            if (((token is ProfileToken) && (context is IPrincipal)) && (context != null))
            {
                object profileProperty = UserManager.GetProfileProperty(((IPrincipal) context).Identity, cleanName);
                if (profileProperty == null)
                {
                    return string.Empty;
                }
                return profileProperty.ToString();
            }
            if (((token is ProfileToken) && (context is IIdentity)) && (context != null))
            {
                object obj3 = UserManager.GetProfileProperty((IIdentity) context, cleanName);
                if ((obj3 == null) && cleanName.Contains("_"))
                {
                    obj3 = UserManager.GetProfileProperty(((IPrincipal) context).Identity, cleanName.Replace("_", " "));
                }
                if (obj3 != null)
                {
                    return obj3.ToString();
                }
            }
            return string.Empty;
        }
    }
}

