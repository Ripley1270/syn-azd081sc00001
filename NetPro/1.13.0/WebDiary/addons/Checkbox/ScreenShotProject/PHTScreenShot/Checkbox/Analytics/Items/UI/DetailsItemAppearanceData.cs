﻿namespace Checkbox.Analytics.Items.UI
{
    using System;

    [Serializable]
    public class DetailsItemAppearanceData : AnalysisItemAppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "ANALYSIS_DETAILS";
            }
        }

        public override Checkbox.Analytics.Items.UI.GraphType GraphType
        {
            get
            {
                return Checkbox.Analytics.Items.UI.GraphType.SummaryTable;
            }
            set
            {
            }
        }
    }
}

