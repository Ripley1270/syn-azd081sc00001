﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Checkbox.Forms;
    using Checkbox.Forms.Items.Configuration;
    using System;

    [Serializable]
    public class IncludeExcludeActionData : ActionData
    {
        public IncludeExcludeActionData(ResponseTemplate context, ItemData receiver) : base(context, receiver)
        {
        }

        public IncludeExcludeActionData(ResponseTemplate context, TemplatePage receiver) : base(context, receiver)
        {
        }

        public override string ToString()
        {
            string str = typeof(ItemData).IsAssignableFrom(base.Receiver.GetType()) ? "Item" : "Page";
            return ("Display " + str);
        }
    }
}

