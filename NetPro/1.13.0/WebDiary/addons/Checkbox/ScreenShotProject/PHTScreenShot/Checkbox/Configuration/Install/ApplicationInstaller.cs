﻿namespace Checkbox.Configuration.Install
{
    using Checkbox.Common;
    using Checkbox.Configuration;
    using Checkbox.Management;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Xml;

    [Serializable]
    public class ApplicationInstaller
    {
        private List<ModuleInfo> _allModules;
        private List<PatchInfo> _allPatches;
        private string _applicationRoot;
        private string _applicationUrl;
        private string _connectionString;
        private List<string> _dbInstallScripts;
        private string _installedVersion;
        private List<InstallFileInfo> _installFiles;
        private readonly bool _newInstall;
        private readonly string _rootFolder;
        private string _version;

        public ApplicationInstaller(string rootFolder) : this(rootFolder, false, null)
        {
        }

        public ApplicationInstaller(string root, bool forceNew, string provider)
        {
            if (string.IsNullOrEmpty(root))
            {
                throw new Exception("Application Root was not specified.");
            }
            this._rootFolder = root;
            this.DatabaseProvider = provider;
            if (forceNew || (ApplicationManager.ApplicationURL.Trim() == string.Empty))
            {
                this._newInstall = true;
            }
            else
            {
                this._newInstall = false;
            }
            this.LoadInstallFilesAndScripts();
            this.LoadExistingInstallInformation();
        }

        private static bool CheckPrerequisite(string preReqName, List<string> preReqVersions, string installedProductName, string installedProductVersion, bool requireVersionMatch)
        {
            if (string.IsNullOrEmpty(preReqName))
            {
                return true;
            }
            if (string.Compare(preReqName, installedProductName, true) == 0)
            {
                if (preReqName.Trim() == string.Empty)
                {
                    return true;
                }
                foreach (string str in preReqVersions)
                {
                    if (CompareVersions(str, installedProductVersion, requireVersionMatch))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static bool CompareVersions(string preReqVersion, string installedProductVersion, bool requireExactMatch)
        {
            string[] strArray = installedProductVersion.Split(new char[] { '.' });
            string[] strArray2 = preReqVersion.Split(new char[] { '.' });
            if (strArray.Length != strArray2.Length)
            {
                return false;
            }
            for (int i = 0; i < strArray2.Length; i++)
            {
                int num2;
                int num3;
                if (!int.TryParse(strArray2[i], out num2))
                {
                    return false;
                }
                if (!int.TryParse(strArray[i], out num3))
                {
                    return false;
                }
                if ((requireExactMatch && (num2 != num3)) || (num2 > num3))
                {
                    return false;
                }
            }
            return true;
        }

        public List<string> CreateDbInstallSelector(string rootPath)
        {
            List<string> list = new List<string>();
            string path = rootPath + string.Format("{0}Install{0}InstallScripts{0}", Path.DirectorySeparatorChar);
            foreach (string str2 in Directory.GetDirectories(path))
            {
                if ((str2.Replace(path, "").ToLower() != "upgrade") && (Directory.GetFiles(str2, "*.sql").Length > 0))
                {
                    list.Add(str2.Replace(path, ""));
                }
            }
            return list;
        }

        private void ExecuteSqlString(string sql)
        {
            if ((sql != null) && (sql.Trim() != string.Empty))
            {
                string databaseProvider = this.DatabaseProvider;
                IDbConnection connection = new SqlConnection(this._connectionString);
                IDbCommand command = new SqlCommand(sql, (SqlConnection) connection);
                try
                {
                    connection.Open();
                    command.CommandTimeout = 0;
                    command.ExecuteNonQuery();
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public ModuleInfo GetAvailableModuleInfo(string moduleName)
        {
            foreach (ModuleInfo info in this.AvailableModules)
            {
                if (info.CompareTo(moduleName) == 0)
                {
                    return info;
                }
            }
            return null;
        }

        public ModuleInfo GetInstalledModuleInfo(string moduleName)
        {
            foreach (ModuleInfo info in this.InstalledModules)
            {
                if (info.CompareTo(moduleName) == 0)
                {
                    return info;
                }
            }
            return null;
        }

        public static bool IsModuleInstalled(string productName, string moduleName)
        {
            bool flag = false;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Product_GetInfo");
            storedProcCommandWrapper.AddInParameter("ProductName", DbType.String, productName);
            int num = -1;
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        num = Convert.ToInt32(reader["ProductID"]);
                    }
                }
                catch (Exception exception)
                {
                    ExceptionPolicy.HandleException(exception, "BusinessPublic");
                }
                finally
                {
                    reader.Close();
                }
            }
            storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Product_GetModules");
            storedProcCommandWrapper.AddInParameter("ProductID", DbType.Int32, num);
            using (IDataReader reader2 = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    try
                    {
                        while (reader2.Read())
                        {
                            if (string.Compare((string) reader2["ModuleName"], moduleName, true) == 0)
                            {
                                flag = true;
                            }
                        }
                    }
                    catch (Exception exception2)
                    {
                        ExceptionPolicy.HandleException(exception2, "BusinessPublic");
                    }
                    return flag;
                }
                finally
                {
                    reader2.Close();
                }
            }
            return flag;
        }

        protected void LoadExistingInstallInformation()
        {
            this._allModules = ModuleInfo.GetAvailableModules(this._rootFolder + Path.DirectorySeparatorChar + "Install");
            this._allPatches = PatchInfo.GetAvailablePatches(this._rootFolder + Path.DirectorySeparatorChar + "Install");
            this.InstalledModules = new List<ModuleInfo>();
            this.InstalledPatches = new List<PatchInfo>();
            if (!this._newInstall)
            {
                try
                {
                    Database database = DatabaseFactory.CreateDatabase();
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Product_GetInfo");
                    storedProcCommandWrapper.AddInParameter("ProductName", DbType.String, this.Name);
                    using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                    {
                        try
                        {
                            if (reader.Read())
                            {
                                this._installedVersion = (string) reader["Version"];
                                Convert.ToDateTime(reader["InstallDate"]);
                                this.InstalledProductID = Convert.ToInt32(reader["ProductID"]);
                            }
                        }
                        catch (Exception exception)
                        {
                            ExceptionPolicy.HandleException(exception, "BusinessPublic");
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                    storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Product_GetModules");
                    storedProcCommandWrapper.AddInParameter("ProductID", DbType.Int32, this.InstalledProductID);
                    using (IDataReader reader2 = database.ExecuteReader(storedProcCommandWrapper))
                    {
                        try
                        {
                            while (reader2.Read())
                            {
                                this.InstalledModules.Add(ModuleInfo.GetModuleInfo((string) reader2["ModuleName"], (string) reader2["Version"], this._rootFolder));
                            }
                        }
                        catch (Exception exception2)
                        {
                            ExceptionPolicy.HandleException(exception2, "BusinessPublic");
                        }
                        finally
                        {
                            reader2.Close();
                        }
                    }
                    storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Product_GetPatches");
                    storedProcCommandWrapper.AddInParameter("ProductID", DbType.Int32, this.InstalledProductID);
                    using (IDataReader reader3 = database.ExecuteReader(storedProcCommandWrapper))
                    {
                        try
                        {
                            while (reader3.Read())
                            {
                                this.InstalledPatches.Add(PatchInfo.GetPatchInfo((string) reader3["PatchName"], (string) reader3["Version"]));
                            }
                        }
                        catch (Exception exception3)
                        {
                            ExceptionPolicy.HandleException(exception3, "BusinessPublic");
                        }
                        finally
                        {
                            reader3.Close();
                        }
                    }
                }
                catch (Exception exception4)
                {
                    if (this.InstallType != "upgrade")
                    {
                        throw new Exception("An error occurred while attempting to get information about the current installation: " + exception4.Message);
                    }
                }
            }
        }

        public virtual void LoadInstallFilesAndScripts()
        {
            XmlDocument document = new XmlDocument();
            document.Load(string.Concat(new object[] { this._rootFolder, Path.DirectorySeparatorChar, "Install", Path.DirectorySeparatorChar, "ProductInfo.xml" }));
            this.Name = XmlUtility.GetNodeText(document.SelectSingleNode("/productInfo/properties/name"), true);
            this._version = XmlUtility.GetNodeText(document.SelectSingleNode("/productInfo/properties/version"), true);
            this._installFiles = new List<InstallFileInfo>();
            foreach (XmlNode node in document.SelectNodes("/productInfo/" + this.InstallType + "/files/file"))
            {
                string nodeText = XmlUtility.GetNodeText(node.SelectSingleNode("source"));
                string str2 = XmlUtility.GetNodeText(node.SelectSingleNode("destination"));
                if ((nodeText.Trim() != string.Empty) && (str2.Trim() != string.Empty))
                {
                    InstallFileInfo item = new InstallFileInfo(this._rootFolder + Path.DirectorySeparatorChar + nodeText, this._rootFolder + Path.DirectorySeparatorChar + str2);
                    this._installFiles.Add(item);
                }
            }
            this._dbInstallScripts = new List<string>();
            foreach (XmlNode node2 in document.SelectNodes("/productInfo/" + this.InstallType + "/dbScripts[@dbProvider='" + this.DatabaseProvider + "']/dbScript"))
            {
                this._dbInstallScripts.Add(this._rootFolder + Path.DirectorySeparatorChar + XmlUtility.GetNodeText(node2).Replace('/', Path.DirectorySeparatorChar));
            }
        }

        public virtual bool RunDBScriptFile(string sqlScript, out string status)
        {
            bool flag;
            try
            {
                if (!File.Exists(sqlScript))
                {
                    status = "A required database installation script was not found:  " + sqlScript;
                    flag = false;
                }
                else
                {
                    StreamReader reader = new StreamReader(sqlScript);
                    StringBuilder builder = new StringBuilder();
                    try
                    {
                        while (!reader.EndOfStream)
                        {
                            string str = reader.ReadLine();
                            if (str != null)
                            {
                                if (string.Compare(str.Trim(), "GO", true) == 0)
                                {
                                    this.ExecuteSqlString(builder.ToString());
                                    builder = new StringBuilder();
                                }
                                else
                                {
                                    builder.AppendLine(str);
                                }
                            }
                        }
                        this.ExecuteSqlString(builder.ToString());
                        status = "Success.";
                        flag = true;
                    }
                    catch (Exception exception)
                    {
                        status = "An error occurred while setting up the database.  The error was:  " + exception.Message;
                        flag = false;
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception exception2)
            {
                status = "An error occurred while setting up the database.  The error was:  " + exception2.Message;
                flag = false;
            }
            return flag;
        }

        public bool SetupDatabase(out string status)
        {
            status = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(this._connectionString))
                {
                    status = "No connection string was specified.";
                    return false;
                }
                foreach (string str in this._dbInstallScripts)
                {
                    if (!File.Exists(str))
                    {
                        status = "A required database installation script was not found:  " + str;
                        return false;
                    }
                }
                foreach (string str2 in this._dbInstallScripts)
                {
                    if (!this.RunDBScriptFile(str2, out status))
                    {
                        return false;
                    }
                }
            }
            catch (Exception exception)
            {
                status = "An error occurred while setting up the database.  The error was:  " + exception.Message;
                return false;
            }
            return true;
        }

        public bool TestForExistingDatabase(string connectionString, out bool v3status)
        {
            bool flag = false;
            v3status = false;
            string databaseProvider = this.DatabaseProvider;
            IDbConnection connection = new SqlConnection(connectionString);
            IDbCommand command = new SqlCommand("SELECT * FROM ckbx_Group WHERE GroupID = 1;", (SqlConnection) connection);
            IDbCommand command2 = new SqlCommand("SELECT * FROM rz_Group WHERE GroupID = 1;", (SqlConnection) connection);
            try
            {
                connection.Open();
                using (IDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        flag = true;
                    }
                }
            }
            catch
            {
            }
            finally
            {
                connection.Close();
            }
            try
            {
                connection.Open();
                using (IDataReader reader2 = command2.ExecuteReader())
                {
                    if (reader2.Read())
                    {
                        v3status = true;
                    }
                }
                return flag;
            }
            catch
            {
            }
            finally
            {
                connection.Close();
            }
            return flag;
        }

        public bool UpdateConfigurationFiles(out string status)
        {
            try
            {
                bool flag = ConfigurationWriter.UpdateConfigFiles(this._rootFolder, this._rootFolder + Path.DirectorySeparatorChar + "Config", this._connectionString, this.DatabaseProvider, out status);
                if (flag)
                {
                    this.UpdateWebConfigSetting("ApplicationRoot", this._applicationRoot);
                    this.UpdateWebConfigSetting("ApplicationURL", this._applicationUrl);
                    this.UpdateWebConfigConnectionStringSetting("DefaultConnectionString", this._connectionString);
                }
                return flag;
            }
            catch (Exception exception)
            {
                status = "An error occurred while updating the configuration files:  " + exception.Message;
                return false;
            }
        }

        private void UpdateWebConfigConnectionStringSetting(string connectionStringName, string settingValue)
        {
            if ((connectionStringName == null) || (connectionStringName.Trim() == string.Empty))
            {
                throw new Exception("No setting name was specified to update.");
            }
            XmlDocument document = new XmlDocument();
            string path = this._rootFolder + Path.DirectorySeparatorChar + "web.config";
            if (!File.Exists(path))
            {
                throw new Exception("Unable to open web.config file: " + path);
            }
            try
            {
                document.Load(path);
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(document.NameTable);
                nsmgr.AddNamespace("ms", "http://schemas.microsoft.com/.NetConfiguration/v2.0");
                XmlNode node = document.SelectSingleNode("/ms:configuration", nsmgr);
                if (node == null)
                {
                    throw new Exception("Unable to find <configuration /> node in web.config " + path);
                }
                XmlNode newChild = document.SelectSingleNode("/ms:configuration/ms:connectionStrings", nsmgr);
                if (newChild == null)
                {
                    newChild = document.CreateElement("connectionStrings", node.NamespaceURI);
                    node.AppendChild(newChild);
                }
                XmlNode node3 = newChild.SelectSingleNode("ms:add[@name='" + connectionStringName + "']", nsmgr);
                if (node3 == null)
                {
                    node3 = document.CreateElement("add", node.NamespaceURI);
                    XmlAttribute attribute = document.CreateAttribute("name");
                    attribute.Value = connectionStringName;
                    node3.Attributes.Append(attribute);
                    newChild.AppendChild(node3);
                }
                if (!settingValue.Contains("Application Name=") && (this.DatabaseProvider.ToLower() == "sqlserver"))
                {
                    settingValue = settingValue + "Application Name=Checkbox Survey Server;";
                }
                XmlAttribute attribute2 = document.CreateAttribute("connectionString");
                attribute2.Value = settingValue;
                node3.Attributes.SetNamedItem(attribute2);
                document.Save(path);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to update web.config at " + path + ". Error was [" + exception.Message + "].");
            }
        }

        private void UpdateWebConfigSetting(string settingName, string settingValue)
        {
            if ((settingName == null) || (settingName.Trim() == string.Empty))
            {
                throw new Exception("No setting name was specified to update.");
            }
            XmlDocument document = new XmlDocument();
            string path = this._rootFolder + Path.DirectorySeparatorChar + "web.config";
            if (!File.Exists(path))
            {
                throw new Exception("Unable to open web.config file: " + path);
            }
            try
            {
                document.Load(path);
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(document.NameTable);
                nsmgr.AddNamespace("ms", "http://schemas.microsoft.com/.NetConfiguration/v2.0");
                XmlNode node = document.SelectSingleNode("/ms:configuration/ms:appSettings", nsmgr);
                if (node == null)
                {
                    throw new Exception("Unable to load appSettings node from web.config " + path);
                }
                XmlNode node2 = node.SelectSingleNode("ms:add[@key='" + settingName + "']", nsmgr);
                if (node2 == null)
                {
                    throw new Exception("AppSetting key " + settingName + " was not found in the web.config " + path);
                }
                XmlAttribute attribute = document.CreateAttribute("value");
                attribute.Value = settingValue;
                node2.Attributes.SetNamedItem(attribute);
                document.Save(path);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to update web.config at " + path + ". Error was [" + exception.Message + "].");
            }
        }

        public static bool WasUpgradedFromUltimateSurvey3()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Install_WasUpgradedFromUS");
            storedProcCommandWrapper.AddOutParameter("WasUpgraded", DbType.Boolean, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("WasUpgraded");
            return ((parameterValue != DBNull.Value) && ((bool) parameterValue));
        }

        public static string ApplicationAssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string ApplicationRoot
        {
            get
            {
                return this._applicationRoot;
            }
            set
            {
                this._applicationRoot = value;
            }
        }

        public string ApplicationURL
        {
            get
            {
                return this._applicationUrl;
            }
            set
            {
                this._applicationUrl = value;
            }
        }

        public List<ModuleInfo> AvailableModules
        {
            get
            {
                List<ModuleInfo> list = new List<ModuleInfo>();
                foreach (ModuleInfo info in this._allModules)
                {
                    bool flag = false;
                    foreach (ModuleInfo info2 in this.InstalledModules)
                    {
                        if (string.Compare(info.Name, info2.Name, true) == 0)
                        {
                            flag = true;
                            break;
                        }
                    }
                    bool flag2 = CheckPrerequisite(info.PreRequisiteProduct, info.PreRequisiteProductVersions, this.ProductName, this.ProductVersion, false);
                    if (!flag && flag2)
                    {
                        list.Add(info);
                    }
                }
                return list;
            }
        }

        public List<PatchInfo> AvailablePatches
        {
            get
            {
                List<PatchInfo> list = new List<PatchInfo>();
                Dictionary<string, PatchInfo> dictionary = new Dictionary<string, PatchInfo>();
                foreach (PatchInfo info in this._allPatches)
                {
                    bool flag = false;
                    bool flag2 = false;
                    foreach (PatchInfo info2 in this.InstalledPatches)
                    {
                        if (string.Compare(info.Name, info2.Name, true) == 0)
                        {
                            flag = true;
                            break;
                        }
                    }
                    bool flag3 = !Utilities.IsNotNullOrEmpty(info.PreRequisiteProduct.Trim()) || info.PreRequisiteProductVersions.Contains(this.Version);
                    if (Utilities.IsNullOrEmpty(info.PreRequisiteModule.Trim()))
                    {
                        flag2 = true;
                    }
                    else
                    {
                        foreach (ModuleInfo info3 in this.InstalledModules)
                        {
                            if (info.PreRequisiteModuleVersions.Contains(info3.Version))
                            {
                                flag2 = true;
                                break;
                            }
                        }
                    }
                    if (flag3 && flag2)
                    {
                        if (flag)
                        {
                            if (dictionary.ContainsKey(info.PreRequisiteProduct + "_" + info.PreRequisiteModule))
                            {
                                string[] strArray = dictionary[info.PreRequisiteProduct + "_" + info.PreRequisiteModule].Version.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                                string[] strArray2 = info.Version.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                                if (strArray.Length == strArray2.Length)
                                {
                                    int index = 0;
                                    foreach (string str in strArray)
                                    {
                                        if (int.Parse(strArray2[index]) < int.Parse(str))
                                        {
                                            dictionary.Remove(info.PreRequisiteProduct + "_" + info.PreRequisiteModule);
                                            break;
                                        }
                                        index++;
                                    }
                                }
                            }
                        }
                        else if (!dictionary.ContainsKey(info.PreRequisiteProduct + "_" + info.PreRequisiteModule))
                        {
                            dictionary[info.PreRequisiteProduct + "_" + info.PreRequisiteModule] = info;
                        }
                        else
                        {
                            string[] strArray3 = dictionary[info.PreRequisiteProduct + "_" + info.PreRequisiteModule].Version.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                            string[] strArray4 = info.Version.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                            if (strArray3.Length == strArray4.Length)
                            {
                                int num2 = 0;
                                foreach (string str2 in strArray3)
                                {
                                    if (int.Parse(strArray4[num2]) > int.Parse(str2))
                                    {
                                        dictionary[info.PreRequisiteProduct + "_" + info.PreRequisiteModule] = info;
                                        break;
                                    }
                                    num2++;
                                }
                            }
                        }
                    }
                }
                foreach (PatchInfo info4 in dictionary.Values)
                {
                    list.Add(info4);
                }
                return list;
            }
        }

        public string ConnectionString
        {
            get
            {
                return this._connectionString;
            }
            set
            {
                this._connectionString = value;
            }
        }

        public string DatabaseProvider { get; set; }

        public List<ModuleInfo> InstalledModules { get; private set; }

        public List<PatchInfo> InstalledPatches { get; private set; }

        public int InstalledProductID { get; private set; }

        protected virtual string InstallType
        {
            get
            {
                return "install";
            }
        }

        public bool IsNewInstall
        {
            get
            {
                return this._newInstall;
            }
        }

        public string Name { get; private set; }

        public string ProductName
        {
            get
            {
                return this.Name;
            }
        }

        public string ProductVersion
        {
            get
            {
                string version = this.Version;
                foreach (PatchInfo info in this.InstalledPatches)
                {
                    if (CompareVersions(version, info.Description, false))
                    {
                        version = info.Description;
                    }
                }
                return version;
            }
        }

        protected string RootFolder
        {
            get
            {
                return this._rootFolder;
            }
        }

        public string Version
        {
            get
            {
                if (this._installedVersion != null)
                {
                    return this._installedVersion;
                }
                return this._version;
            }
        }
    }
}

