﻿namespace Checkbox.Invitations
{
    using System;

    [Serializable]
    public enum RecipientFilter
    {
        All = 1,
        Current = 2,
        Deleted = 8,
        NotResponded = 4,
        OptOut = 7,
        Pending = 6,
        Responded = 3,
        Selected = 5
    }
}

