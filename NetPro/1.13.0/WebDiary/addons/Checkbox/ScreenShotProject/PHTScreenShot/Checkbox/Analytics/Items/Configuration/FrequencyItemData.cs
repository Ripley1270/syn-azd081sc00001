﻿namespace Checkbox.Analytics.Items.Configuration
{
    using Checkbox.Analytics.Items;
    using Checkbox.Forms.Items;
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class FrequencyItemData : AnalysisItemData
    {
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new Exception("Unable to save analysis item.  DataID is <= 0.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertFrequency");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("OtherOption", DbType.String, base.OtherOption.ToString());
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateDataTables(t);
        }

        protected override Item CreateItem()
        {
            return new FrequencyItem();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new Exception("Unable to load analysis item data, no ID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetFrequency");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName, AnalysisItemData.SourceItemsTableName, AnalysisItemData.ResponseTemplatesTableName });
            return dataSet;
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new Exception("Unable to save analysis item.  DataID is <= 0.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateFrequency");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("OtherOption", DbType.String, base.OtherOption.ToString());
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateDataTables(t);
        }

        public override string DataTableName
        {
            get
            {
                return "FrequencyItemData";
            }
        }
    }
}

