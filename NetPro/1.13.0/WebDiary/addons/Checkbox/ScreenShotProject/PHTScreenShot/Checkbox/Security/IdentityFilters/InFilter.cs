﻿namespace Checkbox.Security.IdentityFilters
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections;

    public class InFilter : IdentityFilter
    {
        private ArrayList propertyValues;

        public InFilter(string filterProperty, object[] propertyValues) : base(IdentityFilterType.In, IdentityFilterPropertyType.IdentityProperty, filterProperty)
        {
            ArgumentValidation.CheckForNullReference(propertyValues, "propertyValues");
            this.propertyValues = new ArrayList(propertyValues);
        }

        public InFilter(string filterProperty, object[] propertyValues, IdentityFilterPropertyType propertyType) : base(IdentityFilterType.In, propertyType, filterProperty)
        {
            ArgumentValidation.CheckForNullReference(propertyValues, "propertyValues");
            this.propertyValues = new ArrayList(propertyValues);
        }

        public object[] PropertyValues
        {
            get
            {
                return (object[]) this.propertyValues.ToArray(typeof(object));
            }
        }
    }
}

