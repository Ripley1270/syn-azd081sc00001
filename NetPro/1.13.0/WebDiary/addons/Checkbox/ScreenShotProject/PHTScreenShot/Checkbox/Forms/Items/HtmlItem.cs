﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Management;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Specialized;

    [Serializable]
    public class HtmlItem : ResponseItem
    {
        private string _html;
        private string _inlineCss;

        public override void Configure(ItemData configuration, string languageCode)
        {
            base.Configure(configuration, languageCode);
            ArgumentValidation.CheckExpectedType(configuration, typeof(HtmlItemData));
            HtmlItemData data = (HtmlItemData) configuration;
            this._html = data.HTML;
            this._inlineCss = data.InlineCSS;
        }

        protected override NameValueCollection GetInstanceDataValuesForXmlSerialization()
        {
            NameValueCollection instanceDataValuesForXmlSerialization = base.GetInstanceDataValuesForXmlSerialization();
            instanceDataValuesForXmlSerialization["InlineCss"] = this.InlineCss;
            instanceDataValuesForXmlSerialization["Html"] = this.HTML;
            return instanceDataValuesForXmlSerialization;
        }

        public string HTML
        {
            get
            {
                if ((base.Response == null) && ApplicationManager.AppSettings.DisplayHtmlItemsAsPlainText)
                {
                    return Utilities.StripHtml(this.GetPipedText("HTML", this._html), null);
                }
                return this.GetPipedText("HTML", this._html);
            }
        }

        public string InlineCss
        {
            get
            {
                return this.GetPipedText("CSS", this._inlineCss);
            }
        }
    }
}

