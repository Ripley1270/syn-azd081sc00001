﻿namespace Checkbox.Analytics.Filters
{
    using Checkbox.Analytics.Data;
    using Checkbox.Forms;
    using Checkbox.Forms.Logic;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class AnswerDataObjectFilter : Checkbox.Analytics.Filters.Filter, IAnswerDataObjectFilter
    {
        protected AnswerDataObjectFilter()
        {
        }

        public virtual bool EvaluateFilter(List<ItemAnswer> answers, Dictionary<long, ResponseProperties> responseProperties)
        {
            bool flag = true;
            bool flag2 = false;
            foreach (ItemAnswer answer in answers)
            {
                bool flag3;
                ResponseProperties properties = responseProperties.ContainsKey(answer.ResponseId) ? responseProperties[answer.ResponseId] : null;
                bool flag4 = this.EvaluateFilter(answer, properties, out flag3);
                if (flag4 && (this.Mode == FilterMode.Any))
                {
                    return true;
                }
                flag &= flag4;
                flag2 |= flag3;
            }
            if (!flag2 && this.ValueRequired)
            {
                return false;
            }
            return flag;
        }

        public abstract bool EvaluateFilter(ItemAnswer answer, ResponseProperties responseProperties, out bool answerHasValue);

        protected virtual FilterMode Mode
        {
            get
            {
                if ((((base.Operator != LogicalOperator.Equal) && (base.Operator != LogicalOperator.Answered)) && ((base.Operator != LogicalOperator.Contains) && (base.Operator != LogicalOperator.GreaterThan))) && (((base.Operator != LogicalOperator.GreaterThanEqual) && (base.Operator != LogicalOperator.LessThan)) && (base.Operator != LogicalOperator.LessThanEqual)))
                {
                    return FilterMode.All;
                }
                return FilterMode.Any;
            }
        }

        protected virtual bool ValueRequired
        {
            get
            {
                if ((((base.Operator != LogicalOperator.Contains) && (base.Operator != LogicalOperator.GreaterThan)) && ((base.Operator != LogicalOperator.GreaterThanEqual) && (base.Operator != LogicalOperator.LessThan))) && ((base.Operator != LogicalOperator.LessThanEqual) && (base.Operator != LogicalOperator.Equal)))
                {
                    return false;
                }
                return true;
            }
        }

        [Serializable]
        protected enum FilterMode
        {
            All,
            Any
        }
    }
}

