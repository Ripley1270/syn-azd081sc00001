﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using System;

    public class CaptchaItemValidator : Validator<CaptchaItem>
    {
        private string _errorMessageTextId;

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText(this._errorMessageTextId, languageCode);
        }

        public override bool Validate(CaptchaItem input)
        {
            if (!input.HasAnswer)
            {
                this._errorMessageTextId = "/validationMessages/captchaRequired";
                return false;
            }
            if (!input.Code.Equals(input.GetAnswer(), StringComparison.InvariantCultureIgnoreCase))
            {
                this._errorMessageTextId = "/validationMessages/captchaAnswerDoesNotMatch";
                return false;
            }
            this._errorMessageTextId = string.Empty;
            return true;
        }
    }
}

