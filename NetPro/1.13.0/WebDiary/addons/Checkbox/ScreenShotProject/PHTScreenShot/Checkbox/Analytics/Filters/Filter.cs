﻿namespace Checkbox.Analytics.Filters
{
    using Checkbox.Analytics.Filters.Configuration;
    using Checkbox.Forms.Logic;
    using System;

    [Serializable]
    public abstract class Filter
    {
        private int _filterId;
        private string _filterText;
        private string _languageCode;
        private LogicalOperator _operator;
        private object _value;

        protected Filter()
        {
        }

        protected virtual bool CompareValue(object valueToCompare)
        {
            string str = null;
            string str2 = null;
            if (this.Value != null)
            {
                str2 = this.Value.ToString();
            }
            if (valueToCompare != null)
            {
                str = valueToCompare.ToString();
            }
            StringOperand left = new StringOperand(str);
            StringOperand right = new StringOperand(str2);
            return OperandComparer.Compare(left, this.Operator, right, RuleEventTrigger.Load);
        }

        public virtual void Configure(FilterData filterData, string languageCode)
        {
            this._filterId = filterData.ID.Value;
            this._value = filterData.Value;
            this._operator = filterData.Operator;
            this._languageCode = languageCode;
            this._filterText = filterData.ToString(languageCode);
        }

        public int FilterId
        {
            get
            {
                return this._filterId;
            }
        }

        public string FilterText
        {
            get
            {
                return this._filterText;
            }
        }

        public LogicalOperator Operator
        {
            get
            {
                return this._operator;
            }
            set
            {
                this._operator = value;
            }
        }

        public object Value
        {
            get
            {
                return this._value;
            }
        }
    }
}

