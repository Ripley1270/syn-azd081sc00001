﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using System;
    using System.Collections.Generic;

    public class ItemAnswerUniqueValidator : Validator<List<IAnswerable>>
    {
        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/answerUniqueValidator/validationError", languageCode);
        }

        public override bool Validate(List<IAnswerable> itemsToValidate)
        {
            List<string> list = new List<string>();
            foreach (IAnswerable answerable in itemsToValidate)
            {
                if (answerable.HasAnswer)
                {
                    string item = answerable.GetAnswer().ToLower();
                    if (list.Contains(item))
                    {
                        return false;
                    }
                    list.Add(item);
                }
            }
            return true;
        }
    }
}

