﻿namespace Checkbox.Security
{
    using Checkbox.Security.IdentityFilters;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using System;

    internal class IdentityFilterToQueryCriteriaInterpreter
    {
        private IdentityFilterToQueryCriteriaInterpreter()
        {
        }

        private static string GetLiteral(object literalValue)
        {
            if (literalValue is string)
            {
                return ("'" + literalValue.ToString().Replace("'", "''") + "'");
            }
            return literalValue.ToString();
        }

        private static string GetLiteral(object[] literalValues)
        {
            string str = string.Empty;
            for (int i = 0; i < literalValues.Length; i++)
            {
                if (i > 0)
                {
                    str = str + ",";
                }
                if (literalValues[i] != null)
                {
                    str = str + GetLiteral(literalValues[i]);
                }
                else
                {
                    str = str + "NULL";
                }
            }
            return str;
        }

        private static QueryCriterion Translate(EqualsFilter filter, string tableName)
        {
            if (filter.PropertyValue == null)
            {
                return new QueryCriterion(new SelectParameter(filter.PropertyName, string.Empty, tableName), CriteriaOperator.Is, new LiteralParameter("NULL"));
            }
            return new QueryCriterion(new SelectParameter(filter.PropertyName, string.Empty, tableName), CriteriaOperator.EqualTo, new LiteralParameter(GetLiteral(filter.PropertyValue)));
        }

        private static QueryCriterion Translate(IdentityFilter filter, string tableName)
        {
            if (filter is EqualsFilter)
            {
                return Translate((EqualsFilter) filter, tableName);
            }
            if (filter is NotEqualsFilter)
            {
                return Translate((NotEqualsFilter) filter, tableName);
            }
            if (filter is InFilter)
            {
                return Translate((InFilter) filter, tableName);
            }
            if (filter is NotInFilter)
            {
                return Translate((NotInFilter) filter, tableName);
            }
            if (filter is LikeFilter)
            {
                return Translate((LikeFilter) filter, tableName);
            }
            if (filter is NotLikeFilter)
            {
                return Translate((NotLikeFilter) filter, tableName);
            }
            if (filter is NullFilter)
            {
                return Translate((NullFilter) filter, tableName);
            }
            if (!(filter is NotNullFilter))
            {
                throw new NotSupportedException("Unable to translate filter to query for unknown filter type: " + filter.GetType().ToString());
            }
            return Translate((NotNullFilter) filter, tableName);
        }

        private static QueryCriterion Translate(InFilter filter, string tableName)
        {
            return new QueryCriterion(new SelectParameter(filter.PropertyName, string.Empty, tableName), CriteriaOperator.In, new LiteralParameter(GetLiteral(filter.PropertyValues), string.Empty, true));
        }

        private static QueryCriterion Translate(LikeFilter filter, string tableName)
        {
            if (filter.PropertyValue != null)
            {
                return new QueryCriterion(new SelectParameter(filter.PropertyName, string.Empty, tableName), CriteriaOperator.Like, new LiteralParameter("'%" + filter.PropertyValue.ToString() + "%'"));
            }
            return new QueryCriterion(new SelectParameter(filter.PropertyName, string.Empty, tableName), CriteriaOperator.Like, new LiteralParameter("'%" + string.Empty + "%'"));
        }

        private static QueryCriterion Translate(NotEqualsFilter filter, string tableName)
        {
            if (filter.PropertyValue == null)
            {
                return new QueryCriterion(new SelectParameter(filter.PropertyName, string.Empty, tableName), CriteriaOperator.IsNot, new LiteralParameter("NULL"));
            }
            return new QueryCriterion(new SelectParameter(filter.PropertyName, string.Empty, tableName), CriteriaOperator.NotEqualTo, new LiteralParameter(GetLiteral(filter.PropertyValue)));
        }

        private static QueryCriterion Translate(NotInFilter filter, string tableName)
        {
            return new QueryCriterion(new SelectParameter(filter.PropertyName, string.Empty, tableName), CriteriaOperator.NotIn, new LiteralParameter(GetLiteral(filter.PropertyValues), string.Empty, true));
        }

        private static QueryCriterion Translate(NotLikeFilter filter, string tableName)
        {
            if (filter.PropertyValue != null)
            {
                return new QueryCriterion(new SelectParameter(filter.PropertyName, string.Empty, tableName), CriteriaOperator.NotLike, new LiteralParameter("'%" + filter.PropertyValue.ToString() + "%'"));
            }
            return new QueryCriterion(new SelectParameter(filter.PropertyName, string.Empty, tableName), CriteriaOperator.NotLike, new LiteralParameter("'%" + string.Empty + "%'"));
        }

        private static QueryCriterion Translate(NotNullFilter filter, string tableName)
        {
            return new QueryCriterion(new SelectParameter(filter.PropertyName, string.Empty, tableName), CriteriaOperator.IsNot, new LiteralParameter("NULL"));
        }

        private static QueryCriterion Translate(NullFilter filter, string tableName)
        {
            return new QueryCriterion(new SelectParameter(filter.PropertyName, string.Empty, tableName), CriteriaOperator.Is, new LiteralParameter("NULL"));
        }

        internal static CriteriaCollection Translate(IdentityFilterCollection filterCollection, string identityTableName, string profileTableName)
        {
            return Translate(filterCollection, identityTableName, profileTableName, string.Empty, string.Empty, string.Empty);
        }

        private static CriteriaCollection Translate(IdentityFilter filter, string propertyValueTable, string propertyValueColumn, string propertyMapTable, string propertyNameColumn)
        {
            QueryCriterion criterion = new QueryCriterion(new SelectParameter(propertyNameColumn, string.Empty, propertyMapTable), CriteriaOperator.EqualTo, new LiteralParameter("'" + filter.PropertyName + "'"));
            QueryCriterion criterion2 = Translate(filter, propertyMapTable);
            criterion2.Left = new SelectParameter(propertyValueColumn, string.Empty, propertyValueTable);
            CriteriaCollection criterias = new CriteriaCollection(CriteriaJoinType.And);
            criterias.AddCriterion(criterion);
            criterias.AddCriterion(criterion2);
            return criterias;
        }

        internal static CriteriaCollection Translate(IdentityFilterCollection filterCollection, string identityTableName, string profileValueTable, string profileValueColumn, string profileMapTable, string profileNameColumn)
        {
            CriteriaCollection criterias;
            if (filterCollection == null)
            {
                return null;
            }
            if (filterCollection.Joiner == IdentityFilterJoin.And)
            {
                criterias = new CriteriaCollection(CriteriaJoinType.And);
            }
            else
            {
                criterias = new CriteriaCollection(CriteriaJoinType.Or);
            }
            foreach (IdentityFilter filter in filterCollection.Filters)
            {
                if (filter.PropertyType == IdentityFilterPropertyType.IdentityProperty)
                {
                    criterias.AddCriterion(Translate(filter, identityTableName));
                }
                else if (filter.PropertyType == IdentityFilterPropertyType.ProfileProperty)
                {
                    if (profileMapTable == string.Empty)
                    {
                        criterias.AddCriterion(Translate(filter, profileValueTable));
                    }
                    else
                    {
                        criterias.AddCriteriaCollection(Translate(filter, profileValueTable, profileValueColumn, profileMapTable, profileNameColumn));
                    }
                }
                else
                {
                    criterias.AddCriterion(Translate(filter, string.Empty));
                }
            }
            foreach (IdentityFilterCollection filters in filterCollection.FilterCollections)
            {
                CriteriaCollection collection = Translate(filters, identityTableName, profileValueTable, profileValueColumn, profileMapTable, profileNameColumn);
                criterias.AddCriteriaCollection(collection);
            }
            return criterias;
        }
    }
}

