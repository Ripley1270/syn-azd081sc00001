﻿namespace Checkbox.Globalization.Text
{
    using Checkbox.Globalization.Configuration;
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.ExceptionHandling;
    using System;

    public class TextProviderFactory : ProviderFactory
    {
        public TextProviderFactory(string factoryName) : base(factoryName, typeof(ITextProvider))
        {
            try
            {
                ArgumentValidation.CheckForEmptyString(factoryName, "factoryName");
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        public TextProviderFactory(string factoryName, GlobalizationConfiguration config) : base(factoryName, typeof(ITextProvider), config)
        {
            try
            {
                ArgumentValidation.CheckForEmptyString(factoryName, "factoryName");
                ArgumentValidation.CheckForNullReference(config, "config");
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        protected override ConfigurationBase GetConfigurationObject(string providerName)
        {
            ArgumentValidation.CheckForEmptyString(providerName, "providerName");
            GlobalizationConfiguration config = (GlobalizationConfiguration) base.Config;
            return config.GetTextProviderConfig(providerName);
        }

        protected override Type GetConfigurationType(string textProviderName)
        {
            ArgumentValidation.CheckForEmptyString(textProviderName, "textProviderName");
            ProviderData configurationObject = (ProviderData) this.GetConfigurationObject(textProviderName);
            return base.GetType(configurationObject.TypeName);
        }

        protected override string GetDefaultInstanceName()
        {
            GlobalizationConfiguration config = (GlobalizationConfiguration) base.Config;
            return config.DefaultTextProvider;
        }

        public ITextProvider GetTextProvider()
        {
            try
            {
                return (ITextProvider) base.CreateDefaultInstance();
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return null;
            }
        }

        public ITextProvider GetTextProvider(string providerName)
        {
            try
            {
                ArgumentValidation.CheckForEmptyString(providerName, "providerName");
                return (ITextProvider) base.CreateInstance(providerName);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                return null;
            }
        }
    }
}

