﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class DisplayResponse : AppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "DISPLAY_RESPONSE";
            }
        }
    }
}

