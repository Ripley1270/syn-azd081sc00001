﻿namespace Checkbox.Forms.Logic
{
    using Checkbox.Forms;
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Logic.Configuration;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Data;

    [Serializable]
    public class RulesEngine : IDisposable
    {
        private readonly Response _response;
        private readonly DataSet _ruleData;
        private const string BranchActionDataTableName = "BranchAction";
        private const string ExpressionDataTableName = "Expression";
        private const string ItemOperandDataTableName = "ItemOperand";
        private const string ItemRulesTableName = "ItemRules";
        private const string OperandDataTableName = "Operand";
        private const string PageRulesTableName = "PageRules";
        private const string ProfileOperandDataTableName = "ProfileOperand";
        private const string ResponseOperandDataTableName = "ResponseOperand";
        private const string RuleActionsTableName = "RuleActions";
        private const string RuleDataTableName = "Rule";
        private const string ValueOperandDataTableName = "ValueOperand";

        public RulesEngine(DataSet ruleData, Response response)
        {
            this._ruleData = ruleData;
            this._response = response;
        }

        private void BuildExpressionTree(CompositeExpression composite)
        {
            try
            {
                foreach (DataRow row in this._ruleData.Tables["Expression"].Select("[Parent] = " + composite.Identity.Value))
                {
                    if (row["ChildRelation"] != DBNull.Value)
                    {
                        LogicalConnector connector = (LogicalConnector) Enum.Parse(typeof(LogicalConnector), (string) row["ChildRelation"]);
                        CompositeExpression expression = new CompositeExpression(new List<Expression>(), connector) {
                            Identity = new int?((int) row["ExpressionID"])
                        };
                        this.BuildExpressionTree(expression);
                        composite.Add(expression);
                    }
                    else
                    {
                        Expression child = this.CreateExpression(row);
                        child.Identity = new int?((int) row["ExpressionID"]);
                        composite.Add(child);
                    }
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
        }

        private Expression CreateExpression(DataRow expressionData)
        {
            int num5 = (int) expressionData["Operator"];
            LogicalOperator operation = (LogicalOperator) Enum.Parse(typeof(LogicalOperator), num5.ToString());
            int num = (int) expressionData["LeftOperand"];
            int num2 = (int) expressionData["rightOperand"];
            string str = (string) this._ruleData.Tables["Operand"].Select("OperandID=" + num)[0]["TypeName"];
            string str2 = (string) this._ruleData.Tables["Operand"].Select("OperandID=" + num2)[0]["TypeName"];
            Operand left = null;
            Operand right = null;
            if ((str == typeof(ItemOperandData).FullName) || (str == typeof(MatrixItemOperandData).FullName))
            {
                DataRow row = this._ruleData.Tables["ItemOperand"].Select("OperandID=" + num)[0];
                int itemID = (int) row["ItemID"];
                Item item = this._response.GetItem(itemID);
                if (item is SelectItem)
                {
                    left = new SelectItemOperand((SelectItem) item);
                }
                else if (item is IAnswerable)
                {
                    left = new AnswerableOperand((IAnswerable) item);
                }
            }
            else if (str == typeof(ProfileOperandData).FullName)
            {
                DataRow row2 = this._ruleData.Tables["ProfileOperand"].Select("OperandID=" + num)[0];
                string profileKey = (string) row2["ProfileKey"];
                if (this._response.Respondent != null)
                {
                    left = new ProfileOperand(profileKey, this._response.Respondent.Identity);
                }
            }
            else if (str == typeof(ResponseOperandData).FullName)
            {
                DataRow row3 = this._ruleData.Tables["ResponseOperand"].Select("OperandID=" + num)[0];
                string key = (string) row3["ResponseKey"];
                left = new ResponseOperand(key, this._response);
            }
            if (str2 == typeof(OptionOperandData).FullName)
            {
                DataRow tableRow = this._ruleData.Tables["ValueOperand"].Select("OperandID=" + num2)[0];
                right = new OptionOperand(DbUtility.GetValueFromDataRow<int>(tableRow, "OptionID", -1));
            }
            else if (str2 == typeof(StringOperandData).FullName)
            {
                DataRow row5 = this._ruleData.Tables["ValueOperand"].Select("OperandID=" + num2)[0];
                string str5 = null;
                if (row5["AnswerValue"] != DBNull.Value)
                {
                    str5 = (string) row5["AnswerValue"];
                }
                right = new StringOperand(str5);
            }
            return new Expression(left, right, operation);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._ruleData.Dispose();
            }
        }

        private Checkbox.Forms.Logic.Rule GetRuleForItem(Item item)
        {
            DataRow[] rowArray = this._ruleData.Tables["ItemRules"].Select("ItemID=" + item.ID);
            if (rowArray.Length == 0)
            {
                return null;
            }
            int num = (int) rowArray[0]["RuleID"];
            int num2 = (int) this._ruleData.Tables["Rule"].Select("RuleID=" + num)[0]["ExpressionID"];
            DataRow row = this._ruleData.Tables["Expression"].Select("ExpressionID = " + num2)[0];
            LogicalConnector connector = (LogicalConnector) Enum.Parse(typeof(LogicalConnector), (string) row["ChildRelation"]);
            CompositeExpression composite = new CompositeExpression(new List<Expression>(), connector) {
                Identity = new int?(num2)
            };
            this.BuildExpressionTree(composite);
            return new Checkbox.Forms.Logic.Rule(composite, RuleEventTrigger.Load, new Checkbox.Forms.Logic.Action[] { new IncludeExcludeAction(item) });
        }

        private Checkbox.Forms.Logic.Rule[] GetRulesForPage(ResponsePage page)
        {
            DataRow[] rowArray = this._ruleData.Tables["PageRules"].Select("PageID=" + page.PageId);
            List<Checkbox.Forms.Logic.Rule> list = new List<Checkbox.Forms.Logic.Rule>();
            foreach (DataRow row in rowArray)
            {
                int num = (int) row["RuleID"];
                int num2 = (int) this._ruleData.Tables["Rule"].Select("RuleID=" + num)[0]["ExpressionID"];
                DataRow row2 = this._ruleData.Tables["Expression"].Select("ExpressionID = " + num2)[0];
                LogicalConnector connector = (LogicalConnector) Enum.Parse(typeof(LogicalConnector), (string) row2["ChildRelation"]);
                CompositeExpression composite = new CompositeExpression(new List<Expression>(), connector) {
                    Identity = new int?(num2)
                };
                this.BuildExpressionTree(composite);
                Checkbox.Forms.Logic.Action[] actions = new Checkbox.Forms.Logic.Action[1];
                RuleEventTrigger load = RuleEventTrigger.Load;
                if (((string) row["EventTrigger"]) == "Load")
                {
                    actions[0] = new IncludeExcludeAction(page);
                }
                else
                {
                    DataRow row3 = this._ruleData.Tables["RuleActions"].Select("RuleID=" + num)[0];
                    DataRow branchData = this._ruleData.Tables["BranchAction"].Select("ActionID=" + ((int) row3["ActionID"]))[0];
                    ResponsePage page2 = this._response.Pages.Find(p => p.PageId == ((int) branchData["GoToPageID"]));
                    actions[0] = new GoToPageAction(page, this._response, this._response.Pages.IndexOf(page2));
                    load = RuleEventTrigger.UnLoad;
                }
                Checkbox.Forms.Logic.Rule item = new Checkbox.Forms.Logic.Rule(composite, load, actions);
                list.Add(item);
            }
            return list.ToArray();
        }

        public void RunRules(Item item)
        {
            Checkbox.Forms.Logic.Rule ruleForItem = this.GetRuleForItem(item);
            if (ruleForItem != null)
            {
                ruleForItem.Run();
            }
        }

        public void RunRules(ResponsePage page, RuleEventTrigger trigger)
        {
            Checkbox.Forms.Logic.Rule[] rulesForPage = this.GetRulesForPage(page);
            for (int i = 0; i < rulesForPage.Length; i++)
            {
                if ((rulesForPage[i] != null) && (rulesForPage[i].Trigger == trigger))
                {
                    rulesForPage[i].Run();
                }
            }
        }
    }
}

