﻿namespace Checkbox.Panels
{
    using Checkbox.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class Panel : AccessControllablePersistedDomainObject, IPanel
    {
        private List<Panelist> _addedPanelists;
        private List<Panelist> _panelists;
        private List<Panelist> _removedPanelists;

        protected Panel(string[] supportedPermissionMasks, string[] supportedPermissions) : base(supportedPermissionMasks, supportedPermissions)
        {
        }

        protected override void Create(IDbTransaction t)
        {
            throw new Exception("Method not implemented.  Use overloaded Create(...) instead.");
        }

        protected virtual void Create(ExtendedPrincipal principal, IDbTransaction t)
        {
            this.CreatedBy = (principal != null) ? principal.Identity.Name : string.Empty;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_Insert");
            storedProcCommandWrapper.AddInParameter("Name", DbType.String, this.Name);
            storedProcCommandWrapper.AddInParameter("Description", DbType.String, this.Description);
            storedProcCommandWrapper.AddInParameter("DateCreated", DbType.DateTime, DateTime.Now);
            storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, this.CreatedBy);
            storedProcCommandWrapper.AddInParameter("PanelTypeID", DbType.Int32, this.PanelTypeID);
            storedProcCommandWrapper.AddOutParameter("PanelID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("PanelID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save Panel data.");
            }
            base.ID = new int?((int) parameterValue);
            this.UpdatePanelists(t);
        }

        public override void Delete(IDbTransaction transaction)
        {
            if (base.ID.HasValue && (base.ID > 0))
            {
                try
                {
                    Database database = DatabaseFactory.CreateDatabase();
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_Delete");
                    storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
                    database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                }
                catch (Exception exception)
                {
                    if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                    {
                        throw;
                    }
                }
            }
        }

        public override DataSet GetConfigurationDataSet()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_GetPanel");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, this.DataTableName);
            return dataSet;
        }

        public override SecurityEditor GetEditor()
        {
            throw new NotImplementedException();
        }

        public virtual Panelist GetPanelist(string identifier)
        {
            return this.Panelists.Find(p => p.Email.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
        }

        protected abstract List<Panelist> GetPanelists();
        protected override void Load(DataRow data)
        {
            base.Load(data);
            this.Name = DbUtility.GetValueFromDataRow<string>(data, "Name", string.Empty);
            this.Description = DbUtility.GetValueFromDataRow<string>(data, "Description", string.Empty);
            base.CreatedDate = DbUtility.GetValueFromDataRow<DateTime?>(data, "DateCreated", null);
            this.CreatedBy = DbUtility.GetValueFromDataRow<string>(data, "CreatedBy", string.Empty);
        }

        private void LoadPanelists()
        {
            this.AddedPanelists.Clear();
            this.RemovedPanelists.Clear();
            this._panelists.AddRange(this.GetPanelists());
        }

        public void Save(ExtendedPrincipal principal)
        {
            using (IDbConnection connection = DatabaseFactory.CreateDatabase().GetConnection())
            {
                try
                {
                    connection.Open();
                    IDbTransaction t = connection.BeginTransaction();
                    try
                    {
                        this.Save(principal, t);
                        t.Commit();
                    }
                    catch
                    {
                        t.Rollback();
                        throw;
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public override void Save(IDbTransaction t)
        {
            throw new Exception("Method not implemented.  Use overloaded Save(...) instead.");
        }

        public void Save(ExtendedPrincipal principal, IDbTransaction t)
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                this.Create(principal, t);
            }
            else
            {
                this.Update(principal, t);
            }
        }

        protected override void Update(IDbTransaction t)
        {
            throw new Exception("Method not implemented.  Use overloaded Update(...) instead.");
        }

        protected virtual void Update(ExtendedPrincipal principal, IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_Update");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("Name", DbType.String, this.Name);
            storedProcCommandWrapper.AddInParameter("Description", DbType.String, this.Description);
            storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, this.CreatedBy);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdatePanelists(t);
        }

        protected virtual void UpdatePanelists(IDbTransaction t)
        {
        }

        protected List<Panelist> AddedPanelists
        {
            get
            {
                if (this._addedPanelists == null)
                {
                    this._addedPanelists = new List<Panelist>();
                }
                return this._addedPanelists;
            }
        }

        public string CreatedBy { get; set; }

        public override string DataTableName
        {
            get
            {
                return "Panel";
            }
        }

        public string Description { get; set; }

        public override string DomainDBIdentityColumnName
        {
            get
            {
                return "PanelID";
            }
        }

        public override string DomainDBTableName
        {
            get
            {
                return "ckbx_Panel";
            }
        }

        internal int? Identity
        {
            set
            {
                base.ID = value;
            }
        }

        public override string IdentityColumnName
        {
            get
            {
                return "PanelID";
            }
        }

        public string Name { get; set; }

        public List<Panelist> Panelists
        {
            get
            {
                if (this._panelists == null)
                {
                    this._panelists = new List<Panelist>();
                    this.LoadPanelists();
                }
                return this._panelists;
            }
        }

        protected Checkbox.Panels.PanelType PanelType { get; set; }

        public virtual int PanelTypeID
        {
            get
            {
                return (int) this.PanelType;
            }
        }

        protected List<Panelist> RemovedPanelists
        {
            get
            {
                if (this._removedPanelists == null)
                {
                    this._removedPanelists = new List<Panelist>();
                }
                return this._removedPanelists;
            }
        }
    }
}

