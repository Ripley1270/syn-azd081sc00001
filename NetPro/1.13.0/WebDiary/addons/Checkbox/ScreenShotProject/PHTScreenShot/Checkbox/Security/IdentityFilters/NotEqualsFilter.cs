﻿namespace Checkbox.Security.IdentityFilters
{
    using System;

    public class NotEqualsFilter : IdentityFilter
    {
        private object propertyValue;

        public NotEqualsFilter(string filterProperty) : base(IdentityFilterType.NotEqual, IdentityFilterPropertyType.IdentityProperty, filterProperty)
        {
        }

        public NotEqualsFilter(string filterProperty, object propertyValue) : base(IdentityFilterType.NotEqual, IdentityFilterPropertyType.IdentityProperty, filterProperty)
        {
            this.propertyValue = propertyValue;
        }

        public NotEqualsFilter(string filterProperty, object propertyValue, IdentityFilterPropertyType filterPropertyType) : base(IdentityFilterType.NotEqual, filterPropertyType, filterProperty)
        {
            this.propertyValue = propertyValue;
        }

        public object PropertyValue
        {
            get
            {
                return this.propertyValue;
            }
        }
    }
}

