﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using System;

    [Serializable]
    public class EmailResponseItemData : EmailItemData
    {
        protected override Item CreateItem()
        {
            return new EmailResponseItem();
        }

        public override string DataTableName
        {
            get
            {
                return "EmailResponseItemData";
            }
        }
    }
}

