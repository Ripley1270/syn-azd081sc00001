﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Xml;

    [Serializable]
    public class TabularItem : LabelledItem, ICompositeItem, IScored
    {
        private Dictionary<string, Item> _childItems;
        private int _columnCount;
        private int _rowCount;

        private void childItem_ItemExcluded(object sender, EventArgs e)
        {
            this.OnChildItemExcluded((Item) sender);
        }

        public override void Configure(ItemData configuration, string languageCode)
        {
            ArgumentValidation.CheckExpectedType(configuration, typeof(TabularItemData));
            TabularItemData config = (TabularItemData) configuration;
            base.Configure(configuration, languageCode);
            this.SetRowCount(config);
            this.SetColumnCount(config);
            this.CreateChildItemsDictionary();
            this.CreateChildItems(config, languageCode);
        }

        protected virtual void CreateChildItems(TabularItemData config, string languageCode)
        {
            for (int i = 1; i <= this._rowCount; i++)
            {
                for (int j = 1; j <= this._columnCount; j++)
                {
                    ItemData itemAt = config.GetItemAt(i, j);
                    if (itemAt != null)
                    {
                        this.SetChildItemInDictionary(i + "_" + j, itemAt.CreateItem(languageCode));
                    }
                }
            }
        }

        protected virtual void CreateChildItemsDictionary()
        {
            this._childItems = new Dictionary<string, Item>();
        }

        public override void DeleteAnswers()
        {
            foreach (Item item in this.Items)
            {
                if (item is IAnswerable)
                {
                    ((IAnswerable) item).DeleteAnswers();
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            foreach (Item item in this.Items)
            {
                item.Dispose();
            }
        }

        protected virtual Coordinate GetChildItemCoordinate(Item child)
        {
            if (this._childItems.ContainsValue(child))
            {
                foreach (string str in this._childItems.Keys)
                {
                    if (this._childItems[str] == child)
                    {
                        string[] strArray = str.Split(new char[] { '_' });
                        return new Coordinate(int.Parse(strArray[1]), int.Parse(strArray[0]));
                    }
                }
            }
            return null;
        }

        protected virtual Item GetChildItemFromDictionary(string key)
        {
            if (this._childItems.ContainsKey(key))
            {
                return this._childItems[key];
            }
            return null;
        }

        public virtual Item GetItemAt(int row, int column)
        {
            return this.GetChildItemFromDictionary(row + "_" + column);
        }

        public Item GetItemWithID(int id)
        {
            foreach (Item item2 in this._childItems.Values)
            {
                if (item2.ID == id)
                {
                    return item2;
                }
            }
            return null;
        }

        public double GetScore()
        {
            return 0.0;
        }

        protected override void OnAnswerDataSet()
        {
            base.OnAnswerDataSet();
            foreach (Item item in this.Items)
            {
                if (item is ResponseItem)
                {
                    ((ResponseItem) item).AnswerData = base.AnswerData;
                }
            }
        }

        protected virtual void OnChildItemExcluded(Item child)
        {
            if (child is IAnswerable)
            {
                ((IAnswerable) child).DeleteAnswers();
            }
        }

        protected override void OnResponseSet()
        {
            foreach (Item item in this.Items)
            {
                if ((item != null) && (item is ResponseItem))
                {
                    ((ResponseItem) item).Response = base.Response;
                    if ((item.ID > 0) && !base.Response.Items.ContainsKey(item.ID))
                    {
                        base.Response.Items.Add(item.ID, item);
                    }
                }
            }
        }

        protected virtual void SetChildItemInDictionary(string key, Item childItem)
        {
            this._childItems[key] = childItem;
            childItem.ItemExcluded += new EventHandler(this.childItem_ItemExcluded);
        }

        protected virtual void SetColumnCount(TabularItemData config)
        {
            this._columnCount = config.ColumnCount;
        }

        protected virtual void SetRowCount(TabularItemData config)
        {
            this._rowCount = config.RowCount;
        }

        protected override bool ValidateAnswers()
        {
            bool flag = true;
            foreach (Item item in this.Items)
            {
                if (item is ResponseItem)
                {
                    ((ResponseItem) item).Validate();
                    if (!((ResponseItem) item).Valid)
                    {
                        flag = false;
                    }
                }
            }
            return flag;
        }

        protected virtual void WriteColumnInstanceData(int row, int column, XmlWriter writer)
        {
            writer.WriteStartElement("column");
            writer.WriteAttributeString("columnNumber", column.ToString());
            Item itemAt = this.GetItemAt(row, column);
            if (itemAt != null)
            {
                itemAt.WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        protected virtual void WriteRowInstanceData(int row, XmlWriter writer)
        {
            writer.WriteStartElement("row");
            writer.WriteAttributeString("rowNumber", row.ToString());
            writer.WriteStartElement("columns");
            for (int i = 1; i <= this.ColumnCount; i++)
            {
                this.WriteColumnInstanceData(row, i, writer);
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        protected virtual void WriteTableInstanceData(XmlWriter writer)
        {
            writer.WriteStartElement("rows");
            for (int i = 1; i <= this.RowCount; i++)
            {
                this.WriteRowInstanceData(i, writer);
            }
            writer.WriteEndElement();
        }

        public override void WriteXmlInstanceData(XmlWriter writer)
        {
            base.WriteXmlInstanceData(writer);
            this.WriteTableInstanceData(writer);
        }

        public virtual int ColumnCount
        {
            get
            {
                return this._columnCount;
            }
            set
            {
                this._columnCount = value;
            }
        }

        public override bool HasAnswer
        {
            get
            {
                foreach (Item item in this.Items)
                {
                    if (((item != null) && (item is IAnswerable)) && ((IAnswerable) item).HasAnswer)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public virtual ReadOnlyCollection<Item> Items
        {
            get
            {
                List<Item> list = new List<Item>();
                foreach (Item item in this._childItems.Values)
                {
                    list.Add(item);
                }
                return new ReadOnlyCollection<Item>(list);
            }
        }

        public virtual int RowCount
        {
            get
            {
                return this._rowCount;
            }
            set
            {
                this._rowCount = value;
            }
        }
    }
}

