﻿namespace Checkbox.Users.Data
{
    using Checkbox.Security;
    using Checkbox.Security.Data;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security.Principal;
    using System;

    internal class QueryFactory
    {
        private QueryFactory()
        {
        }

        private static QueryCriterion CreateFilterCriterion(string filterField, string filterValue, string tableName)
        {
            return new QueryCriterion(new SelectParameter(filterField, string.Empty, tableName), CriteriaOperator.Like, new LiteralParameter("'%" + filterValue + "%'"));
        }

        private static SubqueryParameter CreatePagingSubQueryParameter(SelectQuery query, string selectFieldName, string selectFieldTable, int pageNumber, int resultsPerPage)
        {
            int count = (pageNumber - 1) * resultsPerPage;
            SelectQuery subQuery = (SelectQuery) query.Clone();
            subQuery.SelectParameters = null;
            subQuery.AddParameter(selectFieldName, string.Empty, selectFieldTable);
            if (count > 0)
            {
                subQuery.AddSelectTopCriterion(count);
            }
            return new SubqueryParameter(subQuery);
        }

        internal static SelectQuery GetAllGroupsQuery()
        {
            SelectQuery query = new SelectQuery("ckbx_Group");
            query.AddAllParameter("ckbx_Group");
            return query;
        }

        internal static SelectQuery GetAllGroupsQuery(CriteriaCollection filterCollection)
        {
            SelectQuery query = new SelectQuery("ckbx_Group");
            query.AddAllParameter("ckbx_Group");
            if (filterCollection != null)
            {
                query.AddCriteriaCollection(filterCollection);
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsCountQuery(ExtendedPrincipal currentPrincipal, string filterField, string filterValue, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, filterField, filterValue, permissionJoinType, permissions);
            query.SelectParameters = null;
            query.SelectCriteria = null;
            query.AddParameter(new CountParameter("RecordCount", "GroupID", new SelectDistinctCriterion()));
            return query;
        }

        internal static SelectQuery GetAvailableGroupsCountQuery(ExtendedPrincipal currentPrincipal, string filterField, string filterValue, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, filterField, filterValue, filterCollection, permissionJoinType, permissions);
            query.SelectParameters = null;
            query.SelectCriteria = null;
            query.AddParameter(new CountParameter("RecordCount", "GroupID", new SelectDistinctCriterion()));
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, PermissionJoin permissionJoinType, params string[] permissions)
        {
            if ((permissions == null) || (permissions.Length == 0))
            {
                throw new ArgumentException("GetAvailableFormsQuery was called with a null or empty permissions.", "permissions");
            }
            if (currentPrincipal.IsInRole("System Administrator"))
            {
                return GetAllGroupsQuery();
            }
            return Checkbox.Security.Data.QueryFactory.GetSelectEntitiesWithPermissionQuery("ckbx_Group", "GroupID", "AclID", currentPrincipal.AclTypeIdentifier, currentPrincipal.AclEntryIdentifier, "DefaultPolicy", permissionJoinType, permissions);
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            if ((permissions == null) || (permissions.Length == 0))
            {
                throw new ArgumentException("GetAvailableFormsQuery was called with a null or empty permissions.", "permissions");
            }
            if (currentPrincipal.IsInRole("System Administrator"))
            {
                return GetAllGroupsQuery(filterCollection);
            }
            SelectQuery query = Checkbox.Security.Data.QueryFactory.GetSelectEntitiesWithPermissionQuery("ckbx_Group", "GroupID", "AclID", currentPrincipal.AclTypeIdentifier, currentPrincipal.AclEntryIdentifier, "DefaultPolicy", permissionJoinType, permissions);
            if (filterCollection != null)
            {
                query.AddCriteriaCollection(filterCollection);
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, permissionJoinType, permissions);
            if ((resultsPerPage > 0) && (pageNumber > 0))
            {
                query.AddSelectTopCriterion(resultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("GroupID", string.Empty, "ckbx_Group"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "GroupID", "ckbx_Group", pageNumber, resultsPerPage)));
                }
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, string sortField, bool sortAscending, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, permissionJoinType, permissions);
            if ((sortField != null) && (sortField != string.Empty))
            {
                query.AddSortField(new SortOrder(sortField, sortAscending));
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, string filterField, string filterValue, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, permissionJoinType, permissions);
            bool flag = (filterField == null) || (filterField == string.Empty);
            bool flag2 = (filterValue == null) || (filterValue == string.Empty);
            if (!flag && !flag2)
            {
                query.AddCriterion(CreateFilterCriterion(filterField, filterValue, "ckbx_Group"));
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, filterCollection, permissionJoinType, permissions);
            if ((resultsPerPage > 0) && (pageNumber > 0))
            {
                query.AddSelectTopCriterion(resultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("GroupID", string.Empty, "ckbx_Group"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "GroupID", "ckbx_Group", pageNumber, resultsPerPage)));
                }
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, string sortField, bool sortAscending, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, filterCollection, permissionJoinType, permissions);
            if ((sortField != null) && (sortField != string.Empty))
            {
                query.AddSortField(new SortOrder(sortField, sortAscending));
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, string filterField, string filterValue, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, filterCollection, permissionJoinType, permissions);
            bool flag = (filterField == null) || (filterField == string.Empty);
            bool flag2 = (filterValue == null) || (filterValue == string.Empty);
            if (!flag && !flag2)
            {
                query.AddCriterion(CreateFilterCriterion(filterField, filterValue, "ckbx_Group"));
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, string sortField, bool sortAscending, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, sortField, sortAscending, permissionJoinType, permissions);
            if ((resultsPerPage > 0) && (pageNumber > 0))
            {
                query.AddSelectTopCriterion(resultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("GroupID", string.Empty, "ckbx_Group"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "GroupID", "ckbx_Group", pageNumber, resultsPerPage)));
                }
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, string filterField, string filterValue, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, filterField, filterValue, permissionJoinType, permissions);
            if ((resultsPerPage > 0) && (pageNumber > 0))
            {
                query.AddSelectTopCriterion(resultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("GroupID", string.Empty, "ckbx_Group"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "GroupID", "ckbx_Group", pageNumber, resultsPerPage)));
                }
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, string filterField, string filterValue, string sortField, bool sortAscending, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, sortField, sortAscending, permissionJoinType, permissions);
            bool flag = (filterField == null) || (filterField == string.Empty);
            bool flag2 = (filterValue == null) || (filterValue == string.Empty);
            if (!flag && !flag2)
            {
                query.AddCriterion(CreateFilterCriterion(filterField, filterValue, "ckbx_Group"));
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, string sortField, bool sortAscending, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, sortField, sortAscending, filterCollection, permissionJoinType, permissions);
            if ((resultsPerPage > 0) && (pageNumber > 0))
            {
                query.AddSelectTopCriterion(resultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("GroupID", string.Empty, "ckbx_Group"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "GroupID", "ckbx_Group", pageNumber, resultsPerPage)));
                }
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, string filterField, string filterValue, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, filterField, filterValue, filterCollection, permissionJoinType, permissions);
            if ((resultsPerPage > 0) && (pageNumber > 0))
            {
                query.AddSelectTopCriterion(resultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("GroupID", string.Empty, "ckbx_Group"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "GroupID", "ckbx_Group", pageNumber, resultsPerPage)));
                }
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, string filterField, string filterValue, string sortField, bool sortAscending, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, sortField, sortAscending, filterCollection, permissionJoinType, permissions);
            bool flag = (filterField == null) || (filterField == string.Empty);
            bool flag2 = (filterValue == null) || (filterValue == string.Empty);
            if (!flag && !flag2)
            {
                query.AddCriterion(CreateFilterCriterion(filterField, filterValue, "ckbx_Group"));
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, string filterField, string filterValue, string sortField, bool sortAscending, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, filterField, filterValue, permissionJoinType, permissions);
            if ((sortField != null) && (sortField != string.Empty))
            {
                query.AddSortField(new SortOrder(sortField, sortAscending));
            }
            else
            {
                query.AddSortField(new SortOrder("GroupName", true));
            }
            if ((resultsPerPage > 0) && (pageNumber > 0))
            {
                query.AddSelectTopCriterion(resultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("GroupID", string.Empty, "ckbx_Group"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "GroupID", "ckbx_Group", pageNumber, resultsPerPage)));
                }
            }
            return query;
        }

        internal static SelectQuery GetAvailableGroupsQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, string filterField, string filterValue, string sortField, bool sortAscending, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetAvailableGroupsQuery(currentPrincipal, filterField, filterValue, filterCollection, permissionJoinType, permissions);
            if ((sortField != null) && (sortField != string.Empty))
            {
                query.AddSortField(new SortOrder(sortField, sortAscending));
            }
            if ((resultsPerPage > 0) && (pageNumber > 0))
            {
                query.AddSelectTopCriterion(resultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("GroupID", string.Empty, "ckbx_Group"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "GroupID", "ckbx_Group", pageNumber, resultsPerPage)));
                }
            }
            return query;
        }

        internal static SelectQuery GetSelectAvailableUsersQuery(ExtendedPrincipal currentPrincipal, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = new SelectQuery("ckbx_Credential");
            query.AddAllParameter("ckbx_Credential");
            query.AddSelectDistinctCriterion();
            query.AddTableJoin("ckbx_GroupMembers", QueryJoinType.Inner, "MemberUniqueIdentifier", "ckbx_Credential", "UniqueIdentifier");
            SelectQuery subQuery = GetAvailableGroupsQuery(currentPrincipal, permissionJoinType, permissions);
            subQuery.SelectParameters = null;
            subQuery.AddParameter("GroupID", string.Empty, "ckbx_Group");
            query.AddCriterion(new QueryCriterion(new SelectParameter("GroupID", string.Empty, "ckbx_GroupMembers"), CriteriaOperator.In, new SubqueryParameter(subQuery)));
            return query;
        }

        internal static SelectQuery GetSelectAvailableUsersQuery(ExtendedPrincipal currentPrincipal, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = new SelectQuery("ckbx_Credential");
            query.AddAllParameter("ckbx_Credential");
            query.AddSelectDistinctCriterion();
            query.AddTableJoin("ckbx_GroupMembers", QueryJoinType.Inner, "MemberUniqueIdentifier", "ckbx_Credential", "UniqueIdentifier");
            SelectQuery subQuery = GetAvailableGroupsQuery(currentPrincipal, permissionJoinType, permissions);
            subQuery.SelectParameters = null;
            subQuery.AddParameter("GroupID", string.Empty, "ckbx_Group");
            query.AddCriterion(new QueryCriterion(new SelectParameter("GroupID", string.Empty, "ckbx_GroupMembers"), CriteriaOperator.In, new SubqueryParameter(subQuery)));
            if (filterCollection != null)
            {
                query.AddCriteriaCollection(filterCollection);
            }
            return query;
        }

        internal static SelectQuery GetSelectAvailableUsersQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetSelectAvailableUsersQuery(currentPrincipal, permissionJoinType, permissions);
            if ((resultsPerPage > 0) && (pageNumber > 0))
            {
                query.AddSelectTopCriterion(resultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "UniqueIdentifier", "ckbx_Credential", pageNumber, resultsPerPage)));
                }
            }
            return query;
        }

        internal static SelectQuery GetSelectAvailableUsersQuery(ExtendedPrincipal currentPrincipal, string sortField, bool sortAscending, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetSelectAvailableUsersQuery(currentPrincipal, permissionJoinType, permissions);
            if ((sortField != null) && (sortField != string.Empty))
            {
                query.AddSortField(new SortOrder(sortField, sortAscending));
            }
            return query;
        }

        internal static SelectQuery GetSelectAvailableUsersQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetSelectAvailableUsersQuery(currentPrincipal, filterCollection, permissionJoinType, permissions);
            if ((resultsPerPage > 0) && (pageNumber > 0))
            {
                query.AddSelectTopCriterion(resultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "UniqueIdentifier", "ckbx_Credential", pageNumber, resultsPerPage)));
                }
            }
            return query;
        }

        internal static SelectQuery GetSelectAvailableUsersQuery(ExtendedPrincipal currentPrincipal, string sortField, bool sortAscending, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetSelectAvailableUsersQuery(currentPrincipal, filterCollection, permissionJoinType, permissions);
            if ((sortField != null) && (sortField != string.Empty))
            {
                if (sortField.IndexOf(".") < 0)
                {
                    sortField = "ckbx_Credential." + sortField;
                }
                query.AddSortField(new SortOrder(sortField, sortAscending));
            }
            return query;
        }

        internal static SelectQuery GetSelectAvailableUsersQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, string sortField, bool sortAscending, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetSelectAvailableUsersQuery(currentPrincipal, sortField, sortAscending, permissionJoinType, permissions);
            if ((resultsPerPage > 0) && (pageNumber > 0))
            {
                query.AddSelectTopCriterion(resultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "UniqueIdentifier", "ckbx_Credential", pageNumber, resultsPerPage)));
                }
            }
            return query;
        }

        internal static SelectQuery GetSelectAvailableUsersQuery(ExtendedPrincipal currentPrincipal, int resultsPerPage, int pageNumber, string sortField, bool sortAscending, CriteriaCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = GetSelectAvailableUsersQuery(currentPrincipal, sortField, sortAscending, filterCollection, permissionJoinType, permissions);
            if ((resultsPerPage > 0) && (pageNumber > 0))
            {
                query.AddSelectTopCriterion(resultsPerPage);
                query.CriteriaJoinType = CriteriaJoinType.And;
                if (pageNumber > 1)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), CriteriaOperator.NotIn, CreatePagingSubQueryParameter(query, "UniqueIdentifier", "ckbx_Credential", pageNumber, resultsPerPage)));
                }
            }
            return query;
        }
    }
}

