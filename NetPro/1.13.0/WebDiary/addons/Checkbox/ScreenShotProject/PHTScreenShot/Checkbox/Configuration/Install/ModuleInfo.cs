﻿namespace Checkbox.Configuration.Install
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;

    [Serializable]
    public class ModuleInfo : IComparable<ModuleInfo>, IComparable<string>
    {
        private readonly string _moduleConfigurationXML;

        private ModuleInfo(XmlDocument doc)
        {
            this._moduleConfigurationXML = doc.OuterXml;
        }

        public int CompareTo(ModuleInfo other)
        {
            return this.CompareTo(other.Name);
        }

        public int CompareTo(string other)
        {
            return string.Compare(other, this.Name, true);
        }

        internal static List<ModuleInfo> GetAvailableModules(string rootSearchPath)
        {
            List<ModuleInfo> list = new List<ModuleInfo>();
            if (!string.IsNullOrEmpty(rootSearchPath) && Directory.Exists(rootSearchPath + Path.DirectorySeparatorChar + "Modules"))
            {
                foreach (string str in Directory.GetDirectories(rootSearchPath + Path.DirectorySeparatorChar + "Modules"))
                {
                    if (File.Exists(str + Path.DirectorySeparatorChar + "ModuleInfo.xml"))
                    {
                        XmlDocument doc = new XmlDocument();
                        try
                        {
                            doc.Load(str + Path.DirectorySeparatorChar + "ModuleInfo.xml");
                            list.Add(new ModuleInfo(doc));
                        }
                        catch
                        {
                        }
                    }
                }
            }
            return list;
        }

        internal static ModuleInfo GetModuleInfo(string moduleName, string moduleVersion, string searchPath)
        {
            foreach (ModuleInfo info in GetAvailableModules(searchPath + Path.DirectorySeparatorChar + "Install"))
            {
                if ((string.Compare(moduleName, info.Name, true) == 0) && (string.Compare(moduleVersion, info.Version, true) == 0))
                {
                    return info;
                }
            }
            StringBuilder builder = new StringBuilder();
            builder.Append("<moduleInfo><properties><name>");
            builder.Append(moduleName);
            builder.Append("</name><version>");
            builder.Append(moduleVersion);
            builder.Append("</version></properties></moduleInfo>");
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(builder.ToString());
            return new ModuleInfo(doc);
        }

        public List<string> DatabaseScripts
        {
            get
            {
                List<string> list = new List<string>();
                XmlDocument document = new XmlDocument();
                document.LoadXml(this._moduleConfigurationXML);
                foreach (XmlNode node in document.SelectNodes("/moduleInfo/databaseScripts/databaseScript"))
                {
                    list.Add(node.InnerText);
                }
                return list;
            }
        }

        public string Description
        {
            get
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(this._moduleConfigurationXML);
                return XmlUtility.GetNodeText(document.SelectSingleNode("/moduleInfo/properties/description"));
            }
        }

        public List<string> Features
        {
            get
            {
                List<string> list = new List<string>();
                XmlDocument document = new XmlDocument();
                document.LoadXml(this._moduleConfigurationXML);
                foreach (XmlNode node in document.SelectNodes("/moduleInfo/features/feature"))
                {
                    list.Add(node.InnerText);
                }
                return list;
            }
        }

        public List<InstallFileInfo> Files
        {
            get
            {
                List<InstallFileInfo> list = new List<InstallFileInfo>();
                XmlDocument document = new XmlDocument();
                document.LoadXml(this._moduleConfigurationXML);
                foreach (XmlNode node in document.SelectNodes("/moduleInfo/files/file"))
                {
                    list.Add(new InstallFileInfo(XmlUtility.GetNodeText(node.SelectSingleNode("source")), XmlUtility.GetNodeText(node.SelectSingleNode("destination"))));
                }
                return list;
            }
        }

        public string Name
        {
            get
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(this._moduleConfigurationXML);
                return XmlUtility.GetNodeText(document.SelectSingleNode("/moduleInfo/properties/name"));
            }
        }

        public string PreRequisiteProduct
        {
            get
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(this._moduleConfigurationXML);
                return XmlUtility.GetNodeText(document.SelectSingleNode("/moduleInfo/preRequisites/product/name"));
            }
        }

        public List<string> PreRequisiteProductVersions
        {
            get
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(this._moduleConfigurationXML);
                XmlNodeList list = document.SelectNodes("/moduleInfo/preRequisites/product/versions/version");
                List<string> list2 = new List<string>();
                foreach (XmlNode node in list)
                {
                    list2.Add(node.InnerText);
                }
                return list2;
            }
        }

        public string SetupInstructionsUrl
        {
            get
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(this._moduleConfigurationXML);
                return XmlUtility.GetNodeText(document.SelectSingleNode("/moduleInfo/properties/setupInstructionsUrl"));
            }
        }

        public string SetupURL
        {
            get
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(this._moduleConfigurationXML);
                return XmlUtility.GetNodeText(document.SelectSingleNode("/moduleInfo/properties/setupUrl"));
            }
        }

        public string Version
        {
            get
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(this._moduleConfigurationXML);
                return XmlUtility.GetNodeText(document.SelectSingleNode("/moduleInfo/properties/version"));
            }
        }
    }
}

