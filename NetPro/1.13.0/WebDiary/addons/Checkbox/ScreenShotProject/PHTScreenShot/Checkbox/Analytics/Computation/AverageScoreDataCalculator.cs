﻿namespace Checkbox.Analytics.Computation
{
    using Checkbox.Analytics.Items.Configuration;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class AverageScoreDataCalculator : ReportDataCalculator
    {
        private readonly AverageScoreCalculation _scoreCalc;

        public AverageScoreDataCalculator(AverageScoreCalculation scoreCalc)
        {
            this._scoreCalc = scoreCalc;
        }

        protected override DataTable Calculate(ItemAnswerAggregator answerAggregator)
        {
            decimal num4;
            decimal num7;
            DataTable table = base.CreateOutputTable();
            Dictionary<int, int> dictionary = new Dictionary<int, int>();
            Dictionary<int, decimal> dictionary2 = new Dictionary<int, decimal>();
            int num = 0;
            decimal num2 = 0M;
            List<int> itemIDs = answerAggregator.GetItemIDs();
            DataTable aggregatedAnswerData = answerAggregator.GetAggregatedAnswerData();
            foreach (int num3 in itemIDs)
            {
                string filter = "ItemID = " + num3 + " AND OptionIsOther = 0";
                dictionary[num3] = answerAggregator.GetResponseCount(new int?(num3));
                num += DbUtility.ListDataColumnValues<long>(aggregatedAnswerData, "ResponseId", filter, null, true).Count;
                object obj2 = aggregatedAnswerData.Compute("SUM(Points)", filter);
                if (obj2 == DBNull.Value)
                {
                    dictionary2[num3] = 0M;
                }
                else
                {
                    dictionary2[num3] = Convert.ToDecimal(obj2);
                }
                num2 += dictionary2[num3];
            }
            if (num > 0)
            {
                num4 = num2 / num;
            }
            else
            {
                num4 = 0M;
            }
            Dictionary<int, decimal> dictionary3 = new Dictionary<int, decimal>();
            decimal num5 = 0M;
            foreach (int num6 in dictionary2.Keys)
            {
                if (dictionary[num6] > 0)
                {
                    dictionary3[num6] = dictionary2[num6] / dictionary[num6];
                }
                else
                {
                    dictionary3[num6] = 0M;
                }
                num5 += dictionary3[num6];
            }
            if (dictionary3.Count > 0)
            {
                num7 = num5 / dictionary3.Count;
            }
            else
            {
                num7 = 0M;
            }
            if (this.ScoreCalculationMode == AverageScoreCalculation.Aggregate)
            {
                DataRow row = table.NewRow();
                row["AnswerPercent"] = num4;
                table.Rows.Add(row);
                return table;
            }
            if (this.ScoreCalculationMode == AverageScoreCalculation.AverageOfItemScores)
            {
                DataRow row2 = table.NewRow();
                row2["AnswerPercent"] = num7;
                table.Rows.Add(row2);
                return table;
            }
            foreach (int num8 in dictionary3.Keys)
            {
                DataRow row3 = table.NewRow();
                row3["ItemID"] = num8;
                row3["ItemText"] = answerAggregator.GetItemText(num8);
                row3["AnswerPercent"] = dictionary3[num8];
                table.Rows.Add(row3);
            }
            return table;
        }

        public virtual AverageScoreCalculation ScoreCalculationMode
        {
            get
            {
                return this._scoreCalc;
            }
        }
    }
}

