﻿namespace Checkbox.Forms.Security
{
    using Checkbox.Forms;
    using Checkbox.Security;
    using Prezza.Framework.Security;
    using System;
    using System.Collections.Generic;

    public class FormSecurityEditor : AccessControllablePDOSecurityEditor
    {
        public FormSecurityEditor(IAccessControllable form) : base(form)
        {
        }

        public virtual List<IAccessControlEntry> GetMergedAccessPermissible(params string[] permissions)
        {
            Predicate<IAccessControlEntry> match = null;
            List<IAccessControlEntry> accessPermissible = base.GetAccessPermissible(permissions);
            if (permissions != null)
            {
                List<IAccessControlEntry> list2 = new List<IAccessControlEntry>(this.List(null));
                if (match == null)
                {
                    match = entry => (entry.Policy != null) && !entry.Policy.HasAtLeastOnePermission(permissions);
                }
                accessPermissible.AddRange(list2.FindAll(match));
            }
            return accessPermissible;
        }

        protected void MarkTemplateUpdated()
        {
            int? iD = null;
            if (base.ControllableResource is ResponseTemplate)
            {
                iD = ((ResponseTemplate) base.ControllableResource).ID;
            }
            else if (base.ControllableResource is LightweightResponseTemplate)
            {
                iD = new int?(((LightweightResponseTemplate) base.ControllableResource).ID);
            }
            if (iD.HasValue)
            {
                ResponseTemplateManager.MarkTemplateUpdated(iD.Value);
            }
        }

        public override void SaveAcl()
        {
            base.SaveAcl();
            this.MarkTemplateUpdated();
        }

        public override void SetDefaultPolicy(Policy p)
        {
            base.SetDefaultPolicy(p);
            this.MarkTemplateUpdated();
        }

        protected void UpdateAccess(IAccessControlEntry[] pendingEntries, string[] newPermissions, bool remove)
        {
            if (!base.AuthorizationProvider.Authorize(base.CurrentPrincipal, base.ControllableResource, this.RequiredEditorPermission))
            {
                throw new AuthorizationException();
            }
            foreach (IAccessControlEntry entry in pendingEntries)
            {
                IAccessPermissible permissbleEntity = AccessControllablePDOSecurityEditor.GetPermissbleEntity(entry);
                if (permissbleEntity != null)
                {
                    Policy policy = base.ControllableResource.ACL.GetPolicy(permissbleEntity);
                    if (policy == null)
                    {
                        policy = base.ControllableResource.CreatePolicy(newPermissions);
                        base.ControllableResource.ACL.Add(permissbleEntity, policy);
                    }
                    else
                    {
                        bool flag = false;
                        List<string> list = new List<string>(policy.Permissions);
                        foreach (string str in newPermissions)
                        {
                            if (remove && list.Contains(str))
                            {
                                list.Remove(str);
                                flag = true;
                            }
                            else if (!remove && !list.Contains(str))
                            {
                                list.Add(str);
                                flag = true;
                            }
                        }
                        if (flag)
                        {
                            base.ControllableResource.ACL.Delete(permissbleEntity);
                            base.ControllableResource.ACL.Add(permissbleEntity, base.ControllableResource.CreatePolicy(list.ToArray()));
                        }
                    }
                }
            }
            this.SaveAcl();
        }

        protected override string RequiredEditorPermission
        {
            get
            {
                return "Form.Administer";
            }
        }
    }
}

