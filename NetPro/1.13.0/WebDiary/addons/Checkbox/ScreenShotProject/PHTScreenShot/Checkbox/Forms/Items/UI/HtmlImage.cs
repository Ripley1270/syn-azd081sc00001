﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class HtmlImage : AppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "IMAGE";
            }
        }
    }
}

