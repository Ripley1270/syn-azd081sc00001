﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Globalization.Text;
    using System;
    using System.Text;

    public abstract class RangeValidator<T> : Validator<T>
    {
        private T _maxValue;
        private bool _maxValueSet;
        private T _minValue;
        private bool _minValueSet;

        protected RangeValidator()
        {
        }

        public override string GetMessage(string languageCode)
        {
            StringBuilder builder = new StringBuilder();
            if (this._minValueSet && !this._maxValueSet)
            {
                builder.Append(TextManager.GetText("/validationMessages/regex/rangeNoMax", languageCode));
                builder.Replace("{min}", this._minValue.ToString());
            }
            else if (!this._minValueSet && this._maxValueSet)
            {
                builder.Append(TextManager.GetText("/validationMessages/regex/rangeNoMin", languageCode));
                builder.Replace("{max}", this._maxValue.ToString());
            }
            else
            {
                builder.Append(TextManager.GetText("/validationMessages/regex/range", languageCode));
                builder.Replace("{min}", this._minValue.ToString());
                builder.Replace("{max}", this._maxValue.ToString());
            }
            return builder.ToString();
        }

        public T MaxValue
        {
            get
            {
                return this._maxValue;
            }
            set
            {
                this._maxValue = value;
                this._maxValueSet = true;
            }
        }

        public bool MaxValueSet
        {
            get
            {
                return this._maxValueSet;
            }
        }

        public T MinValue
        {
            get
            {
                return this._minValue;
            }
            set
            {
                this._minValue = value;
                this._minValueSet = true;
            }
        }

        public bool MinValueSet
        {
            get
            {
                return this._minValueSet;
            }
        }
    }
}

