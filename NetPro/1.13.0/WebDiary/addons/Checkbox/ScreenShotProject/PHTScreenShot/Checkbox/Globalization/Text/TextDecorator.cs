﻿namespace Checkbox.Globalization.Text
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class TextDecorator : ITransactional
    {
        private List<string> _alternateLanguages;
        private string _language;

        public event EventHandler TransactionAborted;

        public event EventHandler TransactionCommitted;

        protected TextDecorator(string language)
        {
            this._language = string.IsNullOrEmpty(language) ? TextManager.DefaultLanguage : language;
            this._alternateLanguages = new List<string>();
        }

        public void AddAlternateLanguages(List<string> alternateLanguages)
        {
            if (alternateLanguages == null)
            {
                this._alternateLanguages.Clear();
            }
            else
            {
                this._alternateLanguages = alternateLanguages;
            }
        }

        public virtual Dictionary<string, string> GetAllTexts(string textID)
        {
            return TextManager.GetAllTexts(textID);
        }

        public ReadOnlyCollection<string> GetAlternateLanguages()
        {
            return new ReadOnlyCollection<string>(this.AlternateLanguages);
        }

        protected virtual string GetText(string textID)
        {
            string text = string.Empty;
            if (!string.IsNullOrEmpty(this._language))
            {
                text = TextManager.GetText(textID, this._language);
            }
            if (string.IsNullOrEmpty(text))
            {
                text = TextManager.GetText(textID, TextManager.DefaultLanguage);
                if ((text != null) && (text.Trim() != string.Empty))
                {
                    return ("[" + this._language + "] " + text);
                }
            }
            if (string.IsNullOrEmpty(text))
            {
                foreach (string str2 in this.AlternateLanguages)
                {
                    text = TextManager.GetText(textID, str2);
                    if ((text != null) && (text.Trim() != string.Empty))
                    {
                        return ("[" + this._language + "] " + text);
                    }
                }
            }
            return text;
        }

        public void NotifyAbort(object sender, EventArgs e)
        {
            this.OnAbort(sender, e);
        }

        public void NotifyCommit(object sender, EventArgs e)
        {
            this.OnCommit(sender, e);
        }

        protected virtual void OnAbort(object sender, EventArgs e)
        {
        }

        protected virtual void OnCommit(object sender, EventArgs e)
        {
        }

        protected virtual void OnRollback()
        {
        }

        public void Rollback()
        {
            this.OnRollback();
        }

        public abstract void Save();
        public abstract void Save(IDbTransaction t);
        protected abstract void SetLocalizedTexts();
        protected virtual void SetText(string textID, string textValue)
        {
            if (!string.IsNullOrEmpty(this._language))
            {
                TextManager.SetText(textID, this._language, textValue);
            }
        }

        protected virtual void SetText(string textID, string textValue, string language)
        {
            ArgumentValidation.CheckForNullReference(textID, "textID");
            ArgumentValidation.CheckForNullReference(language, "language");
            ArgumentValidation.CheckForEmptyString(textID, "textID");
            ArgumentValidation.CheckForEmptyString(language, "language");
            TextManager.SetText(textID, language, textValue);
        }

        protected List<string> AlternateLanguages
        {
            get
            {
                if (this._alternateLanguages == null)
                {
                    this._alternateLanguages = new List<string>();
                }
                return this._alternateLanguages;
            }
        }

        public string Language
        {
            get
            {
                return this._language;
            }
            set
            {
                this._language = value;
            }
        }
    }
}

