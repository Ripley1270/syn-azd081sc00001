﻿namespace Checkbox.Forms.Items.Configuration
{
    using System;

    [Serializable]
    public class HtmlItemTextDecorator : LocalizableResponseItemTextDecorator
    {
        public HtmlItemTextDecorator(HtmlItemData data, string language) : base(data, language)
        {
        }

        public HtmlItemData Data
        {
            get
            {
                return (HtmlItemData) base.Data;
            }
        }
    }
}

