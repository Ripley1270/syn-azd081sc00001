﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Computation;
    using Checkbox.Analytics.Data;
    using Checkbox.Analytics.Items.Configuration;
    using Checkbox.Common;
    using System;
    using System.Collections.Generic;
    using System.Data;

    [Serializable]
    public class FrequencyItem : AnalysisItem
    {
        protected override object GeneratePreviewData()
        {
            return this.ProcessData(true);
        }

        protected static DataTable GetOtherAnswerTable()
        {
            DataTable table = new DataTable {
                TableName = OtherAnswersTableName
            };
            table.Columns.Add("ResponseID", typeof(long));
            table.Columns.Add("ItemID", typeof(int));
            table.Columns.Add("OptionID", typeof(int));
            table.Columns.Add("AnswerText", typeof(string));
            return table;
        }

        private static bool IncludeOptionInFrequencyData(int itemID, int optionID)
        {
            return true;
        }

        protected override object ProcessData()
        {
            return this.ProcessData(false);
        }

        protected virtual object ProcessData(bool isPreview)
        {
            ItemAnswerAggregator aggregator = new ItemAnswerAggregator();
            FrequencyDataCalculator calculator = new FrequencyDataCalculator();
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            DataTable otherAnswerTable = GetOtherAnswerTable();
            foreach (int num in this.SourceItemIDs)
            {
                List<int> list = isPreview ? this.GetItemOptionIdsForPreview(num) : this.GetItemOptionIdsForReport(num);
                aggregator.AddItem(num, this.GetItemText(num), this.GetSourceItemTypeName(num));
                foreach (int num2 in list)
                {
                    if (IncludeOptionInFrequencyData(num, num2))
                    {
                        string optionText;
                        if (dictionary.ContainsKey(num2))
                        {
                            optionText = dictionary[num2];
                        }
                        else
                        {
                            optionText = this.GetOptionText(num, num2);
                            if ((optionText == null) || (optionText.Trim() == string.Empty))
                            {
                                optionText = base.GetOptionPoints(num, num2).ToString();
                            }
                        }
                        dictionary[num2] = optionText;
                        aggregator.AddItemOption(num, num2, dictionary[num2]);
                    }
                }
            }
            long answerId = 0x3e8L;
            foreach (int num4 in this.SourceItemIDs)
            {
                List<ItemAnswer> list2 = isPreview ? this.GetItemPreviewAnswers(num4, new long?(answerId), 0x3e8L) : base.GetItemAnswers(num4);
                foreach (ItemAnswer answer in list2)
                {
                    if (answer.AnswerId > answerId)
                    {
                        answerId = answer.AnswerId;
                    }
                    if (answer.OptionId.HasValue)
                    {
                        if (answer.IsOther)
                        {
                            if (this.OtherOption == OtherOption.Display)
                            {
                                aggregator.AddAnswer(answer.AnswerId, answer.ResponseId, num4, answer.OptionId, answer.AnswerText);
                            }
                            else
                            {
                                aggregator.AddAnswer(answer.AnswerId, answer.ResponseId, num4, answer.OptionId, this.GetText("/controlText/analysisItem/other"));
                            }
                            DataRow row = otherAnswerTable.NewRow();
                            row["ResponseID"] = answer.ResponseId;
                            row["ItemID"] = num4;
                            row["OptionID"] = answer.OptionId.Value;
                            row["AnswerText"] = answer.AnswerText;
                            otherAnswerTable.Rows.Add(row);
                        }
                        else
                        {
                            string str2 = dictionary.ContainsKey(answer.OptionId.Value) ? dictionary[answer.OptionId.Value] : this.GetOptionText(num4, answer.OptionId.Value);
                            aggregator.AddAnswer(answer.AnswerId, answer.ResponseId, num4, answer.OptionId, str2);
                        }
                    }
                    else if (Utilities.IsNotNullOrEmpty(answer.AnswerText))
                    {
                        aggregator.AddAnswer(answer.AnswerId, answer.ResponseId, num4, answer.AnswerText);
                    }
                }
                base.ResponseCounts[num4] = aggregator.GetResponseCount(new int?(num4));
                answerId += 1L;
            }
            DataSet data = calculator.GetData(aggregator);
            data.Tables.Add(otherAnswerTable);
            base.DataProcessed = true;
            return data;
        }

        public static string CalculatedDataTableName
        {
            get
            {
                return "Calculated";
            }
        }

        public static string OtherAnswersTableName
        {
            get
            {
                return "OtherAnswers";
            }
        }
    }
}

