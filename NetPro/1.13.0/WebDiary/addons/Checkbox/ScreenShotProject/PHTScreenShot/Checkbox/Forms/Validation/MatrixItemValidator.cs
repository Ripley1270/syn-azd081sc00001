﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using System;
    using System.Collections.Generic;

    public class MatrixItemValidator : Validator<MatrixItem>
    {
        private Dictionary<int, List<string>> _columnValidationErrors;

        private void AddColumnValidationError(int column, string error)
        {
            if (!this.ColumnValidationErrors.ContainsKey(column))
            {
                this.ColumnValidationErrors[column] = new List<string>();
            }
            this.ColumnValidationErrors[column].Add(error);
        }

        protected virtual List<IAnswerable> GetAnswerableColumnItems(MatrixItem matrixItem, int columnNumber)
        {
            List<IAnswerable> list = new List<IAnswerable>();
            int rowCount = matrixItem.RowCount;
            for (int i = 1; i <= rowCount; i++)
            {
                Item itemAt = matrixItem.GetItemAt(i, columnNumber);
                if ((itemAt != null) && (itemAt is IAnswerable))
                {
                    list.Add((IAnswerable) itemAt);
                }
            }
            return list;
        }

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/matrix/validationError", languageCode);
        }

        public override bool Validate(MatrixItem input)
        {
            bool flag = true;
            for (int i = 1; i <= input.ColumnCount; i++)
            {
                flag &= this.ValidateColumn(input, i);
            }
            return flag;
        }

        protected virtual bool ValidateColumn(MatrixItem matrixItem, int column)
        {
            bool flag = true;
            string columnTypeName = matrixItem.GetColumnTypeName(column);
            if (matrixItem.ColumnAnswersAreUnique(column))
            {
                flag = this.ValidateUniqueAnswers(matrixItem, column);
            }
            if ((columnTypeName != null) && columnTypeName.Equals("MatrixSumTotal", StringComparison.InvariantCultureIgnoreCase))
            {
                flag &= this.ValidateSumTotalColumn(matrixItem, column);
            }
            for (int i = 1; i <= matrixItem.RowCount; i++)
            {
                if (matrixItem.GetRowType(i) != RowType.Subheading)
                {
                    flag &= this.ValidateItem(matrixItem, column, i);
                }
            }
            return flag;
        }

        protected virtual bool ValidateItem(MatrixItem matrixItem, int column, int row)
        {
            Item itemAt = matrixItem.GetItemAt(row, column);
            if ((itemAt != null) && (itemAt is AnswerableItem))
            {
                if (itemAt.Excluded)
                {
                    return true;
                }
                if (matrixItem.GetRowType(row) == RowType.Other)
                {
                    return this.ValidateOtherItem(matrixItem, (AnswerableItem) itemAt, column, row);
                }
                ((AnswerableItem) itemAt).Validate();
                if (!((AnswerableItem) itemAt).Valid)
                {
                    this.ColumnValidationErrors[column] = ((AnswerableItem) itemAt).ValidationErrors;
                    return false;
                }
            }
            return true;
        }

        protected virtual bool ValidateOtherItem(MatrixItem matrixItem, AnswerableItem otherItem, int column, int row)
        {
            Item itemAt = matrixItem.GetItemAt(row, matrixItem.PrimaryColumnIndex);
            if (column != matrixItem.PrimaryColumnIndex)
            {
                if ((itemAt is AnswerableItem) && ((AnswerableItem) itemAt).HasAnswer)
                {
                    Item item3 = matrixItem.GetItemAt(row, column);
                    if ((item3 != null) && (item3 is AnswerableItem))
                    {
                        ((AnswerableItem) item3).Validate();
                        if (!((AnswerableItem) item3).Valid)
                        {
                            this.ColumnValidationErrors[column] = ((AnswerableItem) item3).ValidationErrors;
                            return false;
                        }
                    }
                }
            }
            else if ((itemAt != null) && (itemAt is AnswerableItem))
            {
                bool flag = false;
                for (int i = 1; i < matrixItem.ColumnCount; i++)
                {
                    if (i != column)
                    {
                        Item item2 = matrixItem.GetItemAt(row, i);
                        if (((item2 != null) && (item2 is AnswerableItem)) && ((AnswerableItem) item2).HasAnswer)
                        {
                            flag = true;
                            break;
                        }
                    }
                }
                if (flag)
                {
                    RequiredItemValidator validator = new RequiredItemValidator();
                    if (!validator.Validate((AnswerableItem) itemAt))
                    {
                        List<string> list = new List<string> {
                            validator.GetMessage(matrixItem.LanguageCode)
                        };
                        this.ColumnValidationErrors[column] = list;
                        return false;
                    }
                    return true;
                }
            }
            return true;
        }

        protected virtual bool ValidateSumTotalColumn(MatrixItem matrixItem, int columnNumber)
        {
            MatrixSumTotalItem columnTemplate = matrixItem.GetColumnTemplate(columnNumber) as MatrixSumTotalItem;
            if (columnTemplate != null)
            {
                ItemSumValidator validator = new ItemSumValidator(columnTemplate.TotalValue, columnTemplate.ComparisonOperator);
                if (!validator.Validate(this.GetAnswerableColumnItems(matrixItem, columnNumber)))
                {
                    this.AddColumnValidationError(columnNumber, validator.GetMessage(matrixItem.LanguageCode));
                    return false;
                }
            }
            return true;
        }

        protected virtual bool ValidateUniqueAnswers(MatrixItem matrixItem, int columnNumber)
        {
            ItemAnswerUniqueValidator validator = new ItemAnswerUniqueValidator();
            if (!validator.Validate(this.GetAnswerableColumnItems(matrixItem, columnNumber)))
            {
                this.AddColumnValidationError(columnNumber, validator.GetMessage(matrixItem.LanguageCode));
                return false;
            }
            return true;
        }

        public Dictionary<int, List<string>> ColumnValidationErrors
        {
            get
            {
                if (this._columnValidationErrors == null)
                {
                    this._columnValidationErrors = new Dictionary<int, List<string>>();
                }
                return this._columnValidationErrors;
            }
        }
    }
}

