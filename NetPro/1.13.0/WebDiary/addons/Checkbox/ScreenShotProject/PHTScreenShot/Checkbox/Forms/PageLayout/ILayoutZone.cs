﻿namespace Checkbox.Forms.PageLayout
{
    using System;

    public interface ILayoutZone
    {
        bool LayoutDesignMode { get; set; }

        string ZoneName { get; }
    }
}

