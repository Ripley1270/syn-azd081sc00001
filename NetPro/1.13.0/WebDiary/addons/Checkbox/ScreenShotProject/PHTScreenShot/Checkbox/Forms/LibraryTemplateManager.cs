﻿namespace Checkbox.Forms
{
    using Checkbox.Forms.Data;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.Sprocs;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Specialized;
    using System.Data;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Xml.Serialization;

    public class LibraryTemplateManager
    {
        public static LibraryTemplate CopyLibraryTemplate(int libraryID, ExtendedPrincipal owner, string languageCode)
        {
            int? nullable;
            DataSet ds = new DataSet();
            LibraryTemplate libraryTemplate = GetLibraryTemplate(libraryID);
            MemoryStream stream = new MemoryStream();
            new XmlSerializer(typeof(LibraryTemplate)).Serialize((Stream) stream, libraryTemplate);
            stream.Seek(0L, SeekOrigin.Begin);
            ds.ReadXml(stream, XmlReadMode.ReadSchema);
            LibraryTemplate template2 = CreateLibraryTemplate("Library Copy", owner);
            template2.Import(ds);
            int num = 1;
            string libraryName = string.Format("{0} {1} {2} {3}", new object[] { TextManager.GetText("/pageText/manageSurveys.aspx/copy", languageCode), num, TextManager.GetText("/pageText/manageSurveys.aspx/of", languageCode), TextManager.GetText(libraryTemplate.NameTextID, languageCode) });
        Label_00FC:
            nullable = null;
            if (LibraryTemplateExists(libraryName, nullable))
            {
                num++;
                libraryName = string.Format("{0} {1} {2} {3}", new object[] { TextManager.GetText("/pageText/manageSurveys.aspx/copy", languageCode), num, TextManager.GetText("/pageText/manageSurveys.aspx/of", languageCode), TextManager.GetText(libraryTemplate.NameTextID, languageCode) });
                goto Label_00FC;
            }
            TextManager.SetText(template2.NameTextID, languageCode, libraryName);
            template2.Save();
            return template2;
        }

        public static LibraryTemplate CreateLibraryTemplate(string libraryName, ExtendedPrincipal owner)
        {
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(owner, "Library.Create"))
            {
                throw new AuthorizationException();
            }
            LibraryTemplate template = new LibraryTemplate();
            if (owner == null)
            {
                string[] permissions = new string[] { "Library.View", "Library.Edit" };
                Policy defaultPolicy = template.CreatePolicy(permissions);
                AccessControlList acl = new AccessControlList();
                acl.Save();
                template.InitializeAccess(defaultPolicy, acl);
            }
            else
            {
                string[] strArray2 = new string[0];
                Policy policy2 = template.CreatePolicy(strArray2);
                AccessControlList list2 = new AccessControlList();
                Policy policy = new Policy(template.SupportedPermissions);
                list2.Add(owner, policy);
                list2.Save();
                template.InitializeAccess(policy2, list2);
            }
            template.Save();
            template.Load();
            return template;
        }

        public static void DeleteLibraryTemplate(int libraryTemplateID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Library_Delete");
            storedProcCommandWrapper.AddInParameter("LibraryTemplateID", DbType.Int32, libraryTemplateID);
            storedProcCommandWrapper.AddInParameter("ModifiedDate", DbType.DateTime, DateTime.Now);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public static DataTable GetAvailableLibraryTemplates(ExtendedPrincipal currentPrincipal, int pageSize, int page, string filterField, string filterText, string sortField, bool descending, string[] permissions, NameValueCollection extraPermissions, out int totalCount)
        {
            totalCount = 0;
            SelectQuery query = QueryFactory.GetAllLibraryTemplatesQuery(filterField, filterText, sortField, !descending);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            DataSet set = database.ExecuteDataSet(sqlStringCommandWrapper);
            if (((set == null) || (set.Tables.Count <= 0)) || (set.Tables[0] == null))
            {
                return null;
            }
            DataTable table = set.Tables[0].Clone();
            foreach (DataRow row in set.Tables[0].Rows)
            {
                LightweightAccessControllable lightweightLibraryTemplate = GetLightweightLibraryTemplate((int) row["LibraryTemplateID"]);
                if (AuthorizationFactory.GetAuthorizationProvider().Authorize(currentPrincipal, lightweightLibraryTemplate, "Library.View"))
                {
                    table.ImportRow(row);
                    totalCount++;
                }
            }
            return table;
        }

        public static LibraryTemplate GetLibraryTemplate(int libraryID)
        {
            LibraryTemplate template = null;
            try
            {
                template = new LibraryTemplate();
                template.Load(libraryID);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
            return template;
        }

        public static LightweightAccessControllable GetLightweightLibraryTemplate(int templateID)
        {
            LightweightTemplate theObject = new LightweightTemplate(templateID);
            StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Select, theObject);
            return theObject;
        }

        public static bool LibraryTemplateExists(string libraryName, int? idToIgnore)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Library_GetLibraryTemplateIDFromName");
            storedProcCommandWrapper.AddInParameter("LibraryName", DbType.String, libraryName);
            storedProcCommandWrapper.AddOutParameter("LibraryID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("LibraryID");
            if (parameterValue == DBNull.Value)
            {
                return false;
            }
            return (!idToIgnore.HasValue || (idToIgnore.Value != ((int) parameterValue)));
        }
    }
}

