﻿namespace Checkbox.Styles
{
    using Checkbox.Forms.Items.UI;
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    public static class ChartStyleManager
    {
        public static int CreateStyle(int appearanceID, string name, string createdBy, bool isPublic, bool isEditable)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AppearancePreset_Insert");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, appearanceID);
            storedProcCommandWrapper.AddInParameter("PresetName", DbType.String, name);
            storedProcCommandWrapper.AddInParameter("CreatedBy", DbType.String, createdBy);
            storedProcCommandWrapper.AddInParameter("Public", DbType.Boolean, isPublic);
            storedProcCommandWrapper.AddInParameter("Editable", DbType.Boolean, isEditable);
            storedProcCommandWrapper.AddOutParameter("PresetID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("PresetID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Create Style failed.  A valid preset ID was not returned.");
            }
            return (int) parameterValue;
        }

        public static void DeleteStyle(int presetID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AppearancePreset_Delete");
            storedProcCommandWrapper.AddInParameter("PresetID", DbType.Int32, presetID);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public static ChartStyle GetChartStyle(int chartStyleID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AppearancePreset_Get");
            storedProcCommandWrapper.AddInParameter("PresetID", DbType.Int32, chartStyleID);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return new ChartStyle(chartStyleID, (int) reader["AppearanceID"], (string) reader["PresetName"], (string) reader["CreatedBy"], (bool) reader["Editable"], (bool) reader["Public"]);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return null;
        }

        public static AppearanceData GetChartStyleAppearance(int chartStyleID)
        {
            ChartStyle chartStyle = GetChartStyle(chartStyleID);
            if (chartStyle != null)
            {
                return AppearanceDataManager.GetAppearanceData(chartStyle.AppearanceID);
            }
            return null;
        }

        public static int? GetStyleIDFromName(string name)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AppearancePreset_FindByName");
            storedProcCommandWrapper.AddInParameter("PresetName", DbType.String, name);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read() && (reader["PresetID"] != DBNull.Value))
                    {
                        return new int?((int) reader["PresetID"]);
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return null;
        }

        public static DataSet ListAllStyles()
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AppearancePreset_ListAll");
            return database.ExecuteDataSet(storedProcCommandWrapper);
        }

        public static DataSet ListAvailableStyles(string uniqueIdentifier, bool onlyEditable)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AppearancePreset_ListForUser");
            storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, uniqueIdentifier);
            storedProcCommandWrapper.AddInParameter("OnlyEditable", DbType.Boolean, onlyEditable);
            return database.ExecuteDataSet(storedProcCommandWrapper);
        }

        public static void UpdateStyle(int presetID, string name, bool isPublic, bool isEditable)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AppearancePreset_Update");
            storedProcCommandWrapper.AddInParameter("PresetID", DbType.Int32, presetID);
            storedProcCommandWrapper.AddInParameter("PresetName", DbType.String, name);
            storedProcCommandWrapper.AddInParameter("Public", DbType.Boolean, isPublic);
            storedProcCommandWrapper.AddInParameter("Editable", DbType.Boolean, isEditable);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }
    }
}

