﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class MatrixRadioButtonScale : MatrixRatingScale
    {
        public override string AppearanceCode
        {
            get
            {
                return "MATRIX_RADIO_BUTTON_SCALE";
            }
        }
    }
}

