﻿namespace Checkbox.Analytics.Filters.Configuration
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate string ItemFilterTextPrefixCallback(int itemId, string languageCode);
}

