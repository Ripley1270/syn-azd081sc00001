﻿namespace Checkbox.Forms.Security
{
    using Prezza.Framework.Security;
    using System;

    [Serializable]
    internal class FormFolderPolicy : Policy
    {
        private static string[] allowablePermissions = new string[] { "FormFolder.Read", "FormFolder.FullControl" };

        public FormFolderPolicy() : base(allowablePermissions)
        {
        }

        public FormFolderPolicy(string[] permissions) : base(permissions)
        {
        }
    }
}

