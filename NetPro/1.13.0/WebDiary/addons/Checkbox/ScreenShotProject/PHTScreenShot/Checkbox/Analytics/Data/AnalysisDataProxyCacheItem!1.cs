﻿namespace Checkbox.Analytics.Data
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class AnalysisDataProxyCacheItem<T>
    {
        public T Data { get; set; }

        public DateTime ReferenceDate { get; set; }
    }
}

