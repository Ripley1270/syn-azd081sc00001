﻿namespace Checkbox.Forms.Piping.Tokens
{
    using System;

    [Serializable]
    public class ItemToken : Token
    {
        private int _itemID;

        public ItemToken(string token, int itemID) : base(token, TokenType.Answer)
        {
            this._itemID = itemID;
        }

        public int ItemID
        {
            get
            {
                return this._itemID;
            }
            set
            {
                this._itemID = value;
            }
        }
    }
}

