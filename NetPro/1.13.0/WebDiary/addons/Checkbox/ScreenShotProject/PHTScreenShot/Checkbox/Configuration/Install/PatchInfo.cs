﻿namespace Checkbox.Configuration.Install
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;

    [Serializable]
    public class PatchInfo : IComparable<PatchInfo>
    {
        private readonly string _patchfConfigurationXML;

        private PatchInfo(XmlDocument doc)
        {
            this._patchfConfigurationXML = doc.OuterXml;
        }

        public int CompareTo(PatchInfo other)
        {
            return string.Compare(other.Name, this.Name, true);
        }

        public static List<PatchInfo> GetAvailablePatches(string rootSearchPath)
        {
            List<PatchInfo> list = new List<PatchInfo>();
            if (!string.IsNullOrEmpty(rootSearchPath))
            {
                foreach (string str in Directory.GetDirectories(rootSearchPath + Path.DirectorySeparatorChar + "Patches"))
                {
                    if (File.Exists(str + Path.DirectorySeparatorChar + "PatchInfo.xml"))
                    {
                        XmlDocument doc = new XmlDocument();
                        try
                        {
                            doc.Load(str + @"\PatchInfo.xml");
                            list.Add(new PatchInfo(doc));
                        }
                        catch
                        {
                        }
                    }
                }
            }
            return list;
        }

        public List<string> GetDatabaseScripts(string currentVersion)
        {
            List<string> list = new List<string>();
            foreach (XmlNode node in this.PatchInfoDoc.SelectNodes("/patchInfo/patchDetails[@currentVersion='" + currentVersion + "']/databaseScripts/databaseScript"))
            {
                list.Add(node.InnerText);
            }
            return list;
        }

        public List<InstallFileInfo> GetFiles(string currentVersion)
        {
            List<InstallFileInfo> list = new List<InstallFileInfo>();
            foreach (XmlNode node in this.PatchInfoDoc.SelectNodes("/patchInfo/patchDetails[@currentVersion='" + currentVersion + "']/files/file"))
            {
                list.Add(new InstallFileInfo(XmlUtility.GetNodeText(node.SelectSingleNode("source")), XmlUtility.GetNodeText(node.SelectSingleNode("destination"))));
            }
            return list;
        }

        internal static PatchInfo GetPatchInfo(string name, string description)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<patchInfo><properties><name>" + name + "</name><description>" + description + "</description></properties></patchInfo>");
            return new PatchInfo(doc);
        }

        public bool InstallStyles(string currentVersion)
        {
            return XmlUtility.GetNodeBool(this.PatchInfoDoc.SelectSingleNode("/patchInfo/patchDetails[@currentVersion='" + currentVersion + "']/installStyles"));
        }

        public bool UpdateDbProfileSecurityConfig(string currentVersion)
        {
            return XmlUtility.GetNodeBool(this.PatchInfoDoc.SelectSingleNode("/patchInfo/patchDetails[@currentVersion='" + currentVersion + "']/updateDbProfileSecurityConfig"));
        }

        public bool UpdateGlobalizationConfig(string currentVersion)
        {
            return XmlUtility.GetNodeBool(this.PatchInfoDoc.SelectSingleNode("/patchInfo/patchDetails[@currentVersion='" + currentVersion + "']/updateGlobalizationConfig"));
        }

        public bool UpdateGraphs(string currentVersion)
        {
            return XmlUtility.GetNodeBool(this.PatchInfoDoc.SelectSingleNode("/patchInfo/patchDetails[@currentVersion='" + currentVersion + "']/updateGraphs"));
        }

        public bool UpdateLdapSecurityConfig(string currentVersion)
        {
            return XmlUtility.GetNodeBool(this.PatchInfoDoc.SelectSingleNode("/patchInfo/patchDetails[@currentVersion='" + currentVersion + "']/updateLdapSecurityConfig"));
        }

        public string Description
        {
            get
            {
                return XmlUtility.GetNodeText(this.PatchInfoDoc.SelectSingleNode("/patchInfo/properties/description"));
            }
        }

        public List<string> Fixes
        {
            get
            {
                List<string> list = new List<string>();
                foreach (XmlNode node in this.PatchInfoDoc.SelectNodes("/patchInfo/fixes/fix"))
                {
                    list.Add(node.InnerText);
                }
                return list;
            }
        }

        public List<string> KnownIssues
        {
            get
            {
                List<string> list = new List<string>();
                foreach (XmlNode node in this.PatchInfoDoc.SelectNodes("/patchInfo/knownIssues/knownIssue"))
                {
                    list.Add(node.InnerText);
                }
                return list;
            }
        }

        public string Name
        {
            get
            {
                return XmlUtility.GetNodeText(this.PatchInfoDoc.SelectSingleNode("/patchInfo/properties/name"));
            }
        }

        private XmlDocument PatchInfoDoc
        {
            get
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(this._patchfConfigurationXML);
                return document;
            }
        }

        public string PreRequisiteModule
        {
            get
            {
                return XmlUtility.GetNodeText(this.PatchInfoDoc.SelectSingleNode("/patchInfo/preRequisites/module/name"));
            }
        }

        public List<string> PreRequisiteModuleVersions
        {
            get
            {
                List<string> list = new List<string>();
                foreach (XmlNode node in this.PatchInfoDoc.SelectNodes("/patchInfo/preRequisites/module/versions/version"))
                {
                    list.Add(node.InnerText);
                }
                return list;
            }
        }

        public string PreRequisiteProduct
        {
            get
            {
                return XmlUtility.GetNodeText(this.PatchInfoDoc.SelectSingleNode("/patchInfo/preRequisites/product/name"));
            }
        }

        public List<string> PreRequisiteProductVersions
        {
            get
            {
                List<string> list = new List<string>();
                foreach (XmlNode node in this.PatchInfoDoc.SelectNodes("/patchInfo/preRequisites/product/versions/version"))
                {
                    list.Add(node.InnerText);
                }
                return list;
            }
        }

        public string SetupInstructionsUrl
        {
            get
            {
                return XmlUtility.GetNodeText(this.PatchInfoDoc.SelectSingleNode("/patchInfo/properties/setupInstructionsUrl"));
            }
        }

        public string SetupURL
        {
            get
            {
                return XmlUtility.GetNodeText(this.PatchInfoDoc.SelectSingleNode("/patchInfo/properties/setupUrl"));
            }
        }

        public string Version
        {
            get
            {
                return XmlUtility.GetNodeText(this.PatchInfoDoc.SelectSingleNode("/patchInfo/properties/version"));
            }
        }
    }
}

