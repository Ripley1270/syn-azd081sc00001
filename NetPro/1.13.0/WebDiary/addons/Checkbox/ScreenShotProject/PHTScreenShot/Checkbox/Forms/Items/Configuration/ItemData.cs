﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Items.UI;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Xml;

    [Serializable]
    public abstract class ItemData : AbstractPersistedDomainObject, IEquatable<ItemData>
    {
        protected ItemData()
        {
            this.IsActive = true;
        }

        protected virtual ItemData Copy()
        {
            return ItemConfigurationManager.CreateConfigurationData(this.ItemTypeID);
        }

        public ItemData Copy(int? idOfCopy)
        {
            ItemData data = this.Copy();
            if (data != null)
            {
                data.Alias = this.Alias;
                data.ItemTypeID = this.ItemTypeID;
                data.ItemTypeName = this.ItemTypeName;
                data.ID = idOfCopy;
                data.IsActive = this.IsActive;
            }
            return data;
        }

        protected override void Create(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Item_Insert");
            storedProcCommandWrapper.AddInParameter("ItemTypeID", DbType.Int32, this.ItemTypeID);
            storedProcCommandWrapper.AddInParameter("Alias", DbType.String, this.Alias);
            storedProcCommandWrapper.AddInParameter("CreatedDate", DbType.DateTime, DateTime.Now);
            storedProcCommandWrapper.AddInParameter("IsActive", DbType.Boolean, true);
            storedProcCommandWrapper.AddOutParameter("ItemID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("ItemID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save item data.");
            }
            base.ID = new int?((int) parameterValue);
        }

        protected abstract Item CreateItem();
        public Item CreateItem(string languageCode)
        {
            try
            {
                Item item = this.CreateItem();
                if (item != null)
                {
                    this.InitializeItem(item, languageCode);
                    return item;
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "FrameworkPublic"))
                {
                    throw;
                }
            }
            return null;
        }

        public virtual ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new ItemTextDecorator(this, languageCode);
        }

        public override void Delete(IDbTransaction t)
        {
            if (base.ID > 0)
            {
                try
                {
                    Database database = DatabaseFactory.CreateDatabase();
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Item_Delete");
                    storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
                    storedProcCommandWrapper.AddInParameter("ModifiedDate", DbType.DateTime, DateTime.Now);
                    database.ExecuteNonQuery(storedProcCommandWrapper, t);
                    this.IsDeleted = true;
                }
                catch (Exception exception)
                {
                    if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                    {
                        throw;
                    }
                }
            }
        }

        public bool Equals(ItemData other)
        {
            return (base.ID == other.ID);
        }

        protected virtual DataSet GetAppearanceDataForExport(out string appearanceDataTableName, out string appearanceIdentityColumnName)
        {
            appearanceDataTableName = string.Empty;
            appearanceIdentityColumnName = string.Empty;
            if (base.ID.HasValue)
            {
                AppearanceData appearanceDataForItem = AppearanceDataManager.GetAppearanceDataForItem(base.ID.Value);
                if (appearanceDataForItem != null)
                {
                    appearanceDataTableName = appearanceDataForItem.ParentDataTableName;
                    appearanceIdentityColumnName = appearanceDataForItem.IdentityColumnName;
                    AppearanceData appearanceDataForCode = AppearanceDataManager.GetAppearanceDataForCode(appearanceDataForItem.AppearanceCode);
                    appearanceDataForCode.ID = new int?(appearanceDataForItem.ID.Value);
                    DataSet configurationDataSet = appearanceDataForCode.GetConfigurationDataSet();
                    appearanceDataForCode.CreateDataRelations(configurationDataSet);
                    if (configurationDataSet.Tables.Contains(appearanceDataTableName) && !configurationDataSet.Tables[appearanceDataTableName].Columns.Contains(this.IdentityColumnName))
                    {
                        configurationDataSet.Tables[appearanceDataTableName].Columns.Add(this.IdentityColumnName, typeof(int));
                        foreach (DataRow row in configurationDataSet.Tables[appearanceDataTableName].Select(appearanceIdentityColumnName + " = " + appearanceDataForItem.ID, null, DataViewRowState.CurrentRows))
                        {
                            row[this.IdentityColumnName] = base.ID.Value;
                            row[appearanceDataForItem.IdentityColumnName] = -1 * appearanceDataForItem.ID.Value;
                        }
                    }
                    return configurationDataSet;
                }
            }
            return null;
        }

        protected virtual DataSet GetDataForExport()
        {
            string str;
            string str2;
            DataSet set = this.ReloadItemData();
            DataSet appearanceDataForExport = this.GetAppearanceDataForExport(out str, out str2);
            if ((set != null) && (appearanceDataForExport != null))
            {
                set.Merge(appearanceDataForExport);
                string name = this.DataTableName + "_" + str;
                if (!set.Relations.Contains(name))
                {
                    DataRelation relation = new DataRelation(name, set.Tables[this.DataTableName].Columns[this.IdentityColumnName], set.Tables[str].Columns[this.IdentityColumnName]);
                    set.Relations.Add(relation);
                }
            }
            return set;
        }

        protected virtual void InitializeItem(Item item, string languageCode)
        {
            item.Configure(this, languageCode);
        }

        protected override void Load(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            if (this.DataTableName == data.Table.TableName)
            {
                this.LoadFromDataRow(data);
            }
            else if (data.Table.TableName == this.ParentDataTableName)
            {
                try
                {
                    this.Alias = DbUtility.GetValueFromDataRow<string>(data, "Alias", string.Empty);
                    this.ItemTypeID = DbUtility.GetValueFromDataRow<int>(data, "ItemTypeID", -1);
                    this.ItemTypeName = DbUtility.GetValueFromDataRow<string>(data, "ItemName", string.Empty);
                    base.CreatedDate = DbUtility.GetValueFromDataRow<DateTime?>(data, "CreatedDate", null);
                    base.LastModified = DbUtility.GetValueFromDataRow<DateTime?>(data, "ModifiedDate", null);
                    this.IsActive = DbUtility.GetValueFromDataRow<bool>(data, "IsActive", true);
                }
                catch (Exception exception)
                {
                    if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                    {
                        throw;
                    }
                }
            }
        }

        protected virtual DataSet ReloadItemData()
        {
            ItemData data = ItemConfigurationManager.CreateConfigurationData(this.ItemTypeName);
            data.ID = base.ID;
            DataSet configurationDataSet = data.GetConfigurationDataSet();
            data.Load(configurationDataSet);
            data.CreateDataRelations(configurationDataSet);
            return configurationDataSet;
        }

        protected override void Update(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Item_Update");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("Alias", DbType.String, this.Alias);
            storedProcCommandWrapper.AddInParameter("ModifiedDate", DbType.DateTime, DateTime.Now);
            storedProcCommandWrapper.AddInParameter("IsActive", DbType.Boolean, this.IsActive);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public override void WriteXml(XmlWriter writer)
        {
            DataSet dataForExport = this.GetDataForExport();
            if (dataForExport != null)
            {
                dataForExport.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;
                dataForExport.WriteXml(writer, XmlWriteMode.WriteSchema);
            }
        }

        public string Alias { get; set; }

        protected override DBCommandWrapper ConfigurationDataSetCommand
        {
            get
            {
                DBCommandWrapper storedProcCommandWrapper = DatabaseFactory.CreateDatabase().GetStoredProcCommandWrapper("ckbx_Item_GetItem");
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
                return storedProcCommandWrapper;
            }
        }

        public override string IdentityColumnName
        {
            get
            {
                return "ItemId";
            }
        }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; private set; }

        public virtual bool IsExportable
        {
            get
            {
                return true;
            }
        }

        public virtual bool ItemIsIAnswerable
        {
            get
            {
                return false;
            }
        }

        public virtual bool ItemIsIScored
        {
            get
            {
                return false;
            }
        }

        public int ItemTypeID { get; set; }

        public string ItemTypeName { get; set; }

        public override string ParentDataTableName
        {
            get
            {
                return "Items";
            }
        }
    }
}

