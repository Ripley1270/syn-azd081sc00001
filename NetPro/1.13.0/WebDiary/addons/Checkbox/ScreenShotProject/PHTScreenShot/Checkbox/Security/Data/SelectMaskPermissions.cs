﻿namespace Checkbox.Security.Data
{
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using System;

    internal static class SelectMaskPermissions
    {
        internal static SelectQuery GetQuery(string maskName)
        {
            SelectQuery query = new SelectQuery("ckbx_PermissionMask");
            query.AddAllParameter("ckbx_Permission");
            query.AddTableJoin("ckbx_PermissionMaskPermissions", QueryJoinType.Inner, "MaskID", "ckbx_PermissionMask", "MaskID");
            query.AddTableJoin("ckbx_Permission", QueryJoinType.Inner, "PermissionID", "ckbx_PermissionMaskPermissions", "PermissionID");
            query.AddCriterion(new QueryCriterion(new SelectParameter("MaskName", string.Empty, "ckbx_PermissionMask"), CriteriaOperator.EqualTo, new LiteralParameter("'" + maskName + "'")));
            return query;
        }
    }
}

