﻿namespace Checkbox.Security.Data
{
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security;
    using System;

    internal static class SelectGroupsInAclQuery
    {
        public static SelectQuery GetQuery(IAccessControlList acl)
        {
            SelectQuery subQuery = new SelectQuery("ckbx_AccessControlEntries");
            subQuery.AddParameter("EntryID", string.Empty, "ckbx_AccessControlEntries");
            subQuery.AddCriterion(new QueryCriterion(new SelectParameter("AclID", string.Empty, "ckbx_AccessControlEntries"), CriteriaOperator.EqualTo, new LiteralParameter(acl.ID)));
            SelectQuery query2 = new SelectQuery("ckbx_AccessControlEntry");
            query2.AddParameter("EntryIdentifier", string.Empty, "ckbx_AccessControlEntry");
            query2.AddCriterion(new QueryCriterion(new SelectParameter("EntryType", string.Empty, "ckbx_AccessControlEntry"), CriteriaOperator.EqualTo, new LiteralParameter("'Checkbox.Users.Group'")));
            query2.AddCriterion(new QueryCriterion(new SelectParameter("EntryID", string.Empty, "ckbx_AccessControlEntry"), CriteriaOperator.In, new SubqueryParameter(subQuery)));
            return query2;
        }
    }
}

