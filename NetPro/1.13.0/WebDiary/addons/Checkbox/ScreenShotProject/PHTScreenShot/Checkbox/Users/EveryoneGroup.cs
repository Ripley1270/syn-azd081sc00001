﻿namespace Checkbox.Users
{
    using System;
    using System.Security.Principal;

    [Serializable]
    public class EveryoneGroup : Group
    {
        public EveryoneGroup() : base("Everyone")
        {
        }

        public void AddUser(IIdentity identity)
        {
            throw new NotSupportedException("Users cannot be added to the Everyone group.");
        }

        public void AddUser(IPrincipal principal)
        {
            throw new NotSupportedException("Users cannot be added to the Everyone group.");
        }

        public void AddUser(string uniqueName)
        {
            throw new NotSupportedException("Users cannot be added to the Everyone group.");
        }

        public IIdentity[] GetUserIdentities()
        {
            throw new NotSupportedException("Listing of users in the Everyone group is not supported.");
        }

        public IPrincipal[] GetUsers()
        {
            throw new NotSupportedException("Listing of users in the Everyone group is not supported.");
        }

        public void RemoveUser(IIdentity identity)
        {
            throw new NotSupportedException("Users cannot be removed from the Everyone group.");
        }

        public void RemoveUser(IPrincipal principal)
        {
            throw new NotSupportedException("Users cannot be removed from the Everyone group.");
        }

        public void RemoveUser(string uniqueName)
        {
            throw new NotSupportedException("Users cannot be removed from the Everyone group.");
        }
    }
}

