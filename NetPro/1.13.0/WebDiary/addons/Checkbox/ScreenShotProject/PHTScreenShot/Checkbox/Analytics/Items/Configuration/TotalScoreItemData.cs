﻿namespace Checkbox.Analytics.Items.Configuration
{
    using Checkbox.Analytics.Items;
    using Checkbox.Forms.Items;
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class TotalScoreItemData : AnalysisItemData
    {
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to save analysis item.  DataID is <= 0.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertTotalScore");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateDataTables(t);
        }

        protected override Item CreateItem()
        {
            return new TotalScoreItem();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID.Value <= 0))
            {
                throw new Exception("DataID not specified.  Unable to load total score item data.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetTotalScore");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName, AnalysisItemData.SourceItemsTableName, AnalysisItemData.ResponseTemplatesTableName });
            return dataSet;
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to save analysis item.  DataID is <= 0.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateTotalScore");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateDataTables(t);
        }

        public override string DataTableName
        {
            get
            {
                return "TotalScoreItemData";
            }
        }
    }
}

