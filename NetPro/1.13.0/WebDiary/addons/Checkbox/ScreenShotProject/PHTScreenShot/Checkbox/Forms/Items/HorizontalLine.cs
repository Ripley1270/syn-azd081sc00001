﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Specialized;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class HorizontalLine : ResponseItem
    {
        private string _unit;
        private int? _width;

        public override void Configure(ItemData configuration, string languageCode)
        {
            base.Configure(configuration, languageCode);
            ArgumentValidation.CheckExpectedType(configuration, typeof(HorizontalLineItemData));
            HorizontalLineItemData data = (HorizontalLineItemData) configuration;
            this._width = data.LineWidth;
            this._unit = data.WidthUnit;
            this.Thickness = data.Thickness;
            this.Color = data.Color;
        }

        protected override NameValueCollection GetInstanceDataValuesForXmlSerialization()
        {
            NameValueCollection instanceDataValuesForXmlSerialization = base.GetInstanceDataValuesForXmlSerialization();
            instanceDataValuesForXmlSerialization["Width"] = this.Width;
            instanceDataValuesForXmlSerialization["Color"] = this.Color;
            instanceDataValuesForXmlSerialization["Unit"] = this.Unit;
            instanceDataValuesForXmlSerialization["Thickness"] = this.Thickness.HasValue ? this.Thickness.ToString() : null;
            return instanceDataValuesForXmlSerialization;
        }

        public string Color { get; private set; }

        public int? Thickness { get; private set; }

        private string Unit
        {
            get
            {
                if (Utilities.IsNullOrEmpty(this._unit))
                {
                    return string.Empty;
                }
                if (string.Compare(this._unit, "PIXEL", true) == 0)
                {
                    return "px";
                }
                return "%";
            }
        }

        public string Width
        {
            get
            {
                if (this._width.HasValue)
                {
                    return (this._width.Value + this.Unit);
                }
                return string.Empty;
            }
        }
    }
}

