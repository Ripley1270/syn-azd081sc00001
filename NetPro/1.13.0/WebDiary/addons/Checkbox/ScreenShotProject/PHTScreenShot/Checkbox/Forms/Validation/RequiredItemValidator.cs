﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using System;

    public class RequiredItemValidator : Validator<AnswerableItem>
    {
        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/required", languageCode);
        }

        public override bool Validate(AnswerableItem input)
        {
            return input.HasAnswer;
        }
    }
}

