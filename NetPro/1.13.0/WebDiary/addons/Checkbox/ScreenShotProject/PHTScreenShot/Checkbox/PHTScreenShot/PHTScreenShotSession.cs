﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Checkbox.Forms
{

	class PHTScreenShotSession
	{
        public PHTScreenShotSession()
        {
        }
        
        public bool ScreenShotMode
        {
            get 
            {
                return this.GetValue("ScreenShot");
            }
        }

        protected bool GetValue(string paramName)
        {
            string screenShot = string.Empty;
            //First Check querystring
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString[paramName]))
                screenShot = HttpContext.Current.Request.QueryString[paramName].ToString().Trim();

            //Check Form if screenShot is empty
            if (string.IsNullOrEmpty(screenShot) && !string.IsNullOrEmpty(HttpContext.Current.Request.Form[paramName]))
                screenShot = HttpContext.Current.Request.Form[paramName].ToString().Trim();

            if (!string.IsNullOrEmpty(screenShot) && (screenShot == "true"))
                return true;
            else
                return false;
      }
    }
}
