﻿namespace Checkbox.Forms.Validation
{
    using System;

    public abstract class Validator<T>
    {
        protected Validator()
        {
        }

        public abstract string GetMessage(string languageCode);
        public abstract bool Validate(T input);
    }
}

