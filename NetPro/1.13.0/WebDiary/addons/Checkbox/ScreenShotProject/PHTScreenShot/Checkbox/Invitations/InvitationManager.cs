﻿using Checkbox.Management;
using Checkbox.Common;
using Checkbox.Forms;
using Checkbox.Globalization.Text;
using Checkbox.Invitations.Data;
using Checkbox.Messaging.Email;
using Prezza.Framework.Data;
using Prezza.Framework.Security;
using Prezza.Framework.Security.Principal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.InteropServices;

namespace Checkbox.Invitations
{
    //using Checkbox.Common;
    //using Checkbox.Forms;
    //using Checkbox.Globalization.Text;
    //using Checkbox.Invitations.Data;
    //using Checkbox.Messaging.Email;
    //using Prezza.Framework.Data;
    //using Prezza.Framework.Security;
    //using Prezza.Framework.Security.Principal;
    //using System;
    //using System.Collections.Generic;
    //using System.Data;
    //using System.Runtime.InteropServices;

    public static class InvitationManager
    {
        public static string SURVEY_URL_PLACEHOLDER = (ApplicationManager.AppSettings.PipePrefix + "SURVEY_URL_PLACEHOLDER__DO_NOT_ERASE");

        public static void CloseInvitationEmailBatch(long batchId)
        {
            EmailGateway.MarkEmailBatchReady(batchId);
        }

        public static long? CreateInvitationEmailBatch(int invitationId, string invitationMode, int recipientCount, string createdBy, DateTime? earliestSendDate)
        {
            long? nullable = null;
            if (EmailGateway.ProviderSupportsBatches)
            {
                string batchName = string.Format("Invitation {0} {1} batch of {2} for {3} recipients", new object[] { invitationId, invitationMode, DateTime.Now, recipientCount });
                nullable = EmailGateway.CreateEmailBatch(batchName, createdBy, earliestSendDate);
                if (nullable.HasValue)
                {
                    Database database = DatabaseFactory.CreateDatabase();
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_RecordBatch");
                    storedProcCommandWrapper.AddInParameter("InvitationId", DbType.Int32, invitationId);
                    storedProcCommandWrapper.AddInParameter("BatchId", DbType.Int64, nullable.Value);
                    storedProcCommandWrapper.AddInParameter("BatchDescription", DbType.String, batchName);
                    database.ExecuteNonQuery(storedProcCommandWrapper);
                }
            }
            return nullable;
        }

        public static Invitation CreateInvitationForSurvey(int surveyId, MailFormat messageFormat, ExtendedPrincipal creatorPrincipal)
        {
            if (surveyId <= 0)
            {
                return null;
            }
            LightweightResponseTemplate lightweightResponseTemplate = ResponseTemplateManager.GetLightweightResponseTemplate(surveyId);
            if ((lightweightResponseTemplate == null) || Utilities.IsNullOrEmpty(lightweightResponseTemplate.DefaultLanguage))
            {
                return null;
            }
            Invitation invitation = new Invitation {
                ParentID = surveyId
            };
            string text = TextManager.GetText(lightweightResponseTemplate.TitleTextId, lightweightResponseTemplate.DefaultLanguage);
            if ((text == null) || (text.Trim() == string.Empty))
            {
                text = lightweightResponseTemplate.Name;
            }
            if ((text == null) || (text.Trim() == string.Empty))
            {
                text = TextManager.GetText("/pageText/newInvitation.aspx/survey", lightweightResponseTemplate.DefaultLanguage, "survey", new string[] { TextManager.DefaultLanguage });
            }
            invitation.Template.Subject = TextManager.GetText("/pageText/newInvitation.aspx/invitationToTakeSurvey", lightweightResponseTemplate.DefaultLanguage, "Invitation to take a survey", new string[] { TextManager.DefaultLanguage });
            string str2 = TextManager.GetText("/siteText/siteName", TextManager.DefaultLanguage);
            if ((str2 != null) && (str2.Trim() != string.Empty))
            {
                InvitationTemplate template = invitation.Template;
                template.Subject = template.Subject + TextManager.GetText("/pageText/newInvitation.aspx/at", lightweightResponseTemplate.DefaultLanguage, " at ", new string[] { TextManager.DefaultLanguage });
                InvitationTemplate template2 = invitation.Template;
                template2.Subject = template2.Subject + str2;
            }
            string str3 = TextManager.GetText("/pageText/newInvitation.aspx/youHaveBeenInvited", lightweightResponseTemplate.DefaultLanguage, "You have been invited to take the survey: ", new string[] { TextManager.DefaultLanguage }) + text + ".";
            if (messageFormat == MailFormat.Html)
            {
                invitation.Template.Format = MailFormat.Html;
                invitation.Template.Body = str3;
                InvitationTemplate template3 = invitation.Template;
                template3.Body = template3.Body + "<br /><br />";
                InvitationTemplate template4 = invitation.Template;
                template4.Body = template4.Body + "<a href=\"" + SURVEY_URL_PLACEHOLDER + "\">";
                InvitationTemplate template5 = invitation.Template;
                template5.Body = template5.Body + TextManager.GetText("/pageText/newInvitation.aspx/clickHere", lightweightResponseTemplate.DefaultLanguage, "Click here", new string[] { TextManager.DefaultLanguage }) + "</a>&nbsp;";
                InvitationTemplate template6 = invitation.Template;
                template6.Body = template6.Body + TextManager.GetText("/pageText/newInvitation.aspx/toTakeTheSurvey", lightweightResponseTemplate.DefaultLanguage, " to take the survey", new string[] { TextManager.DefaultLanguage }) + ".";
                InvitationTemplate template7 = invitation.Template;
                string optOutText = template7.OptOutText;
                template7.OptOutText = optOutText + TextManager.GetText("/pageText/newInvitation.aspx/optOutHref", lightweightResponseTemplate.DefaultLanguage, "Please visit the following URL to remove yourself from the invitation list for this survey: ", new string[] { TextManager.DefaultLanguage }) + "&nbsp;<a href=\"" + invitation.Template.OptOutURL + "\">" + TextManager.GetText("/pageText/newInvitation.aspx/clickHere", lightweightResponseTemplate.DefaultLanguage, "Click here", new string[] { TextManager.DefaultLanguage }) + "</a>";
            }
            else
            {
                invitation.Template.Format = MailFormat.Text;
                invitation.Template.Body = str3;
                InvitationTemplate template8 = invitation.Template;
                template8.Body = template8.Body + Environment.NewLine + Environment.NewLine;
                InvitationTemplate template9 = invitation.Template;
                template9.Body = template9.Body + SURVEY_URL_PLACEHOLDER;
                InvitationTemplate template10 = invitation.Template;
                template10.OptOutText = template10.OptOutText + TextManager.GetText("/pageText/newInvitation.aspx/optOut", lightweightResponseTemplate.DefaultLanguage, "Please visit the following URL to remove yourself from the invitation list for this survey: ", new string[] { TextManager.DefaultLanguage }) + " " + invitation.Template.OptOutURL;
            }
            invitation.Save(creatorPrincipal);
            return invitation;
        }

        public static void DeleteAllInvitationEmailBatches(int invitationId)
        {
            foreach (long num in ListInvitationBatchIds(invitationId))
            {
                EmailGateway.DeleteMessageBatch(num);
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_RemoveMessage");
            storedProcCommandWrapper.AddInParameter("InvitationId", DbType.Int32, invitationId);
            storedProcCommandWrapper.AddInParameter("RecipientId", DbType.Int64, DBNull.Value);
            storedProcCommandWrapper.AddInParameter("MessageId", DbType.Int64, DBNull.Value);
            storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_RemoveBatch");
            storedProcCommandWrapper.AddInParameter("BatchId", DbType.Int64, DBNull.Value);
            storedProcCommandWrapper.AddInParameter("InvitationId", DbType.Int32, invitationId);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public static void DeleteInvitation(Invitation invitation)
        {
            if (invitation.ID.HasValue)
            {
                DeleteInvitation(invitation.ID.Value);
            }
        }

        public static void DeleteInvitation(int invitationID)
        {
            DeleteAllInvitationEmailBatches(invitationID);
            Database database = DatabaseFactory.CreateDatabase();
            using (IDbConnection connection = database.GetConnection())
            {
                connection.Open();
                IDbTransaction transaction = connection.BeginTransaction();
                try
                {
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_Delete");
                    storedProcCommandWrapper.AddInParameter("InvitationID", DbType.Int32, invitationID);
                    database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public static void DeleteInvitationEmailBatch(long batchId)
        {
            EmailGateway.DeleteMessageBatch(batchId);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_RemoveBatch");
            storedProcCommandWrapper.AddInParameter("BatchId", DbType.Int64, batchId);
            storedProcCommandWrapper.AddInParameter("InvitationId", DbType.Int32, DBNull.Value);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public static Invitation GetInvitation(int id)
        {
            Invitation invitation = new Invitation(id);
            try
            {
                if (invitation.Load())
                {
                    return invitation;
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("Could not get Invitation from database", exception);
            }
            return null;
        }

        public static Invitation GetInvitationForRecipient(Guid recipientGuid)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_GetIdFromRecipientGuid");
            storedProcCommandWrapper.AddInParameter("RecipientGuid", DbType.Guid, recipientGuid);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return GetInvitation((int) reader["InvitationID"]);
                    }
                }
                catch
                {
                }
                finally
                {
                    reader.Close();
                }
            }
            return null;
        }

        public static Invitation GetInvitationForRecipient(long recipientID)
        {
            int? invitationIdForRecipient = GetInvitationIdForRecipient(recipientID);
            if (invitationIdForRecipient.HasValue)
            {
                Invitation invitation = new Invitation(invitationIdForRecipient.Value);
                try
                {
                    if (invitation.Load())
                    {
                        return invitation;
                    }
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }

        public static Invitation GetInvitationForRecipient(string partialRecipientGuid)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_GetIdFromPartialRecipientGuid");
            storedProcCommandWrapper.AddInParameter("PartialRecipientGuid", DbType.String, partialRecipientGuid);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return GetInvitation((int) reader["InvitationID"]);
                    }
                }
                catch
                {
                }
                finally
                {
                    reader.Close();
                }
            }
            return null;
        }

        public static int? GetInvitationIdForRecipient(long recipientID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_GetID");
            storedProcCommandWrapper.AddInParameter("RecipientID", DbType.Int64, recipientID);
            int? nullable = null;
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    try
                    {
                        if (reader.Read())
                        {
                            nullable = new int?((int) reader["InvitationID"]);
                        }
                    }
                    catch
                    {
                    }
                    return nullable;
                }
                finally
                {
                    reader.Close();
                }
            }
            return nullable;
        }

        public static DataTable GetInvitations(ExtendedPrincipal principal, int pageNumber, int resultsPerPage, string filterField, string filterText, string sortField, bool sortAscending, out int invitationCount)
        {
            SelectQuery availableInvitations = InvitationQueryFactory.GetAvailableInvitations(filterField, filterText);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(availableInvitations.ToString());
            DataSet set = database.ExecuteDataSet(sqlStringCommandWrapper);
            DataTable table = set.Tables[0].Clone();
            table.TableName = "Invitations";
            int num = ((pageNumber - 1) * resultsPerPage) + 1;
            int num2 = (num + resultsPerPage) - 1;
            int num3 = 0;
            IAuthorizationProvider authorizationProvider = AuthorizationFactory.GetAuthorizationProvider();
            string sort = string.Empty;
            if (Utilities.IsNotNullOrEmpty(sortField))
            {
                sort = sortField + (sortAscending ? " ASC" : " DESC");
            }
            foreach (DataRow row in set.Tables[0].Select(null, sort, DataViewRowState.CurrentRows))
            {
                int templateID = (int) row["ResponseTemplateID"];
                LightweightAccessControllable lightweightResponseTemplate = ResponseTemplateManager.GetLightweightResponseTemplate(templateID);
                if ((lightweightResponseTemplate != null) && authorizationProvider.Authorize(principal, lightweightResponseTemplate, "Form.Administer"))
                {
                    num3++;
                    if (((pageNumber <= 0) || (resultsPerPage <= 0)) || ((num3 >= num) && (num3 <= num2)))
                    {
                        table.ImportRow(row);
                    }
                }
            }
            invitationCount = num3;
            return table;
        }

        public static DataTable GetInvitationsByTemplateId(ExtendedPrincipal principal, int templateId, int pageNumber, int resultsPerPage, string filterField, string filterText, string sortField, bool sortAscending, out int invitationCount)
        {
            SelectQuery query = InvitationQueryFactory.GetAvailableInvitationsByTemplateId(templateId, filterField, filterText);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            DataSet set = database.ExecuteDataSet(sqlStringCommandWrapper);
            DataTable table = set.Tables[0].Clone();
            table.TableName = "Invitations";
            int num = ((pageNumber - 1) * resultsPerPage) + 1;
            int num2 = (num + resultsPerPage) - 1;
            int num3 = 0;
            string sort = string.Empty;
            if (Utilities.IsNotNullOrEmpty(sortField))
            {
                sort = sortField + (sortAscending ? " ASC" : " DESC");
            }
            foreach (DataRow row in set.Tables[0].Select(null, sort, DataViewRowState.CurrentRows))
            {
                num3++;
                if (((pageNumber <= 0) || (resultsPerPage <= 0)) || ((num3 >= num) && (num3 <= num2)))
                {
                    table.ImportRow(row);
                }
            }
            invitationCount = num3;
            return table;
        }

        public static string GetRecipientEmail(Guid recipientGuid)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_GetRecipientEmail");
            storedProcCommandWrapper.AddInParameter("RecipientGuid", DbType.Guid, recipientGuid);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return DbUtility.GetValueFromDataReader<string>(reader, "EmailAddress", null);
                    }
                }
                catch
                {
                }
                finally
                {
                    reader.Close();
                }
            }
            return null;
        }

        public static Guid? GetRecipientGuid(long recipientId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_GetRecipientGuid");
            storedProcCommandWrapper.AddInParameter("RecipientID", DbType.Int64, recipientId);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return DbUtility.GetValueFromDataReader<Guid?>(reader, "Guid", null);
                    }
                }
                catch
                {
                }
                finally
                {
                    reader.Close();
                }
            }
            return null;
        }

        public static long? GetRecipientId(Guid recipientGuid)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_GetRecipientId");
            storedProcCommandWrapper.AddInParameter("RecipientGuid", DbType.Guid, recipientGuid);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return DbUtility.GetValueFromDataReader<long?>(reader, "RecipientId", null);
                    }
                }
                catch
                {
                }
                finally
                {
                    reader.Close();
                }
            }
            return null;
        }

        public static int GetRecipientResponseCount(Guid recipientGuid)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_GetRecipientResponseCount");
            storedProcCommandWrapper.AddInParameter("RecipientGuid", DbType.Guid, recipientGuid);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return DbUtility.GetValueFromDataReader<int>(reader, "ResponseCount", 0);
                    }
                }
                catch
                {
                }
                finally
                {
                    reader.Close();
                }
            }
            return 0;
        }

        public static string GetRecipientUniqueIdentifier(Guid recipientGuid)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_GetRecipientUniqueIdentifier");
            storedProcCommandWrapper.AddInParameter("RecipientGuid", DbType.Guid, recipientGuid);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return DbUtility.GetValueFromDataReader<string>(reader, "UniqueIdentifier", null);
                    }
                }
                catch
                {
                }
                finally
                {
                    reader.Close();
                }
            }
            return null;
        }

        public static Guid? GetResponseTemplateGuidForInvitation(Guid recipientGuid)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_GetResponseTemplateGuid");
            storedProcCommandWrapper.AddInParameter("RecipientGuid", DbType.Guid, recipientGuid);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        return DbUtility.GetValueFromDataReader<Guid?>(reader, "Guid", null);
                    }
                }
                catch
                {
                }
                finally
                {
                    reader.Close();
                }
            }
            return null;
        }

        public static List<long> ListInvitationBatchIds(int invitationId)
        {
            List<long> list = new List<long>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_ListBatches");
            storedProcCommandWrapper.AddInParameter("InvitationId", DbType.Int32, invitationId);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        long? defaultValue = null;
                        long? nullable = DbUtility.GetValueFromDataReader<long?>(reader, "BatchId", defaultValue);
                        if (nullable.HasValue)
                        {
                            list.Add(nullable.Value);
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }

        public static List<long> ListRecipientQueueMessages(long recipientId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_ListRecipientMessages");
            storedProcCommandWrapper.AddInParameter("RecipientId", DbType.Int64, recipientId);
            List<long> list = new List<long>();
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        if (reader["MessageId"] != DBNull.Value)
                        {
                            list.Add((long) reader["MessageId"]);
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }

        public static void OptOutRecipient(Guid recipientGuid)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_OptOut");
            storedProcCommandWrapper.AddInParameter("RecipientGuid", DbType.Guid, recipientGuid);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public static void OptOutRecipient(long recipientId)
        {
            Guid? recipientGuid = GetRecipientGuid(recipientId);
            if (recipientGuid.HasValue)
            {
                OptOutRecipient(recipientGuid.Value);
            }
        }

        public static void RemoveRecipientMessagesFromEmailQueue(long recipientId, IDbTransaction transaction)
        {
            foreach (long num in ListRecipientQueueMessages(recipientId))
            {
                EmailGateway.DeleteMessage(num);
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_RemoveMessage");
                storedProcCommandWrapper.AddInParameter("InvitationId", DbType.Int32, DBNull.Value);
                storedProcCommandWrapper.AddInParameter("RecipientId", DbType.Int64, recipientId);
                storedProcCommandWrapper.AddInParameter("MessageId", DbType.Int64, DBNull.Value);
                if (transaction != null)
                {
                    database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                }
                else
                {
                    database.ExecuteNonQuery(storedProcCommandWrapper);
                }
            }
        }

        public static void UpdateInvitationSentDate(int invitationId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Invitation_UpdateLastSent");
            storedProcCommandWrapper.AddInParameter("InvitationId", DbType.Int32, invitationId);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }
    }
}

