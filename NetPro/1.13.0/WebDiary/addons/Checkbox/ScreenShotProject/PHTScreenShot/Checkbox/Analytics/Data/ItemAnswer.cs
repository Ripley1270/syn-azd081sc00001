﻿namespace Checkbox.Analytics.Data
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ItemAnswer : IEquatable<ItemAnswer>
    {
        public bool Equals(ItemAnswer other)
        {
            return (other.AnswerId == this.AnswerId);
        }

        public long AnswerId { get; set; }

        public string AnswerText { get; set; }

        public bool IsOther { get; set; }

        public int ItemId { get; set; }

        public int? OptionId { get; set; }

        public Guid? ResponseGuid { get; set; }

        public long ResponseId { get; set; }
    }
}

