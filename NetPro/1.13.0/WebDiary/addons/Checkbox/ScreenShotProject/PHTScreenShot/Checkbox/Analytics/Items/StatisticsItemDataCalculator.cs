﻿using Checkbox.Analytics.Computation;
using Checkbox.Common;
using Checkbox.Globalization.Text;
using System;
using System.Globalization;
using System.Collections.Generic;
using System.Data;

namespace Checkbox.Analytics.Items
{
    //using Checkbox.Analytics.Computation;
    //using Checkbox.Common;
    //using System;
    //using System.Collections.Generic;
    //using System.Data;

    [Serializable]
    public class StatisticsItemDataCalculator : ReportDataCalculator
    {
        private double? _maxPossibleAnswerValue;
        private double? _minPossibleAnswerValue;
        private readonly StatisticsItemReportingOption _reportOption;

        public StatisticsItemDataCalculator(StatisticsItemReportingOption reportOption)
        {
            this._reportOption = reportOption;
        }

        protected override DataTable Calculate(ItemAnswerAggregator answerAggregator)
        {
            DataTable table = this.CreateOutputTable();
            foreach (int num in answerAggregator.GetItemIDs())
            {
                StatisticsItemResult itemResult = this.GetItemResult(num, answerAggregator) as StatisticsItemResult;
                if (itemResult != null)
                {
                    DataRow row = table.NewRow();
                    row["ItemId"] = num;
                    row["ItemText"] = answerAggregator.GetItemText(num);
                    row["Responses"] = itemResult.Response;
                    row["Mean"] = itemResult.Mean;
                    row["Median"] = itemResult.Median;
                    row["Mode"] = itemResult.Mode;
                    row["StdDev"] = itemResult.StandardDeviation;
                    table.Rows.Add(row);
                }
            }
            if (!this._minPossibleAnswerValue.HasValue)
            {
                this._minPossibleAnswerValue = 1.0;
            }
            if (!this._maxPossibleAnswerValue.HasValue)
            {
                this._maxPossibleAnswerValue = 10.0;
            }
            return table;
        }

        private static double ComputeMean(List<double> answerValues)
        {
            double num = 0.0;
            foreach (double num2 in answerValues)
            {
                num += num2;
            }
            return (num / ((double) answerValues.Count));
        }

        private static double ComputeMedian(List<double> answerValues)
        {
            List<double> list = new List<double>();
            foreach (double num in answerValues)
            {
                if (!list.Contains(num))
                {
                    list.Add(num);
                }
            }
            list.Sort();
            int num2 = list.Count / 2;
            if ((list.Count % 2) == 0)
            {
                return ((list[num2] + list[num2 - 1]) / 2.0);
            }
            return list[num2];
        }

        private static double ComputeMode(List<double> answerValues)
        {
            double num = 0.0;
            int num2 = 0;
            int num3 = 0;
            for (int i = 0; i < answerValues.Count; i++)
            {
                if (i != (answerValues.Count - 1))
                {
                    if (answerValues[i] == answerValues[i + 1])
                    {
                        num2++;
                    }
                    else
                    {
                        if (++num2 >= num3)
                        {
                            num3 = num2;
                            num = answerValues[i];
                        }
                        num2 = 0;
                    }
                }
                else
                {
                    if (++num2 >= num3)
                    {
                        num3 = num2;
                        num = answerValues[i];
                    }
                    num2 = 0;
                }
            }
            return num;
        }

        private static double ComputeStandardDeviation(List<double> answerValues, double averageValue)
        {
            double num = 0.0;
            foreach (double num2 in answerValues)
            {
                num += Math.Pow(num2 - averageValue, 2.0);
            }
            return Math.Sqrt(num / ((double) answerValues.Count));
        }

        protected override DataTable CreateOutputTable()
        {
            DataTable table = base.CreateOutputTable();
            if (table != null)
            {
                table.Columns.Add("Responses", typeof(double));
                table.Columns.Add("Mean", typeof(double));
                table.Columns.Add("Median", typeof(double));
                table.Columns.Add("Mode", typeof(double));
                table.Columns.Add("StdDev", typeof(double));
            }
            return table;
        }

        private List<double> GetAnswerValues(int itemId, ItemAnswerAggregator answerAggregator)
        {
            int itemAnswerCount = answerAggregator.GetItemAnswerCount(itemId);
            if (itemAnswerCount == 0)
            {
                return new List<double>();
            }
            List<double> list = new List<double>(itemAnswerCount);
            string itemType = answerAggregator.GetItemType(itemId);
            List<int> optionIDs = answerAggregator.GetOptionIDs(itemId);
            if (optionIDs.Count == 0)
            {
                foreach (string str2 in answerAggregator.GetAnswerTexts(itemId))
                {
                    double? nullable = Utilities.GetDouble(str2, new CultureInfo[0]);
                    if (!nullable.HasValue)
                    {
                        continue;
                    }
                    list.Add(nullable.Value);
                    if (this._minPossibleAnswerValue.HasValue)
                    {
                        double? nullable3 = nullable;
                        double minPossibleAnswerValue = this.MinPossibleAnswerValue;
                        if (!((nullable3.GetValueOrDefault() < minPossibleAnswerValue) && nullable3.HasValue))
                        {
                            goto Label_00BE;
                        }
                    }
                    this._minPossibleAnswerValue = new double?(nullable.Value);
                Label_00BE:
                    if (this._maxPossibleAnswerValue.HasValue)
                    {
                        double? nullable4 = nullable;
                        double maxPossibleAnswerValue = this.MaxPossibleAnswerValue;
                        if (!((nullable4.GetValueOrDefault() > maxPossibleAnswerValue) && nullable4.HasValue))
                        {
                            continue;
                        }
                    }
                    this._maxPossibleAnswerValue = new double?(nullable.Value);
                }
            }
            else
            {
                foreach (int num2 in optionIDs)
                {
                    bool optionIsOther = answerAggregator.GetOptionIsOther(num2);
                    if (itemType.Equals("RadioButtonScale", StringComparison.InvariantCultureIgnoreCase) && optionIsOther)
                    {
                        continue;
                    }
                    int count = this.GetOptionResult(num2, answerAggregator).Count;
                    answerAggregator.GetOptionText(num2);
                    double? nullable2 = new double?(answerAggregator.GetOptionPoints(num2));
                    if (!nullable2.HasValue)
                    {
                        continue;
                    }
                    for (int i = 0; i < count; i++)
                    {
                        list.Add(nullable2.Value);
                    }
                    if (this._minPossibleAnswerValue.HasValue)
                    {
                        double? nullable5 = nullable2;
                        double num7 = this.MinPossibleAnswerValue;
                        if (!((nullable5.GetValueOrDefault() < num7) && nullable5.HasValue))
                        {
                            goto Label_01EA;
                        }
                    }
                    this._minPossibleAnswerValue = new double?(nullable2.Value);
                Label_01EA:
                    if (this._maxPossibleAnswerValue.HasValue)
                    {
                        double? nullable6 = nullable2;
                        double num8 = this.MaxPossibleAnswerValue;
                        if (!((nullable6.GetValueOrDefault() > num8) && nullable6.HasValue))
                        {
                            continue;
                        }
                    }
                    this._maxPossibleAnswerValue = new double?(nullable2.Value);
                }
            }
            list.TrimExcess();
            return list;
        }

        private int GetItemAnswerCountWithoutNA(int itemId, ItemAnswerAggregator answerAggregator)
        {
            int num = 0;
            foreach (long num2 in answerAggregator.GetResponseIDs())
            {
                List<long> list2 = answerAggregator.ResponseAnswerDictionary[num2];
                foreach (long num3 in list2)
                {
                    if (answerAggregator.AnswerDictionary[num3].ItemId == itemId)
                    {
                        bool optionIsOther = false;
                        int? optionId = answerAggregator.AnswerDictionary[num3].OptionId;
                        if (optionId.HasValue)
                        {
                            optionIsOther = answerAggregator.GetOptionIsOther(optionId.Value);
                        }
                        if (!optionIsOther)
                        {
                            num++;
                        }
                    }
                }
            }
            return num;
        }

        protected override ItemResult GetItemResult(int itemId, ItemAnswerAggregator answerAggregator)
        {
            StatisticsItemResult result = new StatisticsItemResult();
            List<double> answerValues = this.GetAnswerValues(itemId, answerAggregator);
            if (answerValues.Count != 0)
            {
                switch (this._reportOption)
                {
                    case StatisticsItemReportingOption.Responses:
                        result.Response = this.GetItemAnswerCountWithoutNA(itemId, answerAggregator);
                        return result;

                    case StatisticsItemReportingOption.Mean:
                        result.Mean = ComputeMean(answerValues);
                        return result;

                    case StatisticsItemReportingOption.Median:
                        result.Median = ComputeMedian(answerValues);
                        return result;

                    case StatisticsItemReportingOption.Mode:
                        result.Mode = ComputeMode(answerValues);
                        return result;

                    case StatisticsItemReportingOption.StdDeviation:
                        result.StandardDeviation = ComputeStandardDeviation(answerValues, result.Mean);
                        return result;

                    case StatisticsItemReportingOption.All:
                        result.Response = this.GetItemAnswerCountWithoutNA(itemId, answerAggregator);
                        result.Mean = ComputeMean(answerValues);
                        result.Median = ComputeMedian(answerValues);
                        result.Mode = ComputeMode(answerValues);
                        result.StandardDeviation = ComputeStandardDeviation(answerValues, result.Mean);
                        return result;
                }
                result.Response = this.GetItemAnswerCountWithoutNA(itemId, answerAggregator);
            }
            return result;
        }

        public double MaxPossibleAnswerValue
        {
            get
            {
                return this._maxPossibleAnswerValue.Value;
            }
        }

        public double MinPossibleAnswerValue
        {
            get
            {
                return this._minPossibleAnswerValue.Value;
            }
        }
    }
}

