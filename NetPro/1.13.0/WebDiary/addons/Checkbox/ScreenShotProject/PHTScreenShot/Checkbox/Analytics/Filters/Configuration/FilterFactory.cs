﻿namespace Checkbox.Analytics.Filters.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms.Logic;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public static class FilterFactory
    {
        private static Dictionary<string, Type> _typeCache;

        public static FilterData CreateFilterData(int filterId)
        {
            string filterTypeName = GetFilterTypeName(filterId);
            if (string.IsNullOrEmpty(filterTypeName))
            {
                return null;
            }
            FilterData data = CreateFilterData(filterTypeName);
            if (data == null)
            {
                return null;
            }
            data.ID = new int?(filterId);
            return data;
        }

        public static FilterData CreateFilterData(string filterTypeName)
        {
            Type filterType = GetFilterType(filterTypeName);
            if (filterType == null)
            {
                throw new Exception("Unable to locate type information for filter type: " + filterTypeName);
            }
            if (!typeof(FilterData).IsAssignableFrom(filterType))
            {
                throw new Exception("Filter type data class for [" + filterTypeName + "] does not extend FilterData class.");
            }
            FilterData data = (FilterData) Activator.CreateInstance(filterType);
            if (data != null)
            {
                data.Initialize(filterTypeName);
            }
            return data;
        }

        public static FilterData CreateFilterData(string filterTypeName, LogicalOperator op, object value)
        {
            FilterData data = CreateFilterData(filterTypeName);
            if (data != null)
            {
                data.Operator = op;
                data.Value = value;
            }
            return data;
        }

        public static FilterData GetFilterData(int filterId)
        {
            string filterTypeName = GetFilterTypeName(filterId);
            if (Utilities.IsNotNullOrEmpty(filterTypeName))
            {
                FilterData data = CreateFilterData(filterTypeName);
                data.Load(filterId);
                return data;
            }
            return null;
        }

        private static Type GetFilterType(string filterTypeName)
        {
            if (TypeCache.ContainsKey(filterTypeName))
            {
                return TypeCache[filterTypeName];
            }
            Type type = null;
            string typeAQN = GetTypeAQN(filterTypeName);
            if (Utilities.IsNotNullOrEmpty(typeAQN))
            {
                type = Type.GetType(typeAQN);
                if (type != null)
                {
                    TypeCache[filterTypeName] = type;
                }
            }
            return type;
        }

        private static string GetFilterTypeName(int filterId)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Filter_GetTypeName");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, filterId);
            string str = null;
            IDataReader reader = database.ExecuteReader(storedProcCommandWrapper);
            try
            {
                if (reader.Read())
                {
                    str = DbUtility.GetValueFromDataReader<string>(reader, "FilterTypeName", null);
                }
            }
            catch
            {
                reader.Close();
                throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Dispose();
                }
            }
            return str;
        }

        private static string GetTypeAQN(string filterTypeName)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Filter_GetTypeInfo");
            storedProcCommandWrapper.AddInParameter("FilterTypeName", DbType.String, filterTypeName);
            string str = null;
            IDataReader reader = database.ExecuteReader(storedProcCommandWrapper);
            try
            {
                if (reader.Read())
                {
                    string str2 = DbUtility.GetValueFromDataReader<string>(reader, "DataTypeAssemblyName", null);
                    string str3 = DbUtility.GetValueFromDataReader<string>(reader, "DataTypeClassName", null);
                    if (Utilities.IsNotNullOrEmpty(str2) && Utilities.IsNotNullOrEmpty(str3))
                    {
                        str = str3 + "," + str2;
                    }
                }
            }
            catch
            {
                reader.Close();
                throw;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Dispose();
                }
            }
            return str;
        }

        private static Dictionary<string, Type> TypeCache
        {
            get
            {
                if (_typeCache == null)
                {
                    _typeCache = new Dictionary<string, Type>();
                }
                return _typeCache;
            }
        }
    }
}

