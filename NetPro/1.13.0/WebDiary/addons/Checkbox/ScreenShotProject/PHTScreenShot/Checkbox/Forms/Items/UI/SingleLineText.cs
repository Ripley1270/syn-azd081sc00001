﻿namespace Checkbox.Forms.Items.UI
{
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class SingleLineText : LabelledItemAppearanceData
    {
        protected override void Create(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_CreateSLText");
            storedProcCommandWrapper.AddInParameter("AppearanceCode", DbType.String, this.AppearanceCode);
            storedProcCommandWrapper.AddInParameter("Width", DbType.Int32, this.Width);
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
            storedProcCommandWrapper.AddInParameter("LabelPosition", DbType.String, this.LabelPosition.ToString());
            storedProcCommandWrapper.AddOutParameter("AppearanceID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("AppearanceID");
            if ((parameterValue == null) || (parameterValue == DBNull.Value))
            {
                throw new Exception("Unable to save appearance data.");
            }
            base.ID = new int?((int) parameterValue);
        }

        protected override void Update(IDbTransaction transaction)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Appearance_UpdateSLText");
            storedProcCommandWrapper.AddInParameter("AppearanceID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("Width", DbType.Int32, this.Width);
            storedProcCommandWrapper.AddInParameter("ItemPosition", DbType.String, base.ItemPosition);
            storedProcCommandWrapper.AddInParameter("LabelPosition", DbType.String, this.LabelPosition.ToString());
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public override string AppearanceCode
        {
            get
            {
                return "SINGLE_LINE_TEXT";
            }
        }
    }
}

