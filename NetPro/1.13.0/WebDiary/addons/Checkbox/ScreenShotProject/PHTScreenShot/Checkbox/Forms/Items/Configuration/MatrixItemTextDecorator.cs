﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Common;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class MatrixItemTextDecorator : LabelledItemTextDecorator
    {
        private readonly Dictionary<int, string> _columnDefaultTexts;
        private readonly Dictionary<int, bool> _columnDefaultTextsModified;
        private readonly Dictionary<int, bool> _columnTextModified;
        private readonly Dictionary<int, string> _columnTexts;
        private readonly Dictionary<string, string> _optionTexts;
        private readonly Dictionary<string, bool> _optionTextsModified;
        private readonly Dictionary<int, bool> _rowTextModified;
        private readonly Dictionary<int, string> _rowTexts;
        private readonly Dictionary<string, string> _scaleTexts;
        private readonly Dictionary<string, bool> _scaleTextsModified;

        public MatrixItemTextDecorator(MatrixItemData itemData, string language) : base(itemData, language)
        {
            this._columnTextModified = new Dictionary<int, bool>();
            this._rowTextModified = new Dictionary<int, bool>();
            this._columnTexts = new Dictionary<int, string>();
            this._rowTexts = new Dictionary<int, string>();
            this._optionTexts = new Dictionary<string, string>();
            this._optionTextsModified = new Dictionary<string, bool>();
            this._scaleTextsModified = new Dictionary<string, bool>();
            this._scaleTexts = new Dictionary<string, string>();
            this._columnDefaultTexts = new Dictionary<int, string>();
            this._columnDefaultTextsModified = new Dictionary<int, bool>();
        }

        public string GetColumnDefaultText(int column)
        {
            if (column != this.Data.PrimaryKeyColumnIndex)
            {
                if (this._columnDefaultTextsModified.ContainsKey(column) && this._columnDefaultTextsModified[column])
                {
                    return this._columnDefaultTexts[column];
                }
                ItemData columnPrototype = this.Data.GetColumnPrototype(column);
                if ((columnPrototype != null) && (columnPrototype is TextItemData))
                {
                    string text = this.GetText(((TextItemData) columnPrototype).DefaultTextID);
                    this._columnDefaultTexts[column] = text;
                    return text;
                }
            }
            return string.Empty;
        }

        public string GetColumnText(int column)
        {
            if (column != this.Data.PrimaryKeyColumnIndex)
            {
                if (this._columnTextModified.ContainsKey(column) && this._columnTextModified[column])
                {
                    return this._columnTexts[column];
                }
                ItemData columnPrototype = this.Data.GetColumnPrototype(column);
                if ((columnPrototype != null) && (columnPrototype is LabelledItemData))
                {
                    string text = this.GetText(((LabelledItemData) columnPrototype).TextID);
                    this._columnTexts[column] = text;
                    return text;
                }
            }
            return string.Empty;
        }

        public string GetOptionText(int column, int position)
        {
            string key = column + "_" + position;
            if (this._optionTextsModified.ContainsKey(key) && this._optionTextsModified[key])
            {
                return this._optionTexts[key];
            }
            ItemData columnPrototype = this.Data.GetColumnPrototype(column);
            if (((columnPrototype != null) && (columnPrototype is SelectItemData)) && (((SelectItemData) columnPrototype).ListData != null))
            {
                ListOptionData optionAt = ((SelectItemData) columnPrototype).ListData.GetOptionAt(position);
                if (optionAt != null)
                {
                    string text;
                    if (columnPrototype is RatingScaleItemData)
                    {
                        text = optionAt.IsOther ? this.GetText(((RatingScaleItemData) columnPrototype).NotApplicableTextID) : optionAt.Points.ToString();
                        if (Utilities.IsNullOrEmpty(text))
                        {
                            text = "n/a";
                        }
                    }
                    else
                    {
                        text = this.GetText(optionAt.TextID);
                    }
                    this._optionTexts[key] = text;
                    return text;
                }
            }
            return string.Empty;
        }

        public List<string> GetOptionTexts(int column)
        {
            string str = column + "_";
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            ItemData columnPrototype = this.Data.GetColumnPrototype(column);
            if ((columnPrototype != null) && (columnPrototype is SelectItemData))
            {
                foreach (ListOptionData data2 in ((SelectItemData) columnPrototype).Options)
                {
                    dictionary[str + data2.Position] = data2.TextID;
                }
            }
            List<string> list = new List<string>();
            foreach (string str2 in dictionary.Keys)
            {
                if (this._optionTextsModified.ContainsKey(str2) && this._optionTextsModified[str2])
                {
                    list.Add(this._optionTexts[str2]);
                }
                else
                {
                    string text = this.GetText(dictionary[str2]);
                    this._optionTexts[str2] = text;
                    list.Add(text);
                }
            }
            return list;
        }

        public string GetRowText(int row)
        {
            if (this._rowTextModified.ContainsKey(row) && this._rowTextModified[row])
            {
                return this._rowTexts[row];
            }
            int? itemIDAt = this.Data.GetItemIDAt(row, this.Data.PrimaryKeyColumnIndex);
            if (!itemIDAt.HasValue)
            {
                return string.Empty;
            }
            if (this.Data.IsRowOther(row))
            {
                SingleLineTextItemData data = new SingleLineTextItemData {
                    ID = itemIDAt
                };
                string str = this.GetText(data.DefaultTextID);
                this._rowTexts[row] = str;
                return str;
            }
            MessageItemData data3 = new MessageItemData {
                ID = itemIDAt
            };
            string text = this.GetText(data3.TextID);
            this._rowTexts[row] = text;
            return text;
        }

        public string GetScaleText(int column, string position)
        {
            string key = column + "_" + position;
            if (this._scaleTextsModified.ContainsKey(key))
            {
                return this._scaleTexts[key];
            }
            ItemData columnPrototype = this.Data.GetColumnPrototype(column);
            if ((columnPrototype == null) || !(columnPrototype is RatingScaleItemData))
            {
                return string.Empty;
            }
            string text = string.Empty;
            if (position.ToLower() == "start")
            {
                text = this.GetText(((RatingScaleItemData) columnPrototype).StartTextID);
            }
            else if (position.ToLower() == "middle")
            {
                text = this.GetText(((RatingScaleItemData) columnPrototype).MidTextID);
            }
            else if (position.ToLower() == "end")
            {
                text = this.GetText(((RatingScaleItemData) columnPrototype).EndTextID);
            }
            else if (position.ToLower() == "na")
            {
                text = this.GetText(((RatingScaleItemData) columnPrototype).NotApplicableTextID);
            }
            this._scaleTexts[key] = text;
            return text;
        }

        public void MoveColumnLeft(int column)
        {
            if (column > 1)
            {
                this.Data.MoveColumn(column, column - 1);
                this.SwapColumnTextValues(column, column - 1);
            }
        }

        public void MoveColumnRight(int column)
        {
            if (column < this.Data.ColumnCount)
            {
                this.Data.MoveColumn(column, column + 1);
                this.SwapColumnTextValues(column, column + 1);
            }
        }

        public void MoveRowDown(int row)
        {
            if (row < this.Data.RowCount)
            {
                this.Data.MoveRow(row, row + 1);
                this.SwapRowTextValues(row, row + 1);
            }
        }

        public void MoveRowUp(int row)
        {
            if (row > 1)
            {
                this.Data.MoveRow(row, row - 1);
                this.SwapRowTextValues(row, row - 1);
            }
        }

        public void RemoveColumn(int column)
        {
            if (column > 0)
            {
                for (int i = column; i <= this.Data.ColumnCount; i++)
                {
                    this.SwapColumnTextValues(i, i + 1);
                }
                if (this._columnTextModified.ContainsKey(this.Data.ColumnCount))
                {
                    this._columnTextModified.Remove(this.Data.ColumnCount);
                }
                if (this._columnTexts.ContainsKey(this.Data.ColumnCount))
                {
                    this._columnTexts.Remove(this.Data.ColumnCount);
                }
                if (this._columnDefaultTextsModified.ContainsKey(this.Data.ColumnCount))
                {
                    this._columnDefaultTextsModified.Remove(this.Data.ColumnCount);
                }
                if (this._columnDefaultTexts.ContainsKey(this.Data.ColumnCount))
                {
                    this._columnDefaultTexts.Remove(this.Data.ColumnCount);
                }
                this.Data.RemoveColumn(column);
            }
        }

        public void RemoveRow(int row)
        {
            if (row > 0)
            {
                this.Data.RemoveRow(row);
                for (int i = row; i <= this.Data.RowCount; i++)
                {
                    this.SwapRowTextValues(i, i + 1);
                }
                if (this._rowTextModified.ContainsKey(this.Data.RowCount + 1))
                {
                    this._rowTextModified.Remove(this.Data.RowCount + 1);
                }
                if (this._rowTexts.ContainsKey(this.Data.RowCount + 1))
                {
                    this._rowTexts.Remove(this.Data.RowCount + 1);
                }
            }
        }

        public void SetColumnDefaultText(int column, string text)
        {
            this._columnDefaultTexts[column] = text;
            this._columnDefaultTextsModified[column] = true;
        }

        public void SetColumnText(int column, string text)
        {
            this._columnTexts[column] = text;
            this._columnTextModified[column] = true;
        }

        protected virtual void SetItemTexts(ItemData itemData, int column)
        {
            if (itemData is LabelledItemData)
            {
                this.SetText(((LabelledItemData) itemData).TextID, this.GetColumnText(column));
            }
            if (itemData is RatingScaleItemData)
            {
                this.SetText(((RatingScaleItemData) itemData).StartTextID, this.GetScaleText(column, "start"));
                this.SetText(((RatingScaleItemData) itemData).MidTextID, this.GetScaleText(column, "middle"));
                this.SetText(((RatingScaleItemData) itemData).EndTextID, this.GetScaleText(column, "end"));
                this.SetText(((RatingScaleItemData) itemData).NotApplicableTextID, this.GetScaleText(column, "na"));
            }
            if (itemData is TextItemData)
            {
                this.SetText(((TextItemData) itemData).DefaultTextID, this.GetColumnDefaultText(column));
            }
            if (itemData is SelectItemData)
            {
                foreach (ListOptionData data in ((SelectItemData) itemData).Options)
                {
                    this.SetText(data.TextID, this.GetOptionText(column, data.Position));
                }
            }
        }

        protected virtual void SetItemTexts(int row, int column)
        {
            ItemData itemAt = this.Data.GetItemAt(row, column);
            if (itemAt != null)
            {
                this.SetItemTexts(itemAt, column);
            }
        }

        protected override void SetLocalizedTexts()
        {
            base.SetLocalizedTexts();
            for (int i = 1; i <= this.Data.ColumnCount; i++)
            {
                ItemData columnPrototype = this.Data.GetColumnPrototype(i);
                if (columnPrototype != null)
                {
                    this.SetItemTexts(columnPrototype, i);
                }
                for (int j = 1; j <= this.Data.RowCount; j++)
                {
                    int? itemIDAt = this.Data.GetItemIDAt(j, i);
                    if (itemIDAt.HasValue)
                    {
                        if (i == this.Data.PrimaryKeyColumnIndex)
                        {
                            if (this.Data.IsRowOther(j))
                            {
                                SingleLineTextItemData data2 = new SingleLineTextItemData {
                                    ID = itemIDAt
                                };
                                this.SetText(data2.DefaultTextID, this.GetRowText(j));
                            }
                            else
                            {
                                MessageItemData data4 = new MessageItemData {
                                    ID = itemIDAt
                                };
                                this.SetText(data4.TextID, this.GetRowText(j));
                            }
                        }
                        else
                        {
                            this.SetItemTexts(j, i);
                        }
                    }
                }
            }
        }

        public void SetOptionText(int column, int position, string text)
        {
            string str = column + "_" + position;
            this._optionTexts[str] = text;
            this._optionTextsModified[str] = true;
        }

        public void SetRowText(int row, string text)
        {
            this._rowTexts[row] = text;
            this._rowTextModified[row] = true;
        }

        public void SetScaleText(int column, string position, string text)
        {
            string str = column + "_" + position;
            this._scaleTexts[str] = text;
            this._scaleTextsModified[str] = true;
        }

        private void SwapColumnTextValues(int column1, int column2)
        {
            SwapTextValues<int>(column1, column2, this._columnTexts, this._columnTextModified);
            SwapTextValues<int>(column1, column2, this._columnDefaultTexts, this._columnDefaultTextsModified);
            this.SwapOptionTextValues(column1, column2);
            this.SwapRatingScaleTextValues(column1, column2);
        }

        private void SwapOptionTextValues(int column1, int column2)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            Dictionary<string, bool> dictionary2 = new Dictionary<string, bool>();
            foreach (string str in this._optionTexts.Keys)
            {
                if (str.StartsWith(column1 + "_"))
                {
                    dictionary[str] = this._optionTexts[str];
                    if (this._optionTextsModified.ContainsKey(str))
                    {
                        dictionary2[str] = this._optionTextsModified[str];
                    }
                }
            }
            foreach (string str2 in dictionary.Keys)
            {
                if (this._optionTexts.ContainsKey(str2))
                {
                    this._optionTexts.Remove(str2);
                }
                if (this._optionTextsModified.ContainsKey(str2))
                {
                    this._optionTextsModified.Remove(str2);
                }
            }
            Dictionary<string, string> dictionary3 = new Dictionary<string, string>();
            Dictionary<string, bool> dictionary4 = new Dictionary<string, bool>();
            foreach (string str3 in this._optionTexts.Keys)
            {
                if (str3.StartsWith(column2 + "_"))
                {
                    dictionary3[str3] = this._optionTexts[str3];
                    if (this._optionTextsModified.ContainsKey(str3))
                    {
                        dictionary4[str3] = this._optionTextsModified[str3];
                    }
                }
            }
            foreach (string str4 in dictionary3.Keys)
            {
                string str5 = str4.Replace(column2 + "_", column1 + "_");
                this._optionTexts[str5] = this._optionTexts[str4];
                if (this._optionTextsModified.ContainsKey(str4))
                {
                    this._optionTextsModified[str5] = this._optionTextsModified[str4];
                }
            }
            foreach (string str6 in dictionary3.Keys)
            {
                if (this._optionTexts.ContainsKey(str6))
                {
                    this._optionTexts.Remove(str6);
                }
                if (this._optionTextsModified.ContainsKey(str6))
                {
                    this._optionTextsModified.Remove(str6);
                }
            }
            foreach (string str7 in dictionary.Keys)
            {
                string str8 = str7.Replace(column1 + "_", column2 + "_");
                this._optionTexts[str8] = dictionary[str7];
                if (dictionary2.ContainsKey(str7))
                {
                    this._optionTextsModified[str8] = dictionary2[str7];
                }
            }
        }

        private void SwapRatingScaleTextValues(int column1, int column2)
        {
            string str = column1 + "_start";
            string str2 = column2 + "_start";
            SwapTextValues<string>(str, str2, this._scaleTexts, this._scaleTextsModified);
            str = column1 + "_middle";
            str2 = column2 + "_middle";
            SwapTextValues<string>(str, str2, this._scaleTexts, this._scaleTextsModified);
            str = column1 + "_end";
            str2 = column2 + "_end";
            SwapTextValues<string>(str, str2, this._scaleTexts, this._scaleTextsModified);
        }

        private void SwapRowTextValues(int row1, int row2)
        {
            SwapTextValues<int>(row1, row2, this._rowTexts, this._rowTextModified);
        }

        private static void SwapTextValues<T, U>(T key1, T key2, IDictionary<T, U> dict)
        {
            if (dict.ContainsKey(key1) && !dict.ContainsKey(key2))
            {
                dict[key2] = dict[key1];
                dict.Remove(key1);
            }
            else if (!dict.ContainsKey(key1) && dict.ContainsKey(key2))
            {
                dict[key1] = dict[key2];
                dict.Remove(key2);
            }
            else if (dict.ContainsKey(key1) && dict.ContainsKey(key2))
            {
                U local = dict[key1];
                dict[key1] = dict[key2];
                dict[key2] = local;
            }
        }

        private static void SwapTextValues<T>(T key1, T key2, IDictionary<T, string> textDict, IDictionary<T, bool> modDict)
        {
            SwapTextValues<T, string>(key1, key2, textDict);
            SwapTextValues<T, bool>(key1, key2, modDict);
        }

        public MatrixItemData Data
        {
            get
            {
                return (MatrixItemData) base.Data;
            }
        }
    }
}

