﻿namespace Checkbox.Security.Providers
{
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using System;
    using System.Xml;

    internal class DbAuthorizationProviderData : ProviderData, IXmlConfigurationBase
    {
        public DbAuthorizationProviderData(string providerName) : base(providerName, typeof(DbAuthorizationProvider).AssemblyQualifiedName)
        {
            ArgumentValidation.CheckForEmptyString(providerName, "providerName");
        }

        public void LoadFromXml(XmlNode providerData)
        {
        }
    }
}

