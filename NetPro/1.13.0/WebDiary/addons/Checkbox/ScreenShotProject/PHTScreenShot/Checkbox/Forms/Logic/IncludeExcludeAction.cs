﻿namespace Checkbox.Forms.Logic
{
    using Checkbox.Forms;
    using Checkbox.Forms.Items;
    using System;

    [Serializable]
    public class IncludeExcludeAction : Checkbox.Forms.Logic.Action
    {
        public IncludeExcludeAction()
        {
        }

        public IncludeExcludeAction(Item receiver) : base(receiver)
        {
        }

        public IncludeExcludeAction(ResponsePage receiver) : base(receiver)
        {
        }

        public override void Execute(bool directive)
        {
            if (base.Receiver != null)
            {
                if (base.Receiver is Item)
                {
                    ((Item) base.Receiver).Excluded = !directive;
                }
                else if (base.Receiver is ResponsePage)
                {
                    ((ResponsePage) base.Receiver).Excluded = !directive;
                }
            }
        }
    }
}

