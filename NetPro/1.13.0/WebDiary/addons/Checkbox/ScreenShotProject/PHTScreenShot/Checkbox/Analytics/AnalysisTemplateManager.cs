﻿namespace Checkbox.Analytics
{
    using Checkbox.Analytics.Data;
    using Checkbox.Analytics.Items.Configuration;
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Data;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Globalization.Text;
    using Checkbox.Progress;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.Sprocs;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Xml.Serialization;

    public static class AnalysisTemplateManager
    {
        private static void AddSourceItemToExportItem(ResponseTemplate rt, int itemId, AnalysisItemData exportItemData, bool isChild)
        {
            LightweightItemMetaData data = SurveyMetaDataProxy.GetItemData(itemId, isChild, rt);
            if (data != null)
            {
                if (data.IsAnswerable)
                {
                    exportItemData.AddSourceItem(itemId);
                }
                foreach (int num in data.Children)
                {
                    AddSourceItemToExportItem(rt, num, exportItemData, true);
                }
            }
        }

        public static bool AnalysisTemplateExists(string name, int responseTemplateID)
        {
            return AnalysisTemplateExists(name, responseTemplateID, null);
        }

        public static bool AnalysisTemplateExists(string name, int responseTemplateID, int? analysisTemplateID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AnalysisTemplate_GetIDFromName");
            storedProcCommandWrapper.AddInParameter("AnalysisName", DbType.String, name);
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, responseTemplateID);
            storedProcCommandWrapper.AddOutParameter("AnalysisID", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("AnalysisID");
            if (((parameterValue == null) || (parameterValue == DBNull.Value)) || (((int) parameterValue) <= 0))
            {
                return false;
            }
            return (!analysisTemplateID.HasValue || (Convert.ToInt32(analysisTemplateID) != Convert.ToInt32(parameterValue)));
        }

        public static AnalysisTemplate CopyTemplate(int templateID, ExtendedPrincipal creator, string languageCode)
        {
            DataSet ds = new DataSet();
            AnalysisTemplate analysisTemplate = GetAnalysisTemplate(templateID);
            MemoryStream stream = new MemoryStream();
            new XmlSerializer(typeof(AnalysisTemplate)).Serialize((Stream) stream, analysisTemplate);
            stream.Seek(0L, SeekOrigin.Begin);
            ds.ReadXml(stream, XmlReadMode.ReadSchema);
            int num = 1;
            string name = string.Concat(new object[] { analysisTemplate.Name, " ", TextManager.GetText("/common/duplicate", languageCode), " ", num });
            while (AnalysisTemplateExists(name, analysisTemplate.ResponseTemplateID))
            {
                num++;
                name = string.Concat(new object[] { analysisTemplate.Name, " ", TextManager.GetText("/common/duplicate", languageCode), " ", num });
            }
            int oldTemplateID = analysisTemplate.ID.Value;
            AnalysisTemplate template2 = CreateAnalysisTemplate(name, analysisTemplate.ResponseTemplateID, creator);
            template2.Import(ds, oldTemplateID);
            template2.Name = name;
            template2.Save();
            template2.SaveFilters();
            TextManager.SetText(template2.NameTextID, languageCode, name);
            return template2;
        }

        public static AnalysisTemplate CreateAnalysisTemplate(string analysisName, int responseTemplateID, ExtendedPrincipal creator)
        {
            LightweightResponseTemplate lightweightResponseTemplate = ResponseTemplateManager.GetLightweightResponseTemplate(responseTemplateID);
            if (lightweightResponseTemplate == null)
            {
                return null;
            }
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(creator, lightweightResponseTemplate, "Analysis.Create"))
            {
                throw new AuthorizationException();
            }
            AnalysisTemplate template2 = new AnalysisTemplate {
                Name = analysisName
            };
            if (creator == null)
            {
                string[] permissions = new string[] { "Analysis.Run", "Analysis.Edit" };
                Policy defaultPolicy = template2.CreatePolicy(permissions);
                AccessControlList acl = new AccessControlList();
                acl.Save();
                template2.InitializeAccess(defaultPolicy, acl);
            }
            else
            {
                string[] strArray2 = new string[0];
                Policy policy2 = template2.CreatePolicy(strArray2);
                AccessControlList list2 = new AccessControlList();
                Policy policy = new Policy(template2.SupportedPermissions);
                list2.Add(creator, policy);
                list2.Save();
                template2.InitializeAccess(policy2, list2);
            }
            template2.ResponseTemplateID = responseTemplateID;
            template2.Save();
            return template2;
        }

        public static void DeleteAnalysisTemplate(int analysisTemplateID)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AnalysisTemplate_Delete");
            storedProcCommandWrapper.AddInParameter("AnalysisTemplateID", DbType.Int32, analysisTemplateID);
            storedProcCommandWrapper.AddInParameter("ModifiedDate", DbType.DateTime, DateTime.Now);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        private static DataSet FilterTemplatesAndFolders(DataSet templateDataSet, ExtendedPrincipal currentPrincipal, int pageSize, int currentPage, string[] permissions, out int count)
        {
            count = 0;
            if ((templateDataSet == null) || (templateDataSet.Tables.Count <= 0))
            {
                return null;
            }
            DataSet set = new DataSet();
            DataTable table = templateDataSet.Tables[0].Clone();
            set.Tables.Add(table);
            table.Columns.Add("ViewEditReports", typeof(bool));
            table.Columns.Add("ViewResponses", typeof(bool));
            table.Columns.Add("ExportResponses", typeof(bool));
            IAuthorizationProvider authorizationProvider = AuthorizationFactory.GetAuthorizationProvider();
            int num = ((currentPage - 1) * pageSize) + 1;
            int num2 = (num + pageSize) - 1;
            int num3 = 0;
            foreach (DataRow row in templateDataSet.Tables[0].Select(null, null, DataViewRowState.CurrentRows))
            {
                if ((row["ItemID"] != DBNull.Value) && (row["ItemType"] != DBNull.Value))
                {
                    string str = DbUtility.GetValueFromDataRow<string>(row, "ItemType", null);
                    int dataID = DbUtility.GetValueFromDataRow<int>(row, "ItemID", -1);
                    bool flag = false;
                    bool item = false;
                    bool flag3 = false;
                    bool flag4 = false;
                    bool flag5 = false;
                    if (str.ToLower() == "folder")
                    {
                        FormFolder resource = new FormFolder();
                        resource.Load(dataID);
                        flag5 = authorizationProvider.Authorize(currentPrincipal, resource, "FormFolder.Read");
                    }
                    else
                    {
                        LightweightAccessControllable lightweightResponseTemplate = ResponseTemplateManager.GetLightweightResponseTemplate(dataID);
                        flag = authorizationProvider.Authorize(currentPrincipal, lightweightResponseTemplate, "Analysis.Create");
                        item = authorizationProvider.Authorize(currentPrincipal, lightweightResponseTemplate, "Analysis.Responses.View") || authorizationProvider.Authorize(currentPrincipal, lightweightResponseTemplate, "Analysis.Responses.Edit");
                        flag3 = authorizationProvider.Authorize(currentPrincipal, lightweightResponseTemplate, "Analysis.Responses.Export");
                        if ((!flag && !item) && !flag3)
                        {
                            int num5;
                            GetAnalyses(currentPrincipal, dataID, out num5);
                            flag4 = num5 > 0;
                        }
                    }
                    if ((flag || flag4) || ((item || flag3) || flag5))
                    {
                        num3++;
                        if (((currentPage <= 0) || (pageSize <= 0)) || ((num3 >= num) && (num3 <= num2)))
                        {
                            List<object> list = new List<object>();
                            list.AddRange(row.ItemArray);
                            list.Add(flag4 ? ((object) 1) : ((object) flag));
                            list.Add(item);
                            list.Add(flag3);
                            table.Rows.Add(list.ToArray());
                        }
                    }
                }
            }
            count = num3;
            return set;
        }

        public static AnalysisTemplate GenerateCSVExportTemplate(ResponseTemplate responseTemplate, string progressKey, string languageCode, int progressStart, int progressEnd)
        {
            if (responseTemplate == null)
            {
                return null;
            }
            CSVExportItemData exportItemData = (CSVExportItemData) ItemConfigurationManager.CreateConfigurationData("CSVExport");
            return GenerateExportTemplate(responseTemplate, exportItemData, progressKey, languageCode, progressStart, progressEnd);
        }

        private static AnalysisTemplate GenerateExportTemplate(ResponseTemplate responseTemplate, ExportItemData exportItemData, string progressKey, string languageCode, int progressStart, int progressEnd)
        {
            bool flag = Utilities.IsNotNullOrEmpty(progressKey) && Utilities.IsNotNullOrEmpty(languageCode);
            string format = flag ? TextManager.GetText("/controlText/exportManager/buildingExportTemplate", languageCode) : string.Empty;
            AnalysisTemplate template = new AnalysisTemplate();
            exportItemData.ID = -1;
            template.SetItem(exportItemData);
            template.ResponseTemplateID = responseTemplate.ID.Value;
            exportItemData.AddResponseTemplate(responseTemplate.ID.Value);
            int num = 1;
            foreach (TemplatePage page in responseTemplate.TemplatePages)
            {
                foreach (int num2 in page.ItemIds)
                {
                    AddSourceItemToExportItem(responseTemplate, num2, exportItemData, false);
                    if (flag)
                    {
                        string message = (format.Contains("{0}") && format.Contains("{1}")) ? string.Format(format, num, responseTemplate.ItemCount) : format;
                        int currentItem = progressStart + ((int) ((((double) num) / ((double) responseTemplate.ItemCount)) * (progressEnd - progressStart)));
                        ProgressProvider.SetProgress(progressKey, message, string.Empty, ProgressStatus.Running, currentItem, 100);
                    }
                    num++;
                }
            }
            return template;
        }

        public static AnalysisTemplate GenerateSPSSExportTemplate(ResponseTemplate responseTemplate, string progressKey, string languageCode, int progressStart, int progressEnd)
        {
            if (responseTemplate == null)
            {
                return null;
            }
            SPSSExportItemData exportItemData = (SPSSExportItemData) ItemConfigurationManager.CreateConfigurationData("SPSSExport");
            return GenerateExportTemplate(responseTemplate, exportItemData, progressKey, languageCode, progressStart, progressEnd);
        }

        public static DataTable GetAnalyses(ExtendedPrincipal principal, int responseTemplateID, out int analysisCount)
        {
            return AnalysisDAL.ListAnalyses(principal, responseTemplateID, out analysisCount);
        }

        public static DataTable GetAnalyses(ExtendedPrincipal principal, int responseTemplateID, int pageNumber, int pageSize, string filterField, string filterCriteria, string sortField, bool sortAscending, out int analysisCount)
        {
            int sortDirection = sortAscending ? 1 : 0;
            return AnalysisDAL.ListAnalyses(principal, responseTemplateID, pageNumber, pageSize, filterField, filterCriteria, sortField, sortDirection, out analysisCount);
        }

        public static AnalysisTemplate GetAnalysisTemplate(Guid guid)
        {
            AnalysisTemplate analysisTemplate = null;
            try
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AnalysisTemplate_GetID");
                storedProcCommandWrapper.AddInParameter("GUID", DbType.Guid, guid);
                int analysisTemplateID = 0;
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        if (reader.Read())
                        {
                            analysisTemplateID = Convert.ToInt32(reader["AnalysisTemplateID"]);
                        }
                    }
                    catch
                    {
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                if (analysisTemplateID > 0)
                {
                    analysisTemplate = GetAnalysisTemplate(analysisTemplateID);
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
            return analysisTemplate;
        }

        public static AnalysisTemplate GetAnalysisTemplate(int analysisTemplateID)
        {
            AnalysisTemplate template = null;
            try
            {
                template = new AnalysisTemplate();
                template.Load(analysisTemplateID);
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
            }
            return template;
        }

        public static int? GetAnalysisTemplateId(int responseTemplateId, string analysisTemplateName)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_AnalysisTemplate_GetIdFromName");
            storedProcCommandWrapper.AddInParameter("TemplateName", DbType.String, analysisTemplateName);
            storedProcCommandWrapper.AddInParameter("ResponseTemplateId", DbType.Int32, responseTemplateId);
            storedProcCommandWrapper.AddOutParameter("TemplateId", DbType.Int32, 4);
            database.ExecuteNonQuery(storedProcCommandWrapper);
            object parameterValue = storedProcCommandWrapper.GetParameterValue("TemplateId");
            if ((parameterValue != null) && (parameterValue != DBNull.Value))
            {
                return new int?((int) parameterValue);
            }
            return null;
        }

        public static DataSet GetAvailableTemplatesAndFolders(ExtendedPrincipal currentPrincipal, int? parentFolderId, int pageSize, int page, string filterField, string filterText, string sortField, bool descending, string[] permissions, out int totalItemCount)
        {
            SelectQuery query = Checkbox.Forms.Data.QueryFactory.GetAllTemplatesAndFoldersQuery(filterField, filterText, sortField, descending, parentFolderId);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            return FilterTemplatesAndFolders(database.ExecuteDataSet(sqlStringCommandWrapper), currentPrincipal, pageSize, page, permissions, out totalItemCount);
        }

        public static LightweightAccessControllable GetLightweightAnalysisTemplate(int analysisTemplateID)
        {
            LightweightTemplate theObject = new LightweightTemplate(analysisTemplateID);
            StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Select, theObject);
            return theObject;
        }
    }
}

