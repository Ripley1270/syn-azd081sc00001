﻿namespace Checkbox.Messaging.Email
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class EmailMessage : IEmailMessage
    {
        private List<IEmailAttachment> _attachments;
        private List<long> _attachmentsByRef;

        public List<IEmailAttachment> Attachments
        {
            get
            {
                if (this._attachments == null)
                {
                    this._attachments = new List<IEmailAttachment>();
                }
                return this._attachments;
            }
        }

        public List<long> AttachmentsByRef
        {
            get
            {
                if (this._attachmentsByRef == null)
                {
                    this._attachmentsByRef = new List<long>();
                }
                return this._attachmentsByRef;
            }
        }

        public string Body { get; set; }

        public MailFormat Format { get; set; }

        public string From { get; set; }

        public bool IsBodyHtml
        {
            get
            {
                return (this.Format == MailFormat.Html);
            }
        }

        public string Subject { get; set; }

        public string To { get; set; }
    }
}

