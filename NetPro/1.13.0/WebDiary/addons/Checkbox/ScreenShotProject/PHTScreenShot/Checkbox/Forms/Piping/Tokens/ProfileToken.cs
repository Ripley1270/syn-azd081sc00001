﻿namespace Checkbox.Forms.Piping.Tokens
{
    using System;

    [Serializable]
    public class ProfileToken : Token
    {
        public ProfileToken(string token) : base(token, TokenType.Profile)
        {
        }
    }
}

