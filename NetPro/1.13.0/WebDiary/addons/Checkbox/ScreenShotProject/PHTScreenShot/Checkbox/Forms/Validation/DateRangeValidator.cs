﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using System;
    using System.Globalization;
    using System.Runtime.CompilerServices;
    using System.Text;

    public class DateRangeValidator : RangeValidator<DateTime>
    {
        public DateRangeValidator(DateTime? minDate, DateTime? maxDate, Checkbox.Forms.Items.AnswerFormat answerFormat)
        {
            if (minDate.HasValue)
            {
                base.MinValue = minDate.Value;
            }
            if (maxDate.HasValue)
            {
                base.MaxValue = maxDate.Value;
            }
            this.AnswerFormat = answerFormat;
        }

        public override string GetMessage(string languageCode)
        {
            CultureInfo currentCulture;
            StringBuilder builder = new StringBuilder();
            if (this.AnswerFormat == Checkbox.Forms.Items.AnswerFormat.Date_USA)
            {
                currentCulture = new CultureInfo("en-US");
            }
            else if (this.AnswerFormat == Checkbox.Forms.Items.AnswerFormat.Date_ROTW)
            {
                currentCulture = new CultureInfo("en-GB");
            }
            else
            {
                currentCulture = CultureInfo.CurrentCulture;
            }
            if (base.MinValueSet && base.MaxValueSet)
            {
                builder.Append(TextManager.GetText("/validationMessages/date/dateBetween", languageCode));
                builder.Replace("{start}", base.MinValue.ToString("d", currentCulture));
                builder.Replace("{end}", base.MaxValue.ToString("d", currentCulture));
            }
            else if (base.MinValueSet)
            {
                builder.Append(TextManager.GetText("/validationMessages/date/dateAfter", languageCode));
                builder.Replace("{date}", base.MinValue.ToString("d", currentCulture));
            }
            else if (base.MaxValueSet)
            {
                builder.Append(TextManager.GetText("/validationMessages/date/dateBefore", languageCode));
                builder.Replace("{date}", base.MaxValue.ToString("d", currentCulture));
            }
            return builder.ToString();
        }

        public override bool Validate(DateTime input)
        {
            if (base.MinValueSet && (base.MinValue > input))
            {
                return false;
            }
            if (base.MaxValueSet && (base.MaxValue < input))
            {
                return false;
            }
            return true;
        }

        public Checkbox.Forms.Items.AnswerFormat AnswerFormat { get; private set; }
    }
}

