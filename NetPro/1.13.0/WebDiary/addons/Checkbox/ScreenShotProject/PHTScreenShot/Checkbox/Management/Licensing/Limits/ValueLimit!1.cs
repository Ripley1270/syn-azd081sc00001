﻿namespace Checkbox.Management.Licensing.Limits
{
    using Checkbox.Management;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Runtime.InteropServices;

    public abstract class ValueLimit<T> : LicenseLimit
    {
        protected ValueLimit()
        {
        }

        private static V GetLimitValueFromMasterDb<V>(string limitName)
        {
            V local = default(V);
            try
            {
                Database database = DatabaseFactory.CreateDatabase("MultiMasterDB");
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_GetLicenseLimitValue");
                storedProcCommandWrapper.AddInParameter("ContextName", DbType.String, DatabaseFactory.CurrentDataContext ?? string.Empty);
                storedProcCommandWrapper.AddInParameter("LimitName", DbType.String, limitName);
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        if (reader.Read())
                        {
                            local = DbUtility.GetValueFromDataReader<V>(reader, "LimitValue", default(V));
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                    return local;
                }
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessProtected");
            }
            return local;
        }

        public abstract LimitValidationResult ProtectedValidate(out string message);
        public override LimitValidationResult Validate(out string message)
        {
            if ((this.RuntimeLimitValue == null) && this.ValidIfLimitNull)
            {
                message = string.Empty;
                return LimitValidationResult.LimitNotReached;
            }
            return this.ProtectedValidate(out message);
        }

        public virtual bool IsLimitValueStoredInMasterDb
        {
            get
            {
                return false;
            }
        }

        public abstract T LicenseFileLimitValue { get; }

        public virtual T RuntimeLimitValue
        {
            get
            {
                if ((this.LicenseFileLimitValue == null) && ApplicationManager.AppSettings.EnableMultiDatabase)
                {
                    return ValueLimit<T>.GetLimitValueFromMasterDb<T>(this.LimitName);
                }
                return this.LicenseFileLimitValue;
            }
        }

        protected virtual bool ValidIfLimitNull
        {
            get
            {
                return true;
            }
        }
    }
}

