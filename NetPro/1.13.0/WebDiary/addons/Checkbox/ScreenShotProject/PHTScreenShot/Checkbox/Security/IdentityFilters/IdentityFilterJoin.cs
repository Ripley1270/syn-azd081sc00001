﻿namespace Checkbox.Security.IdentityFilters
{
    using System;

    public enum IdentityFilterJoin
    {
        And,
        Or
    }
}

