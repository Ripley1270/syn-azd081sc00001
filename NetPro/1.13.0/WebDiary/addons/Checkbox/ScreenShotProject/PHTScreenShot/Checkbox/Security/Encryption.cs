﻿namespace Checkbox.Security
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    public class Encryption
    {
        private Encryption()
        {
        }

        public static string HashString(string plaintext)
        {
            byte[] bytes = new MD5CryptoServiceProvider().ComputeHash(Encoding.ASCII.GetBytes(plaintext));
            return Encoding.ASCII.GetString(bytes);
        }

        public static string HashStringDotNetOneFormat(string plaintext)
        {
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
            provider.ComputeHash(Encoding.ASCII.GetBytes(plaintext));
            byte[] bytes = new byte[provider.Hash.Length];
            for (int i = 0; i < provider.Hash.Length; i++)
            {
                bytes[i] = (byte) (provider.Hash[i] & 0x7f);
            }
            return Encoding.ASCII.GetString(bytes);
        }

        public static string RijndaelDecrypt(string ciphertext, byte[] key, byte[] iv)
        {
            ICryptoTransform transform = Rijndael.Create().CreateDecryptor(key, iv);
            ASCIIEncoding encoding = new ASCIIEncoding();
            MemoryStream stream = new MemoryStream(encoding.GetBytes(ciphertext));
            CryptoStream stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Read);
            byte[] buffer = new byte[encoding.GetBytes(ciphertext).Length];
            stream2.Read(buffer, 0, buffer.Length);
            return encoding.GetString(buffer);
        }

        public static string RijndaelEncrypt(string plaintext, byte[] key, byte[] iv)
        {
            ICryptoTransform transform = Rijndael.Create().CreateEncryptor(key, iv);
            MemoryStream stream = new MemoryStream();
            CryptoStream stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Write);
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] bytes = encoding.GetBytes(plaintext);
            stream2.Write(bytes, 0, bytes.Length);
            stream2.FlushFinalBlock();
            return encoding.GetString(stream.ToArray());
        }
    }
}

