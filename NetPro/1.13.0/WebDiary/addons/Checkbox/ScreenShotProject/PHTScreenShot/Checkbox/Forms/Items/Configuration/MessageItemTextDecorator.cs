﻿namespace Checkbox.Forms.Items.Configuration
{
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class MessageItemTextDecorator : LocalizableResponseItemTextDecorator
    {
        private string _message;
        private bool _messageModified;

        public MessageItemTextDecorator(MessageItemData data, string language) : base(data, language)
        {
            this._message = string.Empty;
            this._messageModified = false;
        }

        protected override void CopyLocalizedText(ItemData data)
        {
            base.CopyLocalizedText(data);
            ArgumentValidation.CheckExpectedType(data, typeof(MessageItemData));
            Dictionary<string, string> allTexts = this.GetAllTexts(((MessageItemData) data).TextID);
            if (allTexts != null)
            {
                foreach (string str in allTexts.Keys)
                {
                    this.SetText(this.Data.TextID, allTexts[str], str);
                }
            }
        }

        protected override void SetLocalizedTexts()
        {
            base.SetLocalizedTexts();
            if (this.Data.TextID != string.Empty)
            {
                this.SetText(this.Data.TextID, this.Message);
            }
        }

        public MessageItemData Data
        {
            get
            {
                return (MessageItemData) base.Data;
            }
        }

        public string Message
        {
            get
            {
                if ((this.Data.TextID != string.Empty) && !this._messageModified)
                {
                    return this.GetText(this.Data.TextID);
                }
                return this._message;
            }
            set
            {
                this._message = value;
                this._messageModified = true;
            }
        }
    }
}

