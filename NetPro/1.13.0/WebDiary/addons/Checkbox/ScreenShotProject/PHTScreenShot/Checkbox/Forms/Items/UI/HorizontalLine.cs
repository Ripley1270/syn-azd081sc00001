﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class HorizontalLine : AppearanceData
    {
        public override string AppearanceCode
        {
            get
            {
                return "HORIZONTAL_LINE";
            }
        }
    }
}

