﻿namespace Checkbox.Users.Security
{
    using Checkbox.Security;
    using Checkbox.Users;
    using System;

    public class GroupSecurityEditor : AccessControllablePDOSecurityEditor
    {
        public GroupSecurityEditor(Group group) : base(group)
        {
        }

        protected override string RequiredEditorPermission
        {
            get
            {
                return "Group.Edit";
            }
        }
    }
}

