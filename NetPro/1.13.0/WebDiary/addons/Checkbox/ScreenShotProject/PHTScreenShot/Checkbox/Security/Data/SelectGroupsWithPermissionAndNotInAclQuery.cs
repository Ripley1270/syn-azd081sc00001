﻿namespace Checkbox.Security.Data
{
    using Checkbox.Security;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;

    internal static class SelectGroupsWithPermissionAndNotInAclQuery
    {
        public static SelectQuery GetQuery(IAccessControlList acl, ExtendedPrincipal currentPrincipal)
        {
            SelectQuery query;
            if (currentPrincipal.IsInRole("System Administrator"))
            {
                query = new SelectQuery("ckbx_Group");
                query.AddParameter("GroupID", string.Empty, "ckbx_Group");
            }
            else
            {
                query = QueryFactory.GetSelectEntitiesWithPermissionQuery("ckbx_Group", "GroupID", "AclID", currentPrincipal.AclTypeIdentifier, currentPrincipal.AclEntryIdentifier, "DefaultPolicy", PermissionJoin.Any, new string[] { "Group.View" });
            }
            query.AddParameter("GroupName", string.Empty, "ckbx_Group");
            query.AddCriterion(new QueryCriterion(new LiteralParameter("CONVERT (nvarchar, ckbx_Group.GroupID)"), CriteriaOperator.NotIn, new SubqueryParameter(SelectGroupsInAclQuery.GetQuery(acl))));
            query.AddSortField(new SortOrder("ckbx_Group.GroupName"));
            return query;
        }
    }
}

