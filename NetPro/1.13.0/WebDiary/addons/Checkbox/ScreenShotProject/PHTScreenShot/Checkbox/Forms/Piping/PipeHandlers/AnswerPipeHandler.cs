﻿namespace Checkbox.Forms.Piping.PipeHandlers
{
    using Checkbox.Forms;
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Piping.Tokens;
    using System;

    public class AnswerPipeHandler : PipeHandler
    {
        public override string GetTokenValue(Token token, object context)
        {
            if ((token is ItemToken) && (context is Response))
            {
                int itemID = ((ItemToken) token).ItemID;
                if (((Response) context).Items.ContainsKey(itemID) && (((Response) context).Items[itemID] is IAnswerable))
                {
                    return ((IAnswerable) ((Response) context).Items[itemID]).GetAnswer();
                }
            }
            return string.Empty;
        }
    }
}

