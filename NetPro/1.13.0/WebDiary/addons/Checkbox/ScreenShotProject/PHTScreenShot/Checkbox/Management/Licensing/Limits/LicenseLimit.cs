﻿namespace Checkbox.Management.Licensing.Limits
{
    using System;
    using System.Runtime.InteropServices;
    using Xheo.Licensing;

    public abstract class LicenseLimit
    {
        protected LicenseLimit()
        {
        }

        public abstract void Initialize(ExtendedLicense license);
        public abstract LimitValidationResult Validate(out string messageTextId);

        public abstract string LimitName { get; }
    }
}

