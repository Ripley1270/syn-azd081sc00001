﻿using Checkbox.Analytics.Data;
using Checkbox.Forms;
using System;
using System.Collections.Generic;

namespace Checkbox.Analytics.Filters
{
    //using System;
    //using System.Collections.Generic;

    internal interface IAnswerDataObjectFilter
    {
        bool EvaluateFilter(List<ItemAnswer> answers, Dictionary<long, ResponseProperties> responseProperties);
    }
}

