﻿namespace Checkbox.Forms
{
    using System;

    public class ResponsePageChangedEventArgs : EventArgs
    {
        private readonly int _newPagePosition;
        private readonly int _previousPagePosition;

        public ResponsePageChangedEventArgs(int previousPage, int newPage)
        {
            this._previousPagePosition = previousPage;
            this._newPagePosition = newPage;
        }

        public int NewPage
        {
            get
            {
                return this._newPagePosition;
            }
        }

        public int PreviousPage
        {
            get
            {
                return this._previousPagePosition;
            }
        }
    }
}

