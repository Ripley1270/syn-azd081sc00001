﻿namespace Checkbox.Analytics.Items.Configuration
{
    using Checkbox.Analytics.Items;
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Data;
    using System;
    using System.Data;

    [Serializable]
    public class MatrixSummaryItemData : AnalysisItemData
    {
        private int? _matrixSourceItem;

        public override void AddSourceItem(int sourceItemID)
        {
            if (!this.SourceItemIDs.Contains(sourceItemID))
            {
                foreach (int num in this.SourceItemIDs)
                {
                    this.RemoveSourceItem(num);
                }
                ItemData configurationData = ItemConfigurationManager.GetConfigurationData(sourceItemID);
                if ((configurationData != null) && (configurationData is ICompositeItemData))
                {
                    base.AddSourceItem(sourceItemID);
                    foreach (int num2 in ((ICompositeItemData) configurationData).GetChildItemDataIDs())
                    {
                        this.AddSourceItem(num2);
                    }
                }
            }
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to save matrix item data.  Data ID was null or <= zero.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertMatSummary");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("MatrixSourceItem", DbType.Int32, this._matrixSourceItem);
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateDataTables(t);
        }

        protected override Item CreateItem()
        {
            return new MatrixSummaryItem();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to load matrix summary item data, no ID was specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetMatSummary");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName, AnalysisItemData.SourceItemsTableName, AnalysisItemData.ResponseTemplatesTableName });
            return dataSet;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data != null)
            {
                this._matrixSourceItem = DbUtility.GetValueFromDataRow<int?>(data, "MatrixSourceItem", null);
                base.UseAliases = DbUtility.GetValueFromDataRow<bool>(data, "UseAliases", false);
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (!base.ID.HasValue || (base.ID <= 0))
            {
                throw new Exception("Unable to save matrix item data.  Data ID was null or <= zero.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateMatSummary");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("MatrixSourceItem", DbType.Int32, this._matrixSourceItem);
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateDataTables(t);
        }

        public override string DataTableName
        {
            get
            {
                return "MatrixSummaryItemData";
            }
        }

        public int? MatrixSourceItem
        {
            get
            {
                return this._matrixSourceItem;
            }
            set
            {
                this._matrixSourceItem = value;
                this.AddSourceItem(this._matrixSourceItem.Value);
            }
        }
    }
}

