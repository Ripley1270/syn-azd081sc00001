﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Validation;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Xml;

    [Serializable]
    public class MatrixItem : TabularItem
    {
        private Dictionary<int, Item> _columnTemplates;
        private Dictionary<int, List<string>> _columnValidationErrors;
        private Dictionary<int, int?> _columnWidths;
        private Dictionary<int, RowType> _rowTypes;
        private List<int> _uniqueColumns;

        public bool ColumnAnswersAreUnique(int column)
        {
            return this._uniqueColumns.Contains(column);
        }

        public override void Configure(ItemData configuration, string languageCode)
        {
            ArgumentValidation.CheckExpectedType(configuration, typeof(MatrixItemData));
            MatrixItemData data = (MatrixItemData) configuration;
            base.Configure(configuration, languageCode);
            this.PrimaryColumnIndex = data.PrimaryKeyColumnIndex;
            this._columnTemplates = new Dictionary<int, Item>();
            this._uniqueColumns = new List<int>();
            this._columnWidths = new Dictionary<int, int?>();
            for (int i = 1; i <= data.ColumnCount; i++)
            {
                this._columnWidths[i] = data.GetColumnWidth(i);
                ItemData columnPrototype = data.GetColumnPrototype(i);
                if (columnPrototype != null)
                {
                    Item item = columnPrototype.CreateItem(languageCode);
                    if (item != null)
                    {
                        this._columnTemplates[i] = item;
                    }
                }
                if (data.GetColumnUniqueness(i))
                {
                    this._uniqueColumns.Add(i);
                }
            }
            this._rowTypes = new Dictionary<int, RowType>();
            foreach (DataRow row in data.MatrixRows.Select(null, null, DataViewRowState.CurrentRows))
            {
                int num2 = Convert.ToInt32(row["Row"]);
                if ((row["IsSubheading"] != DBNull.Value) && Convert.ToBoolean(row["IsSubheading"]))
                {
                    this._rowTypes[num2] = RowType.Subheading;
                }
                else if ((row["IsOther"] != DBNull.Value) && Convert.ToBoolean(row["IsOther"]))
                {
                    this._rowTypes[num2] = RowType.Other;
                }
                else
                {
                    this._rowTypes[num2] = RowType.Normal;
                }
            }
        }

        public string GetColumnAlias(int column)
        {
            if (this._columnTemplates.ContainsKey(column))
            {
                Item item = this._columnTemplates[column];
                if (item != null)
                {
                    return item.Alias;
                }
            }
            return string.Empty;
        }

        public List<string> GetColumnOptionTexts(int column)
        {
            List<string> list = new List<string>();
            if (this._columnTemplates.ContainsKey(column))
            {
                Item item = this._columnTemplates[column];
                if (!(item is SelectItem))
                {
                    return list;
                }
                foreach (ListOption option in ((SelectItem) item).Options)
                {
                    list.Add(option.Text);
                }
            }
            return list;
        }

        public bool GetColumnRequired(int column)
        {
            Item columnTemplate = this.GetColumnTemplate(column);
            return (((columnTemplate != null) && (columnTemplate is ResponseItem)) && ((ResponseItem) columnTemplate).Required);
        }

        public List<string> GetColumnSubTexts(int column)
        {
            List<string> list = new List<string>();
            if (this._columnTemplates.ContainsKey(column))
            {
                Item item = this._columnTemplates[column];
                if ((item == null) || !(item is RatingScale))
                {
                    return list;
                }
                list.Add(((RatingScale) item).StartText);
                list.Add(((RatingScale) item).MidText);
                list.Add(((RatingScale) item).EndText);
                foreach (ListOption option in ((RatingScale) item).Options)
                {
                    if (option.IsOther)
                    {
                        list.Add(string.Empty);
                        return list;
                    }
                }
            }
            return list;
        }

        public Item GetColumnTemplate(int columnId)
        {
            if (this._columnTemplates.ContainsKey(columnId))
            {
                return this._columnTemplates[columnId];
            }
            return null;
        }

        public string GetColumnText(int column)
        {
            if (this._columnTemplates.ContainsKey(column))
            {
                Item item = this._columnTemplates[column];
                if ((item != null) && (item is LabelledItem))
                {
                    return ((LabelledItem) item).Text;
                }
            }
            return string.Empty;
        }

        public int? GetColumnTypeID(int column)
        {
            Item columnTemplate = this.GetColumnTemplate(column);
            if (columnTemplate != null)
            {
                return new int?(columnTemplate.TypeID);
            }
            return null;
        }

        public string GetColumnTypeName(int column)
        {
            Item columnTemplate = this.GetColumnTemplate(column);
            if (columnTemplate != null)
            {
                return columnTemplate.ItemTypeName;
            }
            return null;
        }

        public virtual int? GetColumnWidth(int columnNumber)
        {
            if (this._columnWidths.ContainsKey(columnNumber))
            {
                return this._columnWidths[columnNumber];
            }
            return null;
        }

        protected virtual string GetRowAlias(int row)
        {
            Item itemAt = this.GetItemAt(row, this.PrimaryColumnIndex);
            if (itemAt != null)
            {
                return itemAt.Alias;
            }
            return string.Empty;
        }

        protected virtual string GetRowText(int row)
        {
            Item itemAt = this.GetItemAt(row, this.PrimaryColumnIndex);
            if ((itemAt != null) && (itemAt is Message))
            {
                return ((Message) itemAt).Text;
            }
            return string.Empty;
        }

        public RowType GetRowType(int row)
        {
            if (this._rowTypes.ContainsKey(row))
            {
                return this._rowTypes[row];
            }
            return RowType.Normal;
        }

        protected override void OnChildItemExcluded(Item child)
        {
            base.OnChildItemExcluded(child);
            Coordinate childItemCoordinate = this.GetChildItemCoordinate(child);
            for (int i = 1; i <= this.ColumnCount; i++)
            {
                if (i != this.PrimaryColumnIndex)
                {
                    Item itemAt = this.GetItemAt(childItemCoordinate.Y, i);
                    if ((itemAt != null) && !itemAt.Excluded)
                    {
                        itemAt.Excluded = true;
                    }
                }
            }
        }

        protected override void OnResponseSet()
        {
            base.OnResponseSet();
            foreach (Item item in this._columnTemplates.Values)
            {
                if ((item != null) && (item is ResponseItem))
                {
                    ((ResponseItem) item).Response = base.Response;
                }
            }
        }

        internal override void RunRules()
        {
            base.RunRules();
            for (int i = 1; i <= this.RowCount; i++)
            {
                ((ResponseItem) this.GetItemAt(i, this.PrimaryColumnIndex)).RunRules();
            }
        }

        protected override bool ValidateAnswers()
        {
            this.ColumnValidationErrors.Clear();
            this.RunRules();
            MatrixItemValidator validator = new MatrixItemValidator();
            if (!validator.Validate(this))
            {
                this._columnValidationErrors = validator.ColumnValidationErrors;
                base.ValidationErrors.Add(validator.GetMessage(base.LanguageCode));
                return false;
            }
            return true;
        }

        protected override void WriteColumnInstanceData(int row, int column, XmlWriter writer)
        {
            writer.WriteStartElement("column");
            writer.WriteAttributeString("columnNumber", column.ToString());
            writer.WriteAttributeString("uniqueValues", this._uniqueColumns.Contains(column) ? "true" : "false");
            writer.WriteAttributeString("isPkColumn", (column == this.PrimaryColumnIndex) ? "true" : "false");
            string columnText = this.GetColumnText(column);
            string columnAlias = this.GetColumnAlias(column);
            writer.WriteElementString("text", Utilities.IsNotNullOrEmpty(columnText) ? columnText : string.Empty);
            writer.WriteElementString("alias", Utilities.IsNotNullOrEmpty(columnAlias) ? columnAlias : string.Empty);
            Item itemAt = this.GetItemAt(row, column);
            if (itemAt != null)
            {
                itemAt.WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        protected override void WriteRowInstanceData(int row, XmlWriter writer)
        {
            writer.WriteStartElement("row");
            writer.WriteAttributeString("rowNumber", row.ToString());
            RowType rowType = this.GetRowType(row);
            string rowText = this.GetRowText(row);
            string rowAlias = this.GetRowAlias(row);
            writer.WriteAttributeString("rowType", rowType.ToString());
            writer.WriteElementString("text", Utilities.IsNotNullOrEmpty(rowText) ? rowText : string.Empty);
            writer.WriteElementString("alias", Utilities.IsNotNullOrEmpty(rowAlias) ? rowAlias : string.Empty);
            writer.WriteStartElement("columns");
            for (int i = 1; i <= this.ColumnCount; i++)
            {
                this.WriteColumnInstanceData(row, i, writer);
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        public override void WriteXmlMetaData(XmlWriter writer)
        {
            base.WriteXmlMetaData(writer);
            writer.WriteStartElement("matrixMetaData");
            writer.WriteElementString("pkColumnIndex", this.PrimaryColumnIndex.ToString());
            writer.WriteEndElement();
        }

        public Dictionary<int, List<string>> ColumnValidationErrors
        {
            get
            {
                if (this._columnValidationErrors == null)
                {
                    this._columnValidationErrors = new Dictionary<int, List<string>>();
                }
                return this._columnValidationErrors;
            }
        }

        public int PrimaryColumnIndex { get; private set; }

        public override bool Required
        {
            get
            {
                for (int i = 1; i <= this.ColumnCount; i++)
                {
                    if ((i != this.PrimaryColumnIndex) && this.GetColumnRequired(i))
                    {
                        return true;
                    }
                }
                return false;
            }
        }
    }
}

