﻿namespace Checkbox.Panels
{
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using System;
    using System.Collections;
    using System.Data;
    using System.Reflection;

    internal class PanelFactory
    {
        private static readonly Hashtable _panelTypes;

        static PanelFactory()
        {
            lock (typeof(PanelFactory))
            {
                _panelTypes = new Hashtable();
            }
        }

        private static Panel CreateObject(Type type)
        {
            object obj2;
            ArgumentValidation.CheckForNullReference(type, "type");
            ValidateTypeIsItemData(type);
            ConstructorInfo constructor = type.GetConstructor(new Type[0]);
            if (constructor == null)
            {
                throw new Exception("Panel does not have a constructor: " + type.FullName);
            }
            try
            {
                obj2 = constructor.Invoke(null);
            }
            catch (MethodAccessException exception)
            {
                throw new Exception(exception.Message, exception);
            }
            catch (TargetInvocationException exception2)
            {
                throw new Exception(exception2.Message, exception2);
            }
            catch (TargetParameterCountException exception3)
            {
                throw new Exception(exception3.Message, exception3);
            }
            return (Panel) obj2;
        }

        private static Panel CreatePanel(PanelTypeInfo typeInfo)
        {
            return CreateObject(GetType(typeInfo.ClassTypeName + "," + typeInfo.ClassTypeAssembly));
        }

        public Panel CreatePanel(int panelTypeID)
        {
            PanelTypeInfo panelTypeInfo = GetPanelTypeInfo(panelTypeID);
            if (panelTypeInfo != null)
            {
                return CreatePanel(panelTypeInfo);
            }
            return null;
        }

        private static PanelTypeInfo GetPanelTypeInfo(int panelTypeID)
        {
            if (_panelTypes.ContainsKey(panelTypeID))
            {
                return (PanelTypeInfo) _panelTypes[panelTypeID];
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_GetTypeInfo");
            storedProcCommandWrapper.AddInParameter("PanelTypeID", DbType.Int32, panelTypeID);
            string str = string.Empty;
            string str2 = string.Empty;
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        if (reader["TypeName"] != DBNull.Value)
                        {
                            str = (string) reader["TypeName"];
                        }
                        if (reader["TypeAssembly"] != DBNull.Value)
                        {
                            str2 = (string) reader["TypeAssembly"];
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            if (!(str.Trim() != string.Empty) || !(str2.Trim() != string.Empty))
            {
                return null;
            }
            PanelTypeInfo info = new PanelTypeInfo {
                ClassTypeName = str,
                ClassTypeAssembly = str2,
                TypeID = panelTypeID
            };
            lock (_panelTypes.SyncRoot)
            {
                _panelTypes[panelTypeID] = info;
            }
            return info;
        }

        private static Type GetType(string typeName)
        {
            Type type;
            ArgumentValidation.CheckForEmptyString(typeName, "typeName");
            try
            {
                type = Type.GetType(typeName, true, false);
            }
            catch (TypeLoadException exception)
            {
                throw new Exception("A type-loading error occurred.  Type was: " + typeName, exception);
            }
            return type;
        }

        private static void ValidateTypeIsItemData(Type type)
        {
            ArgumentValidation.CheckForNullReference(type, "type");
            if (!typeof(Panel).IsAssignableFrom(type))
            {
                throw new Exception("Type mismatch between Panel type [" + typeof(Panel).AssemblyQualifiedName + "] and requested type [" + type.AssemblyQualifiedName);
            }
        }

        private class PanelTypeInfo
        {
            public string ClassTypeAssembly;
            public string ClassTypeName;
            public int TypeID;
        }
    }
}

