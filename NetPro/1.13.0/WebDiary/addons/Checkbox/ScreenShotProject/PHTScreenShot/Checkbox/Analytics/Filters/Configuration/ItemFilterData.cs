﻿namespace Checkbox.Analytics.Filters.Configuration
{
    using Checkbox.Analytics.Filters;
    using Checkbox.Common;
    using Checkbox.Forms.Data;
    using Checkbox.Forms.Logic;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ItemFilterData : FilterData
    {
        private ItemFilterTextPrefixCallback _itemTextCallback;

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Filter_CreateItem");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, this.ItemId);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override void CreateDataRelationsInternal(DataSet ds)
        {
            base.CreateDataRelationsInternal(ds);
            if ((!ds.Relations.Contains("FilterItemID_AnalysisItemID") && ds.Tables.Contains(this.DataTableName)) && ds.Tables.Contains("TemplateItems"))
            {
                DataRelation relation = new DataRelation("FilterItemID_AnalysisItemID", ds.Tables["TemplateItems"].Columns["ItemID"], ds.Tables[this.DataTableName].Columns["ItemID"]);
                ds.Tables[this.DataTableName].Constraints.Add(new ForeignKeyConstraint(ds.Tables["TemplateItems"].Columns["ItemID"], ds.Tables[this.DataTableName].Columns["ItemID"]));
                ds.Relations.Add(relation);
            }
        }

        protected override Checkbox.Analytics.Filters.Filter CreateFilterObject()
        {
            if (this.Operator == LogicalOperator.Answered)
            {
                return new AnsweredFilter();
            }
            if (this.Operator == LogicalOperator.Equal)
            {
                return new EqualFilter();
            }
            if (this.Operator == LogicalOperator.NotAnswered)
            {
                return new NotAnsweredFilter();
            }
            if (this.Operator == LogicalOperator.NotEqual)
            {
                return new NotEqualFilter();
            }
            return new ItemFilter();
        }

        public override void Delete(IDbTransaction t)
        {
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Filter_DeleteItem");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, base.ID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            base.Delete(t);
        }

        public override DataSet GetConfigurationDataSet()
        {
            if (!base.ID.HasValue)
            {
                throw new Exception("Unable to get data set.  No filter id was specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Filter_GetItem");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, "ItemFilterData");
            return dataSet;
        }

        protected override string GetFilterLeftOperandText(string languageCode)
        {
            string str = string.Empty;
            if (this._itemTextCallback != null)
            {
                str = this._itemTextCallback(this.ItemId, languageCode);
            }
            return string.Format("{0}{1}", str, Utilities.StripHtml(SurveyMetaDataProxy.GetItemText(this.ItemId, languageCode, false, true), 0x80));
        }

        protected override string GetFilterRightOperandText(string languageCode)
        {
            if (this.Value == null)
            {
                return string.Empty;
            }
            int? nullable = Utilities.AsInt(this.Value.ToString());
            if (!nullable.HasValue)
            {
                return this.Value.ToString();
            }
            string str = Utilities.StripHtml(SurveyMetaDataProxy.GetOptionText(null, nullable.Value, languageCode, false, true), 0x80);
            if (Utilities.IsNullOrEmpty(str))
            {
                str = this.Value.ToString();
            }
            return str;
        }

        protected override void Load(DataRow data)
        {
            base.Load(data);
            this.ItemId = DbUtility.GetValueFromDataRow<int>(data, "ItemID", -1);
        }

        public void RegisterTextCallback(ItemFilterTextPrefixCallback callback)
        {
            this._itemTextCallback = callback;
        }

        public void UnRegisterTextCallback()
        {
            this._itemTextCallback = null;
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Filter_UpdateItem");
            storedProcCommandWrapper.AddInParameter("FilterID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, this.ItemId);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public override string DataTableName
        {
            get
            {
                return "ItemFilterData";
            }
        }

        public override string IdentityColumnName
        {
            get
            {
                return "FilterID";
            }
        }

        public int ItemId { get; set; }
    }
}

