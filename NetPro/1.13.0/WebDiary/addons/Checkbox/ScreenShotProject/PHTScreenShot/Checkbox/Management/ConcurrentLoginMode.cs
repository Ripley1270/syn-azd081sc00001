﻿namespace Checkbox.Management
{
    using System;

    public enum ConcurrentLoginMode
    {
        Allowed,
        NotAllowed,
        LogoutCurrent
    }
}

