﻿namespace Checkbox.Forms
{
    using Checkbox.Common;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Data;

    public class RootFormFolder : FormFolder
    {
        internal RootFormFolder() : base(new string[0], new string[0])
        {
            base.Name = "ROOT FOLDER";
            base.Description = "The implicit root";
            base.CreatedBy = "admin";
        }

        public override void Add(int id)
        {
            if (id <= 0)
            {
                throw new Exception("Invalid Response Template ID.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_RT_MoveTemplateToRoot");
            storedProcCommandWrapper.AddInParameter("ResponseTemplateID", DbType.Int32, id);
            database.ExecuteNonQuery(storedProcCommandWrapper);
        }

        public override void Add(object responseTemplate)
        {
            ArgumentValidation.CheckExpectedType(responseTemplate, typeof(ResponseTemplate));
            this.Add(((ResponseTemplate) responseTemplate).ID.Value);
        }

        public override Folder Copy(ExtendedPrincipal owner, string languageCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override Policy CreatePolicy(string[] permissions)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override SecurityEditor GetEditor()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void Remove(int id)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override void Remove(object item)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override Policy DefaultPolicy
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public override string[] SupportedPermissionMasks
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public override string[] SupportedPermissions
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
    }
}

