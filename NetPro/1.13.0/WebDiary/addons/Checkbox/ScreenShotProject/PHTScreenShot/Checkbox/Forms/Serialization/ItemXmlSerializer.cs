﻿namespace Checkbox.Forms.Serialization
{
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Items.UI;
    using System;
    using System.IO;
    using System.Xml;

    public class ItemXmlSerializer : IItemSerializer
    {
        public string SerializeItem(Item item, AppearanceData itemAppearance)
        {
            StringWriter w = new StringWriter();
            XmlTextWriter writer = new XmlTextWriter(w);
            item.WriteXml(writer);
            if (itemAppearance != null)
            {
                itemAppearance.WriteXml(writer);
            }
            writer.Flush();
            writer.Close();
            return w.ToString();
        }
    }
}

