﻿namespace Checkbox.Analytics.Items.Configuration
{
    using Checkbox.Analytics.Items;
    using Checkbox.Forms.Items;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class DetailsItemData : AnalysisItemData
    {
        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertDetailsItem");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("GroupAnswers", DbType.Boolean, this.GroupAnswers);
            storedProcCommandWrapper.AddInParameter("LinkToResponseDetails", DbType.Boolean, this.LinkToResponseDetails);
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateDataTables(t);
        }

        protected override Item CreateItem()
        {
            return new DetailsItem();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new Exception("Unable to load data when the ID is not specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetDetailsItem");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, new string[] { this.DataTableName, AnalysisItemData.SourceItemsTableName, AnalysisItemData.ResponseTemplatesTableName });
            return dataSet;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            this.GroupAnswers = DbUtility.GetValueFromDataRow<bool>(data, "GroupAnswers", false);
            this.LinkToResponseDetails = DbUtility.GetValueFromDataRow<bool>(data, "LinkToResponseDetails", false);
            base.UseAliases = DbUtility.GetValueFromDataRow<bool>(data, "UseAliases", false);
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateDetailsItem");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("GroupAnswers", DbType.Boolean, this.GroupAnswers);
            storedProcCommandWrapper.AddInParameter("LinkToResponseDetails", DbType.Boolean, this.LinkToResponseDetails);
            storedProcCommandWrapper.AddInParameter("UseAliases", DbType.Boolean, base.UseAliases);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
            this.UpdateDataTables(t);
        }

        public override string DataTableName
        {
            get
            {
                return "DetailsItemData";
            }
        }

        public bool GroupAnswers { get; set; }

        public bool LinkToResponseDetails { get; set; }
    }
}

