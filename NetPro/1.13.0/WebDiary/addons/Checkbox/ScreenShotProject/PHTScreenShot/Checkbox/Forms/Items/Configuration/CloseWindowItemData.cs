﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using System;

    [Serializable]
    public class CloseWindowItemData : ResponseItemData
    {
        protected override Item CreateItem()
        {
            return new CloseWindowItem();
        }

        public override string DataTableName
        {
            get
            {
                return this.ParentDataTableName;
            }
        }
    }
}

