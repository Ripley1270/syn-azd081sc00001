﻿using Checkbox.Forms.Items;
using Prezza.Framework.Data;
using Prezza.Framework.ExceptionHandling;
using System;
using System.Collections.ObjectModel;
using System.Data;

namespace Checkbox.Forms.Items.Configuration
{

    [Serializable]
    public class Select1Data : SelectItemData
    {
        private PHTScreenShotSession _screenShotSession;


        protected override void Create(IDbTransaction transaction)
        {
            base.Create(transaction);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertSelect1");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, this.SubTextID);
            storedProcCommandWrapper.AddInParameter("IsRequired", DbType.Int32, this.IsRequired);
            storedProcCommandWrapper.AddInParameter("AllowOther", DbType.Int32, this.AllowOther);
            storedProcCommandWrapper.AddInParameter("OtherTextID", DbType.String, this.OtherTextID);
            storedProcCommandWrapper.AddInParameter("Randomize", DbType.Int32, this.Randomize);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            this.UpdateLists(transaction);
        }

        protected override Item CreateItem()
        {
            return new Select1();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            try
            {
                string calledStoredProcedure = string.Empty;
                this._screenShotSession = new PHTScreenShotSession();
                if (this._screenShotSession.ScreenShotMode)
                    calledStoredProcedure = "PHTScreenShotItemDataGetSelect1";
                else
                    calledStoredProcedure = "ckbx_ItemData_GetSelect1";


                Database database = DatabaseFactory.CreateDatabase();
                //DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetSelect1");
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper(calledStoredProcedure);
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
                DataSet concreteConfigurationDataSet = base.GetConcreteConfigurationDataSet();
                DataSet dataSet = new DataSet();
                database.LoadDataSet(storedProcCommandWrapper, dataSet, this.ConfigurationDataTableNames);
                concreteConfigurationDataSet.Merge(dataSet);
                if (concreteConfigurationDataSet.Tables.Contains("ItemOptions"))
                {
                    concreteConfigurationDataSet.Tables["ItemOptions"].PrimaryKey = new DataColumn[] { concreteConfigurationDataSet.Tables["ItemOptions"].Columns["OptionID"], concreteConfigurationDataSet.Tables["ItemOptions"].Columns["ItemID"] };
                }
                if (concreteConfigurationDataSet.Tables.Contains("ItemLists"))
                {
                    concreteConfigurationDataSet.Tables["ItemLists"].PrimaryKey = new DataColumn[] { concreteConfigurationDataSet.Tables["ItemLists"].Columns["ItemID"], concreteConfigurationDataSet.Tables["ItemLists"].Columns["ListID"] };
                }
                return concreteConfigurationDataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            try
            {
                this.IsRequired = DbUtility.GetValueFromDataRow<int>(data, "IsRequired", 0) == 1;
                this.AllowOther = DbUtility.GetValueFromDataRow<int>(data, "AllowOther", 0) == 1;
                this.Randomize = DbUtility.GetValueFromDataRow<int>(data, "Randomize", 0) == 1;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        protected override void Update(IDbTransaction transaction)
        {
            base.Update(transaction);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateSelect1");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, this.SubTextID);
            storedProcCommandWrapper.AddInParameter("IsRequired", DbType.Int32, this.IsRequired);
            storedProcCommandWrapper.AddInParameter("AllowOther", DbType.Int32, this.AllowOther);
            storedProcCommandWrapper.AddInParameter("OtherTextID", DbType.String, this.OtherTextID);
            storedProcCommandWrapper.AddInParameter("Randomize", DbType.Int32, this.Randomize);
            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
            this.UpdateLists(transaction);
        }

        public override string DataTableName
        {
            get
            {
                return "Select1Data";
            }
        }

        public override ReadOnlyCollection<ListOptionData> Options
        {
            get
            {
                bool flag = false;
                ReadOnlyCollection<ListOptionData> options = base.Options;
                foreach (ListOptionData data in options)
                {
                    if (data.IsDefault)
                    {
                        if (flag)
                        {
                            data.IsDefault = false;
                        }
                        flag = true;
                    }
                }
                return options;
            }
        }
    }
}

