﻿namespace Checkbox.Forms.Logic
{
    using System;

    [Serializable]
    public enum LogicalOperator
    {
        OperatorNotSpecified,
        Equal,
        NotEqual,
        GreaterThan,
        GreaterThanEqual,
        LessThan,
        LessThanEqual,
        Contains,
        DoesNotContain,
        Answered,
        NotAnswered,
        IsNull,
        IsNotNull
    }
}

