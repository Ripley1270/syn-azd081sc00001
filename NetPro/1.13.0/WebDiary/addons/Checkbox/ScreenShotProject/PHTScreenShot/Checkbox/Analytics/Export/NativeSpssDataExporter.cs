﻿namespace Checkbox.Analytics.Export
{
    using Checkbox.Common;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Globalization.Text;
    using Checkbox.Progress;
    using Spss;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;

    public class NativeSpssDataExporter : SpssCompatibleCsvDataExporter
    {
        private List<long> _responseIds;
        private DataTable _resultTable;

        private object[] DataRowCallback(int rowNumber)
        {
            int num = rowNumber - 1;
            if ((this._responseIds == null) || (num >= this._responseIds.Count))
            {
                return null;
            }
            List<string> list = CsvDataExporter.GetAnswerData(base.QuestionFieldNames.Count, this._responseIds[num], base.Analysis);
            List<object> list2 = new List<object>(list.Count + 1);
            if (base.Options.IncludeResponseId)
            {
                list2.Add(this._responseIds[num]);
            }
            else
            {
                list2.Add(rowNumber);
            }
            for (int i = 1; i < this._resultTable.Columns.Count; i++)
            {
                int num3;
                object item = Utilities.IsNotNullOrEmpty(list[i - 1]) ? list[i - 1] : string.Empty;
                if ((this._resultTable.Columns[i].DataType == typeof(int)) && !int.TryParse(item.ToString(), out num3))
                {
                    item = DBNull.Value;
                }
                else
                {
                    item = Utilities.StripHtml(item.ToString(), 0xff);
                }
                list2.Add(item);
            }
            if (Utilities.IsNotNullOrEmpty(base.ProgressKey) && ((rowNumber % 0x19) == 0))
            {
                double num4 = 100.0 * (0.7 + (0.3 * (((double) rowNumber) / ((double) this._responseIds.Count))));
                ProgressData progressData = new ProgressData {
                    CurrentItem = (int) num4,
                    Status = ProgressStatus.Running,
                    Message = TextManager.GetText("/controlText/exportManager/exportingAnswers", base.LanguageCode),
                    TotalItemCount = 100
                };
                ProgressProvider.SetProgress(base.ProgressKey, progressData);
            }
            return list2.ToArray();
        }

        private List<string> GetOptionTextsForField(string fieldName)
        {
            List<string> list = new List<string>();
            if (!fieldName.Contains("_"))
            {
                int itemIdForField = base.GetItemIdForField(fieldName);
                if (itemIdForField <= 0)
                {
                    return list;
                }
                ItemData configurationData = ItemConfigurationManager.GetConfigurationData(itemIdForField);
                if ((configurationData == null) || !(configurationData is SelectItemData))
                {
                    return list;
                }
                Dictionary<int, string> dictionary = ItemConfigurationManager.GetOptionTexts((SelectItemData) configurationData, base.LanguageCode, null);
                foreach (ListOptionData data2 in ((SelectItemData) configurationData).Options)
                {
                    if (dictionary.ContainsKey(data2.OptionID))
                    {
                        list.Add(Utilities.StripHtml(dictionary[data2.OptionID], 60));
                    }
                    else
                    {
                        list.Add("Option_" + data2.OptionID);
                    }
                }
            }
            return list;
        }

        private void MetaDataCallback(VarMetaData metaData)
        {
            metaData.Label = Utilities.StripHtml(base.GetActualTextForField(metaData.Name), 0x100);
            if (metaData.HasValueLabels)
            {
                List<string> optionTextsForField = this.GetOptionTextsForField(metaData.Name);
                for (int i = 1; i <= optionTextsForField.Count; i++)
                {
                    metaData[i] = optionTextsForField[i - 1];
                }
            }
        }

        protected override void WriteExportData(TextWriter writer)
        {
            throw new NotImplementedException();
        }

        public override void WriteToFile(string filePath)
        {
            if (Utilities.IsNotNullOrEmpty(base.ProgressKey))
            {
                ProgressData progressData = new ProgressData {
                    CurrentItem = 0,
                    Status = ProgressStatus.Pending,
                    Message = TextManager.GetText("/controlText/exportManager/analyzingSurveyStructure", base.LanguageCode)
                };
                ProgressProvider.SetProgress(base.ProgressKey, progressData);
            }
            base.QuestionFieldNames = this.ListAllQuestionFieldNames();
            this._resultTable = new DataTable();
            if (base.Options.IncludeResponseId)
            {
                this._resultTable.Columns.Add("ResponseID", typeof(long));
            }
            else
            {
                this._resultTable.Columns.Add("RowNumber", typeof(int));
            }
            for (int i = 0; i < base.QuestionFieldNames.Count; i++)
            {
                Type type = typeof(int);
                if (base.Options.IncludeOpenEnded && !base.QuestionFieldNames[i].Contains("_"))
                {
                    int itemIdForField = base.GetItemIdForField(base.QuestionFieldNames[i]);
                    if (itemIdForField > 0)
                    {
                        ItemData configurationData = ItemConfigurationManager.GetConfigurationData(itemIdForField);
                        if ((configurationData != null) && !(configurationData is SelectItemData))
                        {
                            type = typeof(string);
                        }
                    }
                }
                this._resultTable.Columns.Add(base.QuestionFieldNames[i], type);
            }
            this._responseIds = base.Analysis.Data.ListResponseIds();
            SpssConvert.ToFile(this._resultTable, filePath, new NeedDataRowCallback(this.DataRowCallback), new MetadataProviderCallback(this.MetaDataCallback));
            if (Utilities.IsNotNullOrEmpty(base.ProgressKey))
            {
                ProgressData data3 = new ProgressData {
                    CurrentItem = 0x63,
                    Status = ProgressStatus.Completed,
                    Message = TextManager.GetText("/controlText/exportManager/completed", base.LanguageCode),
                    TotalItemCount = 100
                };
                ProgressProvider.SetProgress(base.ProgressKey, data3);
            }
        }
    }
}

