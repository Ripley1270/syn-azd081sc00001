﻿namespace Checkbox.Forms.Data
{
    using Checkbox.Security.Data;
    using Prezza.Framework.Data;
    using System;

    internal class SelectAvailableFormFoldersQuery
    {
        private SelectAvailableFormFoldersQuery()
        {
        }

        internal static SelectQuery GetQuery(string entryType, string entryIdentifier)
        {
            return Checkbox.Security.Data.QueryFactory.GetSelectEntitiesWithPermissionQuery("ckbx_Folder", "FolderID", "AclID", entryType, entryIdentifier, "FormFolder.FullControl", "DefaultPolicy");
        }
    }
}

