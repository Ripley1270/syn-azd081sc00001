﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public abstract class LabelledItemAppearanceData : AppearanceData
    {
        protected LabelledItemAppearanceData()
        {
        }

        //public override Checkbox.Forms.Items.UI.LabelPosition? LabelPosition
        //{
        //    get
        //    {
        //        if (!base.LabelPosition.HasValue)
        //        {
        //            return 3;
        //        }
        //        return base.LabelPosition;
        //    }
        //    set
        //    {
        //        base.LabelPosition = value;
        //    }
        //}
        public override Checkbox.Forms.Items.UI.LabelPosition? LabelPosition
        {
            get
            {
                Checkbox.Forms.Items.UI.LabelPosition? labelPosition = base.LabelPosition;
                if (labelPosition.HasValue)
                {
                    return base.LabelPosition;
                }
                else
                {
                    return new Checkbox.Forms.Items.UI.LabelPosition?(Checkbox.Forms.Items.UI.LabelPosition.Top);
                }
            }
            set
            {
                base.LabelPosition = value;
            }
        }
    }
}

