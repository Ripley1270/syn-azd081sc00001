﻿namespace Checkbox.Invitations
{
    using Checkbox.Common;
    using Checkbox.Forms;
    using Checkbox.Forms.Piping;
    using Checkbox.Forms.Piping.Tokens;
    using Checkbox.Management;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text.RegularExpressions;

    public class InvitationPipeMediator : PipeMediator
    {
        private string _baseSurveyUrl;
        private List<string> _customUserFieldNames;
        private Invitation _invitation;
        private Recipient _recipient;

        public static string GetBaseSurveyUrl(int responseTemplateId)
        {
            Guid? responseTemplateGUID = ResponseTemplateManager.GetResponseTemplateGUID(responseTemplateId);
            if (!responseTemplateGUID.HasValue)
            {
                return string.Empty;
            }
            string str = ApplicationManager.ApplicationPath + "/Survey.aspx";
            string str2 = "?s=" + responseTemplateGUID.ToString().Replace("-", string.Empty);
            if (ApplicationManager.AppSettings.AllowSurveyUrlRewriting && UrlMapper.SourceMappingExists(str + str2))
            {
                string mapping = UrlMapper.GetMapping(str + str2);
                if (Utilities.IsNotNullOrEmpty(mapping))
                {
                    return mapping;
                }
            }
            return str;
        }

        public string GetRecipientSurveyUrl()
        {
            if (((this._recipient == null) || (this._invitation == null)) || (this._invitation.ParentID <= 0))
            {
                return string.Empty;
            }
            string str = "?i=" + this._recipient.GUID.ToString().Replace("-", string.Empty);
            if (Utilities.IsNullOrEmpty(this._baseSurveyUrl))
            {
                this._baseSurveyUrl = GetBaseSurveyUrl(this._invitation.ParentID);
            }
            return (this._baseSurveyUrl + str);
        }

        public string GetText(string key)
        {
            if ((key != null) && (key.Trim() != string.Empty))
            {
                string str = key;
                PipeMediator.ProcessedText textFromCache = base.GetTextFromCache(str);
                if (textFromCache != null)
                {
                    this.ProcessTokens(textFromCache.TokenValues, true);
                    return textFromCache.Text;
                }
            }
            return string.Empty;
        }

        public void Initialize(Invitation invitation, Recipient recipient, List<string> customUserFieldNames, string baseSurveyUrl)
        {
            this._invitation = invitation;
            this._recipient = recipient;
            this._baseSurveyUrl = baseSurveyUrl;
            this._customUserFieldNames = customUserFieldNames;
        }

        protected override void ProcessTokens(ReadOnlyCollection<PipeMediator.TokenValue> tokenValues, bool removeEmpty)
        {
            foreach (PipeMediator.TokenValue value2 in tokenValues)
            {
                if (value2.IsDirty)
                {
                    if (this._recipient == null)
                    {
                        value2.Value = string.Empty;
                    }
                    else if ((value2.Token.Type == TokenType.Profile) || (value2.Token.TokenName.ToLower() == (ApplicationManager.AppSettings.PipePrefix + "userguid")))
                    {
                        value2.Value = this._recipient[value2.Token.TokenName.Replace(ApplicationManager.AppSettings.PipePrefix, string.Empty)];
                    }
                    else if (value2.Token.TokenName.ToLower() == (ApplicationManager.AppSettings.PipePrefix + "invitationid"))
                    {
                        value2.Value = this._recipient.ID.ToString().Replace("-", string.Empty);
                    }
                    else if (value2.Token.TokenName.ToLower() == (ApplicationManager.AppSettings.PipePrefix + "recipientguid"))
                    {
                        value2.Value = this._recipient.GUID.ToString().Replace("-", string.Empty);
                    }
                    else if (value2.Token.TokenName.Equals(InvitationManager.SURVEY_URL_PLACEHOLDER, StringComparison.InvariantCultureIgnoreCase))
                    {
                        value2.Value = this.GetRecipientSurveyUrl();
                    }
                    else
                    {
                        value2.Value = string.Empty;
                    }
                }
                if (!removeEmpty && Utilities.IsNullOrEmpty(value2.Value))
                {
                    value2.Value = value2.Token.TokenName;
                }
            }
        }

        public void RegisterText(string key, string text)
        {
            if (Utilities.IsNotNullOrEmpty(key) && Utilities.IsNotNullOrEmpty(text))
            {
                string str = key;
                if (base.TextCacheContainsKey(str))
                {
                    base.GetTextFromCache(str).OriginalText = text;
                }
                else
                {
                    PipeMediator.ProcessedText text3 = new PipeMediator.ProcessedText {
                        OriginalText = text
                    };
                    base.CacheText(str, text3);
                    for (Match match = base.RegExp.Match(text); match.Success; match = match.NextMatch())
                    {
                        if (base.ValueCacheContainsKey(match.Value))
                        {
                            text3.AddTokenValue(base.GetValueFromCache(match.Value));
                        }
                        else
                        {
                            Token token = PipeManager.GetToken(match.Value, this.CustomUserFieldNames);
                            if (token != null)
                            {
                                base.TokenValues[match.Value] = new PipeMediator.TokenValue(token);
                                text3.AddTokenValue(base.TokenValues[match.Value]);
                            }
                        }
                    }
                }
            }
        }

        private List<string> CustomUserFieldNames
        {
            get
            {
                if (this._customUserFieldNames == null)
                {
                    this._customUserFieldNames = new List<string>();
                }
                return this._customUserFieldNames;
            }
        }
    }
}

