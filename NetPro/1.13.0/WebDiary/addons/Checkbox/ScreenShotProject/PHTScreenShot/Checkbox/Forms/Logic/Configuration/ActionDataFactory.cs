﻿namespace Checkbox.Forms.Logic.Configuration
{
    using Prezza.Framework.Common;
    using System;
    using System.Reflection;

    internal class ActionDataFactory
    {
        internal ActionData CreateActionData(string typeName, object[] constructorArgs)
        {
            ActionData data;
            try
            {
                data = this.CreateObject(constructorArgs, this.GetType(typeName));
            }
            catch (Exception)
            {
                throw;
            }
            return data;
        }

        private ActionData CreateObject(object[] args, Type type)
        {
            ArgumentValidation.CheckForNullReference(type, "type");
            this.ValidateTypeIsActionData(type);
            Type[] types = new Type[args.Length];
            for (int i = 0; i < args.Length; i++)
            {
                types[i] = args[i].GetType();
            }
            ConstructorInfo constructor = type.GetConstructor(types);
            if (constructor == null)
            {
                throw new Exception("Action does not have a constructor: " + type.FullName);
            }
            object obj2 = null;
            try
            {
                obj2 = constructor.Invoke(args);
            }
            catch (MethodAccessException exception)
            {
                throw new Exception(exception.Message, exception);
            }
            catch (TargetInvocationException exception2)
            {
                throw new Exception(exception2.Message, exception2);
            }
            catch (TargetParameterCountException exception3)
            {
                throw new Exception(exception3.Message, exception3);
            }
            catch (Exception exception4)
            {
                throw exception4;
            }
            return (ActionData) obj2;
        }

        private Type GetType(string typeName)
        {
            Type type;
            ArgumentValidation.CheckForEmptyString(typeName, "typeName");
            try
            {
                type = Type.GetType(typeName, true, false);
            }
            catch (TypeLoadException exception)
            {
                throw new Exception("A type-loading error occurred.  Type was: " + typeName, exception);
            }
            return type;
        }

        private void ValidateTypeIsActionData(Type type)
        {
            ArgumentValidation.CheckForNullReference(type, "type");
            if (!typeof(ActionData).IsAssignableFrom(type))
            {
                throw new Exception("Type mismatch between Action type [" + typeof(ActionData).AssemblyQualifiedName + "] and requested type [" + type.AssemblyQualifiedName);
            }
        }
    }
}

