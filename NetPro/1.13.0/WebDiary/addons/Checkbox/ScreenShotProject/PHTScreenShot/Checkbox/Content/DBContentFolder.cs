﻿namespace Checkbox.Content
{
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.Sprocs;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections.Generic;
    using System.Data;

    [InsertProcedure("ckbx_Content_CreateFolder"), UpdateProcedure("ckbx_Content_UpdateFolder"), DeleteProcedure("ckbx_Content_DeleteFolder"), FetchProcedure("ckbx_Content_GetFolder")]
    public class DBContentFolder : ContentFolder
    {
        private string _createdBy;
        private ExtendedPrincipal _currentUser;
        private int? _folderID;
        private int? _parentFolderID;

        public DBContentFolder(int? folderID, string folderPath, ExtendedPrincipal currentUser)
        {
            this._folderID = folderID;
            this._currentUser = currentUser;
            this.FolderPath = folderPath;
        }

        public void Delete()
        {
            StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Delete, this);
        }

        protected override void DeleteContentItem(ContentItem item)
        {
            if (item is DBContentItem)
            {
                ((DBContentItem) item).Delete();
            }
        }

        public void Load()
        {
            if (!this.FolderID.HasValue)
            {
                Database database = DatabaseFactory.CreateDatabase();
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Content_GetFolderByPath");
                storedProcCommandWrapper.AddInParameter("FolderPath", DbType.String, this.FolderPath);
                using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
                {
                    try
                    {
                        if (reader.Read() && (reader["FolderID"] != DBNull.Value))
                        {
                            this.FolderID = new int?((int) reader["FolderID"]);
                        }
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
            }
            StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Select, this);
        }

        protected override Dictionary<string, ContentFolder> LoadFolders()
        {
            return DBContentManager.ListFolders(this.FolderPath, this._currentUser);
        }

        protected override Dictionary<string, ContentItem> LoadItems()
        {
            return DBContentManager.ListItems(this.FolderPath, this._currentUser, new string[0]);
        }

        public void Save()
        {
            if (this.FolderID.HasValue)
            {
                StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Update, this);
            }
            else
            {
                StoredProcedureCommandExtractor.ExecuteProcedure(ProcedureType.Insert, this);
            }
        }

        protected override void StoreContentItem(ContentItem item)
        {
            if (item is DBContentItem)
            {
                ((DBContentItem) item).FolderID = this.FolderID;
                item.LastUpdated = DateTime.Now;
                ((DBContentItem) item).Save();
            }
        }

        [InsertParameter(Name="CreatedBy", DbType=DbType.String, Direction=ParameterDirection.Input), UpdateParameter(Name="CreatedBy", DbType=DbType.String, Direction=ParameterDirection.Input), FetchParameter(Name="CreatedBy", DbType=DbType.String, Direction=ParameterDirection.ReturnValue)]
        public string CreatedBy
        {
            get
            {
                return this._createdBy;
            }
            set
            {
                this._createdBy = value;
            }
        }

        [InsertParameter(Name="FolderID", DbType=DbType.Int32, Direction=ParameterDirection.Output, Size=4), DeleteParameter(Name="FolderID", DbType=DbType.Int32, Direction=ParameterDirection.Input), UpdateParameter(Name="FolderID", DbType=DbType.Int32, Direction=ParameterDirection.Input), FetchParameter(Name="FolderID", DbType=DbType.Int32, Direction=ParameterDirection.Input)]
        public int? FolderID
        {
            get
            {
                return this._folderID;
            }
            set
            {
                this._folderID = value;
            }
        }

        [UpdateParameter(Name="FolderName", DbType=DbType.String, Direction=ParameterDirection.Input), InsertParameter(Name="FolderName", DbType=DbType.String, Direction=ParameterDirection.Input), FetchParameter(Name="FolderName", DbType=DbType.String, Direction=ParameterDirection.ReturnValue)]
        public override string FolderName
        {
            get
            {
                return base.FolderName;
            }
            set
            {
                base.FolderName = value;
            }
        }

        [InsertParameter(Name="FolderPath", DbType=DbType.String, Direction=ParameterDirection.Input), FetchParameter(Name="FolderPath", DbType=DbType.String, Direction=ParameterDirection.ReturnValue), UpdateParameter(Name="FolderPath", DbType=DbType.String, Direction=ParameterDirection.Input)]
        public override string FolderPath
        {
            get
            {
                return base.FolderPath;
            }
            set
            {
                base.FolderPath = value;
            }
        }

        [FetchParameter(Name="IsPublic", DbType=DbType.Boolean, Direction=ParameterDirection.ReturnValue), UpdateParameter(Name="IsPublic", DbType=DbType.Boolean, Direction=ParameterDirection.Input), InsertParameter(Name="IsPublic", DbType=DbType.Boolean, Direction=ParameterDirection.Input)]
        public override bool IsPublic
        {
            get
            {
                return base.IsPublic;
            }
            set
            {
                base.IsPublic = value;
            }
        }

        [FetchParameter(Name="ParentFolderID", DbType=DbType.Int32, Direction=ParameterDirection.ReturnValue), InsertParameter(Name="ParentFolderID", DbType=DbType.Int32, Direction=ParameterDirection.Input), UpdateParameter(Name="ParentFolderID", DbType=DbType.Int32, Direction=ParameterDirection.Input)]
        public int? ParentFolderID
        {
            get
            {
                return this._parentFolderID;
            }
            set
            {
                this._parentFolderID = value;
            }
        }
    }
}

