﻿namespace Checkbox.Security.IdentityFilters
{
    using System;

    public class NullFilter : IdentityFilter
    {
        public NullFilter(string filterProperty) : base(IdentityFilterType.IsNull, IdentityFilterPropertyType.IdentityProperty, filterProperty)
        {
        }

        public NullFilter(string filterProperty, IdentityFilterPropertyType filterPropertyType) : base(IdentityFilterType.IsNull, filterPropertyType, filterProperty)
        {
        }
    }
}

