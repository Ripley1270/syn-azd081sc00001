﻿namespace Checkbox.Forms.Security
{
    using Prezza.Framework.Security;
    using System;
    using System.Collections.Generic;

    public class FormRespondentSecurityEditor : FormSecurityEditor
    {
        public FormRespondentSecurityEditor(IAccessControllable form) : base(form)
        {
        }

        public override List<IAccessControlEntry> GetAccessPermissible(params string[] permissions)
        {
            return this.GetMergedAccessPermissible(new string[] { "Form.Fill" });
        }

        public override void GrantAccess(IAccessControlEntry[] pendingEntries, params string[] permissions)
        {
            base.UpdateAccess(pendingEntries, new string[] { "Form.Fill" }, false);
        }

        public override void RemoveAccess(IAccessControlEntry[] pendingEntries)
        {
            base.UpdateAccess(pendingEntries, new string[] { "Form.Fill" }, true);
        }
    }
}

