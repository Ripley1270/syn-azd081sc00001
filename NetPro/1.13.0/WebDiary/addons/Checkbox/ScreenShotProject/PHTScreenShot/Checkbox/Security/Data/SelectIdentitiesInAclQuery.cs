﻿namespace Checkbox.Security.Data
{
    using Checkbox.Security;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security;
    using System;
    using System.Text;

    internal static class SelectIdentitiesInAclQuery
    {
        public static SelectQuery GetQuery(IAccessControlList acl)
        {
            SelectQuery query = new SelectQuery("ckbx_Credential");
            query.AddParameter("UniqueIdentifier", string.Empty, "ckbx_Credential");
            query.AddSelectDistinctCriterion();
            CriteriaCollection joinCriteria = new CriteriaCollection(CriteriaJoinType.And);
            joinCriteria.AddCriterion(new QueryCriterion(new SelectParameter("EntryIdentifier", string.Empty, "ckbx_AccessControlEntry"), CriteriaOperator.EqualTo, new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential")));
            joinCriteria.AddCriterion(new QueryCriterion(new SelectParameter("EntryType", string.Empty, "ckbx_AccessControlEntry"), CriteriaOperator.EqualTo, new LiteralParameter("'Prezza.Framework.Security.ExtendedPrincipal'")));
            query.AddTableJoin(new QueryTableJoin("ckbx_AccessControlEntry", QueryJoinType.Inner, joinCriteria, string.Empty));
            query.AddTableJoin("ckbx_AccessControlEntries", QueryJoinType.Inner, "EntryID", "ckbx_AccessControlEntry", "EntryID");
            query.AddCriterion(new QueryCriterion(new SelectParameter("AclID", string.Empty, "ckbx_AccessControlEntries"), CriteriaOperator.EqualTo, new LiteralParameter(acl.ID.ToString())));
            return query;
        }

        public static SelectQuery GetQuery(IAccessControlList acl, PermissionJoin permissionJoin, params string[] permissions)
        {
            SelectQuery query = GetQuery(acl);
            if ((permissions != null) && (permissions.Length > 0))
            {
                query.AddTableJoin("ckbx_PolicyPermissions", QueryJoinType.Inner, "PolicyID", "ckbx_AccessControlEntry", "PolicyID");
                query.AddTableJoin("ckbx_Permission", QueryJoinType.Inner, "PermissionID", "ckbx_PolicyPermissions", "PermissionID");
                if (permissionJoin == PermissionJoin.Any)
                {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < permissions.Length; i++)
                    {
                        if (i > 0)
                        {
                            builder.Append(",");
                        }
                        builder.Append("'");
                        builder.Append(permissions[i]);
                        builder.Append("'");
                    }
                    query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", string.Empty, "ckbx_Permission"), CriteriaOperator.In, new LiteralParameter(builder.ToString(), string.Empty, true)));
                    return query;
                }
                query.CriteriaJoinType = CriteriaJoinType.And;
                foreach (string str in permissions)
                {
                    query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", string.Empty, "ckbx_Permission"), CriteriaOperator.EqualTo, new LiteralParameter("'" + str + "'")));
                }
            }
            return query;
        }
    }
}

