﻿namespace Checkbox.Analytics.Export
{
    using Checkbox.Common;
    using Checkbox.Progress;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.IO;

    public static class ExportManager
    {
        public static void WriteCsvExportToFile(int responseTemplateId, ExportOptions options, string languageCode, string progressKey, string filePath)
        {
            try
            {
                SurveyDataExporter exporter = "CSV".Equals(options.ExportMode, StringComparison.InvariantCultureIgnoreCase) ? new CsvDataExporter() : new SpssCompatibleCsvDataExporter();
                exporter.Initialize(responseTemplateId, languageCode, options, progressKey);
                exporter.WriteToFile(filePath);
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessPublic");
                if (Utilities.IsNotNullOrEmpty(progressKey))
                {
                    ProgressData progressData = new ProgressData {
                        CurrentItem = 0,
                        Status = ProgressStatus.Error,
                        Message = exception.Message
                    };
                    ProgressProvider.SetProgress(progressKey, progressData);
                }
            }
        }

        public static void WriteCsvExportToTextWriter(TextWriter tw, int responseTemplateId, ExportOptions options, string languageCode, string progressKey)
        {
            try
            {
                SurveyDataExporter exporter = "CSV".Equals(options.ExportMode, StringComparison.InvariantCultureIgnoreCase) ? new CsvDataExporter() : new SpssCompatibleCsvDataExporter();
                exporter.Initialize(responseTemplateId, languageCode, options, progressKey);
                exporter.WriteToTextWriter(tw);
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessPublic");
                if (Utilities.IsNotNullOrEmpty(progressKey))
                {
                    ProgressData progressData = new ProgressData {
                        CurrentItem = 0,
                        Status = ProgressStatus.Error,
                        Message = exception.Message
                    };
                    ProgressProvider.SetProgress(progressKey, progressData);
                }
            }
        }

        public static void WriteExportToFile(SurveyDataExporter exporter, string outputFilePath, string progressKey)
        {
            try
            {
                exporter.WriteToFile(outputFilePath);
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessPublic");
                if (Utilities.IsNotNullOrEmpty(progressKey))
                {
                    ProgressData progressData = new ProgressData {
                        CurrentItem = 0,
                        Status = ProgressStatus.Error,
                        Message = exception.Message
                    };
                    ProgressProvider.SetProgress(progressKey, progressData);
                }
            }
        }

        public static void WriteExportToTextWriter(SurveyDataExporter exporter, TextWriter tw, string progressKey)
        {
            try
            {
                exporter.WriteToTextWriter(tw);
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessPublic");
                if (Utilities.IsNotNullOrEmpty(progressKey))
                {
                    ProgressData progressData = new ProgressData {
                        CurrentItem = 0,
                        Status = ProgressStatus.Error,
                        Message = exception.Message
                    };
                    ProgressProvider.SetProgress(progressKey, progressData);
                }
            }
        }

        public static void WriteNativeSpssExportToFile(int responseTemplateId, ExportOptions exportOptions, string languageCode, string progressKey, string outputFileName)
        {
            try
            {
                NativeSpssDataExporter exporter = new NativeSpssDataExporter();
                exporter.Initialize(responseTemplateId, languageCode, exportOptions, progressKey);
                exporter.WriteToFile(outputFileName);
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(exception, "BusinessPublic");
                if (Utilities.IsNotNullOrEmpty(progressKey))
                {
                    ProgressData progressData = new ProgressData {
                        CurrentItem = 0,
                        Status = ProgressStatus.Error,
                        Message = exception.Message
                    };
                    ProgressProvider.SetProgress(progressKey, progressData);
                }
            }
        }
    }
}

