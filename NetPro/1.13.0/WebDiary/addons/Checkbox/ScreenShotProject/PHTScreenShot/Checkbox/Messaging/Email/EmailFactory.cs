﻿namespace Checkbox.Messaging.Email
{
    using Checkbox.Messaging.Configuration;
    using Prezza.Framework.Configuration;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections;

    internal sealed class EmailFactory
    {
        private static readonly Hashtable _providers;

        static EmailFactory()
        {
            lock (typeof(EmailFactory))
            {
                _providers = new Hashtable();
            }
        }

        internal static IEmailProvider GetEmailProvider()
        {
            try
            {
                lock (_providers.SyncRoot)
                {
                    if (_providers.Contains("[DEFAULT]"))
                    {
                        return (IEmailProvider) _providers["[DEFAULT]"];
                    }
                }
                IEmailProvider emailProvider = new EmailProviderFactory("emailProviderFactory", (MessagingConfiguration) ConfigurationManager.GetConfiguration("messagingConfiguration")).GetEmailProvider();
                lock (_providers.SyncRoot)
                {
                    if (!_providers.Contains("[DEFAULT]"))
                    {
                        _providers.Add("[DEFAULT]", emailProvider);
                    }
                }
                return emailProvider;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessInternal"))
                {
                    throw;
                }
                return null;
            }
        }

        internal static IEmailProvider GetEmailProvider(string emailProvider)
        {
            try
            {
                lock (_providers.SyncRoot)
                {
                    if (_providers.Contains(emailProvider))
                    {
                        return (IEmailProvider) _providers[emailProvider];
                    }
                }
                IEmailProvider provider = new EmailProviderFactory("emailProviderFactory", (MessagingConfiguration) ConfigurationManager.GetConfiguration("globalizationConfiguration")).GetEmailProvider(emailProvider);
                lock (_providers.SyncRoot)
                {
                    if (!_providers.Contains(emailProvider))
                    {
                        _providers.Add(emailProvider, provider);
                    }
                }
                return provider;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessInternal"))
                {
                    throw;
                }
                return null;
            }
        }
    }
}

