﻿namespace Checkbox.Analytics.Export
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Text;

    [Serializable]
    public class ExportOptions
    {
        public DateTime? EndDate { get; set; }

        public string ExportMode { get; set; }

        public int? FileSet { get; set; }

        public bool IncludeDetailedResponseInfo { get; set; }

        public bool IncludeDetailedUserInfo { get; set; }

        public bool IncludeHidden { get; set; }

        public bool IncludeIncomplete { get; set; }

        public bool IncludeOpenEnded { get; set; }

        public bool IncludeResponseId { get; set; }

        public bool IncludeScore { get; set; }

        public bool MergeSelectMany { get; set; }

        public Encoding OutputEncoding { get; set; }

        public DateTime? StartDate { get; set; }

        public bool UseAliases { get; set; }
    }
}

