﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Security;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Xml;

    [Serializable]
    public class ProfileUpdater : ResponseItem
    {
        private List<ProfileUpdaterProperty> _props;

        public override void Configure(ItemData configuration, string languageCode)
        {
            base.Configure(configuration, languageCode);
            DataRow[] rowArray = ((ProfileUpdaterItemData) configuration).PropertyData.Select(null, null, DataViewRowState.CurrentRows);
            this._props = new List<ProfileUpdaterProperty>();
            foreach (DataRow row in rowArray)
            {
                ProfileUpdaterProperty item = new ProfileUpdaterProperty(Convert.ToInt32(row["SourceItemID"]), Convert.ToString(row["ProviderName"]), Convert.ToString(row["PropertyName"]));
                this._props.Add(item);
            }
        }

        protected override void OnPageLoad()
        {
            base.OnPageLoad();
            if ((base.Response != null) && (base.Response.Respondent != null))
            {
                Dictionary<string, IProfileProvider> dictionary = new Dictionary<string, IProfileProvider>();
                Dictionary<string, IProfile> dictionary2 = new Dictionary<string, IProfile>();
                foreach (ProfileUpdaterProperty property in this._props)
                {
                    if (!dictionary.ContainsKey(property.ProviderName))
                    {
                        IProfileProvider profileProvider = ProfileFactory.GetProfileProvider(property.ProviderName);
                        if (profileProvider != null)
                        {
                            dictionary[property.ProviderName] = profileProvider;
                            IProfile profile = profileProvider.GetProfile(base.Response.Respondent.Identity);
                            dictionary2[property.ProviderName] = profile;
                        }
                    }
                    if (dictionary2.ContainsKey(property.ProviderName) && (dictionary2[property.ProviderName] != null))
                    {
                        IAnswerable item = base.Response.GetItem(property.SourceItemID) as IAnswerable;
                        if (item != null)
                        {
                            dictionary2[property.ProviderName][property.PropertyName] = item.GetAnswer();
                        }
                    }
                }
                foreach (string str in dictionary2.Keys)
                {
                    if (dictionary.ContainsKey(str) && (dictionary[str] != null))
                    {
                        dictionary[str].SetProfile(base.Response.Respondent.Identity, dictionary2[str]);
                    }
                }
            }
        }

        public override void WriteXmlInstanceData(XmlWriter writer)
        {
            base.WriteXmlInstanceData(writer);
            writer.WriteStartElement("properties");
            foreach (ProfileUpdaterProperty property in this._props)
            {
                writer.WriteStartElement("property");
                writer.WriteElementString("name", property.PropertyName);
                writer.WriteElementString("profileProvider", property.ProviderName);
                writer.WriteElementString("sourceItem", property.SourceItemID.ToString());
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        public override bool Visible
        {
            get
            {
                return (base.Response != null);
            }
        }

        [Serializable]
        private class ProfileUpdaterProperty
        {
            public string PropertyName;
            public string ProviderName;
            public int SourceItemID;

            public ProfileUpdaterProperty(int sourceItemID, string providerName, string propertyName)
            {
                this.SourceItemID = sourceItemID;
                this.ProviderName = providerName;
                this.PropertyName = propertyName;
            }
        }
    }
}

