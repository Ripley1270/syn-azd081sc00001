﻿namespace Checkbox.Analytics.Filters
{
    using Checkbox.Common;
    using System;
    using System.Text;

    [Serializable]
    public class NotEqualFilter : ItemQueryFilter
    {
        protected override string GetValueFilterClause()
        {
            int? nullable = null;
            if (base.Value != null)
            {
                nullable = Utilities.AsInt(base.Value.ToString());
            }
            StringBuilder builder = new StringBuilder();
            if (nullable.HasValue)
            {
                builder.Append(" (OptionID IS NULL AND AnswerText NOT Like '");
                builder.Append(this.GetEscapedValueString());
                builder.Append("') OR (");
                builder.Append(" OptionID IS NOT NULL AND OptionID <> ");
                builder.Append(this.GetEscapedValueString());
                builder.Append(")");
            }
            else
            {
                builder.Append(" (OptionID IS NULL AND AnswerText NOT Like '");
                builder.Append(this.GetEscapedValueString());
                builder.Append("')");
            }
            return builder.ToString();
        }

        public override bool UseNotIn
        {
            get
            {
                return false;
            }
        }
    }
}

