﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class DisplayResponseItemData : LocalizableResponseItemData
    {
        public DisplayResponseItemData()
        {
            this.DisplayInlineResponse = true;
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified for Create()");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertDR");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("DisplayInline", DbType.Int32, this.DisplayInlineResponse);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new DisplayResponseItem();
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new DisplayResponseItemTextDecorator(this, languageCode);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetDR", new object[] { base.ID });
            DataSet concreteConfigurationDataSet = base.GetConcreteConfigurationDataSet();
            database.LoadDataSet(storedProcCommandWrapper, concreteConfigurationDataSet, this.DataTableName);
            return concreteConfigurationDataSet;
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            this.DisplayInlineResponse = DbUtility.GetValueFromDataRow<bool>(data, "DisplayInline", false);
        }

        public override DataSet LoadTextData()
        {
            DataSet ds = base.LoadTextData();
            this.MergeTextData(ds, TextManager.GetTextData(this.LinkTextID), this.TextTableName, "linkText", this.TextIDPrefix, base.ID.Value);
            return ds;
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified for Update()");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateDR");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("DisplayInline", DbType.Int32, this.DisplayInlineResponse);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public override string DataTableName
        {
            get
            {
                return "DisplayResponseItemData";
            }
        }

        public bool DisplayInlineResponse { get; set; }

        public string LinkTextID
        {
            get
            {
                return this.GetTextID("linkText");
            }
        }

        public override string TextIDPrefix
        {
            get
            {
                return "displayResponseItemData";
            }
        }
    }
}

