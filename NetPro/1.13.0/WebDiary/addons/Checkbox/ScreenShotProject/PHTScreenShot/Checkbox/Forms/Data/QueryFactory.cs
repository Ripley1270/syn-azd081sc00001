﻿namespace Checkbox.Forms.Data
{
    using Checkbox.Common;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.Security.Principal;
    using System;

    internal static class QueryFactory
    {
        private static QueryCriterion CreateFilterCriterion(string filterField, string filterValue, string tableName)
        {
            return new QueryCriterion(new SelectParameter(filterField, string.Empty, tableName), CriteriaOperator.Like, new LiteralParameter("'%" + filterValue + "%'"));
        }

        private static CriteriaCollection CreateNotDeletedCriteriaCollection(string tableName, string deletedFieldName)
        {
            CriteriaCollection criterias = new CriteriaCollection(CriteriaJoinType.Or);
            criterias.AddCriterion(new QueryCriterion(new SelectParameter(deletedFieldName, string.Empty, tableName), CriteriaOperator.Is, new LiteralParameter("NULL")));
            criterias.AddCriterion(new QueryCriterion(new SelectParameter(deletedFieldName, string.Empty, tableName), CriteriaOperator.EqualTo, new LiteralParameter(0)));
            return criterias;
        }

        public static SelectQuery GetAllActiveResponseTemplatesQuery(string filterField, string filterValue, string sortField, bool sortDescending)
        {
            SelectQuery query = GetAllResponseTemplatesQuery(filterField, filterValue, sortField, sortDescending);
            query.AddCriterion(new QueryCriterion(new SelectParameter("IsActive"), CriteriaOperator.IsNot, new LiteralParameter("NULL")));
            query.AddCriterion(new QueryCriterion(new SelectParameter("IsActive"), CriteriaOperator.NotEqualTo, new LiteralParameter(0)));
            return query;
        }

        public static SelectQuery GetAllFormFoldersQuery()
        {
            SelectQuery query = new SelectQuery("ckbx_Folder");
            query.AddAllParameter("ckbx_Folder");
            return query;
        }

        public static SelectQuery GetAllLibraryTemplatesQuery()
        {
            SelectQuery query = new SelectQuery("ckbx_LibraryTemplate");
            query.AddParameter("LibraryTemplateID", string.Empty, "ckbx_LibraryTemplate");
            query.AddParameter("NameTextID", string.Empty, "ckbx_LibraryTemplate");
            query.AddParameter("DescriptionTextID", string.Empty, "ckbx_LibraryTemplate");
            query.AddTableJoin("ckbx_Template", QueryJoinType.Inner, "TemplateID", "ckbx_LibraryTemplate", "LibraryTemplateID");
            query.AddCriteriaCollection(CreateNotDeletedCriteriaCollection("ckbx_Template", "Deleted"));
            return query;
        }

        public static SelectQuery GetAllLibraryTemplatesQuery(string filterField, string filterValue, string sortField, bool sortAscending)
        {
            SelectQuery allLibraryTemplatesQuery = GetAllLibraryTemplatesQuery();
            allLibraryTemplatesQuery.AddTableJoin("ckbx_Template_Items", QueryJoinType.Left, "TemplateID", "ckbx_Template", "TemplateID");
            allLibraryTemplatesQuery.AddCountParameter("ItemID", "ItemCount");
            allLibraryTemplatesQuery.IsGrouped = true;
            if (Utilities.IsNotNullOrEmpty(sortField))
            {
                allLibraryTemplatesQuery.AddSortField(new SortOrder(sortField, sortAscending));
            }
            if (Utilities.IsNotNullOrEmpty(filterField) && Utilities.IsNotNullOrEmpty(filterValue))
            {
                allLibraryTemplatesQuery.AddCriterion(CreateFilterCriterion(filterField, filterValue, "ckbx_LibraryTemplate"));
            }
            return allLibraryTemplatesQuery;
        }

        public static SelectQuery GetAllResponseTemplatesQuery()
        {
            SelectQuery allTemplatesAndFoldersQuery = GetAllTemplatesAndFoldersQuery();
            allTemplatesAndFoldersQuery.AddCriterion(new QueryCriterion(new SelectParameter("ItemType"), CriteriaOperator.EqualTo, new LiteralParameter("'Form'")));
            return allTemplatesAndFoldersQuery;
        }

        public static SelectQuery GetAllResponseTemplatesQuery(string filterField, string filterValue, string sortField, bool sortDescending)
        {
            SelectQuery allResponseTemplatesQuery = GetAllResponseTemplatesQuery();
            if (Utilities.IsNotNullOrEmpty(filterField) && Utilities.IsNotNullOrEmpty(filterValue))
            {
                allResponseTemplatesQuery.AddCriterion(CreateFilterCriterion(filterField, filterValue, "ckbx_TemplatesAndFoldersView"));
            }
            if (Utilities.IsNotNullOrEmpty(sortField))
            {
                allResponseTemplatesQuery.AddSortField(new SortOrder(sortField, !sortDescending));
            }
            return allResponseTemplatesQuery;
        }

        public static SelectQuery GetAllTemplatesAndFoldersQuery()
        {
            SelectQuery query = new SelectQuery("ckbx_TemplatesAndFoldersView");
            query.AddAllParameter("ckbx_TemplatesAndFoldersView");
            return query;
        }

        public static SelectQuery GetAllTemplatesAndFoldersQuery(string filterField, string filterValue, string sortField, bool sortDescending)
        {
            SelectQuery allTemplatesAndFoldersQuery = GetAllTemplatesAndFoldersQuery();
            if (Utilities.IsNotNullOrEmpty(filterField) && Utilities.IsNotNullOrEmpty(filterValue))
            {
                allTemplatesAndFoldersQuery.AddCriterion(new QueryCriterion(new SelectParameter(filterField), CriteriaOperator.Like, new LiteralParameter("'%" + filterValue + "%'")));
            }
            if (Utilities.IsNotNullOrEmpty(sortField))
            {
                allTemplatesAndFoldersQuery.AddSortField(new SortOrder(sortField, !sortDescending));
            }
            return allTemplatesAndFoldersQuery;
        }

        public static SelectQuery GetAllTemplatesAndFoldersQuery(string filterField, string filterValue, string sortField, bool sortDescending, int? parentFolderId)
        {
            bool flag = false;
            SelectQuery allTemplatesAndFoldersQuery = GetAllTemplatesAndFoldersQuery();
            if (Utilities.IsNotNullOrEmpty(filterField) && Utilities.IsNotNullOrEmpty(filterValue))
            {
                allTemplatesAndFoldersQuery.AddCriterion(new QueryCriterion(new SelectParameter(filterField), CriteriaOperator.Like, new LiteralParameter("'%" + filterValue + "%'")));
                flag = true;
            }
            if (parentFolderId.HasValue)
            {
                allTemplatesAndFoldersQuery.AddCriterion(new QueryCriterion(new SelectParameter("AncestorID"), CriteriaOperator.EqualTo, new LiteralParameter(parentFolderId.ToString())));
            }
            else if (!flag)
            {
                allTemplatesAndFoldersQuery.AddCriterion(new QueryCriterion(new SelectParameter("AncestorID"), CriteriaOperator.Is, new LiteralParameter("NULL")));
            }
            if (Utilities.IsNotNullOrEmpty(sortField))
            {
                allTemplatesAndFoldersQuery.AddSortField(new SortOrder(sortField, !sortDescending));
            }
            return allTemplatesAndFoldersQuery;
        }

        public static SelectQuery GetSelectAvailableFormFoldersQuery(ExtendedPrincipal currentPrincipal, params string[] permissions)
        {
            ArgumentValidation.CheckForNullReference(currentPrincipal, "currentPrincipal");
            if (currentPrincipal.IsInRole("System Administrator"))
            {
                return GetAllFormFoldersQuery();
            }
            return SelectAvailableFormFoldersQuery.GetQuery(currentPrincipal.AclTypeIdentifier, currentPrincipal.AclEntryIdentifier);
        }

        public static SelectQuery GetSelectAvailableFormFoldersQuery(ExtendedPrincipal currentPrincipal, string sortProperty, bool sortAscending, params string[] permissions)
        {
            SelectQuery selectAvailableFormFoldersQuery = GetSelectAvailableFormFoldersQuery(currentPrincipal, permissions);
            selectAvailableFormFoldersQuery.AddSortField(new SortOrder(sortProperty, sortAscending));
            return selectAvailableFormFoldersQuery;
        }
    }
}

