﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Common;
    using Checkbox.Forms.Items;
    using Checkbox.Globalization.Text;
    using Checkbox.Management;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Data;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ImageItemData : LocalizableResponseItemData
    {
        private string _contentType;
        private Guid? _guid;
        private byte[] _imageData;
        private string _imageName;

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified for Create()");
            }
            this.SaveImageData(t);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertImage");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ImagePath", DbType.String, this.ImagePath);
            storedProcCommandWrapper.AddInParameter("ImageID", DbType.Int32, this.ImageID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new ImageItem();
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new ImageItemTextDecorator(this, languageCode);
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetImage");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            DataSet dataSet = new DataSet();
            database.LoadDataSet(storedProcCommandWrapper, dataSet, this.DataTableName);
            return dataSet;
        }

        public override void Import(DataSet ds, bool doNotNegateRelationalIds)
        {
            if (ds.Tables.Contains(this.DataTableName))
            {
                foreach (DataRow row in ds.Tables[this.DataTableName].Select("ItemID = " + base.ID, null, DataViewRowState.CurrentRows))
                {
                    if ((row["ImageID"] != DBNull.Value) && (((int) row["ImageID"]) > 0))
                    {
                        row["ImageID"] = ((int) row["ImageID"]) * -1;
                    }
                }
            }
            base.Import(ds, doNotNegateRelationalIds);
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("DataRow cannot be null.");
            }
            try
            {
                this.ImagePath = DbUtility.GetValueFromDataRow<string>(data, "ImagePath", string.Empty);
                this.ImageID = DbUtility.GetValueFromDataRow<int?>(data, "ImageId", null);
                this._imageData = DbUtility.GetValueFromDataRow<byte[]>(data, "ImageData", null);
                this._imageName = DbUtility.GetValueFromDataRow<string>(data, "ImageName", string.Empty);
                this._guid = DbUtility.GetValueFromDataRow<Guid?>(data, "Guid", null);
                this._contentType = DbUtility.GetValueFromDataRow<string>(data, "ContentType", string.Empty);
                if (Utilities.IsNullOrEmpty(this.ImagePath) && (this._imageData != null))
                {
                    this.ImagePath = ApplicationManager.ApplicationRoot + "/ViewImage.aspx?ImageID=" + this.ImageID;
                }
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
            }
        }

        public override DataSet LoadTextData()
        {
            DataSet ds = base.LoadTextData();
            this.MergeTextData(ds, TextManager.GetTextData(this.AlternateTextID), this.TextTableName, "altText", this.TextIDPrefix, base.ID.Value);
            return ds;
        }

        protected virtual void SaveImageData(IDbTransaction t)
        {
            if (this.ImageID.HasValue && (this.ImageID.Value < 0))
            {
                string guid = this._guid.HasValue ? this._guid.ToString() : Guid.NewGuid().ToString();
                if (this._imageData != null)
                {
                    this.ImageID = new int?(DbUtility.SaveImage(this._imageData, this._contentType, null, this._imageName, guid));
                    this.ImagePath = ApplicationManager.ApplicationRoot + "/ViewImage.aspx?ImageID=" + this.ImageID;
                }
                else if (Utilities.IsNotNullOrEmpty(this.ImagePath))
                {
                    this.ImageID = new int?(DbUtility.SaveImage(null, this._contentType, this.ImagePath, this._imageName, guid));
                }
            }
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new ApplicationException("No DataID specified for Update()");
            }
            this.SaveImageData(t);
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateImage");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("ImagePath", DbType.String, this.ImagePath);
            storedProcCommandWrapper.AddInParameter("ImageID", DbType.Int32, this.ImageID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        public string AlternateTextID
        {
            get
            {
                return this.GetTextID("altText");
            }
        }

        public override string DataTableName
        {
            get
            {
                return "ImageItemData";
            }
        }

        public int? ImageID { get; set; }

        public string ImagePath { get; set; }

        public override string TextIDPrefix
        {
            get
            {
                return "imageItemData";
            }
        }
    }
}

