﻿namespace Checkbox.Security.Data
{
    using Checkbox.Management;
    using Checkbox.Security;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using System;

    internal static class SelectEntitiesWithPermissionQuery
    {
        internal static SelectQuery GetQuery(string entityTable, string defaultPolicyIDColumn, string permission)
        {
            return GetQuery(entityTable, defaultPolicyIDColumn, PermissionJoin.Any, new string[] { permission });
        }

        internal static SelectQuery GetQuery(string entityTable, string defaultPolicyIDColumn, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = new SelectQuery(entityTable);
            query.AddAllParameter(entityTable);
            SelectQuery subQuery = SelectDefaultPolicyWithPermission.GetQuery(entityTable, defaultPolicyIDColumn, permissionJoinType, permissions);
            query.AddCriterion(new QueryCriterion(new SelectParameter(defaultPolicyIDColumn), CriteriaOperator.In, new SubqueryParameter(subQuery)));
            return query;
        }

        internal static SelectQuery GetQuery(string entityTable, string entityAclIDColumn, string aclEntryType, string aclEntryTypeID, string permission)
        {
            return GetQuery(entityTable, entityAclIDColumn, aclEntryType, aclEntryTypeID, PermissionJoin.Any, new string[] { permission });
        }

        internal static SelectQuery GetQuery(string entityTable, string entityAclIDColumn, string aclEntryType, string aclEntryTypeID, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = new SelectQuery(entityTable);
            query.AddAllParameter(entityTable);
            SelectQuery subQuery = SelectACLWithPermission.GetQuery(entityTable, entityAclIDColumn, aclEntryType, aclEntryTypeID, permissionJoinType, permissions);
            query.AddCriterion(new QueryCriterion(new SelectParameter(entityAclIDColumn, string.Empty, entityTable), CriteriaOperator.In, new SubqueryParameter(subQuery)));
            return query;
        }

        internal static SelectQuery GetQuery(string entityTable, string entityAclIDColumn, string entityDefaultPolicyIDColumn, string aclEntryType, string aclEntryTypeID, string permission)
        {
            return GetQuery(entityTable, entityAclIDColumn, entityDefaultPolicyIDColumn, aclEntryType, aclEntryTypeID, PermissionJoin.Any, new string[] { permission });
        }

        internal static SelectQuery GetQuery(string entityTable, string entityAclIDColumn, string entityDefaultPolicyIDColumn, string aclEntryType, string aclEntryTypeID, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = new SelectQuery(entityTable);
            query.AddAllParameter(entityTable);
            SelectQuery subQuery = SelectDefaultPolicyWithPermission.GetQuery(entityTable, entityDefaultPolicyIDColumn, permissionJoinType, permissions);
            SelectQuery query3 = SelectACLWithPermission.GetQuery(entityTable, entityAclIDColumn, aclEntryType, aclEntryTypeID, permissionJoinType, permissions);
            SelectQuery query4 = SelectACLWithoutPermission.GetQuery(entityTable, entityAclIDColumn, aclEntryType, aclEntryTypeID, permissionJoinType, permissions);
            SelectQuery query5 = SelectACLWithGroupMembershipPermission.GetQuery(entityTable, entityAclIDColumn, aclEntryTypeID, permissionJoinType, permissions);
            SelectQuery query6 = SelectACLWithNoGroupMembershipPermission.GetQuery(entityTable, entityAclIDColumn, aclEntryType, aclEntryTypeID, permissionJoinType, permissions);
            SelectQuery query7 = SelectACLWithEveryoneGroupPermission.GetQuery(entityTable, entityAclIDColumn, permissionJoinType, permissions);
            CriteriaCollection criteria = new CriteriaCollection(CriteriaJoinType.Or);
            CriteriaCollection collection = new CriteriaCollection(CriteriaJoinType.And);
            CriteriaCollection criterias3 = new CriteriaCollection(CriteriaJoinType.Or);
            criterias3.AddCriterion(new QueryCriterion(new SelectParameter(entityDefaultPolicyIDColumn, string.Empty, entityTable), CriteriaOperator.In, new SubqueryParameter(subQuery)));
            criterias3.AddCriterion(new QueryCriterion(new SelectParameter(entityAclIDColumn, string.Empty, entityTable), CriteriaOperator.In, new SubqueryParameter(query5)));
            criterias3.AddCriterion(new QueryCriterion(new SelectParameter(entityAclIDColumn, string.Empty, entityTable), CriteriaOperator.In, new SubqueryParameter(query7)));
            collection.AddCriterion(new QueryCriterion(new SelectParameter(entityAclIDColumn, string.Empty, entityTable), CriteriaOperator.NotIn, new SubqueryParameter(query4)));
            collection.AddCriterion(new QueryCriterion(new SelectParameter(entityAclIDColumn, string.Empty, entityTable), CriteriaOperator.NotIn, new SubqueryParameter(query6)));
            collection.AddCriteriaCollection(criterias3);
            criteria.AddCriterion(new QueryCriterion(new SelectParameter(entityAclIDColumn, string.Empty, entityTable), CriteriaOperator.In, new SubqueryParameter(query3)));
            if (ApplicationManager.AppSettings.AllowExclusionaryAclEntries)
            {
                criteria.AddCriteriaCollection(collection);
            }
            query.AddCriteriaCollection(criteria);
            return query;
        }
    }
}

