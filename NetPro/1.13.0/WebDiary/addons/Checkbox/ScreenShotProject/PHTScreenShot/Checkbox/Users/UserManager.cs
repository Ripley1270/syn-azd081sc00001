﻿namespace Checkbox.Users
{
    using Checkbox.Common;
    using Checkbox.Management;
    using Checkbox.Security;
    using Checkbox.Security.IdentityFilters;
    using Checkbox.Users.Data;
    using Prezza.Framework.Caching;
    using Prezza.Framework.Caching.Expirations;
    using Prezza.Framework.Common;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.ExceptionHandling;
    using Prezza.Framework.Logging;
    using Prezza.Framework.Security;
    using Prezza.Framework.Security.Principal;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Security.Principal;
    using System.Text;

    public static class UserManager
    {
        private static CacheManager _loginCacheManager;
        private static ISecurityCacheProvider _securityCache;
        private static ISessionTokenProvider _sessionTokenProvider;
        private static List<FileInfo> _tourMessages;
        public const string ERROR_USER_NOT_UNIQUE = "USERNOTUNIQUE";

        public static void AddCustomUserField(string fieldName)
        {
            AddCustomUserField(fieldName, true, false);
        }

        public static void AddCustomUserField(string fieldName, bool isDeletable, bool isHidden)
        {
            if (!string.IsNullOrEmpty(fieldName))
            {
                Database database = DatabaseFactory.CreateDatabase();
                using (IDbConnection connection = database.GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Profile_CreateProperty");
                        storedProcCommandWrapper.AddInParameter("CustomUserFieldName", DbType.String, fieldName);
                        storedProcCommandWrapper.AddInParameter("isDeletable", DbType.Boolean, isDeletable);
                        storedProcCommandWrapper.AddInParameter("Hidden", DbType.Boolean, isHidden);
                        DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper("IF EXISTS (select * from sysobjects where name = 'ckbx_IdentityProfile' and type = 'V') DROP VIEW ckbx_IdentityProfile");
                        DBCommandWrapper command = database.GetSqlStringCommandWrapper(BuildIdentityProfileViewString(fieldName, true));
                        database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                        database.ExecuteNonQuery(sqlStringCommandWrapper, transaction);
                        database.ExecuteNonQuery(command, transaction);
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return;
                }
            }
            Logger.Write("Unable to add a custom user field with a null or blank name.", "Warning", 3, -1, Severity.Warning);
        }

        private static IdentityFilterCollection AddIdentityPropertyFilterToCollection(string filterField, string filterValue, IdentityFilterCollection collection)
        {
            LikeFilter filter = CreateIdentityPropertyFilter(filterField, filterValue);
            if (filter != null)
            {
                if (collection == null)
                {
                    collection = new IdentityFilterCollection(IdentityFilterJoin.And);
                }
                collection.AddFilter(filter);
            }
            return collection;
        }

        public static IPrincipal AuthenticateDomainUser(string userName, string domain)
        {
            try
            {
                if (Utilities.IsNullOrEmpty(userName))
                {
                    return null;
                }
                string name = userName;
                if (Utilities.IsNotNullOrEmpty(domain))
                {
                    name = domain + "/" + userName;
                }
                ExtendedPrincipal principal = (ExtendedPrincipal) AuthenticateUser(name, null);
                if ((principal == null) && !ApplicationManager.AppSettings.AllowUnAuthenticatedNetworkUsers)
                {
                    return null;
                }
                if (((ApplicationManager.AppSettings.ConcurrentLoginMode == ConcurrentLoginMode.NotAllowed) && (_loginCacheManager != null)) && _loginCacheManager.Contains(name))
                {
                    Logger.Write("Concurrent login attempt failed for user [" + name + "].", "Warning", 3, -1, Severity.Warning);
                    return null;
                }
                if (principal == null)
                {
                    IIdentity identity = new GenericIdentity(name, "IISAuthenticated");
                    principal = new ExtendedPrincipal(identity, RoleManager.GetIdentityRoleNames(identity).ToArray());
                }
                else
                {
                    IIdentity identity2 = new GenericIdentity(name, principal.Identity.AuthenticationType + ",IISAuthenticated");
                    principal = new ExtendedPrincipal(identity2, RoleManager.GetIdentityRoleNames(identity2).ToArray());
                }
                if (_sessionTokenProvider != null)
                {
                    IToken token = CacheUser(principal);
                    _sessionTokenProvider.SetSessionToken(token);
                    IProfile profile = GetProfile(principal.Identity);
                    if (profile != null)
                    {
                        CacheProfile(profile, token);
                    }
                }
                return principal;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                if (userName != null)
                {
                    Logger.Write("An exception occurred while attempting to authenticate a domain user with userName [" + userName + "].  As dictated by the exception policy, the error was not rethrown.", "Warning");
                }
                else
                {
                    Logger.Write("An exception occurred while attempting to authenticate a domain user with userName [" + userName + "].  As dictated by the exception policy, the error was not rethrown.", "Warning");
                }
                return null;
            }
        }

        public static IPrincipal AuthenticateLDAPUser(string name, string password)
        {
            try
            {
                IIdentity identity;
                ExtendedPrincipal principal = (ExtendedPrincipal) AuthenticateUser(name, password);
                if (principal != null)
                {
                    return principal;
                }
                if (((ApplicationManager.AppSettings.ConcurrentLoginMode == ConcurrentLoginMode.NotAllowed) && (_loginCacheManager != null)) && _loginCacheManager.Contains(name))
                {
                    Logger.Write("Concurrent login attempt failed for user [" + name + "].", "Warning", 3, -1, Severity.Warning);
                    return null;
                }
                if (_sessionTokenProvider != null)
                {
                    IToken sessionToken = _sessionTokenProvider.GetSessionToken();
                    if (sessionToken != null)
                    {
                        ExpireUser(sessionToken);
                    }
                    _sessionTokenProvider.ClearSessionToken();
                }
                if (Utilities.IsNullOrEmpty(name))
                {
                    return null;
                }
                if (password == null)
                {
                    password = string.Empty;
                }
                IAuthenticationProvider authenticationProvider = AuthenticationFactory.GetAuthenticationProvider("LDAPAuthenticationProvider");
                NamePasswordCredential credentials = new NamePasswordCredential(name, password);
                if (!authenticationProvider.Authenticate(credentials, out identity))
                {
                    return null;
                }
                if ((IdentityFactory.GetIdentityProvider().GetIdentity(identity.Name) == null) && !ApplicationManager.AppSettings.AllowUnAuthenticatedNetworkUsers)
                {
                    return null;
                }
                ExtendedPrincipal principal2 = new ExtendedPrincipal(identity, RoleManager.GetIdentityRoleNames(identity).ToArray());
                IToken token2 = null;
                if (_sessionTokenProvider != null)
                {
                    token2 = CacheUser(principal2);
                    _sessionTokenProvider.SetSessionToken(token2);
                    IProfile profile = GetProfile(principal2.Identity);
                    if (profile != null)
                    {
                        CacheProfile(profile, token2);
                    }
                }
                if ((((_loginCacheManager != null) && (token2 != null)) && ((ApplicationManager.AppSettings.ConcurrentLoginMode == ConcurrentLoginMode.LogoutCurrent) && (_loginCacheManager != null))) && _loginCacheManager.Contains(name))
                {
                    Logger.Write("Logging out user [" + name + "] due to concurrent login.", "Warning", 3, -1, Severity.Warning);
                    UserLoginInfo info = (UserLoginInfo) _loginCacheManager[name];
                    ExpireUser(info.UserToken);
                }
                return principal2;
            }
            catch (Exception exception)
            {
                if (!exception.Message.StartsWith("Logon failure: unknown user name or bad password."))
                {
                    if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                    {
                        throw;
                    }
                    Logger.Write("An exception occurred while attempting to authenticate a user with name [" + name + "].  As dictated by the exception policy, the error was not rethrown.", "Warning");
                }
                return null;
            }
        }

        public static IPrincipal AuthenticateUser(string guid)
        {
            try
            {
                IIdentity identity;
                IAuthenticationProvider authenticationProvider = AuthenticationFactory.GetAuthenticationProvider("GuidAuthenticationProvider");
                GuidCredential credentials = new GuidCredential(guid);
                if (_sessionTokenProvider != null)
                {
                    IToken sessionToken = _sessionTokenProvider.GetSessionToken();
                    if (sessionToken != null)
                    {
                        ExpireUser(sessionToken);
                    }
                    _sessionTokenProvider.ClearSessionToken();
                }
                if (!authenticationProvider.Authenticate(credentials, out identity))
                {
                    return null;
                }
                if (((ApplicationManager.AppSettings.ConcurrentLoginMode == ConcurrentLoginMode.NotAllowed) && (_loginCacheManager != null)) && _loginCacheManager.Contains(identity.Name))
                {
                    Logger.Write("Concurrent login attempt failed for user [" + identity.Name + "].", "Warning", 3, -1, Severity.Warning);
                    return null;
                }
                ExtendedPrincipal user = (ExtendedPrincipal) GetUser(identity);
                if (user == null)
                {
                    return null;
                }
                IToken token2 = null;
                if (_sessionTokenProvider != null)
                {
                    token2 = CacheUser(user);
                    _sessionTokenProvider.SetSessionToken(token2);
                    IProfile profile = GetProfile(user.Identity);
                    if (profile != null)
                    {
                        CacheProfile(profile, token2);
                    }
                }
                if ((((_loginCacheManager != null) && (token2 != null)) && ((ApplicationManager.AppSettings.ConcurrentLoginMode == ConcurrentLoginMode.LogoutCurrent) && (_loginCacheManager != null))) && _loginCacheManager.Contains(user.Identity.Name))
                {
                    Logger.Write("Logging out user [" + user.Identity.Name + "] due to concurrent login.", "Warning", 3, -1, Severity.Warning);
                    UserLoginInfo info = (UserLoginInfo) _loginCacheManager[user.Identity.Name];
                    ExpireUser(info.UserToken);
                }
                return user;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                Logger.Write("An exception occurred while attempting to authenticate a user with guid [" + guid + "].  As dictated by the exception policy, the error was not rethrown.", "Warning");
                return null;
            }
        }

        public static IPrincipal AuthenticateUser(string name, string password)
        {
            try
            {
                IIdentity identity;
                bool flag = password == null;
                if ((name.Split(new char[] { '/' }).Length > 1) && (password != null))
                {
                    return null;
                }
                if (((ApplicationManager.AppSettings.ConcurrentLoginMode == ConcurrentLoginMode.NotAllowed) && (_loginCacheManager != null)) && _loginCacheManager.Contains(name))
                {
                    Logger.Write("Concurrent login attempt failed for user [" + name + "].", "Warning", 3, -1, Severity.Warning);
                    return null;
                }
                if (_sessionTokenProvider != null)
                {
                    IToken sessionToken = _sessionTokenProvider.GetSessionToken();
                    if (sessionToken != null)
                    {
                        ExpireUser(sessionToken);
                    }
                    _sessionTokenProvider.ClearSessionToken();
                }
                if (Utilities.IsNullOrEmpty(name))
                {
                    return null;
                }
                if (password == null)
                {
                    password = string.Empty;
                }
                string plaintext = password;
                if (ApplicationManager.AppSettings.UseEncryption)
                {
                    password = Encryption.HashString(password);
                }
                IAuthenticationProvider authenticationProvider = AuthenticationFactory.GetAuthenticationProvider("NamePasswordAuthenticationProvider");
                NamePasswordCredential credentials = new NamePasswordCredential(name, password);
                bool flag2 = authenticationProvider.Authenticate(credentials, out identity);
                if (!flag2 && ApplicationManager.AppSettings.UseEncryption)
                {
                    plaintext = Encryption.HashStringDotNetOneFormat(plaintext);
                    NamePasswordCredential credential2 = new NamePasswordCredential(name, plaintext);
                    flag2 = authenticationProvider.Authenticate(credential2, out identity);
                }
                if (!flag2)
                {
                    return null;
                }
                ExtendedPrincipal user = (ExtendedPrincipal) GetUser(identity);
                if (user == null)
                {
                    return null;
                }
                IToken token2 = null;
                if (_sessionTokenProvider != null)
                {
                    token2 = CacheUser(user);
                    _sessionTokenProvider.SetSessionToken(token2);
                    IProfile profile = GetProfile(user.Identity);
                    if (profile != null)
                    {
                        CacheProfile(profile, token2);
                    }
                }
                if ((((_loginCacheManager != null) && (token2 != null)) && ((ApplicationManager.AppSettings.ConcurrentLoginMode == ConcurrentLoginMode.LogoutCurrent) && !flag)) && ((_loginCacheManager != null) && _loginCacheManager.Contains(name)))
                {
                    Logger.Write("Logging out user [" + name + "] due to concurrent login.", "Warning", 3, -1, Severity.Warning);
                    UserLoginInfo info = (UserLoginInfo) _loginCacheManager[name];
                    if (info != null)
                    {
                        ExpireUser(info.UserToken);
                    }
                }
                return user;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessPublic"))
                {
                    throw;
                }
                Logger.Write("An exception occurred while attempting to authenticate a user with name [" + name + "].  As dictated by the exception policy, the error was not rethrown.", "Warning");
                return null;
            }
        }

        private static string BuildIdentityProfileViewString(string propName, bool add)
        {
            StringBuilder builder = new StringBuilder();
            object[] profilePropertyList = GetProfilePropertyList();
            builder.Append("CREATE VIEW ckbx_IdentityProfile AS SELECT DISTINCT ckbx_Credential.UniqueIdentifier,ckbx_Credential.UserName,ckbx_Credential.Domain,ckbx_Credential.GUID,ckbx_Credential.Password");
            ArrayList list = (profilePropertyList != null) ? new ArrayList(profilePropertyList) : new ArrayList();
            if (add && Utilities.IsNotNullOrEmpty(propName))
            {
                list.Add(propName);
            }
            else if (!add && Utilities.IsNotNullOrEmpty(propName))
            {
                list.Remove(propName);
            }
            for (int i = 0; i < list.Count; i++)
            {
                builder.Append(string.Concat(new object[] { ",(SELECT Value From ckbx_CustomUserFieldMap INNER JOIN ckbx_CustomUserField on ckbx_CustomUserField.CustomUserFieldID = ckbx_CustomUserFieldMap.CustomUserFieldID WHERE ckbx_CustomUserField.CustomUserFieldName = '", new LiteralParameter(list[i], string.Empty, false, false), "' AND ckbx_Credential.UniqueIdentifier = ckbx_CustomUserFieldMap.UniqueIdentifier) AS '", new LiteralParameter(list[i], string.Empty, false, false), "'" }));
            }
            builder.Append(" FROM ckbx_Credential LEFT OUTER JOIN ckbx_CustomUserFieldMap ON ckbx_CustomUserFieldMap.UniqueIdentifier = ckbx_Credential.UniqueIdentifier LEFT OUTER JOIN ckbx_CustomUserField ON ckbx_CustomUserField.CustomUserFieldID = ckbx_CustomUserFieldMap.CustomUserFieldID");
            return builder.ToString();
        }

        public static IToken CacheProfile(IProfile profile)
        {
            return _securityCache.SaveProfile(profile);
        }

        public static void CacheProfile(IProfile profile, IToken token)
        {
            _securityCache.SaveProfile(profile, token);
        }

        public static IToken CacheUser(IPrincipal principal)
        {
            return _securityCache.SavePrincipal(principal);
        }

        public static void CacheUser(IPrincipal principal, IToken token)
        {
            _securityCache.SavePrincipal(principal, token);
        }

        public static bool CanCurrentPrincipalEditUser(string uniqueIdentifier)
        {
            if (!Utilities.IsNullOrEmpty(uniqueIdentifier))
            {
                ExtendedPrincipal currentPrincipal = GetCurrentPrincipal() as ExtendedPrincipal;
                if (currentPrincipal == null)
                {
                    return false;
                }
                if (AuthorizationFactory.GetAuthorizationProvider().Authorize(currentPrincipal, Group.Everyone, "Group.ManageUsers"))
                {
                    return true;
                }
                Group[] groupArray = Group.GetGroups(currentPrincipal, PermissionJoin.Any, new string[] { "Group.ManageUsers" });
                if (groupArray.Length == 0)
                {
                    return false;
                }
                List<int> groupMembershipIds = Group.GetGroupMembershipIds(GetUserIdentity(uniqueIdentifier));
                foreach (Group group in groupArray)
                {
                    if (groupMembershipIds.Contains(group.ID.Value))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static CriteriaCollection CreateIdentitiesFilterCollection(IIdentity[] identities, bool inOperator)
        {
            StringBuilder builder = new StringBuilder(identities.Length * 5);
            if (identities.Length == 0)
            {
                builder.Append("''");
            }
            for (int i = 0; i < identities.Length; i++)
            {
                if (i > 0)
                {
                    builder.Append(",");
                }
                builder.Append("'");
                builder.Append(identities[i].Name.Replace("'", "''"));
                builder.Append("'");
            }
            CriteriaOperator criteriaOperator = inOperator ? CriteriaOperator.In : CriteriaOperator.NotIn;
            return new CriteriaCollection(new QueryCriterion[] { new QueryCriterion(new SelectParameter("UniqueIdentifier", string.Empty, "ckbx_Credential"), criteriaOperator, new LiteralParameter(builder.ToString(), string.Empty, true)) });
        }

        public static LikeFilter CreateIdentityPropertyFilter(string filterField, string filterValue)
        {
            IdentityFilterPropertyType identityProperty;
            if ((!Utilities.IsNotNullOrEmpty(filterField) || !Utilities.IsNotNullOrEmpty(filterValue)) || (filterField.Equals("RolePermission", StringComparison.InvariantCultureIgnoreCase) || filterField.Equals("RoleMembership", StringComparison.InvariantCultureIgnoreCase)))
            {
                return null;
            }
            if (ApplicationManager.AppSettings.UserMapColumnNames.Contains(filterField))
            {
                identityProperty = IdentityFilterPropertyType.IdentityProperty;
            }
            else
            {
                identityProperty = IdentityFilterPropertyType.ProfileProperty;
            }
            return new LikeFilter(filterField, filterValue, identityProperty);
        }

        private static IdentitySortOrder CreateSortOrder(string sortField, bool sortDescending)
        {
            IdentityFilterPropertyType identityProperty;
            if (string.IsNullOrEmpty(sortField))
            {
                return null;
            }
            if (ApplicationManager.AppSettings.UserMapColumnNames.Contains(sortField))
            {
                identityProperty = IdentityFilterPropertyType.IdentityProperty;
            }
            else
            {
                identityProperty = IdentityFilterPropertyType.ProfileProperty;
            }
            return new IdentitySortOrder(sortField, !sortDescending, identityProperty);
        }

        public static IIdentity CreateUser(string name, string password, out string status)
        {
            return CreateUser(name, password, string.Empty, out status);
        }

        public static IIdentity CreateUser(string name, string password, string domain, out string status)
        {
            if ((name == null) || (name.Replace("'", string.Empty).Trim() == string.Empty))
            {
                status = "User name must contain at least one alphanumeric character.";
                return null;
            }
            if (Utilities.IsNullOrEmpty(domain) && Utilities.IsNullOrEmpty(password))
            {
                status = "An invalid password was specified.";
                return null;
            }
            if (password != null)
            {
                password = password.Trim();
            }
            if (domain != null)
            {
                domain = domain.Trim();
            }
            if (ApplicationManager.AppSettings.UseEncryption && (password != null))
            {
                password = Encryption.HashString(password);
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Identity_Create");
            storedProcCommandWrapper.AddInParameter("UserName", DbType.String, name.Trim());
            storedProcCommandWrapper.AddInParameter("Domain", DbType.String, domain);
            storedProcCommandWrapper.AddInParameter("Password", DbType.String, password);
            storedProcCommandWrapper.AddInParameter("Guid", DbType.Guid, Guid.NewGuid());
            storedProcCommandWrapper.AddInParameter("Encrypted", DbType.Boolean, ApplicationManager.AppSettings.UseEncryption);
            storedProcCommandWrapper.AddOutParameter("UniqueIdentifier", DbType.String, 0x4c6);
            storedProcCommandWrapper.AddOutParameter("StatusMessage", DbType.String, 0x4c6);
            int num = database.ExecuteNonQuery(storedProcCommandWrapper);
            status = storedProcCommandWrapper.GetParameterValue("StatusMessage").ToString();
            if ((num > 0) && (status == string.Empty))
            {
                return GetUserIdentity(storedProcCommandWrapper.GetParameterValue("UniqueIdentifier").ToString());
            }
            Logger.Write("Unable to create user with name [" + name + "].  Status was:  " + status, "Warning", 3, -1, Severity.Warning);
            return null;
        }

        public static void DeleteCustomUserField(string fieldName)
        {
            if (!string.IsNullOrEmpty(fieldName))
            {
                Database database = DatabaseFactory.CreateDatabase();
                using (IDbConnection connection = database.GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Profile_DeleteProperty");
                        storedProcCommandWrapper.AddInParameter("CustomUserFieldName", DbType.String, fieldName);
                        DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper("IF EXISTS (select * from sysobjects where name = 'ckbx_IdentityProfile' and type = 'V') DROP VIEW ckbx_IdentityProfile");
                        DBCommandWrapper command = database.GetSqlStringCommandWrapper(BuildIdentityProfileViewString(fieldName, false));
                        database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                        database.ExecuteNonQuery(sqlStringCommandWrapper, transaction);
                        database.ExecuteNonQuery(command, transaction);
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return;
                }
            }
            Logger.Write("Unable to remove a custom user field with a null or blank name.", "Warning", 3, -1, Severity.Warning);
        }

        public static bool DeleteUser(ExtendedPrincipal callingPrincipal, IIdentity identity, bool deleteResponses, out string status)
        {
            if (identity == null)
            {
                status = "Unable to delete user because user identity was null.";
                return false;
            }
            if (!AuthorizationFactory.GetAuthorizationProvider().Authorize(callingPrincipal, Group.Everyone, "Group.ManageUsers"))
            {
                throw new AuthorizationException();
            }
            Database database = DatabaseFactory.CreateDatabase();
            using (IDbConnection connection = database.GetConnection())
            {
                connection.Open();
                IDbTransaction transaction = connection.BeginTransaction();
                bool flag = false;
                try
                {
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Identity_Delete");
                    storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, identity.Name);
                    storedProcCommandWrapper.AddInParameter("DeleteResponses", DbType.Boolean, deleteResponses);
                    storedProcCommandWrapper.AddOutParameter("StatusMessage", DbType.String, 0xff);
                    database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                    transaction.Commit();
                    if ((storedProcCommandWrapper.GetParameterValue("StatusMessage") == DBNull.Value) || Utilities.IsNullOrEmpty((string) storedProcCommandWrapper.GetParameterValue("StatusMessage")))
                    {
                        status = "User " + identity.Name + " successfully deleted.";
                        flag = true;
                    }
                    else
                    {
                        status = (string) storedProcCommandWrapper.GetParameterValue("StatusMessage");
                    }
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
                return flag;
            }
        }

        public static bool DeleteUser(ExtendedPrincipal callingPrincipal, string identity, bool deleteResponses, out string status)
        {
            return DeleteUser(callingPrincipal, GetUserIdentity(identity), deleteResponses, out status);
        }

        public static bool EncryptUserPasswords()
        {
            SelectQuery query = new SelectQuery("ckbx_Credential");
            query.AddParameter("UniqueIdentifier", string.Empty, string.Empty);
            query.AddParameter("Password", string.Empty, string.Empty);
            query.AddCriterion(new QueryCriterion(new SelectParameter("Encrypted"), CriteriaOperator.NotEqualTo, new LiteralParameter(1)));
            Database database = DatabaseFactory.CreateDatabase();
            List<UserDTO> list = new List<UserDTO>();
            using (IDbConnection connection = database.GetConnection())
            {
                connection.Open();
                IDbTransaction transaction = connection.BeginTransaction();
                try
                {
                    DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
                    ApplicationManager.AppSettings.UseEncryption = true;
                    using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper, transaction))
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                string uniqueIdentity = (string) reader["UniqueIdentifier"];
                                if (uniqueIdentity != null)
                                {
                                    IIdentity userIdentity = GetUserIdentity(uniqueIdentity);
                                    if (userIdentity != null)
                                    {
                                        string password = string.Empty;
                                        if (reader["Password"] != DBNull.Value)
                                        {
                                            password = (string) reader["Password"];
                                        }
                                        UserDTO item = new UserDTO(uniqueIdentity, userIdentity, GetDomain(userIdentity), password);
                                        list.Add(item);
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                        finally
                        {
                            reader.Close();
                        }
                    }
                }
                catch
                {
                    ApplicationManager.AppSettings.UseEncryption = false;
                    transaction.Rollback();
                    throw;
                }
                finally
                {
                    connection.Close();
                }
                foreach (UserDTO rdto2 in list)
                {
                    string str3;
                    UpdateUser(rdto2.Identity, rdto2.IdentityName, rdto2.Domain, rdto2.Password, out str3);
                }
            }
            return true;
        }

        public static void ExpireUser(IToken cacheToken)
        {
            ExtendedPrincipal user = (ExtendedPrincipal) GetUser(cacheToken);
            if ((user != null) && (_loginCacheManager != null))
            {
                _loginCacheManager.Remove(user.Identity.Name);
            }
            _securityCache.ExpireIdentity(cacheToken);
            _securityCache.ExpirePrincipal(cacheToken);
            _securityCache.ExpireProfile(cacheToken);
        }

        private static DataSet FilterIdentityListByViewableUsers(ExtendedPrincipal callingPrincipal, IIdentity[] identities, int pageNumber, int resultsPerPage, PermissionJoin permissionJoinType, string[] permissions, out int totalUsersCount)
        {
            SelectQuery query;
            if (identities.Length > ApplicationManager.AppSettings.MaxIdentityInClauseSize)
            {
                query = QueryFactory.GetSelectAvailableUsersQuery(callingPrincipal, permissionJoinType, permissions);
            }
            else
            {
                query = QueryFactory.GetSelectAvailableUsersQuery(callingPrincipal, CreateIdentitiesFilterCollection(identities, true), permissionJoinType, permissions);
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            DataSet ds = database.ExecuteDataSet(sqlStringCommandWrapper);
            totalUsersCount = ds.Tables[0].Rows.Count;
            return GetDataSetPage(ds, identities, pageNumber, resultsPerPage);
        }

        private static List<IIdentity> GetAllUserIdentities()
        {
            return new List<IIdentity>(IdentityFactory.GetIdentityProvider().GetIdentities(null, null, null));
        }

        public static int GetAllUsersCount(IdentityFilterCollection filterCollection)
        {
            return IdentityFactory.GetIdentityProvider().GetIdentities(null, null, filterCollection).Length;
        }

        public static DataSet GetAllUsersDataSet(int resultsPerPage, int pageNumber, string sortField, bool sortDescending, IdentityFilterCollection filterCollection)
        {
            return IdentityArrayToDataSet(IdentityFactory.GetIdentityProvider().GetIdentities(new PageFilter(pageNumber, resultsPerPage), CreateSortOrder(sortField, sortDescending), filterCollection));
        }

        public static IPrincipal GetCurrentPrincipal()
        {
            if ((_sessionTokenProvider != null) && (_sessionTokenProvider.GetSessionToken() != null))
            {
                return GetUser(_sessionTokenProvider.GetSessionToken());
            }
            return null;
        }

        private static DataSet GetDataSetPage(DataSet ds, IIdentity[] identities, int pageNumber, int resultsPerPage)
        {
            int num2;
            int count;
            ArrayList list = new ArrayList(identities);
            ds.Tables[0].PrimaryKey = new DataColumn[] { ds.Tables[0].Columns["UniqueIdentifier"] };
            for (int i = 0; i < list.Count; i++)
            {
                if (!ds.Tables[0].Rows.Contains(((IIdentity) list[i]).Name))
                {
                    list.RemoveAt(i);
                    i--;
                }
            }
            if ((pageNumber <= 0) || (resultsPerPage <= 0))
            {
                num2 = 0;
                count = ds.Tables[0].Rows.Count;
            }
            else
            {
                num2 = resultsPerPage * (pageNumber - 1);
                count = num2 + resultsPerPage;
            }
            DataTable table = IdentityArrayToDataTable((IIdentity[]) list.ToArray(typeof(IIdentity)));
            DataSet set = new DataSet("UsersDataSet");
            set.Tables.Add(table.Clone());
            for (int j = num2; (j < count) && (j < table.Rows.Count); j++)
            {
                object[] values = new object[table.Columns.Count];
                for (int k = 0; k < table.Columns.Count; k++)
                {
                    values[k] = table.Rows[j][k];
                }
                set.Tables[0].Rows.Add(values);
            }
            return set;
        }

        public static string GetDomain(IIdentity identity)
        {
            if (identity.Name.IndexOf("/") >= 0)
            {
                return identity.Name.Split(new char[] { '/' })[0];
            }
            return string.Empty;
        }

        private static List<FileInfo> GetMessagesByRole(ExtendedPrincipal identity, string messagePath)
        {
            List<FileInfo> list = new List<FileInfo>();
            if (_tourMessages == null)
            {
                LoadTourMessages(messagePath);
            }
            foreach (string str in identity.GetRoles())
            {
                string str2 = str.Replace(" ", string.Empty);
                foreach (FileInfo info in _tourMessages)
                {
                    int startIndex = (info.Name.IndexOf("_") == -1) ? 0 : (info.Name.IndexOf("_") + 1);
                    string str3 = info.Name.Substring(startIndex, info.Name.LastIndexOf(info.Extension) - startIndex);
                    if (str2.Equals(str3, StringComparison.InvariantCultureIgnoreCase))
                    {
                        list.Add(info);
                    }
                }
            }
            return list;
        }

        public static List<FileInfo> GetProductTourMessages(ExtendedPrincipal principal, string applicationPath)
        {
            if (principal == null)
            {
                return null;
            }
            List<FileInfo> messagesByRole = GetMessagesByRole(principal, applicationPath);
            List<FileInfo> list2 = new List<FileInfo>();
            if (messagesByRole.Count > 0)
            {
                Database database = DatabaseFactory.CreateDatabase();
                using (IDbConnection connection = database.GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        try
                        {
                            foreach (FileInfo info in messagesByRole)
                            {
                                int num;
                                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Message_OptOut_Get");
                                storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, principal.Identity.Name);
                                storedProcCommandWrapper.AddInParameter("Page", DbType.String, info.Name);
                                object obj2 = database.ExecuteScalar(storedProcCommandWrapper, transaction) ?? string.Empty;
                                if (int.TryParse(obj2.ToString(), out num) && (num == 0))
                                {
                                    list2.Add(info);
                                }
                            }
                            return list2;
                        }
                        catch
                        {
                            transaction.Rollback();
                            throw;
                        }
                        return list2;
                    }
                    finally
                    {
                        transaction.Commit();
                        connection.Close();
                    }
                }
            }
            return list2;
        }

        public static IProfile GetProfile(IToken token)
        {
            return _securityCache.GetProfile(token);
        }

        public static IProfile GetProfile(IIdentity identity)
        {
            return ProfileFactory.GetProfileProvider().GetProfile(identity);
        }

        public static object GetProfileProperty(IIdentity identity, object profileKey)
        {
            ArgumentValidation.CheckForNullReference(profileKey, "profileKey");
            string key = profileKey.ToString();
            if (key.Equals("GUID", StringComparison.InvariantCultureIgnoreCase))
            {
                return GetUserGUID(identity);
            }
            IProfile profile = GetProfile(identity);
            if (profile != null)
            {
                if (profile.Contains(key))
                {
                    return profile[profileKey];
                }
                if (key.Contains("_") && profile.Contains(key.Replace('_', ' ')))
                {
                    return profile[key.Replace('_', ' ')];
                }
            }
            return null;
        }

        public static object[] GetProfilePropertyList()
        {
            return ProfileFactory.GetProfileProvider().GetAllPropertyKeys();
        }

        private static DataSet GetRoleFilteredUsersDataSet(ExtendedPrincipal callingPrincipal, int resultsPerPage, int pageNumber, string filterField, string filterValue, string sortField, bool sortDescending, PermissionJoin permissionJoinType, string[] permissions, out int totalUsersCount)
        {
            DBCommandWrapper storedProcCommandWrapper;
            ArgumentValidation.CheckForEmptyString(filterField, "filterField");
            ArgumentValidation.CheckForEmptyString(filterValue, "filterValue");
            bool flag = filterField.Equals("RolePermission", StringComparison.InvariantCultureIgnoreCase);
            Database database = DatabaseFactory.CreateDatabase();
            if (flag)
            {
                storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Security_ListIdentitiesInRoleWithPermission");
                storedProcCommandWrapper.AddInParameter("PermissionName", DbType.String, filterValue);
            }
            else
            {
                storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Security_ListIdentitiesInRole");
                storedProcCommandWrapper.AddInParameter("RoleName", DbType.String, filterValue);
            }
            List<IIdentity> listToPage = new List<IIdentity>();
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                while (reader.Read())
                {
                    listToPage.Add(new GenericIdentity(DbUtility.GetValueFromDataReader<string>(reader, "UniqueIdentifier", string.Empty)));
                }
            }
            List<IIdentity> list2 = new List<IIdentity>();
            if (!callingPrincipal.IsInRole("System Administrator") && !AuthorizationFactory.GetAuthorizationProvider().Authorize(callingPrincipal, Group.Everyone, "Group.View"))
            {
                return FilterIdentityListByViewableUsers(callingPrincipal, list2.ToArray(), pageNumber, resultsPerPage, permissionJoinType, permissions, out totalUsersCount);
            }
            totalUsersCount = listToPage.Count;
            return IdentityArrayToDataSet(Utilities.GetListDataPage<IIdentity>(listToPage, pageNumber, resultsPerPage));
        }

        public static IPrincipal GetUser(IToken token)
        {
            return _securityCache.GetPrincipal(token);
        }

        public static IPrincipal GetUser(IIdentity identity)
        {
            if (identity != null)
            {
                return new ExtendedPrincipal(identity, RoleManager.GetIdentityRoleNames(identity).ToArray());
            }
            return null;
        }

        public static IPrincipal GetUser(string uniqueIdentity)
        {
            ArgumentValidation.CheckForEmptyString(uniqueIdentity, "UniqueIdentifier");
            if (Utilities.IsNullOrEmpty(uniqueIdentity))
            {
                return null;
            }
            return GetUser(GetUserIdentity(uniqueIdentity));
        }

        public static IPrincipal GetUser(string identity, string domain)
        {
            return GetUser(GetUserIdentity(identity, domain));
        }

        public static IPrincipal GetUserByGUID(Guid userGUID)
        {
            IPrincipal user = null;
            SelectQuery query = new SelectQuery("ckbx_Credential");
            query.AddParameter("UniqueIdentifier", string.Empty, string.Empty);
            query.AddCriterion(new QueryCriterion(new SelectParameter("GUID"), CriteriaOperator.EqualTo, new LiteralParameter("'" + userGUID + "'")));
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        string str = DbUtility.GetValueFromDataReader<string>(reader, "UniqueIdentifier", null);
                        if (Utilities.IsNotNullOrEmpty(str))
                        {
                            user = GetUser(str);
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return user;
        }

        public static string GetUserGUID(IIdentity identity)
        {
            if (identity == null)
            {
                return string.Empty;
            }
            string str = string.Empty;
            SelectQuery query = new SelectQuery("ckbx_Credential");
            query.AddParameter("GUID", string.Empty, string.Empty);
            query.AddCriterion(new QueryCriterion(new SelectParameter("UniqueIdentifier"), CriteriaOperator.EqualTo, new LiteralParameter("'" + identity.Name + "'")));
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    if (reader.Read())
                    {
                        Guid? nullable = DbUtility.GetValueFromDataReader<Guid?>(reader, "GUID", null);
                        if (nullable.HasValue)
                        {
                            str = nullable.ToString();
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return str;
        }

        public static List<IIdentity> GetUserIdentities(ExtendedPrincipal callingPrincipal, PermissionJoin permissionJoinType, params string[] permissions)
        {
            ArgumentValidation.CheckForNullReference(callingPrincipal, "callingPrincipal");
            if (callingPrincipal.IsInRole("System Administrator") || AuthorizationFactory.GetAuthorizationProvider().Authorize(callingPrincipal, Group.Everyone, "Group.View"))
            {
                return GetAllUserIdentities();
            }
            SelectQuery query = QueryFactory.GetSelectAvailableUsersQuery(callingPrincipal, permissionJoinType, permissions);
            List<IIdentity> list = new List<IIdentity>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        list.Add(GetUserIdentity(DbUtility.GetValueFromDataReader<string>(reader, "UniqueIdentifier", string.Empty)));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }

        public static List<IIdentity> GetUserIdentities(ExtendedPrincipal callingPrincipal, IdentityFilterCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            ArgumentValidation.CheckForNullReference(callingPrincipal, "callingPrincipal");
            if (callingPrincipal.IsInRole("System Administrator") || AuthorizationFactory.GetAuthorizationProvider().Authorize(callingPrincipal, Group.Everyone, "Group.View"))
            {
                return new List<IIdentity>(IdentityFactory.GetIdentityProvider().GetIdentities(null, null, filterCollection));
            }
            SelectQuery query = QueryFactory.GetSelectAvailableUsersQuery(callingPrincipal, IdentityFilterToQueryCriteriaInterpreter.Translate(filterCollection, string.Empty, string.Empty), permissionJoinType, permissions);
            List<IIdentity> list = new List<IIdentity>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            using (IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        list.Add(GetUserIdentity(DbUtility.GetValueFromDataReader<string>(reader, "UniqueIdentifier", string.Empty)));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }

        public static IIdentity GetUserIdentity(string uniqueIdentity)
        {
            ArgumentValidation.CheckForEmptyString(uniqueIdentity, "UniqueIdentifier");
            IIdentity identity = IdentityFactory.GetIdentityProvider().GetIdentity(uniqueIdentity);
            if ((identity == null) && (uniqueIdentity.Split(new char[] { '/' }).Length == 2))
            {
                identity = new GenericIdentity(uniqueIdentity);
            }
            return identity;
        }

        public static IIdentity GetUserIdentity(string identity, string domain)
        {
            ArgumentValidation.CheckForEmptyString(identity, "UniqueIdentifier");
            if (!Utilities.IsNotNullOrEmpty(domain))
            {
                return GetUserIdentity(identity);
            }
            return GetUserIdentity(domain + "/" + identity);
        }

        public static List<IPrincipal> GetUserPrincipals(ExtendedPrincipal callingPrincipal, PermissionJoin permissionJoinType, params string[] permissions)
        {
            List<IIdentity> list = GetUserIdentities(callingPrincipal, permissionJoinType, permissions);
            List<IPrincipal> list2 = new List<IPrincipal>();
            foreach (IIdentity identity in list)
            {
                list2.Add(GetUser(identity));
            }
            return list2;
        }

        public static List<IPrincipal> GetUserPrincipals(ExtendedPrincipal callingPrincipal, IdentityFilterCollection filterCollection, PermissionJoin permissionJoinType, params string[] permissions)
        {
            List<IIdentity> list = GetUserIdentities(callingPrincipal, filterCollection, permissionJoinType, permissions);
            List<IPrincipal> list2 = new List<IPrincipal>();
            foreach (IIdentity identity in list)
            {
                list2.Add(GetUser(identity));
            }
            return list2;
        }

        public static DataSet GetUsersDataSet(ExtendedPrincipal callingPrincipal, int resultsPerPage, int pageNumber, string filterField, string filterValue, string sortField, bool sortDescending, IdentityFilterCollection filterCollection, PermissionJoin permissionJoinType, string[] permissions, out int totalUsersCount)
        {
            ArgumentValidation.CheckForNullReference(callingPrincipal, "callingPrincipal");
            if ((Utilities.IsNotNullOrEmpty(filterField) && Utilities.IsNotNullOrEmpty(filterValue)) && (filterField.Equals("RolePermission", StringComparison.InvariantCultureIgnoreCase) || filterField.Equals("RoleMembership", StringComparison.InvariantCultureIgnoreCase)))
            {
                return GetRoleFilteredUsersDataSet(callingPrincipal, resultsPerPage, pageNumber, filterField, filterValue, sortField, sortDescending, permissionJoinType, permissions, out totalUsersCount);
            }
            filterCollection = AddIdentityPropertyFilterToCollection(filterField, filterValue, filterCollection);
            IIdentityProvider identityProvider = IdentityFactory.GetIdentityProvider();
            if (callingPrincipal.IsInRole("System Administrator") || AuthorizationFactory.GetAuthorizationProvider().Authorize(callingPrincipal, Group.Everyone, "Group.View"))
            {
                IIdentity[] identityArray = identityProvider.GetIdentities(new PageFilter(pageNumber, resultsPerPage), CreateSortOrder(sortField, sortDescending), filterCollection);
                totalUsersCount = identityProvider.GetIdentityCount(filterCollection);
                return IdentityArrayToDataSet(identityArray);
            }
            IIdentity[] identities = identityProvider.GetIdentities(null, CreateSortOrder(sortField, sortDescending), filterCollection);
            return FilterIdentityListByViewableUsers(callingPrincipal, identities, pageNumber, resultsPerPage, permissionJoinType, permissions, out totalUsersCount);
        }

        private static DataSet IdentityArrayToDataSet(IEnumerable<IIdentity> identities)
        {
            DataSet set = new DataSet("IdentitySet");
            set.Tables.Add(IdentityArrayToDataTable(identities));
            return set;
        }

        private static DataTable IdentityArrayToDataTable(IEnumerable<IIdentity> identities)
        {
            DataTable table = new DataTable("IdentityTable");
            table.Columns.Add("UserName");
            table.Columns.Add("Domain");
            table.Columns.Add("UniqueIdentifier");
            if (identities != null)
            {
                foreach (IIdentity identity in identities)
                {
                    string str;
                    string name;
                    if (identity.Name.IndexOf("/") > 0)
                    {
                        str = identity.Name.Split(new char[] { '/' })[0];
                        name = identity.Name.Split(new char[] { '/' })[1];
                    }
                    else
                    {
                        str = string.Empty;
                        name = identity.Name;
                    }
                    object[] values = new object[] { name, str, identity.Name };
                    table.Rows.Add(values);
                }
            }
            return table;
        }

        public static void Initialize()
        {
            lock (typeof(UserManager))
            {
                _securityCache = SecurityCacheFactory.GetSecurityCacheProvider();
                _loginCacheManager = CacheFactory.GetCacheManager("loggedInUserCacheManager");
                _sessionTokenProvider = SessionTokenFactory.GetSessionTokenProvider();
            }
        }

        public static bool IsNetworkUser(string authenticationType)
        {
            if ((authenticationType != null) && !string.Empty.Equals(authenticationType.Trim()))
            {
                switch (authenticationType)
                {
                    case "IISAuthenticated":
                        return true;

                    case "LDAPAuthenticationProvider":
                        return true;
                }
            }
            return false;
        }

        private static void LoadTourMessages(string messagePath)
        {
            _tourMessages = new List<FileInfo>();
            if (Directory.Exists(messagePath))
            {
                try
                {
                    string[] files = Directory.GetFiles(messagePath);
                    if (files != null)
                    {
                        foreach (string str in files)
                        {
                            if (File.Exists(str))
                            {
                                FileInfo item = new FileInfo(str);
                                _tourMessages.Add(item);
                            }
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        public static UserLoginInfo[] LoggedInUsers()
        {
            ArrayList list = new ArrayList();
            string[] strArray = _loginCacheManager.ListKeys();
            if (strArray != null)
            {
                foreach (string str in strArray)
                {
                    UserLoginInfo info = (UserLoginInfo) _loginCacheManager[str];
                    if (info != null)
                    {
                        list.Add(info);
                    }
                }
            }
            return (UserLoginInfo[]) list.ToArray(typeof(UserLoginInfo));
        }

        public static bool LogoutUser(string uniqueIdentifier, out string status)
        {
            if (!GetCurrentPrincipal().IsInRole("System Administrator"))
            {
                throw new AuthorizationException();
            }
            if ((_loginCacheManager != null) && _loginCacheManager.Contains(uniqueIdentifier))
            {
                UserLoginInfo info = (UserLoginInfo) _loginCacheManager[uniqueIdentifier];
                if (info != null)
                {
                    IToken userToken = info.UserToken;
                    IToken sessionToken = null;
                    if (_sessionTokenProvider != null)
                    {
                        sessionToken = _sessionTokenProvider.GetSessionToken();
                    }
                    if (((userToken != null) && (sessionToken != null)) && (userToken.Value == sessionToken.Value))
                    {
                        status = "Logging self out is not allowed.";
                        return false;
                    }
                    ExpireUser(userToken);
                }
            }
            status = "Successfully logged user [" + uniqueIdentifier + "] out.";
            return true;
        }

        public static void MoveCustomUserField(string fieldName, bool moveUp)
        {
            if (Utilities.IsNotNullOrEmpty(fieldName))
            {
                Database database = DatabaseFactory.CreateDatabase();
                using (IDbConnection connection = database.GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Profile_MoveProperty");
                        storedProcCommandWrapper.AddInParameter("CustomUserFieldName", DbType.String, fieldName);
                        storedProcCommandWrapper.AddInParameter("MoveUp", DbType.Boolean, moveUp);
                        database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                    return;
                }
            }
            Logger.Write("Unable to add a custom user field with a null or blank name.", "Warning", 3, -1, Severity.Warning);
        }

        public static void OptOutOfProductTourMessages(string userName, List<FileInfo> messages)
        {
            if (!Utilities.IsNullOrEmpty(userName) && (messages != null))
            {
                Database database = DatabaseFactory.CreateDatabase();
                using (IDbConnection connection = database.GetConnection())
                {
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        foreach (FileInfo info in messages)
                        {
                            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Message_OptOut_Set");
                            storedProcCommandWrapper.AddInParameter("UniqueIdentifier", DbType.String, userName);
                            storedProcCommandWrapper.AddInParameter("Page", DbType.String, info.Name);
                            database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                        }
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        transaction.Commit();
                        connection.Close();
                    }
                }
            }
        }

        public static void SetProfile(IIdentity identity, IProfile profile)
        {
            ProfileFactory.GetProfileProvider().SetProfile(identity, profile);
        }

        public static void SetProfileProperty(IIdentity identity, object profileKey, object profileValue)
        {
            IProfile profile = GetProfile(identity);
            if ((profile != null) && (profileKey != null))
            {
                profile[profileKey] = profileValue;
                SetProfile(identity, profile);
            }
        }

        public static void SetUserContext(string userHostName, string userHostAddress, string userAgent, string currentContext)
        {
            if ((_sessionTokenProvider.GetSessionToken() != null) && (_loginCacheManager != null))
            {
                IPrincipal currentPrincipal = GetCurrentPrincipal();
                if (currentPrincipal != null)
                {
                    UserLoginInfo info = ((UserLoginInfo) _loginCacheManager[currentPrincipal.Identity.Name]) ?? new UserLoginInfo(currentPrincipal.Identity.Name, userHostName, userHostAddress, userAgent, _sessionTokenProvider.GetSessionToken());
                    info.CurrentUrl = currentContext;
                    _loginCacheManager.Add(currentPrincipal.Identity.Name, info, CacheItemPriority.NotRemovable, null, new ICacheItemExpiration[] { new SlidingTime(new TimeSpan(0, 0, ApplicationManager.AppSettings.SessionTimeOut, 0)) });
                }
            }
        }

        public static string SuggestedDomain()
        {
            if (Utilities.IsNotNullOrEmpty(ApplicationManager.AppSettings.ActiveDirectoryNamingContext))
            {
                string[] strArray = ApplicationManager.AppSettings.ActiveDirectoryNamingContext.Split(new char[] { ',' });
                if (strArray.Length > 0)
                {
                    string str = strArray[0];
                    if (str.Length >= (str.IndexOf("=") + 1))
                    {
                        return str.Substring(str.IndexOf("=") + 1);
                    }
                }
            }
            return string.Empty;
        }

        public static IPrincipal UpdateUser(IIdentity identity, string newUserName, string newDomain, string newPassword, out string status)
        {
            return UpdateUser(identity, newUserName, newDomain, newPassword, null, out status);
        }

        public static IPrincipal UpdateUser(IIdentity identity, string newUserName, string newDomain, string newPassword, IDbTransaction transaction, out string status)
        {
            if (identity == null)
            {
                status = "Unable to update user with NULL identity.";
                return null;
            }
            if (Utilities.IsNullOrEmpty(newUserName))
            {
                newUserName = identity.Name;
            }
            if (Utilities.IsNullOrEmpty(newUserName.Replace("'", string.Empty)))
            {
                status = "User name must contain at least one alphanumeric character.";
                return null;
            }
            if (ApplicationManager.AppSettings.UseEncryption && (newPassword != null))
            {
                newPassword = Encryption.HashString(newPassword);
            }
            Database database = DatabaseFactory.CreateDatabase();
            StringBuilder builder = new StringBuilder();
            if (Utilities.IsNotNullOrEmpty(newDomain))
            {
                builder.Append(newDomain);
                builder.Append('/');
            }
            builder.Append(newUserName);
            bool flag = transaction == null;
            using (IDbConnection connection = database.GetConnection())
            {
                connection.Open();
                if (transaction == null)
                {
                    transaction = connection.BeginTransaction();
                }
                try
                {
                    if (string.Compare(builder.ToString(), identity.Name, true) != 0)
                    {
                        DBCommandWrapper command = database.GetStoredProcCommandWrapper("ckbx_Identity_Exists");
                        command.AddInParameter("UniqueIdentifier", DbType.String, builder.ToString());
                        if (((int) database.ExecuteScalar(command, transaction)) != 0)
                        {
                            if (flag)
                            {
                                transaction.Rollback();
                            }
                            status = "USERNOTUNIQUE";
                            return null;
                        }
                    }
                    DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Identity_Update");
                    storedProcCommandWrapper.AddInParameter("OldUniqueIdentifier", DbType.String, identity.Name);
                    storedProcCommandWrapper.AddInParameter("NewUniqueIdentifier", DbType.String, builder.ToString());
                    storedProcCommandWrapper.AddInParameter("UserName", DbType.String, newUserName);
                    storedProcCommandWrapper.AddInParameter("Domain", DbType.String, newDomain);
                    storedProcCommandWrapper.AddInParameter("Password", DbType.String, newPassword);
                    storedProcCommandWrapper.AddInParameter("Encrypted", DbType.Int32, ApplicationManager.AppSettings.UseEncryption ? 1 : 0);
                    database.ExecuteNonQuery(storedProcCommandWrapper, transaction);
                    if (flag)
                    {
                        transaction.Commit();
                    }
                }
                catch (Exception exception)
                {
                    ExceptionPolicy.HandleException(exception, "BusinessPublic");
                    if (flag)
                    {
                        transaction.Rollback();
                    }
                    throw new Exception("Unable to update user.");
                }
                finally
                {
                    connection.Close();
                }
            }
            if (newPassword != null)
            {
                SetProfileProperty(identity, "Password", newPassword);
            }
            status = string.Empty;
            if ((GetCurrentPrincipal() != null) && (identity.Name == GetCurrentPrincipal().Identity.Name))
            {
                CacheUser(GetUser(builder.ToString()), _sessionTokenProvider.GetSessionToken());
            }
            return GetUser(builder.ToString());
        }
    }
}

