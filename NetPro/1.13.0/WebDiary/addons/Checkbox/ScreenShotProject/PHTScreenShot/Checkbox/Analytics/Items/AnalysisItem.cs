﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics;
    using Checkbox.Analytics.Data;
    using Checkbox.Analytics.Filters;
    using Checkbox.Analytics.Items.Configuration;
    using Checkbox.Forms;
    using Checkbox.Forms.Data;
    using Checkbox.Forms.Items;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Globalization.Text;
    using Checkbox.Management;
    using Prezza.Framework.Data;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class AnalysisItem : Item
    {
        private AnalysisAnswerData _analysisData;
        private List<int> _itemIDs;
        private DateTime? _metaDataLastModified;
        private Dictionary<int, int> _responseCounts;
        private List<int> _responseTemplateIDs = new List<int>();
        private bool? _templatesValidated;

        protected AnalysisItem()
        {
            this.FilterCollection = new AnalysisItemFilterCollection();
            this._itemIDs = new List<int>();
            this.RunMode = false;
        }

        public override void Configure(ItemData itemData, string languageCode)
        {
            base.Configure(itemData, languageCode);
            this.UseAliases = ((AnalysisItemData) itemData).UseAliases;
            this._responseTemplateIDs = new List<int>(((AnalysisItemData) itemData).ResponseTemplateIDs);
            this.OtherOption = ((AnalysisItemData) itemData).OtherOption;
            this._itemIDs = new List<int>(((AnalysisItemData) itemData).SourceItemIDs);
        }

        public void Configure(ItemData itemData, string languageCode, AnalysisItemFilterCollection filters)
        {
            this.FilterCollection = filters;
            this._metaDataLastModified = itemData.LastModified;
            this.Configure(itemData, languageCode);
        }

        protected string GenerateDataKey()
        {
            if (!this.PreviewMode)
            {
                return "Run";
            }
            return "Preview";
        }

        protected virtual object GeneratePreviewData()
        {
            return null;
        }

        protected virtual object[] GetAdditionalRsParams()
        {
            return new object[0];
        }

        protected AnalysisAnswerData GetAnalysisData()
        {
            if ((this.Analysis != null) && (this.Analysis != null))
            {
                this.StandaloneMode = false;
                return this.Analysis.Data;
            }
            if (this._analysisData == null)
            {
                this._analysisData = new AnalysisAnswerData(this.TemplatesValidated);
                this._analysisData.Initialize(base.LanguageCode);
                this._analysisData.Load(this._responseTemplateIDs, new AnalysisFilterCollection(), null, null, ApplicationManager.AppSettings.ReportIncompleteResponses, string.Empty);
                this.StandaloneMode = true;
            }
            return this._analysisData;
        }

        public ReadOnlyCollection<Checkbox.Analytics.Filters.Filter> GetFilters()
        {
            return new ReadOnlyCollection<Checkbox.Analytics.Filters.Filter>(this.FilterCollection.GetFilters(base.LanguageCode));
        }

        protected virtual string GetItemAliasFromConfigurationData(int itemID, out Dictionary<int, string> optionAliases)
        {
            ItemData configurationData = ItemConfigurationManager.GetConfigurationData(itemID);
            string alias = string.Empty;
            optionAliases = new Dictionary<int, string>();
            if ((configurationData != null) && (configurationData.Alias != null))
            {
                alias = configurationData.Alias;
            }
            if (configurationData is SelectItemData)
            {
                foreach (ListOptionData data2 in ((SelectItemData) configurationData).Options)
                {
                    optionAliases[data2.OptionID] = data2.Alias ?? string.Empty;
                }
            }
            return alias;
        }

        protected List<ItemAnswer> GetItemAnswers(int itemId)
        {
            return this.GetAnalysisData().ListItemAnswers(itemId, new List<int>(this.SourceResponseTemplateIDs), this.FilterCollection.GetFilters(TextManager.DefaultLanguage));
        }

        public virtual List<int> GetItemOptionIdsForPreview(int itemId)
        {
            List<int> itemOptionIdsForReport = this.GetItemOptionIdsForReport(itemId);
            if (itemOptionIdsForReport.Count > ApplicationManager.AppSettings.MaxReportPreviewOptions)
            {
                int count = itemOptionIdsForReport.Count - ApplicationManager.AppSettings.MaxReportPreviewOptions;
                itemOptionIdsForReport.RemoveRange(itemOptionIdsForReport.Count - count, count);
            }
            return itemOptionIdsForReport;
        }

        public virtual List<int> GetItemOptionIdsForReport(int itemId)
        {
            List<int> list = new List<int>();
            LightweightItemMetaData itemData = SurveyMetaDataProxy.GetItemData(itemId, this.TemplatesValidated);
            if (itemData != null)
            {
                list = new List<int>(itemData.Options);
            }
            return list;
        }

        protected virtual List<ItemAnswer> GetItemPreviewAnswers(int itemID, long? answerIdSeed, long? responseIdSeed)
        {
            List<ItemAnswer> list = new List<ItemAnswer>();
            List<int> itemOptionIdsForPreview = this.GetItemOptionIdsForPreview(itemID);
            long? nullable = answerIdSeed;
            long num = nullable.HasValue ? nullable.GetValueOrDefault() : 0x3e8L;
            long? nullable2 = responseIdSeed;
            long num2 = nullable2.HasValue ? nullable2.GetValueOrDefault() : 0x3e8L;
            if (itemOptionIdsForPreview.Count > 0)
            {
                for (int j = 0; j < itemOptionIdsForPreview.Count; j++)
                {
                    LightweightOptionMetaData data = SurveyMetaDataProxy.GetOptionData(itemOptionIdsForPreview[j], new int?(itemID), this.TemplatesValidated);
                    if (data != null)
                    {
                        for (int k = 0; k <= j; k++)
                        {
                            string str = string.Empty;
                            if (data.IsOther)
                            {
                                str = TextManager.GetText("/controlText/analysisItem/other", base.LanguageCode, "Other", new string[0]) + " " + k;
                            }
                            ItemAnswer item = new ItemAnswer();
                            num += 1L;
                            item.AnswerId = num;
                            num2 += 1L;
                            item.ResponseId = num2;
                            item.ItemId = itemID;
                            item.OptionId = new int?(itemOptionIdsForPreview[j]);
                            item.IsOther = data.IsOther;
                            item.AnswerText = str;
                            list.Add(item);
                        }
                    }
                }
                return list;
            }
            for (int i = 1; i <= 5; i++)
            {
                ItemAnswer answer2 = new ItemAnswer();
                num += 1L;
                answer2.AnswerId = num;
                num2 += 1L;
                answer2.ResponseId = num2;
                answer2.ItemId = itemID;
                answer2.AnswerText = TextManager.GetText("/controlText/analysisItem/sample" + i, base.LanguageCode, "Sample " + i, new string[0]);
                list.Add(answer2);
            }
            return list;
        }

        public int GetItemResponseCount(int itemID)
        {
            if (!this.ResponseCounts.ContainsKey(itemID))
            {
                this.ResponseCounts[itemID] = this.GetResponseCountForItem(itemID);
            }
            return this.ResponseCounts[itemID];
        }

        public virtual string GetItemText(int itemID)
        {
            return SurveyMetaDataProxy.GetItemText(itemID, base.LanguageCode, this.UseAliases, this.TemplatesValidated);
        }

        public bool GetOptionIsOther(int itemID, int optionID)
        {
            return SurveyMetaDataProxy.GetOptionIsOther(optionID, this.TemplatesValidated);
        }

        public double GetOptionPoints(int itemID, int optionID)
        {
            return SurveyMetaDataProxy.GetOptionPoints(optionID, this.TemplatesValidated);
        }

        public virtual string GetOptionText(int itemID, int optionID)
        {
            return SurveyMetaDataProxy.GetOptionText(new int?(itemID), optionID, base.LanguageCode, this.UseAliases, true);
        }

        protected virtual int GetResponseCountForItem(int itemId)
        {
            DataSet data = this.Data as DataSet;
            if ((data != null) && data.Tables.Contains("ResponseCounts"))
            {
                DataRow[] rowArray = data.Tables["ResponseCounts"].Select("ItemId = " + itemId);
                if (rowArray.Length > 0)
                {
                    return DbUtility.GetValueFromDataRow<int>(rowArray[0], "ResponseCount", 0);
                }
            }
            return 0;
        }

        public virtual string GetSourceItemTypeName(int itemID)
        {
            return SurveyMetaDataProxy.GetItemTypeName(itemID, true);
        }

        protected virtual object LoadAndProcessData()
        {
            object obj2 = null;
            if (ApplicationManager.AppSettings.UseReportingService && (this.Analysis != null))
            {
            }
            if ((obj2 != null) && (!(obj2 is DataSet) || (((DataSet) obj2).Tables.Count != 0)))
            {
                return obj2;
            }
            return this.ProcessData();
        }

        protected virtual object ProcessData()
        {
            this.DataProcessed = true;
            return null;
        }

        public bool ValidateResultData()
        {
            return (AnalysisDataProxy.ValidateItemResultData(base.ID, base.LanguageCode, this.GenerateDataKey(), this._metaDataLastModified) && AnalysisDataProxy.ValidateItemResultData(base.ID, base.LanguageCode, this.GenerateDataKey(), this.ResultValidationReferenceDate));
        }

        public bool ValidateTemplateData()
        {
            AnalysisDataProxyCacheItem<object> item = AnalysisDataProxy.GetResultFromCache<object>(base.ID, base.LanguageCode, this.GenerateDataKey());
            if (item == null)
            {
                return false;
            }
            foreach (int num in this.SourceResponseTemplateIDs)
            {
                if (ResponseTemplateManager.CheckTemplateUpdated(num, new DateTime?(item.ReferenceDate)))
                {
                    return false;
                }
            }
            return true;
        }

        public Checkbox.Analytics.Analysis Analysis { get; set; }

        public virtual object Data
        {
            get
            {
                object result = null;
                try
                {
                    if (this.ValidateResultData())
                    {
                        AnalysisDataProxyCacheItem<object> item = AnalysisDataProxy.GetResultFromCache<object>(base.ID, base.LanguageCode, this.GenerateDataKey());
                        if (item != null)
                        {
                            result = item.Data;
                        }
                        return result;
                    }
                    result = this.PreviewMode ? this.GeneratePreviewData() : this.LoadAndProcessData();
                    if (result != null)
                    {
                        AnalysisDataProxy.AddResultToCache<object>(base.ID, base.LanguageCode, this.GenerateDataKey(), result);
                    }
                }
                catch (Exception exception)
                {
                    ExceptionPolicy.HandleException(exception, "BusinessProtected");
                }
                return result;
            }
        }

        protected bool DataProcessed { get; set; }

        protected AnalysisItemFilterCollection FilterCollection { get; private set; }

        public virtual Checkbox.Analytics.Items.Configuration.OtherOption OtherOption { get; set; }

        public virtual bool PreviewMode
        {
            get
            {
                return (!this.RunMode && ApplicationManager.AppSettings.ShowPreviewInAnalysis);
            }
        }

        protected object ReportItemData { get; set; }

        protected Dictionary<int, int> ResponseCounts
        {
            get
            {
                if (this._responseCounts == null)
                {
                    this._responseCounts = new Dictionary<int, int>();
                }
                return this._responseCounts;
            }
        }

        public DateTime? ResultValidationReferenceDate { get; set; }

        public bool RunMode { get; set; }

        public virtual ReadOnlyCollection<int> SourceItemIDs
        {
            get
            {
                return new ReadOnlyCollection<int>(this._itemIDs);
            }
        }

        public ReadOnlyCollection<string> SourceItemTexts
        {
            get
            {
                List<string> list = new List<string>();
                foreach (int num in this.SourceItemIDs)
                {
                    list.Add(this.GetItemText(num));
                }
                return new ReadOnlyCollection<string>(list);
            }
        }

        public ReadOnlyCollection<int> SourceResponseTemplateIDs
        {
            get
            {
                return new ReadOnlyCollection<int>(this._responseTemplateIDs);
            }
        }

        public bool StandaloneMode { get; set; }

        public virtual bool TemplatesValidated
        {
            get
            {
                if (!this._templatesValidated.HasValue)
                {
                    this._templatesValidated = new bool?(this.ValidateTemplateData());
                }
                return this._templatesValidated.Value;
            }
        }

        public bool UseAliases { get; set; }
    }
}

