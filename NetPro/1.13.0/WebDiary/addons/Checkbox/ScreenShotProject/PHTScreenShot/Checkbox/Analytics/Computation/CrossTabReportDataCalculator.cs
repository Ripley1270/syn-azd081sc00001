﻿namespace Checkbox.Analytics.Computation
{
    using Checkbox.Common;
    using Prezza.Framework.Common;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class CrossTabReportDataCalculator : TabularReportDataCalculator
    {
        protected override DataTable Calculate(ItemAnswerAggregator answerAggregator)
        {
            DataTable table = base.CreateOutputTable();
            Dictionary<int, Dictionary<long, int>> itemResponses = answerAggregator.ItemResponses;
            Dictionary<int, Dictionary<long, int>> optionResponses = answerAggregator.OptionResponses;
            int columnCount = base.GetColumnCount();
            int rowCount = base.GetRowCount();
            for (int i = 1; i <= (columnCount + 1); i++)
            {
                int? optionIDAtPosition = base.GetOptionIDAtPosition(new Coordinate(i, 1));
                if (optionIDAtPosition.HasValue)
                {
                    int itemID = answerAggregator.GetItemID(optionIDAtPosition.Value);
                    if (itemID > 0)
                    {
                        for (int j = 2; j <= (rowCount + 1); j++)
                        {
                            int? nullable2 = base.GetOptionIDAtPosition(new Coordinate(1, j));
                            if (nullable2.HasValue)
                            {
                                int key = answerAggregator.GetItemID(nullable2.Value);
                                if ((((key > 0) && itemResponses.ContainsKey(itemID)) && (itemResponses.ContainsKey(key) && optionResponses.ContainsKey(optionIDAtPosition.Value))) && optionResponses.ContainsKey(nullable2.Value))
                                {
                                    List<long> list = Utilities.ListIntersection<long>(new List<long>(optionResponses[optionIDAtPosition.Value].Keys), new List<long>(optionResponses[nullable2.Value].Keys));
                                    DataRow row = table.NewRow();
                                    row["Row"] = j - 1;
                                    row["Column"] = i - 1;
                                    row["AnswerCount"] = list.Count;
                                    if (optionResponses[optionIDAtPosition.Value].Count > 0)
                                    {
                                        row["AnswerPercent"] = 100M * (list.Count / optionResponses[optionIDAtPosition.Value].Count);
                                    }
                                    else
                                    {
                                        row["AnswerPercent"] = 0M;
                                    }
                                    table.Rows.Add(row);
                                }
                            }
                        }
                    }
                }
            }
            return table;
        }
    }
}

