﻿namespace Checkbox.Forms.Security.Providers
{
    using Prezza.Framework.Common;
    using Prezza.Framework.Configuration;
    using System;
    using System.Xml;

    public class ResponseTemplateAuthorizationProviderData : ProviderData, IXmlConfigurationBase
    {
        public ResponseTemplateAuthorizationProviderData(string providerName) : this(providerName, typeof(ResponseTemplateAuthorizationProvider).AssemblyQualifiedName)
        {
        }

        public ResponseTemplateAuthorizationProviderData(string providerName, string typeName) : base(providerName, typeName)
        {
            ArgumentValidation.CheckForEmptyString(providerName, "providerName");
        }

        public void LoadFromXml(XmlNode providerData)
        {
        }
    }
}

