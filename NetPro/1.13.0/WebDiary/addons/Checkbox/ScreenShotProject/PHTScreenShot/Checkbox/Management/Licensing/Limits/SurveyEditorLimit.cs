﻿namespace Checkbox.Management.Licensing.Limits
{
    using System;
    using System.Collections.Generic;

    public class SurveyEditorLimit : RolePermissionLimit
    {
        public override string LimitName
        {
            get
            {
                return "SurveyEditorLimit";
            }
        }

        public override string PermissionName
        {
            get
            {
                return "Form.Edit";
            }
        }

        public List<string> UsersInRolePermissionLimit
        {
            get
            {
                return this.GetCurrentUsersInRole();
            }
        }
    }
}

