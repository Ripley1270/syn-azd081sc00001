﻿namespace Checkbox.Forms.Validation
{
    using Checkbox.Globalization.Text;
    using System;

    public class IntegerValidator : RegularExpressionValidator
    {
        public IntegerValidator()
        {
            base._regex = "^-[0-9]+$|^[0-9]+$";
        }

        public override string GetMessage(string languageCode)
        {
            return TextManager.GetText("/validationMessages/regex/integer", languageCode);
        }
    }
}

