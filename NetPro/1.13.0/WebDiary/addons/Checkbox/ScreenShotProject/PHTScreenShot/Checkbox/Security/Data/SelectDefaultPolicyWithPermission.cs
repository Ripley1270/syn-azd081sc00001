﻿namespace Checkbox.Security.Data
{
    using Checkbox.Security;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using System;

    internal class SelectDefaultPolicyWithPermission
    {
        private SelectDefaultPolicyWithPermission()
        {
        }

        internal static SelectQuery GetQuery(string permission, string entityTable, string entityDefaultPolicyIDColumn)
        {
            SelectQuery query = new SelectQuery(entityTable);
            query.AddParameter(entityDefaultPolicyIDColumn, string.Empty, entityTable);
            query.AddTableJoin("ckbx_PolicyPermissions", QueryJoinType.Inner, "PolicyID", entityTable, entityDefaultPolicyIDColumn);
            query.AddTableJoin("ckbx_Permission", QueryJoinType.Inner, "PermissionID", "ckbx_PolicyPermissions", "PermissionID");
            query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", string.Empty, "ckbx_Permission"), CriteriaOperator.EqualTo, new LiteralParameter("'" + permission + "'")));
            return query;
        }

        internal static SelectQuery GetQuery(string entityTable, string entityDefaultPolicyIDColumn, PermissionJoin permissionJoinType, params string[] permissions)
        {
            SelectQuery query = new SelectQuery(entityTable);
            query.AddParameter(entityDefaultPolicyIDColumn, string.Empty, entityTable);
            query.AddTableJoin("ckbx_PolicyPermissions", QueryJoinType.Inner, "PolicyID", entityTable, entityDefaultPolicyIDColumn);
            query.AddTableJoin("ckbx_Permission", QueryJoinType.Inner, "PermissionID", "ckbx_PolicyPermissions", "PermissionID");
            if (permissionJoinType == PermissionJoin.Any)
            {
                string paramValue = string.Empty;
                for (int i = 0; i < permissions.Length; i++)
                {
                    if (i > 0)
                    {
                        paramValue = paramValue + ",";
                    }
                    paramValue = paramValue + "'" + permissions[i] + "'";
                }
                query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", string.Empty, "ckbx_Permission"), CriteriaOperator.In, new LiteralParameter(paramValue, string.Empty, true)));
                return query;
            }
            query.CriteriaJoinType = CriteriaJoinType.And;
            foreach (string str2 in permissions)
            {
                query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", string.Empty, "ckbx_Permission"), CriteriaOperator.EqualTo, new LiteralParameter("'" + str2 + "'")));
            }
            return query;
        }
    }
}

