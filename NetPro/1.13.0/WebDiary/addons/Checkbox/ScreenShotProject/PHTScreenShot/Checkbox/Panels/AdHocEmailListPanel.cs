﻿namespace Checkbox.Panels
{
    using Checkbox.Common;
    using Prezza.Framework.Data;
    using System;
    using System.Collections.Generic;
    using System.Data;

    [Serializable]
    public class AdHocEmailListPanel : Panel
    {
        public AdHocEmailListPanel() : base(new string[0], new string[0])
        {
            base.PanelType = PanelType.AdHocEmailListPanel;
        }

        public void AddEmailAddress(string email)
        {
            if (!Utilities.IsNullOrEmpty(email))
            {
                string str = Utilities.SqlEncode(email);
                if (base.Panelists.Find(p => p.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase)) == null)
                {
                    Panelist item = new Panelist {
                        Email = str
                    };
                    base.Panelists.Add(item);
                    Panelist panelist2 = new Panelist {
                        Email = str
                    };
                    base.AddedPanelists.Add(panelist2);
                }
            }
        }

        public void AddEmailAddresses(IEnumerable<string> emailAddresses)
        {
            foreach (string str in emailAddresses)
            {
                this.AddEmailAddress(str);
            }
        }

        protected DBCommandWrapper GetDeletePanelistCommand(Database db, string panelistEmail)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Panel_DeleteAdHoc");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("EmailAddress", DbType.String, panelistEmail);
            return storedProcCommandWrapper;
        }

        protected DBCommandWrapper GetInsertPanelistCommand(Database db, string panelistEmail)
        {
            DBCommandWrapper storedProcCommandWrapper = db.GetStoredProcCommandWrapper("ckbx_Panel_InsertAdHoc");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID.Value);
            storedProcCommandWrapper.AddInParameter("EmailAddress", DbType.String, panelistEmail);
            return storedProcCommandWrapper;
        }

        protected override List<Panelist> GetPanelists()
        {
            List<Panelist> list = new List<Panelist>();
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_Panel_GetAdHocPanelists");
            storedProcCommandWrapper.AddInParameter("PanelID", DbType.Int32, base.ID);
            using (IDataReader reader = database.ExecuteReader(storedProcCommandWrapper))
            {
                try
                {
                    while (reader.Read())
                    {
                        string str = DbUtility.GetValueFromDataReader<string>(reader, "EmailAddress", string.Empty);
                        if (Utilities.IsNotNullOrEmpty(str))
                        {
                            Panelist item = new Panelist {
                                Email = str
                            };
                            list.Add(item);
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            return list;
        }

        public void RemoveEmailAddress(string email)
        {
            if (!Utilities.IsNullOrEmpty(email))
            {
                int index = base.Panelists.FindIndex(p => p.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase));
                if (index >= 0)
                {
                    base.RemovedPanelists.Add(base.Panelists[index]);
                    base.Panelists.RemoveAt(index);
                }
            }
        }

        public void RemoveEmailAddresses(IEnumerable<string> emailAddresses)
        {
            foreach (string str in emailAddresses)
            {
                this.RemoveEmailAddress(str);
            }
        }

        protected override void UpdatePanelists(IDbTransaction t)
        {
            Database db = DatabaseFactory.CreateDatabase();
            foreach (Panelist panelist in base.RemovedPanelists)
            {
                DBCommandWrapper deletePanelistCommand = this.GetDeletePanelistCommand(db, panelist.Email);
                db.ExecuteNonQuery(deletePanelistCommand, t);
            }
            foreach (Panelist panelist2 in base.AddedPanelists)
            {
                DBCommandWrapper insertPanelistCommand = this.GetInsertPanelistCommand(db, panelist2.Email);
                db.ExecuteNonQuery(insertPanelistCommand, t);
            }
            base.AddedPanelists.Clear();
            base.RemovedPanelists.Clear();
        }
    }
}

