﻿namespace Checkbox.Messaging.Email
{
    using System;

    public enum MailFormat
    {
        Html,
        Text
    }
}

