﻿namespace Checkbox.Analytics.Items.UI
{
    using System;

    [Serializable]
    public class MatrixSummaryItemAppearanceData : AnalysisItemAppearanceData
    {
        public MatrixSummaryItemAppearanceData()
        {
            this.GraphType = GraphType.SummaryTable;
        }

        public override string AppearanceCode
        {
            get
            {
                return "ANALYSIS_MATRIX_SUMMARY";
            }
        }
    }
}

