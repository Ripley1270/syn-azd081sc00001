﻿namespace Checkbox.Messaging.Email
{
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class EmailMessageStatus : IEmailMessageStatus
    {
        public DateTime? LastSendAttempt { get; set; }

        public string LastSendError { get; set; }

        public DateTime? NextSendAttempt { get; set; }

        public DateTime? QueuedDate { get; set; }

        public bool SuccessfullySent { get; set; }
    }
}

