﻿namespace Checkbox.Forms.Items.Configuration
{
    using Checkbox.Forms.Items;
    using System;

    [Serializable]
    public class CurrentScoreItemData : MessageItemData
    {
        protected override Item CreateItem()
        {
            return new CurrentScore();
        }
    }
}

