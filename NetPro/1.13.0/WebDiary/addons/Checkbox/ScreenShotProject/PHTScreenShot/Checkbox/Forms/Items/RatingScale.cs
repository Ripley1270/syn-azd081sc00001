﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Checkbox.Forms.Items.Configuration;
    using Checkbox.Forms.Validation;
    using System;
    using System.Collections.Specialized;
    using System.Xml;

    [Serializable]
    public class RatingScale : Select1
    {
        private string _endText;
        private string _midText;
        private string _notApplicableText;
        private string _notApplicableTextID;
        private string _startText;

        public override void Configure(ItemData configuration, string languageCode)
        {
            RatingScaleItemData data = (RatingScaleItemData) configuration;
            this._notApplicableTextID = data.NotApplicableTextID;
            base.Configure(configuration, languageCode);
            this._startText = this.GetText(data.StartTextID);
            this._midText = this.GetText(data.MidTextID);
            this._endText = this.GetText(data.EndTextID);
        }

        protected override ListOption CreateOption(ListOptionData metaOption)
        {
            ListOption option = base.CreateOption(metaOption);
            if (option.IsOther)
            {
                this._notApplicableText = this.GetText(this._notApplicableTextID);
                if (Utilities.IsNullOrEmpty(this._notApplicableText))
                {
                    this._notApplicableText = this.GetText("/controlText/ratingScale/notApplicableDefault");
                }
                if (Utilities.IsNullOrEmpty(this._notApplicableText))
                {
                    this._notApplicableText = "n/a";
                }
                option.Text = this.NotApplicableText;
                return option;
            }
            option.Text = metaOption.Points.ToString();
            return option;
        }

        protected override NameValueCollection GetInstanceDataValuesForXmlSerialization()
        {
            NameValueCollection instanceDataValuesForXmlSerialization = base.GetInstanceDataValuesForXmlSerialization();
            instanceDataValuesForXmlSerialization["startText"] = this.StartText;
            instanceDataValuesForXmlSerialization["midText"] = this.MidText;
            instanceDataValuesForXmlSerialization["endText"] = this.EndText;
            instanceDataValuesForXmlSerialization["notApplicableText"] = this.NotApplicableText;
            return instanceDataValuesForXmlSerialization;
        }

        protected override bool ValidateAnswers()
        {
            RatingScaleValidator validator = new RatingScaleValidator();
            if (!validator.Validate(this))
            {
                base.ValidationErrors.Add(validator.GetMessage(base.LanguageCode));
                return false;
            }
            return true;
        }

        protected override void WriteOptionAnswer(ListOption option, XmlWriter writer)
        {
            writer.WriteStartElement("answer");
            writer.WriteAttributeString("optionId", option.ID.ToString());
            if (option.IsOther)
            {
                writer.WriteCData(this.NotApplicableText);
            }
            else
            {
                writer.WriteCData(option.Text);
            }
            writer.WriteEndElement();
        }

        public string EndText
        {
            get
            {
                return this._endText;
            }
            set
            {
                this._endText = value;
            }
        }

        public string MidText
        {
            get
            {
                return this._midText;
            }
            set
            {
                this._midText = value;
            }
        }

        public string NotApplicableText
        {
            get
            {
                return this._notApplicableText;
            }
            set
            {
                this._notApplicableText = value;
            }
        }

        public string StartText
        {
            get
            {
                return this._startText;
            }
            set
            {
                this._startText = value;
            }
        }
    }
}

