﻿namespace Checkbox.Forms.Logic
{
    using Checkbox.Users;
    using System;
    using System.Security.Principal;

    [Serializable]
    public class ProfileOperand : Operand
    {
        private readonly string _profileKey;
        private readonly IIdentity _userIdentity;

        public ProfileOperand(string profileKey, IIdentity userIdentity)
        {
            this._userIdentity = userIdentity;
            this._profileKey = profileKey;
        }

        protected override object ProtectedGetValue(RuleEventTrigger trigger)
        {
            if (this.UserIdentity != null)
            {
                return UserManager.GetProfileProperty(this.UserIdentity, this.ProfilePropertyKey);
            }
            return null;
        }

        public string ProfilePropertyKey
        {
            get
            {
                return this._profileKey;
            }
        }

        public IIdentity UserIdentity
        {
            get
            {
                return this._userIdentity;
            }
        }
    }
}

