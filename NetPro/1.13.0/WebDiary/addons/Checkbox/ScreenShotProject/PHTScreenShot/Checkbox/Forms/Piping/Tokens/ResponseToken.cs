﻿namespace Checkbox.Forms.Piping.Tokens
{
    using System;

    [Serializable]
    public class ResponseToken : Token
    {
        public ResponseToken(string token) : base(token, TokenType.Response)
        {
        }
    }
}

