﻿namespace Checkbox.Forms.Logic
{
    using Checkbox.Forms;
    using System;

    [Serializable]
    public class GoToPageAction : Checkbox.Forms.Logic.Action
    {
        private readonly Response _context;
        private readonly int _targetPage;

        public GoToPageAction(ResponsePage receiver, Response context, int targetPageIndex) : base(receiver)
        {
            this._targetPage = targetPageIndex;
            this._context = context;
        }

        public override void Execute(bool directive)
        {
            if ((base.Receiver != null) && directive)
            {
                this._context.PrimeNextPage(this._targetPage);
            }
        }

        public override string ToString()
        {
            return ("Go To Page " + this._targetPage);
        }
    }
}

