﻿namespace Checkbox.Forms.Items.UI
{
    using System;

    [Serializable]
    public class RadioButtonScale : RatingScale
    {
        public override string AppearanceCode
        {
            get
            {
                return "RADIO_BUTTON_SCALE";
            }
        }
    }
}

