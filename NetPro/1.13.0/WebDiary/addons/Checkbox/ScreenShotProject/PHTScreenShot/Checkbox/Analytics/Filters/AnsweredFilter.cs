﻿namespace Checkbox.Analytics.Filters
{
    using System;

    [Serializable]
    public class AnsweredFilter : ItemQueryFilter
    {
        protected override string GetValueFilterClause()
        {
            return " (AnswerText IS NOT NULL AND NOT AnswerText LIKE '') OR OptionID IS NOT NULL";
        }

        public override bool UseNotIn
        {
            get
            {
                return false;
            }
        }

        protected override bool ValueRequired
        {
            get
            {
                return true;
            }
        }
    }
}

