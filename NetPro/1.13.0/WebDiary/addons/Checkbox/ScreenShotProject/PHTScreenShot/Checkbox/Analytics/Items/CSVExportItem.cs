﻿namespace Checkbox.Analytics.Items
{
    using Checkbox.Analytics.Items.Configuration;
    using Checkbox.Forms.Items.Configuration;
    using Prezza.Framework.Common;
    using System;

    [Serializable]
    public class CSVExportItem : ExportItem
    {
        private bool _includeHidden;
        private bool _includeOpenEnded;
        private bool _mergeSelectMany;

        public override void Configure(ItemData itemData, string languageCode)
        {
            ArgumentValidation.CheckExpectedType(itemData, typeof(CSVExportItemData));
            this._includeOpenEnded = ((CSVExportItemData) itemData).IncludeOpenEnded;
            this._mergeSelectMany = ((CSVExportItemData) itemData).MergeSelectMany;
            this._includeHidden = ((CSVExportItemData) itemData).IncludeHidden;
            base.Configure(itemData, languageCode);
        }

        public override bool IncludeHidden
        {
            get
            {
                return this._includeHidden;
            }
        }

        protected override bool IncludeOpenEnded
        {
            get
            {
                return this._includeOpenEnded;
            }
        }

        protected override bool MergeSelectMany
        {
            get
            {
                return this._mergeSelectMany;
            }
        }
    }
}

