﻿namespace Checkbox.Analytics.Computation
{
    using System;
    using System.Data;

    public abstract class ReportDataCalculator
    {
        public const string CALCULATED_DATA_TABLE_NAME = "Calculated";
        public const string RESPONSE_COUNTS_TABLE_NAME = "ResponseCounts";

        protected ReportDataCalculator()
        {
        }

        protected abstract DataTable Calculate(ItemAnswerAggregator answerAggregator);
        protected virtual DataTable CountResponsesAndAnswers(ItemAnswerAggregator answerAggregator)
        {
            DataTable table = this.CreateResponseCountsTable();
            foreach (int num in answerAggregator.GetItemIDs())
            {
                table.Rows.Add(new object[] { num, answerAggregator.GetResponseCount(new int?(num)), answerAggregator.GetItemAnswerCount(num) });
            }
            return table;
        }

        protected virtual DataTable CreateOutputTable()
        {
            DataTable table = new DataTable();
            table.Columns.Add("ItemID", typeof(int));
            table.Columns.Add("ItemText", typeof(string));
            table.Columns.Add("OptionID", typeof(int));
            table.Columns.Add("OptionText", typeof(string));
            table.Columns.Add("AnswerText", typeof(string));
            table.Columns.Add("AnswerCount", typeof(int));
            table.Columns.Add("AnswerPercent", typeof(decimal));
            return table;
        }

        protected virtual DataTable CreateResponseCountsTable()
        {
            DataTable table = new DataTable();
            table.Columns.Add("ItemId", typeof(int));
            table.Columns.Add("ResponseCount", typeof(int));
            table.Columns.Add("AnswerCount", typeof(int));
            return table;
        }

        public virtual DataSet GetData(ItemAnswerAggregator aggregator)
        {
            DataSet set = new DataSet();
            set.Tables.Add(this.CountResponsesAndAnswers(aggregator));
            set.Tables.Add(this.Calculate(aggregator));
            set.Tables[0].TableName = "ResponseCounts";
            set.Tables[1].TableName = "Calculated";
            return set;
        }

        protected virtual ItemResult GetItemResult(int itemID, ItemAnswerAggregator answerAggregator)
        {
            int itemAnswerCount = answerAggregator.GetItemAnswerCount(itemID);
            int responseCount = answerAggregator.GetResponseCount(new int?(itemID));
            if (responseCount > 0)
            {
                return new ItemResult(itemAnswerCount, 100M * (itemAnswerCount / responseCount));
            }
            return new ItemResult(0, 0M);
        }

        protected virtual ItemResult GetItemResult(int itemID, string answerText, ItemAnswerAggregator answerAggregator)
        {
            int itemAnswerCount = answerAggregator.GetItemAnswerCount(itemID, answerText);
            int responseCount = answerAggregator.GetResponseCount(new int?(itemID));
            if (responseCount > 0)
            {
                return new ItemResult(itemAnswerCount, 100M * (itemAnswerCount / responseCount));
            }
            return new ItemResult(0, 0M);
        }

        protected virtual ItemResult GetOptionResult(int optionID, ItemAnswerAggregator answerAggregator)
        {
            int optionAnswerCount = answerAggregator.GetOptionAnswerCount(optionID);
            int responseCount = answerAggregator.GetResponseCount(null);
            if (responseCount > 0)
            {
                return new ItemResult(optionAnswerCount, 100M * (optionAnswerCount / responseCount));
            }
            return new ItemResult(0, 0M);
        }
    }
}

