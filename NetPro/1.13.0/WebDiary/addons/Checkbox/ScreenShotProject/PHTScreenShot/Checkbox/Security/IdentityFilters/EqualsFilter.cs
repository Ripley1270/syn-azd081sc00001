﻿namespace Checkbox.Security.IdentityFilters
{
    using System;

    public class EqualsFilter : IdentityFilter
    {
        private object propertyValue;

        public EqualsFilter(string filterProperty) : base(IdentityFilterType.Equal, IdentityFilterPropertyType.IdentityProperty, filterProperty)
        {
        }

        public EqualsFilter(string filterProperty, object propertyValue) : base(IdentityFilterType.Equal, IdentityFilterPropertyType.IdentityProperty, filterProperty)
        {
            this.propertyValue = propertyValue;
        }

        public EqualsFilter(string filterProperty, object propertyValue, IdentityFilterPropertyType filterPropertyType) : base(IdentityFilterType.Equal, filterPropertyType, filterProperty)
        {
            this.propertyValue = propertyValue;
        }

        public object PropertyValue
        {
            get
            {
                return this.propertyValue;
            }
        }
    }
}

