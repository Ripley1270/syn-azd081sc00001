﻿using Checkbox.Common;
using Checkbox.Forms.Items;
using Checkbox.Forms.Logic;
using Checkbox.Forms.Piping;
using Checkbox.Forms.Piping.Tokens;
using Checkbox.Forms.Security.Principal;
using Checkbox.Globalization.Text;
using Checkbox.Management;
using Checkbox.Users;
using Prezza.Framework.Common;
using Prezza.Framework.Security.Principal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Checkbox.Forms
{
    //using Checkbox.Common;
    //using Checkbox.Forms.Items;
    //using Checkbox.Forms.Logic;
    //using Checkbox.Forms.Piping;
    //using Checkbox.Forms.Security.Principal;
    //using Checkbox.Globalization.Text;
    //using Checkbox.Management;
    //using Checkbox.Users;
    //using Prezza.Framework.Common;
    //using Prezza.Framework.Security.Principal;
    //using System;
    //using System.Collections.Generic;
    //using System.Collections.ObjectModel;
    //using System.Collections.Specialized;
    //using System.Runtime.CompilerServices;
    //using System.Xml;
    //using System.Xml.Schema;
    //using System.Xml.Serialization;

    [Serializable]
    public class Response : IDisposable, IXmlSerializable
    {
        private readonly Dictionary<int, int> _dynamicItemNumbers;
        private readonly Dictionary<int, Item> _items = new Dictionary<int, Item>();
        private int? _nextPagePump;
        private readonly List<ResponsePage> _pages = new List<ResponsePage>();
        private readonly Checkbox.Forms.Piping.PipeMediator _pipeMediator = new Checkbox.Forms.Piping.PipeMediator();
        private readonly List<ItemToken> _responsePipes = new List<ItemToken>();
        private ResponseState _responseState = new ResponseState();
        private Checkbox.Forms.Logic.RulesEngine _rulesEngine;
        private readonly Dictionary<int, int> _staticItemNumbers;
        private readonly Stack<ResponsePage> _visitedPages = new Stack<ResponsePage>();

        public event ResponsePageChangedHandler PageChanged;

        public event ResponseCompletedHandler ResponseCompleted;

        public event StateRestoredHandler StateRestored;

        public Response()
        {
            this._responseState.LoadSchemaOnly();
            this._dynamicItemNumbers = new Dictionary<int, int>();
            this._staticItemNumbers = new Dictionary<int, int>();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                foreach (Item item in this.Items.Values)
                {
                    item.Dispose();
                }
                if (this._rulesEngine != null)
                {
                    this._rulesEngine.Dispose();
                }
                if (this._responseState != null)
                {
                    this._responseState.Dispose();
                }
                if (this._pipeMediator != null)
                {
                    this._pipeMediator.Dispose();
                }
            }
        }

        private ResponsePage FindPage(int pageID)
        {
            foreach (ResponsePage page in this.Pages)
            {
                if (page.PageId == pageID)
                {
                    return page;
                }
            }
            return null;
        }

        private void GenerateItemNumbers()
        {
            this.PopulateDynamicItemNumberDictionary();
            this.PopulateStaticItemNumberDictionary();
        }

        public int GetCurrentPageDisplayNumber()
        {
            if (this.UseDynamicPageNumbers)
            {
                return this.VisitedPageCount;
            }
            return this.CurrentPage.Position;
        }

        public Item GetItem(int itemID)
        {
            if (!this._items.ContainsKey(itemID))
            {
                return null;
            }
            return this._items[itemID];
        }

        public int? GetItemNumber(int itemID)
        {
            if (this.UseDynamicItemNumbers)
            {
                if (this._dynamicItemNumbers.ContainsKey(itemID))
                {
                    return new int?(this._dynamicItemNumbers[itemID]);
                }
                return null;
            }
            if (this._staticItemNumbers.ContainsKey(itemID))
            {
                return new int?(this._staticItemNumbers[itemID]);
            }
            return null;
        }

        public List<int> GetItemOptionOrder(int itemID)
        {
            return this.State.GetItemOptionOrder(itemID);
        }

        public int GetPageCountDisplayNumber()
        {
            if (this.UseDynamicPageNumbers)
            {
                return this.EnabledPageCount;
            }
            return this.PageCount;
        }

        public List<int> GetPageItemOrder(int pageID)
        {
            return this.State.GetPageItemOrder(pageID);
        }

        private NameValueCollection GetResponseProperties()
        {
            NameValueCollection values = new NameValueCollection();
            ResponseProperties properties = new ResponseProperties();
            properties.Initialize(this);
            foreach (string str in ResponseProperties.PropertyNames)
            {
                values[str] = properties.GetStringValue(str);
            }
            return values;
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void GoToCompletionPage()
        {
            this.CurrentPageIndex = this.Pages.Count - 1;
        }

        public void Initialize(string ipAddress, string networkUser, string languageCode, bool isTest, string invitee, ExtendedPrincipal respondent)
        {
            Guid? respondentGuid = null;
            if (respondent is AnonymousRespondent)
            {
                respondentGuid = ((AnonymousRespondent) respondent).Guid;
            }
            if (!ApplicationManager.AppSettings.LogNetworkUser)
            {
                networkUser = string.Empty;
            }
            this._visitedPages.Clear();
            this._responseState.InsertResponseData(Guid.NewGuid(), this.ResponseTemplateID, this.Pages[0].PageId, ipAddress, respondent.Identity.Name, networkUser, languageCode, respondentGuid, isTest, this.AnonymizeResponses, invitee);
            this._responseState.Save();
            foreach (Item item in this.Items.Values)
            {
                if (item is ResponseItem)
                {
                    ((ResponseItem) item).AnswerData = this.State;
                }
            }
            this._pipeMediator.Initialize(this, respondent);
            this.RunAllRules();
            this.GenerateItemNumbers();
            if (this.CurrentPage != null)
            {
                this._visitedPages.Push(this.CurrentPage);
                this.State.PushPageLog(this.CurrentPage.PageId);
            }
            if (this.StateRestored != null)
            {
                this.StateRestored(this, new ResponseStateEventArgs(this._responseState));
            }
        }

        public void LoadCurrentPage()
        {
            if (this.CurrentPage != null)
            {
                this.CurrentPage.OnLoad(false);
            }
        }

        public void MoveNext()
        {
            this.MoveNext(true);
        }

        private void MoveNext(bool validateCurrentPage)
        {
            this.CurrentPage.OnLoad(false);
            if (!validateCurrentPage || this.CurrentPage.Valid)
            {
                bool flag4;
                bool flag = false;
                int position = this.Pages[this.CurrentPageIndex].Position;
                bool flag2 = this.CurrentPageIndex < (this.Pages.Count - 1);
                for (bool flag3 = flag2; flag3; flag3 = flag2 && flag4)
                {
                    this.CurrentPage.OnUnLoad();
                    if (this._nextPagePump.HasValue)
                    {
                        for (int j = this.CurrentPageIndex + 1; j < this._nextPagePump.Value; j++)
                        {
                            this.Pages[j].Excluded = true;
                        }
                        this.CurrentPageIndex = this._nextPagePump.Value;
                        this.CurrentPage.RunRules(RuleEventTrigger.Load);
                        if (this.CurrentPage.Excluded)
                        {
                            for (int k = this.CurrentPageIndex + 1; k < this.Pages.Count; k++)
                            {
                                if (!this.Pages[k].Excluded)
                                {
                                    this.CurrentPageIndex = k;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        this.CurrentPageIndex++;
                    }
                    int num4 = this._nextPagePump.HasValue ? this.CurrentPageIndex : 0;
                    for (int i = num4; i < this.Pages.Count; i++)
                    {
                        this.Pages[i].OnLoad(this.CurrentPageIndex == i);
                    }
                    this._nextPagePump = null;
                    flag2 = this.CurrentPageIndex < (this.Pages.Count - 1);
                    flag4 = this.CurrentPage.Excluded || (this.CurrentPage.Items.Count == 0);
                }
                if (this.CurrentPageIndex >= (this.Pages.Count - 1))
                {
                    flag = true;
                    this.CurrentPageIndex = this.Pages.Count - 1;
                }
                this.State.LastModified = new DateTime?(DateTime.Now);
                this.State.LastPageViewed = new int?(this.CurrentPage.PageId);
                this.State.PushPageLog(this.CurrentPage.PageId);
                this._visitedPages.Push(this.CurrentPage);
                if (flag)
                {
                    this.State.ResponseData["IsComplete"] = true;
                    if (!this.State.DateCompleted.HasValue)
                    {
                        this.State.DateCompleted = new DateTime?(DateTime.Now);
                    }
                }
                this._pipeMediator.Initialize(this, this.Respondent);
                this.State.Save();
                if (flag && (this.ResponseCompleted != null))
                {
                    this.ResponseCompleted(this, new ResponseStateEventArgs(this.State));
                }
                int newPage = this.CurrentPage.Position;
                if (this.PageChanged != null)
                {
                    this.PageChanged(this, new ResponsePageChangedEventArgs(position, newPage));
                }
                if (this.UseDynamicItemNumbers)
                {
                    this.PopulateDynamicItemNumberDictionary();
                }
            }
        }

        public void MovePrevious()
        {
            int position = this.Pages[this.CurrentPageIndex].Position;
            do
            {
                this._visitedPages.Pop();
                this.CurrentPageIndex = this.Pages.IndexOf(this._visitedPages.Peek());
                if (!this.CurrentPage.Excluded)
                {
                    this.CurrentPage.OnLoad(false);
                }
                this.State.PopPageLog();
                this.State.LastPageViewed = new int?(this.CurrentPage.PageId);
                this.State.Save();
            }
            while ((!this.CurrentPageHasVisibleItems || this.CurrentPage.Excluded) && this.CanNavigateBack);
            int newPage = this.CurrentPage.Position;
            if ((!this.CurrentPageHasVisibleItems || this.CurrentPage.Excluded) && !this.CanNavigateBack)
            {
                newPage = position;
            }
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new ResponsePageChangedEventArgs(position, newPage));
            }
            if (this.UseDynamicItemNumbers)
            {
                this.PopulateDynamicItemNumberDictionary();
            }
        }

        public void MoveToPage(int pageID)
        {
            int num = -1;
            for (int i = 0; i < this._pages.Count; i++)
            {
                if ((this._pages[i] != null) && (this._pages[i].PageId == pageID))
                {
                    num = i;
                    break;
                }
            }
            int currentPageIndex = this.CurrentPageIndex;
            if (num != this.CurrentPageIndex)
            {
                if (num <= this.CurrentPageIndex)
                {
                    while (this.CurrentPageIndex > num)
                    {
                        this.MovePrevious();
                        if (currentPageIndex == this.CurrentPageIndex)
                        {
                            return;
                        }
                        currentPageIndex = this.CurrentPageIndex;
                    }
                }
                else
                {
                    while (this.CurrentPageIndex < num)
                    {
                        this.MoveNext(false);
                        if (currentPageIndex == this.CurrentPageIndex)
                        {
                            return;
                        }
                        currentPageIndex = this.CurrentPageIndex;
                    }
                }
            }
        }

        public void MoveToStart()
        {
            this._visitedPages.Clear();
            this.State.ClearPageLog();
            this.CurrentPageIndex = 0;
            this.State.LastPageViewed = new int?(this.CurrentPage.PageId);
            this.State.Save();
        }

        private void PopulateDynamicItemNumberDictionary()
        {
            int num = 1;
            this._dynamicItemNumbers.Clear();
            foreach (ResponsePage page in this.Pages)
            {
                if ((page.Position > 0) && !page.Excluded)
                {
                    foreach (Item item in page.Items)
                    {
                        if ((item is IAnswerable) && !item.Excluded)
                        {
                            this._dynamicItemNumbers[item.ID] = num;
                            num++;
                        }
                    }
                }
            }
        }

        private void PopulateStaticItemNumberDictionary()
        {
            int num = 1;
            this._staticItemNumbers.Clear();
            foreach (ResponsePage page in this.Pages)
            {
                if (page.Position > 0)
                {
                    foreach (Item item in page.Items)
                    {
                        if ((item is IAnswerable) && !item.Excluded)
                        {
                            this._staticItemNumbers[item.ID] = num;
                            num++;
                        }
                    }
                }
            }
        }

        public void PrimeNextPage(int pageIndex)
        {
            this._nextPagePump = new int?(pageIndex);
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void Restore(ResponseState state)
        {
            if (((int) state.ResponseData["ResponseTemplateID"]) != this.ResponseTemplateID)
            {
                throw new InvalidOperationException("A ResponseTemplate mismatch was detected");
            }
            this._responseState = state;
            this.RestoreVisitedPages();
            foreach (Item item in this.Items.Values)
            {
                if (item is ResponseItem)
                {
                    ((ResponseItem) item).AnswerData = this.State;
                }
            }
            this._pipeMediator.Initialize(this, this.Respondent);
            this.RunAllRules();
            this.GenerateItemNumbers();
            if (this.StateRestored != null)
            {
                this.StateRestored(this, new ResponseStateEventArgs(this._responseState));
            }
        }

        public void Restore(Guid responseGuid)
        {
            this._responseState.Load(responseGuid);
            if (((int) this.State.ResponseData["ResponseTemplateID"]) != this.ResponseTemplateID)
            {
                throw new InvalidOperationException("A ResponseTemplate mismatch was detected");
            }
            this.RestoreVisitedPages();
            foreach (Item item in this.Items.Values)
            {
                if (item is ResponseItem)
                {
                    ((ResponseItem) item).AnswerData = this.State;
                }
            }
            this._pipeMediator.Initialize(this, this.Respondent);
            this.RunAllRules();
            this.GenerateItemNumbers();
            if (this.StateRestored != null)
            {
                this.StateRestored(this, new ResponseStateEventArgs(this._responseState));
            }
        }

        private void RestoreVisitedPages()
        {
            int[] visitedPages = this.State.VisitedPages;
            this._visitedPages.Clear();
            for (int i = 0; i < visitedPages.Length; i++)
            {
                ResponsePage item = this.FindPage(visitedPages[i]);
                if (item != null)
                {
                    this._visitedPages.Push(item);
                }
            }
            this.CurrentPageIndex = 0;
            if (this.State.ResponseData["LastPageViewed"] != DBNull.Value)
            {
                ResponsePage page2 = this.FindPage((int) this.State.ResponseData["LastPageViewed"]);
                if (page2 != null)
                {
                    this.CurrentPageIndex = this.Pages.IndexOf(page2);
                }
            }
        }

        private void RunAllRules()
        {
            foreach (ResponsePage page in this.Pages)
            {
                page.OnLoad(false);
            }
        }

        public void SaveCurrentState()
        {
            this.State.LastModified = new DateTime?(DateTime.Now);
            this.State.Save();
        }

        public void SaveItemOptionOrder(int itemID, int optionID, int position)
        {
            this.State.SaveItemOptionOrder(itemID, optionID, position);
        }

        public void SavePageItemOrder(int pageID, int itemID, int position)
        {
            this.State.SavePageItemOrder(pageID, itemID, position);
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("response");
            writer.WriteAttributeString("responseId", this.ID.ToString());
            writer.WriteStartElement("responseProperties");
            XmlUtility.SerializeNameValueCollection(writer, this.GetResponseProperties(), true);
            writer.WriteEndElement();
            writer.WriteStartElement("pages");
            foreach (ResponsePage page in this.ResponsePages)
            {
                page.WriteXml(writer);
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        internal bool AllowBack { get; set; }

        internal bool AnonymizeResponses { get; set; }

        public bool CanNavigateBack
        {
            get
            {
                if (((this.AllowBack && (this.CurrentPage.Position > 1)) && (this.CurrentPageIndex != (this.Pages.Count - 1))) && (this._visitedPages.Count != 0))
                {
                    if ((this._visitedPages.Count == 1) && (this._visitedPages.Peek().Position == this.CurrentPage.Position))
                    {
                        return false;
                    }
                    for (int i = this.Pages.IndexOf(this.CurrentPage); i > 0; i--)
                    {
                        if (!this.Pages[i - 1].Excluded && (this.Pages[i - 1].Position > 0))
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        public bool CanNavigateForward
        {
            get
            {
                if (this.Pages.IndexOf(this.CurrentPage) == (this.Pages.Count - 1))
                {
                    return false;
                }
                return true;
            }
        }

        public bool Completed
        {
            get
            {
                return this.State.IsComplete;
            }
        }

        public bool CompleteOnNext
        {
            get
            {
                int position = this.Pages[this.Pages.Count - 1].Position;
                if (this.CurrentPage.Position < (position - 1))
                {
                    return false;
                }
                return true;
            }
        }

        public ResponsePage CurrentPage
        {
            get
            {
                return this.Pages[this.CurrentPageIndex];
            }
        }

        private bool CurrentPageHasVisibleItems
        {
            get
            {
                foreach (Item item in this.CurrentPage.Items)
                {
                    if (item.Visible)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public int CurrentPageIndex { get; private set; }

        public DateTime? DateCompleted
        {
            get
            {
                return this.State.DateCompleted;
            }
        }

        public DateTime? DateCreated
        {
            get
            {
                return this.State.DateCreated;
            }
        }

        public string Description
        {
            get
            {
                return TextManager.GetText(this.DescriptionTextID, this.LanguageCode);
            }
        }

        internal string DescriptionTextID { get; set; }

        public int EnabledPageCount
        {
            get
            {
                int num = 0;
                for (int i = 0; i < (this.Pages.Count - 1); i++)
                {
                    if ((this.Pages[i].Position != 0) && !this.Pages[i].Excluded)
                    {
                        num++;
                    }
                }
                return num;
            }
        }

        public string FinishButtonTextID { get; set; }

        public Guid? GUID
        {
            get
            {
                return this.State.GUID;
            }
        }

        public long? ID
        {
            get
            {
                return this.State.ResponseID;
            }
        }

        public string Invitee
        {
            get
            {
                return this.State.Invitee;
            }
        }

        public string IPAddress
        {
            get
            {
                return this.State.IPAddress;
            }
        }

        internal Dictionary<int, Item> Items
        {
            get
            {
                return this._items;
            }
        }

        public string LanguageCode
        {
            get
            {
                return this.State.LanguageCode;
            }
            set
            {
                this.State.LanguageCode = value;
            }
        }

        public DateTime? LastModified
        {
            get
            {
                return this.State.LastModified;
            }
        }

        public int? LastPageViewed
        {
            get
            {
                return this.State.LastPageViewed;
            }
        }

        public string NetworkUser
        {
            get
            {
                return this.State.NetworkUser;
            }
        }

        public string NextButtonTextID { get; set; }

        public int PageCount
        {
            get
            {
                int num = 0;
                for (int i = 0; i < (this.Pages.Count - 1); i++)
                {
                    if (this.Pages[i].Position > 0)
                    {
                        num++;
                    }
                }
                return num;
            }
        }

        internal List<ResponsePage> Pages
        {
            get
            {
                return this._pages;
            }
        }

        public Checkbox.Forms.Piping.PipeMediator PipeMediator
        {
            get
            {
                return this._pipeMediator;
            }
        }

        public string PreviousButtonTextID { get; set; }

        public ExtendedPrincipal Respondent
        {
            get
            {
                ExtendedPrincipal user = null;
                if (this.RespondentGuid.HasValue)
                {
                    return new AnonymousRespondent(this.RespondentGuid);
                }
                if (this.UniqueIdentifier != null)
                {
                    user = (ExtendedPrincipal) UserManager.GetUser(this.UniqueIdentifier);
                }
                return user;
            }
        }

        public Guid? RespondentGuid
        {
            get
            {
                if (this._responseState != null)
                {
                    return this._responseState.RespondentGuid;
                }
                return null;
            }
        }

        public ReadOnlyCollection<ResponsePage> ResponsePages
        {
            get
            {
                return new ReadOnlyCollection<ResponsePage>(this._pages);
            }
        }

        public List<ItemToken> ResponsePipes
        {
            get
            {
                return this._responsePipes;
            }
        }

        public Guid ResponseTemplateGuid { get; internal set; }

        public int ResponseTemplateID { get; internal set; }

        internal Checkbox.Forms.Logic.RulesEngine RulesEngine
        {
            get
            {
                if (this._rulesEngine == null)
                {
                    throw new ApplicationException("RulesEngine was not initialized");
                }
                return this._rulesEngine;
            }
            set
            {
                this._rulesEngine = value;
            }
        }

        public string SaveProgressButtonTextID { get; set; }

        public bool ScoringEnabled { get; internal set; }

        public bool ShowItemNumbers { get; set; }

        private ResponseState State
        {
            get
            {
                return this._responseState;
            }
        }

        public bool StateIsValid
        {
            get
            {
                return ((this.State != null) && (this.State.ResponseData != null));
            }
        }

        public ResponseStatus Status
        {
            get
            {
                return ResponseStatus.INPROGRESS;
            }
        }

        public string TemplateName { get; set; }

        public string Title
        {
            get
            {
                string text = TextManager.GetText(this.TitleTextID, this.LanguageCode);
                if (Utilities.IsNotNullOrEmpty(text))
                {
                    return text;
                }
                return this.TemplateName;
            }
        }

        internal string TitleTextID { get; set; }

        public double? TotalScore
        {
            get
            {
                if (!this.ScoringEnabled)
                {
                    return null;
                }
                double num = 0.0;
                foreach (Item item in this._items.Values)
                {
                    if (item is IScored)
                    {
                        num += ((IScored) item).GetScore();
                    }
                }
                return new double?(num);
            }
        }

        public string UniqueIdentifier
        {
            get
            {
                return this.State.UniqueIdentifier;
            }
        }

        internal bool UseDynamicItemNumbers { get; set; }

        internal bool UseDynamicPageNumbers { get; set; }

        public int VisitedPageCount
        {
            get
            {
                int num = 0;
                foreach (ResponsePage page in this._visitedPages)
                {
                    if ((page.Position != 0) && !page.Excluded)
                    {
                        num++;
                    }
                }
                return num;
            }
        }

        public ReadOnlyCollection<ResponsePage> VistedPageStack
        {
            get
            {
                return new ReadOnlyCollection<ResponsePage>(this._visitedPages.ToArray());
            }
        }

        public delegate void ResponseCompletedHandler(object sender, ResponseStateEventArgs e);

        public delegate void ResponsePageChangedHandler(object sender, ResponsePageChangedEventArgs e);

        public delegate void StateRestoredHandler(object sender, ResponseStateEventArgs e);
    }
}

