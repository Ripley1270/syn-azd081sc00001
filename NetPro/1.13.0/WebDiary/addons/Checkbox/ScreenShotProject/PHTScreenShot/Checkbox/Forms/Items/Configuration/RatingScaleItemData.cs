﻿using Checkbox.Forms.Items;
using Checkbox.Globalization.Text;
using Prezza.Framework.Data;
using Prezza.Framework.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Data;

namespace Checkbox.Forms.Items.Configuration
{
    [Serializable]
    public class RatingScaleItemData : Select1Data
    {
        private bool _displayNotApplicable;
        private int _endValue;
        private int _startValue;
        private PHTScreenShotSession _screenShotSession;

        protected override ItemData Copy()
        {
            RatingScaleItemData data = (RatingScaleItemData) base.Copy();
            if (data != null)
            {
                data.StartValue = this.StartValue;
                data.EndValue = this.EndValue;
                data.DisplayNotApplicable = this.DisplayNotApplicable;
            }
            return data;
        }

        protected override void Create(IDbTransaction t)
        {
            base.Create(t);
            if (base.ID <= 0)
            {
                throw new Exception("DataID must be specified to create item data.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_InsertRS");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("StartValue", DbType.Int32, this._startValue);
            storedProcCommandWrapper.AddInParameter("EndValue", DbType.Int32, this._endValue);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, this.SubTextID);
            storedProcCommandWrapper.AddInParameter("IsRequired", DbType.Int32, this.IsRequired ? 1 : 0);
            storedProcCommandWrapper.AddInParameter("DisplayNotApplicableText", DbType.Boolean, this.DisplayNotApplicable);
            storedProcCommandWrapper.AddInParameter("StartTextID", DbType.String, this.StartTextID);
            storedProcCommandWrapper.AddInParameter("MidTextID", DbType.String, this.MidTextID);
            storedProcCommandWrapper.AddInParameter("EndTextID", DbType.String, this.EndTextID);
            storedProcCommandWrapper.AddInParameter("NotApplicableTextID", DbType.String, this.NotApplicableTextID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override Item CreateItem()
        {
            return new RatingScale();
        }

        public override ItemTextDecorator CreateTextDecorator(string languageCode)
        {
            return new RatingScaleItemTextDecorator(this, languageCode);
        }

        private ListOptionData FindNotApplicableOption()
        {
            foreach (ListOptionData data in this.Options)
            {
                if (data.IsOther)
                {
                    return data;
                }
            }
            return null;
        }

        private void GenerateNotApplicableOption()
        {
            ListOptionData data = this.FindNotApplicableOption();
            if (this.DisplayNotApplicable)
            {
                if (data != null)
                {
                    base.UpdateOption(data.OptionID, data.Alias, data.IsDefault, this.NextOptionPosition(), 0.0, true);
                }
                else
                {
                    base.AddOption(string.Empty, false, this.NextOptionPosition(), 0.0, true);
                }
            }
            else if (data != null)
            {
                base.RemoveOption(data.OptionID);
            }
        }

        protected virtual void GenerateOptions()
        {
            int num;
            int num2;
            bool flag = false;
            if (this._startValue > this._endValue)
            {
                num = this._endValue;
                num2 = this._startValue;
                flag = true;
            }
            else
            {
                num = this._startValue;
                num2 = this._endValue;
            }
            List<ListOptionData> list = new List<ListOptionData>();
            List<ListOptionData> list2 = new List<ListOptionData>();
            List<double> list3 = new List<double>();
            foreach (ListOptionData data in this.Options)
            {
                list3.Add(data.Points);
                if (((data.Points < num) || (data.Points > num2)) && !data.IsOther)
                {
                    list.Add(data);
                }
                else
                {
                    list2.Add(data);
                }
            }
            foreach (ListOptionData data2 in list)
            {
                base.RemoveOption(data2.OptionID);
            }
            foreach (ListOptionData data3 in list2)
            {
                int num3;
                if (flag)
                {
                    num3 = (num2 - ((int) data3.Points)) + 1;
                }
                else
                {
                    num3 = (((int) data3.Points) - num) + 1;
                }
                data3.Position = num3;
                base.UpdateOption(data3.OptionID, data3.Alias, data3.IsDefault, num3, data3.Points, data3.IsOther);
            }
            for (int i = num; i <= num2; i++)
            {
                bool flag2 = false;
                foreach (ListOptionData data4 in list2)
                {
                    if (data4.Points == i)
                    {
                        flag2 = true;
                        break;
                    }
                }
                if (!flag2)
                {
                    int num5;
                    if (flag)
                    {
                        num5 = (num2 - i) + 1;
                    }
                    else
                    {
                        num5 = (i - num) + 1;
                    }
                    base.AddOption(string.Empty, false, num5, (double) i, false);
                }
            }
            this.GenerateNotApplicableOption();
        }

        protected override DataSet GetConcreteConfigurationDataSet()
        {
            if (base.ID <= 0)
            {
                throw new Exception("No DataID specified.");
            }
            try
            {
                string calledStoredProcedure = string.Empty;
                this._screenShotSession = new PHTScreenShotSession();
                if (this._screenShotSession.ScreenShotMode)
                    calledStoredProcedure = "PHTScreenShotItemDataGetRS";
                else
                    calledStoredProcedure = "ckbx_ItemData_GetRS";

                Database database = DatabaseFactory.CreateDatabase();
                //DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_GetRS");
                DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper(calledStoredProcedure);
                storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
                DataSet dataSet = new DataSet();
                database.LoadDataSet(storedProcCommandWrapper, dataSet, this.ConfigurationDataTableNames);
                if (dataSet.Tables.Contains("ItemOptions"))
                {
                    dataSet.Tables["ItemOptions"].PrimaryKey = new DataColumn[] { dataSet.Tables["ItemOptions"].Columns["OptionID"], dataSet.Tables["ItemOptions"].Columns["ItemID"] };
                }
                if (dataSet.Tables.Contains("ItemLists"))
                {
                    dataSet.Tables["ItemLists"].PrimaryKey = new DataColumn[] { dataSet.Tables["ItemLists"].Columns["ItemID"], dataSet.Tables["ItemLists"].Columns["ListID"] };
                }
                return dataSet;
            }
            catch (Exception exception)
            {
                if (ExceptionPolicy.HandleException(exception, "BusinessProtected"))
                {
                    throw;
                }
                return null;
            }
        }

        protected override void LoadFromDataRow(DataRow data)
        {
            if (data == null)
            {
                throw new Exception("Unable to load data from null DataRow.");
            }
            this._startValue = DbUtility.GetValueFromDataRow<int>(data, "StartValue", 1);
            this._endValue = DbUtility.GetValueFromDataRow<int>(data, "EndValue", 5);
            this.IsRequired = DbUtility.GetValueFromDataRow<int>(data, "IsRequired", 0) == 1;
            this._displayNotApplicable = DbUtility.GetValueFromDataRow<bool>(data, "DisplayNotApplicableText", false);
        }

        public override DataSet LoadTextData()
        {
            DataSet ds = base.LoadTextData();
            this.MergeTextData(ds, TextManager.GetTextData(this.StartTextID), this.TextTableName, "startText", this.TextIDPrefix, base.ID.Value);
            this.MergeTextData(ds, TextManager.GetTextData(this.MidTextID), this.TextTableName, "midText", this.TextIDPrefix, base.ID.Value);
            this.MergeTextData(ds, TextManager.GetTextData(this.EndTextID), this.TextTableName, "endText", this.TextIDPrefix, base.ID.Value);
            this.MergeTextData(ds, TextManager.GetTextData(this.NotApplicableTextID), this.TextTableName, "notApplicableText", this.TextIDPrefix, base.ID.Value);
            return ds;
        }

        private int NextOptionPosition()
        {
            if (this.Options.Count == 0)
            {
                return 1;
            }
            int position = this.Options[0].Position;
            for (int i = 1; i < this.Options.Count; i++)
            {
                if (position < this.Options[i].Position)
                {
                    position = this.Options[i].Position;
                }
            }
            return (position + 1);
        }

        protected override void Update(IDbTransaction t)
        {
            base.Update(t);
            if (base.ID <= 0)
            {
                throw new Exception("DataID must be specified to update item data.");
            }
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper storedProcCommandWrapper = database.GetStoredProcCommandWrapper("ckbx_ItemData_UpdateRS");
            storedProcCommandWrapper.AddInParameter("ItemID", DbType.Int32, base.ID);
            storedProcCommandWrapper.AddInParameter("StartValue", DbType.Int32, this._startValue);
            storedProcCommandWrapper.AddInParameter("EndValue", DbType.Int32, this._endValue);
            storedProcCommandWrapper.AddInParameter("TextID", DbType.String, this.TextID);
            storedProcCommandWrapper.AddInParameter("SubTextID", DbType.String, this.SubTextID);
            storedProcCommandWrapper.AddInParameter("IsRequired", DbType.Int32, this.IsRequired ? 1 : 0);
            storedProcCommandWrapper.AddInParameter("DisplayNotApplicableText", DbType.Boolean, this.DisplayNotApplicable);
            storedProcCommandWrapper.AddInParameter("StartTextID", DbType.String, this.StartTextID);
            storedProcCommandWrapper.AddInParameter("MidTextID", DbType.String, this.MidTextID);
            storedProcCommandWrapper.AddInParameter("EndTextID", DbType.String, this.EndTextID);
            storedProcCommandWrapper.AddInParameter("NotApplicableTextID", DbType.String, this.NotApplicableTextID);
            database.ExecuteNonQuery(storedProcCommandWrapper, t);
        }

        protected override void UpdateOtherOption(ListOptionData otherOption)
        {
        }

        public override bool AllowOther
        {
            get
            {
                return true;
            }
            set
            {
            }
        }

        public override string DataTableName
        {
            get
            {
                return "RatingScaleData";
            }
        }

        public bool DisplayNotApplicable
        {
            get
            {
                return this._displayNotApplicable;
            }
            set
            {
                this._displayNotApplicable = value;
                this.GenerateOptions();
            }
        }

        public string EndTextID
        {
            get
            {
                return this.GetTextID("endText");
            }
        }

        public int EndValue
        {
            get
            {
                return this._endValue;
            }
            set
            {
                this._endValue = value;
                this.GenerateOptions();
            }
        }

        public string MidTextID
        {
            get
            {
                return this.GetTextID("midText");
            }
        }

        public string NotApplicableTextID
        {
            get
            {
                return this.GetTextID("otherText");
            }
        }

        public override bool Randomize
        {
            get
            {
                return false;
            }
            set
            {
            }
        }

        public string StartTextID
        {
            get
            {
                return this.GetTextID("startText");
            }
        }

        public int StartValue
        {
            get
            {
                return this._startValue;
            }
            set
            {
                this._startValue = value;
                this.GenerateOptions();
            }
        }

        public override string TextIDPrefix
        {
            get
            {
                return "scaleItemData";
            }
        }
    }
}

