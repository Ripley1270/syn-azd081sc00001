﻿namespace Checkbox.Security
{
    using Checkbox.Security.IdentityFilters;
    using Prezza.Framework.Data;
    using System;

    public class IdentitySortOrder : SortOrder
    {
        private IdentityFilterPropertyType propertyType;

        public IdentitySortOrder(string sortProperty) : this(sortProperty, true, IdentityFilterPropertyType.IdentityProperty)
        {
        }

        public IdentitySortOrder(string sortProperty, bool sortAscending) : this(sortProperty, true, IdentityFilterPropertyType.IdentityProperty)
        {
        }

        public IdentitySortOrder(string sortProperty, bool sortAscending, IdentityFilterPropertyType propertyType) : base(sortProperty, sortAscending)
        {
            this.propertyType = propertyType;
        }

        public IdentityFilterPropertyType PropertyType
        {
            get
            {
                return this.propertyType;
            }
        }
    }
}

