﻿namespace Checkbox.Forms.Items
{
    using Checkbox.Common;
    using Checkbox.Globalization.Text;
    using System;
    using System.IO;
    using System.Xml;
    using System.Xml.Xsl;

    public abstract class XsltItemFormatter : IItemFormatter
    {
        protected XsltItemFormatter()
        {
        }

        public string Format(Item item, string format)
        {
            HtmlStrippingStringWriter w = new HtmlStrippingStringWriter {
                StripHtml = "text".Equals(format, StringComparison.InvariantCultureIgnoreCase)
            };
            XmlTextWriter writer = new XmlTextWriter(w) {
                Formatting = Formatting.None
            };
            item.WriteXml(writer);
            writer.Flush();
            writer.Close();
            XmlTextReader reader = new XmlTextReader(w.ToString(), XmlNodeType.Document, null);
            StringWriter writer3 = new StringWriter();
            XmlTextWriter writer4 = new XmlTextWriter(writer3);
            XslCompiledTransform transform = this.LoadTransform(format);
            XsltArgumentList argumentList = this.GetArgumentList(item);
            transform.Transform((XmlReader) reader, argumentList, (XmlWriter) writer4);
            writer.Flush();
            writer.Close();
            return writer3.ToString();
        }

        protected virtual XsltArgumentList GetArgumentList(Item item)
        {
            XsltArgumentList list = new XsltArgumentList();
            list.AddParam("noAnswerText", string.Empty, TextManager.GetText("/controlText/xslItemFormatter/noAnswerText", item.LanguageCode, "Not Answered", new string[0]));
            return list;
        }

        protected virtual string GetXslFilePath(string format)
        {
            return string.Empty;
        }

        protected virtual XslCompiledTransform LoadTransform(string format)
        {
            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(this.GetXslFilePath(format));
            return transform;
        }
    }
}

