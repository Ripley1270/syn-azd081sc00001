﻿namespace Checkbox.Management.Licensing.Limits
{
    using Checkbox.Globalization.Text;
    using Prezza.Framework.Data;
    using Prezza.Framework.Data.QueryCriteria;
    using Prezza.Framework.Data.QueryParameters;
    using Prezza.Framework.ExceptionHandling;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.InteropServices;

    public abstract class RolePermissionLimit : NumericLicenseLimit
    {
        protected RolePermissionLimit()
        {
        }

        protected override long GetCurrentCount()
        {
            return (long) this.GetIdentitiesWithRolePermission().Count;
        }

        protected virtual List<string> GetCurrentUsersInRole()
        {
            return this.GetIdentitiesWithRolePermission();
        }

        private List<string> GetIdentitiesWithRolePermission()
        {
            SelectQuery query = new SelectQuery("ckbx_IdentityRoles");
            query.AddTableJoin("ckbx_Role", QueryJoinType.Inner, "RoleID", "ckbx_IdentityRoles", "RoleID");
            query.AddTableJoin("ckbx_RolePermissions", QueryJoinType.Left, "RoleID", "ckbx_Role", "RoleID");
            query.AddTableJoin("ckbx_Permission", QueryJoinType.Left, "PermissionID", "ckbx_RolePermissions", "PermissionID");
            query.AddLiteralParameter("Distinct UniqueIdentifier", "UniqueIdentifier");
            query.AddCriterion(new QueryCriterion(new SelectParameter("PermissionName", null, "ckbx_Permission"), CriteriaOperator.EqualTo, new LiteralParameter("'" + this.PermissionName + "'")));
            query.AddCriterion(new QueryCriterion(new SelectParameter("RoleName", null, "ckbx_Role"), CriteriaOperator.EqualTo, new LiteralParameter("'System Administrator'")));
            query.CriteriaJoinType = CriteriaJoinType.Or;
            Database database = DatabaseFactory.CreateDatabase();
            DBCommandWrapper sqlStringCommandWrapper = database.GetSqlStringCommandWrapper(query.ToString());
            List<string> list = new List<string>();
            IDataReader reader = database.ExecuteReader(sqlStringCommandWrapper);
            try
            {
                while (reader.Read())
                {
                    list.Add(DbUtility.GetValueFromDataReader<string>(reader, "UniqueIdentifier", string.Empty));
                }
            }
            catch (Exception exception)
            {
                ExceptionPolicy.HandleException(new Exception("Unable to get user count for Survey Editor Limit."), "BusinessProtected");
                ExceptionPolicy.HandleException(exception, "BusinessProtected");
            }
            finally
            {
                if (reader != null)
                {
                    reader.Dispose();
                }
            }
            return list;
        }

        public override LimitValidationResult ProtectedValidate(out string message)
        {
            long currentCount = base.CurrentCount;
            if (currentCount < 0L)
            {
                message = TextManager.GetText("/rolePermissionLimit/unableToGetCount", TextManager.DefaultLanguage) ?? "Unable to determine the number of in roles that have {0} permission";
                message = string.Format(message, this.PermissionName);
                return LimitValidationResult.UnableToEvaluate;
            }
            if (!this.RuntimeLimitValue.HasValue)
            {
                message = string.Empty;
                return LimitValidationResult.LimitNotReached;
            }
            if (currentCount > this.RuntimeLimitValue.Value)
            {
                message = TextManager.GetText("/rolePermissionLimit/limitExceeded", TextManager.DefaultLanguage) ?? "{0} users were found in roles with the {1} permission, but your license only allows {2} users in this role.  To correct this, remove users from these roles in the User Manager.";
                message = string.Format(message, currentCount, this.PermissionName, this.RuntimeLimitValue);
                return LimitValidationResult.LimitExceeded;
            }
            if (currentCount == this.RuntimeLimitValue.Value)
            {
                message = string.Empty;
                return LimitValidationResult.LimitReached;
            }
            message = string.Empty;
            return LimitValidationResult.LimitNotReached;
        }

        public abstract string PermissionName { get; }
    }
}

