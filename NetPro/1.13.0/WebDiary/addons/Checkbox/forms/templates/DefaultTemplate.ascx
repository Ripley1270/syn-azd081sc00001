﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="CheckboxWeb.Forms.Templates.DefaultTemplate" Codebehind="DefaultTemplate.ascx.cs" %>
<%@ Register TagPrefix="cc1" Namespace="Checkbox.Web.Forms.UI.Templates" Assembly="Checkbox.Web" %>

<%-- This file is used by Checkbox when a survey page has no user-defined template associated with it --%>

<!-- Header -->
<div>
    <cc1:ControlLayoutZone ID="_headerZone" ZoneName="Header" runat="server" DesignBlockMode="true"></cc1:ControlLayoutZone>
</div>

<cc1:ControlLayoutZone ID="_titleZone" ZoneName="Title" runat="server" DesignBlockMode="true"></cc1:ControlLayoutZone>
<cc1:ControlLayoutZone ID="_progressZone" ZoneName="Progress Bar" runat="server" DesignBlockMode="true"></cc1:ControlLayoutZone>
<cc1:ControlLayoutZone ID="_pageNumberZone" ZoneName="Page Numbers" runat="server" DesignBlockMode="true"></cc1:ControlLayoutZone>

<!-- Top -->
<cc1:ControlLayoutZone ID="_top" ZoneName="Top" runat="server" DesignBlockMode="true"></cc1:ControlLayoutZone>

<!-- Default -->
<cc1:ControlLayoutZone ID="_defaultZone" ZoneName="Default" runat="server" DesignBlockMode="true"></cc1:ControlLayoutZone>

<!-- Bottom -->
<cc1:ControlLayoutZone ID="_bottom" ZoneName="Bottom" runat="server" DesignBlockMode="true"></cc1:ControlLayoutZone>

<!-- Buttons -->
<div style="padding-top:25px;">
    <cc1:ControlLayoutZone ID="_previousZone" ZoneName="Back" runat="server" DesignBlockMode="false"></cc1:ControlLayoutZone>
    <cc1:ControlLayoutZone ID="_saveZone" ZoneName="Save and Quit" runat="server" DesignBlockMode="false"></cc1:ControlLayoutZone>
    <cc1:ControlLayoutZone ID="_nextZone" ZoneName="Next/Finish" runat="server" DesignBlockMode="false"></cc1:ControlLayoutZone>
</div>
<!-- Footer -->
<div>
    <cc1:ControlLayoutZone ID="_footerZone" ZoneName="Footer" runat="server" DesignBlockMode="true"></cc1:ControlLayoutZone>
</div>