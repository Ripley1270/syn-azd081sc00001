﻿<%@ Control Language="C#" Inherits="CheckboxWeb.ResponseView" Codebehind="ResponseView.ascx.cs" %>
<%@ Register TagPrefix="cc1" Namespace="Checkbox.Web.UI.Controls" Assembly="Checkbox.Web" %>
<%@ Register TagPrefix="cc1" Namespace="Checkbox.Web.Forms.UI.Templates" Assembly="Checkbox.Web" %>

<%-- Survey Header --%>
<asp:Panel ID="_headerPlace" runat="server" />

<%-- Error Messages  --%>
<asp:Panel ID="_errorPanel" runat="server" EnableViewState="false" Visible="false">
    <p>
        <asp:Label ID="_errorLbl" runat="server" Style="color:Red;font-weight:bold;font-size:12px;font-family:Arial;" />
    </p>
</asp:Panel>
      
<%-- Survey Title --%>
<asp:Label runat="server" ID="_surveyTitleLbl" CssClass="title" EnableViewState="false" Style="display:block;border-bottom:1px solid gray;margin-bottom:15px;" />

<%-- Language Select --%>
<asp:Panel  runat="server" ID="_languageSelectPanel">
    <br />
    <cc1:MultiLanguageLabel runat="server" ID="_selectLanguagePrompt" CssClass="Question" TextId="/pageText/takeSurvey.aspx/selectLanguage" />
    <br /><br />
    <asp:DropDownList ID="_languageSelect" runat="server" CssClass="Answer" />
    <br /><br />
    <cc1:MultiLanguageButton ID="_languageSelected" runat="server" TextId="/pageText/takeSurvey.aspx/continue" />
    <br /><br />
</asp:Panel>

<%-- Password Entry --%>
<asp:Panel runat="server" ID="_passwordPanel">
    <br />
    <cc1:MultiLanguageLabel runat="server" ID="_passwordTxt" CssClass="Answer" TextId="/pageText/survey.aspx/enterPassword" />
    <br />
    <cc1:MultiLanguageTextBox TextMode="Password" runat="server" ID="_password" CssClass="Answer" />         
    &nbsp;
    <cc1:MultiLanguageLabel runat="server" ID="_passwordError" CssClass="Error" EnableViewState="false" TextId="/pageText/survey.aspx/incorrectPassword" Visible="false" />
    <br /><br />
    <cc1:MultiLanguageButton ID="_passwordEntered" runat="server" TextId="/pageText/takeSurvey.aspx/continue" />
</asp:Panel>

<%-- Edit Existing Response --%>
<asp:Panel runat="server" ID="_responsesPanel">
    <br />
    <cc1:MultiLanguageLabel runat="server" ID="_editTxt" CssClass="Question" TextId="/pageText/survey.aspx/editText" />
    <br /><br />
    <asp:Panel ID="_newResponseButtonContainer" runat="server">
        <cc1:MultiLanguageButton runat="server" ID="_createNewBtn" CssClass="button" TextId="/pageText/survey.aspx/clickHere" />
        <cc1:MultiLanguageLabel runat="server" ID="_createNewText" CssClass="Question" TextId="/pageText/survey.aspx/createNew" />
        <br /><br />
    </asp:Panel>
    <asp:GridView ID="_responsesGrid" runat="server" CssClass="Matrix" CellPadding="4" CellSpacing="1" Width="75%" AutoGenerateColumns="false" Border="0">
        <AlternatingRowStyle CssClass="AlternatingItem" />
        <RowStyle CssClass="Item" />
        <HeaderStyle CssClass="header" />
        <Columns>
            <asp:BoundField DataField="GUID" Visible="false" />
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="18px">
                <ItemTemplate>
                    <asp:LinkButton ID="_editResponseBtn" CommandName="EditResponse" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GUID") %>' runat="server"><asp:Image runat="server" ID="_linkImg" ImageUrl="~/Images/edit16.gif" /></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Started" />
            <asp:BoundField DataField="Ended" />
        </Columns>
    </asp:GridView>          
    <br />
</asp:Panel>


<%-- Survey Content Place --%>
<div id="OuterSurveyContainer" class="OuterSurveyContainer">
    <div id="InnerSurveyContainer" class="InnerSurveyContainer">
        <asp:Panel ID="_surveyContentPlace" runat="server">
        </asp:Panel>
        <asp:Panel ID="_progressBarPlace" runat="server">
            <cc1:ProgressBar id="_progressBar" runat="server" />
        </asp:Panel>
        <asp:Panel ID="_saveProgressPlace" runat="server">
            <table cellspacing="2" cellpadding="2" class="Matrix" style="border:1px solid #FEFEFE">
                <tr>
                    <td align="center" class="header">
                        <cc1:MultiLanguageLabel runat="server" ID="_progressSavedLbl" TextId="/controlText/responseView/progressSaved" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <cc1:MultiLanguageLabel runat="server" ID="_toResumeLbl" TextId="/controlText/responseView/toResume" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <cc1:MultiLanguageLabel runat="server" ID="_resumeLinkLbl" />
                    </td>
                </tr>
                <asp:Panel ID="_emailPanel" runat="server" Visible="false">
                    <tr><td>&nbsp</td></tr>
                    <tr>
                        <td align="center">
                            <cc1:MultiLanguageLabel ID="MultiLanguageLabel1" runat="server" TextId="/controlText/responseView/toEmail" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:MultiLanguageLabel ID="_emailAddressLabel" runat="server" TextID="/controlText/responseView/emailAddress" />
                            <asp:TextBox runat="server" ID="_emailAddressField" Width="200"/>
                            <asp:RequiredFieldValidator runat="server" ID="_emailAddressValidator" ControlToValidate="_emailAddressField"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:MultiLanguageButton runat="server" ID="_sendResumeUrlButton" TextId="/controlText/responseView/sendEmail" OnClick="SendEmail_ClickEvent"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <cc1:MultiLanguageLabel runat="server" ID="_message" Visible="False"/>
                        </td>
                    </tr>
                </asp:Panel>
            </table>
        </asp:Panel>
        <asp:Panel ID="_pageNumberPlace" runat="server">
            <cc1:MultiLanguageLabel runat="server" ID="_pageLbl" CssClass="PageNumber" TextId="/pageText/survey.aspx/page" />
            <asp:Label runat="server" ID="_curPageNumberLbl" CssClass="PageNumber" />
            <cc1:MultiLanguageLabel runat="server" ID="_ofLbl" CssClass="PageNumber" TextId="/pageText/survey.aspx/of" />
            <asp:Label runat="server" ID="_totalPagesLbl" CssClass="PageNumber" />
            <br /><br />
        </asp:Panel>
                
        <asp:Panel ID="_itemsPlace" runat="server" />

        <cc1:MultiLanguageButton ID="_prevBtn" runat="server" Text="<< Back" CssClass="button" CausesValidation="false" />
        <cc1:MultiLanguageButton ID="_saveBtn" runat="server" Text="Save and Exit" CssClass="button" CausesValidation="false" />
        <cc1:MultiLanguageButton ID="_nextBtn" runat="server" Text="Next" CssClass="button" CausesValidation="true" />       
        <cc1:MultiLanguageButton ID="_finishBtn" runat="server" Text="Next" CssClass="button" CausesValidation="true" />       
    </div>
</div>

      
<%-- Footer Place --%>
<asp:Panel ID="_footerPlace" runat="server" />
      
      

