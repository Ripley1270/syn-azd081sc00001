/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.phtcorp.surveys;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Jsalesi
 */
public class Questions {

    private ArrayList m_questions;

    public Questions() {
        this(null);
    }

    public Questions(Integer numQuestions) {
       
        m_questions = numQuestions != null ?
        new ArrayList(numQuestions.intValue()) :
        new ArrayList();
    }

    public void addQuestion(Question question) {
        if(question != null) {
            m_questions.add(question);
        } else {
            new RuntimeException("Illegal Argument. Param can't be null.");
        }
    }

    public void removeQuestion(Question question) {
        if(question != null) {
            m_questions.remove(question);
        } else {
            new RuntimeException("Illegal Argument. Param can't be null.");
        }
    }

    public Iterator getQuestionsIterator() {
        Iterator iter = null;
        if(m_questions != null) {
            iter = m_questions.iterator();
        }
        return iter;
    }

    public int getNumberOfQuestions() {
        return m_questions.size();
    }
}
