/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.phtcorp.surveys;

/**
 *
 * @author Jsalesi
 */
public class Answer {
    
    private String m_itemName;

    public Answer(String itemName) {
        m_itemName = itemName;
    }

    public String getItemName() {
        return m_itemName;
    }

    public void setItemName(String itemName) {
        m_itemName = itemName;
    }
}
