-- START OF FILE
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PDE_NetPro_InsertNewWorkMedIngredients]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PDE_NetPro_InsertNewWorkMedIngredients]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

IF OBJECT_ID('tempdb..#tempVersionTable') is NOT NULL DROP TABLE #tempVersionTable

Select procVersion = '1.00' -- TODO: UPDATE VERSION
Into #tempVersionTable

GO

CREATE procedure dbo.PDE_NetPro_InsertNewWorkMedIngredients
	(@WRKCOD1C [varchar] (36),
	 @ingredNamePart1 [varchar] (36),
	 @ingredNamePart2 [varchar] (36),
	 @dose [varchar] (36),
	 @ingredNumber [varchar] (36),
	 @logTimeClient [varchar] (25),
	 @logTimeClientTZOffset [varchar] (36))
as

SET NOCOUNT ON
SET ANSI_WARNINGS OFF 

/* INSTRUCTIONS: 
* 1)	Make sure the first line of this file contain "START OF FILE".
*		Also check if the last line of this file contain "END OF FILE"
* 2)	Change procVersion ABOVE with each update
* 3)	Change history BELOW with each update
**/

/*****************************************************************************************************************
ERT Med Module PDE_NetPro_InsertNewWorkMedIngredients: Inserts a new work medication ingredients into the Study wide table

Version	    Date		Change
1.00	    12Mar2018   [schan] 
                        - Initial Release
*****************************************************************************************************************/
Declare @syncdata varchar(max)

Declare @ProcedureName varchar(256)
Select @ProcedureName = OBJECT_NAME(@@PROCID)

-- insert query
INSERT INTO DD_OtherMedWorksheetIngredRepeating (WRKCOD1C, 
								INGWRK1C, 
								DOSWRK1N,
								ORDWRK1N)
VALUES (@WRKCOD1C, 
		@ingredNamePart1 + @ingredNamePart2, 
		@dose, 
		@ingredNumber);


-- Retrieve unique key generated on insert
Declare @WorkRowID varchar(8)
Select @WorkRowID = SCOPE_IDENTITY()

-- Return RowID 
Select @syncdata = isnull(@WorkRowID, ' ') -- TODO is this needed
	   
Select @syncdata

-- Get Version
Declare @Version varchar(10)
Select @Version = ver.procVersion
From PDE_SQLVersion VER with (nolock)
Where VER.procName = @ProcedureName

-- Add sync data to log
exec [dbo].[insertStudySyncLogs] @ProcedureName, @Version, '', 'NetPro', @logTimeClient, @logTimeClientTZOffset, @syncdata


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PDE_NetPro_InsertNewWorkMedIngredients' AND TYPE = 'P')
BEGIN

    declare @name as varchar(50)
	Select @name = 'PDE_NetPro_InsertNewWorkMedIngredients'

	IF OBJECT_ID('tempdb..#tempVersionTable') is NULL
	BEGIN
		RAISERROR (N'WARNING: SQL version is missing. Please verify version is properly defined in [%s] stored procedure.', 
				11, 1, @name); 
	END
	ELSE
	BEGIN
		declare @version as numeric(18,2)	
		Select @version = procVersion from #tempVersionTable
		exec [dbo].[PDE_UpdateSQLVersion] @name, @version		
		DROP TABLE #tempVersionTable
	END 
END

GO

if NOT exists (select * from allowed_clin_procs with (nolock) where proc_name='PDE_NetPro_InsertNewWorkMedIngredients')
insert into allowed_clin_procs(proc_name) values ('PDE_NetPro_InsertNewWorkMedIngredients')

GRANT EXECUTE ON dbo.PDE_NetPro_InsertNewWorkMedIngredients TO pht_server_read_only
-- END OF FILE