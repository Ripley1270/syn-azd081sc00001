-- START OF FILE
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PDE_NetPro_GetSubjectMedList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PDE_NetPro_GetSubjectMedList]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

IF OBJECT_ID('tempdb..#tempVersionTable') is NOT NULL DROP TABLE #tempVersionTable

Select procVersion = '1.00' -- TODO: UPDATE VERSION
Into #tempVersionTable

GO

CREATE procedure dbo.PDE_NetPro_GetSubjectMedList
	(@krpt [varchar] (255) = '',
	@deviceID [varchar] (10),
	@logTimeClient [varchar] (25),
	@logTimeClientTZOffset [varchar] (36))
as

/* INSTRUCTIONS: 
* 1)	Make sure the first line of this file contain "START OF FILE".
*		Also check if the last line of this file contain "END OF FILE"
* 2)	Change procVersion ABOVE with each update
* 3)	Change history BELOW with each update
**/

SET NOCOUNT ON
SET ANSI_WARNINGS OFF 

/*****************************************************************************************************************
ERT Med Module PDE_NetPro_GetSubjectMedList
Version	    Date		Change
1.00		02FEB2018   [schan] Initial Release
*****************************************************************************************************************/
Declare @syncdata varchar(max)

Declare @ProcedureName varchar(256)
Select @ProcedureName = OBJECT_NAME(@@PROCID)

Declare @SubjectMedTable table (id int identity, MEDCOD1C varchar(10), SBJALS1C nvarchar(255))
Declare @latestSigorig varchar(25)

-- Get the SigOrig from the latest SubjectMedList or OtherMedResolution report
SELECT TOP 1 @latestSigorig = su.SIGORIG
FROM ig_SubjectMed ig with (nolock) 
	join lookup_su su with(nolock)
on ig.SIGORIG = su.sigorig	
where su.krpt = @krpt
		and su.deleted = 0
order by CAST(su.ReportStartDate_mirror as datetime) desc

-- Get Subject Meds
Insert into @SubjectMedTable(MEDCOD1C, SBJALS1C)
Select ig.MEDCOD1C, ig.SBJALS1C
FROM ig_SubjectMed ig with(nolock) 	
where ig.MEDCOD1C is not null 
	and ig.SBJALS1C is not null
	and (ig.SBJDEL1B is null or ig.SBJDEL1B = 0)
	and ig.SIGORIG = @latestSigorig
Order by ig.KRIGR 

select @syncdata = (
    Select
		(select MEDCOD1C as '@MEDCOD1C', 
				dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(SBJALS1C, CHAR(13) + CHAR(10), ''))) as '@SBJALS1C'
			From @SubjectMedTable 
			order by id asc
		for xml path('subjectmed'), type)
	for xml path('subjectmeds'))
					   

Select @syncdata

-- Get Version
Declare @Version varchar(10)
Select @Version = ver.procVersion
From PDE_SQLVersion VER with (nolock)
Where VER.procName = @ProcedureName

-- Add sync data to log
exec [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @krpt, @deviceID, @logTimeClient, @logTimeClientTZOffset, @syncdata

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PDE_NetPro_GetSubjectMedList' AND TYPE = 'P')
BEGIN

    declare @name as varchar(50)
	Select @name = 'PDE_NetPro_GetSubjectMedList'

	IF OBJECT_ID('tempdb..#tempVersionTable') is NULL
	BEGIN
		RAISERROR (N'WARNING: SQL version is missing. Please verify version is properly defined in [%s] stored procedure.', 
				11, 1, @name); 
	END
	ELSE
	BEGIN
		declare @version as numeric(18,2)	
		Select @version = procVersion from #tempVersionTable
		exec [dbo].[PDE_UpdateSQLVersion] @name, @version		
		DROP TABLE #tempVersionTable
	END 
END

GO

if NOT exists (select * from allowed_clin_procs with (nolock) where proc_name='PDE_NetPro_GetSubjectMedList')
insert into allowed_clin_procs(proc_name) values ('PDE_NetPro_GetSubjectMedList')

GRANT EXECUTE ON dbo.PDE_NetPro_GetSubjectMedList TO pht_server_read_only
-- END OF FILE