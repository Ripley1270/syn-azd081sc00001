if exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[PDE_NetPro_LaunchSurveyDataSQLV1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PDE_NetPro_LaunchSurveyDataSQLV1]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF OBJECT_ID('tempdb..#tempVersionTable') is NOT NULL DROP TABLE #tempVersionTable

Select procVersion = '1.00' -- TODO: UPDATE VERSION
Into #tempVersionTable

GO

CREATE procedure dbo.PDE_NetPro_LaunchSurveyDataSQLV1
   (@krPT varchar(255),
    @deviceID varchar(36),
    @logTimeClient varchar(64),
    @logTimeClientTZOffset varchar(36))
as

/*****************************************************************************************************************
AstraZeneca AZ_D081SC00001 PDE_NetPro_LaunchSurveyDataSQLV1
Version    Date        Change

					   
1.00       05Sep2018  [jpresto] Initial Release 

*****************************************************************************************************************/

Declare @ProcedureName varchar(256)
Select @ProcedureName = OBJECT_NAME(@@PROCID)

Declare @customSync varchar(max)
Declare @customSyncXML xml

/*******************************************************
* Insert Custom SQL Here
*******************************************************/

Declare @PRCDAT1D  varchar(36)

Select top 1 @PRCDAT1D = su.ReportStartDate 
From ig_HHTrainingModule ig with(nolock)
    join lookup_su su with(nolock)
	on ig.sigorig = su.sigorig
where su.deleted = 0
     and su.krpt = @krpt
	 --and ig.PRCDAT1D is not null
order by su.ReportStartDate_mirror desc


Declare @lastBPIReportDT varchar(36)

Select top 1 @lastBPIReportDT = Convert(VarChar(20), Cast(su.ReportStartDate AS DateTime), 113)

From lookup_su su with(nolock)
where su.krpt = @krpt
    and su.deleted = 0
	and su.krsu = 'BPID'
order by su.ReportStartDate_mirror desc	


Declare @RNDMDT1D varchar(36)

Select top 1 @RNDMDT1D = ig.RNDMDT1D
From ig_VisitConfirmation ig with(nolock)
    join lookup_su su with(nolock)
	on ig.sigorig = su.sigorig
where su.deleted = 0
     and su.krpt = @krpt
	 and ig.RNDMDT1D is not null
order by su.ReportStartDate_mirror desc


Select @customSyncXML =	(Select	
						   @PRCDAT1D as '@PRCDAT1D',
						   @lastBPIReportDT as '@lastBPIReportDT',
						   @RNDMDT1D as '@RNDMDT1D'
						For XML Path('CustomSync')
						)	


/*******************************************************
* Custom SQL - END
*******************************************************/

Select @customSyncXML
Select @customSync = cast(@customSyncXML as varchar(max))


-- Get Version
Declare @procVersion varchar(10)
Select @procVersion = ver.procVersion
From PDE_SQLVersion VER with (nolock)
Where VER.procName = @ProcedureName

---- Add sync data to log
exec [dbo].[insertStudySyncLogs] @ProcedureName, @procVersion, @krpt, @deviceID, @logTimeClient, @logTimeClientTZOffset, @customSync

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PDE_NetPro_LaunchSurveyDataSQLV1' AND TYPE = 'P')
BEGIN

    declare @name as varchar(50)
	Select @name = 'PDE_NetPro_LaunchSurveyDataSQLV1'

	IF OBJECT_ID('tempdb..#tempVersionTable') is NULL
	BEGIN
		RAISERROR (N'WARNING: SQL version is missing. Please verify version is properly defined in [%s] stored procedure.', 
				11, 1, @name); 
	END
	ELSE
	BEGIN
		declare @version as numeric(18,2)	
		Select @version = procVersion from #tempVersionTable
		exec [dbo].[PDE_UpdateSQLVersion] @name, @version		
		DROP TABLE #tempVersionTable
	END 
END

GO

---- Set Permissions
if NOT exists (Select * From allowed_clin_procs with (nolock) Where proc_name='PDE_NetPro_LaunchSurveyDataSQLV1')
insert into allowed_clin_procs(proc_name) values ('PDE_NetPro_LaunchSurveyDataSQLV1')

GRANT EXECUTE ON dbo.PDE_NetPro_LaunchSurveyDataSQLV1 TO pht_server_read_only
---- END OF FILE

