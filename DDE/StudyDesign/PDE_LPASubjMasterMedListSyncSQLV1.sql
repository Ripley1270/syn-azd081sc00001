if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PDE_LPASubjMasterMedListSyncSQLV1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PDE_LPASubjMasterMedListSyncSQLV1]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON
GO

IF OBJECT_ID('tempdb..#tempVersionTable') is NOT NULL DROP TABLE #tempVersionTable

Select procVersion = '1.01' -- TODO: UPDATE VERSION
Into #tempVersionTable

GO

CREATE procedure dbo.PDE_LPASubjMasterMedListSyncSQLV1
	(@krpt [varchar] (255) = '',
	@deviceID [varchar] (36),
	@logTimeClient [varchar] (25),
	@logTimeClientTZOffset [varchar] (36),
	@MasterMedListEffectiveDate DateTime = null,
	@lastSyncVersion varchar(10) = null,
	@SubjectMedListEffectiveDate DateTime = null)
as

SET NOCOUNT ON
SET ANSI_WARNINGS ON

/*****************************************************************************************************************
ERT Med Module PDE_LPASubjMasterMedListSyncSQLV1
Version	Date		Change
1.00	14FEB2018	[schan] Initial Release
1.01	21MAR2018	[schan] Filter master med with no MEDCOD1C
*****************************************************************************************************************/
Declare @ProcedureName varchar(256)
Select @ProcedureName = OBJECT_NAME(@@PROCID)

Declare @syncdata varchar(max)
/*******************************************************
* Insert Custom SQL Here
*******************************************************/

Declare @syncXML XML
Declare @subjectMedsXML xml
Declare @masterMedsXML xml
Declare @updateSubjectMeds varchar(1)
Declare @updateMasterMeds varchar(1)

/*****************************
* Subject Meds
*****************************/
Declare @SBJEFF1S varchar(36)
Declare @SubjectMedTable table (id int identity, MEDCOD1C varchar(10), SBJALS1C nvarchar(255))
Declare @latestSigorig varchar(25)

-- Get the SigOrig from the latest SubjectMedList report
SELECT TOP 1 @latestSigorig = su.SIGORIG,
			@SBJEFF1S = ig.SBJEFF1S
FROM lookup_su su with(nolock)
join ig_SubjectMed ig with (nolock)
	on ig.sigorig = su.sigorig
where su.krpt = @krpt
		and su.deleted = 0
		and (su.krsu = 'SubjectMed' or su.krsu = 'OtherMedResolve')
		and ig.SBJEFF1S is not null
order by CAST(su.ReportStartDate_mirror as datetime) desc

-- If last Medication Selection is newer than the subject med list on the device
IF (@SubjectMedListEffectiveDate is null OR
	@SubjectMedListEffectiveDate != @SBJEFF1S OR
	@SBJEFF1S is null) 
BEGIN
	Select @updateSubjectMeds = '1'

	-- Get Subject Meds
	Insert into @SubjectMedTable(MEDCOD1C, SBJALS1C)
	Select ig.MEDCOD1C, ig.SBJALS1C
	FROM ig_SubjectMed ig with(nolock) 	
	where ig.MEDCOD1C is not null 
		and ig.SBJALS1C is not null
		and (ig.SBJDEL1B is null or ig.SBJDEL1B = 0)
		and ig.SIGORIG = @latestSigorig
	Order by ig.KRIGR 
END
ELSE
BEGIN
	Select @updateSubjectMeds = '0'
END

Select @subjectMedsXML = (Select @updateSubjectMeds as '@updateNeeded', 
								@SBJEFF1S as '@effectiveDate',								
								(select MEDCOD1C as '@MEDCOD1C', 
										dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(SBJALS1C, CHAR(13) + CHAR(10), ''))) as '@SBJALS1C'
									From @SubjectMedTable 
									order by id asc
								for xml path('subjectmed'), type)
							for xml path('subjectmeds'), type)

/*****************************
* Master Meds
*****************************/
Declare @numNewMeds int
Declare @EffectiveDate varchar(255)
-- Flag to indicate if the device master med list needs to do a clean update by deleting existing meds if 
-- any and get the full list
Declare @cleanUpdate varchar(1) 

-- Get proc version
Declare @Version varchar(10)
Select @Version = ver.procVersion
From PDE_SQLVersion VER with (nolock)
Where VER.procName = @ProcedureName

-- If this is the 1st sync or the version have change since the last sync
IF (@lastSyncVersion is null OR @Version != @lastSyncVersion)
BEGIN
	Select @cleanUpdate = '1'
END

-- Get the number of new meds since the last update
Select @numNewMeds = COUNT(*),		
		@EffectiveDate = MAX(convert(varchar, m.MEDEFF1S, 126))
From DD_MasterMed m with(nolock)
Where m.MEDSTS1L = 1 and
	(@MasterMedListEffectiveDate is null or
	m.MEDEFF1S > @MasterMedListEffectiveDate or
	@cleanUpdate = '1')
	
IF (@numNewMeds > 0 or @cleanUpdate = '1')
BEGIN
	Declare @MasterMedIngred table (MedRowID int, MEDCOD1C varchar(10), INGORD1N int, INGNAM1C varchar(244), INGDOS1N varchar(20), INGUNT1L int)	
	INSERT INTO @MasterMedIngred(MedRowID, MEDCOD1C, INGORD1N, INGNAM1C, INGDOS1N, INGUNT1L)
	Select ing.MedRowID, 
			ing.MEDCOD1C, 
			ing.INGORD1N,
			ing.INGNAM1C,
			ing.INGDOS1N,
			ing.INGUNT1L
	From DD_MasterMedIngredRepeating ing with(nolock)

	Select @updateMasterMeds = '1'

	Select @masterMedsXML =  (
			Select @updateMasterMeds as '@updateNeeded',
					@EffectiveDate as '@effectiveDate',
					@cleanUpdate as '@cleanUpdate',
					@version as '@syncVersion',			
					(Select m.MEDCOD1C as '@MEDCOD1C',
							m.MEDGRP1L as '@MEDGRP1L',
							dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDNAM1C, CHAR(13) + CHAR(10), ''))) as '@MEDNAM1C',
							dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDDOS1C, CHAR(13) + CHAR(10), ''))) as '@MEDDOS1C',
							m.MEDUNT1L as '@MEDUNT1L',
							m.MEDRTE1L as '@MEDRTE1L',
							m.MEDFRM1L as '@MEDFRM1L',							
							m.MEDFRM2L as '@MEDFRM2L',
							m.OMEFLG1L as '@OMEFLG1L',
							m.OMEDOS1N as '@OMEDOS1N',	
							Case when m.OMEREF1C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.OMEREF1C, CHAR(13) + CHAR(10), ''))) end as '@OMEREF1C',
							Case when m.OMEREF2C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.OMEREF2C, CHAR(13) + CHAR(10), ''))) end as '@OMEREF2C',
							m.OMETTL1N as '@OMETTL1N',
							m.MEDFLG1B as '@MEDFLG1B',							
							m.MEDFLG2B as '@MEDFLG2B',							
							m.MEDFLG3B as '@MEDFLG3B',							
							m.MEDFLG4B as '@MEDFLG4B',							
							m.MEDFLG5B as '@MEDFLG5B',							
							m.MEDFLG6B as '@MEDFLG6B',							
							m.MEDFLG7B as '@MEDFLG7B',							
							m.MEDFLG8B as '@MEDFLG8B',							
							m.MEDFLG9B as '@MEDFLG9B',
							m.MEDVAL1N as '@MEDVAL1N',							
							m.MEDVAL2N as '@MEDVAL2N',							
							m.MEDVAL3N as '@MEDVAL3N',							
							m.MEDVAL4N as '@MEDVAL4N',							
							m.MEDVAL5N as '@MEDVAL5N',							
							m.MEDVAL6N as '@MEDVAL6N',							
							m.MEDVAL7N as '@MEDVAL7N',							
							m.MEDVAL8N as '@MEDVAL8N',							
							m.MEDVAL9N as '@MEDVAL9N',
							Case when m.MEDTXT1C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT1C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT1C',
							Case when m.MEDTXT2C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT2C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT2C',
							Case when m.MEDTXT3C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT3C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT3C',
							Case when m.MEDTXT4C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT4C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT4C',
							Case when m.MEDTXT5C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT5C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT5C',
							Case when m.MEDTXT6C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT6C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT6C',
							Case when m.MEDTXT7C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT7C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT7C',
							Case when m.MEDTXT8C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT8C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT8C',
							Case when m.MEDTXT9C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT9C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT9C'							
					 From DD_MasterMed m with(nolock)
					 Where m.MEDSTS1L = 1 and
							m.MEDCOD1C is not null and
							(@MasterMedListEffectiveDate is null or
							m.MEDEFF1S > @MasterMedListEffectiveDate)
					 order by TRY_CAST(m.MEDCOD1C as numeric) asc
					 For XML Path ('med'), type)
				For XML Path ('mastermeds')
			)
END
ELSE
BEGIN
	Select @updateMasterMeds = '0'
	
	Select @masterMedsXML =  (
				Select	@updateMasterMeds as '@updateNeeded'			
				For XML Path ('mastermeds')
			)
END						
							
-- Results
Select @syncXML = (Select null	
					For XML Path('sync'))

	/*(Select @cleanUpdate as '@cleanUpdate',
			@updateSubjectMeds as '@updateSubjectMeds',
			@updateMasterMeds as '@updateMasterMeds',
			@version as '@syncVersion'
	For XML Path('sync'))*/

IF(@updateSubjectMeds = '1')
BEGIN	
	-- Append subject meds
	Set @syncXML.modify('insert sql:variable("@subjectMedsXML")
					into (/sync)[1]')
END

IF(@updateMasterMeds = '1')
BEGIN
	-- Append master meds	
	Set @syncXML.modify('insert sql:variable("@masterMedsXML")
					into (/sync)[1]')
END
					
-- Set syncData
Select @syncData = cast(@syncXML as varchar(max))

-- Return syncData
Select @syncData

-- Add sync data to log
exec [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @krpt, @deviceID, @logTimeClient, @logTimeClientTZOffset, @syncdata

GO
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PDE_LPASubjMasterMedListSyncSQLV1' AND TYPE = 'P')
BEGIN

    declare @name as varchar(50)
	Select @name = 'PDE_LPASubjMasterMedListSyncSQLV1'

	IF OBJECT_ID('tempdb..#tempVersionTable') is NULL
	BEGIN
		RAISERROR (N'WARNING: SQL version is missing. Please verify version is properly defined in [%s] stored procedure.', 
				11, 1, @name); 
	END
	ELSE
	BEGIN
		declare @version as numeric(18,2)	
		Select @version = procVersion from #tempVersionTable
		exec [dbo].[PDE_UpdateSQLVersion] @name, @version		
		DROP TABLE #tempVersionTable
	END 
END

GO

if NOT exists (select * from allowed_clin_procs with (nolock) where proc_name='PDE_LPASubjMasterMedListSyncSQLV1')
insert into allowed_clin_procs(proc_name) values ('PDE_LPASubjMasterMedListSyncSQLV1')

GRANT EXECUTE ON dbo.PDE_LPASubjMasterMedListSyncSQLV1 TO pht_server_read_only
