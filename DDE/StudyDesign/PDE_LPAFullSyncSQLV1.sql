-- START OF FILE
if exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[PDE_LPAFullSyncSQLV1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PDE_LPAFullSyncSQLV1]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

IF OBJECT_ID('tempdb..#tempVersionTable') is NOT NULL DROP TABLE #tempVersionTable

Select procVersion = '1.05'
Into #tempVersionTable

GO

CREATE procedure dbo.PDE_LPAFullSyncSQLV1
   (@krPT varchar(255),
    @deviceID varchar(36),
    @logTimeClient varchar(64),
    @logTimeClientTZOffset varchar(36))
as

/* INSTRUCTIONS: 
* 1)	Make sure the first line of this file contain "START OF FILE".
*		Also check if the last line of this file contain "END OF FILE"
* 2)	Change procVersion ABOVE with each update
* 3)	Change history BELOW with each update
**/

/*****************************************************************************************************************
ERT Med Module PDE_LPAFullSyncSQLV1 - Full custom sync SQL
Version		Date        Change
1.05        27SEP2018   [cjames] Corrected krsu for Subject Training Module to STM (ONEECOA-110182)
1.04		26SEP2018	[schan] Removed diary window; Added Visit Confirmation
1.03		20SEP2018	[cjames] Adds SubjectTrainingModule to DiaryCompletionTempTable for fixes under ONEECOA-109330
1.02		17SEP2018   [tegan] Diary completions to use 10000 days from study day. Also adding HH Training module
1.01		31AUG2018	[tegan] Add EQ5D5L into script
1.00		24AUG2018   [tegan] Initial Release								
*****************************************************************************************************************/

Declare @ProcedureName varchar(256)
Select @ProcedureName = OBJECT_NAME(@@PROCID)

Declare @customSync varchar(max)
Declare @customSyncXML xml

/*******************************************************
* Insert Custom SQL Here - START
*******************************************************/

--Get Med Taken
Declare @MedTakenTempTable table (MedID varchar(30),
									MedCode varchar(10),
									MedTime varchar(36),									
									SubjectAlias nvarchar(40),
									MedAmount varchar(36))
													
INSERT INTO @MedTakenTempTable (MedID, MedCode, MedTime, SubjectAlias, MedAmount)
Select ig.MED_ID1N as MedID,	
		ig.MEDCOD1C as MedCode,
		ig.MEDTKN1S as MedTime,	
		ig.SBJALS1C as SubjectAlias,
		ig.MEDAMT1N	as MedAmount
FROM ig_DiaryMedRepeating ig with (nolock)
join lookup_su su with (nolock)
	on ig.SIGORIG = su.sigorig
where ig.KRPT = @krPT and 
	su.deleted = 0 
Order by su.reportStartdate_mirror desc, ig.krigr

-- Get Other Resolve Meds
Declare @OtherMedResolveTable table (MedID varchar(30), 
									ResolutionSource varchar(2),
									OTHCOD1C varchar(10),
									MedAmount varchar(2))
										
INSERT INTO @OtherMedResolveTable (MedID, ResolutionSource, OTHCOD1C, MedAmount)
Select ig.MED_ID1N as MedID,
		ig.OTHRES1L as ResolutionSource,
		ig.OTHCOD1C as OTHCOD1C,
		ig.MEDAMT1N as MedAmout
FROM ig_OtherMedResolve ig with (nolock)
join lookup_su su with (nolock)
	on ig.SIGORIG = su.sigorig
where ig.KRPT = @krPT and 
		su.deleted = 0 		

Declare @MedTakenFinalTable table (MedID varchar(30),
									MedCode varchar(10),
									MedTime varchar(36),									
									SubjectAlias nvarchar(40),
									MedAmount varchar(36))

-- Add Meds that is NOT other(99999999)								
INSERT INTO	@MedTakenFinalTable
Select *						
From @MedTakenTempTable
Where MedCode != '99999999'							

-- Add Resolved Meds from Subject List or Master Med List
INSERT INTO	@MedTakenFinalTable	(MedID, MedCode, MedTime, MedAmount)						
Select mt.MedID,		
		om.OTHCOD1C,
		mt.MedTime,
		om.MedAmount
From @MedTakenTempTable mt
join @OtherMedResolveTable om
on mt.MedID = om.MedID
where om.ResolutionSource = 'SC' or 
		om.ResolutionSource = 'MC'

-- Add Resolved Meds from Existing work med or new work med with a med code
INSERT INTO	@MedTakenFinalTable	(MedID, MedCode, MedTime, MedAmount)						
Select mt.MedID,
		wm.MEDCOD1C,
		mt.MedTime,
		om.MedAmount
From @MedTakenTempTable mt
join @OtherMedResolveTable om
on mt.MedID = om.MedID
join DD_OtherMedWorksheet wm 
on om.OTHCOD1C = wm.WRKCOD1C
where (om.ResolutionSource = 'WC' or 
		om.ResolutionSource = 'WE')
		and wm.MEDCOD1C is not null
	
-- Insert any unresolved other meds		
INSERT INTO	@MedTakenFinalTable
Select *						
From @MedTakenTempTable tt
Where NOT EXISTS (Select * 
				FROM @MedTakenFinalTable ft
				Where ft.MedID = tt.MedID)

/************************************
* Diary Completion Windows
************************************/

declare @DiaryCompletionTable table (dateTime varchar(36), diary varchar(20))

Insert INTO @DiaryCompletionTable
Select
	x.dateTime,
	x.diary
From
	(Select 
		su.ReportStartDate AS dateTime,		
		su.krsu AS diary,
		DENSE_RANK() Over(Partition By su.KRSU Order by su.ReportStartDate_mirror Desc, su.krsur Desc) as Rnk
From lookup_su su with (nolock)
Where su.deleted = 0
	and su.krpt = @krpt
	AND su.krsu IN ('BPID', 'FACTP', 'EQ5D5L', 'HHTrainingModule', 'STM')
	and su.ReportStartDate is not null) X
Where x.Rnk <= 8

-- Get Visit Confirmations

declare @VisitConfirmations table (
  VST1L int,
  RNDMDT1D varchar(12),
  source varchar(20),
  dateTime varchar(36)
)
          									
insert into @VisitConfirmations
select
  ig.VST1L,
  ig.RNDMDT1D,
  su.source,
  su.ReportStartDate as dateTime
from ig_VisitConfirmation ig
JOIN lookup_su su with (nolock) on su.sigorig = ig.sigorig
WHERE su.krpt = @krPT and
		su.deleted = 0

Select @customSyncXML = (
   Select		
        (Select
           (Select MedID as '@MED_ID1N',
                   MedCode as '@MEDCOD1C',
				   MedTime as '@MEDTKN1S',
				   SubjectAlias as '@SBJALS1C',
				   MedAmount as '@MEDAMT1N'
            From @MedTakenFinalTable
            For xml path('MedTakenRecord'), type)
        For xml path('MedTaken'), type),
    	(select
      		(select dateTime as '@dateTime',
          			diary as '@diary'
      		from @DiaryCompletionTable
			order by diary, cast(dateTime as datetime)
      		for xml PATH('DiaryCompletionModel'), type)
    	for xml PATH('DiaryCompletionCollection'), type),
		(select
      		(select VST1L as '@VST1L',
					RNDMDT1D as '@RNDMDT1D',
					source as '@source',
					dateTime as '@dateTime'
      		from @VisitConfirmations
      		for xml PATH('VisitConfirmationModel'), type)
    	for xml PATH('VisitConfirmationCollection'), type)      
    For XML path('CustomSync')
)

/*******************************************************
* Custom SQL - END
*******************************************************/
	
Select @customSyncXML
Select @customSync = cast(@customSyncXML as varchar(max))

-- Get Version
Declare @procVersion varchar(10)
Select @procVersion = ver.procVersion
From PDE_SQLVersion VER with (nolock)
Where VER.procName = @ProcedureName

-- Add sync data to log
exec [dbo].[insertStudySyncLogs] @ProcedureName, @procVersion, @krpt, @deviceID, @logTimeClient, @logTimeClientTZOffset, @customSync

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PDE_LPAFullSyncSQLV1' AND TYPE = 'P')
BEGIN

    declare @name as varchar(50)
	Select @name = 'PDE_LPAFullSyncSQLV1'

	IF OBJECT_ID('tempdb..#tempVersionTable') is NULL
	BEGIN
		RAISERROR (N'WARNING: SQL version is missing. Please verify version is properly defined in [%s] stored procedure.', 
				11, 1, @name); 
	END
	ELSE
	BEGIN
		declare @version as numeric(18,2)	
		Select @version = procVersion from #tempVersionTable
		exec [dbo].[PDE_UpdateSQLVersion] @name, @version		
		DROP TABLE #tempVersionTable
	END 
END

GO

-- Set Permissions
if NOT exists (Select * From allowed_clin_procs with (nolock) Where proc_name='PDE_LPAFullSyncSQLV1')
insert into allowed_clin_procs(proc_name) values ('PDE_LPAFullSyncSQLV1')

GRANT EXECUTE ON dbo.PDE_LPAFullSyncSQLV1 TO pht_server_read_only
-- END OF FILE
