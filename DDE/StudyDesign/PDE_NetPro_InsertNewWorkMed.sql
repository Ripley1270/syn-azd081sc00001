-- START OF FILE
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PDE_NetPro_InsertNewWorkMed]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PDE_NetPro_InsertNewWorkMed]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

IF OBJECT_ID('tempdb..#tempVersionTable') is NOT NULL DROP TABLE #tempVersionTable

Select procVersion = '1.01' -- TODO: UPDATE VERSION
Into #tempVersionTable

GO

CREATE procedure dbo.PDE_NetPro_InsertNewWorkMed
	(
	-- WRKNAM2C
	@WRKNAM2CPart1 varchar(36),
	@WRKNAM2CPart2 varchar(36),
	--The datetime that the site user added the record to the Other Medication Worksheet form section
	@WRKENT1S varchar(36),
	-- Dose strength unit of the active ingredients 
	@MEDUNT1L int,
	-- Administration route of the medication 
	@MEDRTE1L int,
	-- Dose administration form 
	@MEDFRM1L int,
	-- Dosage units
	@MEDFRM2L int,
	@siteUser varchar (36),
	@logTimeClient varchar(36),
	@logTimeClientTZOffset varchar(36))
as

SET NOCOUNT ON
SET ANSI_WARNINGS OFF 

/* INSTRUCTIONS: 
* 1)	Make sure the first line of this file contain "START OF FILE".
*		Also check if the last line of this file contain "END OF FILE"
* 2)	Change procVersion ABOVE with each update
* 3)	Change history BELOW with each update
**/

/*****************************************************************************************************************
ERT Med Module PDE_NetPro_InsertNewWorkMed: Inserts a new work medication into the Study wide table

Version	    Date		Change
1.00	    12NOV2017   [schan] - Initial Release
1.01		03OCT2018	[schan] - Added Site User (WRKSIT1C)
*****************************************************************************************************************/
Declare @syncdata varchar(max)

Declare @ProcedureName varchar(256)
Select @ProcedureName = OBJECT_NAME(@@PROCID)

-- insert query
INSERT INTO DD_OtherMedWorksheet (WRKNAM2C,
								MEDUNT1L, 
								MEDRTE1L, 
								MEDFRM1L,
								MEDFRM2L, 
								WRKENT1S,
								WRKSTS1L,
								WRKSIT1C)
VALUES (@WRKNAM2CPart1 + @WRKNAM2CPart2,
		@MEDUNT1L, 
		@MEDRTE1L, 
		@MEDFRM1L, 
		@MEDFRM2L, 
		@WRKENT1S,
		'3',
		@siteUser); -- 3 = Pending

-- Retrieve unique key generated on insert
Declare @WorkRowID varchar(6)
Select @WorkRowID = SCOPE_IDENTITY()

--Prepend 'WRK' and left pad with zeros
Declare @WRKCOD1C varchar(10)
Set @WRKCOD1C = @WorkRowID
Select @WRKCOD1C = 'WRK' + REPLACE(STR(@WRKCOD1C, 5), SPACE(1), '0')

-- Insert Temp WRK Code into the table
Update DD_OtherMedWorksheet
Set WRKCOD1C = @WRKCOD1C
Where WorkRowID = @WorkRowID	
-- Return WorkID 
Select @syncdata = (
    select @WRKCOD1C as '@WRKCOD1C',
			@WorkRowID as '@WorkRowID'
	for xml path('sync'))

Select @syncdata

-- Get Version
Declare @Version varchar(10)
Select @Version = ver.procVersion
From PDE_SQLVersion VER with (nolock)
Where VER.procName = @ProcedureName

-- Add sync data to log
exec [dbo].[insertStudySyncLogs] @ProcedureName, @Version, '', 'NetPro', @logTimeClient, @logTimeClientTZOffset, @syncdata


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PDE_NetPro_InsertNewWorkMed' AND TYPE = 'P')
BEGIN

    declare @name as varchar(50)
	Select @name = 'PDE_NetPro_InsertNewWorkMed'

	IF OBJECT_ID('tempdb..#tempVersionTable') is NULL
	BEGIN
		RAISERROR (N'WARNING: SQL version is missing. Please verify version is properly defined in [%s] stored procedure.', 
				11, 1, @name); 
	END
	ELSE
	BEGIN
		declare @version as numeric(18,2)	
		Select @version = procVersion from #tempVersionTable
		exec [dbo].[PDE_UpdateSQLVersion] @name, @version		
		DROP TABLE #tempVersionTable
	END 
END

GO

-- Set Permissions
if NOT exists (select * from allowed_clin_procs with (nolock) where proc_name='PDE_NetPro_InsertNewWorkMed')
insert into allowed_clin_procs(proc_name) values ('PDE_NetPro_InsertNewWorkMed')

GRANT EXECUTE ON dbo.PDE_NetPro_InsertNewWorkMed TO pht_server_read_only
-- END OF FILE