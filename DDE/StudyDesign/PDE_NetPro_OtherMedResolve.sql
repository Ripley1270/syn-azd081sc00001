-- START OF FILE
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PDE_NetPro_OtherMedResolve]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PDE_NetPro_OtherMedResolve]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

IF OBJECT_ID('tempdb..#tempVersionTable') is NOT NULL DROP TABLE #tempVersionTable

Select procVersion = '1.00' -- TODO: UPDATE VERSION
Into #tempVersionTable

GO

CREATE procedure dbo.PDE_NetPro_OtherMedResolve
	(@krpt varchar (255),
	@deviceID [varchar] (10),
	@logTimeClient [varchar] (25),
	@logTimeClientTZOffset [varchar] (36),
	@MED_ID1N varchar(30))
as

SET NOCOUNT ON
SET ANSI_WARNINGS OFF 

/* INSTRUCTIONS: 
* 1)	Make sure the first line of this file contain "START OF FILE".
*		Also check if the last line of this file contain "END OF FILE"
* 2)	Change procVersion ABOVE with each update
* 3)	Change history BELOW with each update
**/

/*****************************************************************************************************************
ERT Med Module PDE_NetPro_OtherMedResolve
Version	    Date		Change
1.00		17APR2017	[schan] Initial Release
*****************************************************************************************************************/
Declare @syncdata varchar(max)

Declare @ProcedureName varchar(256)
Select @ProcedureName = OBJECT_NAME(@@PROCID)

Declare @MEDTKN1S varchar(36)

Select top 1 @MEDTKN1S = ig.MEDTKN1S
From ig_DiaryMedRepeating ig with (nolock)
    join lookup_su su with (nolock)
	on su.sigorig = ig.sigorig
Where @krpt = su.krpt
    and su.deleted = 0
	and ig.MED_ID1N = @MED_ID1N

select @syncdata = (
	select @MEDTKN1S as '@MEDTKN1S'
	for xml path('sync'))					   

Select @syncdata

-- Get Version
Declare @Version varchar(10)
Select @Version = ver.procVersion
From PDE_SQLVersion VER with (nolock)
Where VER.procName = @ProcedureName

-- Add sync data to log
exec [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @krpt, @deviceID, @logTimeClient, @logTimeClientTZOffset, @syncdata

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PDE_NetPro_OtherMedResolve' AND TYPE = 'P')
BEGIN

    declare @name as varchar(50)
	Select @name = 'PDE_NetPro_OtherMedResolve'

	IF OBJECT_ID('tempdb..#tempVersionTable') is NULL
	BEGIN
		RAISERROR (N'WARNING: SQL version is missing. Please verify version is properly defined in [%s] stored procedure.', 
				11, 1, @name); 
	END
	ELSE
	BEGIN
		declare @version as numeric(18,2)	
		Select @version = procVersion from #tempVersionTable
		exec [dbo].[PDE_UpdateSQLVersion] @name, @version		
		DROP TABLE #tempVersionTable
	END 
END

GO

if NOT exists (select * from allowed_clin_procs with (nolock) where proc_name='PDE_NetPro_OtherMedResolve')
insert into allowed_clin_procs(proc_name) values ('PDE_NetPro_OtherMedResolve')

GRANT EXECUTE ON dbo.PDE_NetPro_OtherMedResolve TO pht_server_read_only
-- END OF FILE