-- START OF FILE
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PDE_NetPro_GetWorkList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PDE_NetPro_GetWorkList]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

IF OBJECT_ID('tempdb..#tempVersionTable') is NOT NULL DROP TABLE #tempVersionTable

Select procVersion = '1.00' -- TODO: UPDATE VERSION
Into #tempVersionTable

GO

CREATE procedure dbo.PDE_NetPro_GetWorkList
	(@modifyDate DateTime)
as

SET NOCOUNT ON
SET ANSI_WARNINGS OFF 

/* INSTRUCTIONS: 
* 1)	Make sure the first line of this file contain "START OF FILE".
*		Also check if the last line of this file contain "END OF FILE"
* 2)	Change procVersion ABOVE with each update
* 3)	Change history BELOW with each update
**/

/*****************************************************************************************************************
ERT Med Module PDE_NetPro_GetWorkList
Version	    Date		Change
1.00		13NOV2017    [schan] Initial Release   
*****************************************************************************************************************/
Declare @syncdata varchar(max)

Declare @ProcedureName varchar(256)
Select @ProcedureName = OBJECT_NAME(@@PROCID)

-- Determine the most recent entry date in the DD_WorkMedication Table
Declare @workModDT DateTime

Select top 1 @workModDT = wm.WRKENT1S
from DD_OtherMedWorksheet wm with (nolock)
order by cast(wm.WRKENT1S as datetime) desc

If (@workModDT >= @modifyDate)
	BEGIN
		Declare @WorkMedIngred table (WRKCOD1C varchar(8), INGWRK1C varchar(40), DOSWRK1N varchar(15), ORDWRK1N int)
	
		INSERT INTO @WorkMedIngred (WRKCOD1C, INGWRK1C, DOSWRK1N, ORDWRK1N)
		Select ing.WRKCOD1C,
				ing.INGWRK1C,
				ing.DOSWRK1N,
				ing.ORDWRK1N
		From DD_OtherMedWorksheetIngredRepeating ing with (nolock)	
	
		Select @syncdata = (
				Select '1' as '@updateList',
						@workModDT as '@workModDT',
						(Select wm.WRKCOD1C as '@WRKCOD1C',
								--dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(wm.WRKNAM1C, CHAR(13) + CHAR(10), ''))) as '@WRKNAM1C',
								dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(wm.WRKNAM2C, CHAR(13) + CHAR(10), ''))) as '@WRKNAM2C',
								--dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(wm.WRKDOS1C, CHAR(13) + CHAR(10), ''))) as '@WRKDOS1C',
								wm.MEDUNT1L as '@MEDUNT1L',
								wm.MEDRTE1L as '@MEDRTE1L',
								wm.MEDFRM1L as '@MEDFRM1L',
								wm.MEDFRM2L as '@MEDFRM2L',
								(Select dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(ing.INGWRK1C, CHAR(13) + CHAR(10), ''))) as '@INGWRK1C',
										ing.DOSWRK1N as '@DOSWRK1N'
								From @WorkMedIngred ing
								where wm.WRKCOD1C = ing.WRKCOD1C
								order by ing.ORDWRK1N
								For XML Path ('ing'), type)
						From DD_OtherMedWorksheet wm with (nolock)
						order by wm.WorkRowID asc
						For XML Path ('med'), type)
				For XML Path ('workmeds'))
	END
Else
	Begin
	Select @syncdata = (
		Select '0' as '@updateList'
		For XML Path ('workmeds'))
	End
	
Select @syncdata as '@syncdata'

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PDE_NetPro_GetWorkList' AND TYPE = 'P')
BEGIN

    declare @name as varchar(50)
	Select @name = 'PDE_NetPro_GetWorkList'

	IF OBJECT_ID('tempdb..#tempVersionTable') is NULL
	BEGIN
		RAISERROR (N'WARNING: SQL version is missing. Please verify version is properly defined in [%s] stored procedure.', 
				11, 1, @name); 
	END
	ELSE
	BEGIN
		declare @version as numeric(18,2)	
		Select @version = procVersion from #tempVersionTable
		exec [dbo].[PDE_UpdateSQLVersion] @name, @version		
		DROP TABLE #tempVersionTable
	END 
END

GO

if NOT exists (select * from allowed_clin_procs with (nolock) where proc_name='PDE_NetPro_GetWorkList')
insert into allowed_clin_procs(proc_name) values ('PDE_NetPro_GetWorkList')

GRANT EXECUTE ON dbo.PDE_NetPro_GetWorkList TO pht_server_read_only
-- END OF FILE