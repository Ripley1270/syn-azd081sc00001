-- START OF FILE
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PDE_NetPro_GetMasterMedList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PDE_NetPro_GetMasterMedList]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

IF OBJECT_ID('tempdb..#tempVersionTable') is NOT NULL DROP TABLE #tempVersionTable

Select procVersion = '1.01' -- TODO: UPDATE VERSION
Into #tempVersionTable

GO

CREATE procedure dbo.PDE_NetPro_GetMasterMedList
	(@krpt [varchar] (255) = '',
	@deviceID [varchar] (10),
	@logTimeClient [varchar] (25),
	@logTimeClientTZOffset [varchar] (36),
	@modifyDate DateTime,
	@lastSyncVersion varchar(10) = null)
as

SET NOCOUNT ON
SET ANSI_WARNINGS OFF 

/* INSTRUCTIONS: 
* 1)	Make sure the first line of this file contain "START OF FILE".
*		Also check if the last line of this file contain "END OF FILE"
* 2)	Change procVersion ABOVE with each update
* 3)	Change history BELOW with each update
**/

/*****************************************************************************************************************
ERT Med Module PDE_NetPro_GetMasterMedList
Version	    Date		Change
1.00        13NOV2017   [schan] Initial Release
1.01        05JUN2018   [schan] Added logging support
*****************************************************************************************************************/
Declare @syncdata varchar(max)

Declare @ProcedureName varchar(256)
Select @ProcedureName = OBJECT_NAME(@@PROCID)

Declare @Version varchar(10)
Select @Version = ver.procVersion
From PDE_SQLVersion VER with (nolock)
Where VER.procName = @ProcedureName

-- Determine the most recent modification date in MasterMed Table
Declare @masterModDate DateTime

Select top 1 @masterModDate = m.MEDEFF1S
from DD_MasterMed m with (nolock)
order by m.MEDEFF1S desc

/***************************************************************
 *   If the master medication table or the region table have been 
 *   modified since NetPro last generated master javascript file, 
 *   bring master list to regenerate file
 ***************************************************************/
If (@masterModDate >= @modifyDate OR @lastSyncVersion is null OR @lastSyncVersion != @Version)
    Begin
		Declare @MasterMedIngred table (MedRowID int, MEDCOD1C varchar(10), INGORD1N int, INGNAM1C varchar(244), INGDOS1N varchar(20), INGUNT1L int)
		
		INSERT INTO @MasterMedIngred(MedRowID, MEDCOD1C, INGORD1N, INGNAM1C, INGDOS1N, INGUNT1L)
		Select ing.MedRowID, 
				ing.MEDCOD1C, 
				ing.INGORD1N,
				ing.INGNAM1C,
				ing.INGDOS1N,
				ing.INGUNT1L
		From DD_MasterMedIngredRepeating ing with(nolock)    
    				
		Select @syncdata = (
			Select '1' as '@updateList',
					@Version as '@syncVersion',
					@masterModDate as '@EFF1S',
					@modifyDate as '@lastModifyDate',
					(Select m.MEDCOD1C as '@MEDCOD1C',
							m.MEDGRP1L as '@MEDGRP1L',
							m.MEDRTE1L as '@MEDRTE1L',
							m.MEDFRM1L as '@MEDFRM1L',							
							m.MEDFRM2L as '@MEDFRM2L',
							m.MEDSTS1L as '@MEDSTS1L',
							m.MEDNOT1B as '@MEDNOT1B',
							m.MEDEFF1S as '@MEDEFF1S',
							m.OMEFLG1L as '@OMEFLG1L',
							m.OMEDOS1N as '@OMEDOS1N',	
							Case when m.OMEREF1C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.OMEREF1C, CHAR(13) + CHAR(10), ''))) end as '@OMEREF1C',
							Case when m.OMEREF2C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.OMEREF2C, CHAR(13) + CHAR(10), ''))) end as '@OMEREF2C',
							m.OMETTL1N as '@OMETTL1N',
							m.MEDFLG1B as '@MEDFLG1B',							
							m.MEDFLG2B as '@MEDFLG2B',							
							m.MEDFLG3B as '@MEDFLG3B',							
							m.MEDFLG4B as '@MEDFLG4B',							
							m.MEDFLG5B as '@MEDFLG5B',							
							m.MEDFLG6B as '@MEDFLG6B',							
							m.MEDFLG7B as '@MEDFLG7B',							
							m.MEDFLG8B as '@MEDFLG8B',							
							m.MEDFLG9B as '@MEDFLG9B',
							m.MEDVAL1N as '@MEDVAL1N',							
							m.MEDVAL2N as '@MEDVAL2N',							
							m.MEDVAL3N as '@MEDVAL3N',							
							m.MEDVAL4N as '@MEDVAL4N',							
							m.MEDVAL5N as '@MEDVAL5N',							
							m.MEDVAL6N as '@MEDVAL6N',							
							m.MEDVAL7N as '@MEDVAL7N',							
							m.MEDVAL8N as '@MEDVAL8N',							
							m.MEDVAL9N as '@MEDVAL9N',
							Case when m.MEDTXT1C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT1C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT1C',
							Case when m.MEDTXT2C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT2C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT2C',
							Case when m.MEDTXT3C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT3C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT3C',
							Case when m.MEDTXT4C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT4C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT4C',
							Case when m.MEDTXT5C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT5C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT5C',
							Case when m.MEDTXT6C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT6C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT6C',
							Case when m.MEDTXT7C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT7C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT7C',
							Case when m.MEDTXT8C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT8C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT8C',
							Case when m.MEDTXT9C IS null then null else dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(m.MEDTXT9C, CHAR(13) + CHAR(10), ''))) end as '@MEDTXT9C',
							(Select dbo.PDE_fn_NVarCharToHTMLEncode(RTRIM(Replace(ing.INGNAM1C, CHAR(13) + CHAR(10), ''))) as '@INGNAM1C',
									ing.INGDOS1N as '@INGDOS1N',
									ing.INGUNT1L as '@INGUNT1L'
							From @MasterMedIngred ing
							where m.MedRowID = ing.MedRowID
							order by ing.INGORD1N
							For XML Path ('ing'), type)
					 From DD_MasterMed m with(nolock)
					 Where MEDCOD1C is not null
					 order by TRY_CAST(m.MEDCOD1C as numeric) asc
					 For XML Path ('med'), type)
				For XML Path ('mastermeds')
			)
	End
Else
	Begin
	Select @syncdata = (
		Select '0' as '@updateList',
				@modifyDate as '@lastModifyDate'
		For XML Path ('mastermeds')
		)
	End
	
Select @syncdata as '@syncdata'

-- Add sync data to log
exec [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @krpt, @deviceID, @logTimeClient, @logTimeClientTZOffset, @syncdata


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PDE_NetPro_GetMasterMedList' AND TYPE = 'P')
BEGIN

    declare @name as varchar(50)
	Select @name = 'PDE_NetPro_GetMasterMedList'

	IF OBJECT_ID('tempdb..#tempVersionTable') is NULL
	BEGIN
		RAISERROR (N'WARNING: SQL version is missing. Please verify version is properly defined in [%s] stored procedure.', 
				11, 1, @name); 
	END
	ELSE
	BEGIN
		declare @version as numeric(18,2)	
		Select @version = procVersion from #tempVersionTable
		exec [dbo].[PDE_UpdateSQLVersion] @name, @version		
		DROP TABLE #tempVersionTable
	END 
END

GO

-- Set Permissions
if NOT exists (select * from allowed_clin_procs with (nolock) where proc_name='PDE_NetPro_GetMasterMedList')
insert into allowed_clin_procs(proc_name) values ('PDE_NetPro_GetMasterMedList')

GRANT EXECUTE ON dbo.PDE_NetPro_GetMasterMedList TO pht_server_read_only
-- END OF FILE