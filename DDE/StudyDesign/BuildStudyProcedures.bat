rem $Header: /Clients/PHT_TemplateStudy/Template StudyPad/StudyDesign/LPRSQL_4.13+/BuildStudyProcedures.bat 1     12/22/06 10:49a Gningappa $

@ECHO OFF
SET Study=StudyPad414
SET SqlDb=%1
SET SqlSvr=%2
SET UserName=%3
SET PassWord=%4

:get1
IF NOT "%1"=="" GOTO get2
SET SqlDb=StudyPad414

:get2
IF NOT "%2"=="" GOTO get3
SET SqlSvr=COREDB1

:get3
IF NOT "%3"=="" GOTO get4
SET UserName=

:get4
IF NOT "%4"=="" GOTO runit
SET PassWord=

:runit
ECHO Running BuildDataDelivery for study %Study%
ECHO on %SqlSvr%\%SqlDb%
ECHO UN=%UserName%, PW=%PassWord%
ECHO BuildDataDelivery for study %Study% on %SqlSvr%\%SqlDb%  (%UserName% / %PassWord%) > DD_BuildDataDelivery.log
PAUSE

ECHO.
ECHO **DD/PDE General Objects
CALL DD_BuildHelper Grant_DDE_Access.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_UpdateSQLVersion.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper DD_SetDSLandingPage.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper DD_APA_Config.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper DD_Default_form_check.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper LoadPermissions.sql	%UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper StudySyncLogs.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_fn_NVarCharToHTMLEncode.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper DD_Propeller_Health_Outbound.sql %UserName% %PassWord% %SqlDb% %SqlSvr%

ECHO.
ECHO **LogPad App 1.6+
CALL DD_BuildHelper lastDiaryTableSync.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper getLanguageLocale.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
Rem LPA_submitSetupCode.sql MUST be run PRIOR to DD_PatientInfoListSQL_XML29.sql
CALL DD_BuildHelper LPA_submitSetupCode.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
REM LPA_SiteUserAccessCodes.sql updated 20 Jul 2016 to include LPApp 1.10.1+ LPA_getRoleAccessCodes_XML.sql code
CALL DD_BuildHelper LPA_SiteUserAccessCodes.sql %UserName% %PassWord% %SqlDb% %SqlSvr%

ECHO.
ECHO **SitePad SyncSQLs
CALL DD_BuildHelper DD_PivotData.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper DD_PatientInfoListSQL_XML28.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
Rem LPA_submitSetupCode.sql MUST be run PRIOR to DD_PatientInfoListSQL_XML29.sql
CALL DD_BuildHelper DD_PatientInfoListSQL_XML29.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper DD_PatientInfoListSQL_XMLXX.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper DD_UserInfoSQL_SP_XML4.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper SitePad_PDE_SWAPISubjectFullSyncV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper SitePad_PDE_SWAPISiteFullSyncV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%

ECHO.
ECHO **SitePad 6.x SyncSQLs
CALL DD_BuildHelper SitePad_Swapi_GetValidSiteCodes.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper SitePad_Swapi_PostMBNPerformData.sql %UserName% %PassWord% %SqlDb% %SqlSvr%


ECHO.
ECHO **LogPad 4.x SyncSQLs
CALL DD_BuildHelper DD_UserInfoSQL.sql 	%UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper DD_PatientInfoSQL.sql 	%UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper DD_PatientAssignInfoSQL.sql %UserName% %PassWord% %SqlDb% %SqlSvr%

ECHO.
ECHO **LogPad 5.x SyncSQLs
CALL DD_BuildHelper getkrPTfromPatientID.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper ReportInfoSyncV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper SubjectInfoSyncV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_SWAPIFullSyncSQLV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_SWAPIPartialSyncSQLV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_SWAPISyncSQLV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper LP_ActivePhaseELPU.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper LP_PhaseReportInfo.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper LP_GetDeviceVersion.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper NP_GetAllowedSitesForUser.sql %UserName% %PassWord% %SqlDb% %SqlSvr%

ECHO.
ECHO **NetPRO Support
CALL DD_BuildHelper GetSiteCode.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper Getsubjectdetails.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper DD_SWAPI-SetNetProPermissions.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper permit_web_diary.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper NP_GetCountryCodeByKrDOM.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper LPA_getDiary.sql %UserName% %PassWord% %SqlDb% %SqlSvr%

ECHO.
ECHO **LogPad App 1.10.1 SyncSQLs
CALL DD_BuildHelper LPA_LogPadAppUser.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper LPA_getKrdom_XML.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper getLanguageLocale_XML.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper lastDiaryTableSync_XML.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper LPA_ValidateSQL.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_LPAFullSyncSQLV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_LPAPartialSyncSQLV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper synd_GetValidSiteCode.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper synd_GetSubjectsByKrDom.sql %UserName% %PassWord% %SqlDb% %SqlSvr%

ECHO.
ECHO **Syndication 2.3 Build 21 SyncSQLs
CALL DD_BuildHelper getLanguageLocale_XML.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper lastDiaryTableSync_XML.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper LPA_getKrdom_XML.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper LPA_LogPadAppUser.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper LPA_SiteUserAccessCodes.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper LPA_submitSetupCode.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper synd_SyncVisitsAndForms.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper synd_GetSubjectByKrPT.sql %UserName% %PassWord% %SqlDb% %SqlSvr%

ECHO.
ECHO **Syndication 2.4 Build 51 SyncSQLs
CALL DD_BuildHelper Synd_SiteTZ.sql %UserName% %PassWord% %SqlDb% %SqlSvr%

ECHO.
ECHO **PDE Custom Support
REM Please review the PDE_Custom_Sample.sql.txt file for a sample Custom script.
REM unREMark the following line and replace the [PDE_Custom_Sample.sql] with the FILE name of your custom script.  Copy this line to call EACH custom script you have added.

REM CALL DD_BuildHelper [PDE_Custom_Sample.sql] %UserName% %PassWord% %SqlDb% %SqlSvr%

CALL DD_BuildHelper PDE_NetPro_OtherMedResolve %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_NetPro_InsertNewWorkMedIngredients %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_NetPro_InsertNewWorkMed %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_NetPro_GetWorkList %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_NetPro_GetSubjectMedList %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_NetPro_GetOtherRecordList %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_NetPro_GetMasterMedList %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_LPASubjMasterMedListSyncSQLV1 %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_NetPro_LaunchSurveyDataSQLV1 %UserName% %PassWord% %SqlDb% %SqlSvr%


CALL DD_BuildHelper PDE_LPAFullSyncSQLV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_LPAPartialSyncSQLV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_TabletFullSyncSQLV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper PDE_TabletPartialSyncSQLV1.sql %UserName% %PassWord% %SqlDb% %SqlSvr%

ECHO.
ECHO **StudyWorks Configuration
CALL DD_BuildHelper DD_SetMetaDeviceType.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
CALL DD_BuildHelper DD_GrantPermissions.sql %UserName% %PassWord% %SqlDb% %SqlSvr%
REM Update the IG Meta Procs LAST.
CALL DD_BuildHelper UpdateIG_TableProcs.sql %UserName% %PassWord% %SqlDb% %SqlSvr%

ECHO.
ECHO Done
ECHO.
ECHO ***********************************************************************
REM look for errors
FINDSTR "Msg Invalid Incorrect Error Cannot" DD_BuildDataDelivery.log > NUL
IF %ERRORLEVEL% EQU 0 (ECHO Failure exit with Return Code = 1 at >> DD_BuildDataDelivery.log)
IF %ERRORLEVEL% EQU 1 (ECHO Success exit with Return Code = 0 at >> DD_BuildDataDelivery.log)
DATE /T >> DD_BuildDataDelivery.log
TIME /T >> DD_BuildDataDelivery.log
IF %ERRORLEVEL% EQU 0 (ECHO Errors listed below:  && TYPE DD_BuildDataDelivery.log)
PAUSE