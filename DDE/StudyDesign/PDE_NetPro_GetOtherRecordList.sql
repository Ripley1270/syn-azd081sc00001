-- START OF FILE
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PDE_NetPro_GetOtherRecordList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PDE_NetPro_GetOtherRecordList]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

IF OBJECT_ID('tempdb..#tempVersionTable') is NOT NULL DROP TABLE #tempVersionTable

Select procVersion = '1.00' -- TODO: UPDATE VERSION
Into #tempVersionTable

GO

CREATE procedure dbo.PDE_NetPro_GetOtherRecordList
	(@krpt varchar (255),
	@deviceID [varchar] (10),
	@logTimeClient [varchar] (36),
	@logTimeClientTZOffset [varchar] (36),	
	@otherMedCode varchar(30))
as

SET NOCOUNT ON
SET ANSI_WARNINGS OFF 

/* INSTRUCTIONS: 
* 1)	Make sure the first line of this file contain "START OF FILE".
*		Also check if the last line of this file contain "END OF FILE"
* 2)	Change procVersion ABOVE with each update
* 3)	Change history BELOW with each update
**/

/*****************************************************************************************************************
ERT Med Module PDE_NetPro_GetOtherRecordList
Version	    Date		Change
1.00		17APR2017	[schan] Initial Release
*****************************************************************************************************************/
Declare @syncdata varchar(max)

Declare @ProcedureName varchar(256)
Select @ProcedureName = OBJECT_NAME(@@PROCID)

--Get Other Record Dates
Declare @OtherWorkTable table (id int identity, MED_ID1N varchar(30), MEDTKN1S varchar(36))

Insert into @OtherWorkTable(MED_ID1N, MEDTKN1S)
Select ig.MED_ID1N,
		ig.MEDTKN1S
From ig_DiaryMedRepeating ig with (nolock)
    join lookup_su su with (nolock)
	on su.sigorig = ig.sigorig
Where @krpt = su.krpt
    and su.deleted = 0
	and ig.MEDCOD1C = @otherMedCode	
	and not exists (Select *
	                From ig_OtherMedResolve rec with (nolock)
					    join lookup_su su2 with (nolock)
						on su2.sigorig = rec.sigorig
				    where @krpt = su2.krpt
						and rec.MED_ID1N = ig.MED_ID1N
					    and su2.deleted = 0)
order by su.ReportStartDate_mirror asc

select @syncdata = (
    Select
		(select MED_ID1N as '@MED_ID1N', 
				MEDTKN1S as '@MEDTKN1S'
			From @OtherWorkTable
			order by id asc
		for xml path('med'), type)
	for xml path('othermeds'))					   

Select @syncdata

-- Get Version
Declare @Version varchar(10)
Select @Version = ver.procVersion
From PDE_SQLVersion VER with (nolock)
Where VER.procName = @ProcedureName

-- Add sync data to log
exec [dbo].[insertStudySyncLogs] @ProcedureName, @Version, @krpt, @deviceID, @logTimeClient, @logTimeClientTZOffset, @syncdata

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'PDE_NetPro_GetOtherRecordList' AND TYPE = 'P')
BEGIN

    declare @name as varchar(50)
	Select @name = 'PDE_NetPro_GetOtherRecordList'

	IF OBJECT_ID('tempdb..#tempVersionTable') is NULL
	BEGIN
		RAISERROR (N'WARNING: SQL version is missing. Please verify version is properly defined in [%s] stored procedure.', 
				11, 1, @name); 
	END
	ELSE
	BEGIN
		declare @version as numeric(18,2)	
		Select @version = procVersion from #tempVersionTable
		exec [dbo].[PDE_UpdateSQLVersion] @name, @version		
		DROP TABLE #tempVersionTable
	END 
END

GO

if NOT exists (select * from allowed_clin_procs with (nolock) where proc_name='PDE_NetPro_GetOtherRecordList')
insert into allowed_clin_procs(proc_name) values ('PDE_NetPro_GetOtherRecordList')

GRANT EXECUTE ON dbo.PDE_NetPro_GetOtherRecordList TO pht_server_read_only
-- END OF FILE