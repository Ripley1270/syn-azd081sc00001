USE AZ_D081SC00001;
Go


/***************************************************
1.00	Tan Nguyen 01/30/2019	ONEECOA-134217 - MEDCOD1C to inlcude missing digit (0) where length = 7
****************************************************/
Update MM
set MEDCOD1C  = '0'+MEDCOD1C
From AZ_D081SC00001..DD_MasterMed MM
Where LEN (MEDCOD1C) = 7 


Update MM
set MEDCOD1C  = '0'+MEDCOD1C 
From AZ_D081SC00001..DD_MasterMedIngredRepeating MM
Where LEN (MEDCOD1C) = 7

Update MM
set MEDCOD1C  = '0'+MEDCOD1C
From AZ_D081SC00001..DD_OtherMedWorksheet MM
Where LEN (MEDCOD1C) = 7


