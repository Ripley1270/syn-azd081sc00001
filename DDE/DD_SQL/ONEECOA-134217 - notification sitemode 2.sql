USE DD_AZ_D081SC00001;
Go

/***************************************************
1.00	Tan Nguyen 01/30/2019	ONEECOA-134217 - Update Notification Role to Site_mode  = 2
****************************************************/

 -- Change #1: have site_mode of 2 so they only receive alerts based on their domain
Update nn
set site_mode = 2
from dbo.DD_Notification nn
Where mailTo is null and site_mode = 0