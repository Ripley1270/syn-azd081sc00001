IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LPA_getKrdom_XML]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[LPA_getKrdom_XML]
GO

SET ANSI_NULLS ON

GO
SET QUOTED_IDENTIFIER ON

GO
CREATE procedure dbo.LPA_getKrdom_XML(@krpt varchar(36))
AS
/*****************************************************************************************************************
LPA_getKrdom_XML - get Krdom
Version	Date		Change
1.00	07Oct2015	[bcalderwood] Initial Release
1.01	17Dec2015	[dyildiz] Changes to return data in XML format
*****************************************************************************************************************/

set nocount on

SELECT 	sitecode,
		DM.krdom
FROM 	Lookup_PT PT (nolock)
		JOIN Lookup_domain DM (nolock) on DM.krdom = PT.krdom 
WHERE 	PT.krpt= @krpt  
ORDER BY ptkey ASC
FOR XML RAW ('site'), TYPE, ROOT('Results');
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'LPA_getKrdom_XML' AND TYPE = 'P')
BEGIN
	declare @name as varchar(50)
	declare @version as numeric(18,2)

	select @name = 'LPA_getKrdom_XML', @version = 1.01 -- UPDATE VERSION NUMBER HERE
	exec [dbo].[PDE_UpdateSQLVersion] @name, @version
END

GO
GRANT EXECUTE ON LPA_getKrdom_XML TO pht_server_read_only

IF NOT EXISTS (SELECT * FROM [allowed_clin_procs] WHERE [proc_name] = 'LPA_getKrdom_XML') BEGIN
	INSERT [allowed_clin_procs] ([proc_name]) VALUES ('LPA_getKrdom_XML')
END