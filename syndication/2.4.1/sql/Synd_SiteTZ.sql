IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Synd_AddTimeZone]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Synd_AddTimeZone]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Synd_getTimeZone]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Synd_getTimeZone]
GO



IF NOT EXISTS (SELECT * FROM SYSOBJECTS WHERE id = object_id(N'[dbo].[SyndWebApp_SiteTZ]'))
Begin
    CREATE TABLE [dbo].[SyndWebApp_SiteTZ](
        siteCode NVARCHAR(36),
        time_zone NVARCHAR(36),
        updated DATETIME
    );
End

GO
CREATE PROCEDURE Synd_AddTimeZone
    (
      @siteCode nvarchar(36),
      @time_zone nvarchar(36)
    )
AS
/*****************************************************************************************************************
Synd_addTimeZone - store and return time zone
Version Date        Change
1.00    19Sep2017   [againessmith] Initial Release
*****************************************************************************************************************/

set nocount on
DECLARE @retval varchar(5) = @time_zone

BEGIN TRY

    IF EXISTS (SELECT 1
                 FROM dbo.SyndWebApp_SiteTZ WITH(NOLOCK)
                WHERE [siteCode] = @siteCode
              )
    BEGIN
            SET @retval = 'D';
    END ELSE BEGIN

        INSERT
          INTO dbo.SyndWebApp_SiteTZ (
                    [siteCode],
                    [time_zone],
                    [updated]
                )
        SELECT
            @siteCode,
            @time_zone,
            GETUTCDATE()
    END
END TRY

BEGIN CATCH
    SET @retval = 'E';
END CATCH

SELECT @retval;

GO
IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'Synd_AddTimeZone' AND TYPE = 'P')
BEGIN
    declare @name as varchar(50)
    declare @version as numeric(18,2)

    select @name = 'Synd_AddTimeZone', @version = 1.00 -- UPDATE VERSION NUMBER HERE
    exec [dbo].[PDE_UpdateSQLVersion] @name, @version
END
GRANT EXECUTE ON [dbo].[Synd_AddTimeZone] TO pht_server_read_only
GO


IF NOT EXISTS (SELECT * FROM allowed_clin_procs WHERE proc_name='Synd_AddTimeZone')
    INSERT INTO allowed_clin_procs (proc_name) values ('Synd_AddTimeZone');
GO


CREATE procedure dbo.Synd_getTimeZone(@siteCode varchar(36))
AS
/*****************************************************************************************************************
Synd_getTimeZone - get time zone
Version Date        Change
1.00    19Sep2017   [againessmith] Initial Release
*****************************************************************************************************************/

set nocount on

SELECT  time_zone
FROM    dbo.SyndWebApp_SiteTZ

WHERE   siteCode = @siteCode
GO

IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'Synd_getTimeZone' AND TYPE = 'P')
BEGIN
    declare @name as varchar(50)
    declare @version as numeric(18,2)

    select @name = 'Synd_getTimeZone', @version = 1.00 -- UPDATE VERSION NUMBER HERE
    exec [dbo].[PDE_UpdateSQLVersion] @name, @version
END

GO
GRANT EXECUTE ON Synd_getTimeZone TO pht_server_read_only

IF NOT EXISTS (SELECT * FROM [allowed_clin_procs] WHERE [proc_name] = 'Synd_getTimeZone') BEGIN
    INSERT [allowed_clin_procs] ([proc_name]) VALUES ('Synd_getTimeZone')
END





