//This config file is included only for webstorm to correctly resolve root paths for imports.
//it should not be included or copied in any fashion to the builds
module.exports = {
    resolve: {
        alias: {
            'core'    : path.resolve(__dirname, 'app/core'),
            'sitepad' : path.resolve(__dirname, 'app/sitepad'),
            'logpad'  : path.resolve(__dirname, 'app/logpad'),
            'trainer' : path.resolve(__dirname, 'app/trainer'),
            'web'     : path.resolve(__dirname, 'app/web'),
            'trial'   : path.resolve(__dirname, 'trial'),
            'PDE_Core': path.resolve(__dirname, 'PDE_Core')
        }
    }
};
