module.exports = {
    livereload_logpad_web: {
        tasks: ['browserSync', 'watch:logpad_js', 'watch:logpad_less'],
        options: { logConcurrentOutput: true }
    },
    livereload_logpad_android: {
        tasks: ['exec:taco_android_logpad', 'watch:logpad_less'],
        options: { logConcurrentOutput: true }
    },

    livereload_sitepad_web: {
        tasks: ['browserSync', 'watch:sitepad_js', 'watch:sitepad_less'],
        options: { logConcurrentOutput: true }
    },

    livereload_sitepad_android: {
        tasks: ['exec:taco_android_sitepad', 'watch:sitepad_less'],
        options: { logConcurrentOutput: true }
    },

    livereload_web: {
        tasks: ['browserSync', 'watch:web_js', 'watch:web_less'],
        options: { logConcurrentOutput: true }
    }
};
