module.exports = {
    ios: {
        files: {
            'target/logpad/www': ['target/logpad/www/runtime.bundle.js']
        }
    },
    web: {
        files: {
            'target/web': [
                'target/web/runtime.bundle.js'
            ]
        }
    }
};
