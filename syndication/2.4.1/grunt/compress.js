module.exports = {
    sitepad : {
        options : {
            archive : '<%= target %>/sitepad/www/sitepad.zip',
            mode    : 'zip'
        },
        cwd : '<%= target %>/sitepad/www',
        expand : true,
        src : ['**'],
        dest : './'
    },

    web_dev: {
        options : {
            archive : '<%= target %>/web/web.zip',
            mode    : 'zip'
        },
        cwd : '<%= target %>/web',
        expand : true,
        src : ['**'],
        dest : './'
    },

    web_production_release: {
        options : {
            archive : '<%= target %>/web/release.zip',
            mode    : 'zip'
        },
        cwd : '<%= target %>/web',
        expand : true,
        src : ['**'],
        dest : './'
    },

    compress_windows_sitepad : {
        options : {
            archive : 'sitepad_windows.zip',
            mode    : 'zip'
        },
        cwd : '<%= target %>/sitepad/platforms/windows/AppPackages',
        expand : true,
        src : ['*/**'],
        dest : './'
    },

    compress_jsdoc : {
        options : {
            archive : 'jsdoc.zip',
            mode    : 'zip'
        },
        cwd : 'doc',
        expand : true,
        src : ['**']
    },

    compress_tablet_locale_resources: {
        options: {
            archive: 'tablet_locale_resources.zip',
            mode   : 'zip'
        },
        cwd    : 'trial',
        expand : true,
        flatten: true,
        src    : ['tablet/**/nls/**/*.en_US.json',
            'common_study/**/nls/**/*.en_US.json'],
        dest   : './'
    },

    compress_handheld_locale_resources: {
        options: {
            archive: 'handheld_locale_resources.zip',
            mode   : 'zip'
        },
        cwd    : 'trial',
        expand : true,
        flatten: true,
        src    : ['handheld/**/nls/**/*.en_US.json',
            'common_study/**/nls/**/*.en_US.json'],
        dest   : './'
    }
};
