/**
 * Created by uadhikari on 1/4/2018.
 */
module.exports = {
    cleanup: {
        options: {
            cwd: './'
        },
        files: [{
            src: [
                'trial/handheld/**/*.json',
                'trial/tablet/**/*.json'
            ]
        }]
    }
};
