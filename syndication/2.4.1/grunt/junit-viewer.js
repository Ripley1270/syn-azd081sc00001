module.exports = {
    options: {
        cwd: './',
        srcDirectory: 'junit',
        destDir: 'junit/junit_reports'
    },
    handheld: {
        options: {
            destFile: 'handheld-test-suite_report.html'
        }
    },
    tablet: {
        options: {
            destFile: 'tablet-test-suite_report.html'
        }
    },
    pde_core: {
        options: {
            destFile: 'PDE_Core-test-suite_report.html'
        }
    }
};
