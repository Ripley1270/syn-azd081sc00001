/**
 * Created by uadhikari on 1/4/2018.
 */
'use strict';

module.exports = function (grunt) {

    var fs = require('fs'),
        path = require('path'),
        readFiles = require('../PDE_Core/tools/json_cleanup/readFiles');

    grunt.registerMultiTask('jsonscript', 'Cleans up extraneous HTML tags from JSON files.', function () {

        var options = this.options({ cwd: '' });

        // Force task into async mode and grab a handle to the "done" function.
        var done = this.async();

        let src;

        this.files.forEach(function (file) {
             src = file.src.filter(function (filepath) {
                    filepath = file.cwd ? file.cwd + filepath : filepath;
                    if (!grunt.file.exists(filepath)) {
                        grunt.log.warn('Source File "' + filepath + '" not found.');
                        return false;
                    }
                    return true;
                });

        });

        let returnVals = [],
            cleanedData,
            colorFound,
            colorsFoundArr = [];

        src.forEach(function (filePath) {
            returnVals = readFiles.cleanFile(filePath);
            cleanedData = returnVals[0];
            colorFound = returnVals[1];
            colorsFoundArr.push(colorFound);

            fs.writeFileSync(filePath, cleanedData);
        });

        fs.writeFileSync('trial/colorsFound.txt', colorsFoundArr);

        done();

    });
};
