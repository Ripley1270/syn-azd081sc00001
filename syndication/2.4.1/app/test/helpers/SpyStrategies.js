// Jasmine doesn't easily allow modifications to the SpyStrategy.
// We need overrride the original spyOn method and provided a custom, decorated one.
// SpyStrategy makes use of a lot of local variables, so for some functionality, we might be out of luck.
let originalSpyOn = window.spyOn;
let customSpyOn = function (...args) {
    let spy = originalSpyOn(...args),
        plan;

    let originalExec = spy.and.exec;

    // We need to override the exec method, so our custom ones can execute each plan.
    spy.and.exec = function () {
        if (plan != null) {
            return plan.apply(this, arguments);
        } else {
            // Jasmine must apply the scope of this to the spied upon object.
            return originalExec.apply(this, arguments);
        }
    };

    // Return a resolved promise.
    spy.and.resolve = function (value) {
        plan = function () {
            return Q(value);
        };

        // SpyStrategy returns the result of a private function getSpy, passed in via an argument.
        // We can just directly return the spy instead.
        return spy;
    };

    // Return a rejected promise.
    spy.and.reject = function (value) {
        plan = function () {
            return Q.reject(value);
        };

        return spy;
    };

    return spy;
};

window.spyOn = customSpyOn;
