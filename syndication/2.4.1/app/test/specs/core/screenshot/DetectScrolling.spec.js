import ScrollingDetector from 'core/screenshot/detectFeatures/detectScrolling';
import * as specHelpers from 'test/helpers/SpecHelpers';

TRACE_MATRIX('US7153')
.describe('DetectFeatures', () => {
    describe('loopSections', () => {
        let detector,
            takeScreenshot;

        beforeEach(() => {
            takeScreenshot = jasmine.createSpy('takeScreenshot');
            detector = new ScrollingDetector($.noop, takeScreenshot);
        });

        afterEach(() => {
            takeScreenshot.calls.reset();
        });

        Async.it('should skip if there is nothing to scroll', () => {
            return detector.run()
            .then(() => {
                expect(detector.isDetected).toBe(false);
            });
        });
        Async.it('should loop through every section of a page that requires scrolling', () => {
            const fullHeightSections = 5;
            const height = $(window).height();

            // We scroll 75% of the screen to avoid text truncation.
            const expectedSections = 1 + Math.ceil((height * (fullHeightSections - 1)) / (height * detector.scrollPercentage));

            return detector.run({
                documentHeight: height * fullHeightSections,
                delay: 0
            })
            .then(() => {
                expect(detector.isDetected).toBe(true);
                expect(takeScreenshot).toHaveBeenCalledTimes(expectedSections);
                expect(takeScreenshot).toHaveBeenCalledWith({ suffix: '_SCROLL1' });
            });
        });
    });
});
