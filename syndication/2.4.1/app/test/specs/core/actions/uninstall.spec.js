import Logger from 'core/Logger';
import { eCoaDB, LogDB, createDatabases } from 'core/dataAccess';
import { uninstall } from 'core/actions/uninstall';

describe('uninstall', () => {
    let context = {};

    beforeAll(() => {
        spyOn(Logger.prototype, 'info');
        spyOn(Logger.prototype, 'traceEnter');
        LF.security = jasmine.createSpyObj('security', ['pauseSessionTimeOut']);
        spyOn(Logger.prototype, 'trace');
        spyOn(localStorage, 'clear');
        LF.Wrapper.Utils = jasmine.createSpyObj('Utils', ['setPreference']);
    });

    Async.it('should have been called', () => {
        return uninstall().then(() => {
            expect(Logger.prototype.info).toHaveBeenCalledWith('******************** UNINSTALL ******************** ');
            expect(Logger.prototype.traceEnter).toHaveBeenCalledWith('Uninstall');
            expect(LF.security.pauseSessionTimeOut).toHaveBeenCalled();
            expect(Logger.prototype.trace).toHaveBeenCalledWith('Wiping various local storages');
            expect(localStorage.clear).toHaveBeenCalled();
            expect(Logger.prototype.trace).toHaveBeenCalledWith('Wiping databases');
        });
    });

    Async.it('should have been set up eCoaDB', () => {
        let input = ['extra1', 'extra2'];

        spyOn(eCoaDB, 'collection').and.callThrough();

        return uninstall(input)
        .then(() => {
            expect(eCoaDB.collection).toHaveBeenCalled();
            expect(eCoaDB.collections).toEqual({
                ActiveAlarm: {},
                Answer: {},
                Dashboard: {},
                DatabaseVersion: {},
                ESenseDevice: {},
                LastDiary: {},
                Log: {},
                Site: {},
                StoredSchedule: {},
                Subject: {},
                SubjectAlarm: {},
                Transmission: {},
                User: {},
                extra1: {},
                extra2: {}
            });
        });
    });

    //For this test, I want to test clear, but cannot find the right object to spy on
    // Async.it('should have been clear', () => {
    //     spyOn(eCoaB, 'clear').and.callFake(() => {});
    //     return uninstall().then(() => {
    //         expect(eCoaB.clear).toHaveBeenCalled();
    //     });
    // });

    it('should have returned a promise', () => {
        expect(uninstall()).toBePromise();
    });
});
