import notify from 'core/actions/notify';
import { MessageRepo } from 'core/Notify';
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';
import Logger from 'core/Logger';
import Spinner from 'core/Spinner';

describe('notify', () => {
    beforeEach(() => {
        // Logger = jasmine.createSpyObj('Logger', ['warn', 'info']);
        spyOn(Logger.prototype, 'warn');
        spyOn(Logger.prototype, 'info');
        spyOn(MessageHelpers, 'notifyDialogCreator').and.returnValue(() => Q());
        spyOn(Spinner, 'hide').and.resolve();
        spyOn(MessageRepo, 'display');
    });

    Async.it(`should display dialog when dialogVal is defined`, () => {
        let params = {
            dialog: 'testString',
            key: 'testString'
        };

        return notify(params, () => {}).then((res) => {
            expect(Logger.prototype.warn).not.toHaveBeenCalled();
            expect(Logger.prototype.info).not.toHaveBeenCalled();
            expect(MessageHelpers.notifyDialogCreator).not.toHaveBeenCalled();
            expect(Spinner.hide).toHaveBeenCalled();
            expect(MessageRepo.display).toHaveBeenCalled();
            expect(res).toEqual(true);
        });
    });

    Async.it(`should notify dialog not found in registry`, () => {
        let params = {key: 'testString'};

        return Q.Promise(resolve => {
            notify(params, ()=> {});
            expect(Logger.prototype.warn).toHaveBeenCalled();
            expect(Logger.prototype.info).not.toHaveBeenCalled();
            expect(MessageHelpers.notifyDialogCreator).not.toHaveBeenCalled();
            expect(Spinner.hide).not.toHaveBeenCalled();
            expect(MessageRepo.display).not.toHaveBeenCalled();
            expect(notify(params, ()=> {})).toEqual(true);
            resolve();
        });
    });

    Async.it(`should notify dialog being shown without being registered, 
        message will not be available for screenshots.`, () => {
        let params = {};

        return Q.Promise((resolve) => {
            notify(params, () => {
                expect(Logger.prototype.warn).not.toHaveBeenCalled();
                expect(Logger.prototype.info).toHaveBeenCalled();
                expect(MessageHelpers.notifyDialogCreator).toHaveBeenCalled();
                expect(Spinner.hide).not.toHaveBeenCalled();
                expect(MessageRepo.display).not.toHaveBeenCalled();
                resolve();
            });
        });
    });
});
