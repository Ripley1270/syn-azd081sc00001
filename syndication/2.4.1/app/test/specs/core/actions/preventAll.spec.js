import preventAll from 'core/actions/preventAll';

describe('preventAll', () => {
    Async.it(`should return an object literal with preventDefault
    set to true and stopRules set to true.`, () => {
        return preventAll()
        .then((res) => {
            expect(res).toEqual({ preventDefault: true, stopRules: true });
        });
    });
});
