import State from 'sitepad/classes/State';
import setState from 'sitepad/actions/setState';

describe('setState', () => {
    beforeAll(() => {
        spyOn(State, 'set');
    });

    Async.it('State.set should have been called with toState', () => {
        let toState = '';
        return setState(toState)
        .then(() => {
            expect(State.set).toHaveBeenCalledWith(toState);
        });
    });
});
