import { confirm } from 'core/actions/confirm';
import Spinner from 'core/Spinner';
import { MessageRepo } from 'core/Notify';
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';
import Logger from 'core/Logger';

describe('confirm', () => {
    let params = {
        header: 'DummyHeader',
        message: 'Dummy Message',
        dialog: 'You have successfully updated your password.',
        key: 'Dummy key',
        options: {
            interrupt: true
        }
    };
    let paramsNullDialogAndKey = {
        header: 'DummyHeader',
        message: 'Dummy Message',
        options: {
            interrupt: true
        }
    };
    let paramsNullDialog = {
        header: 'DummyHeader',
        message: 'Dummy Message',
        key: 'Dummy key',
        options: {
            interrupt: true
        }
    }
    let test = {
        fakeFunction: (value) => {
            return value;
        }
    };

    Async.it('Should called the Spinner.hide, MessageRepo.display and the callback methods', () => {
        spyOn(Spinner, 'hide').and.resolve();
        spyOn(MessageRepo, 'display');
        spyOn(test, 'fakeFunction');
        return confirm(params, test.fakeFunction)
        .then(() => {
            expect(Spinner.hide).toHaveBeenCalled();
            expect(MessageRepo.display).toHaveBeenCalledWith(params.dialog,params.options);
            expect(test.fakeFunction).toHaveBeenCalledWith(true);
        });
    });

    Async.it('Should call the logger.info, MessageHelpers.confirmDialogCreator and Spinner.hide() methods', () => {
        let message = 'Confirm dialog being shown without being registered, message will not be available for screenshots.'
        spyOn(Logger.prototype, 'info').and.resolve();
        spyOn(Spinner, 'hide').and.resolve();
        spyOn(MessageHelpers, 'confirmDialogCreator').and.returnValue(() => Q());
        spyOn(test, 'fakeFunction');
        return confirm(paramsNullDialogAndKey, test.fakeFunction)
        .then(() => {
            expect(Logger.prototype.info).toHaveBeenCalledWith(message);
            expect(MessageHelpers.confirmDialogCreator).toHaveBeenCalledWith(paramsNullDialogAndKey);
            expect(Spinner.hide).toHaveBeenCalled();
            expect(test.fakeFunction).toHaveBeenCalledWith(true);
        });
    });

    Async.it('Should call the logger.warn and callback methods', () => {
        spyOn(Logger.prototype, 'warn').and.resolve();
        spyOn(test, 'fakeFunction');
        return confirm(paramsNullDialog, test.fakeFunction)
        .then(() => {
            expect(Logger.prototype.warn).toHaveBeenCalledWith('Dialog "Dummy key" not found in registry.');
            expect(test.fakeFunction).toHaveBeenCalledWith(true);
        });
    });
});
