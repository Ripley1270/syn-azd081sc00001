const { Model } = Backbone;

import { NumberSpinnerInputOptions } from 'core/widgets/input/NumberSpinnerInput';
import NumberSpinnerInput from 'core/widgets/input/NumberSpinnerInput';
import BaseSpinnerInput from 'core/widgets/input/BaseSpinnerInput';

const DEFAULT_MIN = 0,
    DEFAULT_MAX = 100,
    DEFAULT_STEP = 1,
    DEFAULT_SHOW_LEADING_ZEROS = false,
    DEFAULT_PRECISION = 0,
    DEFAULT_INCLUDE_BLANK_VALUE = true;

/* global describe, beforeEach, afterEach, beforeAll, afterAll, it, spyOn */

TRACE_MATRIX('US6106')
.describe('NumberSpinnerInput', () => {
    let options,
        spinnerInput,
        $parent,
        oldTemplateSettings;

    beforeAll(() => {
        // Set the correct interpolation settings for templates.
        oldTemplateSettings = _.templateSettings;
        _.templateSettings = {
            evaluate: /\{\[([\s\S]+?)]}/g,
            interpolate: /\{\{(.+?)}}/g
        };
    });

    afterAll(() => {
        _.templateSettings = oldTemplateSettings;
    });

    beforeEach(() => {
        $parent = $('<div class="spinner-container">');

        document.documentElement.appendChild($parent[0]);

        options = new NumberSpinnerInputOptions({
            min: 1,
            max: 99,
            step: 0.5,
            precision: 2,
            showLeadingZeros: true,
            includeBlankValue: true
        });

        options = _.extend(options, {
            parent: $parent[0],
            itemTemplate: 'DEFAULT:NumberItemTemplate',
            template: LF.templates.display('DEFAULT:NumberSpinnerControl'),
            deceleration: 0.003,
            numItems: 5,
            model: new Model()
        });
    });

    afterEach(() => {
        if (spinnerInput) {
            spinnerInput.remove();
            spinnerInput = null;
        }
        if ($parent) {
            $parent.remove();
            $parent = null;
        }
    });

    describe('.NumberSpinnerInputOptions class', () => {
        it('can be constructed, and contains properties from options', () => {
            expect(options instanceof NumberSpinnerInputOptions).toBe(true);
            expect(options.min).toBe(1);
            expect(options.max).toBe(99);
            expect(options.step).toBe(0.5);
            expect(options.precision).toBe(2);
            expect(options.showLeadingZeros).toBe(true);
            expect(options.includeBlankValue).toBe(true);
        });
    });

    TRACE_MATRIX('DE22219')
    .TRACE_MATRIX('DE17681')
    .describe('Tests before rendering', () => {
        describe('#constructor', () => {
            it('can be constructed as a BaseSpinnerInput with values in our model', () => {
                spinnerInput = new NumberSpinnerInput(options);
                expect(spinnerInput instanceof BaseSpinnerInput).toBe(true);
                expect(spinnerInput.model.get('min')).toBe(options.min);
                expect(spinnerInput.model.get('max')).toBe(options.max);
                expect(spinnerInput.model.get('step')).toBe(options.step);
                expect(spinnerInput.model.get('precision')).toBe(options.precision);
                expect(spinnerInput.model.get('showLeadingZeros')).toBe(options.showLeadingZeros);
                expect(spinnerInput.model.get('includeBlankValue')).toBe(options.includeBlankValue);
            });

            it('can be constructed with option defaults', () => {
                options.min = undefined;
                options.max = undefined;
                options.step = undefined;
                options.showLeadingZeros = undefined;
                options.precision = undefined;
                options.includeBlankValue = undefined;
                spinnerInput = new NumberSpinnerInput(options);
                expect(spinnerInput.model.get('min')).toBe(DEFAULT_MIN);
                expect(spinnerInput.model.get('max')).toBe(DEFAULT_MAX);
                expect(spinnerInput.model.get('step')).toBe(DEFAULT_STEP);
                expect(spinnerInput.model.get('showLeadingZeros')).toBe(DEFAULT_SHOW_LEADING_ZEROS);
                expect(spinnerInput.model.get('precision')).toBe(DEFAULT_PRECISION);
                expect(spinnerInput.model.get('includeBlankValue')).toBe(DEFAULT_INCLUDE_BLANK_VALUE);
            });
        });

        describe('#itemDisplayValueFunction', () => {
            it('returns a string (not number type) unaltered', () => {
                let spinnerInput = new NumberSpinnerInput(options),
                    value = 'a string';
                spyOn(NumberSpinnerInput, 'getNumericDisplayValue');
                expect(spinnerInput.itemDisplayValueFunction(value)).toBe(value);
                expect(NumberSpinnerInput.getNumericDisplayValue.calls.count()).toBe(0);
            });

            it('calls getNumericDisplayValueFunction with params. Returns value from that.', () => {
                let spinnerInput = new NumberSpinnerInput(options),
                    value = 15,
                    returnVal = 'OK';
                spyOn(NumberSpinnerInput, 'getNumericDisplayValue').and.callFake(() => {
                    return 'OK';
                });
                expect(spinnerInput.itemDisplayValueFunction(value)).toBe(returnVal);
                expect(NumberSpinnerInput.getNumericDisplayValue.calls.count()).toBe(1);
                expect(NumberSpinnerInput.getNumericDisplayValue)
                    .toHaveBeenCalledWith(value, options.precision, options.showLeadingZeros);
            });
        });

        describe('#itemValueFunction', () => {
            it('returns un-parseable values unaltered', () => {
                let spinnerInput = new NumberSpinnerInput(options),
                    value = 'a string';
                expect(spinnerInput.itemValueFunction(value)).toBe(value);
            });

            it('returns other values fixed to our specified precision', () => {
                let spinnerInput = new NumberSpinnerInput(options),
                    value = '15.7652';
                expect(spinnerInput.itemValueFunction(value)).toBe(15.77);
                spinnerInput.model.set('precision', 0);
                expect(spinnerInput.itemValueFunction(value)).toBe(16);
            });
        });

        describe('#getNumericDisplayValue', () => {
            it('returns a string of the float value set to our precision', () => {
                let value = 28.8532;
                expect(NumberSpinnerInput.getNumericDisplayValue(value, 2, true)).toBe('28.85');
                expect(NumberSpinnerInput.getNumericDisplayValue(value)).toBe('29');
                expect(NumberSpinnerInput.getNumericDisplayValue(value, 1, true)).toBe('28.9');
            });

            it('strips leading zeros', () => {
                let value = 0.9763;
                expect(NumberSpinnerInput.getNumericDisplayValue(value, 2, true)).toBe('0.98');
                expect(NumberSpinnerInput.getNumericDisplayValue(value, 2, false)).toBe('.98');
            });
        });
    });

    TRACE_MATRIX('DE17681')
    .describe('Tests after rendering', () => {
        beforeEach((done) => {
            spinnerInput = new NumberSpinnerInput(options);
            spinnerInput.render().then(() => {
                done();
            });
        });
        describe('#setValues', () => {
            it('sets all values from min/max by step intervals', () => {
                let min = 0,
                    max = 0.99,
                    step = 0.01,
                    precision = 2;

                spinnerInput.model.set('min', min);
                spinnerInput.model.set('max', max);
                spinnerInput.model.set('step', step);
                spinnerInput.model.set('precision', precision);

                spyOn(spinnerInput, 'itemValueFunction').and.callFake((val) => {
                    return `value:${val}`;
                });

                spyOn(spinnerInput, 'itemDisplayValueFunction').and.callFake((val) => {
                    return `display value:${val}`;
                });

                // Clear items that were entered during render.
                spinnerInput.$items.remove();
                spinnerInput.clearItemCache();
                spinnerInput.setValues();

                // verify dataset begins with a blank value
                expect($(spinnerInput.$items[0]).data('value')).toBe('');

                // iterate our values in a for loop and make sure they appended in the right order,
                //  utilizing the value and display value functions
                let index = 0;
                for (let i = min; i <= parseFloat(max); i = parseFloat((i + step).toFixed(precision))) {
                    let $item = $(spinnerInput.$items[++index]);
                    expect($item.data('value')).toBe(`value:${i}`);
                    expect($item.html()).toBe(`display value:${i}`);
                }
            });
        });

        it('sets max in model to the actual max, after iterating with the step value', () => {
            let min = 0,
                max = 0.99,
                step = 0.05,
                precision = 2;

            spinnerInput.model.set('min', min);
            spinnerInput.model.set('max', max);
            spinnerInput.model.set('step', step);
            spinnerInput.model.set('precision', precision);

            // Clear items that were entered during render.
            spinnerInput.$items.remove();
            spinnerInput.clearItemCache();
            spinnerInput.setValues();

            // verify dataset begins with a blank value
            expect(spinnerInput.model.get('max')).toBe(0.95);
        });

        it('does not contain a blank value if includeBlankValue is false', () => {
            let min = 0,
                max = 0.99,
                step = 0.05,
                precision = 2;

            spinnerInput.model.set('min', min);
            spinnerInput.model.set('max', max);
            spinnerInput.model.set('step', step);
            spinnerInput.model.set('precision', precision);
            spinnerInput.model.set('includeBlankValue', false);

            // Clear items that were entered during render.
            spinnerInput.$items.remove();
            spinnerInput.clearItemCache();
            spinnerInput.setValues();

            // verify dataset does not begin with a blank value
            expect($(spinnerInput.$items[0]).data('value')).not.toBe('');
        });
    });
});
