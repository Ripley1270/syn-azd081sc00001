import 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';
import 'core/resources/Templates';

import WidgetBaseTests from './WidgetBase.specBase.js';
import Widget from 'core/models/Widget';
import ImageWidget from 'core/widgets/ImageWidget';

const defaultImageTemplate = 'DEFAULT:ImageTemplate',
    defaultImageFolder = 'IMAGE_FOLDER',
    defaultSingleSelect = false,
    defaultOptions = [{
        value: '0',
        css: {
            fill: '#FFFFFF'
        }
    }, {
        value: '1',
        css: {
            fill: '#0000FF'
        }
    }],
    largeOptionsArray = [{
        value: '0',
        css: {
            fill: '#FFFFFF'
        }
    }, {
        value: '1',
        css: {
            fill: '#0000FF'
        }
    }, {
        value: '40',
        css: {
            fill: 'green'
        }
    }, {
        value: '50',
        css: {
            fill: 'orange'
        }
    }];

/*global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
export default class ImageWidgetTests extends WidgetBaseTests {
    constructor (model = ImageWidgetTests.model) {
        super(model);
    }

    /**
     * {Widget} get the widget model.
     */
    static get model () {
        return new Widget({
            id: 'IMAGE_WIDGET_W_1',
            type: 'ImageWidget',
            className: 'IMAGE_WIDGET_W_1',
            imageName: 'skeleton.svg',
            imageText: {
                legend0: 'Q1_LEGEND_0',
                legend1: 'Q1_LEGEND_1'
            },
            nodeIDRegex: /[rl]\d+/
        });
    }

    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${this.dummyParent.id}`).find('object.imageSvg').length).toEqual(1);
            });
    }

    execTests () {

        // Override i18n, to return static values for strings as we would need them.
        beforeEach(() => {
            let oldI18n = ImageWidget.prototype.i18n;

            spyOn(ImageWidget.prototype, 'i18n').and.callFake(function (...args) {
                return Q().then(() => {
                    return oldI18n.apply(this, args);
                }).then((val) => {
                    let results = val;
                    if (typeof results === 'object' && results.imageFolder) {
                        results.imageFolder = 'base/test/images/';
                    }
                    return results;
                });
            });

        });
        super.execTests();

        // eslint-disable-next-line new-cap
        TRACE_MATRIX('US6808').
        describe('ImageWidget', () => {
            let templates,
                dummyParent,
                security,
                imageWidget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();

                imageWidget = new LF.Widget[this.model.get('type')]({
                    model: this.model,
                    mandatory: false,
                    parent: dummyParent
                });
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();

                imageWidget = null;
            });

            describe('getters', () => {
                beforeEach (() => {
                    let basicModel = new Widget({
                        id: 'IMAGE_WIDGET_W_1',
                        type: 'ImageWidget',
                        className: 'IMAGE_WIDGET_W_1',
                        imageName: 'skeleton.svg',
                        nodeIDRegex: /[rl]\d+/
                    });
                    imageWidget = new LF.Widget[basicModel.get('type')]({
                        model: basicModel,
                        mandatory: false,
                        parent: this.dummyParent
                    });
                });

                describe ('#defaultImageTemplate', () => {
                    it ('returns expected value', () => {
                        expect(imageWidget.defaultImageTemplate).toBe(defaultImageTemplate);
                    });
                });
                describe ('#defaultImageFoler', () => {
                    it ('returns expected value', () => {
                        expect(imageWidget.defaultImageFolder).toBe(defaultImageFolder);
                    });
                });
                describe ('#defaultSingleSelect', () => {
                    it ('returns expected value', () => {
                        expect(imageWidget.defaultSingleSelect).toBe(defaultSingleSelect);
                    });
                });
                describe ('#defaultOptions', () => {
                    it ('returns expected value', () => {
                        expect(imageWidget.defaultOptions).toEqual(defaultOptions);
                    });
                });
            });

            describe('constructor', () => {
                it('initalizes with default values for $svgPaths and completed', () => {
                    let basicModel = new Widget({
                        id: 'IMAGE_WIDGET_W_1',
                        type: 'ImageWidget',
                        className: 'IMAGE_WIDGET_W_1',
                        imageName: 'skeleton.svg',
                        nodeIDRegex: /[rl]\d+/
                    });
                    imageWidget = new LF.Widget[basicModel.get('type')]({
                        model: basicModel,
                        mandatory: false,
                        parent: this.dummyParent
                    });

                    expect(imageWidget.completed).toBe(false);
                    expect(imageWidget.$svgPaths).toBe(null);
                });
            });
            describe('setupModelParams', () => {
                Async.it('throws error if imageName is missing', () => {
                    let basicModel = new Widget({
                        id: 'IMAGE_WIDGET_W_1',
                        type: 'ImageWidget',
                        className: 'IMAGE_WIDGET_W_1',
                        nodeIDRegex: /[rl]\d+/
                    });
                    imageWidget = new LF.Widget[basicModel.get('type')]({
                        model: basicModel,
                        mandatory: false,
                        parent: this.dummyParent
                    });

                    return imageWidget.setupModelParams()
                        .then(() => {
                            fail();
                        }).catch((e) => {
                            expect(e).toEqual(new Error('Missing property "imageName" in ImageWidget.'));
                        });
                });

                Async.it('throws error if nodeIDRegex is missing', () => {
                    let basicModel = new Widget({
                        id: 'IMAGE_WIDGET_W_1',
                        type: 'ImageWidget',
                        className: 'IMAGE_WIDGET_W_1',
                        imageName: 'skeleton.svg'
                    });

                    imageWidget = new LF.Widget[basicModel.get('type')]({
                        model: basicModel,
                        mandatory: false,
                        parent: this.dummyParent
                    });
                    
                    return imageWidget.setupModelParams()
                        .then(() => {
                            fail();
                        }).catch((e) => {
                            expect(e).toEqual(new Error('Configuration error.  nodeIDRegex must supplied as a regular expression, used to test if nodes are selectable.'));
                        });
                });

                Async.it('sets model values to defaults', () => {
                    let basicModel = new Widget({
                        id: 'IMAGE_WIDGET_W_1',
                        type: 'ImageWidget',
                        className: 'IMAGE_WIDGET_W_1',
                        imageName: 'skeleton.svg',
                        nodeIDRegex: /[rl]\d+/
                    });
                    imageWidget = new LF.Widget[basicModel.get('type')]({
                        model: basicModel,
                        mandatory: false,
                        parent: this.dummyParent
                    });

                    return imageWidget.setupModelParams().then(() => {
                        expect(imageWidget.model.get('singleSelect')).toBe(false);
                        expect(imageWidget.model.get('options')).toEqual(defaultOptions);
                        expect(imageWidget.model.get('imageText')).toEqual({});
                        expect(imageWidget.model.get('imageFolder')).toBe(defaultImageFolder);
                        expect(imageWidget.model.get('templates')).toEqual({imageTemplate: defaultImageTemplate});
                    });
                });

                Async.it('sets values to model values when supplied', () => {
                    let options = [
                        {
                            value: '0',
                            css: {
                                fill: 'gray'
                            }
                        },
                        {
                            value: '1',
                            css: {
                                fill: 'red'
                            }
                        }],
                        imageText = {
                            TEST_KEY: 'testValue',
                            TEST_KEY2: 'testValue'
                        },
                        templates = {
                            imageTemplate: 'TEST TEMPLATE'
                        },
                        customModel = new Widget({
                            id: 'IMAGE_WIDGET_W_1',
                            type: 'ImageWidget',
                            className: 'IMAGE_WIDGET_W_1',
                            imageName: 'skeleton.svg',
                            nodeIDRegex: /[rl]\d+/,
                            singleSelect: true,
                            options: options,
                            imageText: imageText,
                            imageFolder: 'test/folder',
                            templates: templates
                        });

                    imageWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: this.dummyParent
                    });


                    return imageWidget.setupModelParams().then(() => {
                        expect(imageWidget.model.get('singleSelect')).toBe(true);
                        expect(imageWidget.model.get('options')).toEqual(options);
                        expect(imageWidget.model.get('imageText')).toEqual(imageText);
                        expect(imageWidget.model.get('imageFolder')).toBe('test/folder');
                        expect(imageWidget.model.get('templates')).toEqual(templates);
                    });
                });
            });

            describe('post-rendered methods', () => {
                Async.beforeEach(() => {
                    return imageWidget.render();
                });

                describe('#registerClickEvents', () => {
                    it('registers a click event on the $svgPaths variable', () => {
                        spyOn(imageWidget.$svgPaths, 'on');
                        imageWidget.registerClickEvents();
                        expect(imageWidget.$svgPaths.on.calls.count()).toBe(1);
                        expect(imageWidget.$svgPaths.on).toHaveBeenCalledWith('click', imageWidget.respond);
                    });
                });

                describe('#unRegisterClickEvents', () => {
                    it('removes a click event on the $svgPaths variable', () => {
                        spyOn(imageWidget.$svgPaths, 'off');
                        imageWidget.unRegisterClickEvents();
                        expect(imageWidget.$svgPaths.off.calls.count()).toBe(1);
                        expect(imageWidget.$svgPaths.off).toHaveBeenCalledWith('click', imageWidget.respond);
                    });
                });

                describe('#delegateEvents', () => {
                    it('calls registerClickEvents', () => {
                        spyOn(imageWidget, 'registerClickEvents');
                        imageWidget.delegateEvents();
                        expect(imageWidget.registerClickEvents.calls.count()).toBe(1);
                    });
                });

                describe('#undelegateEvents', () => {
                    it('calls unRegisterClickEvents', () => {
                        spyOn(imageWidget, 'unRegisterClickEvents');
                        imageWidget.delegateEvents();
                        expect(imageWidget.unRegisterClickEvents.calls.count()).toBe(1);
                    });
                });

                describe('#jQueryNodeFilter', () => {
                    it('returns true for an item that matches the regex', () => {
                        expect(imageWidget.jQueryNodeFilter(0, {id: 'r35'})).toBe(true);
                    });
                    it('returns false for an item that does not match the regex', () => {
                        expect(imageWidget.jQueryNodeFilter(0, {id: 'i40'})).toBe(false);
                    });
                    it('responds to changes in nodeIDRegex', () => {
                        imageWidget.model.set('nodeIDRegex', /i\d*/);
                        expect(imageWidget.jQueryNodeFilter(0, {id: 'i40'})).toBe(true);
                        expect(imageWidget.jQueryNodeFilter(0, {id: 'r35'})).toBe(false);
                    });
                });
                
                describe('#jQueryNodeSort', () => {
                    it('returns 1 when first element has a greater ID', () => {
                        let elemA = document.createElement('div'),
                            elemB = document.createElement('div');
                        elemA.id = 'zartan';
                        elemB.id = 'alphabet';

                        expect(imageWidget.jQueryNodeSort(elemA, elemB)).toBe(1);
                    });
                    it('returns -1 when second element has a greater ID', () => {
                        let elemA = document.createElement('div'),
                            elemB = document.createElement('div');
                        elemA.id = '4';
                        elemB.id = '5';

                        expect(imageWidget.jQueryNodeSort(elemA, elemB)).toBe(-1);
                    });
                    it('returns 0 when elements have the same id', () => {
                        let elemA = document.createElement('div'),
                            elemB = document.createElement('div');
                        elemA.id = 'test';
                        elemB.id = 'test';

                        expect(imageWidget.jQueryNodeSort(elemA, elemB)).toBe(0);
                    });
                });
                
                describe('#buildHTML', () => {
                    it ('renders HTML as expected', () => {
                        // already called as part of render
                        expect(imageWidget.$el.find('object.imageSvg').length).toEqual(1);
                    });
                });
                
                describe('#createTextNodeInRect', () => {
                    it ('renders HTML as expected', () => {
                        let $rects = $(imageWidget.$('object.imageSvg')[0].contentDocument).find('rect');
                        let $rect = $($rects.get(0));
                        let $textNode = imageWidget.createTextNodeInRect($rect, 'test');
                        expect(parseFloat($textNode.attr('x'))).toEqual(parseFloat($rect.attr('x')) + parseFloat($rect.attr('width')) / 2);
                        expect(parseFloat($textNode.attr('y'))).toEqual(parseFloat($rect.attr('y')) + parseFloat($rect.attr('height')) / 2);
                        expect($textNode[0].textContent).toBe('test');
                    });
                });

                describe('#injectSVGText', () => {
                    Async.it('calls createTextNodeInRect with text lookups', () => {
                        imageWidget.model.set('imageText', {test1: 'TEST_1', test2: 'TEST_2'});
                        spyOn(imageWidget, 'createTextNodeInRect');

                        return imageWidget.injectSVGText().then(() => {
                            expect(imageWidget.createTextNodeInRect).toHaveBeenCalledWith(jasmine.anything(), '{{ TEST_1 }}');
                            expect(imageWidget.createTextNodeInRect).toHaveBeenCalledWith(jasmine.anything(), '{{ TEST_2 }}');
                        });
                    });
                });

                describe('#getOptionIndex', () => {
                    it('returns array index with the value specified', () => {
                        imageWidget.model.set('options', largeOptionsArray);
                        expect(imageWidget.getOptionIndex('40')).toBe(2);
                    });
                    it('returns -1 if it cannot find the value', () => {
                        imageWidget.model.set('options', largeOptionsArray);
                        expect(imageWidget.getOptionIndex('100')).toBe(-1);
                    });
                });

                describe('#getNextOptionIndex', () => {
                    it('returns array index for next item after value specified', () => {
                        imageWidget.model.set('options', largeOptionsArray);
                        expect(imageWidget.getNextOptionIndex('40')).toBe(3);
                    });
                    it('wraps back to 0 on the last attempt', () => {
                        imageWidget.model.set('options', largeOptionsArray);
                        expect(imageWidget.getNextOptionIndex('50')).toBe(0);
                    });
                });

                describe('#parseAnswers', () => {
                    beforeEach(() => {
                        spyOn($.fn, 'data').and.callThrough();
                        spyOn($.fn, 'css').and.callThrough();                        
                    });
                    
                    Async.it('returns safely if $svgPaths is null', () => {
                        imageWidget.$svgPaths = null;
                        return imageWidget.parseAnswers().then(() => {
                            expect($.fn.data).not.toHaveBeenCalled();
                        });
                    });

                    describe('singleSelect', () => {
                        Async.beforeEach(() => {
                            imageWidget.undelegateEvents();
                            imageWidget.$('object.imageSvg').remove();
                            imageWidget.$el.html('');
                            imageWidget.$svgPaths = null;
                            imageWidget.removeAllAnswers();

                            imageWidget.model.set('singleSelect', true);

                            return imageWidget.render().then(() => {
                                // reset the spies since they were used in render.                                
                                $.fn.data.calls.reset();
                                $.fn.css.calls.reset();
                            });
                        });

                        Async.it('disables all elements, and does not enable any', () => {
                            return imageWidget.parseAnswers().then(() => {
                                expect(imageWidget.$svgPaths.length > 0).toBe(true);
                                expect($.fn.data.calls.count()).toBe(imageWidget.$svgPaths.length);
                                expect($.fn.css.calls.count()).toBe(imageWidget.$svgPaths.length);
                                expect($.fn.data).toHaveBeenCalledWith('state', '0');
                                expect($.fn.css).toHaveBeenCalledWith(imageWidget.model.get('options')[0].css);
                                expect($.fn.data).not.toHaveBeenCalledWith('state', '1');
                                expect($.fn.css).not.toHaveBeenCalledWith(imageWidget.model.get('options')[1].css);
                            });
                        });

                        Async.it('disables all elements, and enables the current answer', () => {
                            imageWidget.answer = imageWidget.addAnswer();

                            return imageWidget.respondHelper(imageWidget.answer, 'r17', true).then(() => {
                                return imageWidget.parseAnswers();
                            }).tap(() => {
                                expect(imageWidget.$svgPaths.length > 0).toBe(true);
                                expect($.fn.data.calls.count()).toBe(imageWidget.$svgPaths.length + 1);
                                expect($.fn.css.calls.count()).toBe(imageWidget.$svgPaths.length + 1);
                                expect($.fn.data).toHaveBeenCalledWith('state', '0');
                                expect($.fn.css).toHaveBeenCalledWith(imageWidget.model.get('options')[0].css);
                                expect($.fn.data).toHaveBeenCalledWith('state', '1');
                                expect($.fn.css).toHaveBeenCalledWith(imageWidget.model.get('options')[1].css);
                            });
                        });
                    });
                    
                    describe('multiSelect', () => {
                        Async.it('disables all elements, and does not enable any, also adds answers', () => {
                            return imageWidget.parseAnswers().then(() => {
                                expect(imageWidget.$svgPaths.length > 0).toBe(true);
                                expect($.fn.data.calls.count()).toBe(imageWidget.$svgPaths.length);
                                expect($.fn.css.calls.count()).toBe(imageWidget.$svgPaths.length);
                                expect($.fn.data).toHaveBeenCalledWith('state', '0');
                                expect($.fn.css).toHaveBeenCalledWith(imageWidget.model.get('options')[0].css);
                                expect($.fn.data).not.toHaveBeenCalledWith('state', '1');
                                expect($.fn.css).not.toHaveBeenCalledWith(imageWidget.model.get('options')[1].css);
                                expect(imageWidget.answers.length).toBe(imageWidget.$svgPaths.length);
                            });
                        });

                        Async.it('disables all elements, and enables the current answers', () => {
                            imageWidget.answer = imageWidget.addAnswer();

                            return imageWidget.respondHelper(imageWidget.answer, 'r17', true).then(() => {
                                return imageWidget.parseAnswers();
                            }).then(() => {
                                return imageWidget.handleMultiSelect($(imageWidget.$svgPaths.first()));
                            }).then(() => {
                                $.fn.data.calls.reset();
                                $.fn.css.calls.reset();
                                return imageWidget.parseAnswers();
                            }).tap(() => {
                                expect(imageWidget.$svgPaths.length > 0).toBe(true);
                                expect($.fn.data.calls.count()).toBe(imageWidget.$svgPaths.length);
                                expect($.fn.css.calls.count()).toBe(imageWidget.$svgPaths.length);
                                expect($.fn.data).toHaveBeenCalledWith('state', '0');
                                expect($.fn.css).toHaveBeenCalledWith(imageWidget.model.get('options')[0].css);
                                expect($.fn.data).toHaveBeenCalledWith('state', '1');
                                expect($.fn.css).toHaveBeenCalledWith(imageWidget.model.get('options')[1].css);
                            });
                        });
                    });
                });
                
                describe('#render', () => {
                    Async.it('loads svg, injects, text, and parses answers', () => {
                        imageWidget.undelegateEvents();
                        imageWidget.$('object.imageSvg').remove();
                        imageWidget.$el.html('');
                        imageWidget.$svgPaths = null;
                        imageWidget.removeAllAnswers();

                        spyOn(imageWidget, 'injectSVGText').and.callThrough();
                        spyOn(imageWidget, 'delegateEvents').and.callThrough();
                        spyOn(imageWidget, 'parseAnswers').and.callThrough();

                        return imageWidget.render().tap(() => {
                            expect(imageWidget.$svgPaths.length > 0).toBe(true);
                            expect(imageWidget.injectSVGText.calls.count()).toBe(1);
                            expect(imageWidget.delegateEvents.calls.count()).toBe(1);
                            expect(imageWidget.parseAnswers.calls.count()).toBe(1);
                        });
                    });
                });

                describe('#generateNodeIDFromIT', () => {
                    it('replaces question pattern out of string', () => {
                        let test = `${imageWidget.getQuestion().id}_TEST_A_BIG_STRING`;
                        expect(imageWidget.generateNodeIDFromIT(test)).toBe('TEST_A_BIG_STRING');
                    });
                });

                describe('#generateITFromNodeID', () => {
                    it('appends question and underscore to string', () => {
                        let test = 'TEST_A_BIG_STRING';
                        expect(imageWidget.generateITFromNodeID(test)).toBe(`${imageWidget.getQuestion().id}_TEST_A_BIG_STRING`);
                    });
                });

                describe('#handleSingleSelect', () => {
                    beforeEach(() => {
                        spyOn($.fn, 'data').and.callThrough();
                        spyOn($.fn, 'css').and.callThrough();
                        spyOn(imageWidget, 'respondHelper').and.callThrough();

                        // Minimal setup for single select
                        imageWidget.removeAllAnswers();
                        imageWidget.model.set('singleSelect', true);
                    });

                    Async.it('tags selected node as selected, unselects other nodes, and calls respond helper', () => {
                        let $node = $(imageWidget.$svgPaths.get(5));

                        return imageWidget.handleSingleSelect($node).tap(() => {
                            expect($.fn.data.calls.count()).toBe(imageWidget.$svgPaths.length);
                            expect($.fn.css.calls.count()).toBe(imageWidget.$svgPaths.length);
                            
                            // Node has been activated.
                            expect($node.data('state')).toBe('1');
                            expect($.fn.css).toHaveBeenCalledWith(imageWidget.model.get('options')[1].css);

                            // Other nodes have been disabled
                            expect($.fn.css).toHaveBeenCalledWith(imageWidget.model.get('options')[0].css);
                            expect($.fn.data).toHaveBeenCalledWith('state', '0');

                            // respondHelper has been called
                            expect(imageWidget.respondHelper).toHaveBeenCalledWith(imageWidget.answer, $node.attr('id'), true);
                        });
                    });

                    Async.it('alters nothing if selected node not passed', () => {
                        return imageWidget.handleSingleSelect().tap(() => {
                            expect($.fn.data.calls.count()).toBe(0);
                            expect($.fn.css.calls.count()).toBe(0);

                            // respondHelper has been called
                            expect(imageWidget.respondHelper.calls.count()).toBe(0);
                        });
                    });
                });

                describe('#handleMultiSelect', () => {
                    beforeEach(() => {
                        spyOn($.fn, 'data').and.callThrough();
                        spyOn($.fn, 'css').and.callThrough();
                        spyOn(imageWidget, 'respondHelper').and.callThrough();
                    });

                    Async.it('toggles state of the selected node', () => {
                        let $node = $(imageWidget.$svgPaths.get(5));
                        return imageWidget.handleMultiSelect($node).then(() => {
                            expect($node.data('state')).toBe('1');
                            expect($.fn.css).toHaveBeenCalledWith(imageWidget.model.get('options')[1].css);
                            $.fn.css.calls.reset();
                            return imageWidget.handleMultiSelect($node);
                        }).tap(() => {
                            expect($node.data('state')).toBe('0');
                            expect($.fn.css).toHaveBeenCalledWith(imageWidget.model.get('options')[0].css);
                        });
                    });

                    Async.it('populates the answer array', () => {
                        let $nodes = $(imageWidget.$svgPaths.slice(0, 5));
                        return imageWidget.handleMultiSelect($nodes).tap(() => {
                            // verify response is 1 for the first 5 elements, and 0 for the rest.

                            expect(imageWidget.answers.size() > 0).toBe(true);

                            for (let i = 0; i < imageWidget.answers.size(); ++i) {
                                expect(imageWidget.answers.at(i).get('response')).toBe(i <= 4 ? '1' : '0');
                            }
                        });
                    });
                });

                describe('#respond', () => {
                    Async.it('calls singleSelect or multiSelect with the value of e.target', () => {
                        let singleTarget = {
                            target: imageWidget.$svgPaths.get(15)
                        };
                        let multiTarget = {
                            target: imageWidget.$svgPaths.get(18)
                        };

                        spyOn(imageWidget, 'handleSingleSelect').and.callFake(($node) => {
                            expect($node.attr('id')).toBe($(singleTarget.target).attr('id'));
                            return Q();
                        });
                        spyOn(imageWidget, 'handleMultiSelect').and.callFake(($node) => {
                            expect($node.attr('id')).toBe($(multiTarget.target).attr('id'));
                            return Q();
                        });

                        return Q().then(() => {
                            imageWidget.model.set('singleSelect', true);
                            return imageWidget.respond(singleTarget);
                        }).then(() => {
                            imageWidget.model.set('singleSelect', false);
                            return imageWidget.respond(multiTarget);
                        }).tap(() => {
                            expect(imageWidget.handleSingleSelect.calls.count()).toBe(1);
                            expect(imageWidget.handleMultiSelect.calls.count()).toBe(1);
                        });
                    });
                });
            });
        });
    }
}

let tester = new ImageWidgetTests();
tester.execTests();
