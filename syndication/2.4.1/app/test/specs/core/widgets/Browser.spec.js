import Widget from 'core/models/Widget';
import WidgetBaseTests from './WidgetBase.specBase';
import 'core/resources/Templates';

const ANSWER_VALUE = '1';

export default class BrowserTests extends WidgetBaseTests {
    static get model () {
        return new Widget({
            id: 'BW_1',
            type: 'Browser',
            className: 'BW_1',
            url: 'www.ert.com',
            linkText: 'Tap'
        });
    }

    static get model_noURL () {
        return new Widget({
            id: 'BW_1',
            type: 'Browser',
            className: 'BW_1'
        });
    }

    constructor (model = BrowserTests.model) {
        super(model);
    }

    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
        .then(() => {
            expect($(`#${widget.linkID}`).length).toBe(1);
        });
    }

    mandatoryTests () {
        const myModel = this.model;
        let widget = new LF.Widget[myModel.get('type')]({
            model: myModel,
            mandatory: false,
            parent: this.dummyParent
        });

        return widget.render()
        .then(() => {
            return Q.Promise((resolve) => {
                widget.$(`#${widget.linkID}`).trigger('click').ready(() => {
                    resolve();
                });
            });
        })
        .tap(() => {
            expect(widget.validate()).toBeTruthy('widget should always validate properly');
        });
    }

    execTests () {
        const browserObj = {
            addEventListener: $.noop
        };
        beforeEach(() => {
            spyOn(window, 'open').and.returnValue(browserObj);

            if (!LF.StudyDesign) {
                LF.StudyDesign = {
                    defaultLanguage: 'en-US'
                };
            }
        });

        super.execTests();

        TRACE_MATRIX('US7178')
        .describe('Browser Widget tests', () => {
            let security,
                templates,
                dummyParent,
                browserWidget;

            beforeAll(() => {
                security = LF.security;
                LF.security = WidgetBaseTests.getDummySecurity();
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
                browserWidget = null;
            });

            describe('Default properties', () => {
                beforeEach(() => {
                    browserWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                });

                describe('#defaultTemplate', () => {
                    it('should be DEFAULT:Browser', () => {
                        expect(browserWidget.defaultTemplate).toEqual('DEFAULT:Browser');
                    });
                });
            });

            describe('Constructor', () => {
                it('should throw an exception if the URL property is not properly set', () => {
                    let model = this.model;
                    model.unset('url');

                    expect(() => {
                        new LF.Widget[model.get('type')]({
                            model,
                            mandatory: false,
                            parent: dummyParent
                        });
                    }).toThrow();
                });
            });

            describe('post-rendered methods', () => {
                Async.beforeEach(() => {
                    browserWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return browserWidget.render();
                });

                describe('#loadStop', () => {
                    it('closes the browser if it matches the URL pattern', () => {
                        let closeCalled = false;
                        browserWidget.model.set('browserCloseURLPattern', 'test.html');
                        browserWidget.browser = {
                            close: () => {
                                closeCalled = true;
                            }
                        };
                        browserWidget.loadStop({ url: 'test.html' });
                        expect(closeCalled).toBe(true);
                    });

                    it('calls markCompleted if it does not match the pattern', () => {
                        let closeCalled = false;
                        browserWidget.model.set('browserCloseURLPattern', 'test2.html');
                        browserWidget.browser = {
                            close: () => {
                                closeCalled = true;
                            }
                        };
                        spyOn(browserWidget, 'markCompleted').and.resolve();
                        browserWidget.loadStop({ url: 'test.html' });
                        expect(closeCalled).toBe(false);
                        expect(browserWidget.markCompleted).toHaveBeenCalled();
                    });
                });

                describe('#loadError', () => {
                    it('sets the browser error string and closes the browser', () => {
                        let closeCalled = false;
                        browserWidget.browser = {
                            close: () => {
                                closeCalled = true;
                            }
                        };
                        browserWidget.loadError();
                        expect(browserWidget.$('.Browser-Error').html()).toBe('{{ BROWSER_LOAD_ERROR }}');
                        expect(closeCalled).toBe(true);
                    });
                });

                describe('#browserExit', () => {
                    it('unsets the browser pointer', () => {
                        browserWidget.browser = 'something';
                        browserWidget.browserExit();
                        expect(browserWidget.browser).toBeNull;
                    });
                });

                describe('#markCompleted', () => {
                    it('sets completed to true and calls the respond helper', () => {
                        let newAnswer = {
                            mock: 'answer'
                        };
                        browserWidget.completed = false;
                        spyOn(browserWidget, 'addAnswer').and.returnValue(newAnswer);
                        spyOn(browserWidget, 'respondHelper').and.resolve();
                        browserWidget.markCompleted();
                        expect(browserWidget.completed).toBe(true);
                        expect(browserWidget.answer).toBe(newAnswer);
                        expect(browserWidget.respondHelper).toHaveBeenCalledWith(newAnswer, ANSWER_VALUE);
                    });
                });

                TRACE_MATRIX('DE22849')
                .describe('#respond', () => {
                    Async.it('should log an answer value of 1 if the link is clicked (unwrapped)', () => {
                        return Q.Promise((resolve) => {
                            browserWidget.$(`#${browserWidget.linkID}`).trigger('click').ready(() => {
                                resolve();
                            });
                        })
                        .tap(() => {
                            expect(browserWidget.answer.get('response')).toEqual('1');
                        });
                    });
                    Async.it('DE22849 - does not add event handlers (unwrapped)', () => {
                        // spoof window.open to create a mock object for this.browser
                        spyOn(browserObj, 'addEventListener');
                        return Q.Promise((resolve) => {
                            browserWidget.$(`#${browserWidget.linkID}`).trigger('click').ready(() => {
                                resolve();
                            });
                        })
                        .tap(() => {
                            expect(browserWidget.browser.addEventListener).not.toHaveBeenCalled();
                        });
                    });
                });
            });
        });
    }
}

let tests = new BrowserTests();
tests.execTests();
