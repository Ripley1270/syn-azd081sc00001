import Language from 'core/models/Language';

(function () {

    describe('Language', function () {

        let model;

        beforeEach(function () {

            model = new Language({
                language    : 'en',
                locale      : 'US',
                resources   : {
                    COMPLETED   : 'Completed',
                    UPCOMING    : 'Upcoming'
                }
            });

        });

        afterEach(function () {
            localStorage.clear();
        });

        it('should have a language of en', function () {

            expect(model.get('language')).toEqual('en');

        });

        it('should have a locale of us', function () {

            expect(model.get('locale')).toEqual('US');

        });

        it('should have a resources property', function () {

            expect(model.get('resources')).toBeDefined();

        });

    });

}).call(this);
