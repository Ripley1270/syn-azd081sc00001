import ActivationBaseViewSuite from './ActivationBaseView.specBase';
import ActivationBaseView from 'core/views/ActivationBaseView';

class ActivationBaseViewTests extends ActivationBaseViewSuite {

    beforeEach () {
        this.view = new ActivationBaseView();

        return super.beforeEach();
    }
}

describe('ActivationBaseView', () => {
    let suite = new ActivationBaseViewTests();

    suite.executeAll({
        id: 'page',
        tagName: 'div',
        template: '#page-tpl',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass'
        ]
    });

    suite.testProperty('tempStorageNameList', []);
});
