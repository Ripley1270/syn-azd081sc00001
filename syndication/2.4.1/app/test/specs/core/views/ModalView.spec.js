import ModalView from 'core/views/ModalView';
import ModalViewTests from './ModalView.specBase';

class Modal extends ModalView {
    constructor () {
        super();

        this.template = '#modal-tpl';
    }
}

class ModalTests extends ModalViewTests {
    beforeEach () {
        this.template = '#modal-view';
        this.view = new Modal();

        $('body').append(`<script id="modal-tpl" type="text/template">
            <h1 class="modal-title">Title</h1>
            <div>{{ content }}</div>
        </script>`);

        return Q();
    }
}

describe('ModalView', () => {
    let suite = new ModalTests();

    suite.testId('modal');
    suite.testClass('modal');
    suite.testAttribute('role', 'dialog');
    suite.testAttribute('aria-hidden', 'true');
    suite.testAttribute('data-backdrop', 'static');
    suite.testGetTemplate();
    suite.testTeardown();
    suite.testShow({ content: 'Hello World' });
    suite.testAddGlyphicon();
    suite.testFindModalElement({ content: 'Hello World' });
    suite.testAddModalClass();
    suite.testApplyStyles({ type: 'error', icon: 'minus-circle'});
    suite.testApplyStyles({ type: 'success', icon: 'check-circle'});
    suite.testApplyStyles({ type: 'warning', icon: 'exclamation-triangle'});
    suite.testHide();
});
