export default {
    studyPhase: {
        SCREENING     : 10,
        RANDOMIZATION : 20,
        TREATMENT     : 30,
        FOLLOWUP      : 40,
        TERMINATION   : 999
    },

    questionnaires: [
        {
            id: 'TEST',
            SU: 'TEST',
            displayName: 'DISPLAY_NAME',
            className: 'medication',
            previousScreen: false,
            triggerPhase: 'TREATMENT',
            screens: ['TEST_S_0', 'TEST_S_1', 'TEST_S_2', 'TEST_S_3', 'TEST_S_4', 'TEST_S_5', 'TEST_S_6', 'TEST_S_7'],
            affidavit: 'DEFAULT',
            branches: [
                {
                    branchFrom: 'TEST_S_3',
                    branchTo: 'TEST_S_5',
                    clearBranchedResponses: true,
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'TEST_Q_3',
                        value: '2'
                    }
                }, {
                    branchFrom: 'TEST_S_3',
                    branchTo: 'TEST_S_6',
                    clearBranchedResponses: true,
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'TEST_Q_3',
                        value: '3'
                    }
                }, {
                    branchFrom: 'TEST_S_4',
                    branchTo: 'TEST_S_6',
                    clearBranchedResponses: false,
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'TEST_Q_4',
                        value: '1'
                    }
                }, {
                    branchFrom: 'TEST_S_4',
                    branchTo: 'AFFIDAVIT',
                    clearBranchedResponses: false,
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'TEST_Q_4',
                        value: '0'
                    }
                }, {
                    branchFrom: 'TEST_S_5',
                    branchTo: 'TEST_S_2',
                    clearBranchedResponses: true,
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'TEST_Q_5',
                        value: '0'
                    }
                }, {
                    branchFrom: 'TEST_S_2',
                    branchTo: 'TEST_S_6',
                    clearBranchedResponses: true,
                    branchFunction: 'isCurrentPhase',
                    branchParams: {
                        value: 'RANDOMIZATION'
                    }
                }, {
                    branchFrom: 'TEST_S_1',
                    branchTo: 'TEST_S_6',
                    clearBranchedResponses: true,
                    branchFunction: 'isCurrentPhase',
                    branchParams: {
                        value: 'TREATMENT'
                    }
                }
            ]
        }
    ],

    screens: [
        {
            id: 'TEST_S_0',
            className: 'TEST_S_0',
            questions: [
                {
                    id: 'TEST_Q_0',
                    mandatory: true
                }
            ]
        }, {
            id: 'TEST_S_1',
            className: 'TEST_S_1',
            questions: [
                {
                    id: 'TEST_Q_1',
                    mandatory: true
                }
            ]
        }, {
            id: 'TEST_S_2',
            className: 'TEST_S_2',
            questions: [
                {
                    id: 'TEST_Q_2',
                    mandatory: false
                }
            ]
        }, {
            id: 'TEST_S_3',
            className: 'TEST_S_3',
            questions: [
                {
                    id: 'TEST_Q_3',
                    mandatory: false
                }
            ]
        }, {
            id: 'TEST_S_4',
            className: 'TEST_S_4',
            questions: [
                {
                    id: 'TEST_Q_4',
                    mandatory: false
                }
            ]
        }, {
            id: 'TEST_S_5',
            className: 'TEST_S_5',
            questions: [
                {
                    id: 'TEST_Q_5',
                    mandatory: false
                }
            ]
        }, {
            id: 'TEST_S_6',
            className: 'TEST_S_6',
            questions: [
                {
                    id: 'TEST_Q_6',
                    mandatory: false
                }
            ]
        }, {
            id: 'TEST_S_7',
            className: 'TEST_S_7',
            questions: [
                {
                    id: 'TEST_Q_7',
                    mandatory: false
                }
            ]
        }
    ],

    questions: [
        {
            id: 'TEST_Q_0',
            IG: 'TEST',
            IT: 'TEST_Q_0',
            skipIT: 'TEST_Q_0_SKP',
            text: 'QUESTION_0',
            className: 'TEST_Q_0',
            widget: {
                id: 'TEST_W_0',
                type: 'RadioButton',
                className: 'TEST_W_0',
                validation: {
                    validationFunc: 'testFunc'
                },
                answers: [
                    {
                        text: 'No',
                        value: 0
                    }, {
                        text: 'Yes',
                        value: 1
                    }, {
                        text: 'Maybe',
                        type: 'number',
                        value: 2
                    }
                ]
            }
        }, {
            id: 'TEST_Q_1',
            IG: 'TEST',
            IT: 'TEST_Q_1',
            text: 'QUESTION_1',
            className: 'TEST_Q_1',
            widget: {
                id: 'TEST_W_1',
                type: 'RadioButton',
                className: 'TEST_W_1',
                answers: [
                    {
                        text: 'YES',
                        value: 0
                    }, {
                        text: 'NO',
                        value: 1
                    }
                ]
            }
        }, {
            id: 'TEST_Q_2',
            IG: 'TEST',
            IT: 'TEST_Q_2',
            skipIT: 'TEST_Q_2_SKP',
            text: 'QUESTION_2',
            className: 'TEST_Q_2',
            widget: {
                id: 'TEST_W_2',
                type: 'RadioButton',
                className: 'TEST_W_2',
                answers: [
                    {
                        text: 'YES',
                        value: 0
                    }, {
                        text: 'NO',
                        value: 1
                    }
                ]
            }
        }, {
            id: 'TEST_Q_3',
            IG: 'TEST',
            IT: 'TEST_Q_3',
            text: 'QUESTION_3',
            className: 'TEST_Q_3',
            widget: {
                id: 'TEST_W_3',
                type: 'RadioButton',
                className: 'TEST_W_3',
                answers: [
                    {
                        text: 'YES',
                        value: '0'
                    }, {
                        text: 'NO',
                        value: '1'
                    }, {
                        text: 'MAYBE',
                        value: '2'
                    }, {
                        text: 'SOMEWHAT',
                        value: '3'
                    }
                ]
            }
        }, {
            id: 'TEST_Q_4',
            IG: 'TEST',
            IT: 'TEST_Q_4',
            skipIT: 'TEST_Q_4_SKP',
            text: 'QUESTION_4',
            className: 'TEST_Q_4',
            widget: {
                id: 'TEST_W_4',
                type: 'RadioButton',
                className: 'TEST_W_4',
                answers: [
                    {
                        text: 'YES',
                        value: 0
                    }, {
                        text: 'NO',
                        value: 1
                    }
                ]
            }
        }, {
            id: 'TEST_Q_5',
            IG: 'TEST',
            IT: 'TEST_Q_5',
            text: 'QUESTION_5',
            className: 'TEST_Q_5',
            widget: {
                id: 'TEST_W_5',
                type: 'RadioButton',
                className: 'TEST_W_5',
                answers: [
                    {
                        text: 'YES',
                        value: 0
                    }, {
                        text: 'NO',
                        value:  1
                    }
                ]
            }
        }, {
            id: 'TEST_Q_6',
            IG: 'TEST',
            IT: 'TEST_Q_6',
            skipIT: 'TEST_Q_6_SKP',
            text: 'QUESTION_6',
            className: 'TEST_Q_6',
            widget: {
                id: 'TEST_W_6',
                type: 'RadioButton',
                className: 'TEST_W_6',
                answers: [
                    {
                        text: 'YES',
                        value: 0
                    }, {
                        text: 'NO',
                        value: 1
                    }
                ]
            }
        }, {
            //US7608 suppress transmission for localOnly set as true
            id: 'TEST_Q_7',
            IG: 'TEST',
            IT: 'TEST_Q_7',
            skipIT: 'TEST_Q_7_SKP',
            text: 'QUESTION_7',
            className: 'TEST_Q_7',
            localOnly: true,
            widget: {
                id: 'TEST_W_7',
                type: 'RadioButton',
                className: 'TEST_W_7',
                answers: [
                    {
                        text: 'YES',
                        value: 0
                    }, {
                        text: 'NO',
                        value: 1
                    }
                ]
            }
        }
    ],

    affidavits: [
        // Do NOT remove this configuration. Please refer to Affidavit_Study_Design_1_4 for more information.
        {
            id: 'DEFAULT',
            text: [
                'AFFIDAVIT_HELP',
                'AFFIDAVIT'
            ],
            krSig: 'AFFIDAVIT',
            widget: {
                id: 'AFFIDAVIT_WIDGET',
                className: 'AFFIDAVIT',
                type: 'CheckBox',
                answers: [
                    {
                        text: 'OK',
                        value: 0
                    }
                ]
            }
        }, {
            id: 'CustomAffidavit',
            text: 'AFFIDAVIT',
            krSig: 'CustomAffidavit',
            widget: {
                id: 'CUSTOM_AFFIDAVIT_WIDGET',
                className: 'AFFIDAVIT',
                type: 'CheckBox',
                answers: [
                    {
                        text: 'OK',
                        value: 0
                    }
                ]
            }
        }, {
            id: 'SignatureAffidavit',
            text: 'SITE_REPORT_AFF',
            krSig: 'SubmitForm',
            widget: {
                id: 'SIGNATURE_AFFIDAVIT_WIDGET',
                className: 'AFFIDAVIT',
                type: 'SignatureBox'
            }
        } , {
            id: 'SignatureAffidavit',
            text: 'SITE_REPORT_AFF',
            krSig: 'SubmitForm',
            widget: {
                id: 'SIGNATURE_AFFIDAVIT_WIDGET',
                className: 'AFFIDAVIT',
                type: 'SignatureBox'
            }
        }, {
            id: 'PasswordAffidavit',
            text: ['SITE_REPORT_PASSWORD_AFF'],
            krSig: 'SubmitForm',
            widget: {
                id: 'PASSWORD_AFFIDAVIT_WIDGET',
                type: 'TextBox',
                className: 'PasswordAffidavit',
                templates: {
                    input: 'PasswordAffidavitPassword'
                },
                validation: {
                    validationFunc: 'checkAffidavitCredentials',
                    params: {
                        errorStrings: {
                            failed: {
                                errString: 'PASSWORD_INVALID',
                                header: 'ERROR_TITLE',
                                ok: 'OK'
                            }
                        }
                    }
                }
            }
        }
    ]
};

export let templates = [
    {
        name: 'Screen',
        namespace: 'DEFAULT',
        template: '<section class="screen {{ className }}"></div>'
    },
    {
        name: 'Question',
        namespace: 'DEFAULT',
        template: '<p class="{{ label }}">{{ text }}</p>'
    },
    {
        name: 'BarcodeScanner',
        namespace: 'DEFAULT',
        template: '<form class="scannerInputForm" data-ajax="false" autocomplete="off" autocorrect="off">' +
                    '<input type="text" class="scannerData" />' +
                    '</form>' +
                    '<div class="scanBtn" data-role="button"></div>'
    },
    // Label wrapper for radio buttons.
    {
        name: 'RadioButtonWrapper',
        namespace: 'DEFAULT',
        template: '<label class="btn btn-default btn-block"><span data-container></span></label>'
    },
    // Default radio button input.
    {
        name: 'RadioButton',
        namespace: 'DEFAULT',
        template: '<input type="radio" id="{{ id }}" name="{{ name }}" value="{{ value }}"/>'
    },

    // Default template for custom RadioButton input that does not include jquery mobile css style.
    {
        name: 'CustomRadioButton',
        namespace: 'DEFAULT',
        template: '<input type="radio" id="{{ id }}" name="{{ name }}" value="{{ value }}" data-role="none"/>'
    },

    // Default template for RadioButton label.
    {
        name: 'CustomRadioButtonLabel',
        namespace: 'DEFAULT',
        template: '<label for="{{ link }}" class="radio-text {{ className }}"> {{ text }} </label>'
    },

    // Default template for RadioButton label.
    {
        name: 'RadioButtonLabel',
        namespace: 'DEFAULT',
        template: '<span class="radio-text {{ className }}"> {{ text }} </span>'
    },

    {
        name: 'VerticalButtonGroup',
        namespace: 'DEFAULT',
        template: '<div class="btn-group-vertical" data-toggle="buttons" data-container></ul>'
    },
    // Label wrapper for radio buttons.
    {
        name: 'CheckBoxWrapper',
        namespace: 'DEFAULT',
        template: '<label class="btn btn-default btn-block"><span data-container></span></label>'
    },
    // Default radio button input.
    {
        name: 'CheckBox',
        namespace: 'DEFAULT',
        template: '<input type="checkbox" id="{{ id }}" name="{{ name }}" value="{{ value }}"/>'
    },
    // Default template for RadioButton label.
    {
        name: 'CheckboxLabel',
        namespace: 'DEFAULT',
        template: '<span class="checkbox-text {{ className }}"> {{ text }} </span>'
    },

    {
        name: 'PasswordAffidavitPassword',
        namespace: 'DEFAULT',
        template: '<div class="form-group"><div class="input-group"><span class="input-group-addon"><span class="fa fa-asterisk"></span></span><input id="{{ id }}" placeholder="{{ placeholder }}" name="{{ name }}" class="{{ className }} form-control" maxlength="{{ maxLength }}" type="password" autocapitalize="off"/></div></div>'
    },

    {
        name: 'FormGroup',
        namespace: 'DEFAULT',
        template: '<div class="form-group" data-container></div>'
    }
];
