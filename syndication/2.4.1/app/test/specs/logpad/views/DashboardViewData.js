export default {

    user: {
        id: 1,
        username: 'admin',
        role: 'admin',
        userType: 'Admin',
        language: 'en-US',
        active: 1
    },

    subject: {
        device_id: 'e10adc3949ba59abbe56e057f20f883e',
        initials: 'bc',
        log_level: 'ERROR',
        id: 1,
        phase: 10,
        phaseStartDateTZOffset: '2013-10-05T08:15:30-05:00',
        phaseTriggered: 'false',
        secret_answer: '1f3870be274f6c49b3e31a0c6728957f',
        secret_question: '0',
        site_code: '267',
        subject_id: '12345',
        subject_password: '2e3af8a17d7c0bff0dcc25521f80a675',
        service_password: '2e3af8a17d7c0bff0dcc25521f80a675',
        subject_number: '54321',
        subject_active: 1,
        krpt: 'krpt.39ed120a0981231',
        enrollmentDate: '2013-10-05T08:15:30-05:00',
        activationDate: '2013-10-05T08:15:30-05:00'
    },

    transmissions: [{
        id: 1
    }],

    schedules: [{
        id: 'Daily_DiarySchedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'P_Daily_Diary'
        },
        scheduleFunction: 'checkAlwaysAvailability'
    }, {
        id: 'WelcomeSchedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'Welcome'
        },
        scheduleFunction: 'checkAlwaysAvailability'
    }, {
        id: 'MedsSchedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'Meds'
        },
        scheduleFunction: 'checkAlwaysAvailability'
    }, {
        id: 'Toss_DiarySchedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'Toss_Diary'
        },
        scheduleFunction: 'checkAlwaysAvailability'
    }, {
        id: 'Screening_TossSchedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'Screening_Toss'
        },
        scheduleFunction: 'checkAlwaysAvailability'
    }, {
        id: 'Date_DiarySchedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'Date_Diary'
        },
        scheduleFunction: 'checkAlwaysAvailability'
    }, {
        id: 'VAS_DiarySchedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'VAS_Diary'
        },
        scheduleFunction: 'checkAlwaysAvailability'
    }, {
        id: 'NRS_DiarySchedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'NRS_Diary'
        },
        scheduleFunction: 'checkAlwaysAvailability'
    }],

    questionnaires: [{
        id: 'P_Daily_Diary',
        SU: 'Daily',
        displayName: 'DISPLAY_NAME',
        className: 'Daily_Diary',
        affidavit: 'DEFAULT',
        previousScreen: true,
        triggerPhase: 'TREATMENT',
        screens: [
            'DAILY_DIARY_S_1'
        ]
    }, {
        id: 'Welcome',
        SU: 'Welcome',
        displayName: 'DISPLAY_NAME',
        className: 'introduction',
        affidavit: 'CustomAffidavit',
        previousScreen: true,
        screens: [
            'WELCOME_S_1'
        ]
    }, {
        id: 'Meds',
        SU: 'Meds',
        displayName: 'DISPLAY_NAME',
        className: 'medication',
        previousScreen: false,
        affidavit: 'DEFAULT',
        screens: [
            'MEDS_S_1'
        ]
    }, {
        id: 'Toss_Diary',
        SU: 'Toss',
        displayName: 'DISPLAY_NAME',
        className: 'TOSS',
        previousScreen: true,
        affidavit: 'DEFAULT',
        screens: [
            'TOSS_S_0'
        ]
    }, {
        id: 'Screening_Toss',
        SU: 'ScrToss',
        displayName: 'DISPLAY_NAME',
        className: 'SCREENING_TOSS',
        previousScreen: true,
        affidavit: 'DEFAULT',
        screens: [
            'SCREENING_TOSS_S_1'
        ]
    }, {
        id: 'Date_Diary',
        SU: 'Date',
        displayName: 'DISPLAY_NAME',
        className: 'DATE_DIARY',
        previousScreen: true,
        affidavit: 'DEFAULT',
        screens: [
            'DATE_DIARY_S_1'
        ]
    }, {
        id: 'VAS_Diary',
        SU: 'VAS',
        displayName: 'DISPLAY_NAME',
        className: 'VAS_DIARY',
        previousScreen: true,
        affidavit: 'DEFAULT',
        screens: [
            'VAS_DIARY_S_1'
        ]
    }, {
        id: 'NRS_Diary',
        SU: 'NRS',
        displayName: 'DISPLAY_NAME',
        className: 'NRS_DIARY',
        previousScreen: true,
        affidavit: 'DEFAULT',
        screens: [
            'NRS_DIARY_S_1'
        ]
    }]
};
