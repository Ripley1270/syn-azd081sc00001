export default {
    schedules: [{
        id: 'LogPadSchedule',
        target: {
            objectType: 'questionnaire',
            id: 'LogPad'
        },
        scheduleFunction: 'checkRepeatByDateAvailability',
        phase: [
            'SCREENING'
        ],
        scheduleParams: {
            startAvailability: '01:00',
            endAvailability: '19:00'
        },
        alarmFunction: 'addAlwaysAlarm',
        alarmParams: {
            id: 1,
            time: '9:00',
            repeat: 'daily',
            subjectConfig: {
                minAlarmTime: '9:00',
                maxAlarmTime: '15:00',
                alarmRangeInterval: 30,
                alarmOffSubject: true
            }
        }
    }, {
        id: 'LogPad1Schedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'LogPad1'
        },
        scheduleFunction: 'checkRepeatByDateAvailability',
        phase: [
            'SCREENING'
        ],
        scheduleParams: {
            startAvailability: '00:01',
            endAvailability: '23:58'
        }
    }]
};
