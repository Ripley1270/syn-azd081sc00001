import ContextSwitchingView from 'logpad/views/ContextSwitchingView';
import Subject from 'core/models/Subject';
import { Banner } from 'core/Notify';
import Users from 'core/collections/Users';
import Subjects from 'core/collections/Subjects';
import Languages from 'core/collections/Languages';
import Session from 'core/classes/Session';
import Data from 'core/Data';
import CurrentContext from 'core/CurrentContext';

import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

/*
 * These will most likely just be removed once UniversalLogin is complete, and new tests
 * in a new file will be created.
 */
xdescribe('ContextSwitchingView', () => {

    let view,
        template = `<div id="context-tpl">
            <select id="site" name="site" data-placeholder="Select User"></select>
            <input id="password" name="password" type="password" placeholder="{{ password }}..."/>
            <button id="login" type="submit" disabled> Login </button>
        </div>`;

    Async.beforeEach(() => {
        let userName,
            validRoles = ['admin', 'subject'],
            roleOwners = {},
            loginFunction = $.noop,
            arrowFunction = $.noop,
            questionnaireId = 'DAILY_DIARY';

        // Append the template to the body of the DOM, and then reset the Study Design.
        $('body').append(template);
        resetStudyDesign();

        LF.security = new Session();

        let languages = new Languages([{
            namespace: 'CORE',
            language: 'en',
            locale: 'US',
            localized: 'English (US)',
            direction: 'ltr',
            resources: {}
        }]);

        LF.router = { navigate: $.noop };
        spyOn(LF.router, 'navigate');

        return specHelpers.initializeContent()
            .then(() => {
                CurrentContext().set('defaultLanguageModels', languages.toJSON());
                spyOn(CurrentContext(), 'setContextByUser').and.callFake(() => Q());
            })
            .then(() => Users.clearStorage())
            .then(() => {
                return Subjects.clearStorage();
            })
            .then(() => {
                specHelpers.createUsers();

                const user = Data.Users.at(0),
                    userName = '100';
                user.set('userType', 'Subject');
                user.set('username', userName);

                return specHelpers.saveUsers();
            })
            .then(() => {
                Data.subject = new Subject({
                    subject_active: 1,
                    subject_id: '456',
                    subject_number: userName,
                    service_password: 'aaa',
                    phaseStartDateTZOffset: '2013-10-05T08:15:30-05:00',
                    device_id: '101',
                    site_code: '0081',
                    initials: 'fg',
                    krpt: 'DOM.1234.1234',
                    phase: 10
                });
                roleOwners.subject = Data.subject;
                return Data.subject.save();
            })
            .then(() => {
                view = new ContextSwitchingView({
                    validRoles,
                    roleOwners,
                    loginFunction,
                    arrowFunction,
                    questionnaireId
                });
                return view.resolve();
            })
            .then(() => {
                return view.render();
            });
    });

    afterEach(() => $('#context-tpl').remove());

    // This test will fail. The id property needs to be dropped to the prototype chain.
    it('should have an id.', () => {
        expect(view.$el.attr('id')).toEqual('context-switching-view');
    });

    describe('method:renderUsers', () => {
        Async.it('shouldn\'t add any users (no users).', () => {
            spyOn(view, 'addUser');

            view.users = new Users();
            return view.renderUsers()
                .tap(() => {
                    expect(view.userSelectData.length).toBe(0);
                    expect(view.addUser).not.toHaveBeenCalled();
                });
        });

        Async.it('should fail to render the users (failed translation).', () => {
            let err = new Error('Translation error.');

            spyOn(view, 'i18n').and.callFake(() => {
                throw err;
            });

            return view.renderUsers()
                .then(() => {
                    fail('expected error to be thrown');
                })
                .catch((err) => {
                    expect(err).toBe(err);
                });
        });
    });

    describe('method:login', () => {
        Async.it('shouldn\'t login', () => {
            spyOn(LF.security, 'login');

            return view.login({
                preventDefault: $.noop
            })
            .then(() => {
                expect(LF.security.login).not.toHaveBeenCalled();
            });
        });

        Async.it('should login and call loginFunction', () => {
            let users = new Users();

            return users.fetch()
                .then(() => {
                    view.$password.val('password');

                    spyOn(view, 'loginFunction');
                    spyOn(Banner, 'closeAll');

                    spyOn(LF.security, 'login').and.callFake(() => {
                        return Q(users.at(1));
                    });

                    return view.login({
                        preventDefault: $.noop
                    });
                })
                .tap(() => {
                    expect(view.loginFunction).toHaveBeenCalled();
                });
        });
    });
});
