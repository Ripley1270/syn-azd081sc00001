import Logger from 'core/Logger';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import StudyDesign from 'core/classes/StudyDesign';
import UserSync from 'core/classes/UserSync';
import PatientSession from 'core/classes/PatientSession';
import * as lStorage from 'core/lStorage';
import { eCoaDB } from 'core/dataAccess';
import { MessageRepo } from 'core/Notify';
import setupCoreMessages from 'core/resources/Messages';
import { syncHistoricalData } from 'core/actions/syncHistoricalData';

import Router from 'logpad/Router';
import ApplicationController from 'logpad/controllers/ApplicationController';
import RegistrationController from 'logpad/controllers/RegistrationController';
import SettingsController from 'logpad/controllers/SettingsController';

import Transmissions from 'core/collections/Transmissions';
import Dashboards from 'core/collections/Dashboards';
import Subjects from 'core/collections/Subjects';
import Answers from 'core/collections/Answers';
import Users from 'core/collections/Users';

import User from 'core/models/User';
import Role from 'core/models/Role';

import Data from 'core/Data';

import trial from 'core/index';
import COOL from 'core/COOL';
import WebService from 'logpad/classes/WebService';
import CurrentSubject from 'core/classes/CurrentSubject';

import * as helpers from 'test/helpers/SpecHelpers';

import 'logpad/resources/Rules';

let logger = new Logger('SyncUserData.spec');

describe('Synchronizing User Data:', () => {
    let data = {},
        webService = COOL.new('WebService', WebService),
        userId = 10,
        getUserId = () => {
            userId++;
            return userId;
        };

    let addNewUser = (newUser, responseText) => {
        responseText = `"${responseText || getUserId()}"`;

        let defaultDashboards = [{
            SU: 'AddNewUser',
            battery_level: null,
            change_phase: 'false',
            completed: '2015-08-27T17:26:39Z',
            completed_tz_offset: -4,
            core_version: '1.10.0',
            device_id: 'f8a6409b-5e4f-4fd5-b3da-4f4896171dd6',
            diary_id: 1440696399454454,
            id: 1,
            ink: '',
            instance_ordinal: 1,
            phase: 30,
            phaseStartDateTZOffset: '2015-05-13T13:16:01-04:00',
            questionnaire_id: 'AddNewUser_Diary',
            report_date: '2015-08-27',
            sig_id: 'LA.14f7031e9ccnull',
            started: '2015-08-27T17:26:03Z',
            study_version: '00.01',
            subject_id: '18080085'
        }];

        let defaultAnswers = [{
            SW_Alias: 'ADDNEWUSER.0.ADDNEW_IT',
            id: 100,
            instance_ordinal: 1,
            question_id: 'ADD_NEW_USER',
            questionnaire_id: 'AddNewUser_Diary',
            response: JSON.stringify(newUser.toJSON()),
            subject_id: '18080085'
        }, {
            SW_Alias: 'ADDNEWUSER.0.ADDNEW_IT',
            id: 101,
            instance_ordinal: 1,
            question_id: 'ADD_USER_ROLE',
            questionnaire_id: 'AddNewUser_Diary',
            response: JSON.stringify({ role: newUser.get('role') }),
            subject: '18080085'
        }, {
            SW_Alias: 'ADDNEWUSER.0.ADDNEW_IT',
            id: 102,
            instance_ordinal: 1,
            question_id: 'ADD_USERNAME',
            questionnaire_id: 'AddNewUser_Diary',
            response: JSON.stringify({ username: newUser.get('username') }),
            subject: '18080085'
        }];

        let answers = new Answers(defaultAnswers);
        let dashboards = new Dashboards(defaultDashboards);

        jasmine.Ajax.install();
        jasmine.Ajax.stubRequest(data.defaultFakeAjax.url).andReturn({ responseText });

        return Q.all([answers.save(), dashboards.save()])
        .then(() => {
            const dash = dashboards.at(0);

            transmissions.add({
                created: Date.now(),
                method: 'transmitUserData',
                id: 1,
                params: JSON.stringify({
                    userId: newUser.get('id')
                })
            });
            return transmissions.execute(0)
            .then(() => {
                data.newUsers.push(newUser);
            })
            .catch(() => {
                // swallow the error. Normally this is being handled in executeAll
            });
        })
        .then(() => {
            helpers.uninstallAjax();
        });
    };

    let transmissions = new Transmissions();
    let defaultUser = {
        id: 100,
        username: 'new-user-1',
        userType: 'test',
        language: 'en-US',
        password: 'apple',
        salt: 'pie',
        role: 'caregiver',
        active: 1
    };

    helpers.clearCollections(Transmissions, Dashboards, Subjects, Answers, Users);

    LF.router = new Router({
        controllers: {
            registration: new RegistrationController(),
            application: new ApplicationController(),
            settings: new SettingsController()
        }
    });

    /** @param {StorageBase<T>[]} collections */
    let clear = (...collections) => {
        return Q.all(collections.map((c) => {
            return eCoaDB[new c().storage].clear();
        }));
    };

    UserSync.addSyncFunctions({ broken: () => Q(false) });

    afterAll(() => {
        MessageRepo.clear();
        helpers.uninstallAjax();
    });

    beforeAll(() => {
        setupCoreMessages();
        LF.appName = 'LogPad App';
        data.studyDesign = window.LF.StudyDesign;
        lStorage.setItem('serviceBase', data.studyDesign.environments.at(0).get('url'));
        lStorage.setItem('site', JSON.stringify({ sitecode: '267', krdom: 'DOM.85605.178' }));
        lStorage.setItem('admin', JSON.stringify({
            role: 'admin',
            hash: '0xee9dab80da2e4165abb19324df1b29a1',
            key: 'hello'
        }));

        data.defaultFakeAjax = {
            url: `${lStorage.getItem('serviceBase')}/api/v1/SWAPI/GetDataClin`,
            type: 'POST',
            success: {
                data: '100',
                status: 'success',
                xhr: {
                    getResponseHeader: $.noop,
                    status: 200
                }
            }
        };

        data.studyDesign.assign([{
            id: 'subject',
            theme: 'a',
            displayName: 'SUBJECT_ROLE',
            defaultAffidavit: 'DEFAULT',
            unlockCodeConfig: {
                seedCode: 1111,
                codeLength: 6
            }
        }, {
            id: 'admin',
            theme: 's',
            displayName: 'ADMIN_ROLE'
        }, {
            id: 'siteUser',
            theme: 's',
            displayName: 'SITE_ROLE',
            syncLevel: 'site'
        }, {
            id: 'caregiver',
            theme: 'a',
            displayName: 'CAREGIVER_ROLE',
            syncLevel: 'subject',
            offlineSync: true
        }, {
            id: 'studyUser',
            theme: 's',
            displayName: 'STUDY_ROLE',
            syncLevel: 'study',
            offlineSync: true
        }], 'roles');
    });

    Async.beforeEach(() => {
        return Q()
        .then(() => {
            LF.security = new PatientSession();

            localStorage.setItem('krpt', 'krpt.39ed120a0981231');
            helpers.createSubject();
            return helpers.saveSubject();
        })
        .then((subjects) => {
            data.subjectsLength = subjects.length;
        })
        .then(() => helpers.initializeContent());
    });

    describe('Adding new users to the database', () => {
        beforeEach(() => {
            /** @type Array<User> */
            data.newUsers = [];
        });

        Async.afterEach(() => {
            helpers.uninstallAjax();
            return Q.all(data.newUsers.map(user => user.destroy()));
        });

        Async.it('should have subjects and Admin pre-populated', () => {
            return Users.fetchCollection()
            .tap((users) => {
                expect(users.length).toBe(data.subjectsLength + 1);
                expect(users.at(0).get('username')).toBe('SUBJECT_ROLE');
            });
        });

        // @TODO: Fix with TA37530.
        describe('with a user that already exists', () => {
            Async.it('should show a message that the user already exists.', () => {
                let newUser = new User(defaultUser),
                    users = new Users();

                users.add(newUser);

                data.responseText = 'D';

                spyOn(MessageRepo, 'display').and.callFake((key) => {
                    data.key = key;
                    return Q();
                });

                return newUser.save()
                .then(() => addNewUser(newUser, data.responseText))
                .tap(() => {
                    expect(data.key).toBe(MessageRepo.Dialog.USER_ALREADY_EXISTS_ERROR);
                });
            });
        });

        describe('with an error from the server', () => {
            let setup = (role) => {
                let newUser = new LF.Model.User(_.extend({}, defaultUser, { role }));

                return newUser.save()
                .then(() => data.users.fetch())
                .tap(() => {
                    expect(data.users.size()).toBe(data.initialUsersLength + 1);
                })
                .then(() => addNewUser(newUser, data.responseText))
                .tap(() => expect(transmissions.size()).toBe(0))
                .then(() => {
                    data.users.reset();
                    return data.users.fetch();
                })
                .then(() => newUser);
            };

            Async.beforeEach(() => {
                data.responseText = 'E';
                data.users = new Users();

                return data.users.fetch()
                .then(() => {
                    data.initialUsersLength = data.users.length;
                });
            });

            describe('with a user of a role with a sync level that cannot be added offline', () => {
                Async.it('should delete the transmission and not save the user', () => {
                    return setup('siteUser')
                    .tap((newUser) => {
                        expect(data.users.size()).toBe(data.initialUsersLength);
                        expect(data.users.findWhere({ username: defaultUser.username })).toBeUndefined();
                    });
                });
            });

            describe('with a user of a role with a sync level that can be added offline', () => {
                Async.it('should keep the transmission to try again later, and add the user locally', () => {
                    return setup('caregiver')
                    .then((newUser) => {
                        data.newUser = newUser;
                    })
                    .tap(() => {
                        expect(data.users.size()).toBe(data.initialUsersLength + 1);
                        expect(data.users.findWhere({ username: defaultUser.username })).toBeDefined();
                    });
                });
            });
        });

        describe('with roles that have broken or no sync levels', () => {
            let noSyncRole = new Role({ id: 'noSync' });
            let brokenSyncRole = new Role({ id: 'brokenSyncLevel', syncLevel: 'broken' });
            let setup = (role) => {
                let newUser = new User(_.extend({}, defaultUser, { role }));

                spyOn(webService, 'addUser');

                return newUser.save()
                .then(() => addNewUser(newUser))
                .then(() => {
                    expect(webService.addUser).not.toHaveBeenCalled();
                    data.users.reset();
                    return data.users.fetch();
                })
                .then(() => {
                    transmissions.reset();
                    return transmissions.fetch();
                })
                .catch(() => {
                    // swallow the error
                });
            };

            Async.beforeEach(() => {
                data.responseText = 'E';
                data.users = new Users();
                data.studyDesign.roles.add(noSyncRole);
                data.studyDesign.roles.add(brokenSyncRole);

                return data.users.fetch()
                .then(() => {
                    data.initialUsersLength = data.users.length;
                });
            });

            afterEach(() => {
                data.studyDesign.roles.remove(noSyncRole);
                data.studyDesign.roles.remove(brokenSyncRole);
            });

            describe('when the role has no sync level', () => {
                Async.beforeEach(() => {
                    return setup('noSync');
                });

                it('should save the user locally and not attempt a transmission when there is no sync level', () => {
                    let user = data.users.findWhere({
                        username: defaultUser.username,
                        role: 'noSync'
                    });

                    expect(data.users.size()).toBe(data.initialUsersLength + 1);
                    expect(user).toBeDefined();
                    expect(transmissions.size()).toBe(0);
                });
            });

            describe('when the role has a sync level that returns false', () => {
                Async.beforeEach(() => {
                    return setup('brokenSyncLevel');
                });

                it('should not send a transmission and delete the transmission when the sync level returns false', () => {
                    let user = data.users.findWhere({
                        username: defaultUser.username,
                        role: 'brokenSyncLevel'
                    });

                    expect(data.users.size()).toBe(data.initialUsersLength + 1);
                    expect(user).toBeDefined();
                    expect(transmissions.size()).toBe(0);
                });
            });
        });

        describe('Updating temporary password and secret question/answer for new users on first login', () => {
            let newCredentials = {
                password: 'password',
                salt: 'salt',
                secretQuestion: '0',
                secretAnswer: 'answer'
            };

            beforeEach(() => {
                data.newUser = new User(defaultUser);
                data.newUser.save();
                data.users ? data.users.reset() : data.users = new Users();

                data.update = (role) => {
                    let newUser = new User(_.extend({}, defaultUser, { role }));

                    jasmine.Ajax.install();

                    jasmine.Ajax.stubRequest(data.defaultFakeAjax.url).andReturn({ responseText: 'S' });

                    return newUser.save()
                    .then(() => newUser.changeCredentials(newCredentials))
                    .then(() => data.users.fetch())
                    .catch((err) => {
                        logger.error('Error saving new user', err);
                    })
                    .then(() => {
                        helpers.uninstallAjax();
                    });
                };
            });

            afterEach(() => {
                helpers.uninstallAjax();
            });

            // @TODO: circular dependency in transmitAll.js (Transmissions = {})
            xdescribe('when sending updateCredentials to the server', () => {
                Async.beforeEach(() => {
                    return data.update('caregiver');
                });

                it('should update the client database', () => {
                    let user = data.users.findWhere({
                        username: 'new-user-1',
                        role: 'caregiver'
                    });

                    expect(user.attributes).toEqual(jasmine.objectContaining(newCredentials));
                });
            });

            describe('if the role does not have a sync level', () => {
                Async.beforeEach(() => {
                    // @TODO:
                    spyOn(webService, 'updateUserCredentials');

                    return data.update('noSync');
                });

                it('should save locally and not transmit', () => {
                    let user = data.users.findWhere({
                        username: 'new-user-1',
                        role: 'noSync'
                    });

                    expect(webService.updateUserCredentials).not.toHaveBeenCalled();
                    expect(user.toJSON()).toEqual(jasmine.objectContaining(newCredentials));
                });
            });
        });

        describe('Retrieving the value of a users sync level ->', () => {
            let getSyncValue = (userData = {}) => {
                let newUser = new User(_.extend({}, defaultUser, userData));
                return UserSync.getValue(newUser.getSyncLevel());
            };

            describe('with a subject level role ->', () => {
                describe('if the subject is not found ->', () => {
                    Async.it('should callback false if the subject is not found', () => {
                        return Subjects.fetchCollection()
                        .then(subjects => subjects.destroy())
                        .then(() => Subjects.fetchCollection())
                        .then(() => {
                            CurrentSubject.clearSubject();
                            return getSyncValue();
                        })
                        .tap((syncVal) => {
                            expect(syncVal).toBe(false);
                        });
                    });
                });

                describe('if the subject is found ->', () => {
                    Async.beforeEach(() => {
                        return getSyncValue()
                        .then((syncVal) => {
                            data.syncVal = syncVal;
                        });
                    });

                    Async.it('should return subject\'s "krpt" value', () => {
                        return CurrentSubject.getSubject()
                        .tap((subject) => {
                            expect(data.syncVal).toBe(subject.get('krpt'));
                        });
                    });
                });
            });

            describe('with a site level role ->', () => {
                Async.it('should return false if localstorage and subject site code are mismatched', () => {
                    lStorage.setItem('site', JSON.stringify({ sitecode: 'wrong', krdom: 'DOM.85605.178' }));

                    return getSyncValue({ role: 'siteUser' })
                    .tap((syncVal) => {
                        expect(syncVal).toBe(false);
                    });
                });

                Async.it('should return site krdom', () => {
                    lStorage.setItem('site', JSON.stringify({ sitecode: '267', krdom: 'DOM.85605.178' }));
                    return getSyncValue({ role: 'siteUser' })
                    .tap((syncVal) => {
                        expect(syncVal).toBe('DOM.85605.178');
                    });
                });
            });

            describe('with a study level role ->', () => {
                Async.it('should return an empty string', () => {
                    return getSyncValue({ role: 'studyUser' })
                    .tap((syncVal) => {
                        expect(syncVal).toBe('');
                    });
                });
            });
        });

        // @TODO: circular dependency in syncHistoricalData.js (Transmissions = {})
        xdescribe('Retrieving users from the database', () => {
            beforeEach(() => {
                data.usersInClient = new Users();
                data.usersInDB = [
                    defaultUser,
                    _.extend({}, defaultUser, {
                        id: 200,
                        userId: 200,
                        username: 'new-user-2',
                        role: 'siteUser'
                    }),
                    _.extend({}, defaultUser, {
                        id: 300,
                        userId: 300,
                        username: 'new-user-3',
                        role: 'studyUser'
                    })
                ];

                jasmine.Ajax.install();
                jasmine.Ajax.stubRequest(data.defaultFakeAjax.url).andReturn({
                    responseText: JSON.stringify(data.usersInDB)
                });

                data.syncUsers = () => {
                    // helpers.initializeContent fakes stuff.call, so restore it
                    if (syncHistoricalData.and) {
                        syncHistoricalData.and.callThrough();
                    }

                    return data.usersInClient.syncUsers()
                    .then(() => {
                        data.usersInClient.reset();
                        return data.usersInClient.fetch();
                    })
                    .catch((err) => {
                        err && logger.error('Error syncing users', err);
                    });
                };
            });

            afterEach(() => {
                helpers.uninstallAjax();
                data.usersInDB = [];
            });

            Async.it('should update the user collection with new users.', () => {
                return data.syncUsers().tap(() => {
                    expect(data.usersInClient.findWhere({
                        username: 'new-user-1',
                        role: 'caregiver'
                    })).toBeDefined();
                    expect(data.usersInClient.findWhere({
                        username: 'new-user-2',
                        role: 'siteUser'
                    })).toBeDefined();
                    expect(data.usersInClient.findWhere({
                        username: 'new-user-3',
                        role: 'studyUser'
                    })).toBeDefined();
                });
            });

            Async.it('should not sync any users if one syncValue returns false', () => {
                lStorage.setItem('site', JSON.stringify({ sitecode: 'wrong', krdom: 'DOM.85605.178' }));

                return data.syncUsers()
                .tap(() => {
                    expect(data.usersInClient.findWhere({
                        username: 'new-user-1',
                        role: 'caregiver'
                    })).toBeUndefined();
                    expect(data.usersInClient.findWhere({
                        username: 'new-user-2',
                        role: 'siteUser'
                    })).toBeUndefined();
                    expect(data.usersInClient.findWhere({
                        username: 'new-user-3',
                        role: 'studyUser'
                    })).toBeUndefined();
                });
            });

            Async.it('should first deactivate any users from a previous syncValue', () => {
                data.usersInClient.add(_.extend({}, defaultUser, {
                    id: 101,
                    userId: 101,
                    username: 'deactivateME',
                    role: 'siteUser',
                    syncValue: 'old',
                    active: 1
                }));

                expect(data.usersInClient.findWhere({
                    username: 'deactivateME'
                })).toBeDefined();

                return data.syncUsers()
                .tap(() => {
                    expect(data.usersInClient.findWhere({
                        username: 'deactivateME'
                    }).get('active')).toBe(0);
                });
            });

            Async.it('should activate a user if a user was deactivated previously, but is now at the correct syncValue', () => {
                data.usersInClient.add(_.extend({}, defaultUser, {
                    id: 101,
                    userId: 101,
                    username: 'reactivateME',
                    role: 'siteUser',
                    syncValue: 'DOM.85605.178',
                    active: 0
                }));

                expect(data.usersInClient.findWhere({
                    username: 'reactivateME'
                })).toBeDefined();

                return data.syncUsers()
                .tap(() => {
                    expect(data.usersInClient.findWhere({
                        username: 'reactivateME'
                    }).get('active')).toBe(1);
                });
            });
        });
    });
});
