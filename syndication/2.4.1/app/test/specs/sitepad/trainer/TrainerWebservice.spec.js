import COOL from 'core/COOL';
import * as trainerWebservice from 'trainer/sitepad/classes/TrainerWebservice';
import TrainerData from 'trainer/sitepad/classes/TrainerData';

TRACE_MATRIX('US6733')
.describe('TrainerWebservice', () => {
    let originalWebservice = COOL.getClass('WebService');

    afterEach(() => {
        COOL.add('WebService', originalWebservice);
    });

    it('Should override the class in COOL library.', () => {
        let Webservice = COOL.getClass('WebService');

        // In core, Webservice is extended and replaced by StudyWebService
        // I've disabled the expectation. It's true for the running app, but false here.
        // The testing harness doesn't ensure the order of which WebService classes are extended.
        // expect(Webservice.name).toEqual('StudyWebService');

        trainerWebservice.extendCoreWebservice();

        Webservice = COOL.getClass('WebService');
        expect(Webservice.name).toEqual('SitepadTrainerWebservice');
    });

    Async.it('Should return a promise resolved with trainerData for "getSites".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('getSites'),
            result = webservice.getSites();

        expect(result).toBePromise();
        return result
        .then((sites) => {
            expect(sites).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "registerDevice".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('registerDevice'),
            result = webservice.registerDevice();

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "addUser".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('addUser'),
            result = webservice.addUser();

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "getAllSubjects".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('getAllSubjects'),
            onSuccessCalled = false,
            onSuccessParams = null,
            result = webservice.getAllSubjects({});

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "syncUserVisitsAndReports".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('syncUserVisitsAndReports'),
            onSuccessCalled = false,
            onSuccessParams = null,
            result = webservice.syncUserVisitsAndReports({});

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "syncUsers".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('syncUsers'),
            result = webservice.syncUsers();

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "updateUserCredentials".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('updateUserCredentials'),
            result = webservice.updateUserCredentials();

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "sendLogs".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('sendLogs'),
            onSuccessCalled = false,
            onSuccessParams = null,
            result = webservice.sendLogs({});

        expect(result).toBePromise();
        return result
        .catch((reason) => {
            expect(result.isRejected()).toBeTruthy();
            expect(reason).toBe('Not sending logs on trainer!');
        });
    });

    Async.it('Should return a promise resolved with trainerData for "sendSubjectAssignment".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('sendSubjectAssignment'),
            result = webservice.sendSubjectAssignment();

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "setSubjectData".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('setSubjectData'),
            onSuccessCalled = false,
            onSuccessParams = null,
            result = webservice.setSubjectData({});

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "sendDiary".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('sendDiary'),
            onSuccessCalled = false,
            onSuccessParams = null,
            result = webservice.sendDiary({}, '', true);

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "updateSubjectData".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('updateSubjectData'),
            onSuccessCalled = false,
            onSuccessParams = null,
            result = webservice.updateSubjectData('', {}, '');

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "sendEditPatient".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('sendEditPatient'),
            result = webservice.sendEditPatient();

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "getSetupCode".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('getSetupCode'),
            result = webservice.getSetupCode();

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData.res);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "getDeviceID".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('getDeviceID'),
            onSuccessCalled = false,
            onSuccessParams = null,
            result = webservice.getDeviceID({});

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });

    Async.it('Should return a promise resolved with trainerData for "updateUser".', () => {
        trainerWebservice.extendCoreWebservice();
        let webservice = COOL.getInstance('WebService'),
            trainerData = TrainerData.getData('updateUser'),
            result = webservice.updateUser();

        expect(result).toBePromise();

        return result
        .then((res) => {
            expect(res).toEqual(trainerData);
        });
    });
});
