import ELF from 'core/ELF';
import EditPatientQuestionnaireView from 'sitepad/views/EditPatientQuestionnaireView';
import Questionnaire from 'core/models/Questionnaire';
import Transmission from 'core/models/Transmission';
import Subject from 'core/models/Subject';
import Subjects from 'core/collections/Subjects';
import Answers from 'core/collections/Answers';
import Data from 'core/Data';
import Site from 'core/models/Site';
import Sites from 'core/collections/Sites';

import User from 'core/models/User';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as action from 'sitepad/actions/editPatientSave';
import * as DateTimeUtil from 'core/DateTimeUtil';
import * as lStorage from 'core/lStorage';
import { Dialog, MessageRepo } from 'core/Notify';
import 'sitepad/resources/Rules';
import setupSitepadMessages from 'sitepad/resources/Messages';

import * as specHelpers from 'test/helpers/SpecHelpers';

TRACE_MATRIX('US5886')
.describe('editPatientSave', () => {
    let view,
        removeTemplate,
        EditPatientRenderRule = {
            id: 'EditPatientQuestionnaire',
            trigger: 'QUESTIONNAIRE:Rendered/Edit_Patient',
            resolve: [{
                // This action populates answer records based on a configured model.
                action: 'populateFieldsByModel',
                data: [{
                    SW_Alias: 'PT.0.Patientid',
                    question_id: 'EDIT_PATIENT_ID',
                    questionnaire_id: 'Edit_Patient',

                    // Use the subject model scoped to the EditPatientQuestionnaireView (this.subject).
                    model: 'subject',

                    // Use the subject model's subject_id property to populate the response.
                    // e.g. this.subject.get('subject_id');
                    property: 'subject_id'
                }, {
                    SW_Alias: 'PT.0.Initials',
                    question_id: 'EDIT_PATIENT_INITIALS',
                    questionnaire_id: 'Edit_Patient',

                    // Use the subject model scoped to the EditPatientQuestionnaireView (this.subject).
                    model: 'subject',

                    // Use the subject model's initials property to populate the response.
                    // e.g. this.subject.get('initials');
                    property: 'initials'
                }, {
                    SW_Alias: 'Assignment.0.Language',
                    question_id: 'EDIT_PATIENT_LANGUAGE',
                    questionnaire_id: 'New_Patient',

                    // Use the user model scoped to the EditPatientQuestionnaireView (this.user).
                    model: 'user',

                    // Use the user model's language property to populate the response.
                    // e.g. this.user.get('language');
                    property: 'language'
                }]
            }]
        },
        deviceId = '9876543210ABCDEF',
        siteData = { siteCode: '0001', site_id: 'DOM.287206.246' },
        subjectData = {
            id: 1,
            subject_id: '0001-0172',
            site_code: siteData.siteCode,
            krpt: 'SA.1234567890',
            initials: 'AAA',
            user: 1
        },
        userData = { id: 1, username: 'batman', language: 'en-US' },
        answers = new Answers([{
            SW_Alias: 'PT.0.Patientid',
            question_id: 'EDIT_PATIENT_ID',
            questionnaire_id: 'Edit_Patient',
            response: subjectData.subject_id,
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'PT.0.Initials',
            question_id: 'EDIT_PATIENT_INITIALS',
            questionnaire_id: 'Edit_Patient',
            response: subjectData.initials,
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'Assignment.0.Language',
            question_id: 'EDIT_PATIENT_LANGUAGE',
            questionnaire_id: 'Edit_Patient',
            response: '{"language":"en-US"}',
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'PT.Initials.Comment',
            question_id: 'EDIT_PATIENT_REASON',
            questionnaire_id: 'Edit_Patient',
            response: 'NoReason',
            subject_id: subjectData.subject_id
        }]),
        changedAnswers = new Answers([{
            SW_Alias: 'PT.0.Patientid',
            question_id: 'EDIT_PATIENT_ID',
            questionnaire_id: 'Edit_Patient',
            response: '0001-0200',
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'PT.0.Initials',
            question_id: 'EDIT_PATIENT_INITIALS',
            questionnaire_id: 'Edit_Patient',
            response: 'BBB',
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'Assignment.0.Language',
            question_id: 'EDIT_PATIENT_LANGUAGE',
            questionnaire_id: 'Edit_Patient',
            response: '{"language":"en-US"}',
            subject_id: subjectData.subject_id
        }, {
            SW_Alias: 'PT.Initials.Comment',
            question_id: 'EDIT_PATIENT_REASON',
            questionnaire_id: 'Edit_Patient',
            response: 'ReasonIsRelative',
            subject_id: subjectData.subject_id
        }]);

    Async.beforeAll(() => {
        resetStudyDesign();
        setupSitepadMessages();

        LF.StudyDesign.questionnaires.add(new Questionnaire({
            id: 'Edit_Patient',
            SU: 'Edit_Patient',
            displayName: 'DISPLAY_NAME',
            className: 'EDIT_PATIENT',
            affidavit: 'DEFAULT',
            previousScreen: false,
            product: ['sitepad'],
            screens: [{
                id: 'EDIT_PATIENT_S_1',
                className: 'EDIT_PATIENT_S_1',
                questions: [{
                    id: 'EDIT_PATIENT_ID',
                    mandatory: true,
                    IG: 'PT',
                    IT: 'Patientid',
                    skipIT: '',
                    title: 'QUESTION_1_MAIN_TITLE',
                    text: ['QUESTION_1'],
                    className: 'EDIT_PATIENT_ID',
                    widget: {
                        id: 'EDIT_PATIENT_W_1',
                        type: 'PatientIDTextBox',
                        templates: {},
                        answers: [],

                        // numeric from 1 to 4 characters
                        maxLength: 4,
                        allowedKeyRegex: /[\d]/,
                        validateRegex: /[\d]+/,
                        _isUnique: true,
                        _isInRange: true,
                        validationErrors: [{
                            property: 'completed',
                            errorType: 'popup',
                            errString: 'QUESTION_1_EMPTY',
                            header: 'QUESTION_1_EMPTY_HEADER',
                            ok: 'VALIDATION_OK'
                        }, {
                            property: 'isInRange',
                            errorType: 'popup',
                            errString: 'QUESTION_1_RANGE',
                            header: 'QUESTION_1_RANGE_HEADER',
                            ok: 'VALIDATION_OK'
                        }],
                        validation: {
                            validationFunc: 'checkEditPatientID',
                            params: {
                                errorStrings: {
                                    errString: 'QUESTION_1_UNIQUE',
                                    header: 'QUESTION_1_UNIQUE_HEADER',
                                    ok: 'VALIDATION_OK'
                                }
                            }
                        }
                    }
                }, {
                    // Subject initials
                    id: 'EDIT_PATIENT_INITIALS',
                    mandatory: true,
                    IG: 'PT',
                    IT: 'Initials',
                    skipIT: '',
                    title: '',
                    text: ['QUESTION_2'],
                    className: 'EDIT_PATIENT_INITIALS',
                    widget: {
                        id: 'EDIT_PATIENT_W_2',
                        type: 'TextBox',
                        templates: {},
                        answers: [{ text: 'REASON_0', value: '0' }],

                        // Allow English keyboard characters only
                        // jscs:disable maximumLineLength
                        allowedKeyRegex: /^[a-zA-Z]*$/,
                        // jscs:enable

                        maxLength: 6,
                        validateRegex: /.{2,}/,
                        validationErrors: [{
                            property: 'completed',
                            errorType: 'popup',
                            errString: 'QUESTION_2_EMPTY',
                            header: 'QUESTION_2_EMPTY_HEADER',
                            ok: 'VALIDATION_OK'
                        }]
                    }
                }]
            }, {
                id: 'EDIT_PATIENT_S_2',
                className: 'EDIT_PATIENT_S_2',
                questions: [
                    {
                        id: 'EDIT_PATIENT_LANGUAGE',
                        mandatory: true,
                        IG: 'Assignment',
                        IT: 'Language',
                        skipIT: '',
                        title: 'QUESTION_3_TITLE',
                        text: ['QUESTION_3'],
                        className: '',
                        widget: {
                            id: 'EDIT_PATIENT_W_3',
                            type: 'LanguageSelectWidget',
                            label: 'LANGUAGE',
                            field: 'language',
                            templates: {},
                            answers: [],
                            validation: {
                                validationFunc: 'checkLanguageFieldWidget',
                                params: {}
                            }
                        }
                    }
                ]
            }, {
                id: 'EDIT_PATIENT_S_3',
                className: 'EDIT_PATIENT_S_3',
                questions: [{
                    id: 'EDIT_PATIENT_REASON',
                    mandatory: true,
                    IG: 'Edit_Patient',
                    IT: 'EDIT_PATIENT_REASON',
                    title: 'QUESTION_4_TITLE',
                    text: 'QUESTION_4',
                    className: 'EDIT_PATIENT_REASON',
                    widget: {
                        id: 'EDIT_PATIENT_REASON',
                        type: 'EditReason',
                        className: 'EDIT_PATIENT_REASON',
                        answers: [
                            { text: 'REASON_0', value: 'Correcting error by clinician' },
                            { text: 'REASON_1', value: 'Reason #1' },
                            { text: 'REASON_2', value: 'Reason #2' },
                            { text: 'REASON_3', value: 'Reason #3' }
                        ]
                    }
                }]
            }],
            branches: []
        }));
        return Q();
    });

    Async.beforeEach(() => {
        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/questionnaire.ejs');

        Data.Questionnaire = view;

        LF.security = LF.security || {};
        LF.security.activeUser = new User(userData);
        LF.security.startQuestionnaireTimeOut = $.noop;

        LF.spinner = LF.spinner || {};
        LF.spinner.hide = () => Q();

        // Ensure the DynamicText namespace exists.
        LF.DynamicText = {};

        spyOn(Subject.prototype, 'fetch').and.callFake(function () {
            this.set(subjectData);

            return Q();
        });

        spyOn(User.prototype, 'fetch').and.callFake(function () {
            this.set(userData);

            return Q();
        });

        spyOn(Sites.prototype, 'fetch').and.callFake(function () {
            this.add(siteData);

            return Q();
        });

        view = new EditPatientQuestionnaireView({
            id: 'Edit_Patient',
            showCancel: false,
            showLoginInfo: false,
            subjectId: subjectData.id,
            user: LF.security.activeUser
        });

        view.displayScreen = () => Q();
        lStorage.setItem('deviceId', deviceId);
        return view.resolve();
    });

    Async.afterEach(() => {
        removeTemplate();
        Data = {};
        return Q();
    });

    Async.afterAll(() => {
        MessageRepo.clear();
    });

    Async.it('should create answer record on render.', () => {
        ELF.rules.add(EditPatientRenderRule);

        return ELF.trigger(`QUESTIONNAIRE:Rendered/${view.id}`, {
            questionnaire: view.id
        }, view).then(() => {
            expect(view.data.answers.models[0].get('response')).toEqual(subjectData.subject_id);
            expect(view.data.answers.models[1].get('response')).toEqual(subjectData.initials);
            expect(view.data.answers.models[2].get('response')).toEqual(userData.language);
        });
    });

    Async.it('should save Transmission record in the database.', () => {
        view.data.answers = changedAnswers;
        spyOn(Transmission.prototype, 'save').and.resolve();

        return action.editPatientSave.call(view, {
            questionnaire: view.id
        })
        .then(() => {
            expect(Transmission.prototype.save).toHaveBeenCalledWith({
                method: 'transmitEditPatient',
                params: jasmine.any(String),
                created: jasmine.any(Number)
            });
        });
    });
});
