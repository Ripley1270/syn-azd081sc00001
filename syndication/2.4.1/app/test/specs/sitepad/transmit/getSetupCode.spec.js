import WebService from 'core/classes/WebService';
import Subject from 'core/models/Subject';
import getSetupCode from 'sitepad/transmit/getSetupCode';
import Logger from 'core/Logger';

describe('Transmit:getSetupCode', () => {
    let subject;

    beforeEach(() => {
        subject = new Subject({ krpt: '01ac231La' });

        spyOn(WebService.prototype, 'getSetupCode');
    });

    Async.it('should resolve (w/setupCode).', () => {
        subject.set('setupCode', 12345);

        let request = getSetupCode(subject);

        expect(request).toBePromise();

        return request.then(() => {
            expect(WebService.prototype.getSetupCode).not.toHaveBeenCalled();
        });
    });

    Async.it('should fail to get the setup code.', () => {
        spyOn(Logger.prototype, 'error');
        WebService.prototype.getSetupCode.and.resolve();

        let request = getSetupCode(subject);

        expect(request).toBePromise();

        return request.then(() => fail('getSetupCode should have been rejected.'))
        .catch(() => {
            expect(Logger.prototype.error).toHaveBeenCalledWith('Error, SetupCode is not available yet');
        });
    });

    Async.it('should resolve the setup code.', () => {
        WebService.prototype.getSetupCode.and.resolve(12345);
        spyOn(Subject.prototype, 'save').and.resolve();

        let request = getSetupCode(subject);

        expect(request).toBePromise();

        return request.then(() => {
            expect(Subject.prototype.save).toHaveBeenCalledWith({ setupCode: 12345 });
        });
    });
});
