import UserAgentParser from 'web/classes/UserAgentParser';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

const edgeUA = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246',
    chromeUA = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36',
    ffUA = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1',
    ie11UA = 'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
    ie9UA = 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)';

class UserAgentParserSuite {
    runTests () {
        describe('method:parse', () => {
            it('should correctly parse the user agent', () => {
                const edge = UserAgentParser.parse(edgeUA),
                    chrome = UserAgentParser.parse(chromeUA);

                expect(edge.browser.name).toBe('Edge');
                expect(chrome.browser.name).toBe('Chrome');
            });
        });
        describe('method:isBrowserSupported', () => {
            describe('Default browser support', () => {
                it('should accept Chrome as a supported browser', () => {
                    expect(UserAgentParser.isBrowserSupported(chromeUA)).toBe(true);
                });
                it('should accept Edge as a supported browser', () => {
                    expect(UserAgentParser.isBrowserSupported(edgeUA)).toBe(true);
                });
                it('should not accept Firefox as a supported browser', () => {
                    expect(UserAgentParser.isBrowserSupported(ffUA)).toBe(false);
                });
            });

            describe('Versioned browser support', () => {
                beforeEach(() => {
                    resetStudyDesign();
                    LF.StudyDesign.supportedBrowsers = [
                        { name: 'IE', version: '10.0' },
                        { name: 'IE', version: '11.0' }
                    ];
                });

                it('should validate IE11', () => {
                    expect(UserAgentParser.isBrowserSupported(ie11UA)).toBe(true);
                });

                it('should not validate IE9', () => {
                    expect(UserAgentParser.isBrowserSupported(ie9UA)).toBe(false);
                });
            });
        });
    }
}

// eslint-disable-next-line dot-location
TRACE_MATRIX('US7870').
describe('UserAgentParser', () => {
    const suite = new UserAgentParserSuite();
    suite.runTests();
});
