import SecretQuestionView from 'web/views/WebSecretQuestionView';
import SitePadSecretQuestionView from 'sitepad/views/SecretQuestionView';
import User from 'core/models/User';
import * as lStorage from 'core/lStorage';
import CurrentContext from 'core/CurrentContext';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import { MessageRepo } from 'core/Notify';
import ELF from 'core/ELF';

class SecretQuestionViewSuite {
    constructor () {
        Async.beforeEach(() => this.beforeEach());
    }

    beforeEach () {
        resetStudyDesign();
        CurrentContext.init();
        let user = CurrentContext().get('users').add({
            id: 1,
            language: 'fr-CA',
            role: 'subject',
            username: 'Justin Trudeau'
        });
        return user.save()
            .then(() => CurrentContext().setContextByUser(user))
            .then(() => {
                this.view = new SecretQuestionView();
            });
    }

    testNext () {
        describe('method:next', () => {
            let preventOffline = {
                id: 'preventOffline',
                trigger: [
                    'WEB:PreventOffline',
                    'QUESTIONNAIRE:Navigate'
                ],
                salience: Number.MAX_SAFE_INTEGER,

                // mock offline scenario
                evaluate: false,
                resolve: [],
                reject: [
                    { action: 'displayBanner', data: 'CONNECTION_REQUIRED' },
                    { action: 'preventAll' }
                ]
            };

            it('should trigger an ELF rule.', () => {
                spyOn(ELF, 'trigger').and.resolve();
                this.view.next();

                expect(ELF.trigger).toHaveBeenCalledWith('WEB:PreventOffline', {}, this.view);
            });

            Async.it('should display the banner.', () => {
                ELF.rules.add(preventOffline);
            MessageRepo.Banner = { CONNECTION_REQUIRED: 'CONNECTION_REQUIRED' };

                spyOn(MessageRepo, 'display').and.stub();

                return ELF.trigger('WEB:PreventOffline', {}, this.view).then(() => {
                    expect(MessageRepo.display).toHaveBeenCalledWith('CONNECTION_REQUIRED');
                });
            });
        });
    }
}

TRACE_MATRIX('DE22357')
.describe('SecretQuestionView', () => {
    const suite = new SecretQuestionViewSuite();

    suite.testNext();
});
