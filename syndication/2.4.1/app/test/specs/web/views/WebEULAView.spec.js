import WebEULAView from 'web/views/WebEULAView';
import EULAView from 'core/views/EULAView';
import State from 'web/classes/State';
import ELF from 'core/ELF';

import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import EULAViewTests from 'test/specs/core/views/EULAView.specBase';

class WebEULAViewSuite extends EULAViewTests {
    beforeEach () {
        this.view = new WebEULAView();
        this.removeTemplate = specHelpers.renderTemplateToDOM('web/templates/end-user-license.ejs');

        return super.beforeEach();
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(WebEULAViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testNextChild () {
        describe('method:next (child)', () => {
            it('should navigate to the unlockCode view.', () => {
                spyOn(EULAView.prototype, 'next').and.callThrough();
                spyOn(State, 'set').and.stub();
                spyOn(this.view, 'navigate').and.stub();

                this.view.next();

                expect(EULAView.prototype.next).toHaveBeenCalled();
                expect(State.set).toHaveBeenCalledWith(State.states.locked);
                expect(this.view.navigate).toHaveBeenCalledWith('unlock-code');
            });
        });
    }

    testBackChild () {
        describe('method:back (child)', () => {
            it('should trigger an uninstall event.', () => {
                spyOn(EULAView.prototype, 'back').and.stub();
                spyOn(ELF, 'trigger').and.resolve();

                this.view.back();

                expect(ELF.trigger).toHaveBeenCalledWith('APPLICATION:Uninstall', jasmine.any(Object), this.view);
                expect(EULAView.prototype.back).toHaveBeenCalled();
            });
        });
    }
}

describe('WebEULAView', () => {
    let suite = new WebEULAViewSuite();

    suite.executeAll({
        id: 'end-user-license-view',
        template: '#end-user-license-template',
        button: '#back',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});