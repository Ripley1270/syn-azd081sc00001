import WebFormGatewayView from 'web/views/WebFormGatewayView';

import * as lStorage from 'core/lStorage';
import Data from 'core/Data';
import ELF from 'core/ELF';
import { MessageRepo } from 'core/Notify';
import CurrentContext from 'core/CurrentContext';
import Session from 'core/classes/Session';
import ScheduleManager from 'core/classes/ScheduleManager';

import Site from 'core/models/Site';
import Visit from 'core/models/Visit';
import User from 'core/models/User';
import Subject from 'core/models/Subject';
import Questionnaire from 'core/models/Questionnaire';
import FormRow from 'sitepad/models/FormRow';

import Questionnaires from 'core/collections/Questionnaires';
import Visits from 'core/collections/Visits';
import Schedules from 'core/collections/Schedules';
import Sites from 'core/collections/Sites';
import UserReports from 'sitepad/collections/UserReports';
import UserVisits from 'sitepad/collections/UserVisits';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

describe('WebFormGatewayView', () => {
    let view,
        removeTemplate;

    Async.beforeEach(() => {
        resetStudyDesign();

        LF.appName = 'Web App';
        LF.security = new Session();
        LF.security.activeUser = new User({
            id: 1,
            role: 'admin'
        });

        CurrentContext.init();
        CurrentContext().setContextByRole('admin');

        // jscs:disable requireArrowFunctions
        // Long hand functions used in this case to retain the context of this to the spied upon objects.
        spyOn(Sites.prototype, 'fetch').and.callFake(function () {
            this.add({
                id: 1,
                siteCode: '001',
                studyName: 'DemoStudy',
                hash: '0x12345'
            });

            return Q();
        });

        spyOn(Subject.prototype, 'fetch').and.callFake(function () {
            this.set({
                id: 1,
                subject_id: '100-001',
                phase: 10,
                initials: 'SA',
                lastVisit: new Date()
            });

            return Q();
        });
        // jscs:enable requireArrowFunctions

        let visit = LF.StudyDesign.visits.at(1);
        let subject = new Subject({
            id: 1,
            subject_id: '100-001',
            phase: 10,
            initials: 'SA',
            lastVisit: new Date()
        });

        removeTemplate = specHelpers.renderTemplateToDOM('web/templates/form-gateway.ejs');
        view = new WebFormGatewayView({ subject, visit });

        spyOn(view, 'renderTable').and.stub();
        spyOn(window.localStorage, 'setItem').and.stub();
        spyOn(window.localStorage, 'getItem').and.stub();
        spyOn(window.localStorage, 'removeItem').and.stub();
        spyOn(view, 'reload').and.stub();
        spyOn(view, 'navigate').and.stub();
        spyOn(view, 'i18n').and.callFake(value => Q(value));

        return view.resolve()
        .then(() => view.render());
    });

    afterEach(() => removeTemplate());

    TRACE_MATRIX('DE21158').
    TRACE_MATRIX('DE21151').
    TRACE_MATRIX('US7850').
    describe('method:select', () => {
        it('should enable the `Start Transcription Mode` button when allowTranscriptionMode is true', () => {
            spyOn(view, 'enableButton');

            let row = new FormRow({ status: 'available', allowTranscriptionMode: true });
            view.select(row);

            expect(view.enableButton).toHaveBeenCalledWith('#start-form-transcription');
        });

        it('should not enable the `Start Transcription Mode` button when allowTranscriptionMode is false', () => {
            spyOn(view, 'enableButton');

            let row = new FormRow({ status: 'available', allowTranscriptionMode: false });
            view.select(row);

            expect(view.enableButton).not.toHaveBeenCalledWith('#start-form-transcription');
        });

        it('should not enable the `Start Transcription Mode` button when status is not available', () => {
            spyOn(view, 'enableButton');

            let row = new FormRow({ status: 'completed', allowTranscriptionMode: true });
            view.select(row);

            expect(view.enableButton).not.toHaveBeenCalledWith('#start-form-transcription');
        });
    });

    describe('method:deselect', () => {
        it('should disable the Start Transcription Mode button if the row is deselected', () => {
            spyOn(view, 'disableButton');

            let row = new FormRow({ status: 'available', allowTranscriptionMode: true });
            view.select(row);
            view.deselect(row);

            expect(view.disableButton).toHaveBeenCalledWith('#start-form-transcription');
        });
    });

    describe('method:startTranscription', () => {
        it('should call startTranscription if a row is selected and the button tapped', () => {
            spyOn(view, 'startTranscription');

            let row = new FormRow({ status: 'available', allowTranscriptionMode: true });
            view.select(row);

            $(view.$el.find('#start-form-transcription')[0]).trigger('click');

            expect(view.startTranscription).toHaveBeenCalled();
        });
    });
});
