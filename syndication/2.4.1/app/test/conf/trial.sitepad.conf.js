'use strict';

// jscs:disable disallowKeywords
var manifest = require('./manifest'),
    sharedConfig = require('./shared.conf');

module.exports = function (config) {

    sharedConfig(config);

    config.set({

        // list of files / patterns to load in the browser
        // Order matters!
        files : manifest.mergeFilesFor('thirdparty', 'core', 'helpers', '~tabletUnitTest'),

        // list of files to exclude
        exclude: manifest.mergeFilesFor('exclude'),

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'core/templates/*.ejs': ['html2js'],
            'sitepad/templates/*.ejs': ['html2js'],
            '../trial/tablet/**/*.js': ['browserify'],
            'test/helpers/*.js': ['browserify']
        },

        junitReporter: {
            outputDir: '../junit',
            outputFile: 'TEST-results-sitepad.xml',
            suite: 'SitePad',
            useBrowserName: false
        },

        coverageReporter : {
            type: 'cobertura',
            dir: '../coverage',
            file: 'sitepad-coverage.xml'
        }
    });
};
