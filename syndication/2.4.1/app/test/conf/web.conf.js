'use strict';

// jscs:disable disallowKeywords
var manifest = require('./manifest'),
    sharedConfig = require('./shared.conf');

module.exports = function (config) {
    sharedConfig(config);

    config.set({
        // list of files / patterns to load in the browser
        // Order matters!
        files : manifest.mergeFilesFor('thirdparty', 'core', 'helpers', '~web'),

        // list of files to exclude
        exclude: manifest.mergeFilesFor('exclude'),

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'core/templates/*.ejs': ['html2js'],
            'sitepad/templates/*.ejs': ['html2js'],
            'web/templates/*.ejs': ['html2js'],
            'test/specs/web/**/*.js': ['browserify'],
            'test/helpers/*.js': ['browserify'],
            'web/**/*.js': ['coverage']
        },

        junitReporter: {
            outputDir: '../junit',
            outputFile: 'TEST-results-web.xml',
            suite: 'Web',
            useBrowserName: false
        },

        coverageReporter : {
            type: 'cobertura',
            dir: '../coverage',
            file: 'web-coverage.xml'
        }
    });
};
