import Base from './Base';

/**
 * A model that defines a screen configuration.
 * @class Screen
 * @extends Base
 * @description
 * Screens are direct children of a Questionnaire view and are configured in the study design.
 * Each screen can be configured with any number of questions for display.
 * Access to each screen can be limited by role via the accessRoles property.
 * @example
 * let model = Screen({
 *     id: 'DailyScreen01',
 *     questions: [{ id: 'DailyQuestion01': mandatory: true }],
 *     className: 'daily-screen',
 *     template: 'DAILY:Screen',
 *     accessRoles: ['site']
 * });
 */
export default class Screen extends Base {
    /**
     * @property {Object} schema - The model's schema used for validation.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            id: {
                type: String,
                required: true
            },

            // A list of questions to display on the screen. e.g. ['DAILY_Q1', 'DAILY_Q2']
            questions: {
                relationship: {
                    to: 'Questions',
                    via: 'id',
                    multiplicity: '1..*'
                }
            },

            // An optional class to append to the root DOM node of the screen view. e.g. 'screen'
            className: {
                type: String,
                required: false
            },

            // If set to true, will hide the back button on the screen.
            disableBack: {
                type: Boolean,
                required: false
            },

            // An optional template override. e.g. 'DEFAULT:Screen'
            template: {
                type: String,
                required: false
            },

            // Configure the role that have access to the questionnaire. e.g. ['site', 'subject']
            accessRoles: {
                relationship: {
                    to: 'Roles',
                    multiplicity: '0..*'
                }
            }
        };
    }

    /**
     * @property {Object} defaults - Default attributes of the screen model.
     * @readonly
     */
    get defaults () {
        return {

            /**
             * @property {string} defaults.className - The default class assigned to the questionnaire's root element.
             */
            className: 'screen',

            /**
             * @property {string} defaults.template - The default template used to display the screen.
             */
            template: 'DEFAULT:Screen'

        };
    }
}

window.LF.Model.Screen = Screen;
