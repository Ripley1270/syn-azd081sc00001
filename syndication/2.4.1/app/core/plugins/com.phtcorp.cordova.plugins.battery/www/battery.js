
/**
 * This class contains information about the current battery status.
 * @constructor
 */
var exec = require('cordova/exec');

var Battery = function () {

};

/**
 * Get device info
 *
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Battery.prototype.getInfo = function(successCallback, errorCallback) {
    if (errorCallback == null) {
        errorCallback = function () {
        };
    }

    if (typeof errorCallback != "function") {
        console.log("Battery.getInfo failure: failure parameter not a function");
        return;
    }

    if (typeof successCallback != "function") {
        console.log("Battery.getInfo failure: success callback parameter must be a function");
        return;
    }
    exec(successCallback, errorCallback, "Battery", "getBatteryInfo", []);
};

var battery = new Battery();

module.exports = battery;

