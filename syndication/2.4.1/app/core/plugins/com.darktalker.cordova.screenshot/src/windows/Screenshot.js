let cordova = require('cordova'),
// ScreenShotPlugin = require('./ScreenShotPlugin'),
html2canvas = require('./html2canvas');

module.exports = {

    /**
     *  Handles the creation and save of a screenshot.
     *  @param {Function} callback
     *  @param {Array} params.  Contains: format, quality, fileName, folderName
     */
    saveScreenshot: function (success, error, params) {
        let theCanvas,
            $body = $('body');

        let format = params[0],
            quality = params[1],
            // Strip out potential reserved characters
            fileName = params[2].replace(/[<>:"/\\|?*]/g, ''),
            folderName = params[3];

        fileName = fileName + '.' + format;
        window.Promise = Q.Promise;
        let p = html2canvas($body);
        p.then(function (canvas) {
            let input,
                output,
                outputStream;

            theCanvas = canvas;

            let blob = canvas.msToBlob();

            // We need to get a handle to the directory passed into the call, depending on what Windows allows us to do
            // Windows.Storage.ApplicationData.current.localFolder
            // Windows.Storage.DownloadsFolder.createFileAsync(fileName)
            Windows.Storage.KnownFolders.picturesLibrary.createFolderAsync(folderName, Windows.Storage.CreationCollisionOption.openIfExists).then(function (folder) {
                return folder.createFileAsync(fileName, Windows.Storage.CreationCollisionOption.replaceExisting)
            })
                .then(function fileSuccess (file) {

                    return file.openAsync(Windows.Storage.FileAccessMode.readWrite);
                })
                .then(function (stream) {
                    outputStream = stream;
                    output = stream.getOutputStreamAt(0);
                    input = blob.msDetachStream();

                    return Windows.Storage.Streams.RandomAccessStream.copyAsync(input, output);
                })
                .then(function () {
                    return output.flushAsync();
                })
                /*.fail(function (e) {
                    console.log('Screenshot failed: ' + e);
                }) */
                .done(function () {
                    console.log('In done()');
                    input.close();
                    output.close();
                    outputStream.close();

                    success(true);
                });
        }, function fileError(failure) {
            console.log('fileError - ' + failure);
        });

        /* }).catch(function (e) {
            throw 'screenshot save has failed! ' + e;
        }); */
    }
};

require('cordova/exec/proxy').add('Screenshot', module.exports);
