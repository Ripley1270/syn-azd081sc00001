//
// Screenshot.h
//
// Created by Simon Madine on 29/04/2010.
// Copyright 2010 The Angry Robot Zombie Factory.
// - Converted to Cordova 1.6.1 by Josemando Sobral.
// MIT licensed
//
// Modifications to support orientation change by @ffd8
// <ERTMODS> by bjanaszek to support the screenshot tool requirements
//

#import <Cordova/CDV.h>
#import "Screenshot.h"

@implementation Screenshot

@synthesize webView;

// <ERTMOD> - Function to retrieve application documents directory
+ (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory    
                                                   inDomains:NSUserDomainMask] lastObject];
}
// </ERTMOD>

//- (void)saveScreenshot:(NSArray*)arguments withDict:(NSDictionary*)options

 - (void)saveScreenshot:(CDVInvokedUrlCommand*)command
{
	/*
		ERT Modifications:
		1) Updated function signature to accept an additional paramter for directory name
		2) Ensure directory exists
		3) Create directory and write image file to device's Documents directory, so the images can be accessed via iTunes.
	*/
	// <ERTMOD>
	NSError *error;
	NSArray *array = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *docDir = [array lastObject];

	NSString *folderName = [command.arguments objectAtIndex:3];
	NSString *filename = [command.arguments objectAtIndex:2];
	NSNumber *quality = [command.arguments objectAtIndex:1];

	NSString *path = [NSString stringWithFormat:@"%@.png",filename];
	

	NSString *dataPath = [docDir stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@", folderName]];

	if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
    	[[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Will Create folder
	}

	NSString *jpgPath = [dataPath stringByAppendingPathComponent:path];
	// </ERTMOD>

	CGRect imageRect;
	CGRect screenRect = [[UIScreen mainScreen] bounds];

	// statusBarOrientation is more reliable than UIDevice.orientation
	UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;

	if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
		// landscape check
		imageRect = CGRectMake(0, 0, CGRectGetHeight(screenRect), CGRectGetWidth(screenRect));
	} else {
		// portrait check
		imageRect = CGRectMake(0, 0, CGRectGetWidth(screenRect), CGRectGetHeight(screenRect));
	}

	// Adds support for Retina Display. Code reverts back to original if iOs 4 not detected.
	if (NULL != UIGraphicsBeginImageContextWithOptions)
		UIGraphicsBeginImageContextWithOptions(imageRect.size, NO, 0);
	else
		UIGraphicsBeginImageContext(imageRect.size);

	CGContextRef ctx = UIGraphicsGetCurrentContext();
	[[UIColor blackColor] set];
	CGContextTranslateCTM(ctx, 0, 0);
	CGContextFillRect(ctx, imageRect);

    if ([webView respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)]) {
        [webView drawViewHierarchyInRect:webView.bounds afterScreenUpdates:YES];
    } else {
        [webView.layer renderInContext:ctx];
    }

	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	// <ERTMOD> Changed image type to PNG
	NSData *imageData = UIImagePNGRepresentation(image);
	[imageData writeToFile:jpgPath atomically:NO];
	// </ERTMOD>

	UIGraphicsEndImageContext();

	CDVPluginResult* pluginResult = nil;
	NSDictionary *jsonObj = [ [NSDictionary alloc]
		initWithObjectsAndKeys :
		jpgPath, @"filePath",
		@"true", @"success",
		nil
		];

	pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:jsonObj];
	[self writeJavascript:[pluginResult toSuccessCallbackString:command.callbackId]];
}

@end
