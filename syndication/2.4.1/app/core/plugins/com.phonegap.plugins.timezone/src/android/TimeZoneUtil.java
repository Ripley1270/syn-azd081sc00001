package com.timezone;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.ArrayList;

import java.util.TimeZone;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import android.util.Log;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

// Better way to launch the app than this.
//import com.phtcorp.logpadapp.LogPad;

public final class TimeZoneUtil extends CordovaPlugin {

    private static final String ACTION_SET_TIME_ZONE = "setTimeZone";
    private static final String ACTION_GET_TIME_ZONES = "getTimeZones";
    private static final String ACTION_GET_CURRENT_TIME_ZONE = "getCurrentTimeZone";
    private static final String ACTION_RESTART_APPLICATION = "restartApplication";
    private static final String ACTION_GET_OPTIONS = "getOptions";
    private static final String ACTION_GET_AUTOMATIC_TIMEZONE = "getAutomaticTimezone";


    protected Context context;
    private static SharedPreferences options;

    private ArrayList<String> mLstAvailableTimeZones = null;

    @Override
    public void initialize (CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        context = super.cordova.getActivity().getApplicationContext();
        options = context.getSharedPreferences("LogPad", Context.MODE_PRIVATE);
    }


    /**
     * Executes the given action.
     *
     * @param action            The action to execute.
     * @param args              Potential arguments.
     * @param callbackContext   Callback context
     */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        boolean result = false;

        try {
            if (action.equals(ACTION_SET_TIME_ZONE)) {
                final String strTzId = args.getString(0);
                final TimeZone newTimeZone = TimeZone.getTimeZone( strTzId );
                //Using AlarmManager setTimeZone to set the time zone for the OS
                AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                am.setTimeZone(newTimeZone.getID());
                options.edit().putBoolean("TIMEZONE_SET", true).commit();
                callbackContext.success();
                result = true;
            } else if (action.equals(ACTION_GET_CURRENT_TIME_ZONE)) {
                final long time = Long.parseLong(args.getString(0));
                Calendar cal = Calendar.getInstance();
                TimeZone tz = cal.getTimeZone();

                final String displayName = getTimeZone(tz, time);
                final String id = tz.getID();

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("displayName", displayName);
                    jsonObject.put("id", id);
                } catch (JSONException e) {
                    e.printStackTrace();
                    //@todo
                }

                callbackContext.success(jsonObject);
                result = true;
            } else if (action.equals(ACTION_GET_TIME_ZONES)) {
                final long time = Long.parseLong(args.getString(0));

                List<TimeZone> lstTimeZones = new ArrayList<TimeZone>();
                String[] ids = TimeZone.getAvailableIDs();
				for (String id : ids) {
					lstTimeZones.add(TimeZone.getTimeZone(id));
				}
				Collections.sort(lstTimeZones,
						new Comparator<TimeZone>() {
							public int compare(TimeZone s1, TimeZone s2) {
								return s1.getOffset(time) - s2.getOffset(time);
							}
						});

				JSONArray jsonArray = new JSONArray();

				for (TimeZone tz : lstTimeZones) {
					final String displayName = getTimeZone(tz, time);
					final String id = tz.getID();

					JSONObject jsonObject = new JSONObject();
					try {
					  jsonObject.put("displayName", displayName);
					 jsonObject.put("id", id);
                     jsonObject.put("offset", tz.getOffset(time));
					} catch (JSONException e) {
						e.printStackTrace();
						//not going to happen
					}

					jsonArray.put(jsonObject);
				}

                callbackContext.success(jsonArray);
                result = true;
            } else if (action.equals(ACTION_RESTART_APPLICATION)) {

                // TODO: Fix this we are now using a higher version of Android. We need to figure out if this command will work
                // or if there is a new Android command we need to use. We are building with API 21 right now.
                if(android.os.Build.VERSION.SDK_INT <= 19) {
                    // We should use this now for launching the application.
                    // Just added not sure if it work
                    context = super.cordova.getActivity().getApplicationContext();

                    String pkgName  = context.getPackageName();

                    Intent intent = context
                        .getPackageManager()
                        .getLaunchIntentForPackage(pkgName);

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_RECEIVER_FOREGROUND |
                        Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);


                    callbackContext.success();
                    //Intent mStartActivity = new Intent(context, LogPad.class);
                    int mPendingIntentId = 123456;
                    //context = super.cordova.getActivity().getApplicationContext();

                    //Using AlarmManager setTimeZone to set the time zone for the OS
                    AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                    PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                    am.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, mPendingIntent);
                    android.os.Process.killProcess(android.os.Process.myPid());
                } else {
                    callbackContext.success();
                    result = true;
                }
            } else if (action.equals(ACTION_GET_AUTOMATIC_TIMEZONE)) {
                int auto = android.provider.Settings.Global.getInt(context.getContentResolver(), android.provider.Settings.Global.AUTO_TIME_ZONE);

                callbackContext.success(auto);
                result = true;
            } else if (action.equals(ACTION_GET_OPTIONS)) {
				 PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, options.getBoolean("TIMEZONE_SET", false));
				 options.edit().putBoolean("TIMEZONE_SET", false).commit();
				 callbackContext.sendPluginResult(pluginResult);

				 result = true;
             } else {
                callbackContext.error("No such action defined");
            }
        } catch(Exception e) {
            callbackContext.error(e.getMessage());
            Log.e("TimezoneUtil::execute", "Unknown exception", e);
        }

        return result;
    }

        private static String getTimeZone(final TimeZone tz, final long date) {
            long hours = TimeUnit.MILLISECONDS.toHours(tz.getOffset(date));
            long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getOffset(date))
                    - TimeUnit.HOURS.toMinutes(hours);
            minutes = Math.abs(minutes);
            String result = "";

            if (hours > 0) {
                result = String.format("(GMT+%02d:%02d) %s", hours, minutes, tz.getID());
            } else if (hours < 0) {
                result = String.format("(GMT%03d:%02d) %s", hours, minutes, tz.getID());
            } else {
                result = String.format("(GMT) %s", tz.getID());
            }
            return result;
        }
}
