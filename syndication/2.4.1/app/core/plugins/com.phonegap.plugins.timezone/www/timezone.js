var exec = require('cordova/exec'),
    TimeZoneUtil = function () {};

/**
 * Get the user's current time zone
 *
 * @param {Function} onSuccess Invoked on successful get current time zone, containing a result in a callback value
 * @param {Function} onError Invoked if there is an error.
 */
TimeZoneUtil.prototype.getCurrentTimeZone = function (onSuccess, onError) {
    exec(onSuccess, onError, 'TimeZoneUtil', 'getCurrentTimeZone', [new Date().getTime()]);
};

/**
 * Get the list of system installed time zones
 *
 * @param {Function} onSuccess Invoked on successful get, containing a result in a callback value
 * @param {Function} onError Invoked if there is an error.
 */
TimeZoneUtil.prototype.getAvailableTimeZones = function (onSuccess, onError) {
    exec(onSuccess, onError, 'TimeZoneUtil', 'getTimeZones', [new Date().getTime()]);
};

/**
 * Get the list of system installed time zone IDs
 *
 * @param {Function} onSuccess Invoked on successful get, containing a result in a callback value
 * @param {Function} onError Invoked if there is an error.
 */
TimeZoneUtil.prototype.getAutomaticTimezone = function (onSuccess, onError) {
    exec(onSuccess, onError, 'TimeZoneUtil', 'getAutomaticTimezone', []);
};

/**
 * Set the OS time zone
 *
 * @param {Function} onSuccess Invoked on successful set.
 * @param {Function} onError Invoked if there is an error.
 * @param {string}   tzId Time zone ID to set.
 */
TimeZoneUtil.prototype.setTimeZone = function (onSuccess, onError, tzId) {
    exec(onSuccess, onError, 'TimeZoneUtil', 'setTimeZone', [tzId]);
};

/**
 * Get the options for timezone
 *
 * @param {Function} onSuccess Invoked on successful set.
 * @param {Function} onError Invoked if there is an error.
 */
TimeZoneUtil.prototype.getOptions = function (onSuccess, onError) {
    exec(onSuccess, onError, 'TimeZoneUtil', 'getOptions', []);
};

/**
 * Restart the application if Android version less than 5
 *
 * @param {Function} onSuccess Invoked on successful execution
 * @param {Function} onError Invoked if there is an error.
 */
TimeZoneUtil.prototype.restartApplication = function (onSuccess, onError) {
    exec(onSuccess, onError, 'TimeZoneUtil', 'restartApplication', []);
};

var timeZoneUtil = new TimeZoneUtil();
module.exports = timeZoneUtil;
