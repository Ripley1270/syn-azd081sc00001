import Logger from './Logger.js';

/**
 * @namespace ELF
 * @type {Object}
 * @description
 * The Syndication application consists of “core” software developed as a framework with extensibility provided by other application teams. The extensibility comes in a variety of flavors, one of which is called the ELF system (Event Logic Framework).
 *
 * In short, as the core system executes, it encounters situations that an external entity might want to be aware and perhaps take some actions itself and possibly influence how the core proceeds. These situations are called “triggers” and the core logic will “raise a trigger” (that is, send a notification about this event) and potentially there are some registered “receivers” (listeners) that are interested in that trigger.
 *
 * Originally, this was used as a mechanism for application teams to extend or augment pure core behavior. Now, there is a movement that more and more of core’s own logic will be built on this mechanism (that is, core itself being a receiver for various triggers).  This allows the overall system to be seen more as a set of building blocks, anyone of which is easy to replace or augment.
 *
 * While the rules (listeners) and triggers for ELF can be defined at any time (or altered or removed), they are typically loaded at the system start (first the core ones, and then the application ones which might supercede or inject). In order for ELF to do its job, there are a handful of ELF related terms:
 * - __{@link ELF.triggers Trigger}__: This is string-name of some event in the system (e.g. ‘SystemStarted’) that the core (or any code actually) wants to announce to the ELF system.  Consult the {@link ELF.triggers Trigger} section of this document for a full documented list of all triggers -- there are 100’s of triggers. Triggers are always prefixed with a namespace. e.g. APPLICATION:SystemStarted
 * - __{@link ELF.rules Rule}__: This term is better thought of as a “receiver” (or “listener”) of a trigger; that is, the code or logic that cares about a certain trigger (or set of triggers) that is being announced.  Rules, in general, are not defined by the core system but rather by the application extensions.
 * - __{@link ELF.actions Action}__: Various functions (either core or otherwise) can be declared to ELF and recorded in a table of ‘actions’ (by a name). Later, rules can identify those actions to execute.  Actions should be thought of as “convenience functions” -- functions that likely will be needed by the application team and perhaps in several places thus there is value in having a registry of such actions. Consult the {@link LF.actions Action} section of this document for a full documented list of all actions -- there are dozens of actions.
 * - __{@link ELF.expressions Expression}__: Very similar to Action, these are also registered functions that determine something of a boolean nature (e.g. isStudyOnline()) and are expected to be used by several rules to ‘evaluate’ what should be done. Again, see the {@link ELF.expressions Expression} section of this document for more details -- there are a small handful of useful/defined expressions.
 *
 * For the most part, the core code defines a set of 100 or so Triggers and executes those at the appropriate times. Those triggers are detailed in the Trigger section of this document (and similarly for Actions and Expressions). While triggers can have an string-type name to them, in newer practice they tend to have a prefix that indicates something about a more global activity (e.g. “STARTUP:DatabasesLoaded”, “STARTUP:FirstUse”) which helps find all their related neighbors more easily in the alphabetical list.
 *
 * In general, an application creator will define the Rules (the “Listeners”) for the events of their interest. To do so, they will list which triggers this rule should catch (typically it is only one, but can be more than one).  Then they will state an activity/action/expression/evaluation to execute to determine what to do next. This ‘action’ section of ELF has changed over releases (in syntax) but conceptually remains the same: Evaluate “something” which returns essentially a TRUE or FALSE response which then determines which path ELF takes next.
 *
 * That “path” is also defined and created by the application engineer in the ELF Rule.  In pre-Syndication it might have been called “ifTrue” and “ifFalse”; in Syndication it mirrors the Promise system and the terms are “resolve” (true) and “reject” (false).
 *
 * In review, an Application engineer defines a rule (a “receiver”) for an event, gives it a name, defines an action to ‘test’ what to do next, and then defines a series of activities if that result is true, and a series if false.
 */
let ELF = {},

    /**
     * Private namespace for all configured rules.
     * @type {ELF.rules.Rule[]}
     * @private
     */
    _rules = [],

    /**
     * Private namespace for all configured actions.
     * @type {{string:ELF.Action}}
     * @private
     */
    _actions = {},

    /**
     * Private namespace for all configured expressions.
     * @type {{string:ELF.Expression}}
     * @private
     */
    _expressions = {},

    /**
     * Private array of rules that are withdrawn from evaluation for a given amount of time.
     * @type {Array<Object>}
     * @private
     */
    _withdrawn = [],

    _listenCount = 0,

    logger = new Logger('ELF'),

    // Couple funcs copied in from Utilities.js, wanting to avoid dependencies
    promiseChain = (items, handle, seed = undefined) => {
        return items.reduce((chain, item) => {
            return chain.then(prevRes => handle(item, prevRes));
        }, Q(seed));
    },

    /**
     * Returns the type of the provided argument.
     * @private
     * @param {any} obj - The target of the type check.
     * @return {string}
     */
    toType = function (obj) {
        return {}.toString.call(obj).match(/\s(\w+)/)[1].toLowerCase();
    };

/**
 * @namespace ELF.rules
 * @memberOf ELF
 * @type {Object}
 * @description
 * The heart of ELF is the definition of rules (the “listeners” of triggered events). As stated earlier, ELF was originally intended to be structure-oriented (or configuration based) and thus there is still a requirement for definition structures (although now they can be augmented with application-created javascript code more easily).  Let’s look at the main components of a Rule:
 * - __id__: Some string name you give to a rule so that later you can find it and perhaps delete or replace it.
 * - __trigger__: an array of trigger names (possibly can just be a single string and not an array depending on release) that this rule will handle. Most often a rule catches one trigger, but it may catch more, hence the array-of-named-triggers.
 * - __salience__: A number indicating the importance of the rule. Rules are evaluated in order of the highest salience to lowest. e.g. A rule with a salience of 2 evaluates before a rule with a salience of 1. If two rules have the same salience, the rule registered last will execute first. Since trial rules are registered after core, they will always have precedence.
 * - __evaluate__: (Note: Pre-Syndication this was called expression and was more limited). This is a flexible item that can either be an ‘expression’ or a structure with elements (such as an ‘expression’ element within it). For the sake of this document and release, we will claim that it must be a structure and must contain the ‘expression’ attribute.  The expression attribute is much like the pre-syndication one and as described above; it can be a known ELF Expression (registered) or ELF Action or an immediate or known function (global function or something in the scope of this rule’s definition) or an immediate value such as true or false. If a function (that is, not an immediate), ELF will invoke the function and pass two parameters:  some data, and a function to be called when the the action is finished.  The ‘data’ will come from the ‘data’ attribute of the ‘evaluate’ structure, if present/specified. The callback function must be called by the action when the action is done and must be passed a true or false which tells ELF how to continue (see next).
 * - __resolve__: an array of action descriptors (see below for details). These get processed in-order presented if the expression in evaluate resulted in true.
 * - __reject__: as above, but if false.
 *
 * The action descriptor structure has the following items:
 * - __action__: This is action to invoke where an action is as described above (either a known ELF action, or an immediate function, or something in the scope of ‘this’, etc).
 * - __data__: If specified, this data is passed to the action.
 *
 * When the action is invoked by ELF, ELF passes it the ‘data’ (if defined) and passes it a ‘done’ function that must be called by the action in order for ELF to continue. When called, ELF will continue to the next action in the list.  IF the action does not call done, then no further actions will occur and essentially that trigger-chain stops (this is not the right/desired behavior and it is unclear what happens to the ELF-Trigger-invoker because that code will never resume -- this is seen as a more desperate way (currently) to change the overall flow of the system (perhaps routing the user to a different view/area).
 */
ELF.rules = {};

// as in: _.flatten(_.sortBy(ELF.rules.all(), 'trigger').map(r=>r.trigger)).forEach(x=>console.log(x))
/**
 * Fetch and return all ELF rules.
 * @memberOf ELF.rules
 * @return {Array<ELF~Rule>}
 * @example
 * let rules = ELF.rules.all();
 */
ELF.rules.all = () => {
    return _rules;
};

/**
 * Find and return a rule by ID.
 * @memberOf ELF.rules
 * @param {string} id - The ID of the rule to find.
 * @return {ELF~Rule}
 * @example
 * let rule = ELF.rules.find('BatteryLevelWarning');
 */
ELF.rules.find = (id) => {
    return _.findWhere(_rules, { id });
};

/**
 * Register a rule to the Event Logic Framework
 * @memberOf ELF.rules
 * @param {(ELF~Rule|Array<ELF~Rule>)} rule - The rule(s) to register.
 * @returns {ELF.rules}
 */
ELF.rules.add = (rule) => {
    /*
     * @param {ELF.rules.LegacyRule} legacyRule
     * @returns {ELF.rules.Rule}
     * @private
     */
    let _convertOldELFToNew = (legacyRule) => {
        if (!legacyRule.actionData) {
            // New style already
            return legacyRule;
        }

        logger.traceEnter(`convertOldELFToNew  Rule:${legacyRule.id}`);

        /**
         * @type {ELF.rules.Rule}
         */
        let newRule = {
            id: legacyRule.id,
            trigger: legacyRule.trigger,
            evaluate: legacyRule.expression,
            resolve: [],
            reject: []
        };

        delete legacyRule.id;
        delete legacyRule.trigger;
        delete legacyRule.expression;

        legacyRule.actionData.forEach((actionData) => {
            if (actionData.trueAction) {
                let item = {};
                item.action = actionData.trueAction;
                if (actionData.trueArguments) {
                    item.data = actionData.trueArguments;
                }
                newRule.resolve.push(item);
                delete actionData.trueAction;
                delete actionData.trueArguments;
            }

            if (actionData.falseAction) {
                let item = {};
                item.action = actionData.falseAction;
                if (actionData.falseArguments) {
                    item.data = actionData.falseArguments;
                }
                newRule.reject.push(item);
                delete actionData.falseAction;
                delete actionData.falseArguments;
            }

            if (Object.keys(actionData).length) {
                logger.error(`PANIC: Did not fully consume actionData of rule:${newRule.id}`, actionData);
            }
        });

        delete legacyRule.actionData;

        if (Object.keys(legacyRule).length) {
            logger.error(`PANIC: Old rule not fully consumed! ${newRule.id}`);
        }

        logger.trace('new rule:', newRule);
        logger.traceExit();
        return newRule;
    };

    // handle single rule or array of rules
    _.forEach([].concat(rule), (rule) => {
        rule = _convertOldELFToNew(rule);

        // Fill in some defaults now, and not have to worry about undefined's later
        rule = _.defaults(rule, {
            id: [].concat(rule.trigger).join(','),
            resolve: [],
            reject: [],
            evaluate: true
        });

        logger.debug(`rules add ${rule.id}`);
        logger.trace(`rule: ${JSON.stringify(rule, null, 2)}`);

        // For logging purposes, give object-like actions a useful toString()
        ['resolve', 'reject'].forEach((branch) => {
            rule[branch].forEach((action) => {
                if (toType(action) === 'object') {
                    action.toString = () => `{action:${action.action}, ...}`;
                }
            });
        });

        let exists = ELF.rules.find(rule.id);
        if (!exists) {
            _rules.push(rule);
        } else {
            logger.error(`Attempt to re-add rule ${rule.id}`);

            // throw new Error(`Rule ${rule.id}` + ' already exists.');  Don't throw! Just log err and continue, gosh.
        }
    });

    return ELF.rules;
};

/**
 * Remove a rule from the Event Logic Framework (ELF) by ID.
 * @memberOF ELF.rules
 * @param {string} id - ID of the rule to remove.
 * @returns {ELF.rules}
 * @example
 * ELF.rules.remove('BatteryLevelWarning');
 */
ELF.rules.remove = (id) => {
    logger.debug(`rules.remove: id:${id}`);

    // Stakutis updated; don't expect or allow 'id' to be an array
    // let  matches = _.isArray(id) ? id : [id];
    //    rules = _.reject(rules, (rule) => matches.indexOf(rule.id) !== -1);
    if (!ELF.rules.find(id)) {
        logger.warn(`remove: could not find rule id: ${id} to remove`);
    }
    _rules = _.reject(_rules, rule => id === rule.id);
    return ELF.rules;
};

/**
 * Clear all rules from the Event Logic Framework (ELF).
 * @memberOf ELF.rules
 * @returns {ELF.rules}
 * @example
 * ELF.rules.clear();
 */
ELF.rules.clear = () => {
    _listenCount = 0;
    _rules = [];
    return ELF.rules;
};

/**
 * Return the total count of available rules.
 * @memberOf ELF.rules
 * @returns {number}
 * @example
 * let ruleCount = ELF.rules.size();
 */
ELF.rules.size = () => _rules.length;

/*
 * @method get
 * Return the complete list of rules.
 */
ELF.rules.get = () => _rules;

/**
 * Trigger an event.
 * @memberOf ELF
 * @param {string} eventName - The name of the event to trigger.
 * @param {Object} input - Any data to pass along to expressions and actions.
 * @param {Object} context The context of this within any rule or action.
 * @returns {Q.Promise<object>} flags
 */
ELF.trigger = (eventName, input = {}, context = {}) => {
    logger.info(`Firing event ${eventName}`);
    logger.trace(`Context: ${context}; input:`, input);

    // Apply (do, activate, execute, effectuate, whatever) a rule:
    // 1. Evaluate the rule's expression.
    // 2. Based on result, choose either the 'resolve' or the 'reject' branch
    // 3. Execute any/all actions in that branch
    // 4. Return a result that is influenced by the result of every action that was run
    let applyRule = (rule) => {
        let fcall = (func, context, input = {}) => {
            return Q.Promise((resolve, reject) => {
                // // context gets passed in as a field of data param
                // params = _.extend({}, params, {source: context});

                if (func.length === 2) {
                    // old-style callback based action
                    // Run the function, passing the arguments and a callback
                    logger.trace(`[${rule.id}] Calling old-style function with callback: ${func}`);
                    let ret = func.call(context, input, res => resolve(res));
                    if (typeof ret !== 'undefined') {
                        logger.warn(`[${rule.id}] Function returned a value, which will be discarded: ${ret}`);
                    }
                } else {
                    logger.trace(`[${rule.id}] Calling new-style function`);

                    // new style Promise based action
                    // Run the function, passing the arguments
                    let res = func.call(context, input);
                    logger.trace(`[${rule.id}] Function call returned ${Q.isPromise(res) ? 'a Promise' : 'immediate result'}`);

                    // if func did not return a promise, but a direct result, wrap it in Q
                    let promise = Q.isPromise(res) ? res : Q(res);
                    promise
                    .then((res) => {
                        resolve(res);
                    })
                    .catch((e) => {
                        reject(e);
                    });
                }
            });
        };

        // Evaluate a single expression, with optional data passed into it.
        // TODO explain difference between data and input
        let evaluateExpression = (expression, data) => {
            // Compound expression may be of the form:
            // [ 'AND', expr, expr, ... ]
            // [ 'OR', expr, expr, ... ]
            // [ expr, expr, ... ] (which is treated as AND)
            // All sub-exprs may be asynchronous.
            // This implementation does not feature short-circuit evaluation;
            // nor is the execution order of exprs predictable. Exprs are
            // evaluated in parallel.
            // It is possible for each expr to itself be a compound expression,
            // which will get recursively evaluated.
            let evaluateCompoundExpression = (expressionArr) => {
                return Q.Promise((resolve) => {
                    let subExpressions = expressionArr;

                    // By default, always assume a 'AND' operator.
                    let operator = 'AND';
                    let results = [];

                    // If the first expression is either an 'AND' or 'OR' operator...
                    if (expressionArr[0] === 'AND' || expressionArr[0] === 'OR') {
                        // Split the operator out of the expression list.
                        [operator, ...subExpressions] = expressionArr;
                    }

                    // Step through and evaluate each expression.
                    let substep = (index) => {
                        let expression = subExpressions[index];

                        evaluateExpression(expression)
                        .then((res) => {
                            results.push(res);

                            // If there is another expression to evaluate...
                            if (index + 1 < subExpressions.length) {
                                // If the operator is 'AND', and the expression
                                // resolved to false, short circuit the expression as false.
                                if (operator === 'AND' && !res) {
                                    resolve(false);
                                } else {
                                    substep(index + 1);
                                }

                            // Otherwise, if there is another expression to evaluate...
                            } else {
                                let result;

                                if (operator === 'OR') {
                                    // Returns true if any values in the list are true.
                                    result = _.some(results);
                                } else {
                                    // Returns true if all the values in the list are true.
                                    result = _.every(results);
                                }

                                resolve(result);
                            }
                        });
                    };

                    substep(0);
                });
            };

            // noinspection OverlyComplexFunctionJS,FunctionTooLongJS
            return Q.Promise((resolve, reject) => {
                switch (toType(data)) {
                    case 'function':
                        logger.trace(`[${rule.id}] About to call:${data.name}`);

                        // TODO: fix the context->this thing
                        data = data.call(context, input);
                        break;
                    case 'object':
                        if (_.isObject(input)) {
                            data = _.extend(input, data);
                        }
                        break;
                    default:
                        // Nothing to do here...custom input will replace the input from the trigger...
                        break;
                }

                let good = (res) => {
                    logger.trace(`[${rule.id}] Expression function resolving to ${res}`);
                    resolve(res);
                };
                let bad = (e) => {
                    logger.trace(`[${rule.id}] Expression function rejecting`, e);
                    reject(e);
                };

                // Determine what action to take based on the data type of the expression.
                switch (toType(expression)) {
                    case 'function':
                        // If the expression is a function, invoke it...
                        logger.trace(`[${rule.id}] Evaluating function expression ${expression.name}`);
                        fcall(expression, context, data || input)
                            .then(good)
                            .catch(bad);
                        break;

                    // If the expression is a string, look for a named function in the provided context, otherwise use a defined action...
                    case 'string': {
                        logger.trace(`[${rule.id}] Looking up string expression ${expression}`);

                        // If true, the result of the expression will be reversed.
                        let invert = false;

                        // Look for a ! at the first character of the expression name.
                        if (expression.charAt(0) === '!') {
                            invert = true;

                            // Remove ! from the expression name.
                            expression = expression.substr(1, expression.length);
                        }

                        let func = context[expression] || _expressions[expression];
                        if (!_.isFunction(func)) {
                            logger.error(`[${rule.id}] Expression name '${expression}' not found to be function`);
                            reject(`[${rule.id}] Expression '${expression}' not found to be function`);
                            break;
                        }
                        logger.trace(`[${rule.id}] Got function ${func.name}; about to call it, with:`, data || input);
                        fcall(func, context, data || input)
                            .then((res) => {
                                return good(invert ? !res : res);
                            })
                            .catch(bad);
                        break;
                    }

                    // If a boolean, continue processing the rule...
                    case 'boolean':
                        logger.trace(`[${rule.id}] Resolving trivial expression ${expression}`);
                        resolve(expression);
                        break;

                    // If the expression is an object, it may have some custom input...
                    case 'object':
                        logger.trace(`[${rule.id}] Evaluating expression ${expression.expression} with input`, expression);

                        // recursion here....
                        evaluateExpression(expression.expression, expression.input)
                            .then(good)
                            .catch(bad);
                        break;

                    // If an array, we need to loop through and evaluate each expression.
                    case 'array':
                        if (expression.length) {
                            logger.trace(`[${rule.id}] Evaluating compound expression`);
                            evaluateCompoundExpression(expression)
                                .then(good)
                                .catch(bad);
                        } else {
                            logger.error(`[${rule.id}] Empty expression []; rejecting.`);
                            reject('Empty expression []');
                        }
                        break;

                    default :
                        logger.error(`[${rule.id}] Unexpected type of expression: ${toType(expression)}`);
                        reject(`[${rule.id}] Unexpected type of expression: ${toType(expression)}`);
                        break;
                }
            })
            .then((res) => {
                // Expression is supposed to evaluate to true/false, not truthy/falsy
                // If it does not, then complain, coerce to boolean, and carry on
                if (!_.isBoolean(res)) {
                    let coercedRes = !!res;
                    logger.warn(`[${rule.id}] Expression did not return boolean value; interpreting ${res} as ${coercedRes}`);
                    return coercedRes;
                }
                return res;
            })
            .finally((res) => {
                logger.trace(`[${rule.id}] Rule resolved to`, res);
            });
        };

        let executeAction = (action, input = {}, context = {}) => {
            logger.debug(`[${rule.id}] executeAction ${action}`);
            return Q.Promise((resolve, reject) => {
                if (action.action == null) {
                    logger.error('Null/undefined action');
                    reject(`[${rule.id}] Action is ${action.action}`);
                }

                let good = (res) => {
                    logger.trace(`[${rule.id}] Action ${action} resolving to ${res}`);
                    resolve(res);
                };
                let bad = (e) => {
                    logger.error(`[${rule.id}] Action ${action} rejecting with ${e}`, e);
                    reject(e);
                };

                let exec = (params) => {
                    logger.trace(`[${rule.id}] Executing ${action.action} with:`, params);

                    // If the action is a function to be run...
                    if (_(action.action).isFunction()) {
                        fcall(action.action, context, params)
                        .then(good)
                        .catch(bad);
                    } else {
                        // presuming string...
                        // find the function by name, either in context, or in actions
                        let func = context[action.action] || _actions[action.action];
                        if (func) {
                            fcall(func, context, params)
                            .then(good)
                            .catch(bad);
                        } else {
                            logger.error(`[${rule.id}] ACTION DOESN'T EXIST: ${action.action}`);
                            reject(`[${rule.id}] Action ${action.action} not defined`);
                        }
                    }
                };

                switch (toType(action.data)) {
                    case 'function':
                        action.data.call(context, input, exec);
                        break;
                    case 'object':
                        exec(_.extend(input, action.data));
                        break;

                    // PhantomJS - toType(undefined) === 'domwindow' in older versions
                    case 'domwindow':
                    case 'undefined':
                        exec(input);
                        break;
                    default:
                        exec(action.data);
                        break;
                }
            });
        };

        // Execute a list of actions.  Actions run asynchronously, but serially, in the
        // order that they appear in the list.
        // Each action has the ability to control what happens next, by returning (resolving to)
        // some flags in an object value, e.g. {stopActions: true}
        //
        // The flags, and their meanings, are:
        //
        // - preventDefault: Continue processing actions, but do not execute any core/app
        //                   actions for this rule. Core/app actions, as opposed to trial
        //                   custom actions, are sometimes implemented outside the ELF system
        //                   (i.e. hard-coded). The expected behavior is that core/app will
        //                   honor this return value and conditionally execute or not execute
        //                   such hard-coded actions based on the returned flags from ELF.trigger.
        //                   A *better* solution is for the core/app to implement ALL its actions
        //                   in the ELF system itself, and let ELF do the conditional execution.
        //
        // - stopActions:    Do not execute any more actions for this rule, *excluding* core
        //                   actions. Subsequent rules are still run.
        //
        // - stopRules:      Do not run any more rules after this one. The remaining actions
        //                   for this rule are not preempted, however.
        //
        // The flags may be combined, so that, for example, {stopActions:true, stopRules:true}
        // cancels all remaining processing for the current event.
        //
        // It is *not possible* for any action to un-set a flag that was set by an earlier action.
        // For example, once an action has raised 'stopRules', a subsequent action (in the same rule,
        // and further down than the first) cannot cancel 'stopRules' e.g. by returning {stopRules:false}
        // The way to think of it is that these flags are actually more like function calls: stopRules();
        // what's done is done.
        //
        // The effects of these flags extend only to the individual event in which they are raised.
        // The next event of the same name starts with a clean slate.
        let executeActions = actions => promiseChain(actions, (action, accumulatedResult) => {
            logger.trace(`[${rule.id}] Executing action ${action}; accumulatedResult=`, accumulatedResult);
            if (accumulatedResult.stopActions) {
                logger.trace(`[${rule.id}] Skipping action due to prior stopActions flag`);
                return Q(accumulatedResult);
            } else if (accumulatedResult.preventDefault && action.builtIn) {
                // If earlier action result has flag 'preventDefault' set, and this action
                // is marked as 'builtIn', then do execute this action.
                // flag will propagate (below), so all following built-in actions will also be skipped.
                logger.trace(`[${rule.id}] Skipping action due to preventDefault and action.builtIn`);
                return Q(accumulatedResult);
            }
            return executeAction(action, input, context)
                    .catch((e) => {
                        // a failed action stops the action chain; but not the rule chain
                        return { error: e, stopActions: true };
                    })
                    .then((thisActionResult = {}) => {
                        switch (toType(thisActionResult)) {
                            case 'boolean':
                                // Semi-backward-compatibility hack:
                                // an action that returns false is same as raising preventDefault
                                // In prior ELF, this only applied to LAST action of LAST rule,
                                // so this is not 100% equivalent.  Here, last action of any
                                // matching rule may raise preventDefault.
                                if (thisActionResult === false) {
                                    logger.trace(`[${rule.id}] Action ${action} returned false; treating as provisional preventDefault`);
                                    thisActionResult = { provisionalPreventDefault: true };
                                }
                                break;
                            case 'object':
                                break;
                            case 'undefined':
                                thisActionResult = {};
                                break;
                            default: {
                                logger.warn([
                                    `[${rule.id}] Action ${action} did not resolve to an object or undefined,`,
                                    `but rather to a ${toType(thisActionResult)}`
                                ].join(' '));
                                thisActionResult = {};
                            }
                        }

                        // the flags, once set, propagate through all subsequent actions
                        // in the list, and ultimately are returned to the rule
                        // DE19405 This should not be an extend of thisActionResult, it should contain only those exact flags
                        return _.extend({}, thisActionResult, {
                            provisionalPreventDefault: thisActionResult.provisionalPreventDefault, // NOT accumulated; last one wins
                            stopActions: thisActionResult.stopActions || accumulatedResult.stopActions,
                            stopRules: thisActionResult.stopRules || accumulatedResult.stopRules,
                            preventDefault: thisActionResult.preventDefault || accumulatedResult.preventDefault,
                            problems: thisActionResult.error ? [].concat(accumulatedResult.problems).concat(thisActionResult.error) : undefined
                        });
                    });
        }, {});

        let branch;
        return evaluateExpression(rule.evaluate, input)
            .then((res) => {
                branch = res ? 'resolve' : 'reject';
                logger.trace(`[${rule.id}] Evaluate chose the ${branch.toUpperCase()} branch`);
                return branch; // un-necessary async step here; makes code clearer though
            })
            .then((branch) => {
                let actions = rule[branch]; // may be [], but not null
                logger.trace(`[${rule.id}] About to execute ${actions.length} actions of ${rule.id}.${branch}`);
                return executeActions(actions);
            })
            .then((actionRes = {}) => {
                // The result of applying this rule is computed from the (accumulated)
                // result of executing the actions. We discard stopActions, provisionalPreventDefault (not relevant to rules)
                // and pass on the other flags.
                // DE19405 This should not be an extend of actionRes
                return _.omit(_.extend({}, actionRes, {
                    preventDefault: actionRes.preventDefault || actionRes.provisionalPreventDefault,
                    stopRules: actionRes.stopRules
                }), 'stopActions', 'provisionalPreventDefault');
            })
            .catch((e) => {
                return { error: e };
            });
    };

    // Generate a list of rules that match this event. Matching
    // compares the event name and the rules's .trigger string.
    // Match can be exact, or partial, with rule's .trigger string
    // having to be equally or less specific than the actual eventName;
    // 'KITCHEN-ORDER:Steak/filet-mignon/medium-rare'
    // will match rules which trigger on:
    // - 'KITCHEN-ORDER:Steak/filet-mignon/medium-rare', or
    // - KITCHEN-ORDER:Steak/filet-mignon', (substring match on segment boundary) or
    // - 'KITCHEN-ORDER'
    // but *not*
    // - 'KITCHEN-ORDER:Steak/filet' (substring match, but not on segment boundary), or
    // - 'KITCHEN-ORDER:Steak/filet-mignon/medium' (again, substring match, but not on segment boundary), or
    // - 'KITCHEN-ORDER:Steak/sirloin/medium-rare' (not a substring match at all)
    //
    let matchingRules = _.chain(_rules)
        .filter((rule) => {
            let matchName = (triggersOn) => {
                // Note: str.startWith not supported on IE or PhantomJS
                // and _.startsWith not available in our current version of lodash
                return triggersOn === eventName ||
                    eventName.indexOf(`${triggersOn}:`) === 0 ||
                    eventName.indexOf(`${triggersOn}/`) === 0;
            };

            if (_.isArray(rule.trigger)) {
                return _.find(rule.trigger, item => matchName(item));
            }
            return matchName(rule.trigger);
        })
        .filter((rule) => {
            // There are cases where we may want to limit the number of times an rule can be triggered within a given amount of time.
            // For example, Encryption/Decryption errors can trigger many times in a row, but we'd only want to run uninstall once.
            // We could potentially debounce the action itself where it's defined, but that'd take more work every time we need to add the functionality
            // to an action.
            if (rule.deferSubsequentEvals) {
                let withdrawn = _.findWhere(_withdrawn, { id: rule.id });

                if (withdrawn) {
                    if (withdrawn.timeout < _.now()) {
                        // If the timeout has expired, remove the rule from the withdrawn list.
                        _withdrawn = _.reject(_withdrawn, ({ id }) => id === rule.id);
                    } else {
                        // If the rule has been widthdrawn then prevent it from being evaluated.
                        return false;
                    }
                } else {
                    _withdrawn.push({
                        id: rule.id,
                        timeout: _.now() + rule.deferSubsequentEvals
                    });
                }
            }

            return true;
        })

        // Sort the matches by importance, then reverse the order.
        .sortBy(rule => -(rule.salience || 0))
        .value();

    logger.trace(`Matched ${matchingRules.length} rules: [${_.pluck(matchingRules, 'id').join(', ')}]`);

    // Build a promise chain, asynchronous, but serial, to process all the rules that
    // matched the eventName.
    let applyRules = (matchingRules) => {
        return promiseChain(matchingRules, (rule, accumulatedResult) => {
            if (accumulatedResult.stopRules) {
                // If previous rule result has flag 'stopRules' set, then do not apply this rule.
                // flag will propagate (below), so all following rules will also be skipped.
                return Q(accumulatedResult);
            }

            // If no special flags received from previous rule, then apply this rule;
            // then combine its result with the previous result. In this combined result,
            // the special flags are accumulated with OR; thus as soon as a rule sets a flag,
            // that flag propagates through all subsequent rule application.
            return applyRule(rule)
                    .catch(e => ({ error: e })) // capture and keep going
                    .then((thisRuleResult = {}) => {
                        // DE19405 This should not be an extend of thisRuleResult, it should contain only those exact flags
                        return _.extend({}, thisRuleResult, {
                            stopRules: thisRuleResult.stopRules || accumulatedResult.stopRules,
                            preventDefault: thisRuleResult.preventDefault || accumulatedResult.preventDefault,
                            problems: thisRuleResult.error ?
                                [].concat(accumulatedResult.problems, thisRuleResult.error) :
                                accumulatedResult.problems
                        });
                    });
        }, {});
    };

    return applyRules(matchingRules)
        .catch((e) => {
            logger.error(`${eventName} event failed with error`, e);
            return { problems: [e] };
        })
        .tap((res) => {
            // if (res.problems && res.problems.length === 0) {
            //    delete res.problems;
            // }
            if (res.problems || res.preventDefault || res.stopRules || res.stopActions) {
                logger.debug(`${eventName} event resolved to:`, res);
            } else {
                logger.debug(`${eventName} event resolved un-interestingly`);
            }
        })
        .then((res) => {
            // Construct the final result.
            // The final result does not use stopRules or stopActions; those
            // are ELF internal.  Also, problems key is only present if there are any.

            // DE19405 Remove this _extend and restore the commented out let
            let finalResult = _.extend({}, _.omit(res, ['stopRules', 'stopActions']));

            // let finalResult = {preventDefault: !!res.preventDefault};
            if (finalResult.problems && finalResult.problems.length) {
                finalResult.problems = res.problems.filter(e => e); // vacuum undefined values
            }
            return finalResult;
        });
};

/**
 * @memberOf ELF
 * @description
 * Register named {@link ELF~ActionFunction} to Event Logic Framework.
 * Once added, the action can be configured via a string.
 * @param {string} name - The name of the action.
 * @param {ELF~ActionFunction} action - The action to register.
 * @returns {ELF}
 * @example
 * ELF.action('logout', function (input) {
 *    return LF.session.logout()
 *    .then(() => {
 *        return { stopRules: true };
 *    });
 * });
 */
ELF.action = (name, action) => {
    _actions[name] = action;
    if (toType(action) === 'object') {
        action.toString = () => `action:${action.action}`;
    }
    return this;
};

/**
 * @memberOf ELF
 * @description
 * Register named {@link ELF~ExpressionFunction} to the Event Logic Framework.
 * @param {string} name - The name of the expression to register.
 * @param {ELF~ExpressionFunction} expression - The expression to register.
 * @returns {ELF}
 * @example
 * ELF.expression('isSitePad', () => {
 *    return Q(LF.appName === 'SitePad App');
 * });
 */
ELF.expression = (name, expression) => {
    _expressions[name] = expression;
    return this;
};

ELF.listen = (trigger, actions) => {
    ++_listenCount;
    ELF.rules.add({
        id: `#${_listenCount}:${[].concat(trigger).join(',')}`,
        trigger,
        resolve: [].concat(actions).map(a => ({ action: a }))
    });
};

window.ELF = ELF;
export default ELF;
