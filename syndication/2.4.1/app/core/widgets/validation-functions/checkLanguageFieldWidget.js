import { MessageRepo } from 'core/Notify';

/**
 * Widget Validation Function for a Confirmation Textbox Widget
 * @param {Object} answer - the answer object from the widget
 * @param {Object} params - the validation object form the widget model
 * @param {function} callback - A callback function invoked upon completion.
 */
export function checkLanguageFieldWidget (answer, params, callback) {
    let languageAnswer = JSON.parse(answer.get('response')).language;

    if (!languageAnswer) {
        MessageRepo.display(MessageRepo.Banner.USER_SELECT_LANGUAGE_VALIDATION);
        callback(false);
    } else {
        callback(true);
    }
}

LF.Widget.ValidationFunctions.checkLanguageFieldWidget = checkLanguageFieldWidget;

export default checkLanguageFieldWidget;
