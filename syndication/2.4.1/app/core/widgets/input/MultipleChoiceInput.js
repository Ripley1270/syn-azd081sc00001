const { View } = Backbone;

/**
 * Implement a multiple choice input.  This is implemented as buttons that alter a value on click (similar to pushbutton)
 * but is not a widget.
 */

class MultipleChoiceInput extends View {
    /**
     * Value of the control.  Interfaces with value in the model.
     * @type {string}
     */
    get value () {
        if (this.model) {
            return this.model.get('value');
        }
        return null;
    }
    set value (val) {
        this.model.set('value', val);
    }

    /**
     * Constructs the MultipleChoiceInput
     * @param {Object} options
     * @param {string} options.itemTemplate template for the items.
     * @param {Array} options.items array of pushbutton items.
     * @param {string} options.items[].value Value of the individual item.
     * @param {Array} options.items[].text Lookup for translated text (translated from "strings" object).
     * @param {Object} options.strings Key/value pairs of translated strings for pushbuttons.
     * @param {string} options.callerId ID of the caller, to be used by the pushbutton item group.
     */
    constructor (options) {
        super(options);

        this.parent = options.parent;

        this.model.set('itemTemplate', options.itemTemplate);
        this.model.set('items', options.items);
        this.model.set('strings', options.strings || {});
        this.model.set('callerId', options.callerId);

        this._renderPromise = null;

        this.isRendered = false;
    }

    /**
     * Show our modal control.  Do nothing if it has already been rendered.
     * @returns {Promise<{jQuery}>} promise containing this.$el for convenience, in case the caller is interested
     * in listening to ready().
     */
    show () {
        if (!this.isRendered) {
            this._renderPromise = this.render();
        } else {
            this._renderPromise = this._renderPromise.then(() => {
                return this.$el;
            });
        }
        return this._renderPromise;
    }

    /**
     * Render the control.
     * @returns {Promise<{jQuery}>} promise containing this.$el for convenience, in case the caller is interested
     * in listening to ready().
     */
    render () {
        return Q()
        .then(() => {
            $(this.parent).append(this.$el);
            this.displayItems();
        })
        .then(() => {
            this.isRendered = true;
            this.delegateEvents();
            return this.$el;
        });
    }

    /**
     * Handler for when input is hidden.  Clears UI values.
     */
    onHidden () {
        this.clearValues();
    }

    /**
     * Clear values from control.
     */
    clearValues () {
        // Clear old values
        this.$('.btn').removeClass('active');
        this.$('input').removeAttr('checked');
    }

    /**
     * Parse the display value from the actual value that gets stored.
     * Default is for them to be identical.  One can override if deemed necessary
     * For instance, a number should do some fixed precision, and an item spinner (e.g. "Sunday-Saturday")
     * should do some sort of lookup based on a numeric value.
     * @param {string|number} value The value
     * @returns {string}
     */
    itemDisplayValueFunction (value) {
        return value;
    }

    /**
     * Set the value.  Also calls out to update the UI.
     * @param {string|number} val The value
     * @returns {Promise<void>} promise resolving when value is unhidden and set.
     */
    setValue (val) {
        this.value = val;
        return this.pullValue();
    }

    /**
     * Display our pushbutton items.
     */
    displayItems () {
        let items = this.model.get('items'),
            itemTemplate = this.model.get('itemTemplate'),
            fullItemString = '',
            strings = this.model.get('strings'),
            itemTemplateFactory = LF.templates.getTemplateFromKey(itemTemplate),
            groupName = `${this.model.get('callerId')}-${this.model.get('inputIndex') || 0}`;

        _.each(items, (item) => {
            let template = itemTemplateFactory(
                {
                    value: item.value,
                    text: strings[item.text] || item.text,
                    groupName
                }
            );
            fullItemString += template;
        });

        this.$el.append(fullItemString);
    }

    /**
     * Take (pull) value from model and use it to update the UI.
     */
    pullValue () {
        let value = this.value;

        this.clearValues();

        this.$el.find('input').each(function () {
            let $this = $(this);
            if ($this.val() === value) {
                $this.attr('checked', true)
                    .closest('.btn')
                    .addClass('active');
            }
        });
    }

    /**
     * Take value from UI and push it into the model.
     */
    pushValue () {
        let that = this;
        this.$el.find('input').each(function () {
            let $this = $(this);
            if ($this.attr('checked')) {
                that.value = $this.val();
            }
        });
    }
}

export default MultipleChoiceInput;
