import BaseSpinnerInput from './BaseSpinnerInput';

/**
 * Defines options for the SpinnerInput itself.
 */
export class ValueSpinnerInputOptions {
    /**
     * Construct a SpinnerOptions class
     * @param {object} options spinner input options passed to the control.
     */
    constructor (options) {
        /**
         * Array of items, containing a numeric value and a string representation
         * @type {{value: number, text: string}[]}
          */
        this.items = options.items;
    }
}

/**
 * Extension of spinner input that accommodates the ValueSpinner (showing an array of items).
 * @param {Object} options {ValueSpinnerInputOptions} spinner input options passed to this control.
 */
export default class ValueSpinnerInput extends BaseSpinnerInput {
    constructor (options) {
        super(options);

        this.model.set('items', options.items || []);
    }

    /**
     * Set display value for the number, based on precision and whether or not we should show leading zeros.
     * @param {string|number} value  The value
     * @returns {string}
     */
    itemDisplayValueFunction (value) {
        let items = this.model.get('items');
        if (typeof value === 'number') {
            for (let i = 0; i < items.length; ++i) {
                if (items[i].value === value) {
                    return items[i].text;
                }
            }
        }
        return value;
    }

    /**
     * Set stored value for the number, based on precision and whether or not we should show leading zeros.
     * @param {string|number} value The value
     * @returns {string|number}
     */
    itemValueFunction (value) {
        return value;
    }

    /**
     * Set values of our spinner from our array
     */
    setValues () {
        // Initialize with a blank value.  Will be removed as soon as something else is selected.
        let fullItemString,
            template,
            i,
            items = this.model.get('items'),
            itemTemplate = this.model.get('itemTemplate'),
            itemTemplateFactory;

        itemTemplateFactory = LF.templates.getTemplateFromKey(itemTemplate);
        fullItemString = itemTemplateFactory({ value: '', displayValue: '&nbsp;' });

        // Add range of values if min/max/step are defined.
        for (i = 0; i < items.length; ++i) {
            template = itemTemplateFactory(
                {
                    value: this.itemValueFunction(items[i].value),
                    displayValue: this.itemDisplayValueFunction(items[i].value)
                }
            );
            fullItemString += template;
        }
        this.$itemContainer.append(fullItemString);
    }
}
