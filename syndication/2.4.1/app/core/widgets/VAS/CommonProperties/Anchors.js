/**
 * Created by mark.matthews on 8/20/2016.
 */
export const defaultPointerColor = '#00ACE6';

export default class Anchors {
    get defaultAnchorFont () {
        return {
            name: 'Courier',
            size: 15,
            color: '#000'
        };
    }

    constructor (vasModel) {
        let _swapMinMaxLocation = false,
            _translatedMinText,
            _translatedMaxText,
            _min = {
                get text () {
                    return _translatedMinText;
                },
                set text (val) {
                    _translatedMaxText = val;
                },
                value: 0
            },
            _max = {
                get text () {
                    return _translatedMaxText;
                },
                set text (val) {
                    _translatedMaxText = val;
                },
                value: 100
            },
            _font = this.defaultAnchorFont;

        Object.defineProperty(this, 'swapMinMaxLocation', {
            get: () => {
                return _swapMinMaxLocation;
            },
            set: (val) => {
                _swapMinMaxLocation = val;
            }
        });

        Object.defineProperty(this, 'min', {
            get: () => {
                return _min;
            }
        });

        Object.defineProperty(this, 'max', {
            get: () => {
                return _max;
            }
        });

        Object.defineProperty(this, 'font', {
            get: () => {
                return _font;
            }
        });

        if (vasModel !== undefined) {
            if (vasModel.has('anchors')) {
                let tmpAnchors = vasModel.get('anchors'),
                    swapMinMax = tmpAnchors.swapMinMaxLocation,
                    anchorFont = tmpAnchors.font,
                    minAnchor = tmpAnchors.min,
                    maxAnchor = tmpAnchors.max;

                if (swapMinMax !== undefined) {
                    _swapMinMaxLocation = swapMinMax;
                }

                if (anchorFont !== undefined) {
                    _font.name = anchorFont.name === undefined ?
                        _font.name : anchorFont.name;

                    _font.size = anchorFont.size === undefined ?
                        _font.size : anchorFont.size;

                    _font.color = anchorFont.color === undefined ?
                        _font.color : anchorFont.color;
                }

                _min = minAnchor === undefined ? _min : minAnchor;
                _max = maxAnchor === undefined ? _max : maxAnchor;

                _min.text = _min.text === undefined ? 'Undefined' : _min.text;
                _min.value = _min.value === undefined ? 0 : _min.value;
                _max.text = _max.text === undefined ? 'Undefined' : _max.text;
                _max.value = _max.value === undefined ? 100 : _max.value;
            }
        }
    }
}
