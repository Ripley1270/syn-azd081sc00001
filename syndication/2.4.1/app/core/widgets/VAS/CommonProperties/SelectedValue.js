/**
 * Created by mark.matthews on 8/21/2016.
 */
export default class SelectedValue {
    static get defaultSelectedAnswerFont () {
        return {
            name: 'Arial',
            size: 14,
            color: '#fff'
        };
    }

    constructor (vasModel) {
        let _isVisible = false,
            _location = 'static',
            _font = SelectedValue.defaultSelectedAnswerFont,
            _selectionBox = {
                isVisible: false,
                borderWidth: 1,
                borderColor: '#000',
                fillColor: '#000'
            };

        Object.defineProperty(this, 'isVisible', {
            get: () => {
                return _isVisible;
            },
            set: (val) => {
                _isVisible = val;
            }
        });

        Object.defineProperty(this, 'location', {
            get: () => {
                return _location;
            },
            set: (val) => {
                _location = val;
            }
        });

        Object.defineProperty(this, 'font', {
            get: () => {
                return _font;
            },
            set: (val) => {
                _font = val;
            }
        });

        Object.defineProperty(this, 'selectionBox', {
            get: () => {
                return _selectionBox;
            },
            set: (val) => {
                _selectionBox = val;
            }
        });

        // attempts to load the display settings for the selected answer
        if (vasModel.has('selectedValue')) {
            let tmpSelectedValue = vasModel.get('selectedValue'),
                valueDisplayIsVisible = tmpSelectedValue.isVisible,
                valueDisplayLocation = tmpSelectedValue.location,
                valueDisplayFont = tmpSelectedValue.font,
                valueSelectionBox = tmpSelectedValue.selectionBox;

            _isVisible = valueDisplayIsVisible === undefined ? _isVisible : valueDisplayIsVisible;
            _location = valueDisplayLocation === undefined ? _location : valueDisplayLocation;

            if (valueDisplayFont !== undefined) {
                _font.name = valueDisplayFont.name === undefined ?
                    _font.name : valueDisplayFont.name;

                _font.size = valueDisplayFont.size === undefined ?
                    _font.size : valueDisplayFont.size;

                _font.color = valueDisplayFont.color === undefined ?
                    _font.color : valueDisplayFont.color;
            }

            if (valueSelectionBox !== undefined) {
                _selectionBox.borderWidth = valueSelectionBox.borderWidth === undefined ?
                    _selectionBox.borderWidth : valueSelectionBox.borderWidth;

                _selectionBox.borderColor = valueSelectionBox.borderColor === undefined ?
                    _selectionBox.borderColor : valueSelectionBox.borderColor;

                _selectionBox.fillColor = valueSelectionBox.fillColor === undefined ?
                    _selectionBox.fillColor : valueSelectionBox.fillColor;
            }
        }
    }
}
