import SelectWidgetBase from './SelectWidgetBase';
import Logger from 'core/Logger';
import Sites from 'core/collections/Sites';
import * as utils from 'core/utilities';
const logger = new Logger('LanguageSelectWidget');
const INPUT = 'DEFAULT:SelectWidget';

export default class LanguageSelectWidget extends SelectWidgetBase {
    get input () {
        return INPUT;
    }

    /**
     * Get the languages to display and save them to the "items" attribute in the model.
     * @returns {Q.Promise}
     */
    setItems () {
        // @todo change languages to be only those configured for a role. Part of US6150

        let languages = LF.strings.getLanguages();

        // filter the languages first. Depending on study definition, may use all or just a subset.
        return this.filterLanguagesByLocale(languages)
        .then((languages) => {
            let addItemPromiseFactories = languages.map((resource) => {
                resource.localized = `${resource.language}-${resource.locale}`.toLocaleUpperCase();
                return LF.strings.display.call(LF.strings, resource.localized);
            });

            return Q.all(addItemPromiseFactories)
            .then((localizedLangKeys) => {
                let localizedResources = localizedLangKeys.map((localizedLangKey, index) => {
                    languages[index].localized = localizedLangKey;
                    return languages[index];
                });

                this.model.set('items', localizedResources);
            })
            .catch((err) => {
                logger.error('Error in localizing language resource keys', err);
            });
        });
    }

    /**
     * Extension of SelectWidgetBase for a particular instance of a Select Widget control.
     * @param {string} divToAppendTo the select ID that the options should be appended to
     */
    renderOptions (divToAppendTo) {
        let template = _.template('<option id="{{ language }}-{{ locale }}" value="{{ language }}-{{ locale }}">{{ localized }}</option>');

        this.model.get('items').forEach((localizedResource) => {
            this.$(divToAppendTo).append(template(localizedResource));
        });
    }

    /**
     * @param {string} roleID the id of the role that has been set
     */
    refreshLanguages (roleID) {
        // eslint-disable-next-line
        let role = _.find(LF.StudyDesign.roles.models, (role)=> role.id === roleID);

        // with implementation of US6150 this will need to call renderOptions('#' + this.model.id) and will need to remove the answer.
    }

    /**
     * Filters an array of language objects.  Checks the countryCode for the Site, and compares to the study-design siteLanguageList.
     * If the site locale is located in the study-defined list, it will return all core languages  present in the defined list.
     * If the site locale is not found, all languages are returned as default.
     * @param  {Object} languages -  String array of language objects. ex.: LF.strings.getLanguages().
     * @returns {Q.Promise<Language[]>}
     */
    filterLanguagesByLocale (languages) {
        // This is a workaround for an issue this functionality created on the LogPad App.
        // This widget needs to be extended in sitepad/widgets.
        if (utils.isSitePad() || utils.isWeb()) {
            return Sites.fetchFirstEntry().then((site) => {
                let siteCode = site && site.get('countryCode');
                let studyLanguages = languages;
                if (LF.StudyDesign.sitelanguagelist && (Object.keys(LF.StudyDesign.sitelanguagelist).length > 0)) {
                    let filteredLanguageArray = [];

                    // get a set/array of all the locales int the site language list
                    // get default list if there is one.
                    let locales = Object.keys(LF.StudyDesign.sitelanguagelist);
                    let defaultList = LF.StudyDesign.sitelanguagelist.default;

                    // If countryCode matches a locales key filter the languages for inclusion. If NOT found in the study sitelanguage list,
                    // do NOT filter and return all languages. Study language set is ultimate source of truth.
                    if (siteCode && locales.indexOf(siteCode) !== -1) {
                        let sitelanguageset = LF.StudyDesign.sitelanguagelist[siteCode];
                        filteredLanguageArray = studyLanguages.filter((languageObj) => {
                            return sitelanguageset.indexOf(`${languageObj.language}-${languageObj.locale}`) !== -1;
                        });
                        studyLanguages = filteredLanguageArray;
                    } else if (defaultList) {
                        filteredLanguageArray = studyLanguages.filter((languageObj) => {
                            return defaultList.indexOf(`${languageObj.language}-${languageObj.locale}`) !== -1;
                        });
                        studyLanguages = filteredLanguageArray;
                    }
                }
                return studyLanguages;
            });
        }

        return Q(languages);
    }
}

window.LF.Widget.LanguageSelectWidget = LanguageSelectWidget;
