import Input from './input';
import Logger from 'core/Logger';

const logger = new Logger('ModalLauncherMixin'),
    defaultOkButtonText = 'MODAL_OK_TEXT';

let { Model } = Backbone;

/**
 * Mixin for allowing a view/object to launch a modal dialog.
 * Assumes that the superclass is a backbone View.
 * @param {Backbone.View} superclass super class to be extended with this mixin.
 * @returns {Object} resulting class, superclass extended with this mixin.
 */
function ModalLauncherMixin (superclass) {
    return class extends superclass {
        /**
         * default modal template
         * @type {string}
         * @abstract
         */
        get defaultModalTemplate () {
            throw new Error('Unimplemented defaultModalTemplate getter in a ModalLauncher implementation.');
        }

        /**
         * Default text lookup for OK button.
         * @type {string}
         */
        get defaultOkButtonText () {
            return defaultOkButtonText;
        }

        /**
         * Getter for modal jQuery selector
         * @type {jQuery}
         */
        get $modal () {
            return this._$modal || null;
        }

        /**
         * Setter for modal jQuery selector
         * @param {jQuery} $value value to be set
         * @type {jQuery}
         */
        set $modal ($value) {
            this._$modal = $value;
        }

        /**
         * Method to parse a value into an array for the individual inputs values.
         * i.e. splitting a string, date/time values, etc.
         * @abstract
         */
        getInputValuesArray () {
            throw new Error('Must define getInputValuesArray() in ModalLauncherMixin implementation');
        }

        /**
         * Method to convert the form elements/widgets in the modal dialog
         *      into a string.
         * @abstract
         */
        getModalValuesString () {
            throw new Error('Unimplemented getModalValuesString() method in a ModalLauncher implementation');
        }

        /**
         * Overridden to have handler for modal dialog.  Doesn't work as a regular event
         * (in the events object) so create and destroy manually
         */
        delegateEvents () {
            super.delegateEvents();
            this.addCustomEvents();
        }

        /**
         * Overridden to also cancel custom events.
         */
        undelegateEvents () {
            super.undelegateEvents();
            this.removeCustomEvents();
        }

        /**
         * Add custom events that could not be set the Backbone way
         */
        addCustomEvents () {
            if (this.$modal) {
                this.dialogClosing = _.bind(this.dialogClosing, this);
                this.dialogHidden = _.bind(this.dialogHidden, this);
                this.destroy = _.bind(this.destroy, this);
                this.$modal.on('click', '[data-dismiss]', this.dialogClosing);
                this.$modal.on('hidden.bs.modal', this.dialogHidden);
            }
        }

        /**
         * Remove custom events that could not be set the Backbone way
         */
        removeCustomEvents () {
            if (this.$modal) {
                this.$modal.off('click', '[data-dismiss]', this.dialogClosing);
                this.$modal.off('hidden.bs.modal', this.dialogHidden);
            }
        }

        /**
         * Render the modal dialog.
         * @param {Object} textStrings Object of text strings to translate.
         * @returns {Q.Promise<jQuery>} promise resolving when modal has been rendered.
         */
        renderModal (textStrings) {
            let modalTemplate = (this.model.get('templates') || {}).modal || this.defaultModalTemplate,

                // Default common strings for modal templates to blank
                strings = _.defaults(textStrings,
                    {
                        modalTitle: '&nbsp;',
                        okButtonText: '&nbsp;',
                        labelOne: '&nbsp;',
                        labelTwo: '&nbsp;'
                    });

            // Fill out blanks for labels that are defaulted
            _.each(this.model.get('labels') || this.defaultLabels, (value, key) => {
                if (value === '') {
                    strings[key] = '&nbsp;';
                }
            });

            this.inputs = [];

            return Q()
            .then(() => {
                this.$modal = $(this.renderTemplate(modalTemplate, strings));
            })
            .then(() => {
                return this.injectModalInputs(strings);
            })
            .then(() => {
                this.getModalContainer().append(this.$modal.get(0));
                return this.$modal;
            });
        }

        /**
         * Get the modal container
         * @returns {jQuery} jQuery collection containing the first modal container element, or <body> if there is none.
         */
        getModalContainer () {
            let ret = $('#modalContainer').first();
            if (ret.length === 0) {
                ret = $('body');
            }
            return ret;
        }

        /**
         * Method to inject modal inputs into the modal dialog.
         * @param {Object} strings key/value pairs of translated strings.
         */
        injectModalInputs (strings) {
            // Ensure inputOptions is an array
            let inputOptions = _.compact(_.flatten([this.model.get('inputOptions')]));

            // jscs:disable requireArrowFunctions
            this.$modal.find('.modal-input-container').each((i, elem) => {
                this.inputs[i] = new Input[inputOptions[i].type](
                    _.extend({
                        model: new Model(),
                        parent: elem,
                        callerId: this.model.get('id'),
                        inputIndex: i,
                        strings,

                        // Do not include a blank value for controls (e.g. spinners) that can have one.
                        // Generic version of launcher doesn't handle or hide them.
                        includeBlankValue: false
                    }, inputOptions[i] || {})
                );
            });
        }

        /**
         * Refresh the spinners' UI and set them to our current values (from parsing the textbox).
         * @returns {Q.Promise<void>} promise resolving when all spinners are refreshed with values set.
         */
        refreshInputs () {
            let valArray = this.getInputValuesArray(),
                setValuePromiseFactories = [];

            for (let i = 0; i < this.inputs.length; ++i) {
                setValuePromiseFactories.push(() => {
                    return this.inputs[i].show()
                    .then(() => {
                        logger.trace(`refreshInputs() -> Setting value to ${valArray[i]}`);
                        return this.inputs[i].setValue(valArray[i]);
                    })
                    .then(() => {
                        this.inputs[i].pushValue();
                    });
                });
            }

            // Order may matter here.  Set them in order just in case it does.
            return setValuePromiseFactories.reduce(Q.when, Q());
        }

        /**
         * @abstract
         * @param {Event} e The event args
         * Overrideable hook for after the dialog is fully opened.
         * @returns {Q.Promise<void>}
         */
        dialogOpened (e) {
            let ret = Q()
                .then(() => {
                    return this.refreshInputs();
                });

            // If called directly from an event handler, close the promise,
            // and return undefined (value of ret.done())
            //  Otherwise return the promise to be handled by the caller.
            return e instanceof $.Event ? ret.done() : ret;
        }

        /**
         * Handler for when the dialog is hidden.  This can be because of an OK or a cancel.
         * Calls out to the onHidden handler for each input class.
         */
        dialogHidden () {
            _.each(this.inputs, (input) => {
                _.bind(input.onHidden || $.noop, input)();
            });
        }

        /**
         * Get object containing strings that should be translated for the modal dialog.
         * By default gets an OK button... and also any text coming from any modal inputs "items" array.
         */
        getModalTranslationsObject () {
            let toTranslate = {
                okButtonText: this.model.get('okButtonText') || this.defaultOkButtonText
            };

            this.model.get('modalTitle') && (toTranslate.modalTitle = this.model.get('modalTitle'));

            _.each(_.compact(_.flatten([this.model.get('inputOptions')])), (input) => {
                _.each(input.items || [], (item) => {
                    toTranslate[item.text] = item.text;
                });
            });

            _.each(this.model.get('labels') || this.defaultLabels, (value, key) => {
                if (value !== '') {
                    toTranslate[key] = value;
                }
            });

            return toTranslate;
        }

        /**
         * Launch the dialog.
         * @param {Event} e The event args
         * @returns {Q.Promise<void>} undefined if called from an event handler.
         * Otherwise, returns a promise, resolving when the dialog is fully opened.
         * After opened, this function will launch the afterDialogOpened hook.
         */
        openDialog (e) {
            // jscs:disable requireArrowFunctions
            this.$el.find('input[type=tel], input[type=text]').each(function () {
                this.blur();
            });
            // jscs:enable requireArrowFunctions

            let ret = new Q.Promise((resolve) => {
                let isShown = () => {
                    this.$modal && this.$modal.off('shown.bs.modal', isShown);
                    resolve();
                };
                if (this.$modal) {
                    this.$modal.on('shown.bs.modal', isShown);
                    this.$modal.modal('show');
                } else {
                    resolve();
                }
            })
            .then(() => {
                return this.dialogOpened();
            });


            // If called directly from an event handler, close the promise,
            // and return undefined (value of ret.done())
            //  Otherwise return the promise to be handled by the caller.
            return e instanceof $.Event ? ret.done() : ret;
        }

        /**
         * Overrideable hook for when the dialog is closing.
         * @param {Event} e The event args
         * @returns {Q.Promise<void>} the promise if not coming from an event handler
         *      , or undefined if it is.
         */
        dialogClosing (e) {
            // If called directly from an event handler, close the promise,
            // and return undefined (value of ret.done())
            //  Otherwise return the promise to be handled by the caller.
            return e instanceof $.Event ? undefined : Q();
        }

        /**
         * Destroy and de-reference inputs
         */
        removeInputs () {
            _.each(this.inputs, (item) => {
                (item.destroy || $.noop)();
            });
            this.inputs && this.inputs.splice(0, this.inputs.length);
        }

        /**
         * Remove and de-reference modal dialog.
         */
        removeModal () {
            if (this.$modal) {
                this.$modal.modal('hide');
                this.$modal.remove();
                this.$modal = null;
            }
        }

        /**
         * Destroy the UI for this widget, and call to remove the modal.
         */
        destroy () {
            // If the parent view implements a destroy method, run it, otherwise do nothing.
            (super.destroy || $.noop)();

            this.undelegateEvents();
            this.removeInputs();
            this.removeModal();
        }

        /**
         * Note, the following method has been copied from WidgetBase
         * so that the superclass can just be a View if necessary (not a widget)
         */
        /**
         * An alias for LF.templates.display. See {@link core/collections/Templates Templates} for details.
         * @param {string} name The name of the template to render.
         * @param {object} options The options passed to the template.
         * @returns {string} The rendered template.
         */
        renderTemplate (name, options) {
            return LF.templates.display(name, options);
        }
    };
}

export default ModalLauncherMixin;
