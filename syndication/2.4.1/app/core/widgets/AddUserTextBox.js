import TextBox from './TextBox';
import { Banner } from 'core/Notify';

export default class AddUserTextBox extends TextBox {
    /**
     *  Constructor
     *  @param {Object} options The options for the widget
     */
    constructor (options) {
        super(options);

        this.validation = this.model.get('validation');
        this.isUserTextBox = true;
        this.events = {
            'keypress input[type=text]': 'keyPressed',
            'input input[type=text]': 'sanitize',
            'focus input[type=text]': 'onFocus'
        };
    }

    /**
     *  Responds to a key press event.
     *  @param {Event} e Event data
     */
    // eslint-disable-next-line consistent-return
    keyPressed (e) {
        let keyCode = e.which || e.keyCode;

        if ((keyCode === 13) || (keyCode === 10)) {
            let $notDateboxInputs = $(':input[type="text"]').not('[data-role = "datebox"]'),
                numOfInputs = $notDateboxInputs.size(),
                nextIndex = $notDateboxInputs.index(e.target) + 1;
            if (nextIndex < numOfInputs) {
                $('input').blur();
                e.preventDefault();
                return false;
            }
            this.questionnaire.nextHandler(e);

            return true;
        }
        super.keyPressed(e);
    }

    /**
     *  Handles user input on the widget.
     *  @param {Event} e Event data
     */
    respond (e) {
        // DE17147 - A value of ' ' is trimmed to a length of 0.
        // This triggered the remove answer condition below.
        // This condition was hoisted to the top of the method to ensure the answer record
        // exists prior to accessing it.
        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        let validateRegex = this.answer.get('validateRegex'),
            field = this.model.get('field'),
            value = this.$(e.target).val().trim(),
            responseString = JSON.stringify({ [field]: value }),
            comp = (typeof this.isValid === 'boolean') ? this.completed : true;

        if (typeof value !== 'string' || value.length === 0) {
            this.completed = false;
            this.removeAnswer(this.answer);
            return;
        }

        if (!(validateRegex instanceof RegExp)) {
            this.completed = comp;
        } else {
            this.completed = validateRegex.test(value) && comp;
        }
        if (this.completed) {
            this.validation.answers = this.questionnaire.answers;
        }

        this.respondHelper(this.answer, responseString, this.completed);
    }

    onFocus () {
        this.$el.find('input').removeClass('ui-state-error-border');
        Banner.closeAll();
    }

    /**
      * clears the textbox and removes the answer
      */
    clear () {
        this.completed = false;
        this.removeAnswer(this.answer);
        $(`#${this.model.get('id')}`).val('');
    }
}

AddUserTextBox.prototype.input = 'DEFAULT:TextBox';
window.LF.Widget.AddUserTextBox = AddUserTextBox;
