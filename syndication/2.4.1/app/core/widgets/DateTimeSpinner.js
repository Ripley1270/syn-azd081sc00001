import IntervalSpinnerBase from './IntervalSpinnerBase';

const DEFAULT_MODAL_TEMPLATE = 'DEFAULT:DateTimeSpinnerModal';
const DEFAULT_DISPLAY_FORMAT = 'LLL';
const DEFAULT_STORAGE_FORMAT = 'DD MMM YYYY HH:mm:ss';

/**
 * Variation of Interval Spinner for Date and Time.
 */
export default class DateTimeSpinner extends IntervalSpinnerBase {
    /**
     * default modal template
     * @returns {string}
     */
    get defaultModalTemplate () {
        return DEFAULT_MODAL_TEMPLATE;
    }

    get defaultDisplayFormat () {
        return DEFAULT_DISPLAY_FORMAT;
    }

    get defaultStorageFormat () {
        return DEFAULT_STORAGE_FORMAT;
    }

    /**
     * Verify all necessary containers exist.
     */
    validateUI () {
        if (this.yearIndex === null || this.monthIndex === null || this.dayIndex === null || this.hourIndex === null || this.minuteIndex === null) {
            throw new Error('Invalid template for DateTimeSpinner widget. Expected day + month + year + hour + minute containers');
        }
    }
}

window.LF.Widget.DateTimeSpinner = DateTimeSpinner;
