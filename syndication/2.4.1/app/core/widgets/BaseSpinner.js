import ModalWidgetBase from './ModalWidgetBase';
import Logger from 'core/Logger';

const logger = new Logger('BaseSpinner');
const DEFAULT_CLASS_NAME = 'Spinner',
    DEFAULT_LABELS = { labelOne: '', labelTwo: '' };

/**
 * BaseSpinner widget.  This is an interface for spinners.
 * It is essentially a textbox that launches a modal dialog (think "keyboard") with an
 * array of spinners.  It then calls a function to determine the value string from the array.<br /><br />
 * Note: 3 things passed in from study-design.js must be arrays in this widget.  They depend
 * on there being the same number of HTML elements in the modal template that have spinners injected into them
 * (specific implementations will specify how this is done).
 * Each index of these arrays will apply to each spinner, respectively, based on the order they occur in the template.
 * <ul>
 *     <li>spinnerInputOptions</li>
 *     <li>templates.spinnerTemplates</li>
 *     <li>templates.spinnerItemTemplates</li>
 * </ul>
 */
export default class BaseSpinner extends ModalWidgetBase {
    /**
     * default spinner template
     */
    get defaultSpinnerTemplate () {
        throw new Error('Unimplemented defaultSpinnerTemplate getter in a BaseSpinner implementation.');
    }

    /**
     * default spinner item template
     * @abstract
     */
    get defaultSpinnerItemTemplate () {
        throw new Error('Unimplemented defaultSpinnerItemTemplate getter in a BaseSpinner implementation.');
    }

    /**
     * default class name for this widget, unless overridden by model.
     * @type {string}
     */
    get defaultClassName () {
        return DEFAULT_CLASS_NAME;
    }

    /**
     * default labels for modal dialog (keys to be translated)
     * @type {Object}
     */
    get defaultLabels () {
        return DEFAULT_LABELS;
    }

    /**
     * TODO: Remove this eventually.
     * For forwards compatibility with generic modal control.
     * Uses "inputs" array behind the scenes when setting "spinners".
     */
    get spinners () {
        return this.inputs;
    }

    /**
     * TODO: Remove this eventually.
     * For forwards compatibility with generic modal widget.
     * Uses "inputs" array behind the scenes when setting "spinners".
     */
    set spinners (value) {
        this.inputs = value;
    }

    /**
     * @abstract
     * Get the array of spiner values from a string.
     * @returns {Array<Number>} the array to be passed along to our spinners.
     * @abstract
     */
    getSpinnerValuesArray () {
        throw new Error('Unimplemented getSpinnerValuesArray method in BaseSpinner implementation.');
    }

    /**
     * Override of getInputValuesArray from mixin.  Currently calls getSpinnerValuesArray()
     * for backwards compatibility.
     */
    getInputValuesArray () {
        return this.getSpinnerValuesArray();
    }

    /**
     * @abstract
     * Use this function to find all the containers that should have spinners, and add them.
     */
    injectSpinnerInputs () {
        throw new Error('Unimplemented injectSpinnerInputs() method for a BaseSpinner Implementation.');
    }

    /**
     * Override of injectModalInputs.
     * For BaseSpinner (currently) just return injectSpinnerInputs.
     */
    // TODO:  Make Modal Implementation use this method (not assume spinners) and use CSS container classname to determine input type.
    injectModalInputs () {
        return this.injectSpinnerInputs();
    }

    /**
     * Construct the spinner
     * @param {Object} options options for this widget.
     * @param {Array<Object>} options.spinnerInputOptions Array of options for each spinner in the
     * template.
     * Note: this array length should equal the number of elements with 'number-spinner-container' in the
     * modal template.
     * @param {string} options.defaultVal default value for the spinner
     * @param {Object} options.templates templates for the spinner
     * @param {string} options.templates.input template for the input (text box) for the spinner
     * @param {string} options.templates.modal template for the modal dialog for the spinner
     * @param {Array<string>} options.templates.spinnerTemplates templates for each spinner.
     * Note: this array length should equal the number of elements with 'number-spinner-container' in the
     * modal template.
     * @param {Array<string>} options.templates.spinnerItemTemplates templates for each item of each spinner.
     * Note: this array length should equal the number of elements with 'number-spinner-container' in the
     * modal template
     * @param {string} [options.modalTitle=''] title of the modal dialog.
     * @param {string} [options.okButtonText=''] text to display within the OK button dialog.
     * @param {Object} [options.labels={labelOne:'',labelTwo:''}] key/value pairs of labels.
     * You can define any key here and it will replace the placeholder of that key
     * in the template into a translated string (only for the modal template).
     */
    constructor (options) {
        super(options);

        /**
         * Spinners
         * @type {Array<BaseSpinnerInput>}
         */
        this.spinners = [];
    }

    /**
     * Refresh the spinners' UI and set them to our current values (from parsing the textbox).
     * @returns {Q.Promise<void>} promise resolving when all spinners are refreshed with values set.
     */
    refreshSpinners () {
        let valArray = this.getSpinnerValuesArray(),
            setValuePromiseFactories = [];
        for (let i = 0; i < this.spinners.length; ++i) {
            setValuePromiseFactories.push(() => {
                return this.spinners[i].show()
                .then(() => {
                    logger.trace(`refreshSpinners() -> Setting value to ${valArray[i]}`);
                    return this.spinners[i].setValue(valArray[i]);
                })
                .then(() => {
                    this.spinners[i].pushValue();
                });
            });
        }

        // Order may matter here.  Set them in order just in case it does.
        return setValuePromiseFactories.reduce(Q.when, Q());
    }

    /**
     * Get array of spinner templates from the model.  Fill with defaults where undefined
     * @param {number} index index of our current spinner
     * @returns {string} template name for the spinner at this index.
     */
    getSpinnerTemplate (index) {
        let modelTemplates = (this.model.get('templates') || {}).spinnerTemplates || [];
        return modelTemplates[index] || this.defaultSpinnerTemplate;
    }

    /**
     * Get array of spinner item templates from the model.  Fill with defaults where undefined
     * @param {number} index index of our current spinner
     * @returns {string} item template name for the spinner at this index.
     */
    getSpinnerItemTemplate (index) {
        let modelTemplates = (this.model.get('templates') || {}).spinnerItemTemplates || [];
        return modelTemplates[index] || this.defaultSpinnerItemTemplate;
    }
}

window.LF.Widget.BaseSpinner = BaseSpinner;
