// A collection of secret questions

import ArrayRef from 'core/classes/ArrayRef';

export default {
    securityQuestions: new ArrayRef([
        { text: 'SECURITY_QUESTION_0', key: 0, display: true },
        { text: 'SECURITY_QUESTION_1', key: 1, display: false },
        { text: 'SECURITY_QUESTION_2', key: 2, display: true },
        { text: 'SECURITY_QUESTION_3', key: 3, display: false },
        { text: 'SECURITY_QUESTION_4', key: 4, display: true },
        { text: 'SECURITY_QUESTION_5', key: 5, display: false },
        { text: 'SECURITY_QUESTION_6', key: 6, display: true },
        { text: 'SECURITY_QUESTION_7', key: 7, display: false },
        { text: 'SECURITY_QUESTION_8', key: 8, display: true }
    ])
};
