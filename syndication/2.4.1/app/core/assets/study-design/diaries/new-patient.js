// This is a core questionnaire required for the New Patient workflow on the SitePad App.
// Please do not remove it, but configure it as needed.
// All resource strings referenced in this questionnaire are core (namespace:CORE) resources,
// but can be overwritten at either the questionnaire (namespace:New_Patient)
// or study (namespace:STUDY) levels.
import getRoleInputType from 'core/widgets/param-functions/getRoleInputType';

export default {
    questionnaires: [{
        id: 'New_Patient',
        SU: 'New_Patient',
        displayName: 'ADD_PATIENT',
        className: 'ADD_PATIENT',

        // Use a custom affidavit designed for this questionnaire.
        // The affidavit configuration can be found below.
        affidavit: 'NewPatientAffidavit',

        // This questionnaire is only available on the SitePad App.
        product: ['sitepad'],
        screens: [
            'ADD_PATIENT_S_1',
            'ADD_PATIENT_S_2'
        ],
        branches: [],

        // Only Site Users and System Adminstrators may access this questionnaire.
        accessRoles: ['site', 'admin']
    }],

    screens: [{
        id: 'ADD_PATIENT_S_1',
        className: 'ADD_PATIENT_S_1',

        // Prevent the back button from displaying on the first screen.
        disableBack: true,
        questions: [
            { id: 'ADD_PATIENT_INFO' },
            { id: 'ADD_PATIENT_ID', mandatory: true },
            { id: 'ADD_PATIENT_INITIALS', mandatory: true },
            { id: 'ADD_PATIENT_PASSWORD', mandatory: true },
            { id: 'ADD_PATIENT_PASSWORD_CONFIRM', mandatory: true },
            { id: 'ADD_PATIENT_ROLE', mandatory: true },
            { id: 'ADD_PATIENT_SALT', mandatory: true }
        ]
    }, {
        id: 'ADD_PATIENT_S_2',
        className: 'ADD_PATIENT_S_2',
        questions: [
            { id: 'ADD_PATIENT_LANGUAGE_INFO' },
            { id: 'ADD_PATIENT_LANGUAGE', mandatory: true }
        ]
    }],

    questions: [{
        id: 'ADD_PATIENT_INFO',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_INFO',
        skipIT: '',
        title: '',
        text: ['NEW_PATIENT_QUESTION_1_MAIN_TITLE'],
        className: 'ADD_PATIENT_INFO'
    }, {
        id: 'ADD_PATIENT_ID',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_ID',
        skipIT: '',
        title: '',
        text: [],
        className: 'ADD_PATIENT_ID',
        widget: {
            id: 'ADD_PATIENT_W_1',
            type: 'PatientIDTextBox',
            templates: {},
            answers: [],
            label: 'NEW_PATIENT_QUESTION_1',

            // numeric from 1 to 4 characters
            maxLength: 4,
            allowedKeyRegex: /[\d]/,
            validateRegex: /[\d]+/,
            validationErrors: [{
                property: 'completed',
                errorType: 'popup',
                errString: 'NEW_PATIENT_QUESTION_1_EMPTY',
                header: 'NEW_PATIENT_QUESTION_1_EMPTY_HEADER',
                ok: 'OK'
            }],
            validation: {
                validationFunc: 'checkPatientID',
                params: {
                    errorStrings: {
                        isInRange: {
                            errString: 'NEW_PATIENT_QUESTION_1_RANGE',
                            header: 'NEW_PATIENT_QUESTION_1_RANGE_HEADER',
                            ok: 'OK'
                        },
                        isUnique: {
                            errString: 'NEW_PATIENT_QUESTION_1_UNIQUE',
                            header: 'NEW_PATIENT_QUESTION_1_UNIQUE_HEADER',
                            ok: 'OK'
                        }
                    }
                }
            }
        }
    }, {
        id: 'ADD_PATIENT_INITIALS',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_INITIALS',
        skipIT: '',
        title: '',
        text: [],
        className: 'ADD_PATIENT_INITIALS',
        widget: {
            id: 'ADD_PATIENT_W_2',
            type: 'TextBox',
            templates: {},
            answers: [],
            label: 'NEW_PATIENT_QUESTION_2',

            // Allow English keyboard characters only
            // jscs:disable maximumLineLength
            allowedKeyRegex: /^[a-zA-Z]*$/,
            // jscs:enable
            maxLength: 6,
            validateRegex: /.{2,}/,
            validationErrors: [{
                property: 'completed',
                errorType: 'popup',
                errString: 'NEW_PATIENT_QUESTION_2_EMPTY',
                header: 'NEW_PATIENT_QUESTION_2_EMPTY_HEADER',
                ok: 'OK'
            }]
        }
    }, {
        id: 'ADD_PATIENT_PASSWORD',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_PASSWORD',
        skipIT: '',
        title: '',
        text: [],
        className: 'PASSWORD',
        widget: {
            id: 'ADD_PATIENT_W_3',
            type: 'TempPasswordTextBox',
            label: 'TEMP_PASSWORD',
            templates: {},
            answers: [],
            field: 'password',
            validationErrors: [{
                property: 'isValid',
                errorType: 'banner',
                errString: 'INVALID_ENTRY'
            }],
            validation: {
                validationFunc: 'checkPasswordFieldWidget',
                params: {}
            },
            getRoleInputType: () => {
                return getRoleInputType('subject');
            }
        }
    }, {
        id: 'ADD_PATIENT_PASSWORD_CONFIRM',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_PASSWORD_CONFIRM',
        skipIT: '',
        title: '',
        text: [],
        className: 'PASSWORD',
        widget: {
            id: 'ADD_PATIENT_W_4',
            type: 'ConfirmationTextBox',
            label: 'CONFIRM_TEMP_PASS',
            templates: {},
            answers: [],
            field: 'confirm',
            fieldToConfirm: 'ADD_PATIENT_W_3',
            validationErrors: [{
                property: 'isValid',
                errorType: 'banner',
                errString: 'INVALID_ENTRY'
            }],
            validation: {
                validationFunc: 'confirmFieldValue',
                params: {
                    errorString: 'PASSWORD_MISMATCH'
                }
            },
            getRoleInputType: () => {
                return getRoleInputType('subject');
            }
        }
    }, {
        id: 'ADD_PATIENT_SALT',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_SALT',
        className: 'ADD_PATIENT_SALT',
        title: '',
        text: [],
        widget: {
            id: 'ADD_PATIENT_W_5',
            type: 'HiddenField',
            value: 'temp',
            field: 'salt'
        }
    }, {
        id: 'ADD_PATIENT_ROLE',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_ROLE',
        className: 'ADD_PATIENT_ROLE',
        title: '',
        text: [],
        widget: {
            id: 'ADD_PATIENT_W_8',
            type: 'HiddenField',
            value: 'subject',
            field: 'role'
        }
    }, {
        id: 'ADD_PATIENT_LANGUAGE_INFO',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_LANGUAGE_INFO',
        skipIT: '',
        title: '',

        text: ['NEW_PATIENT_QUESTION_3_TITLE'],
        className: 'ADD_PATIENT_LANGUAGE_INFO'
    }, {
        id: 'ADD_PATIENT_LANGUAGE',
        IG: 'Add_Patient',
        IT: 'ADD_PATIENT_LANGUAGE',
        skipIT: '',
        title: '',
        text: ['NEW_PATIENT_QUESTION_3'],
        className: '',
        widget: {
            id: 'ADD_PATIENT_W_3',
            type: 'LanguageSelectWidget',
            label: 'LANGUAGE',
            field: 'language',
            templates: {},
            answers: [],
            validation: {
                validationFunc: 'checkLanguageFieldWidget',
                params: {}
            }
        }
    }],

    affidavits: [{
        id: 'NewPatientAffidavit',
        text: [
            'NEW_PATIENT_AFFIDAVIT_HEADER',
            'SITE_REPORT_AFF'
        ],
        templates: { question: 'AffidavitText' },
        krSig: 'SubmitForm',
        widget: {
            id: 'SIGNATURE_AFFIDAVIT_WIDGET',
            className: 'AFFIDAVIT',
            type: 'SignatureBox'
        }
    }],

    rules: [
        // NewPatientCancel
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        // This rule ensures that when the questionnaire is cancelled, the correct view is displayed.
        {
            id: 'NewPatientCancel',
            trigger: 'QUESTIONNAIRE:Canceled/New_Patient',
            salience: 3,
            resolve: [
                // Instead of navigating to 'dashboard', we want to navigate to 'home'.
                { action: 'navigateTo', data: 'home' },

                // Prevent the default navigate call.
                { action: 'preventDefault' }
            ]
        },

        // QuestionnaireTransmitNewPatient
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        // This rule ensures that the newly created patient is transmitted after the questionnaire is completed.
        //
        {
            id: 'QuestionnaireTransmitNewPatient',
            trigger: 'QUESTIONNAIRE:Transmit/New_Patient',
            salience: 1,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'navigateTo', data: 'home' },
                { action: 'removeMessage' },
                { action: 'preventDefault' }
            ]
        },

        // NewPatientAffidavitBackout
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        // Due to the NewPatientSave we need this rule to
        // reinstate the back button behavior on the affidavit screen of the questionnaire.
        {
            id: 'NewPatientAffidavitBackout',

            // Trigger only on a navigation event on the affidavit screen of the New Patient questionnaire.
            trigger: 'QUESTIONNAIRE:Navigate/New_Patient/AFFIDAVIT',

            // If the back button was pressed, evaluate as true.
            evaluate: 'isDirectionBackward',

            // Set the importance of this rule below that of NewPatientSave.
            salience: 2,

            // Trigger the navigation handler of the BaseQuestionnaireView to navigate back to the previous screen.
            resolve: [
                { action: 'navigationHandler', data: 'back' },
                { action: 'preventDefault' }
            ]
        },

        // NewPatientSave
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        // After the user clicks 'Next' on the affidavit screen, save the new patient
        // and queue a transmission record to transmit the patient.
        {
            id: 'NewPatientSave',
            trigger: 'QUESTIONNAIRE:Navigate/New_Patient/AFFIDAVIT',

            // Only resolved on 'next' click.
            evaluate: 'isDirectionForward',
            salience: 1,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'newPatientSave' }
            ]
        },

        // NewPatientSaveOnTimeout
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        {
            id: 'NewPatientQuestionnaireTimeout',
            trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/New_Patient',
            evaluate: 'isAffidavitSigned',
            salience: 3,
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'newPatientSave' },
                { action: 'removeMessage' },
                { action: 'navigateTo', data: 'home' },
                { action: 'preventAll' }
            ],
            reject: [{
                action: 'navigateTo',
                data: 'home'
            }, {
                action: 'notify',
                data: { key: 'DIARY_TIMEOUT' }
            }, {
                action: 'preventAll'
            }]
        },

        // NewPatientSaveOnTimeout
        // NOTE: This rule is required for the New Patient workflow. Do not remove.
        {
            id: 'NewPatientSessionTimeout',
            trigger: 'QUESTIONNAIRE:SessionTimeout/New_Patient',
            salience: 3,
            evaluate: 'isAffidavitSigned',
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'newPatientSave' },
                { action: 'removeMessage' }
            ]
        },

        // NewPatientPreventNavigation
        // NOTE: This rule is required for the New Patient workflow
        {
            id: 'NewPatientDefect',
            trigger: 'NAVIGATE:application/addNewPatient',
            evaluate: 'isQuestionnaireCompleted',
            resolve: [{ action: 'preventDefault' }]
        }
    ]
};
