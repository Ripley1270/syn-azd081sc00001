import PageView from './PageView';
import { evaluateBranching } from 'core/branching/evaluateBranching';
import QuestionView from './QuestionView';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import Spinner from 'core/Spinner';
import * as Utilities from 'core/utilities';
import * as lStorage from 'core/lStorage';
import { MessageRepo, Banner } from 'core/Notify';
import Data from 'core/Data';
import CurrentContext from 'core/CurrentContext';
import Screen from 'core/models/Screen';
import Users from 'core/collections/Users';
import User from 'core/models/User';
import AnswerBase from 'core/models/AnswerBase';
import Answers from 'core/collections/Answers';
import Questions from 'core/collections/Questions';

let logger = new Logger('core/view/BaseQuestionnaireView');

export default class BaseQuestionnaireView extends PageView {
    /**
     *
     * @param {Object} options
     * Required properties
     * id {string} - the ID of the questionnaire
     * Optional properties
     * showCancel {boolean} - Indicates whether to display the cancel button
     */
    constructor (options = {}) {
        super(options);

        this.subject = options.subject;

        // DyncamicText architecture does not allow a way to pass parameters so we have to set the subject for it.
        // Better to set into DynamicText workspace than to global
        LF.DynamicText.subject = this.subject;

        // Questionnaire views need the view assigned to Data.Questionnaire.
        Data.Questionnaire = this;

        /**
         * need to define handlers and event data prior to calling backbone.View.super()
         * which registers the handlers
         */
        /**
         * Handles the previous button being pressed.
         */
        this.previousHandler = _.debounce(() => {
            this.previous();
        }, 300, true);

        /**
         * Handles the next button being pressed.
         */
        this.nextHandler = _.debounce(() => {
            let valid = this.validateScreens();
            if (valid) {
                // TODO: swallow failure??  this is what original code did (dtp).
                this.attemptToSkip();
            } else {
                this.showValidationErrorMessage();
            }
        }, 300, true);

        /**
         *  Handles the cancel button being pressed.
         *  @param {Event} evt Event data
         */
        this.cancelHandler = _.debounce((evt) => {
            const { Dialog } = MessageRepo;

            evt.preventDefault();

            this.disableButton(this.$('#cancelItem'));
            MessageRepo.display(Dialog && Dialog[`BASE_QUESTIONNAIREVIEW_${this.id}`], { cancelPopupStyle: this.cancelPopupStyle })
            .then(() => {
                return this.onCancel()
                .then(() => ELF.trigger(`QUESTIONNAIRE:Canceled/${this.id}`, {
                    questionnaire: this.id
                }, this))
                .then((flags) => {
                    if (flags.preventDefault) {
                        return;
                    }

                    this.navigate(this.defaultRouteOnExit, true, this.defaultFlashParamsOnExit);
                });
            })

            // TODO: ConfirmView either resolves or rejects its returned promise based on the button clicked.
            // This needs to be fixed to resolve a boolean value.  Until then, I'm adding a placeholder
            // to prevent an error from being logged when the user clicks 'No'.
            .catch(() => {
                logger.info('User Clicked "No" when asked for confirmation when canceling diary.');
            })
            .finally(() => {
                this.enableButton(this.$('#cancelItem'));
            })
            .done();
        }, 300, true);

        // noinspection JSValidateTypes
        /**
         * Application events
         * @readonly
         * @enum {Event}
         */
        this.events = {
            // Click event for going to previous screen
            'click #prevItem': 'previousHandler',

            // Click event for going to next screen
            'click #nextItem': 'nextHandler',

            // Click event for cancel button
            'click #cancelItem': 'cancelHandler'
        };

        /**
         * The unique id of the root element.
         * @readonly
         * @type String
         * @default 'questionnaire-page'
         */
        this.id = 'questionnaire-page';

        /**
         *  Used to indicate if the cancel button should be displayed.
         *  @type boolean
         *  @default false
         */
        this.showCancel = options.showCancel || false;

        /**
         *  Used to indicate the cancel popup style css class.
         *  @type string
         *  @default undefined
         */
        this.cancelPopupStyle = options.cancelPopupStyle || false;

        /**
         * Id of template to render
         * @type String
         * @readonly
         * @default '#questionnaire-template'
         */
        this.template = '#questionnaire-template';

        /**
         * The unique pageType of the root element.
         * @readonly
         * @type String
         * @default 'questionnaire-page'
         */
        this.pageType = 'questionnaire-page';

        /**
         * Used to keep track of screens.
         * @type Array
         * @default new {Array}
         */
        this.screenStack = [];

        this.options = options;

        // remove questionnaire to dashboard navigation flag
        localStorage.removeItem('questionnaireToDashboard');
        localStorage.removeItem('questionnaireCompleted');

        this.Answer = AnswerBase;

        // the method to run on transmissions
        this.transmissionMethod = 'transmitQuestionnaire';

        // The Questionnaire's id.
        this.id = options.id;

        if (options.ordinal) {
            this.editMode = false;
            this.ordinal = options.ordinal;
        } else {
            this.editMode = true;

            // critical section... but JS is single threaded
            this.ordinal = BaseQuestionnaireView.getInstanceOrdinal();
        }

        // The Questionnaire's schedule id.
        this.schedule_id = this.options.schedule_id;

        // The current screens index within this.data.screens.
        this.screen = 0;

        // Clear the screen stack
        this.screenStack = [];

        // Event delegation
        if (this.showCancel) {
            this.delegateEvents(_.extend(_.clone(this.events), {
                'click #cancelItem': 'cancelHandler'
            }));
        } else {
            this.delegateEvents();
        }

        // The Questionnaire's in-mem. namespace.
        this.data = {
            // The IG list for current Questionnaire and their IGRs.
            igList: {},

            // This list will have the current IGR for all IGs
            currentIGR: {},

            questions: new Questions(),
            answers: new Answers(),
            screens: [],
            started: new Date()
        };

        this.questionViews = [];

        // logs that the diary has been started
        logger.operational(`Diary ${this.id} started.`, {
            diary: this.id
        });

        // When window 'resize' event is fired, trigger it on the questionnaire element
        $(window).resize(_.debounce(() => {
            this.$('#questionnaire').trigger('questionnaire:resize');
        }, 100));

        let model = LF.StudyDesign.questionnaires.get(this.id);

        this.model = model ? model.clone() : null;
    }

    /**
     * onCancel
     * A function for subviews to hook view specific operations to be executed onCancel button click
     * @return {Q.Promise<void>} resolved when complete
     */
    onCancel () {
        return Q();
    }

    /**
     * @property {Answers} answers - The answers associated with this QuestionnaireView
     */
    get answers () {
        return this.data.answers;
    }

    set answers (answers) {
        this.data.answers = answers;
    }

    /**
     * gets the id of the currently displayed screen
     */
    get currentScreenId () {
        return this.screens[this.screen].id;
    }

    /**
     * gets the id of the currently displayed screen
     */
    get currentScreen () {
        return this.screens[this.screen];
    }

    get screens () {
        return this.data.screens;
    }

    get defaultRouteOnExit () {
        return 'dashboard';
    }

    get defaultFlashParamsOnExit () {
        return {
            subject: this.subject,
            visit: this.visit
        };
    }

    /**
     * Performs the standard screen, Question and affidavit prep work.
     * @returns {Q.Promise<void>} resolved when complete
     */
    resolve () {
        return Q.Promise((resolve, reject) => {
            this.prepScreens();
            this.prepQuestions();
            this.configureAffidavit();

            return this.prepAnswerData(this.id, this.ordinal)
            .then(() => this.open())
            .then(() => resolve())
            .catch(e => reject(e));
        });
    }

    /**
     * performs the actual Open / render.   Triggers the QUESTIONNAIRE:Open rule
     * @returns {Q.Promise<void>} resolved when complete
     */
    open () {
        return ELF.trigger(`QUESTIONNAIRE:Open/${this.id}`, {}, this)
        .then(() => {
            // if returning a new value/promise, use .then, not .finally
            LF.security.startQuestionnaireTimeOut();
        });
    }

    /**
     * initializes the answer set for the Questionnaire.
     * @param {string} id - The questionnaire ID to prep answers for.
     * @param {number} editMode - true if editable
     * @returns {deferred.promise|{then}}
     */
    prepAnswerData (id, editMode) {
        let deferred = Q.defer();

        try {
            this.answers = new Answers();

            if (editMode) {
                deferred.resolve();
            } else {
                this.answers.fetch({
                    search: {
                        where: {
                            questionnaire_id: id,
                            instance_ordinal: this.ordinal
                        }
                    }
                })
                .then(deferred.resolve)
                .catch(deferred.reject);
            }
        } catch (e) {
            deferred.reject(e);
        }

        return deferred.promise;
    }

    /**
     * Finds and prepares all screen data.
     * @returns {Screens}
     * @example this.prepScreens();
     */
    prepScreens () {
        let screenIDs = this.model.get('screens');

        this.data.screens = [];

        for (let ndx = 0; ndx < screenIDs.length; ndx++) {
            let screen = LF.StudyDesign.screens.get(screenIDs[ndx]);

            if (screen) {
                // Cloning the screen configuration to prevent the master copy from being modified.
                this.data.screens.push(screen.clone());
            }
        }

        return this.data.screens;
    }

    /**
     * Reset answers past a given question.
     * @param {string[]} records An array of question ids to remove.
     * @param {Function=} resolve Optional callback function to resolve the ELF rule calling this function
     */
    resetAnswersSelected (records, resolve) {
        let questions = this.questionViews,
            answers = this.answers;

        for (let ndx = 0; ndx < records.length; ndx++) {
            let item = records[ndx],
                answersToClear = [],
                record;

            record = _(questions).find((view) => {
                let IG = view.ig;
                if (IG) {
                    return view.id === item && view.igr === this.getCurrentIGR(IG);
                }
                return view.id === item;
            });

            if (record) {
                questions.splice(_(questions).indexOf(record), 1);

                if (item === 'AFFIDAVIT') {
                    answersToClear = this.queryAnswersByID(item);
                    answers.remove(answersToClear);
                } else {
                    answersToClear = this.queryAnswersByQuestionIDAndIGR(record.id, record.igr);
                    answers.remove(answersToClear);
                }
            }
        }

        resolve && resolve();
    }

    /**
     * Creates a new collection of Question models pulled from the study design.
     * @example this.prepQuestions();
     */
    prepQuestions () {
        let data = this.data;

        this.questionViews = [];
        data.questions = new Questions();

        for (let ndx = 0; ndx < data.screens.length; ndx++) {
            let screen = data.screens[ndx],
                questions = screen.get('questions');

            for (let questionNdx = 0; questionNdx < questions.length; questionNdx++) {
                let question = questions[questionNdx],
                    model = LF.StudyDesign.questions.get(question.id);

                if (!model) {
                    const msg = `Configuration Error:
                        Screen "${screen.get('id')}" has undefined question "${question.id}"`;
                    logger.error(msg);
                    throw msg;
                }

                // evaluate if the IG is for repeating, if yes then set the IGR to 1
                if (model.get('repeating') === true && model.get('IG')) {
                    data.igList[model.get('IG')] = 1;
                    data.currentIGR[model.get('IG')] = 1;
                } else if (!data.igList[model.get('IG')] && model.get('IG')) {
                    data.igList[model.get('IG')] = 0;
                    data.currentIGR[model.get('IG')] = 0;
                }

                // add the question model to the namespace
                data.questions.add(model);
            }
        }
    }

    /**
     * Renders the view.
     * @param {string=} id - screen id to render (TODO: if omitted does what?)
     * @returns {Q.Promise<void>}
     */
    render (id) {
        this.templateStrings = {
            header: {
                namespace: this.model.get('id'),
                key: this.model.get('displayName')
            },
            back: 'BACK',
            next: 'NEXT'
        };

        if (this.showCancel) {
            this.templateStrings.cancel = 'CANCEL';
        } else {
            this.templateStrings.cancel = '';
        }

        return this.buildHTML({
            className: this.model.get('className')
        })
        .then(() => {
            this.page();

            this.disableButton(this.$('#nextItem'));
            this.disableButton(this.$('#prevItem'));

            // Hide the cancel button, if appropriate:
            if (!this.showCancel) {
                this.hideCancelButton();
            }
        })
        .then(() => ELF.trigger(`QUESTIONNAIRE:Rendered/${this.id}`, { questionnaire: this.id }, this))
        .then(() => {
            return this.displayScreen(id || this.data.screens[0].get('id'));
        })
        .then(() => {
            if (!id) {
                this.screenStack.push(this.data.screens[0].get('id'));
            }
            this.delegateEvents();
        })
        .catch((e) => {
            if (e.doNotLog) {
                return Q();
            }

            logger.error('From render.QUESTIONNAIRE:Rendered: ', e);
            return Q.reject(e);
        });
    }

    /**
     * Configures the affidavit.
     * @example this.configureAffidavit();
     */
    configureAffidavit () {
        let screen,
            question,
            affidavit,
            getAffidavit = () => {
                let config = this.model.get('affidavit');

                switch (typeof config) {
                    case 'object':
                        return config[CurrentContext().role];
                    default:
                        return config;
                }
            },
            affidavitId = getAffidavit();

        if (Utilities.toType(affidavitId) !== 'boolean' && affidavitId !== false && this.editMode) {
            if (typeof affidavitId === 'undefined') {
                affidavitId = CurrentContext().get('role').get('defaultAffidavit') || 'DEFAULT';
            }
            affidavit = LF.StudyDesign.affidavits.where({
                id: affidavitId
            })[0];

            screen = new Screen({
                id: 'AFFIDAVIT',
                className: 'AFFIDAVIT',
                questions: [{
                    id: 'AFFIDAVIT',
                    mandatory: true
                }]
            });

            question = affidavit.clone();
            question.set({
                id: 'AFFIDAVIT'
            });

            this.data.screens.push(screen);
            this.data.questions.add(question);
        }
    }

    // noinspection JSMethodCanBeStatic
    /**
     * Get the selected site user's name.
     * TODO: need to have the site user specific to this questionnaire, not a global object for
     * shared sitepad with waiting room functionality
     * @return {String} site user's name
     */
    getSiteUser () {
        return Data.SiteUserName || null;
    }

    /**
     * Invoked before the display of each screen.  Determines if the screen exists, and triggers an ELF event.
     * @param {String} id screen id
     * @return {Q.Promise<void>} resolved when complete
     */
    beforeDisplayScreen (id) {
        let screens = this.data.screens,
            screen = _.findWhere(screens, { id }),
            screenNdx = screens.map(screen => screen.id).indexOf(id);

        // DE16954 - Before the screen is displayed, either hide or show the back button.
        // This is configured via Screen.disableBack<boolean>. See core/models/Screen
        this.$('#prevItem')[screen.get('disableBack') === true ? 'hide' : 'show']();

        if (screenNdx > -1) {
            this.screen = screenNdx;
        } else {
            return Q.Promise((resolve, reject) => {
                reject(`.beforeDisplayScreen(): id = ${id} not found in current questionnaire.`);
            });
        }

        return ELF.trigger(`QUESTIONNAIRE:Before/${this.id}/${id}`, {
            questionnaire: this.id,
            subject: this.subject,
            visit: this.visit
        }, this);
    }

    /**
     * afterContextSwitchControl
     * A function to hook operations to be done after the Context Switch control is done
     * @param {Object} e The event parameters returned by the ELF engine.
     * @param {Object} params - Parameters passed into the context switch control.
     * @return {Q.Promise<void>} resolved when complete
     */
    afterContextSwitchControl (e = {}, params = {}) {
        return Q.Promise((resolve, reject) => {
            if (e.preventDefault) {
                let id = params.id || 'N/A';

                // If there is gonna be a context switch we should break the promise so that the screen is not displayed
                logger.operational(`Do not display screen because a context switch is required. Screen id: ${id}`, { id });
                reject({ doNotLog: true });
            } else {
                resolve();
            }
        });
    }

    /**
     * Disable the footer's button elements.
     * @returns {Function} A function to return the footer to it's previous state.
     */
    disableFooter () {
        let $footer = this.getElement('#footer');
        let results = [];

        // Just to be sure no superfluous events are triggered, undelegate
        // all bound events on the questionnaire view.
        this.undelegateEvents();

        // Find each button in the footer and disable it.
        $footer.find('button')
            .each((i, btn) => {
                let $btn = this.$(btn);

                results.push({
                    previousState: this.isDisabled($btn) ? 'disabled' : 'enabled',
                    ele: $btn
                });

                $btn && this.disableElement($btn);
            });
        $footer = null;

        // Returned function can be invoked to revert the footer to its prior state.
        return () => {
            // For each resulting button, if it was previously enabled, revert its state.
            _.forEach(results, (btn) => {
                if (btn.previousState === 'enabled') {
                    this.enableButton(btn.ele);
                }
            });
            this.delegateEvents();

            // Clear out the results to prevent memory leaks due to DOM references in a closure.
            results = null;
        };
    }

    /**
     * Show the cancel button.
     */
    showCancelButton () {
        let $cancel = $('#cancelItem');

        // The LPA doesn't have a cancel button, so we need to check if it exists in the DOM.
        if ($cancel != null) {
            this.showElement($cancel);

            // Null out the DOM fragment reference to prevent a memory leak.
            $cancel = null;
        }
    }

    /**
     * Hide the cancel button.
     */
    hideCancelButton () {
        let $cancel = this.$('#cancelItem');

        // The LPA doesn't have a cancel button, so we need to check if it exists in the DOM.
        if ($cancel != null) {
            this.hideElement($cancel);

            // Null out the DOM fragment reference to prevent a memory leak.
            $cancel = null;
        }
    }

    /**
     * Renders a screen indicated.  Note this not place the screen in the screenStack.
     * @param {String} id The id of the screen to display.
     * @returns {Q.Promise<void>}  resolved when complete
     */
    displayScreen (id) {
        const screens = this.data.screens;

        let ndx,
            screen;

        try {
            for (ndx = 0; ndx < screens.length; ndx++) {
                screen = screens[ndx];
                if (screen.id === id) {
                    break;
                }
            }
            if (ndx >= screens.length) {
                return Q.reject(`.displayScreen(): id = ${id} not found in current questionnaire.`);
            }

            this.screen = ndx;
        } catch (e) {
            return Q.reject(e);
        }

        // DE17495 - Before displaying a screen, we need to hide the footer so the user cannot interact with it.
        let revertFooter = this.disableFooter();

        return this.beforeDisplayScreen(id)
        .tap(e => this.afterContextSwitchControl(e, { id }))
        .then(() => {
            let model,
                questions,
                outstandingPromises = [];

            revertFooter();

            try {
                questions = screen.get('questions');

                // Add a screen and a modal container.
                this.$('#questionnaire').html(LF.templates.display(screen.get('template') || 'DEFAULT:Screen', {
                    className: screen.get('className')
                }))
                    .children()
                    .first()
                    .append('<div id="modalContainer"></div>');

                for (let questionNdx = 0; questionNdx < questions.length; questionNdx++) {
                    let question = questions[questionNdx],
                        view = _(this.questionViews).find((item) => {
                            let IG = item.model.get('IG');

                            if (IG) {
                                return item.id === question.id && item.igr === this.getCurrentIGR(IG);
                            }
                            return item.id === question.id;
                        });

                    // If no view exists create a new one.
                    if (!view) {
                        model = this.data.questions.get(question.id);

                        view = new QuestionView({
                            id: model.get('id'),
                            screen: id,
                            model,
                            parent: this,
                            mandatory: !!(typeof question.mandatory === 'undefined' ||
                            question.mandatory),
                            ig: model.get('IG'),
                            igr: model.get('IG') ? this.getCurrentIGR(model.get('IG')) : null
                        });

                        this.questionViews.push(view);
                    }

                    view.clearErrors();
                    outstandingPromises.push(view.render(`.${screen.get('className')}`));
                }
                this.enableButton(this.$('#nextItem'));
                this.enableButton(this.$('#prevItem'));
                return Q.allSettled(outstandingPromises);
            } catch (e) {
                logger.error(`displayScreen() unexpected exception setting up promise: ${e || ''}`);
                return Q.reject(e);
            }
        })
        .then(() => {
            return ELF.trigger(`QUESTIONNAIRE:Displayed/${this.id}/${screen.get('id')}`, {
                questionnaire: this.id
            }, this);
        });
    }

    /**
     * @returns {Q.Promise<void>} resolved when error shown
     */
    showValidationErrorMessage () {
        let questions = this.data.invalidQuestions;

        for (let ndx = 0; ndx < questions.length; ndx++) {
            let question = questions[ndx],
                errorShown;

            // If the screen is not valid, set valid to false.
            errorShown = _(this.questionViews).find((item) => {
                let IG = item.model.get('IG');
                if (IG) {
                    return item.id === question.id && item.igr === this.getCurrentIGR(IG);
                }
                return item.id === question.id;
            }).displayError();

            if (errorShown && ndx === questions.length) {
                return Q();
            }
        }

        // Nothing showed an error message.  Show the default one.
        return MessageRepo.display(MessageRepo.Banner.REQUIRED_ANSWER);
    }

    /**
     * Validates the screen.
     * @returns {Boolean} Return true if all questions are valid, otherwise false.
     * @example this.validateScreens();
     */
    validateScreens () {
        let questions = this.getQuestions(),
            valid = true;
        this.data.invalidQuestions = [];

        for (let ndx = 0; ndx < questions.length; ndx++) {
            let question = questions[ndx],

            // If the screen is not valid, set valid to false.
                isValid = _(this.questionViews).find((item) => {
                    let IG = item.model.get('IG');
                    if (IG) {
                        return item.id === question.id && item.igr === this.getCurrentIGR(IG);
                    }
                    return item.id === question.id;
                }).validate();

            if (!isValid) {
                this.data.invalidQuestions.push(question);
                valid = false;
            }
        }
        return valid;
    }

    /**
     * Get question views belonging to the current screen.
     * @return {Array} Includes questions
     */
    getQuestions () {
        let screen = this.data.screens[this.screen],
            questions = screen.get('questions'),
            retQuestions = [];

        for (let ndx = 0; ndx < questions.length; ndx++) {
            let question = questions[ndx];

            retQuestions.push(_(this.questionViews).find(item => item.id === question.id));
        }

        return retQuestions;
    }

    /**
     * Attempts to skip every displayed question, if it isn't answered.
     * @returns {Q.Promise<boolean>} true if skipped
     */
    attemptToSkip () {
        const questionViews = this.getQuestions(),
            outstandingChecks = [];
        let valid = true,
            questionsSkipped = false;

        // decide if there is any question which can be skipped
        for (let ndx = 0; ndx < questionViews.length; ndx++) {
            let questionView = questionViews[ndx];
            if (questionView.widget) {
                if (questionView.widget.completed) {
                    // we have an answered question... run validation checks
                    if (questionView.widget.model.get('validation')) {
                        outstandingChecks.push(ELF.trigger('WIDGET:Validation', {}, questionView.widget)
                        .then((res) => {
                            if (res.preventDefault) {
                                valid = false;
                                logger.trace([
                                    'WIDGET:Validation trigger signaled preventDefault',
                                    `for widget ${questionView.widget.id}`
                                ].join(' '));
                            } else if (!questionView.widget.completed) {
                                if (questionView.isSkippable()) {
                                    questionsSkipped = true;
                                } else {
                                    valid = false;
                                }
                            }
                        }));
                    }
                } else if (questionView.isSkippable()) {
                    questionsSkipped = true;
                    this.disableButton('#nextItem');
                } else {
                    // if it isn't complete and you can't skip
                    valid = false;
                }
            }
        }

        return Q.allSettled(outstandingChecks)
        .then(() => {
            if (valid && questionsSkipped) {
                return ELF.trigger(`QUESTIONNAIRE:SkipQuestions/${this.id}`, {
                    screen: this.data.screens[this.screen]
                }, this)
                .then(flags => !flags.preventDefault);
            }
            return valid;
        })
        .then((doDefault) => {
            if (doDefault) {
                return this.next()
                .then(() => true);
            }
            this.enableButton('#nextItem');

            return false;
        });
    }

    /**
     * Try and skip each question belonging to the current screen.
     * @returns {Q.Promise<void>} resolved when done
     */
    skip () {
        let questions = this.getQuestions();

        return Q.allSettled(questions.map(question => question.attemptToSkip()));
    }

    /**
     * Performs a save operation.  Base class implementation does nothing.
     * @returns {Q.Promise<void>} promise resolved when save complete.
     */
    save () {
        let deferred = Q.defer();
        deferred.resolve();

        return deferred.promise;
    }

    // noinspection JSUnusedLocalSymbols
    /**
     * Handles diary completion by activating triggers and saving diary
     * @param {*=} params Optional parameters passed to the action.
     * @param {Function=} callback A callback function invoked upon the completion of the action.
     * @return {Q.Promise<void>} resolved when complete
     */
    completeDiary (params, callback = $.noop) {
        if (this.diaryCompleted) {
            return Q.Promise((resolve, reject) => {
                reject('save already started');
            });
        }
        this.diaryCompleted = true;

        let siteUserQuestion = this.data.questions.get('SITEUSER');

        LF.security.stopQuestionnaireTimeOut();
        LF.security.pauseSessionTimeOut();

        // previousPhase is needed to save the proper phase into UserReport record
        this.previousPhase = this.subject ? this.subject.get('phase') : null;

        // The questionnaire is completed, but not yet saved
        return ELF.trigger(`QUESTIONNAIRE:Completed/${this.id}`, {
            questionnaire: this.id
        }, this)
        // eslint-disable-next-line consistent-return
        .then((res) => {
            if (!res.preventDefault) {
                let retPromise;

                logger.info('Diary Saving process is started');

                // FIXME: This logic should not be here. Extract it to somewhere related to that widget
                // if site user widget was used, update the last_used timestamp in SiteUser db table
                if (siteUserQuestion) {
                    let siteUserName = this.getSiteUser(),
                        tempCollection = new Users();

                    retPromise = tempCollection.fetch({
                        search: {
                            where: {
                                site_user_name: siteUserName
                            }
                        }
                    })
                    .then((result) => {
                        return new User({
                            id: result[0].get('id'),
                            site_user_name: result[0].get('site_user_name'),
                            last_used: new Date().now().toString()
                        }).save();
                    });
                } else {
                    retPromise = Q();
                }

                return retPromise
                .then(() => this.save(null))
                .then(() => ELF.trigger(`QUESTIONNAIRE:Saved/${this.id}`, {
                    questionnaire: this.id
                }, this))
                .finally(() => {
                    try {
                        callback();
                    } finally {
                        LF.security.startQuestionnaireTimeOut();
                        LF.security.restartSessionTimeOut();
                    }
                });
            }
        });
    }

    /**
     * Hook called after the questionnaire is saved and completed.
     * SitePad and PDEs can use this one to extend the base questionnaire
     * view and add custom behavior.
     * @returns {Q.Promise<void>}
     */
    onAfterQuestionnaireCompleted () {
        return Q();
    }

    /**
     * Hook called before the questionnaire is saved and completed.
     * SitePad and PDEs can use this one to extend the base questionnaire
     * view and add custom behavior.
     * @returns {Q.Promise<void>}
     */
    onBeforeQuestionnaireCompleted () {
        return Q();
    }

    // noinspection JSUnusedLocalSymbols
    /**
     * Move to the next screen.
     * @returns {Q.Promise<void>}
     */
    next () {
        let screens = this.data.screens,
            currentScreen = screens[this.screen].get('id'),
            triggerName = `QUESTIONNAIRE:Navigate/${this.id}/${currentScreen}`;

        if (this.screen + 1 >= screens.length) {
            logger.trace('last screen completed; about to complete questionnaire');

            return ELF.trigger(triggerName, {
                answers: this.data.Answers,
                screenId: currentScreen,
                direction: 'next'
            }, this)
            .then((res) => {
                if (res && !res.preventDefault) {
                    localStorage.setItem('questionnaireCompleted', true);
                    logger.trace('Navigating to questionnaire-completion');
                    this.navigate('questionnaire-completion', true);
                }
            });
        }
        this.$('#questionnaire').unbind('questionnaire:resize');

        return ELF.trigger(triggerName, {
            screenId: currentScreen,
            direction: 'next'
        }, this)
        .then((res) => {
            if (res.preventDefault) {
                return Q();
            }

            // Move forward with branching! This is the real stuff.
            Spinner.hide();
            this.direction = 'forward';
            return this.navigationHandler('next');
        })
        .catch((e) => {
            if (e.doNotLog) {
                return Q();
            }
            return logger.error('exception in BaseQuestionaireView.next.ELF.QUESTIONNAIRE:Navigate.then(): ', e);
        });
    }

    /**
     * Handles the questionnaire back-out.
     * @returns {Q.Promise<void>}
     */
    backoutHandler () {
        let deferred = Q.defer();

        this.disableButton(this.$('#prevItem'));
        ELF.trigger(`QUESTIONNAIRE:BackOut/${this.id}`, {
            questionnaire: this.id
        }, this)
        .then((res) => {
            if (!res.preventDefault) {
                localStorage.setItem('questionnaireToDashboard', true);
                LF.router.flash({
                    subject: this.subject,
                    visit: this.visit
                }).navigate('dashboard', true);
            }
        })
        .finally(() => {
            this.enableButton(this.$('#prevItem'));
            deferred.resolve();
        });

        return deferred.promise;
    }

    /**
     * Move to the previous screen.
     * @returns {Q.Promise}
     */
    previous () {
        let currentScreen,
            screens = this.data.screens;

        if (this.screenStack.length < 2) {
            return this.backoutHandler();
        }
        this.$('#questionnaire').unbind('questionnaire:resize');
        currentScreen = screens[this.screen].get('id');

        return ELF.trigger(`QUESTIONNAIRE:Navigate/${this.id}/${currentScreen}`, {
            screenId: currentScreen,
            direction: 'previous'
        }, this)
        .then((res) => {
            if (res.preventDefault) {
                return Q();
            }

            // Close banner notifications when going back to previous screen
            setTimeout(Banner.closeAll, 500);

            // Move backwards! This is the real stuff.
            this.direction = 'backward';
            return this.navigationHandler('previous');
        })
        .catch((e) => {
            if (e.doNotLog) {
                return Q();
            }
            return logger.error('exception in BaseQuestionaireView.previous.ELF.QUESTIONNAIRE:Navigate.then(): ', e);
        });
    }

    /**
     * performs default branching.
     * @param {string} screenId - The ID of the screen to evaluate.
     * @returns {Q.Promise<boolean>}
     */
    evaluateBranching (screenId) {
        return evaluateBranching(this.model.get('branches'), screenId, this.answers, this);
    }

    /**
     * Move to the correct screen with respect to direction and branching
     * @param {String} direction The direction to navigate in. "next" or "previous".
     * @return {Q.Promise<void>} resolved when complete
     */
    navigationHandler (direction) {
        let screens = this.data.screens,
            screenObj = screens[this.screen],
            screenId = screenObj.get('id'),
            screenIndex = this.screen,
            defaultNavigate = (direction) => {
                if (direction === 'next') {
                    // Moving forward, push the next sequential screen on to the stack.
                    this.screenStack.push(screens[screenIndex + 1].get('id'));
                } else {
                    // Moving backward, pop the last screen off of the stack.
                    this.screenStack.pop();
                }
                return _(this.screenStack).last();
            };

        // Evaluate branching only on next navigation
        if (direction === 'next') {
            // wait until branch finish with evaluation
            return Q(this.evaluateBranching(screenId))        // should work whether returns a promise or value
            .then((branchObj) => {
                if (!branchObj) {
                    // No branch evaluates to true, move forward one screen.
                    return defaultNavigate(direction);
                }

                if (branchObj.clearBranchedResponses !== false) {
                    // TODO: I don't think this clears loops properly
                    // limitation: this only works moving from lower to higher indexes within the screens
                    let isSkippedByBranching = false,
                        questionsToClear = [];

                    // Get the list of skipped questions
                    for (let ndx = 0; ndx < screens.length; ndx++) {
                        let screen = screens[ndx];
                        if (screenId === screen.get('id') || (branchObj.branchTo === screen.get('id'))) {
                            if (isSkippedByBranching) {
                                // reach the end of the skipped block
                                break;
                            }

                            // at the start of a block.
                            isSkippedByBranching = true;
                            continue;
                        }

                        if (isSkippedByBranching) {
                            questionsToClear = questionsToClear.concat(_.pluck(screen.get('questions'), 'id'));
                        }
                    }
                    this.resetAnswersSelected(questionsToClear);
                }

                this.screenStack.push(branchObj.branchTo);
                return branchObj.branchTo;
            })
            .then((screenId) => {
                return this.displayScreen(screenId);
            })
            .finally(() => {
                LF.spinner.hide();
            });
        }
        screenId = defaultNavigate(direction);

        return this.displayScreen(screenId)
        .then(() => {
            LF.spinner.hide();
        });
    }

    /**
     * utility functions
     */
    /**
     * Query all answers with matching Question ID & IGR and returns array of answer models.
     * @param {String} id a Question ID string
     * @param {Number} IGR an IGR number
     * @return {Array} an array of question models matching the "Question ID" and "IGR"
     * @example
     * let results = LF.Utilities.queryAnswersByQuestionIDAndIGR('NRS_DIARY_Q1', 2);
     */
    queryAnswersByQuestionIDAndIGR (id, IGR) {
        let swAlias,
            answersQuery = [],
            answersCollection = this.answers;

        answersCollection.each((model) => {
            swAlias = model.get('SW_Alias').split('.');
            if (model.get('question_id') === id && parseInt(swAlias[1], 10) === IGR) {
                answersQuery.push(model);
            }
        });

        return answersQuery;
    }

    /**
     * Gets the current IGR for the matching IG and returns a number. It looks for the IGR in
     * Data.Questionnaire.currentIGR which is an Object which holds current IGRs for all IGs
     * (as key pair value) for the diary in progress.
     * @param {String} IG an IG string.
     * @return {Number} a number which is the current IGR for the matching IG.
     * @example
     */
    getCurrentIGR (IG) {
        if (!_.isNumber(this.data.currentIGR[IG])) {
            throw new Error(`Attempt failed to get current IGR for IG: ${IG}`);
        } else {
            return this.data.currentIGR[IG];
        }
    }

    /**
     * Sets the IGR passed for the matching IG. It looks for the IG in
     * Data.Questionnaire.currentIGR and change IGR to the number passed.
     * This is very useful when editing Loop event previously added.
     * Setting the IGR allows proper fetching of answers for the Questions to be edited.
     * @param {String} IG an IG string.
     * @param {Number} IGR an IGR number
     * @example
     * LF.Utilities.setCurrentIGR('Headache', 1);
     */
    setCurrentIGR (IG, IGR) {
        if (!_.isNumber(this.data.currentIGR[IG])) {
            throw new Error(`Attempt failed to set IGR: ${IGR} for IG: ${IG}`);
        } else {
            this.data.currentIGR[IG] = IGR;
        }
    }

    /**
     * Updates the IGR for the matching IG to next available number. It looks for the IGR in
     * Data.Questionnaire.currentIGR and change the IGR to the next available IGR
     * which can be used to add a looping event. The master list of IGs and their IGRs
     * for current diary is at Data.Questionnaire.igList.
     * This is very useful when editing (of looping event) is done and the IGR needs to be set back
     * to its original state.
     * @param {String} IG an IG string.
     * @example
     * LF.Utilities.updateIGR('Headache');
     */
    updateIGR (IG) {
        if (!_.isNumber(this.data.currentIGR[IG])) {
            throw new Error(`Attempt failed to update IGR to the current IGR for IG: ${IG}`);
        } else {
            this.data.currentIGR[IG] = this.data.igList[IG];
        }
    }

    // noinspection JSUnusedGlobalSymbols
    clearIGRValues () {
        this.data.currentIGR = {};
    }

    /**
     * Increments the IGR for the matching IG to next number. It looks for the IG in
     * Data.Questionnaire.igList and increments the IGR. It also sets the same value of IGR to the IG
     * in Data.Questionnaire.currentIGR. So on doing LF.Utilities.getCurrentIGR(IG) it will get
     * the current available IGR which can be used.
     * This Utility function should be called as soon as loop event is done, so it increments the IGR
     * which can be used for adding another loop event.
     * @param {String} IG an IG string.
     * @example
     * LF.Utilities.incrementIGR('Headache');
     */
    incrementIGR (IG) {
        if (!this.data.igList[IG]) {
            throw new Error(`Attempt failed to increment IGR for IG: ${IG}`);
        } else {
            this.data.igList[IG]++;
            this.data.currentIGR[IG] = this.data.igList[IG];
        }
    }

    /**
     * Decrements the IGR for the matching IG. It looks for the IG in
     * Data.Questionnaire.igList and decrements the IGR. It also sets the same value of IGR to the IG
     * in Data.Questionnaire.currentIGR. So on doing LF.Utilities.(IG) it will get
     * the current available IGR which can be used. If the IGR is 1, it will not decrement to 0 as IGR
     * for looping always starts with 1.
     * This Utility function should be called as soon as loop event is deleted, so it decrements the IGR.
     * This function assumes that one loop event has already been deleted.
     * @param {String} IG an IG string.
     * @example
     * LF.Utilities.decrementIGR('Headache');
     */
    decrementIGR (IG) {
        if (!this.data.igList[IG]) {
            throw new Error(`Attempt failed to decrement IGR for IG: ${IG}`);
        } else {
            if (this.data.igList[IG] !== 1) {
                this.data.igList[IG]--;
            }
            this.data.currentIGR[IG] = this.data.igList[IG];
        }
    }

    /**
     * query answers collection returning an array of models based on query params.
     * @param {Object} params an object containing query parameters
     * @return {Array} an array of answer models matching the query params
     * @example
     * let results = LF.Utilities.queryAnswers({ where: { question_id:'NRS_DIARY_Q1' } });
     */
    queryAnswers (params) {
        let answersQuery = [],
            answersCollection = this.answers;

        if (params.where) {
            answersQuery = answersCollection.where(params.where);
        }

        return answersQuery;
    }

    /**
     * Query all answers with matching Question ID and returns array of answer models.
     * @param {String} id an ID string
     * @return {Array} an array of answer models matching the "ID"
     * @example
     * let results = LF.Utilities.queryAnswersByID('NRS_Q1');
     */
    queryAnswersByID (id) {
        let answersQuery = [],
            answersCollection = this.answers;

        answersCollection.each((model) => {
            if (model.get('question_id') === id) {
                answersQuery.push(model);
            }
        });

        return answersQuery;
    }

    /**
     * Query all answers with matching IG and returns array of answer models.
     * @param {String} IG an IG string
     * @return {Array} an array of answer models matching the "IG"
     * @example
     * let results = LF.Utilities.queryAnswersByIG('NRS');
     */
    queryAnswersByIG (IG) {
        let swAlias,
            answersQuery = [],
            answersCollection = this.answers;

        answersCollection.each((model) => {
            swAlias = model.get('SW_Alias').split('.');
            if (swAlias[0] === IG) {
                answersQuery.push(model);
            }
        });

        return answersQuery;
    }

    /**
     * Query all answers with matching IT and returns array of answer models.
     * @param {String} IT an IT string
     * @return {Array} an array of answer models matching the "IT"
     * @example
     * let results = LF.Utilities.queryAnswersByIT('PainLevel');
     */
    queryAnswersByIT (IT) {
        let swAlias,
            answersQuery = [],
            answersCollection = this.answers;

        answersCollection.each((model) => {
            swAlias = model.get('SW_Alias').split('.');
            if (swAlias[2] === IT) {
                answersQuery.push(model);
            }
        });

        return answersQuery;
    }

    /**
     * Query all answers with matching IGR and returns array of answer models.
     * @param {Number} IGR an IGR number
     * @return {Array} an array of answer models matching the "IGR"
     * @example
     * let results = LF.Utilities.queryAnswersByIGR(2);
     */
    queryAnswersByIGR (IGR) {
        let swAlias,
            answersQuery = [],
            answersCollection = this.answers;

        answersCollection.each((model) => {
            swAlias = model.get('SW_Alias').split('.');
            if (parseInt(swAlias[1], 10) === IGR) {
                answersQuery.push(model);
            }
        });

        return answersQuery;
    }

    /**
     * Query all answers with matching IG & IGR and returns array of answer models.
     * @param {String} IG an IG string
     * @param {Number} IGR an IGR number
     * @return {Array} an array of question models matching the "IG" and "IGR"
     * @example
     * let results = LF.Utilities.queryAnswersByIGAndIGR('NRS', 1);
     */
    queryAnswersByIGAndIGR (IG, IGR) {
        let swAlias,
            answersQuery = [],
            answersCollection = this.answers;

        answersCollection.each((model) => {
            swAlias = model.get('SW_Alias').split('.');
            if (swAlias[0] === IG && parseInt(swAlias[1], 10) === IGR) {
                answersQuery.push(model);
            }
        });

        return answersQuery;
    }

    /**
     * Query all answers with matching IT & IGR and returns array of answer models.
     * @param {String} IT an IT string
     * @param {Number} IGR an IGR number
     * @return {Array} an array of question models matching the "IT" and "IGR"
     * @example
     * let results = LF.Utilities.queryAnswersByITAndIGR('MedsToday', 1);
     */
    queryAnswersByITAndIGR (IT, IGR) {
        let swAlias,
            answersQuery = [],
            answersCollection = this.answers;

        answersCollection.each((model) => {
            swAlias = model.get('SW_Alias').split('.');
            if (parseInt(swAlias[1], 10) === IGR && swAlias[2] === IT) {
                answersQuery.push(model);
            }
        });

        return answersQuery;
    }

    /**
     * Query all answers with matching IG, IT & IGR and returns array of answer models.
     * @param {String} IG an IG string
     * @param {String} IT an IT string
     * @param {Number} IGR an IGR number
     * @return {Question[]} an array of question models matching the "IT" and "IGR"
     * @example
     * let results = LF.Utilities.queryAnswersByIGITAndIGR('Loop2', MedsToday', 1);
     */
    queryAnswersByIGITAndIGR (IG, IT, IGR) {
        let swAlias,
            answersQuery = [],
            answersCollection = this.answers;

        answersCollection.each((model) => {
            swAlias = model.get('SW_Alias').split('.');
            if (swAlias[0] === IG && parseInt(swAlias[1], 10) === IGR && swAlias[2] === IT) {
                answersQuery.push(model);
            }
        });

        return answersQuery;
    }

    /**
     * Query all QuestionViews with answers matching the IG & IGR and returns array of Question views.
     * @param {String} IG an IG string.
     * @param {Number} IGR an IGR number
     * @return {Array} an array containing all the matching Question Views with answers matching the IG & IGR.
     * @example
     * let results = LF.Utilities.queryQuestionViewsByIGAndIGR('NRS', 2);
     */
    queryQuestionViewsByIGAndIGR (IG, IGR) {
        let questionQuery = [];

        if (!this.data) {
            throw new Error(`Attempt failed to fetch QuestionViews for IG: ${IG} and IGR: ${IGR}`);
        } else {
            questionQuery = this.questionViews.filter((view) => {
                return view.ig === IG && view.igr === IGR;
            });
        }

        return questionQuery;
    }

    /**
     * Execute a query on an array of questions returning all matching questions by IG.
     * @param {String} IG an IG string.
     * @return {Array} an array containing all the matching questions by IG.
     * @example
     * let results = LF.Utilities.queryQuestionsByIG('NRS');
     */
    queryQuestionsByIG (IG) {
        let questionQuery = [];

        if (!this.data) {
            throw new Error(`Attempt failed to fetch QuestionViews for IG: ${IG}`);
        } else {
            questionQuery = this.questionViews.filter((view) => {
                return view.ig === IG;
            });
        }

        return questionQuery;
    }

    /**
     * Execute a query on an array of questions returning all matching questions view by IGR.
     * @param {Number} IGR an IGR number
     * @return {Array} an array containing all the matching questions by IGR.
     * @example
     * let results = LF.Utilities.queryQuestionsByIGR(2);
     */
    queryQuestionsByIGR (IGR) {
        let questionQuery = [];

        if (!this.data) {
            throw new Error(`Attempt failed to fetch QuestionViews for IGR: ${IGR}`);
        } else {
            questionQuery = this.questionViews.filter((view) => {
                return view.igr === IGR;
            });
        }

        return questionQuery;
    }

    /**
     * Gets the IGR from the master igList for the matching IG and returns a number. It looks for the IGR in
     * Data.Questionnaire.igList which is an Object which holds most recent IGRs for all IGs
     * (as key pair value) for the diary in progress.
     * @param {String} IG an IG string.
     * @return {Number} a number which is the most recent IGR for the matching IG.
     * @example
     * let results = LF.Utilities.getIGR('Headache');
     */
    getIGR (IG) {
        if (!_.isNumber(this.data.igList[IG])) {
            throw new Error(`Attempt failed to get IGR for IG: ${IG}`);
        } else {
            return this.data.igList[IG];
        }
    }

    /**
     * This function removes a loop event matching with IG & IGR passed.
     * A loop event includes all answers and all Question Views matching with IG & IGR.
     * This function doesn't decrement the IGR for the IG, so LF.Utilities.decrementIGR(IG)
     * has to be called after this function.
     * @param {String} IG an IG string.
     * @param {Number} IGR an IGR number.
     * @example
     * LF.Utilities.removeLoopEvent('NRS', 2);
     */
    removeLoopEvent (IG, IGR) {
        let answersToDelete,
            questionViewsToDelete,
            that = this;

        if (!this.data.igList[IG]) {
            throw new Error(`Attempt failed to remove Loop event for IG: ${IG} and IGR: ${IGR}`);
        } else {
            answersToDelete = this.queryAnswersByIGAndIGR(IG, IGR);
            questionViewsToDelete = this.queryQuestionViewsByIGAndIGR(IG, IGR);

            this.answers.remove(answersToDelete);

            _.each(questionViewsToDelete, (view) => {
                let filter = function (item) {
                    return item.ig === view.ig && item.igr === view.igr;
                };
                that.questionViews.removeIf(filter);
            });
        }
    }

    /**
     * This function changes IGR of all answers matching with IG and IGR to the IGR passed in.
     * This function also changes the igr on the Question views.
     * This function is very helpful when an IGR needs to be inserted in between.
     * LF.Utilities.makeIGRAvailable(IG, IGR) can be used to make room and then the available
     * IGR can be used to insert new at the available location.
     * @param {String} IG an IG string.
     * @param {Number} fromIGR an IGR number which needs to be changed.
     * @param {Number} toIGR an IGR number which needs to the new IGR.
     * @example
     * LF.Utilities.changeIGR('NRS', -1, 2);
     */
    changeIGR (IG, fromIGR, toIGR) {
        let questionViewsToUpdate,
            answersToUpdate,
            that = this;

        if (!this.data.igList[IG]) {
            throw new Error(`Attempt failed to change IGR for IG: ${IG} from IGR: ${fromIGR} to IGR: ${toIGR}`);
        } else {
            questionViewsToUpdate = this.queryQuestionViewsByIGAndIGR(IG, fromIGR);
            answersToUpdate = this.queryAnswersByIGAndIGR(IG, fromIGR);
            if (questionViewsToUpdate.length) {
                _.each(questionViewsToUpdate, (question) => {
                    question.igr = toIGR;
                });
            }
            if (answersToUpdate.length) {
                _.each(answersToUpdate, (answer) => {
                    that.answerSWAliasUpdate(answer, { IGR: toIGR });
                });
            }
        }
    }

    /**
     * This function shifts IGR by 1 (decrement) and follows the sequence until all looping
     * events' IGR are changed. IGR are changed on the answer record (SWAlias) and also on Question Views.
     * This function takes IGR as an argument with IG. The IGR passed is assumed to be deleted and
     * all other IGR occurred after are bumped up a number. So for example we have IGR 1, 2, 4, 5 and 6, then 3
     * is missing, we need 4 to become 3, 5 to become 4 and so on. This can be done with this function.
     * This function is very useful an event is deleted and all other event occurred after needs to be reordered.
     * @param {String} IG an IG string.
     * @param {Number} IGR an IGR number which is assumed to be deleted no answer exists for this IGR.
     * @example
     * LF.Utilities.orderIGR('NRS', 2);
     */
    orderIGR (IG, IGR) {
        let questionViewsToUpdate,
            answersToUpdate,
            ordinal = IGR,
            that = this;

        if (!this.data.igList[IG]) {
            throw new Error(`Attempt failed to order IGRs for IG: ${IG} and IGR: ${IGR}`);
        } else {
            questionViewsToUpdate = this.queryQuestionViewsByIGAndIGR(IG, ordinal + 1);
            answersToUpdate = this.queryAnswersByIGAndIGR(IG, ordinal + 1);
            while (questionViewsToUpdate.length || answersToUpdate.length) {
                _.each(questionViewsToUpdate, (question) => {
                    question.igr = ordinal;
                });
                _.each(answersToUpdate, (answer) => {
                    that.answerSWAliasUpdate(answer, { IGR: ordinal });
                });
                ordinal++;
                questionViewsToUpdate = this.queryQuestionViewsByIGAndIGR(IG, ordinal + 1);
                answersToUpdate = this.queryAnswersByIGAndIGR(IG, ordinal + 1);
            }
        }
    }

    /**
     * This function shifts IGR by 1 (increment) and follows the sequence until all looping
     * events' IGR are changed. IGR are changed on the answer record (SWAlias) and also on Question Views.
     * This function takes IGR as an argument with IG. The IGR passed in is the IGR which needs to become available.
     * So for example we have IGR 1, 2, 3, 4 and 5, and if we need 3 to become available
     * then this can be done with this function and the resulting IGR will be 1, 2, 4, 5 and 6.
     * Now since 3 is available, new IGR can take this spot.
     * NOTE: This function doesn't increment the IGR for the IG in the master IG list, so LF.Utilities.incrementIGR(IG)
     * has to be called after this function.
     * @param {String} IG an IG string.
     * @param {Number} IGR an IGR number which needs to become available.
     * @example
     * LF.Utilities.makeIGRAvailable('NRS', 2);
     */
    makeIGRAvailable (IG, IGR) {
        let questionViewsToUpdate,
            answersToUpdate,
            lastIGR,
            that = this;

        if (!this.data.igList[IG]) {
            throw new Error(`Attempt failed to make IGR available for IG: ${IG} and IGR: ${IGR}`);
        } else {
            lastIGR = this.data.igList[IG];
            while (lastIGR >= IGR) {
                questionViewsToUpdate = this.queryQuestionViewsByIGAndIGR(IG, lastIGR);
                answersToUpdate = this.queryAnswersByIGAndIGR(IG, lastIGR);
                _.each(questionViewsToUpdate, (question) => {
                    question.igr = lastIGR + 1;
                });
                _.each(answersToUpdate, (answer) => {
                    that.answerSWAliasUpdate(answer, { IGR: lastIGR + 1 });
                });

                lastIGR--;
            }
        }
    }

    /**
     * This function calls LF.Utilities.makeIGRAvailable to make IGR available where
     * the new IGR needs to be inserted (shifts all IGRs). This function assumes that the new IGR is already
     * been answered and in memory with IGR as a placeholder. For example the IGR for new
     * event can be -1. After making the space this function just calls LF.Utilities.changeIGR
     * and change the IGR to one just made available.
     * @param {String} IG an IG string.
     * @param {Number} fromIGR an IGR number which needs to be inserted.
     * @param {Number} toIGR an IGR which will be made available for new IGR to be inserted.
     * @example
     * LF.Utilities.insertIGR('NRS', -1, 3);
     */
    insertIGR (IG, fromIGR, toIGR) {
        this.makeIGRAvailable(IG, toIGR);
        this.changeIGR(IG, fromIGR, toIGR);
    }

    // noinspection JSMethodCanBeStatic
    /**
     * Modify an answer model's swAlias.
     * @param {Object} answer a widget answer model.
     * @param {Object} obj an object containing swAlias overwrite properties. (IG,IGR,IT)
     * @return {Object} a reference to answer model updated
     * @example
     * let results = LF.Utilities.answerSWAliasUpdate(Data.Questionnaire.questionViews[0].widget.answers.at(0), { IT: 'TEST_NRS_Q1' });
     */
    answerSWAliasUpdate (answer, obj) {
        let swAlias = answer.get('SW_Alias').split('.'),
            swAliasObj = obj || {};

        swAlias[0] = swAliasObj.IG || swAlias[0];
        swAlias[1] = swAliasObj.IGR || swAlias[1];
        swAlias[2] = swAliasObj.IT || swAlias[2];

        answer.set('SW_Alias', swAlias.join('.'));

        return answer;
    }

    /**
     * This function calls removeLoopEvent to remove the event for passed IGR.
     * Once removing is done, orderIGR is called to shift IGR and make all IGRs
     * in a sequence (filling up the gap).
     * @param {String} IG an IG string.
     * @param {Number} IGR an IGR number that needs to be removed.
     * @example
     * deleteIGRAndOrder('NRS', 3);
     */
    deleteIGRAndOrder (IG, IGR) {
        this.removeLoopEvent(IG, IGR);
        this.orderIGR(IG, IGR);
    }

    /**
     * This function add an Answer model to Data.Questionnaire.data.answers collections.
     * This function doesn't create any QuestionViews, but just Answer model and save it in
     * Data.Questionnaire.data.answers and on completion of the diary, saves it to the Answers
     * table. If an answer already exists (matched with IT and IGR), then this function just
     * change the response to that answer and doesn't add another Answer model.
     * There are several pieces required for this function to run and not throw an error. They are
     * question_id, questionnaire_id, response, IG, IGR, and IT and they should be in an Object.
     * @param {Object} params an object containing all required properties for an Answer.
     * @example
     * addIT({
     *     question_id : 'SOME_Q2',
     *     questionnaire_id : 'Loop2',
     *     response : '5',
     *     IG : 'Loop2A',
     *     IGR : 1
     *     IT : 'MedsToday'
     * });
     */
    addIT (params) {
        let model,
            answer;

        if (params.question_id != null &&
            params.questionnaire_id != null &&
            params.response != null &&
            params.IG != null &&
            params.IGR != null &&
            params.IT != null) {
            answer = this.queryAnswersByIGITAndIGR(params.IG, params.IT, params.IGR);

            // if answer already in collection, just modify the response
            if (answer.length) {
                answer[0].set('response', String(params.response));
            } else {
                model = new this.Answer({
                    question_id: params.question_id,
                    questionnaire_id: params.questionnaire_id,
                    response: String(params.response),
                    SW_Alias: `${params.IG}.${params.IGR}.${params.IT}`
                });
                this.answers.add(model);
            }
        } else {
            throw new Error('Attempt failed to insert an IT.');
        }
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * This function removes answer from Data.Questionnaire.data.answers collections with matching IT and IGR
     * This function doesn't delete any QuestionViews for matching IT and IGR.
     * This function is very helpful when removing an IT which was added as hidden IT and no views were
     * created for that IT.
     * @param {String} IG an IG string.
     * @param {String} IT an IT string.
     * @param {Number} IGR an IGR number that needs to be removed.
     * @example
     * LF.Utilities.removeIT('MedsToday', 0);
     */
    removeIT (IG, IT, IGR) {
        let answer;

        answer = this.queryAnswersByIGITAndIGR(IG, IT, IGR);
        this.data.answers.remove(answer);
    }

    /**
     * This function copies all the answers and question views with matching IG using given
     * fromIGR and assign an IGR to all answers and question view using given fromIGR.
     * Since Backbone cloning for views is not a good way to fulfill the requirement, this
     * function just create new QuestionViews and push in all answers for any widget on them.
     * If there are any ITs with out QuestionViews (Hidden ITs), they are also cloned and added
     * to the Answers collection.
     * This function will be very helpful and allow us to copy an IGR to be able to work on (edit mode)
     * and remove entirely if editing was abandoned thus preserving the original answers.
     * @param {String} IG an IG string.
     * @param {Number} fromIGR an IGR number which needs to be copied.
     * @param {Number} toIGR an IGR number which will be the resulting IGR for copied version.
     * @example
     * LF.Utilities.changeIGR('NRS', 2, -1);
     */
    copyIGR (IG, fromIGR, toIGR) {
        let questionViewsToCopy,
            answersToCopy,
            currentIGR,
            viewsPushed = [],
            that = this;

        if (!this.data.igList[IG]) {
            throw new Error(`Attempt failed to copy IGR for IG: ${IG} from IGR: ${fromIGR} to IGR: ${toIGR}`);
        } else {
            currentIGR = this.getCurrentIGR(IG);
            this.setCurrentIGR(IG, toIGR);
            questionViewsToCopy = this.queryQuestionViewsByIGAndIGR(IG, fromIGR);
            answersToCopy = this.queryAnswersByIGAndIGR(IG, fromIGR);

            // iterate through question views and create duplicates
            _.each(questionViewsToCopy, (question) => {
                let view = new LF.View.QuestionView({
                    id: question.id,
                    screen: question.screenID,
                    model: question.model,
                    parent: question.parent,
                    mandatory: question.widget.mandatory,
                    ig: IG,
                    igr: toIGR
                });

                // preserve the status of question
                view.widget.completed = question.widget.completed;

                // if the answer was skipped, clone the skipped IT
                if (question.widget.skipped) {
                    view.widget.skipped = that.answerSWAliasUpdate(question.widget.skipped.clone(), { IGR: toIGR });
                }

                if (question.widget.dateObj) {
                    view.widget.dateObj = question.widget.dateObj;
                }

                // Only for Date and Time picker, preserve the localized strings
                if (question.widget.localizedDate) {
                    view.widget.localizedDate = question.widget.localizedDate;
                }

                if (question.widget.localizedTime) {
                    view.widget.localizedTime = question.widget.localizedTime;
                }

                // Push to the array of all Question Views
                this.questionViews.push(view);
                viewsPushed.push(view);
            });

            // iterate through each answers and push them to Widgets on QuestionViews.
            _.each(answersToCopy, (answer) => {
                let view = viewsPushed.filter((view) => {
                    return view.id === answer.get('question_id');
                })[0];

                // if the question has a view, push the answer on the widget
                if (view) {
                    let rsp = answer.get('response');
                    view.widget.addAnswer({ response: rsp });
                } else {
                    // Hidden IT, must be cloned and added to Answers collection
                    that.data.answers.add(that.answerSWAliasUpdate(answer.clone(), { IGR: toIGR }));
                }
            });

            this.setCurrentIGR(IG, currentIGR);
        }
    }

    /**
     * This function removes all screen ID's from the Screen Stack which are associated
     * with the IG passed. The intention for this function is to be called before the Review Screen
     * is pushed on the stack. If this is called after the review screen is rendered then the screen ID
     * for the review screen will also be removed from the stack. The best place to use this function
     * is from a branching function which is called on navigation and before the new screen is rendered.
     * Also an ELF rule could be a better place for calling this function.
     * @param {String} IG an IG string.
     * @example
     * yourView.clearLoopScreenFromStack('NRS');
     */
    clearLoopScreenFromStack (IG) {
        let screenStack,
            loopScreens = [],
            mappedQuestions = {};

        screenStack = this.screenStack;

        // map all questions to their respective screen ID
        _.each(this.data.screens, (screen) => {
            _.each(screen.get('questions'), (question) => {
                mappedQuestions[question.id] = screen.id;
            });
        });

        // get all screens for the IG passed in
        loopScreens = this.data.questions.models.filter((question) => {
            return question.get('IG') === IG;
        }).map((question) => {
            return mappedQuestions[question.get('id')];
        });

        // remove all matching screen ID from the stack
        screenStack.removeIf((screen) => {
            let index = loopScreens.indexOf(screen);
            return index !== -1;
        });
    }

    /**
     * Clean up this view and all questions within this view.
     * @returns {Q.Promise<void>}
     */
    remove () {
        return this.questionViews.reduce((chain, questionView) => {
            return chain
            .then(() => questionView.remove());
        }, Q())
        .then(() => {
            return super.remove();
        });
    }

    /**
     * Helper function to get next instance ordinal.
     * @returns {Object}
     */
    static getInstanceOrdinal () {
        let result = lStorage.getItem('instance_ordinal');
        result = +(result || 0) + 1;
        lStorage.setItem('instance_ordinal', result);
        return result;
    }
}
