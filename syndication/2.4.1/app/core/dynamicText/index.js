import DynamicText from 'core/classes/DynamicText';
import * as affidavit from './affidavit';
import * as am1PlusDiary from './am1PlusDiary';
import * as clinicalTrial from './clinicalTrial';
import * as currentDate from './currentDate';
import * as editPatient from './editPatient';
import * as getParticipantCode from './getParticipantCode';
import * as getPatientIDForDuplicate from './getPatientIDForDuplicate';
import * as getQuestionnaireAnswers from './getQuestionnaireAnswers';
import * as imageFolder from './imageFolder';
import * as newPatient from './newPatient';
import * as selectedTimeZone from './selectedTimeZone';
import * as startForm from './startForm';
import * as studyAlias from './studyAlias';
import * as skipVisit from './skipVisit';
import * as timeZoneChangeRestartCountdown from './timeZoneChangeRestartCountdown';
import * as patientIDRange from './patientIDRange';
import * as appHeader from './appHeader';
import * as getSupportedBrowsers from './getSupportedBrowsers';

const dynamicText = new DynamicText();

dynamicText.addFromObjects(
    affidavit,
    am1PlusDiary,
    clinicalTrial,
    currentDate,
    imageFolder,
    newPatient,
    studyAlias,
    startForm,
    skipVisit,
    getParticipantCode,
    editPatient,
    getPatientIDForDuplicate,
    selectedTimeZone,
    getQuestionnaireAnswers,
    timeZoneChangeRestartCountdown,
    patientIDRange,
    appHeader,
    getSupportedBrowsers
);

export default dynamicText;
