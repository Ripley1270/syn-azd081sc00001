import { getCurrentProtocol } from 'core/Helpers';

export const appHeader = {
    id: 'appHeader',
    evaluate: () => {
        let currentProtocol = getCurrentProtocol();

        return `${LF.StudyDesign.clientName}  - ${currentProtocol}`;
    },
    screenshots: {
        values: ['ERT - TestStudy']
    }
};
