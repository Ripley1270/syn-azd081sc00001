export const getSupportedBrowsers = {
    id: 'getSupportedBrowsers',
    evaluate: () => {
        return LF.StudyDesign.supportedBrowsers.map((elem) => {
            return elem.name;
        }).join(',');
    },
    screenshots: {
        values: ['Chrome, Edge']
    }
};
