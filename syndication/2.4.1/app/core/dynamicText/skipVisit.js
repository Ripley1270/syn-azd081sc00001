import Data from 'core/Data';

/**
 * Get the list of names of skipped visits.
 */
export const skippedVisits = {
    id: 'skippedVisits',
    evaluate: () => {
        let list = '';

        if (Data.skippedVisits) {
            Data.skippedVisits.forEach(visit => list += `${visit.get('name')}<br/>`);
        }

        return list;
    },
    screenshots: {
        getValues () {
            return LF.getStrings('VISIT_1')
            .then((text) => {
                return [
                    `${text}<br />`
                ];
            });
        }
    }
};
