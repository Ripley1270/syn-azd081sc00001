import Context from 'core/models/Context';

/**
 * Single instance of Context data model.
 */
let context = null;

/**
 * Factory method that returns the context singleton
 * @returns {Object} Context current context
 */
function CurrentContext () {
    return context;
}

/**
 * Initialize the current context singleton. MUST be run after StudyDesign is set up.
 * @param {Object} options options to pass to Context model contstructor
 * @returns {Object} Context current context
 */
CurrentContext.init = (options) => {
    context = new Context(options);

    // Here just in case, hopefully we can  remove this.
    LF.content = context;

    return context;
};

export default CurrentContext;
