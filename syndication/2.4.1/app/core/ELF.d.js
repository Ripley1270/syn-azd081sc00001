// This file is not consumed by the application runtime.
// It exists purely for documentation purposes.

/**
 * @typedef {Object} ELF~Rule
 * @property {string} id - The ID of the ELF rule.
 * @property {(string|Array<string>)} trigger - An ELF event, or list of ELF events, that will trigger evaluation of the rule.
 * @property {number} [salience=1] - Determines when the rule is evaluated if more than one rule is matched per trigger.  The higher the number, the sooner the rule will evaluate.
 * @property {number} [deferSubsequentEvals] - If set prevents evaluation of the rule for subsequent triggers within a given time in milliseconds.
 * @property {(ELF~Expression|Array<ELF~Expression>)} [evaluate=true] - Determines if the rule should be resolved, or rejected.
 * @property {Array<ELF~Action>} [resolve] - A chain of actions to execute upon the evaluate property resolving as true.
 * @property {Array<ELF~Action>} [reject] - A chain of actions to execute upon the evaluate property resolving as false.
 * @example
 * {
 *     id: 'NewUserSave',
 *     // Evaluate of the rule is triggered upon navigation on the New User questionnaire's affidavit screen.
 *     trigger: 'QUESTIONNAIRE:Navigate/New_User_SitePad/AFFIDAVIT',
 *     // Resolves only when the direction of the navigation is forward.
 *     evaluate: 'isDirectionForward',
 *     // resolve chain is executed when the result of evaluate returns true.
 *     resolve: [{
 *         // Display a please wait spinner.
 *         action: 'displayMessage'
 *     }, {
 *         // Save the new user.
 *         action: 'saveNewUser'
 *     }]s
 * }
 */

/**
 * @typedef {Object} ELF~ActionResult
 * @property {boolean} [preventDefault=false] - If set to true, will prevent any default actions from executing after all rules have executed.
 * @property {boolean} [stopActions=false] - Stops any further actions in the immediate chain from executing.
 * @property {boolean} [stopRules=false] - Stops any further rules from executing in the trigger sequence.
 */

/**
 * @typedef {Function} ELF~ActionFunction
 * @param {Object} data - Data passed into the action via rule.
 * @returns {Q.Promise<ELF~ActionResult>}
 * @example
 * action: (stopActions) => {
 *     // Get the network status of the device.
 *     return isOnline().then(result => {
 *         // If online...
 *         if (result) {
 *             // Transmit all stored records.
 *             return transmitAll();
 *         } else {
 *             // Otherwise, prevent any further actions from executing, of the stopActions variable
 *             // was configured as true.
 *             return { stopActions };
 *         }
 *     });
 * }
 */

/**
 * @typedef {Object} ELF~ActionObject
 * @property {(ELF~ActionFunction|string)} action - A function literal to execute, or a string reference to an action registered to ELF.actions.
 * @property {any} [data] - Data passed into the action via configuration.
 * @example
 * // The following configuration will navigate to the ToolboxView.
 * {
 *     action: 'navigateTo',
 *     data: 'toolbox'
 * }
 * @example
 * // The following configuration returns {Q.Promise<ELF~ActionResult} that will stop any further rules from executing.
 * {
 *     action: () => {
 *         return Q({ stopRules: true });
 *     }
 * }
 */

/**
 * @typedef {(ELF~ActionObject|ELF~ActionFunction)} ELF~Action
 */

/**
 * @typedef {(ELF~ExpressionObject|ELF~ExpressionFunction|string|boolean)} ELF~Expression
 */

/**
 * A function that resolves either to true or false.
 * @typedef {Function} ELF~ExpressionFunction
 * @param {any} [input] - Input passed into the action via configuration.
 * @returns {(boolean|Q.Promise<boolean>)}
 * @example
 * function () {
 *    // Returns true if the device is running on iOS.
 *    return Q(LF.Wrapper.platform === 'ios');
 * }
 */

/**
 * A configuration object used to pass data into ExpressionFunctions.
 * @typedef {Object} ELF~ExpressionObject
 * @property {(boolean|string|ExpressionFunction)} expression - The expression to evaluate.  If a string, looks for the ExpressionFunction registered to ELF.expression() by name.
 * @property {any} [input] - Data to pass into the ExpressionFunction as its first argument.
 * @example
 * {
 *    // This expression will resolve to true if today is either Monday, Wednesday or Friday.
 *    expression : 'isDayOfTheWeek',
 *    input      : { days : ['Monday', 'Wednesday', 'Friday'] }
 * }
 */


// //////////////////////////////////////////////////////////////////////
//                         Actions                                    //
// //////////////////////////////////////////////////////////////////////

/**
 * @namespace ELF.actions
 * @memberOf ELF
 * @description
 * Each of the actions described below is added to the ELF.actions list with a string-name that exactly matches the name. Thus, where ‘actions’ are expected in the ELF logic, that string-name can be used and will be looked-up in that ELF.actions table.
 *
 * Every action below is also an exported ES6 module, and thus can be imported an invoked as well. *Some* actions are also added to the ELF.expression table and can be used wherever ELF expressions are expected.
 *
 * And remember, wherever an ‘action’ is expected, the creator can provide an immediate function or scope-known function as well (that is, the action does *not* have to one that has been defined in the ELF.actions table).
 *
 * Each ELF action is passed a configurable parameter/object.  The data parameter is an assembly (extend) of any data in the action “data” section plus the data from the actual ELF.trigger call.  Each action must return a promise (Q.Promise); No value is expected to be returned by the promise, but {@link ELF~ActionResponse} is permitted.  If the promise is not resolved, the ELF.trigger will never complete. Sometimes this is a useful redirection capability.
 */

/**
 * @namespace ELF.actions/sitepad
 * @memberOf ELF
 * @description
 * The following actions are restricted to the eCoa Tablet (SitePad) application. <br />
 * _For more information about ELF actions, see {@link ELF.actions}._
 */

/**
 * @namespace ELF.actions/logpad
 * @memberOf ELF
 * @description
 * The following actions are restricted to the eCoa Handheld (LogPad) application. <br />
 * _For more information about ELF actions, see {@link ELF.actions}._
 */

/**
 * @namespace ELF.actions/web
 * @memberOf ELF
 * @description
 * The following actions are restricted to the eCoa Web application. <br />
 * _For more information about ELF actions, see {@link ELF.actions}._
 */

// //////////////////////////////////////////////////////////////////////
//                         Triggers                                   //
// //////////////////////////////////////////////////////////////////////

/**
 * @namespace ELF.triggers
 * @memberOf ELF
 * @description
 * The main reader of this document is the application developer who wants to catch system triggers. However, it is still useful to understand the trigger process (for more core engineers, but also application engineers can and do call triggers as well).
 *
 * When the core or other code knows a condition that it wants to announce, it will call ELF.trigger() stating the trigger name.  ELF.trigger() returns a Promise.  Typically the caller should wait for the trigger to complete before marching onwards (although this is not a requirement and many event announcement systems simply announce-and-continue).
 *
 * While ELF.trigger() returns a Promise, there are several important behaviors and known-problems that need to be understood by the developer:
 * - There is NO indication on return whether the ‘expression’ (the ‘evaluate’ clause of the rule(s)) determined success (resolve) or failure (reject). (Remember, there could be several rules catching this trigger and each might have a different opinion and different set of activities).  The assumption is that the rule(s) decided completely on how to handle this trigger, both for success and for failure.
 *
 * - The reject path of the ELF.trigger() call MIGHT get called.  It will get called with a traditional error object IF a real error/exception occurred (that is, a programming error, not the rule deciding false or reject).  This should be very rare.  BUT, unfortunately, it will also get called if there were no receivers for this trigger. Sometimes the caller cares that no one listened and might decide to advance differently.  But if someone did listen, the caller has no idea how that listener handled the situation so has to somehow proceed neutrally.
 *
 * #### Trigger Parameters
 *
 * There are three parameters passed into the ELF.trigger() function:
 *  - triggerName
 *  - data
 *  - context
 *
 * If ‘data’ is provided, it will be passed onto any/all actions that are eventually called. If the invocation of the action (in the rule definition) also has data specified, these two are merged/extended.  (Actions all accept a parameter although many do not utilize it -- see the Actions sections for details on their parameters).
 *
 * The ‘context’ is optional and if provided then is the context (the ‘this’) that will be active when the actions are called.  Depending on the trigger/action, this sometimes can be very valuable (such as the object of an event or some other system activity).  Again, see the Actions section for details.
 *
 * ### Quick Links
 * <hr />
 * #### ACTIVATION
 * - {@link ELF.triggers~ACTIVATION:SubjectDataReceived ACTIVATION:SubjectDataReceived}
 * - {@link ELF.triggers~ACTIVATION:Transmit ACTIVATION:Transmit}
 *
 * #### ALARM
 * - {@link ELF.triggers~ALARM:Trigger:scheduleId ALARM:Trigger:scheduleId}
 *
 * #### APPLICATION
 * - {@link ELF.triggers~APPLICATION:DatabaseError APPLICATION:DatabaseError}
 * - {@link ELF.triggers~APPLICATION:EncryptionError APPLICATION:EncryptionError}
 * - {@link ELF.triggers~APPLICATION:DecryptionError APPLICATION:DecryptionError}
 *
 * #### DYNAMICTEXT
 * - {@link ELF.triggers~DYNAMICTEXT:Trigger DYNAMICTEXT:Trigger}
 *
 * #### HISTORICALDATASYNC
 * - {@link ELF.triggers~HISTORICALDATASYNC:Received/:collection HISTORICALDATASYNC:Received/:collection}
 *
 * #### LOCALSTORAGE
 * - {@link ELF.triggers~LOCALSTORAGE:NoSupport LOCALSTORAGE:NoSupport}
 *
 * #### LOGIN
 * - {@link ELF.triggers~LOGIN:Rendered LOGIN:Rendered}
 *
 * #### NAVIGATE
 * - {@link ELF.triggers~NAVIGATE::controller/:route NAVIGATE::controller/:router}
 *
 * #### QUESTIONNAIRE
 * - {@link ELF.triggers~QUESTIONNAIRE:Canceled/:questionnaireId QUESTIONNAIRE:Canceled/:questionnaireId}
 * - {@link ELF.triggers~QUESTIONNAIRE:QuestionnaireTimeout/:questionnaireId QUESTIONNAIRE:QuestionnaireTimeout/:questionnaireId}
 * - {@link ELF.triggers~QUESTIONNAIRE:SessionTimeout/:questionnaireId QUESTIONNAIRE:SessionTimeout/:questionnaireId}
 * - {@link ELF.triggers~QUESTIONNAIRE:Rendered/:questionnaireId QUESTIONNAIRE:Rendered/:questionnaireId}
 * - {@link ELF.triggers~QUESTIONNAIRE:Open/:questionnaireId QUESTIONNAIRE:Open/:questionnaireId}
 * - {@link ELF.triggers~QUESTIONNAIRE:Before/:questionnaireId/:screenId  QUESTIONNAIRE:Before/:questionnaireId/:screenId }
 * - {@link ELF.triggers~QUESTIONNAIRE:Displayed/:questionnaireId/:screenId QUESTIONNAIRE:Displayed/:questionnaireId/:screenId }
 * - {@link ELF.triggers~QUESTIONNAIRE:SkipQuestions/:questionnaireId QUESTIONNAIRE:SkipQuestions/:questionnaireId}
 * - {@link ELF.triggers~QUESTIONNAIRE:Completed/:questionnaireId QUESTIONNAIRE:Completed/:questionnaireId}
 * - {@link ELF.triggers~QUESTIONNAIRE:Saved/:questionnaireId QUESTIONNAIRE:Saved/:questionnaireId}
 * - {@link ELF.triggers~QUESTIONNAIRE:Navigate/:questionnaireId/:screenId  QUESTIONNAIRE:Navigate/:questionnaireId/:screenId }
 * - {@link ELF.triggers~QUESTIONNAIRE:Backout/:questionnaireId QUESTIONNAIRE:Backout/:questionnaireId}
 * - {@link ELF.triggers~QUESTIONNAIRE:Transmit/:questionnaireId QUESTIONNAIRE:Transmit/:questionnaireId}
 * - {@link ELF.triggers~QUESTIONNAIRE:Answered/:questionnaireId/:questionId QUESTIONNAIRE:Answered/:questionnaireId/:questionId}
 *
 * #### TERMINATION
 * - {@link ELF.triggers~Termination Termination}
 *
 * #### TIMESLIP
 * - {@link ELF.triggers~TIMESLIP:Retry TIMESLIP:Retry}
 *
 * #### TRANSMIT
 * - {@link ELF.triggers~TRANSMIT:Duplicate/Subject TRANSMIT:Duplicate/Subject}
 * - {@link ELF.triggers~TRANSMIT:Duplicate/User TRANSMIT:Duplicate/User}
 *
 * #### USERS
 * - {@link ELF.triggers~USER:ChangeCredentials/:userId USER:ChangeCredentials/:userId}
 *
 * #### WIDGET
 * - {@link ELF.triggers~WIDGET:Validation WIDGET:Validation}
 * - {@link ELF.triggers~WIDGET:Rendering  WIDGET:Rendering }
 */

// //////////////////////////////////////////////////////////////////////
//                       Triggers - Core                              //
// //////////////////////////////////////////////////////////////////////

/**
 * @description
 * An {@link ELF} event triggered when a database error has occurred<br />
 * __Default Action__: Log the error to the console and reject the associated promise.
 * @typedef {Object} ELF.triggers~APPLICATION:DatabaseError
 * @property {string} trigger - APPLICATION:DatabaseError
 * @property {Backbone.sync} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Error} params.err - The error thrown by the Database Access Layer.
 * @example
 * ELF.trigger('APPLICATION:DatabaseError', { err }, this);
 * @example
 * ELF.rules.add({
 *    id: 'databaseError',
 *    trigger: 'APPLICATION:DatabaseError',
 *    resolve: [{ action: 'refresh' }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when an encryption error is thrown by a model.<br />
 * __Default Action__: Log the error to the console.
 * @typedef {Object} ELF.triggers~APPLICATION:EncryptionError
 * @property {string} trigger - APPLICATION:EncryptionError
 * @property {Backbone.Model} context - The model attempting to encrypt itself.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Error} params.err - The error thrown.
 * @example
 * ELF.trigger('APPLICATION:EncryptionError', { err }, this);
 * @example
 * ELF.rules.add({
 *    id: 'uninstall',
 *    trigger: [
 *        'APPLICATION:EncryptionError',
 *        'APPLICATION:DecryptionError'
 *    ],
 *    resolve: [{ action: 'uninstall' }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a decryption error is thrown by a model.<br />
 * __Default Action__: Log the error to the console.
 * @typedef {Object} ELF.triggers~APPLICATION:DecryptionError
 * @property {string} trigger - APPLICATION:DecryptionError
 * @property {Backbone.Model} context - The model attempting to encrypt itself.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Error} params.err - The error thrown.
 * @example
 * ELF.trigger('APPLICATION:DecryptionError', { err }, this);
 * @example
 * ELF.rules.add({
 *    id: 'uninstall',
 *    trigger: [
 *        'APPLICATION:EncryptionError',
 *        'APPLICATION:DecryptionError'
 *    ],
 *    resolve: [{ action: 'uninstall' }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a questionnaire has been canceled. <br />
 * __Default Action__: Navigate to the dashboard view.
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:Canceled/:questionnaireId
 * @property {string} trigger - QUESTIONNAIRE:Canceled/:questionnaireId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.questionnaire - The ID of the questionnaire canceled.
 * @example
 * ELF.trigger('QUESTIONNAIRE:Canceled/Activate_User', { questionnaire: 'Activate_User' }, this);
 * @example
 * ELF.rules.add({
 *    id: 'ActivateUserCanceled',
 *    trigger: 'QUESTIONNAIRE:Canceled/Activate_User',
 *    resolve: [{
 *        action: 'navigateTo',
 *        data: 'site-users'
 *    }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a subject/user attempts to back out of a questionnaire. <br />
 * __Default Action__: Navigate to the dashboard view.
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:Backout/:questionnaireId
 * @property {string} trigger - QUESTIONNAIRE:Backout/:questionnaireId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.questionnaire - The ID of the questionnaire.
 * @example
 * ELF.trigger('QUESTIONNAIRE:Backout/Activate_User', { questionnaire: 'Activate_User' }, this);
 * @example
 * ELF.rules.add({
 *    id: 'ActivateUserBackout',
 *    trigger: 'QUESTIONNAIRE:Backout/Activate_User',
 *    resolve: [{
 *        action: 'navigateTo',
 *        data: 'site-users'
 *    }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered after a questionnaire has been rendered to the screen.<br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:Rendered/:questionnaireId
 * @property {string} trigger - QUESTIONNAIRE:Rendered/:questionnaireId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.questionnaire - The ID of the questionnaire rendered.
 * @example
 * ELF.trigger('QUESTIONNAIRE:Rendered/Activate_User', { questionnaire: 'Activate_User' }, this);
 * @example
 * ELF.rules.add({
 *    id: 'ActivateUserRendered',
 *    trigger: 'QUESTIONNAIRE:Rendered/Activate_User',
 *    resolve: [{ .... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered after a questionnaire has opened.<br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:Open/:questionnaireId
 * @property {string} trigger - QUESTIONNAIRE:Open/:questionnaireId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Empty object literal.
 * @example
 * ELF.trigger('QUESTIONNAIRE:Open/Activate_User', {}, this);
 * @example
 * ELF.rules.add({
 *    id: 'ActivateUserOpen',
 *    trigger: 'QUESTIONNAIRE:Open/Activate_User',
 *    resolve: [{ .... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered prior to a questionnaire screen rendering.<br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:Before/:questionnaireId/:screenId
 * @property {string} trigger - QUESTIONNAIRE:Before/:questionnaireId/:screenId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.questionnaire - The ID of the questionnaire.
 * @property {Subject} params.subject - The model of the subject taking the questionnaire.
 * @property {Visit} params.visit - The current visit record.
 * @example
 * ELF.trigger('QUESTIONNAIRE:Before/Activate_User/ACTIVATE_USER_S_1', {
 *     questionnaire,
 *     subject,
 *     visit
 * }, this);
 * @example
 * ELF.rules.add({
 *    id: 'BeforeActivateUserS1',
 *    trigger: 'QUESTIONNAIRE:Before/Activate_User/ACTIVATE_USER_S_1',
 *    resolve: [{ .... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered after a questionnaire screen has rendered.<br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:Displayed/:questionnaireId/:screenId
 * @property {string} trigger - QUESTIONNAIRE:Displayed/:questionnaireId/:screenId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.questionnaire - The ID of the questionnaire.
 * @example
 * ELF.trigger('QUESTIONNAIRE:Displayed/Activate_User/ACTIVATE_USER_S_1', {
 *     questionnaire
 * }, this);
 * @example
 * ELF.rules.add({
 *    id: 'DisplayedActivateUserS1',
 *    trigger: 'QUESTIONNAIRE:Displayed/Activate_User/ACTIVATE_USER_S_1',
 *    resolve: [{ .... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered after attempting to skip questions on a screen.<br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:SkipQuestions/:questionnaireId
 * @property {string} trigger - QUESTIONNAIRE:SkipQuestions/:questionnaireId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Screen} params.screen - The model of the current screen being skipped.
 * @example
 * ELF.trigger('QUESTIONNAIRE:SkipQuestions/Activate_User', {
 *     screen
 * }, this);
 * @example
 * ELF.rules.add({
 *    id: 'SkipQuestionsActivateUserS1',
 *    trigger: 'QUESTIONNAIRE:SkipQuestions/Activate_User',
 *    resolve: [{ .... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered after a questionnaire has been completed, but not yet saved to the database.<br />
 * __Default Action__: Save the questionnaire.
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:Completed/:questionnaireId
 * @property {string} trigger - QUESTIONNAIRE:Completed/:questionnaireId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Screen} params.questionnaire - The ID of the completed questionnaire.
 * @example
 * ELF.trigger('QUESTIONNAIRE:Completed/Activate_User', {
 *     screen
 * }, this);
 * @example
 * ELF.rules.add({
 *    id: 'ActivateUserCompleted',
 *    trigger: 'QUESTIONNAIRE:Completed/Activate_User',
 *    resolve: [{ .... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered after a questionnaire has been saved to the database and queued for transmission.<br />
 * __Default Action__: Save the questionnaire.
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:Saved/:questionnaireId
 * @property {string} trigger - QUESTIONNAIRE:Saved/:questionnaireId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Screen} params.questionnaire - The ID of the saved questionnaire.
 * @example
 * ELF.trigger('QUESTIONNAIRE:Saved/Activate_User', {
 *     screen
 * }, this);
 * @example
 * ELF.rules.add({
 *    id: 'ActivateUserSaved',
 *    trigger: 'QUESTIONNAIRE:Saved/Activate_User',
 *    resolve: [{ .... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when the Next or Back buttons are clicked on a questionnaire.<br />
 * __Default Action__: navigate
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:Navigate/:questionnaireId/:screenId
 * @property {string} trigger - QUESTIONNAIRE:Navigate/:questionnaireId/:screenId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.screenId - The ID of the current screen.
 * @property {string} params.direction - The direction of the navigation. 'next'|'previous'
 * @example
 * ELF.trigger('QUESTIONNAIRE:Navigate/Activate_User/ACTIVATE_USER_S_1', {
 *     screenId,
 *     direction
 * }, this);
 * @example
 * ELF.rules.add({
 *    id: 'NavigateActivateUserS1',
 *    trigger: 'QUESTIONNAIRE:Navigate/Activate_User/ACTIVATE_USER_S_1',
 *    resolve: [{ .... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered to transmit a completed questionnaire.<br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:Transmit/:questionnaireId
 * @property {string} trigger - QUESTIONNAIRE:Transmit/:questionnaireId
 * @property {QuestionnaireCompletionView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.questionnaire - The ID of the questionnaire to transmit.
 * @property {Subject} params.subject - The Subject associated with the completed questionnaire.
 * @property {Visit} params.visit - The visit associated with the completed questionnaire.
 * @example
 * ELF.trigger('QUESTIONNAIRE:Transmit/Activate_User', {
 *     questionnaire,
 *     subject,
 *     visit
 * }, this);
 * @example
 * ELF.rules.add({
 *    id: 'TransmitActivateUser',
 *    trigger: 'QUESTIONNAIRE:Transmit/Activate_User',
 *    resolve: [{ .... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a question has been answered.<br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:Answered/:questionnaireId/:questionId
 * @property {string} trigger - QUESTIONNAIRE:Answered/:questionnaireId/:questionId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.questionnaire - The ID of the questionnaire.
 * @property {string} params.screen - The ID of the current screen.
 * @property {string} params.question - The ID of the question answered.
 * @property {Answers} params.answer - The widget's Answers collection.
 * @example
 * ELF.trigger('QUESTIONNAIRE:Answered/Activate_User/Activate_User_Q_1', {
 *     questionnaire,
 *     screen,
 *     question,
 *     answer
 * }, this);
 * @example
 * ELF.rules.add({
 *    id: 'ActivateUserQ1Answered',
 *    trigger: 'QUESTIONNAIRE:Answered/Activate_User/Activate_User_Q_1',
 *    resolve: [{ .... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event fired to trigger validation on a widget.
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~WIDGET:Validation
 * @property {string} trigger - WIDGET:Validation
 * @property {WidgetBase} context - The current widget being validated.
 * @property {Object} params - An empty object literal.
 * @example
 * ELF.trigger('WIDGET:Validation', {}, questionView.widget);
 * @example
 * ELF.rules.add({
 *    id: 'WidgetValidation',
 *    trigger: 'WIDGET:Validation',
 *    resolve: [{ .... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered prior to a widget rendering.
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~WIDGET:Rendering
 * @property {string} trigger - WIDGET:Rendering
 * @property {undefined} context - N/A
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {WidgetBase} params.widget - The widget being rendered.
 * @example
 * ELF.trigger('WIDGET:Rendering', { widget });
 * @example
 * ELF.rules.add({
 *    id: 'WidgetRendering',
 *    trigger: 'WIDGET:Rendering',
 *    resolve: [{ .... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered by the {@link AlarmManager} for a given alarm.
 * @typedef {Object} ELF.triggers~ALARM:Trigger:scheduleId
 * @property {string} trigger - ALARM:Trigger:scheduleId
 * @property {window} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Object} alarmConfig - The configuration of the alarm in question.
 * @example
 * ELF.trigger(`ALARM:Trigger${scheduleId}`, { alarmConfig }, window)
 * @example
 * ELF.rules.add({
 *     id: 'AutoDial',
 *     trigger: 'ALARM:Trigger/AutoDial',
 *     evaluate: ['AND', 'isOnline', 'isStudyOnline'],
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when time slip correction requires a retry.
 * @typedef {Object} ELF.triggers~TIMESLIP:Retry
 * @property {string} trigger - TIMESLIP:Retry
 * @property {undefined} context - No context provided.
 * @property {undefined} params - No parameters provided.
 * @example
 * ELF.trigger('TIMESLIP:Retry');
 * @example
 * ELF.rules.add({
 *     id: 'SkipTimeConfirmation',
 *     trigger: ['APPLICATION:Loaded', 'TIMESLIP:Retry'],
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered prior to navigation being completed.
 * This route is useful for intercepting navigation and rerouting.  Returning a {@link ELF~ActionResult} with preventDefault: true will cancel default navigation.
 * @typedef {Object} ELF.triggers~NAVIGATE::controller/:route
 * @property {string} trigger - NAVIGATE::controller/:route
 * @property {RouterBase} context - The current context of the ELF event.
 * @property {Array<string>} params - Arguments provided to the Backbone.history.navigate method.
 * @example
 * ELF.trigger(`NAVIGATE:${controller}/${route}`, args, this)
 * @example
 * ELF.rules.add({
 *     id: 'NewPatientDefect',
 *     trigger: 'NAVIGATE:application/addNewPatient',
 *     evaluate: 'isQuestionnaireCompleted',
 *     resolve : [{ action: 'preventDefault' }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a Session timeout occurs while a Questionnaire is open.
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:SessionTimeout/:questionnaireId
 * @property {string} trigger - QUESTIONNAIRE:SessionTimeout/:questionnaireId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.questionnaire - The ID of the questionnaire opened.
 * @example
 * ELF.trigger(`QUESTIONNAIRE:SessionTimeout/Daily`, { questionnaire: 'Daily' }, this)
 * @example
 * ELF.rules.add({
 *     id: 'AffidavitSessionTimeout',
 *     trigger: 'QUESTIONNAIRE:SessionTimeout/Daily',
 *     evaluate: 'isAffidavitSigned',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a questionnaire timeout occurs while a Questionnaire is open.
 * @typedef {Object} ELF.triggers~QUESTIONNAIRE:QuestionnaireTimeout/:questionnaireId
 * @property {string} trigger - QUESTIONNAIRE:QuestionnaireTimeout/:questionnaireId
 * @property {BaseQuestionnaireView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.questionnaire - The ID of the questionnaire opened.
 * @example
 * ELF.trigger(`QUESTIONNAIRE:QuestionnaireTimeout/Daily`, { questionnaire: 'Daily' }, this)
 * @example
 * ELF.rules.add({
 *     id: 'AffidavitQuestionnaireTimeout',
 *     trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/Daily',
 *     evaluate: 'isAffidavitSigned',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered prior to a DynamicText function executing.
 * Used by Screenshot Mode to intercept dynamic string placement. <br />
 * __Default Action__: Execute dynamic string function.
 * @typedef {Object} ELF.triggers~DYNAMICTEXT:Trigger
 * @property {string} trigger - DYNAMICTEXT:Trigger
 * @property {undefined} context - No context provided.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.key - The key or ID of the dynamic text function.
 * @property {Function} params.dynamicText - The dynamic text function to execute.
 * @example
 * ELF.trigger('DYNAMICTEXT:Trigger', { key,  dynamicText });
 */

/**
 * @description
 * An {@link ELF} event triggered when no support for localStorage is detected.
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~LOCALSTORAGE:NoSupport
 * @property {string} trigger - LOCALSTORAGE:NoSupport
 * @property {undefined} context - No context provided.
 * @property {Object} params - Parameters passed into the triggered event.
 * @example
 * ELF.trigger('LOCALSTORAGE:NoSupport', { });
 * @example
 * ELF.rules.add({
 *    id: 'noLocalStorageSupport',
 *    trigger: 'LOCALSTORAGE:NoSupport',
 *    resolve: [{
 *        action: 'notify',
 *        data: { key: 'LOCAL_STORAGE_FAILURE' }
 *    }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a user's login credentials have changed. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~USER:ChangeCredentials/:userId
 * @property {string} trigger - USER:ChangeCredentials/:userId
 * @property {User} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.method - The method of the update. e.g. 'updateUserCredentials'
 * @property {string} params.params - Stringified JSON of the updated credentials.
 * @property {number} params.created - Epoch time of credential update.
 * @example
 * ELF.trigger(`USER:ChangeCredentials/${this.get('userId')}`, {
 *     method: 'updateUserCredentials',
 *     params: JSON.stringify({ ... }),
 *     created: new Date().getTime()
 * }, this)
 * @example
 * ELF.rules.add({
 *     id: 'UserChangeCredentials',
       trigger: 'USER:ChangeCredentials',
 * })
 */


/**
 * @description
 * An {@link ELF} event triggered when Historical Data has been received from the web service.<br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~HISTORICALDATASYNC:Received/:collection
 * @property {string} trigger - HISTORICALDATASYNC:Received/:collection
 * @property {undefined} context - N/A
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {any} params.res - The historical data received. Type may vary depending on service request.
 * @property {Backbone.Collection} params.collection - An instance of the collection the data belongs to. e.g. Users.
 * @example
 * rules.add({
 *      id: 'handleUserSyncData',
 *      // Triggered when the device receives User data from the web service.
 *      trigger: 'HISTORICALDATASYNC:Received/Users',
 *      resolve: [
 *          { action: 'handleUserSyncData' }
 *      ]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered a subject has been terminated from the study. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~Termination
 * @property {string} trigger - Termination
 * @property {undefined} context - N/A
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {boolean} params.endParticipation - Determines if the subject's participation in the study should be ended.
 * @property {boolean} params.subjectActive - Determines if the subject record on the device is currently in an active state.
 * @property {string} params.deviceID - The ID of the device.
 * @example
 * rules.add({
 *     id: 'Termination',
 *     trigger: 'Termination',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a duplicate subject has been transmitted. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~TRANSMIT:Duplicate/Subject
 * @property {string} trigger - TRANSMIT:Duplicate/Subject
 * @property {undefined} context - N/A
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.krpt - The KRPT of the duplicate subject.
 * @example
 * rules.add({
 *     id: 'handleDuplicateSubject',
 *     trigger: 'TRANSMIT:Duplicate/Subject',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a duplicate user has been transmitted. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~TRANSMIT:Duplicate/User
 * @property {string} trigger - TRANSMIT:Duplicate/User
 * @property {undefined} context - N/A
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {User} params.user - The duplicate user record.
 * @property {Transmission} params.transmission - The transmission record that started the user transmission.
 * @example
 * rules.add({
 *     id: 'handleDuplicateUser',
 *     trigger: 'TRANSMIT:Duplicate/User',
 *     resolve: [
 *         { action: 'notify', data: { key: 'USER_ALREADY_EXISTS_ERROR' }},
 *         { action: 'displayMessage', data: 'PLEASE_WAIT' },
 *         { action: 'handleDuplicateUserTransmission' }
 *     ]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a subject's data has been fetched during activation. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~ACTIVATION:SubjectDataReceived
 * @property {string} trigger - ACTIVATION:SubjectDataReceived
 * @property {ActivationBaseView} context - The current context of the ELF event.
 * @property {Object} params - N/A
 * @example
 * ELF.rules.add({
 *     id: 'RequestLastDiaryData',
 *     trigger: 'ACTIVATION:SubjectDataReceived',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered after a subject has been successfully activated on the device. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~ACTIVATION:Transmit
 * @property {string} trigger - ACTIVATION:Transmit
 * @property {ActivationBaseView} context - The current context of the ELF event.
 * @property {Object} params - N/A
 * @example
 * ELF.rules.add({
 *     id: 'subjectActivated',
 *     trigger: 'ACTIVATION:Transmit',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered after the LoginView has rendered.
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers~LOGIN:Rendered
 * @property {string} trigger - LOGIN:Rendered
 * @property {LoginView} context - The current context of the ELF event.
 * @property {Object} params - N/A
 * @example
 * ELF.rules.add({
 *     id: 'LoginRendered',
 *     trigger: 'LoginRendered',
 *     resolve: [{ ... }]
 * });
 */

// //////////////////////////////////////////////////////////////////////
//                       Triggers - LogPad                            //
// //////////////////////////////////////////////////////////////////////

/**
 * @namespace ELF.triggers/logpad
 * @memberOf ELF
 * @description
 * The following triggers are restricted to the eCoa Handheld (LogPad) application. <br />
 * _For more information about ELF triggers, see {@link ELF.triggers}._
 * ### Quick Links
 * <hr />
 * #### APPLICATION
 * - {@link ELF.triggers/logpad~APPLICATION:Loaded APPLICATION:Loaded}
 * - {@link ELF.triggers/logpad~APPLICATION:Uninstall APPLICATION:Uninstall}
 *
 * #### CHANGEPASSWORD
 * - {@link ELF.triggers/logpad~CHANGEPASSWORD:Transmit CHANGEPASSWORD:Transmit }
 *
 * #### CHANGESECRETQUESTION
 * - {@link ELF.triggers/logpad~CHANGESECRETQUESTION:Transmit CHANGESECRETQUESTION:Transmit }
 *
 * #### CODEENTRY
 * - {@link ELF.triggers/logpad~CODEENTRY:Submit}
 *
 * #### DASHBOARD
 * - {@link ELF.triggers/logpad~DASHBOARD:Open/:roleId DASHBOARD:Open/:roleId }
 * - {@link ELF.triggers/logpad~DASHBOARD:Transmit/:roleId DASHBOARD:Transmit/:roleId}
 * - {@link ELF.triggers/logpad~DASHBOARD:Rendered/:roleId DASHBOARD:Rendered/:roleId}
 * - {@link ELF.triggers/logpad~DASHBOARD:RemovedFromDashboard/:questionnaireId DASHBOARD:RemovedFromDashboard/:questionnaireId}
 * - {@link ELF.triggers/logpad~DASHBOARD:AddedToDashboard/:questionnaireId DASHBOARD:AddedToDashboard/:questionnaireId}
 * - {@link ELF.triggers/logpad~DASHBOARD:Logout/:roleId DASHBOARD:Logout/:roleId}
 * - {@link ELF.triggers/logpad~DASHBOARD:OpenQuestionnaire/:questionnaireId DASHBOARD:OpenQuestionnaire/:questionnaireId}
 *
 * #### LOGIN
 * - {@link ELF.triggers/logpad~LOGIN:Transmit/:roleId LOGIN:Transmit/:roleId}
 *
 * #### PRIVACYPOLICYACTIVATION
 * - {@link ELF.triggers/logpad~PRIVACYPOLICYACTIVATION:Backout PRIVACYPOLICYACTIVATION:Backout}
 *
 * #### REACTIVATION
 * - {@link ELF.triggers/logpad~REACTIVATION:Transmit REACTIVATION:Transmit}
 *
 * #### TOOLBOX
 * - {@link ELF.triggers/logpad~TOOLBOX:Transmit TOOLBOX:Transmit}
 * - {@link ELF.triggers/logpad~TOOLBOX:Render TOOLBOX:Render}
 */

/**
 * @description
 * An {@link ELF} event triggered when the application has loaded.
 * @typedef {Object} ELF.triggers/logpad~APPLICATION:Loaded
 * @property {string} trigger - APPLICATION:Loaded
 * @property {undefined} context - N/A
 * @property {undefined} params - N/A
 * @example
 * ELF.trigger('APPLICATION:Loaded');
 * @example
 * ELF.rules.add({
 *     id: 'ApplicationLoaded',
 *     trigger: 'APPLICATION:Loaded',
 *     resolve: [{ ... }]
 * })
 */

 /**
 * @description
 * An {@link ELF} event triggered when the application will be uninstalled. The uninstall opreation is handled with a built-in rule.
 * @typedef {Object} ELF.triggers/logpad~APPLICATION:Uninstall
 * @property {string} trigger - APPLICATION:Uninstall
 * @property {Object} context - The class instance firing the ELF trigger
 * @property {Object} params - Empty object literal
 * @example
 * ELF.trigger('APPLICATION:Uninstall', {}, );
 * @example
 * ELF.rules.add({
 *     id: 'ApplicationUninstall',
 *     trigger: 'APPLICATION:Uninstall',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered to initiate Reactivation transmissions. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers/logpad~REACTIVATION:Transmit
 * @property {string} trigger - REACTIVATION:Transmit
 * @property {(ForgotPasswordActivationView|ReactivationView)} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @example
 * ELF.trigger('REACTIVATION:Transmit', {  }, this);
 * @example
 * ELF.rules.add({
 *     id: 'ReactivationTransmit',
 *     trigger: 'REACTIVATION:Transmit',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when transmit is initated from the Toolbox. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers/logpad~TOOLBOX:Transmit
 * @property {string} trigger - TOOLBOX:Transmit
 * @property {ToolboxView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Subject} params.subject - The subject assigned to the device.
 * @example
 * ELF.trigger('TOOLBOX:Transmit', { subject }, this);
 * @example
 * ELF.rules.add({
 *     id: 'ToolboxTransmit',
 *     trigger: 'TOOLBOX:Transmit',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered after the Toolbox view has rendered. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers/logpad~TOOLBOX:Render
 * @property {string} trigger - TOOLBOX:Render
 * @property {ToolboxView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @example
 * ELF.trigger('TOOLBOX:Render', { }, this);
 * @example
 * ELF.rules.add({
 *     id: 'ToolboxRender',
 *     trigger: 'TOOLBOX:Render',
 *     resolve: [{ ... }]
 * });
 */


/**
 * @description
 * An {@link ELF} event triggered when a subject's password has been changed. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers/logpad~CHANGEPASSWORD:Transmit
 * @property {string} trigger - CHANGEPASSWORD:Transmit
 * @property {ChangePasswordView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Subject} params.subject - The target subject of the password change.
 * @example
 * ELF.trigger('CHANGEPASSWORD:Transmit', { subject }, this);
 * @example
 * ELF.rules.add({
 *     id: 'PasswordChange',
 *     trigger: 'CHANGEPASSWORD:Transmit',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a subject's secret question and answer have been changed. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers/logpad~CHANGESECRETQUESTION:Transmit
 * @property {string} trigger - CHANGESECRETQUESTION:Transmit
 * @property {ChangeSecretQuestionView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Subject} params.subject - The target subject of the secret question/answer change.
 * @example
 * ELF.trigger('CHANGESECRETQUESTION:Transmit', { subject }, this);
 * @example
 * ELF.rules.add({
 *     id: 'SecretQuestionChange',
 *     trigger: 'CHANGESECRETQUESTION:Transmit',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a QR Code is scanned on the CodeEntryView. This is fired
 * prior to processing the QR Code. <br />
 * __Default Action__: Process the QR Code.
 * @typedef {Object} ELF.triggers/logpad~CODEENTRY:Submit
 * @property {string} trigger - CODEENTRY:Submit
 * @property {CodeEntryView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.value - The study name.
 * @property {string} params.product - The product the event triggered from.
 * @example
 * ELF.trigger('CODEENTRY:Submit', { value: 'pht-syn-study09', product: 'logpad' }, this);
 * @example
 * ELF.rules.add({
 *     id: 'Screenshot_Tool',
 *     trigger: 'CODEENTRY:Submit',
 *     evaluate: 'isScreenshot',
 *     resolve: [...]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when the Dashboard view has opened.
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers/logpad~DASHBOARD:Open/:roleId
 * @property {string} trigger - DASHBOARD:Open/:roleId
 * @property {DashboardView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @example
 * ELF.trigger('DASHBOARD:Open/subject', { }, this);
 * @example
 * ELF.rules.add({
 *     id: 'SubjectOpenedDashboard',
 *     trigger: 'DASHBOARD:Open/subject',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a user logs out of the application. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers/logpad~DASHBOARD:Logout/:roleId
 * @property {string} trigger - DASHBOARD:Logout/:roleId
 * @property {DashboardView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @example
 * ELF.trigger('DASHBOARD:Logout/subject', { }, this);
 * @example
 * ELF.rules.add({
 *     id: 'SubjectLogout',
 *     trigger: 'DASHBOARD:Logout/subject',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered prior to opening a questionnaire from the Dashboard. <br />
 * __Default Action__: Open the target questionnaire.
 * @typedef {Object} ELF.triggers/logpad~DASHBOARD:OpenQuestionnaire/:questionnaireId
 * @property {string} trigger - DASHBOARD:OpenQuestionnaire/:questionnaireId
 * @property {FormGatewayView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.questionnaire - The ID of the questionnaire to open.
 * @property {string} params.schedule_id - The schedule ID associated with the questionnaire.
 * @example
 * ELF.trigger('DASHBOARD:OpenQuestionnaire/Daily', { questionnaire, schedule_id }, this);
 * @example
 * ELF.rules.add({
 *     id: 'OpenDaily',
 *     trigger: 'DASHBOARD:OpenQuestionnaire/Daily',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when transmit is initiated from the Dashboard view. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers/logpad~DASHBOARD:Transmit/:roleId
 * @property {string} trigger - DASHBOARD:Transmit/:roleId
 * @property {DashboardView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Subject} params.subject - The activated subject.
 * @example
 * ELF.trigger('DASHBOARD:Transmit/subject', { subject }, this);
 * @example
 * ELF.rules.add({
 *     id: 'DashboardTransmit',
 *     trigger: 'DASHBOARD:Transmit/subject',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when the Dashboard view is rendered.<br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers/logpad~DASHBOARD:Rendered/:roleId
 * @property {string} trigger - DASHBOARD:Rendered/:roleId
 * @property {DashboardView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Subject} params.subject - The activated subject.
 * @example
 * ELF.trigger('DASHBOARD:Rendered/subject', { subject }, this);
 * @example
 * ELF.rules.add({
 *     id: 'SubjectDashboardRendered',
 *     trigger: 'DASHBOARD:Rendered/subject',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a questionnaire is removed from the Dashboard.<br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers/logpad~DASHBOARD:RemovedFromDashboard/:questionnaireId
 * @property {string} trigger - DASHBOARD:RemovedFromDashboard/:questionnaireId
 * @property {DashboardView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @example
 * ELF.trigger('DASHBOARD:RemovedFromDashboard/Daily', { }, this);
 * @example
 * ELF.rules.add({
 *     id: 'DailyRemovedFromDashboard',
 *     trigger: 'DASHBOARD:RemovedFromDashboard/Daily',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when a questionnaire is added from the Dashboard.<br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers/logpad~DASHBOARD:AddedToDashboard/:questionnaireId
 * @property {string} trigger - DASHBOARD:AddedToDashboard/:questionnaireId
 * @property {DashboardView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @example
 * ELF.trigger('DASHBOARD:AddedToDashboard/Daily', { }, this);
 * @example
 * ELF.rules.add({
 *     id: 'DailyAddedToDashboard',
 *     trigger: 'DASHBOARD:AddedToDashboard/Daily',
 *     resolve: [{ ... }]
 * });
 */

/**
 * @description
 * An {@link ELF} event triggered when transmit is initiated from the Login view.
 * @typedef {Object} ELF.triggers/logpad~LOGIN:Transmit/:roleId
 * @property {string} trigger - LOGIN:Transmit/:roleId
 * @property {LoginView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Subject} params.subject - The subject assigned to the device.
 * @example
 * ELF.trigger('LOGIN:Transmit/subject', { subject }, this);
 * @example
 * ELF.rules.add({
 *     id: 'SubjectTransmissions',
 *     trigger: ['LOGIN:Transmit/subject', ...],
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered backing out of the Privacy Policy during activation. <br />
 * __Default Action__: N/A
 * @typedef {Object} ELF.triggers/logpad~PRIVACYPOLICYACTIVATION:Backout
 * @property {string} trigger - PRIVACYPOLICYACTIVATION:Backout
 * @property {PrivacyPolicyActivationView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.

 * @example
 * ELF.trigger('PRIVACYPOLICYACTIVATION:Backout', { subject }, this);
 * @example
 * ELF.rules.add({
 *     id: 'PrivacyPolicyBackout',
 *     trigger: 'PRIVACYPOLICYACTIVATION:Backout',
 *     resolve: [{ ... }]
 * })
 */

// //////////////////////////////////////////////////////////////////////
//                       Triggers - SitePad                           //
// //////////////////////////////////////////////////////////////////////

/**
 * @namespace ELF.triggers/sitepad
 * @memberOf ELF
 * @description
 * The following triggers are restricted to the eCoa Tablet (SitePad) application. <br />
 * _For more information about ELF triggers, see {@link ELF.triggers}._
 * ### Quick Links
 * <hr />
 * #### APPLICATION
 * - {@link ELF.triggers/sitepad~APPLICATION:Loaded APPLICATION:Loaded}
 *
 * #### DASHBOARD/FORMGATEWAY
 * - {@link ELF.triggers/sitepad~DASHBOARD:OpenQuestionnaire/:questionnaireId DASHBOARD:OpenQuestionnaire/:questionnaireId}
 * - {@link ELF.triggers/sitepad~FORMGATEWAY:Transmit FORMGATEWAY:Transmit}
 *
 * #### HOME
 * - {@link ELF.triggers/sitepad~HOME:Transmit HOME:Transmit}
 *
 * #### LOGIN
 * - {@link ELF.triggers/sitepad~LOGIN:Transmit LOGIN:Transmit}
 *
 * #### MODESELECT
 * - {@link ELF.triggers/sitepad~MODESELECT:Submit MODESELECT:Submit}
 *
 * #### SETTIMEZONE
 * - {@link ELF.triggers/sitepad~SETTIMEZONE:Completed SETTIMEZONE:Completed}
 *
 * #### SITESELECTION
 * - {@link ELF.triggers/sitepad~SITESELECTION:Back SITESELECTION:Back}
 *
 * #### SubjectTermination
 * - {@link ELF.triggers/sitepad~SubjectTermination SubjectTermination}
 *
 * #### TRANSMIT
 * - {@link ELF.triggers/sitepad~TRANSMIT:Nonexistent/User TRANSMIT:Nonexistent/User}
 *
 * #### USERMANAGEMENT
 * - {@link ELF.triggers/sitepad~USERMANAGEMENT:Transmit USERMANAGEMENT:Transmit}
 * - {@link ELF.triggers/sitepad~USERMANAGEMENT:DeactivateUser USERMANAGEMENT:DeactivateUser}
 * - {@link ELF.triggers/sitepad~USERMANAGEMENT:ActivateUser USERMANAGEMENT:ActivateUser}
 * - {@link ELF.triggers/sitepad~USERMANAGEMENT:EditUser USERMANAGEMENT:EditUser}
 * - {@link ELF.triggers/sitepad~USERMANAGEMENT:NewUser USERMANAGEMENT:NewUser}
 *
 * #### VISITGATEWAY
 * - {@link ELF.triggers/sitepad~VISITGATEWAY:Transmit VISITGATEWAY:Transmit}
 */

/**
 * @description
 * An {@link ELF} event triggered when the application has loaded.
 * @typedef {Object} ELF.triggers/sitepad~APPLICATION:Loaded
 * @property {string} trigger - APPLICATION:Loaded
 * @property {undefined} context - N/A
 * @property {undefined} params - N/A
 * @example
 * ELF.trigger('APPLICATION:Loaded');
 * @example
 * ELF.rules.add({
 *     id: 'ApplicationLoaded',
 *     trigger: 'APPLICATION:Loaded',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when a edit is transmitted for a nonexistent user.
 * @typedef {Object} ELF.triggers/sitepad~TRANSMIT:Nonexistent/User
 * @property {string} trigger - TRANSMIT:Nonexistent/User
 * @property {undefined} context - N/A
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {User} params.user - The nonexistent user model.
 * @example
 * ELF.trigger('TRANSMIT:Nonexistent/User', { user });
 * @example
 * ELF.rules.add({
 *     id: 'NonexistentUser',
 *     trigger: 'TRANSMIT:Nonexistent/User',
 *     resolve: [{ action: 'handleNonexistentUser' }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when the Time Zone has been set on the eCoa Tablet (SitePad).
 * @typedef {Object} ELF.triggers/sitepad~SETTIMEZONE:Completed
 * @property {string} trigger - SETTIMEZONE:Completed
 * @property {PageView} context - The current context of the ELF event.
 * @property {Object} params - Empty object literal.
 * @example
 * ELF.trigger('SETTIMEZONE:Completed', { }, new PageView());
 * @example
 * ELF.rules.add({
 *     id: 'TimeZoneSet',
 *     trigger: 'SETTIMEZONE:Completed',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when transmit is initiated from the Home view (Subject list).
 * @typedef {Object} ELF.triggers/sitepad~HOME:Transmit
 * @property {string} trigger - HOME:Transmit
 * @property {HomeView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @example
 * ELF.trigger('HOME:Transmit', { }, this);
 * @example
 * ELF.rules.add({
 *     id: 'TransmitAll',
 *     trigger: ['HOME:Transmit', ...],
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when transmit is initiated from the Login view.
 * @typedef {Object} ELF.triggers/sitepad~LOGIN:Transmit
 * @property {string} trigger - LOGIN:Transmit
 * @property {LoginView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @example
 * ELF.trigger('LOGIN:Transmit', { }, this);
 * @example
 * ELF.rules.add({
 *     id: 'TransmitAll',
 *     trigger: ['LOGIN:Transmit', ...],
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when transmit is initiated from the Visit Gateway view.
 * @typedef {Object} ELF.triggers/sitepad~VISITGATEWAY:Transmit
 * @property {string} trigger - VISITGATEWAY:Transmit
 * @property {VisitGatewayView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Subject} params.subject - The target subject for displayed visits.
 * @example
 * ELF.trigger('VISITGATEWAY:Transmit', { subject }, this);
 * @example
 * ELF.rules.add({
 *     id: 'TransmitAll',
 *     trigger: ['VISITGATEWAY:Transmit', ...],
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when transmit is initiated from the User Management (SiteUsersView) view.
 * @typedef {Object} ELF.triggers/sitepad~USERMANAGEMENT:Transmit
 * @property {string} trigger - USERMANAGEMENT:Transmit
 * @property {SiteUsersView} context - The current context of the ELF event.
 * @property {Object} params - An empty object literal.
 * @example
 * ELF.trigger('USERMANAGEMENT:Transmit', { }, this);
 * @example
 * ELF.rules.add({
 *     id: 'TransmitAll',
 *     trigger: ['USERMANAGEMENT:Transmit', ...],
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when the Deactivate User button is clicked from the User Management (SiteUsersView) view. <br />
 * __Default Action__: Open the DEACTIVATE_USER questionnaire.
 * @typedef {Object} ELF.triggers/sitepad~USERMANAGEMENT:DeactivateUser
 * @property {string} trigger - USERMANAGEMENT:DeactivateUser
 * @property {SiteUsersView} context - The current context of the ELF event.
 * @property {Object} params - An empty object literal.
 * @example
 * ELF.trigger('USERMANAGEMENT:DeactivateUser', { }, this);
 * @example
 * ELF.rules.add({
 *     id: 'DeactivateUser',
 *     trigger: 'USERMANAGEMENT:DeactivateUser',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when the Activate User button is clicked from the User Management (SiteUsersView) view. <br />
 * __Default Action__: Open the ACTIVATE_USER questionnaire.
 * @typedef {Object} ELF.triggers/sitepad~USERMANAGEMENT:ActivateUser
 * @property {string} trigger - USERMANAGEMENT:ActivateUser
 * @property {SiteUsersView} context - The current context of the ELF event.
 * @property {Object} params - An empty object literal.
 * @example
 * ELF.trigger('USERMANAGEMENT:ActivateUser', { }, this);
 * @example
 * ELF.rules.add({
 *     id: 'ActivateUser',
 *     trigger: 'USERMANAGEMENT:ActivateUser',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when the Edit User button is clicked from the User Management (SiteUsersView) view. <br />
 * __Default Action__: Open the EDIT_USER questionnaire.
 * @typedef {Object} ELF.triggers/sitepad~USERMANAGEMENT:EditUser
 * @property {string} trigger - USERMANAGEMENT:EditUser
 * @property {SiteUsersView} context - The current context of the ELF event.
 * @property {Object} params - An empty object literal.
 * @example
 * ELF.trigger('USERMANAGEMENT:EditUser', { }, this);
 * @example
 * ELF.rules.add({
 *     id: 'EditUser',
 *     trigger: 'USERMANAGEMENT:EditUser',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when the New User button is clicked from the User Management (SiteUsersView) view. <br />
 * __Default Action__: Open the NEW_USER questionnaire.
 * @typedef {Object} ELF.triggers/sitepad~USERMANAGEMENT:NewUser
 * @property {string} trigger - USERMANAGEMENT:NewUser
 * @property {SiteUsersView} context - The current context of the ELF event.
 * @property {Object} params - An empty object literal.
 * @example
 * ELF.trigger('USERMANAGEMENT:NewUser', { }, this);
 * @example
 * ELF.rules.add({
 *     id: 'NewUser',
 *     trigger: 'USERMANAGEMENT:NewUser',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when transmit is initiated from the Form Gateway.
 * @typedef {Object} ELF.triggers/sitepad~FORMGATEWAY:Transmit
 * @property {string} trigger - FORMGATEWAY:Transmit
 * @property {FormGatewayView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Subject} params.subject - The target subject for displayed questionnaires.
 * @example
 * ELF.trigger('FORMGATEWAY:Transmit', { subject }, this);
 * @example
 * ELF.rules.add({
 *     id: 'TransmitAll',
 *     trigger: ['FORMGATEWAY:Transmit', ...],
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered prior to opening a questionnaire from the Form Gateway. <br />
 * __Default Action__: Open the target questionnaire.
 * @typedef {Object} ELF.triggers/sitepad~DASHBOARD:OpenQuestionnaire/:questionnaireId
 * @property {string} trigger - DASHBOARD:OpenQuestionnaire/:questionnaireId
 * @property {FormGatewayView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Questionnaire} questionnaire - The configuration of the questionnaire to open.
 * @example
 * ELF.trigger('DASHBOARD:OpenQuestionnaire/Daily', { questionnaire }, this);
 * @example
 * ELF.rules.add({
 *     id: 'OpenDaily',
 *     trigger: 'DASHBOARD:OpenQuestionnaire/Daily',
 *     resolve: [{ ... }]
 * })
 */


/**
 * @description
 * An {@link ELF} event triggered a subject's participation has been terminated on the eCoa Tablet (SitePad).
 * @typedef {Object} ELF.triggers/sitepad~SubjectTermination
 * @property {string} trigger - SubjectTermination
 * @property {undefined} context - N/A
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Subject} params.subject - The subject that was terminated.
 * @property {boolean} params.endParticipation - Determines if the subject's termination should end.
 * @property {boolean} params.subjectActive - Current active status of the subject.
 * @example
 * ELF.trigger('SubjectTermination', { subject, endParticipation, subjectActive }, this);
 * @example
 * ELF.rules.add({
 *     id: 'subjectTermination',
 *     trigger: 'SubjectTermination',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when the Next button is pressed on the {@link ModeSelectionView}.
 * @typedef {Object} ELF.triggers/sitepad~MODESELECT:Submit
 * @property {string} trigger - MODESELECT:Submit
 * @property {ModeSelectionView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.mode - The current mode of the eCoa Tablet. e.g. 'trainer'
 * @property {string} params.product - The current product. e.g. 'sitepad'
 * @example
 * ELF.trigger('MODESELECT:Submit', { mode: 'trainer', product: 'sitepad' }, this);
 * @example
 * ELF.rules.add({
 *     id: 'modeSelectionSubmit',
 *     trigger: 'MODESELECT:Submit',
 *     evaluate: true,
 *     resolve: [{
 *         action: 'navigateTo',
 *         data: 'select-time-zone'
 *     }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when the Back button is pressed on the SiteSelectionView.
 * @typedef {Object} ELF.triggers/sitepad~SITESELECTION:Back
 * @property {string} trigger - SITESELECTION:Back
 * @property {SiteSelectionView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {string} params.product - The current product. e.g. 'sitepad'
 * @example
 * ELF.trigger('SITESELECTION:Back', { product: 'sitepad' }, this);
 * @example
 * ELF.rules.add({
 *     id: 'SiteSelectionBack',
 *     trigger: 'SITESELECTION:Back',
 *     resolve: [{
 *         action: 'navigateTo',
 *         data: 'modeSelection'
 *     }]
 * })
 */

// //////////////////////////////////////////////////////////////////////
//                       Triggers - Web                           //
// //////////////////////////////////////////////////////////////////////

/**
 * @namespace ELF.triggers/web
 * @memberOf ELF
 * @description
 * The following triggers are restricted to the eCoa Web application. <br />
 * _For more information about ELF triggers, see {@link ELF.triggers}._
 * ### Quick Links
 * <hr />
 * #### APPLICATION
 * - {@link ELF.triggers/web~APPLICATION:InactiveTab APPLICATION:InactiveTab}
 * - {@link ELF.triggers/web~APPLICATION:Loaded APPLICATION:Loaded}
 * - {@link ELF.triggers/web~APPLICATION:Uninstall APPLICATION:Uninstall}
 *
 * #### COMPLETIONVIEW
 * - {@link ELF.triggers/web~COMPLETIONVIEW:SavedDiaryQuestionnaireTimeout COMPLETIONVIEW:SavedDiaryQuestionnaireTimeout}
 * - {@link ELF.triggers/web~COMPLETIONVIEW:SavedDiarySessionTimeout COMPLETIONVIEW:SavedDiarySessionTimeout}
 *
 * #### DASHBOARD
 * - {@link ELF.triggers/web~DASHBOARD:TranscribeQuestionnaire/:questionnaireId DASHBOARD:TranscribeQuestionnaire/:questionnaireId}
 *
 * #### LOGIN
 * - {@link ELF.triggers/web~LOGIN:Completed LOGIN:Completed}
 *
 * #### SETSITETIMEZONE
 * - {@link ELF.triggers/web~SETSITETIMEZONE:Transmit SETSITETIMEZONE:Transmit}
 *
 * #### SITEASSIGNMENT
 * - {@link ELF.triggers/web~SITEASSIGNMENT:ConfirmChange SITEASSIGNMENT:ConfirmChange}
 * - {@link ELF.triggers/web~SITEASSIGNMENT:Completed SITEASSIGNMENT:Completed}
 *
 * #### WEB
 * - {@link ELF.triggers/web~WEB:PreventOffline WEB:PreventOffline}
 */

/**
 * @description
 * An {@link ELF} event triggered when the application has loaded.
 * @typedef {Object} ELF.triggers/web~APPLICATION:Loaded
 * @property {string} trigger - APPLICATION:Loaded
 * @property {undefined} context - N/A
 * @property {undefined} params - N/A
 * @example
 * ELF.trigger('APPLICATION:Loaded');
 * @example
 * ELF.rules.add({
 *     id: 'ApplicationLoaded',
 *     trigger: 'APPLICATION:Loaded',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when the application will be uninstalled. The uninstall opreation is handled with a built-in rule.
 * @typedef {Object} ELF.triggers/web~APPLICATION:Uninstall
 * @property {string} trigger - APPLICATION:Uninstall
 * @property {Object} context - The class instance firing the ELF trigger
 * @property {Object} params - Empty object literal
 * @example
 * ELF.trigger('APPLICATION:Uninstall', {}, );
 * @example
 * ELF.rules.add({
 *     id: 'ApplicationUninstall',
 *     trigger: 'APPLICATION:Uninstall',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when a browser tab becomes inactive.
 * @typedef {Object} ELF.triggers/web~APPLICATION:InactiveTab
 * @property {string} trigger - APPLICATION:InactiveTab
 * @property {Object} context - The class instance firing the ELF trigger
 * @property {Object} params - Empty object literal
 * @example
 * ELF.trigger('APPLICATION:InactiveTab', {}, );
 * @example
 * ELF.rules.add({
 *     id: 'ApplicationInactiveTab',
 *     trigger: 'APPLICATION:InactiveTab',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when a questionnaire timeout occurs before a web specific diary is transmitted
 * @typedef {Object} ELF.triggers/web~COMPLETIONVIEW:SavedDiaryQuestionnaireTimeout
 * @property {string} trigger - COMPLETIONVIEW:SavedDiaryQuestionnaireTimeout
 * @property {WebQuestionnaireCompletionView} context - The WebQuestionnaireCompletionView instance firing the ELF trigger
 * @property {Object} params - Empty object literal
 * @example
 * ELF.trigger('COMPLETIONVIEW:SavedDiaryQuestionnaireTimeout', {}, );
 * @example
 * ELF.rules.add({
 *     id: 'SavedDiaryQuestionnaireTimeout',
 *     trigger: 'COMPLETIONVIEW:SavedDiaryQuestionnaireTimeout',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when a session timeout occurs before a web specific diary is transmitted
 * @typedef {Object} ELF.triggers/web~COMPLETIONVIEW:SavedDiarySessionTimeout
 * @property {string} trigger - COMPLETIONVIEW:SavedDiarySessionTimeout
 * @property {WebQuestionnaireCompletionView} context - The WebQuestionnaireCompletionView instance firing the ELF trigger
 * @property {Object} params - Empty object literal
 * @example
 * ELF.trigger('COMPLETIONVIEW:SavedDiarySessionTimeout', {}, );
 * @example
 * ELF.rules.add({
 *     id: 'SavedDiarySessionTimeout',
 *     trigger: 'COMPLETIONVIEW:SavedDiarySessionTimeout',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when the user successfully logs in.
 * @typedef {Object} ELF.triggers/web~LOGIN:Completed
 * @property {string} trigger - LOGIN:Completed
 * @property {Object} context - The class instance firing the ELF trigger
 * @property {Object} params - Empty object literal
 * @example
 * ELF.trigger('LOGIN:Completed', {}, );
 * @example
 * ELF.rules.add({
 *     id: 'LoginCompleted',
 *     trigger: 'LOGIN:Completed',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered after a TimeZone Selection is completed.
 * @typedef {Object} ELF.triggers/web~SETSITETIMEZONE:Transmit
 * @property {string} trigger - SETSITETIMEZONE:Transmit
 * @property {Object} context - The class instance firing the ELF trigger
 * @property {Object} params - Empty object literal
 * @example
 * ELF.trigger('SETSITETIMEZONE:Transmit', {}, );
 * @example
 * ELF.rules.add({
 *     id: 'transmitSiteTimeZone',
 *     trigger: 'SETSITETIMEZONE:Transmit',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when user clicks on the "change site" button on Login Screen.
 * @typedef {Object} ELF.triggers/web~SITEASSIGNMENT:ConfirmChange
 * @property {string} trigger - SITEASSIGNMENT:ConfirmChange
 * @property {undefined} context - N/A
 * @property {undefined} params - N/A
 * @example
 * ELF.trigger('SITEASSIGNMENT:ConfirmChange');
 * @example
 * ELF.rules.add({
 *     id: 'SiteAssignmentConfirmChange',
 *     trigger: 'SITEASSIGNMENT:ConfirmChange',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered after the Site Assignment form is completed.
 * @typedef {Object} ELF.triggers/web~SITEASSIGNMENT:Completed
 * @property {string} trigger - SITEASSIGNMENT:Completed
 * @property {Object} context - The class instance firing the ELF trigger
 * @property {Object} params - Empty object literal
 * @example
 * ELF.trigger('SITEASSIGNMENT:Completed', {}, );
 * @example
 * ELF.rules.add({
 *     id: 'SiteAssignmentCompleted',
 *     trigger: 'SITEASSIGNMENT:Completed',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when the offline application usage is being prevented. This trigger is being used internally with a built-in rule.
 * If the browser is offline, the built-in rule displays a banner stating that  connection is requires and prevents any default behavior or rules from being executed.
 * @typedef {Object} ELF.triggers/web~WEB:PreventOffline
 * @property {string} trigger - WEB:PreventOffline
 * @property {Object} context - The class instance firing the ELF trigger
 * @property {Object} params - Empty object literal
 * @example
 * ELF.trigger('WEB:PreventOffline', {}, );
 * @example
 * ELF.rules.add({
 *     id: 'WebPreventOffline',
 *     trigger: 'WEB:PreventOffline',
 *     resolve: [{ ... }]
 * })
 */

/**
 * @description
 * An {@link ELF} event triggered when a transcription mode is being started for a questionnaire.<br />
 * __Default Action__: Starts the transcription mode
 * @typedef {Object} ELF.triggers/web~DASHBOARD:TranscribeQuestionnaire/:questionnaireId
 * @property {string} trigger - DASHBOARD:TranscribeQuestionnaire/:questionnaireId
 * @property {DashboardView} context - The current context of the ELF event.
 * @property {Object} params - Parameters passed into the triggered event.
 * @property {Object} params.id - id of the questionnaire
 * @example
 * ELF.trigger('DASHBOARD:TranscribeQuestionnaire/Daily', { questionnaire: 'Daily' }, this);
 * @example
 * ELF.rules.add({
 *     id: 'DailyRemovedFromDashboard',
 *     trigger: 'DASHBOARD:TranscribeQuestionnaire/Daily',
 *     resolve: [{ ... }]
 * });
 */

// //////////////////////////////////////////////////////////////////////
//                         Expressions                                //
// //////////////////////////////////////////////////////////////////////

/**
 * @namespace ELF.expressions
 * @memberOf ELF
 * @description
 * ELF rules take a evaluate property which must resolve to a boolean value.  The outcome of the evaluation determines which course of actions the engine will take (resolve or reject).
 * <br />
 * #### Booleans
 * <hr />
 * At its most basic configuration, an expression can be a boolean value, true or false. By default, the each rule is configured to evaluate true, unless configured otherwise.
 * ```js
 *     evaluate: true
 * ```
 * #### Functions - See {@link ELF~ExpressionFunction}
 * <hr />
 * Evaluate can be assigned an {@link ELF~ExpressionFunction}.  The target function should return a deferred boolean value {Q.Promise<boolean>}.  In the snippet below, the provided function returns true if a site exists, and false if not.
 * ```js
 * evaluate: () => {
 *     return Sites.fetchCollection().then(sites => {
 *         return sites.size() ? true : false;
 *     });
 * }
 * ```
 * #### Strings - Registered Expressions
 * <hr />
 * To aid in code reuse, expressions can, and should, be defined separately in external files and registered with ELF via {@link ELF.expression ELF.expression(name, func);}  After an expression has been registered with ELF, said expression can be referenced as a string value.  In the snippet below, a function isSiteConfigured is defined and registered with ELF.
 * ```js
 * export function isSiteConfigured () {
 *     return Sites.fetchCollection().then(sites => {
 *         return sites.size() ? true : false;
 *     });
 * }
 *
 * ELF.expression(‘isSiteConfigured’, isSiteConfigured);
 * ```
 * ```js
 *    evaluate: 'isSiteConfigured'
 * ```
 * #### Object - See {@link ELF~ExpressionObject}
 * <hr />
 * Some {@link ELF~ExpressionFunctions} take configurable parameters via {@link ELF~ExpressionObject}.
 * @example
 * {
 *    // This expression will resolve to true if today is either Monday, Wednesday or Friday.
 *    expression : 'isDayOfTheWeek',
 *    input      : { days : ['Monday', 'Wednesday', 'Friday'] }
 * }
 */

/**
 * @namespace ELF.expressions/logpad
 * @memberOf ELF
 */

/**
 * @namespace ELF.expressions/sitepad
 * @memberOf ELF
 */

/**
 * @namespace ELF.expressions/web
 * @memberOf ELF
 */
