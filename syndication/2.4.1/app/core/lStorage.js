import Logger from 'core/Logger';
import ELF from 'core/ELF';

let store = window.localStorage,
    logger = new Logger('lStorage');

const prefix = 'PHT_';

/**
 * Gets a value from the localStorage
 * @param {string} name name of the value
 * @return {Object} The value.
 */
export function getItem (name) {
    if (typeof store === 'undefined') {
        logger.fatal('localStorage does not exist');
        return null;
    }

    // Attempt to read from localStorage
    try {
        return store.getItem(prefix + name);
    } catch (e) {
        logger.fatal('Unexpected error while reading from localStorage', e);
    }

    return null;
}

LF.Utilities.getItem = getItem;

/**
 * Removes an Item from the localStorage
 * @param {string} name name of the value
 */
export function removeItem (name) {
    if (typeof store === 'undefined') {
        logger.fatal('localStorage does not exist');
        return;
    }

    // Attempt to remove from localStorage
    try {
        store.removeItem(prefix + name);
    } catch (e) {
        if (/QuotaExceededError\.*/.test(e.message)) {
            ELF.trigger('LOCALSTORAGE:NoSupport', {}, LF.Actions);
        } else {
            logger.fatal('Unexpected error while removing item from localStorage', e);
        }
    }
}

LF.Utilities.removeItem = removeItem;

/**
 * Sets a value to the localStorage
 * @param {string} name name of the value
 * @param {*} val the value to be stored in local storage
 */
export function setItem (name, val) {
    if (typeof store === 'undefined') {
        logger.fatal('localStorage does not exist');
        return;
    }

    // Attempt to write to localStorage
    try {
        store.setItem(prefix + name, val);
    } catch (e) {
        if (/QuotaExceededError\.*/.test(e.message)) {
            ELF.trigger('LOCALSTORAGE:NoSupport', {}, LF.Actions);
        } else {
            logger.fatal('Unexpected error while setting item in localStorage', e);
        }
    }
}

LF.Utilities.setItem = setItem;
