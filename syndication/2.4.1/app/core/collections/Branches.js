import BaseCollection from './BaseCollection';
import Branch from '../models/Branch';

/**
 * A collection of branching configurations.
 * @class Branches
 * @extends BaseCollection
 */
export default class Branches extends BaseCollection {
    /**
     * @property {Branch} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Branch}'
     */
    get model () {
        return Branch;
    }
}

window.LF.Collection.Branches = Branches;
