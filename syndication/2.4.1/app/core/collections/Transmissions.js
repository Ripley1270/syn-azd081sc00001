import QueueBase from './QueueBase';
import Transmission from '../models/Transmission';
import Transmit from 'core/transmit';
import COOL from 'core/COOL';
import ELF from 'core/ELF';

/**
 * A class that creates a collection of Transmissions.
 * @class Transmissions
 * @extends QueueBase
 * @example let collection = new Transmissions();
 */
export default class Transmissions extends QueueBase {
    /**
     * The Model that belongs to the collection
     * @property {Transmission} model
     * @readonly
     * @default '{@link Transmission}'
     */
    get model () {
        return Transmission;
    }

    /**
     * Executes the action.
     * @param {number} ordinal - The ordinal of the action to execute within the collection.
     * @param {Function} [callback] - Function invoked upon completion of execution.
     * @returns {Q.Promise}
     */
    execute (ordinal, callback = $.noop) {
        let transmissionMethod,
            transmission = this.at(ordinal),
            reject = (err) => {
                return ELF.trigger('TRANSMISSION:Failed', { err, transmission }, this)
                .then(() => Q.reject({ err, transmission }));
            };

        if (!this.size()) {
            return reject(new Error('Transmission queue is empty.'));
        }

        if (!transmission) {
            return reject(new Error('Nonexistent transmission record.'));
        }

        if (transmission.get('status') === 'failed') {
            this.remove(transmission);
            return Q();
        }

        transmissionMethod = COOL.getService('Transmit', Transmit)[transmission.get('method')];

        if (!transmissionMethod) {
            return reject(new Error('Nonexistent transmission type.'));
        }

        let request;

        // If the transmission method takes two arguments we need to execute assuming a callback function.
        if (transmissionMethod.length === 2) {
            request = Q.Promise((resolve) => {
                transmissionMethod.call(this, transmission, resolve);
            });
        } else {
            request = transmissionMethod.call(this, transmission);
        }

        return request
        .catch(reject)
        .finally(callback);
    }

    /**
     * Counts transmission items in the transmission queue.
     * @returns {Q.Promise<number>}
     */
    getCount () {
        return this.pullQueue(() => _.countBy(this.models, item => item.get('method')));
    }
}

window.LF.Collection.Transmissions = Transmissions;
