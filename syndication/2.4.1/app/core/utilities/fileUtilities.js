import cordovaPromiseFS from 'cordova-promise-fs';

const storageSize = 0,
    concurrency = 3;

/**
 * Generates the Cordova filesystem object for reading and writing files and directories.
 * Simple wrapper for https://github.com/markmarijnissen/cordova-promise-fs
 * @returns {Object} A configured cordova-promise-fs object.
 */
export function createFilesystem () {
    return cordovaPromiseFS({
        persistent: true,
        Promise: Q.Promise,
        storageSize,
        concurrency
    });
}
