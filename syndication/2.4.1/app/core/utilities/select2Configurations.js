/**
 * The purpose of this function file is to provide configurable/overridable functions that will return parameter values for
 * configuring of the select2 query.
 *
 *
 * @example
 * The select2 can be configured by passing parameters via an object to the query
 * https://select2.org/i18n#message-translations
 * $('#search-select').select2({
 *
 *  "language": {
 *      "noResults": function(){
 *          return "No Results Found <a href='#' class='btn btn-danger'>Use it anyway</a>";
 *      }
 *  },
 *   escapeMarkup: function (markup) {
 *       return markup;
 *   }
 * }
 * @example
 * You can find all of the options in the language files provided in the
 * build. They ALL MUST BE FUNCTIONS that return the string that should be
 * displayed.
 *
 * language: {
 *  inputTooShort: function () {
 *    return "You must enter more characters...";
 *  }
 * }
 */


/**
 * Returns a function object, that itself returns the  string for a "No Results Found"  type of query result.
 * @example
 * return queryConfigurations.noResults()
 * .then(noResultStringFunction => Q.Promise((resolve) => {
 *               $.when(this.$(id).select2({
 *                   minimumResultsForSearch: Infinity,
 *                   width: '',
 *                   templateResult: checkForRTLNumbers,
 *                   templateSelection: checkForRTLNumbers,
 *                   language: noResultStringFunction       //<----used here
 *               }))
 * @returns {Object} function object which returns a string.
 */
export function noResults () {
    return LF.getStrings('NO_RESULTS_FOUND')
     .then((string) => {
         let noResultStringFunc = () => string;
         return noResultStringFunc;
     });
}
