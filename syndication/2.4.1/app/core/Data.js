
/*
 * This module is used to maintain in memory data, that should be shared across modules.
 */
let Data = { };

export default Data;

// @todo We want to phase this alias out...
LF.Data = Data;
