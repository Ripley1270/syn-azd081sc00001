import ELF from 'core/ELF';
import COOL from 'core/COOL';

/**
 * @memberOf ELF.expressions
 * @method  isOnline
 * @description
 * An ELF expression that determines if the device is online.
 * @param {Object} input Input provided by ELF.
 * @returns {Q.Promise<boolean>} True if online, false if not.
 * @example
 * // Example of isOnline in use as an ELF expression.
 * evaluate: 'isOnline'
 * @example
 * // Example of importing and invoking isOnline.
 * import { isOnline } from 'core/expressions/isOnline';
 *
 * isOnline()
 * .then(() => {
 *    if (online) {
 *        // The device is online.
 *    } else {
 *        // The device is not online.
 *    }
 * })
 * .done();
 */
export function isOnline (input) {
    return Q.Promise((resolve) => {
        COOL.getClass('Utilities').isOnline((status) => {
            input.isOnline = status;

            resolve(status);
        })
        .done();
    });
}

ELF.expression('isOnline', isOnline);
