import ELF from 'core/ELF';

/**
 * Add an ELF Rule to intercept param function.
 * @param {Object} options - The options
 * @param {String} options.id - The id for the elf rule
 * @param {String} options.salience - The salience for the elf rule
 * @param {Function|String} options.action - The action called when ParamFunction is triggered.
 */
export function addParamFunctionTriggerRule ({ id, action, salience = 1 }) {
    if (ELF.rules.find(id)) {
        ELF.rules.remove(id);
    }

    ELF.rules.add({
        id,
        salience,
        trigger: 'PARAMFUNCTION:Trigger',
        evaluate: true,
        resolve: [{ action }]
    });
}

/**
 * A detector that will detect when param function is used,
 *  then loop through the provided screenshot values and re-render the screen with each value.
 */
export default class ParamFunctionDetector {
    constructor (renderFunc, takeScreenshot, cleanup = $.noop) {
        this.ELFId = 'ParamFunctionScreenshot';

        this.renderFunc = renderFunc;
        this.takeScreenshot = takeScreenshot;
        this.cleanup = cleanup;

        this.isDetected = false;
        this.namingIndex = 1;
    }

    /**
     * Function that sets up a listener that will call "detected" when the feature is detected.
     */
    setupDetectionListener () {
        addParamFunctionTriggerRule({
            id: this.ELFId,
            salience: 1,
            action: ({ key, paramFunction }) => this.detected(key, paramFunction)
        });
    }

    /**
     * Parse the param function's screenshot options and retrieve the list of values.
     * @param {Object} paramFunction - The parameter function object
     * @returns {Array} Array of all screenshot values.
     */
    parseParamFunctionValues (paramFunction = {}) {
        _.defaults(paramFunction, {
            screenshots: {
                values: [],
                getValues: $.noop
            }
        });

        const { values, getValues } = paramFunction.screenshots;

        // Start with static values
        let valueList = values || [];

        // Get values from provided function and add to list.
        valueList = valueList.concat(getValues && getValues() || []);

        if (!valueList.length) {
            valueList.push('{{ No param function screenshot values provided }}');
        }

        return valueList;
    }

    /**
     * Increment the current value of an item to use for the next screenshot.
     * @param {String} key - The key of the item to increment current value.
     * @returns {String}
     */
    increment (key) {
        const item = this.list.get(key);
        const text = item.get('values')[item.get('index')];

        if (item.get('index') < item.get('values').length - 1) {
            item.set('index', item.get('index') + 1);
        } else {
            item.set('completed', true);
        }

        return text;
    }

    /**
     * Run when ParamFunction is detected, then sets up or continues the param function screenshot loop, resolving the next value to display for screenshots.
     * @param {String} key - The key of the paramFunction function
     * @param {Object} paramFunction - The paramFunction object.
     * @returns {Object} object containing the evaluated param function.
     */
    detected (key, paramFunction) {
        this.isDetected = true;

        if (!this.list) {
            // Used to track multiple param function on a screen.
            this.list = new Backbone.Collection();
        }

        if (!this.list.has(key)) {
            let values = this.parseParamFunctionValues(paramFunction) || [];
            this.list.add({
                values,
                id: key,
                index: 0,
                completed: false
            });
        }

        let value = this.increment(key);

        // ELF Trigger expects an object.
        return { value };
    }

    /**
     * Render using the provided renderFunc, then run provided takeScreenshot function
     * and determine if there are more values to display for the param function on this screen.
     * @returns {Q.Promise<any>}
     */
    run () {
        if (!this.isDetected || !this.list || this.list.isEmpty()) {
            ELF.rules.remove(this.ELFId);
            return Q();
        }

        return this.takeScreenshot({ paramFunc: this.namingIndex })
        .then(() => this.cleanup())
        .then(() => {
            if (!this.list.findWhere({ completed: false })) {
                ELF.rules.remove(this.ELFId);
                return Q();
            }

            this.namingIndex++;

            return Q()
            .then(() => this.renderFunc())
            .then(() => this.run());
        });
    }
}
