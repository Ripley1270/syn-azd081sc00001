import ScreenshotController from './ScreenshotController';
import COOL from 'core/COOL';
import ELF from 'core/ELF';
import Data from 'core/Data';
import Subjects from 'core/collections/Subjects';
import Subject from 'core/models/Subject';
import Site from 'core/models/Site';
import Logger from 'core/Logger';
let logger = new Logger('ScreenshotRules');

export default class ScreenshotSetup {
    constructor (options = {}) {
        this.strings = options.strings || {};
        this.product = options.product || '';
    }

    /**
     * Create fake data in order to render certain screens.
     * @returns {Q.Promise}
     */
    static fakeData () {
        /**
         * Create a fake function for a wi dget
         * @param {string} text - The text to display instead.
         * @param {number} [count=1] - The amount of fake values to create, for use in lists.
         * @returns {function} fake widget function
         */
        function fakeWidgetObj (text, count = 1) {
            return () => {
                let arr = _.range(1, count + 1);

                return Q([{
                    id: '1',
                    text: _.map(arr, value => `--${text}:${value}--`)
                }]);
            };
        }

        /**
         * Create a fake function for the given object.
         * @param {Object} obj - The object that needs a fake function applied to it.
         * @param {function} [fake] - The fake function to apply, defaults to $.noop
         */
        function fakeObjectFunctions (obj, fake = $.noop) {
            _.chain(obj)
                .keys()
                .each((key) => {
                    // DE17599 - A few functions need to be singled out to prevent errors from
                    // being thrown and crashing the application on Windows.
                    switch (key) {
                        case 'barCodeList':
                            return obj[key] = fakeWidgetObj('fakeReviewScreenText', 3);
                        case 'medsLoop1':
                        case 'painNRS':
                            return obj[key] = fakeWidgetObj('fakeReviewScreenText', 2);
                        default:
                            return obj[key] = fake;
                    }
                });
        }

        fakeObjectFunctions(LF.Widget.ReviewScreen, fakeWidgetObj('fakeReviewScreenText'));
        fakeObjectFunctions(LF.DynamicText, callback => callback('{{ No dynamic text screenshot values provided }}'));

        let krpt = 'fake';

        localStorage.setItem('krpt', krpt);

        return Subjects.fetchCollection()
        .then((subjects) => {
            let subject = subjects.findWhere({ krpt });
            Data.subject = subject || new Subject({
                id: 1,
                subject_id: '123',
                krpt,
                subject_active: 1,
                phase: 10,
                site_code: '001',
                subject_number: '0001-0039'
            });
            return Data.subject.save();
        })
        .then(() => {
            Data.site = new Site();
            ELF.rules.remove('QuestionnaireScreenAuthenticationByRole');
        });
    }

    /**
     * Create the new Router.
     * @returns {Q.Promise}
     */
    createRouter () {
        const opts = {
            routerOpts: {
                routes: {
                    screenshot: 'screenshot#screenshot',
                    screenshot_take_screenshots: 'screenshot#takeScreenshots',
                    screenshot_mode: 'screenshot#screenshotModeSelection',
                    screenshot_diary: 'screenshot#screenshotDiary',
                    screenshot_page: 'screenshot#screenshotPage',
                    screenshot_messagebox: 'screenshot#screenshotMessagebox',
                    screenshot_next_mode: 'screenshot#nextMode'
                }
            },
            controllers: {
                screenshot: new ScreenshotController({
                    strings: this.strings,
                    diaryList: this.getDiaryList()
                })
            }
        };

        // Run whichever startup sequences are registered.
        const startups = ['Startup'];

        return Q.all(_.map(startups, (name) => {
            const Startup = COOL.getClass(name);
            return Startup && new Startup().backboneStartup(opts);
        }))
        .catch((err) => {
            err && logger.error('Error starting backbone', err);
        });
    }

    /**
     * Create the function to be used in the ELF rule action on screenshot mode entry.
     * @returns {function} The function to be used as an action.
     */
    screenshotAction () {
        return () => {
            return this.createRouter()
            .then(() => ScreenshotSetup.fakeData())
            .then(() => {
                LF.security.pauseSessionTimeOut();
                localStorage.setItem('screenshot', true);
            })
            .catch((err) => {
                logger.error('An error occurred', err);
            });
        };
    }

    /**
     * Create and return the action data to be used on screenshot entry.
     * @returns {Object[]}
     */
    createActionsList () {
        return [
            { action: this.screenshotAction() },
            { action: 'navigateTo', data: 'screenshot' },
            { action: 'preventDefault' }
        ];
    }

    /**
     * Get the list of diaries filtering out any that have "product" set to the opposite product.
     * @returns {Object[]} An array of diairies for this product, or all products.
     */
    getDiaryList () {
        this.fakeDatePickerDiaryConfig();

        return LF.StudyDesign.questionnaires.filter((questionnaire) => {
            const product = questionnaire.get('product');
            return product ? _.contains(product, this.product) : true;
        });
    }

    /**
     * US6879: Fake diary config for DatePicker screen-shots.
     */
    fakeDatePickerDiaryConfig () {
        const logpadID = 'P_DatePicker_Screenshot_Diary';
        const sitepadID = 'P_DatePicker_Screenshot_Diary_SitePad';

        // Questionnaire for DatePicker screen-shots
        LF.StudyDesign.questionnaires.add([{
            id: logpadID,
            SU: 'Date',
            displayName: 'DISPLAY_NAME',
            className: 'DATEPICKER_WIDGET_DIARY',
            affidavit: undefined,
            product: ['logpad'],
            screens: [
                'DATEPICKER_DIARY_S_1',
                'DATEPICKER_DIARY_S_2'
            ]
        }, {
            id: sitepadID,
            SU: 'Date',
            displayName: 'DISPLAY_NAME',
            className: 'DATEPICKER_WIDGET_DIARY',
            accessRoles: ['admin', 'site'],
            affidavit: undefined,
            product: ['sitepad'],
            screens: [
                'DATEPICKER_DIARY_S_1',
                'DATEPICKER_DIARY_S_2'
            ]
        }]);

        // Question for DatePicker screen-shots
        LF.StudyDesign.questions.add([{
            id: 'DATEPICKER_DIARY_Q_1',
            IG: 'Date',
            IT: 'DATE_1',
            text: ['QUESTION_1'],
            className: 'DATEPICKER_DIARY_Q_1',
            widget: {
                id: 'DATEPICKER_DIARY_W_1',
                type: 'DatePicker',
                className: 'DATEPICKER_DIARY_W_1',
                configuration: {
                    maxDays: 180,
                    useAnimation: false,
                    defaultValue: '2014-01-01'
                }
            }
        }, {
            id: 'DATEPICKER_DIARY_Q_2',
            IG: 'Date',
            IT: 'DATE_2',
            skipIT: 'Date_2_SKP',
            text: ['QUESTION_2'],
            className: 'DATEPICKER_DIARY_Q_2',
            widget: {
                id: 'DATEPICKER_DIARY_W_2',
                type: 'DatePicker',
                className: 'DATEPICKER_DIARY_W_1',
                configuration: {
                    minDays: 0,
                    maxDays: 1460,
                    useAnimation: false,
                    defaultValue: '2015-07-01',
                    useClearButton: true
                }
            }
        }]);

        // Screens for DatePicker screen-shots
        LF.StudyDesign.screens.add([{
            id: 'DATEPICKER_DIARY_S_1',
            className: 'DATEPICKER_DIARY_S_1',
            questions: [
                { id: 'DATEPICKER_DIARY_Q_1', mandatory: true }
            ]
        }, {
            id: 'DATEPICKER_DIARY_S_2',
            className: 'DATEPICKER_DIARY_S_2',
            questions: [
                { id: 'DATEPICKER_DIARY_Q_2', mandatory: false }
            ]
        }]);

        // Translated strings for DatePicker screen-shots
        LF.strings.add([{
            namespace: logpadID,
            language: 'en',
            locale: 'US',
            direction: 'ltr',
            resources: {
                DISPLAY_NAME: 'DatePicker Screenshot Diary',
                DATE_PICKER_HELP: 'Tap a date to select it.',
                QUESTION_1: 'On which day did you take your study medicine?',
                QUESTION_2: 'When would you like to schedule your appointment?'
            }
        }, {
            namespace: sitepadID,
            language: 'en',
            locale: 'US',
            direction: 'ltr',
            resources: {
                DISPLAY_NAME: 'DatePicker Screenshot Diary',
                DATE_PICKER_HELP: 'Tap a date to select it.',
                QUESTION_1: 'On which day did you take your study medicine?',
                QUESTION_2: 'When would you like to schedule your appointment?'
            }
        }]);
    }
}

