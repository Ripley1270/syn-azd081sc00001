import COOL from 'core/COOL';
import CurrentSubject from 'core/classes/CurrentSubject';
import ELF from 'core/ELF';
import Subjects from 'core/collections/Subjects';
import { MessageRepo } from 'core/Notify';
import WebService from 'core/classes/WebService';
import Spinner from 'core/Spinner';

/**
 * Handles the resetPassword transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise}
 */
export default function resetPassword (transmissionItem) {
    let resetPasswordSuccess,
        resetPasswordError,
        params = JSON.parse(transmissionItem.get('params')),
        mappedJSON = {
            W: params.password,
            Q: params.secret_question,
            A: params.secret_answer
        };

    return Subjects.getSubjectBy({ krpt: params.krpt })
    .then((subject) => {
        resetPasswordSuccess = ({ res, isSubjectActive }) => {
            return subject.save({
                service_password: res.W,
                subject_active: isSubjectActive
            })
            .then(() => {
                CurrentSubject.clearSubject();

                return this.destroy(transmissionItem.get('id'));
            });
        };

        resetPasswordError = ({ errorCode, httpCode, isSubjectActive }) => {
            const { Dialog } = MessageRepo;

            if (httpCode === LF.ServiceErr.HTTP_FORBIDDEN && errorCode === LF.ServiceErr.SUBJECT_NOT_FOUND) {
                // Delete clinical data
                return ELF.trigger('Termination', {
                    endParticipation: true,
                    subjectActive: !!isSubjectActive,
                    deviceID: subject.get('device_id')
                }, this)
                .finally(() => this.destroy(transmissionItem.get('id')));
            }

            if (!isSubjectActive) {
                return Spinner.hide()
                .then(() => LF.Actions.notify({
                    dialog: Dialog && Dialog.PASSWORD_CHANGE_FAILED
                }))
                .then(() => Spinner.show())
                .then(() => this.destroy(transmissionItem.get('id')));
            }

            if (httpCode !== LF.ServiceErr.HTTP_NOT_FOUND) {
                return LF.spinner.hide()
                .then(() => {
                    errorCode = errorCode ? `-${errorCode}` : '';

                    return LF.Actions.notify({
                        dialog: Dialog && Dialog.TRANSMISSION_ERROR_HTTP_AND_ERROR,
                        options: { httpCode, errorCode }
                    });
                })
                .then(() => LF.spinner.show())
                .then(() => {
                    this.remove(transmissionItem);
                });
            }

            this.remove(transmissionItem);

            // Error cases are handled within the transmit method.
            // No need to reject the promise chain. At least for now.
            return Q();
        };

        let service = COOL.new('WebService', WebService);

        return service.updateSubjectData(subject.get('device_id'), mappedJSON, null)
        .then(resetPasswordSuccess)
        .catch(resetPasswordError);
    });
}
