import COOL from 'core/COOL';

import getServerTime from './getServerTime';
import historicalDataSync from './historicalDataSync';
import resetPassword from './resetPassword';
import resetSecretQuestion from './resetSecretQuestion';
import transmitEditPatient from './transmitEditPatient';
import transmitQuestionnaire from './transmitQuestionnaire';
import transmitSubjectAssignment from './transmitSubjectAssignment';
import transmitUserData from './transmitUserData';
import updateUserCredentials from './updateUserCredentials';

let Transmit = {
    getServerTime,
    historicalDataSync,
    resetPassword,
    resetSecretQuestion,
    transmitEditPatient,
    transmitQuestionnaire,
    transmitSubjectAssignment,
    transmitUserData,
    updateUserCredentials
};

LF.Transmit = _.extend({}, LF.Transmit, Transmit);

COOL.service('Transmit', Transmit);

export default Transmit;
