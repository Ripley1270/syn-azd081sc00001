import User from 'core/models/User';
import Users from 'core/collections/Users';
import Logger from 'core/Logger';
import Transmission from 'core/models/Transmission';
import * as utils from 'core/utilities';
import { MessageRepo } from 'core/Notify';
import { createGUID } from 'core/utilities';
import UserSync from 'core/classes/UserSync';
import Spinner from 'core/Spinner';
import ELF from 'core/ELF';

let logger = new Logger('newUserSave');

/**
 * @memberOf ELF.actions
 * @method newUserSave
 * @description
 * Saves a new user and creates a new transmission record.
 * <br />
 * **Note**: _This is for core use only on New User forms._
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{
 *     action: 'newUserSave'
 * }]
 */
export default function newUserSave () {
    // eslint-disable-next-line consistent-return
    return Q.Promise((resolve) => {
        let affidavit = this.data.answers.match({ question_id: 'AFFIDAVIT' })[0],
            requiredResponses = _.invoke(this.data.answers.models, 'get', 'response').toString().match(/\{("role"|"language"|"username"|"password"|"salt"|"secretAnswer"|"secretQuestion"):.+?\}/g),
            parsedResponses = _.map(requiredResponses, res => JSON.parse(res)),
            answers = _.extend.apply(Object, _.filter(parsedResponses, res => _.isObject(res))),

            // Get the default user values from the study design if they exist.
            defaults = utils.getNested(LF, 'StudyDesign.defaultUserValues') || {};

        if (!answers.role) {
            logger.error('Action:newUserSave failed. No role provided.');

            Spinner.hide();

            return resolve();
        }

        // The userType can be modified by adding a hidden field in each diary.
        // By default, the userType will be 'SiteUser'.
        answers.userType = answers.userType || 'SiteUser';

        // Ensure the new user is active by default.
        answers.active = 1;

        // The salt can be set to 'temp' via a hidden field in each diary.
        // If the salt is not present, then one will be generated.
        answers.salt = answers.salt || createGUID();

        answers.password = hex_sha512(answers.password + answers.salt);

        if (answers.secretAnswer) {
            answers.secretAnswer = hex_sha512(answers.secretAnswer);
        }

        // Get the correct sync value for the assigned role.
        let getSyncValue = () => {
            // Get the sync level of the assigned role.
            let syncLevel = LF.StudyDesign.roles.findWhere({ id: answers.role }).get('syncLevel');

            if (syncLevel) {
                return UserSync.getValue(syncLevel);
            }
            return Q();
        };

        // Create the new user record.
        let createUser = (syncValue = '') => {
            let user = new User();

            defaults.syncValue = syncValue;
            answers = _.defaults(answers, defaults);

            if (user.isSchemaSet(answers)) {
                return user.save(answers)
                .then(() => user);
            }
            logger.error('User is missing required field and was not saved.');

            return Q().reject();
        };

        // Determine if the new user is the first of it's role.
        let isFirstOfRole = (user) => {
            let role = user.get('role'),
                users = new Users();

            // Fetch all users of the same role.
            return users.fetch({
                search: {
                    where: { role }
                }
            })
            .then(() => {
                // If there are no other users of the same role, set a value in localStorage.
                if (users.size() === 0) {
                    localStorage.setItem('isFirstOfRole', role);
                }
            });
        };

        // Create a transmission record to transmit the new user.
        let createTransmission = (user) => {
            let transmission = new Transmission({
                method: 'transmitUserData',
                params: JSON.stringify({
                    userId: user.get('id'),
                    affidavit: affidavit.get('SW_Alias'),
                    signature: this.data.ink
                }),
                created: new Date().getTime()
            });

            return transmission.save();
        };

        getSyncValue()
        .then(createUser)
        .tap(isFirstOfRole)
        .then(createTransmission)
        .then(() => {
            MessageRepo.display(MessageRepo.Banner.NEW_USER_CREATED);
        })
        .then(resolve)
        .catch((err) => {
            logger.error('Action:newUserSave failed.', err);

            Spinner.hide();

            resolve();
        })
        .done();
    });
}

ELF.action('newUserSave', newUserSave);
