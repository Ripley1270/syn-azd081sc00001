import ELF from 'core/ELF';
import * as syncHistoricalData from 'core/actions/syncHistoricalData';

/**
 * @memberOf ELF.actions
 * @method syncUsers
 * @description
 * Triggers the syncHistoricalData action for syncing Users.
 * @param {boolean} [rejectOnError=false] If true rejects the promise returned
 * @return {Q.Promise<void>}
 * resolve: [{ action: 'syncUsers' }]
 */
export function syncUsers (rejectOnError = false) {
    return syncHistoricalData.syncHistoricalData({
        collection: 'Users',
        webServiceFunction: 'syncUsers',
        activation: false
    }, rejectOnError);
}

ELF.action('syncUsers', syncUsers);
