import COOL from 'core/COOL';
import Users from 'core/collections/Users';
import Logger from 'core/Logger';
import FileHandler from 'core/classes/FileHandler';
import AWSS3Client from 'core/classes/AWSS3Client';
import * as CoreUtilities from 'core/utilities';

const logger = new Logger('getRaterTraining');

// A singleton promise, used to ensure that the download process is queued and not running simultaneously.
let getRaterTrainingPromise = Q();

/**
 * Factory for getting the rating S3 client, based on our rater training config constants.
 * @returns {AWSS3Client} S3 client configured with rater training configuration constants.
 */
export function getRatingS3Client () {
    let config = CoreUtilities.getRaterTrainingConfig();

    return new AWSS3Client(
        config.awsKey,
        config.awsSecretKey,
        config.awsRegion,
        config.awsBucket
    );
}

/**
 * Factory for getting the rating file handler, based on our rater training config constants.
 * @returns {FileHandler} file handler configured with rater training configuration constants.
 */
export function getRatingFileHandler () {
    return new FileHandler();
}

let downloadFiles = function (fileList) {
    let promises = [],
        fileHandler = getRatingFileHandler(),
        s3Client = getRatingS3Client();

    _.each(fileList, (file) => {
        promises.push(
            fileHandler.getSize(file.Key)
            .then((size) => {
                if (size !== file.Size && typeof file.Size !== 'undefined') {
                    logger.trace(`File: ${file.Key}.  Size mismatch (${size} : ${file.Size}.  Getting signed URL....`);

                    return s3Client.getDownloadURL(file.Key)
                    .then((url) => {
                        logger.trace(`File: ${file.Key}.  Downloading to file system....`);
                        return fileHandler.download(url, file.Key);
                    });
                } else if (typeof file.Size !== 'undefined') {
                    logger.trace(`File: ${file.Key}.  Skipping.  File exists with identical size.`);
                    return Q();
                }
                logger.trace(`File: ${file.Key}.  Skipping.  File does not exist on server.`);
                return Q();
            })
        );
    });

    return Q.all(promises);
};

let prepareLanguageRaterTraining = function (langCode) {
    let s3Client = getRatingS3Client(),
        config = CoreUtilities.getRaterTrainingConfig(),
        langOnly = langCode.split('-')[0],
        promises = [],
        zipFile = config.zipFile,
        lookupSuffix = zipFile ? '.zip' : '/',
        langCodeLookup = `${config.rootDirectory}/${langCode}${lookupSuffix}`,
        langOnlyLookup = `${config.rootDirectory}/${langOnly}${lookupSuffix}`,
        includeLangOnly = langOnly !== langCode;

    promises.push(s3Client.getFileListByPrefix(langCodeLookup));
    includeLangOnly && promises.push(s3Client.getFileListByPrefix(langOnlyLookup));

    return Q.all(promises)
    .then((results) => {
        return _.flatten(results);
    });
};

/**
 * Download the rater training files
 * @memberOf ELF.actions
 * @method getRaterTraining
 * @returns {Q.Promise}
 */
let getRaterTraining = function () {
    let users = new Users(),
        Utilities = COOL.getClass('Utilities', CoreUtilities),
        config = CoreUtilities.getRaterTrainingConfig(),
        langCodes = {};

    getRaterTrainingPromise = getRaterTrainingPromise.then(() => {
        return Utilities.isOnline();
    })
    .then((online) => {
        if (!online || !config.useRaterTraining) {
            return [];
        }
        return Q.Promise((resolve) => {
            LF.Wrapper.exec({
                execWhenWrapped: () => {
                    users.fetch()
                    .then(() => {
                        let langPromises = [],
                            defLangCode = `${LF.StudyDesign.defaultLanguage}-${LF.StudyDesign.defaultLocale}`.replace(/-$/, '');

                        // Always get the default language
                        langCodes[defLangCode] = true;
                        langPromises.push(prepareLanguageRaterTraining(defLangCode));

                        users.each((user) => {
                            let curLangCode = user.get('language');

                            if (curLangCode && !langCodes[curLangCode]) {
                                langCodes[curLangCode] = true;
                                langPromises.push(prepareLanguageRaterTraining(curLangCode));
                            }
                        });
                        resolve(langPromises);
                    });
                },
                execWhenNotWrapped: () => {
                    resolve([]);
                }
            });
        })
        .then((langPromises) => {
            return Q.all(langPromises);
        })
        .then((fileList) => {
            let allFiles = _.compact(_.flatten(fileList));
            return downloadFiles(allFiles);
        })
        .then(() => {
            // return an empty object for the ELF caller.
            return {};
        })
        .catch((e) => {
            // Kindly handle an error in this process.
            if (e && e.location === 'getFileList') {
                // Quit without doing anything special, if we just couldn't get a file list.
                logger.error('error getting rater training file list... quitting...', e);
                return Q();
            } else if (config.zipFile) {
                logger.error('error retrieving zip file... quitting...', e);
                return Q();
            }

            // If the failure was on actual file downloads (and they were busted out, not zipped),
            //  remove index.html files, so incomplete packages are not shown.
            logger.error(`error getting rater training.  ${e.toString()} ... removing index.html files...`);

            let removePromises = [],
                rootDirectory = config.rootDirectory,
                pages = config.pages,
                fileHandler = getRatingFileHandler();
            _.each(_.keys(langCodes), (langCode) => {
                _.each(_.keys(pages), (page) => {
                    removePromises.push(fileHandler.remove(`${rootDirectory}/${langCode}/${page}`));
                });
            });

            return Q.all(removePromises);
        });
    });
    return getRaterTrainingPromise;
};

/**
 * Start downloading of the rater training files in the background
 * @memberOf ELF.actions
 * @method launchGetRaterTraining
 * @returns {Q.Promise}
 */
let launchGetRaterTraining = function () {
    getRaterTraining();

    // return separate promise resolving immediately so rater training can run in the background.
    return Q();
};

ELF.action('getRaterTraining', getRaterTraining);
ELF.action('launchGetRaterTraining', launchGetRaterTraining);
