import ELF from '../ELF';

/**
 * @memberOf ELF.actions
 * @method removeRules
 * @description
 * Remove ELF rules by ID.
 * @param {Object} data - Action configuration.
 * @param {Array<string>} data.rules - An array of rule ID's to remove.
 * @param {Function} done - Invoked upon completion of the action.
 * @example
 * resolve: [{
 *     action: 'removeRules',
 *     data: { rules: ['ruleOne', 'ruleTwo'] }
 * }]
 */
export function removeRules (data, done) {
    if (data.rules) {
        _.forEach(data.rules, (id) => {
            ELF.rules.remove(id);
        });
    }

    done();
}

ELF.action('removeRules', removeRules);
