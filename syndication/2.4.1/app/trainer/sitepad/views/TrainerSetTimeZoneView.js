import COOL from 'core/COOL';
import SetTimeZoneView from 'sitepad/views/SetTimeZoneView';

/**
 * Extends SetTimeZoneActivationView to fake setting time zone on trainer
 * @function extendSetTimeZoneView
 */
export function extendSetTimeZoneView () {
    /**
     * A class that extends the sitepad SetTimeZoneView view.
     * @class TrainerSetTimeZoneView
     * @extends SetTimeZoneView
     */
    class TrainerSetTimeZoneView extends COOL.getClass('SetTimeZoneView', SetTimeZoneView) {
        /**
         * Resolve any dependencies required for the view to render.
         * @returns {Q.Promise<void>}
         */
        resolve () {
            this.timeZonesDisplay = [{ id: 'America/New_York', displayName: '(GMT-05:00) America/New_York' }];
            this.currentTimeZone = { id: 'America/New_York', displayName: '(GMT-05:00) America/New_York' };
            return Q();
        }

        /**
         * Overrides the render to enable the next button
         * @returns {Q.Promise<void>}
         */
        render () {
            return super.render()
            .then(() => {
                return this.enableButton(this.$set);
            });
        }

        /**
         * Changes the Timezone. In trainer, just go to the next screen.
         * @returns {Q.Promise<void>}
         */
        setTimeZone () {
            return this.back();
        }
    }

    COOL.add('SetTimeZoneView', TrainerSetTimeZoneView);
}
