import ELF from 'core/ELF';
import Logger from 'core/Logger';

const logger = new Logger('shouldSkipTimeSlip');

/**
 * Determines if time slip should be checked.
 * @memberOf ELF.expressions/web
 * @method shouldSkipTimeSlip
 * @returns {Q.Promise<boolean>}
 * @example
 * evaluate: 'shouldSkipTimeSlip'
 */
export default function shouldSkipTimeSlip () {
    if (LF.environment.checkForTimeSlip === false) {
        logger.operational('TimeSlipCheck is skipped due to studyDesign flag "checkForTimeSlip".');
        return Q(true);
    }

    return Q(false);
}

ELF.expression('shouldSkipTimeSlip', shouldSkipTimeSlip);
