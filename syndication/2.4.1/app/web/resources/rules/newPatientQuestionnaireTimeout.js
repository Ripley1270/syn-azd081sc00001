// Workflow logic for questionnaire timeout while new site user creation on the web app.
// It runs the same actions as it would for unsigned affidavit on SitePad.
const newPatientQuestionnaireTimeout = {
    id: 'newPatientQuestionnaireTimeout',
    trigger: 'QUESTIONNAIRE:QuestionnaireTimeout/New_Patient',
    salience: 3,
    resolve: [
        { action: 'navigateTo', data: 'home' },
        { action: 'notify', data: { key: 'DIARY_TIMEOUT' } },
        { action: 'preventAll' }
    ]
};

export default newPatientQuestionnaireTimeout;
