import EULAView from 'core/views/EULAView';
import COOL from 'core/COOL';
import Logger from 'core/Logger';
import State from 'web/classes/State';
import ELF from 'core/ELF';
import * as lStorage from 'core/lStorage';
import CurrentContext from 'core/CurrentContext';

const logger = new Logger('WebEULAView');

/*
 * View for EULA page, user's acceptance is required for activation.
 * @class WebEULAView
 * @extends EULAView
 */
export default class WebEULAView extends COOL.getClass('EULAView', EULAView) {
    /**
     * Navigate to the next view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    next (evt) {
        logger.operational('EULA is accepted by the first site user.');
        super.next(evt);

        State.set(State.states.locked);
        this.navigate('unlock-code');
    }

    /**
     * Navigate back to the previous view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    back (evt) {
        logger.operational('EULA is declined by the first site user.');
        super.back(evt);

        ELF.trigger('APPLICATION:Uninstall', {}, this)
            .done();
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise<Object>}
     */
    render () {
        let preferredLanguage = lStorage.getItem('preferredLanguageLocale');
        if (preferredLanguage) {
            // DE22806 fix
            CurrentContext().setContextLanguage(preferredLanguage);
        }
        return super.render();
    }
}

COOL.add('WebEULAView', WebEULAView);
