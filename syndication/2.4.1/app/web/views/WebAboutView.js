import SitePadAboutView from 'sitepad/views/AboutView';
import * as lStorage from 'core/lStorage';

export default class WebAboutView extends SitePadAboutView {
    /**
     * Creates the parameters for the template
     * @returns {Q.Promise<Object>}
     */
    getTemplateParameters () {
        return super.getTemplateParameters()
        .then((parameters) => {
            parameters.studyURL = lStorage.getItem('NetProURL');

            return parameters;
        });
    }
}
