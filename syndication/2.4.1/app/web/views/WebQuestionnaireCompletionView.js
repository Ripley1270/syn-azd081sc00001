import QuestionniareCompletionView from 'core/views/QuestionnaireCompletionView';

/**
 * Handles questionnaire completion workflow.
 * @class WebQuestionnaireCompletionView
 * @extends QuestionniareCompletionView
 */
export default class WebQuestionnaireCompletionView extends QuestionniareCompletionView {
    /**
     * Transmit the questionnaire and navigate to dashboard
     * @return {Q.Promise} resolves when transmission is done.
     */
    transmitQuestionnaire () {
        // DE22251 this fixes the current state for IE
        // to assure Back Button Banner doesn't show.
        this.replaceState();

        return super.transmitQuestionnaire();
    }

    /**
     * Change the state of the window.history.state
     */
    replaceState () {
        window.history.replaceState(null, null);
    }
}
