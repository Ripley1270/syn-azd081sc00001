import CoreState from 'core/classes/State';

export default class State extends CoreState {
    /**
     * @property {Object} states - The states for the web modality
     * @property {string} states.new - The default state for a new web browser
     * @property {string} states.registered - Site has been resgistered but still locked
     * @property {string} states.locked - The browser instance needs unlock code entry
     * @property {string} states.activated - The browser is fully registered/activated and ready for use.
     * @property {String} states.endUserLicenseAgreements - EULA needs to be accepted by the first user.
     * @readonly
     * @static
     */
    static get states () {
        return {
            new: 'NEW',
            registered: 'REGISTERED',
            locked: 'LOCKED',
            activated: 'ACTIVATED',
            endUserLicenseAgreements: 'END_USER_LICENSE_AGREEMENTS'
        };
    }

    /**
     * @property {string} initialState - The initial state for the web modality
     * @readonly
     * @static
     */
    static get initialState () {
        return this.states.new;
    }
}
