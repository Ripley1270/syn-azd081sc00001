import ELF from 'core/ELF';
import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';
import * as lStorage from 'core/lStorage';

/**
 * Get the locked out sites from server side.
 * @memberOf ELF.actions/web
 * @method fetchLockedOutSites
 * @returns {Q.Promise<void>}
 */
export function fetchLockedOutSites () {
    const service = COOL.new('WebService', WebService);

    // TODO: Either return the promise or finalize it with .done()
    service.getLockedOutSites()
    .then((sites) => {
        lStorage.setItem('LockedOutSites', JSON.stringify(sites.res));
    })
    .catch(() => {
        return Q.reject({ preventActions: true });
    });
}

ELF.action('fetchLockedOutSites', fetchLockedOutSites);
