import setupCoreMessages from 'core/resources/Messages';
import setupLogpadBanners from './Banners';
import setupLogpadDialogs from './DialogBoxes';

export default function setupLogpadMessages () {
    setupCoreMessages();
    setupLogpadBanners();
    setupLogpadDialogs();
}
