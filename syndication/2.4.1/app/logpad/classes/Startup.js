import Logger from 'core/Logger';
const logger = new Logger('Startup');

logger.traceEnter('Import Startup');
logger.trace('Import phase I');

import trial from 'logpad/trialMerge'; //PDE UPDATE: use modality specific trial
import { setServiceBase } from 'core/utilities/coreUtilities';
import StudyDesign from 'core/classes/StudyDesign';

import Router from 'logpad/Router';
import ApplicationController from 'logpad/controllers/ApplicationController';
import RegistrationController from 'logpad/controllers/RegistrationController';
import SettingsController from 'logpad/controllers/SettingsController';

logger.trace('Import phase II');
import ELF from 'core/ELF';
import COOL from 'core/COOL';
import WebService from 'logpad/classes/WebService';
import AlarmManager from 'core/AlarmManager';
import setupLogpadMessages from 'logpad/resources/Messages';

// Transmit is imported to bootstrap the module into COOL.
import 'logpad/transmit';

// - Core Modules
import * as Helpers from 'core/Helpers';
import * as Utilities from 'core/utilities';

logger.trace('Import phase III');

import Users from 'core/collections/Users';

// The following modules are imported so some value, or another, can be bootstrapped.
import 'core/collections/Languages';
import 'core/collections/Templates';
import 'core/collections/StoredSchedules';
import 'core/collections/Logs';
import 'core/collections/SubjectAlarms';
import 'logpad/scheduling/DisplayItem';
logger.traceExit('Startup');

import BaseStartup from 'core/classes/BaseStartup';

export default class Startup extends BaseStartup {
    /**
     * Constructs key study components such as the Study Design, Templates and Web Service.
     * @returns {Q.Promise<void>}
     */
    studyStartup () {
        return Q.Promise((resolve) => {
            logger.traceEnter('studyStartup');

            // Set the study design in the global namespace.
            logger.trace(`Processing study design: ${trial.assets.studyDesign}`);
            LF.StudyDesign = COOL.new('StudyDesign', StudyDesign, trial.assets.studyDesign);

            setServiceBase();

            // Add the custom trial-specific study rules
            trial.assets.trialRules(ELF.rules);

            // Add the custom study templates to the global collection.
            logger.trace('Adding templates...');
            LF.templates.add(trial.assets.studyTemplates);

            // Add the custom study web service to the global namespace.
            LF.studyWebService = COOL.new('WebService', trial.assets.studyWebService);

            // PDE UPDATE: add custom study messages
            trial.assets.studyMessages();

            logger.traceExit('studyStartup');
            resolve();
        });
    }
    /**
     * Create the router.
     * @param {Object} [options={}] options passed into the Router's constructor.
     * @returns {Q.Promise<Router>}
     */
    createRouter (options = {}) {
        logger.traceEnter('routerStartup');

        const application = new ApplicationController();
        const registration = new RegistrationController();
        const settings = new SettingsController();
        const study = new trial.assets.StudyController();

        const routerOpts = _.extend({}, options.routerOpts, {
            controllers: _.extend({}, options.controllers, {
                application, registration, settings, study
            })
        });

        return Q(new Router(routerOpts));
    }

    /**
     * Setup the application's router.
     * @param {Object} [options={}] - Options passed into the Router's constructor.
     * @returns {Q.Promise<void>}
     */
    backboneStartup (options = {}) {
        logger.traceEnter('backboneStartup');

        return Q()
        .then(() => this.createRouter(options))
        .then((router) => {
            LF.router = router;

            // Add the web service to the LF namespace.
            // @TODO: MOVE THIS!!!
            LF.webService = COOL.new('WebService', WebService);
            logger.traceExit('backboneStartup');
        })
        .catch((err) => {
            logger.error('Error on backbone startup', err);
        });
    }

    /**
     * Sets up application specific services such as alarms.
     * @returns {Q.Promise<void>}
     */
    applicationStartup () {
        logger.traceEnter('applicationStartup');

        return Q.Promise((resolve) => {
            if (LF.Preferred) {
                Utilities.setLanguage(`${LF.Preferred.language}-${LF.Preferred.locale}`,
                    LF.strings.getLanguageDirection());
            }

            // Checks to see if application was started because of an alarm
            LF.Wrapper.exec({
                execWhenWrapped: () => {
                    // Setup Alarm events.
                    // TODO: Should we wait until this is resolved to continue?
                    AlarmManager.registerPluginEvents();
                }
            });

            logger.traceExit('Done with application setup');
            resolve();
        })
        .catch((err) => {
            logger.error('Error during application startup', err);
        });
    }

    /**
     * Checks localStorage support and handles orientaion change of the device.
     * @returns {Q.Promise<void>}
     */
    finalStartup () {
        return Q()
        .then(() => {
            logger.traceEnter('finalStartup');
            Helpers.checkLocalStorage();
            Helpers.handleOrientationChange();
            logger.traceExit('finalStartup');
        })
        .catch((err) => {
            logger.error('Error on final startup', err);
        });
    }

    /**
     * Runs prior to any other startup method.
     * @returns {Q.Promise<void>}
     */
    preSystemStartup () {
        logger.traceEnter('preSystemStartup');

        return super.preSystemStartup()
        .then(() => {
            // If in browser, resize to aspect ratio approximating phone (9:16)
            // (Note: using actual pixel dimensions of even low-end phone would result
            // in window too big to fit on typical monitor)
            LF.Wrapper.exec({
                execWhenNotWrapped: () => {
                    let width = 480,
                        height = width * 16 / 9;
                    window.resizeTo(
                        width + (window.outerWidth - window.innerWidth),
                        height + (window.outerHeight - window.innerHeight)
                    );
                }
            });
        })
        .finally(() => logger.traceExit('preSystemStartup'));
    }

    /**
     * Start Backbone.history.
     * @returns {Q.Promise<void>}
     */
    startUI () {
        return Q()
        .then(() => {
            logger.traceEnter('startUI');
            let b = Backbone.history.start({ pushState: false, silent: true });

            // hack so backbone things route it changing.  Unfortunately it sets fragment to ''
            // if silent: true, even though it never actually navigated anywhere.
            Backbone.history.fragment = undefined;
            logger.trace(`startUI backbone history start: ${b}`);
            logger.traceExit('startUI');
        })
        .catch((err) => {
            logger.error('Error starting UI', err);
        });
    }

    /**
     * Setup the local users and context.
     * @returns {Q.Promise<void>}
     */
    setupUsersAndContext () {
        return Users.fetchCollection()
        .then(users => users.updateAdminUser())
        .then(() => super.setupUsersAndContext());
    }

    /**
     * Startup the application.
     * @returns {Q.Promise<void>}
     */
    startup () {
        logger.trace('startup.');
        setupLogpadMessages();
        Utilities.storeUrlParams();
        LF.Wrapper.detectWrapper();

        // Only because it makes the below nicer to look at
        return Q()
        .then(() => this.preSystemStartup())
        .then(() => this.jQueryStartup())
        .then(() => this.cordovaStartup())
        .then(() => this.studyStartup())
        .then(() => this.backboneStartup())
        .then(() => this.applicationStartup())
        .then(() => this.setupUsersAndContext())
        .then(() => this.finalStartup())
        .then(() => this.startUI())
        .then(() => this.notifyStudyDesignValidation())
        .then(() => ELF.trigger('APPLICATION:Loaded'))
        .catch(e => logger.error('Exception in startup:', e));
    }
}
