import COOL from 'core/COOL';
import CurrentSubject from 'core/classes/CurrentSubject';
import * as lStorage from 'core/lStorage';
import Spinner from 'core/Spinner';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';
import CurrentContext from 'core/CurrentContext';
import WebService from 'core/classes/WebService';
import ActiveAlarms from 'core/collections/ActiveAlarms';
import Users from 'core/collections/Users';

let logger = new Logger('Transmit.subjectSync');

/**
 * Handles the subjectSync transmission to the web-service.
 * @param {Object} incomingSite - Site object that is retrieved from SW prior to subject sync. Site object contains site number and krdom
 * @returns {Q.Promise}
 */
export default function subjectSync (incomingSite) {
    let currentSite,
        subjectActive,
        subjectSyncSuccess,
        subjectSyncError,
        syncedSubjectData,
        queryList = ['Phase', 'PhaseStartDate', 'Initials', 'SiteCode', 'LogLevel', 'SubjectNumber', 'EnrollmentDate', 'ActivationDate', 'Language'];

    return CurrentSubject.getSubject()
    .then((subject) => {
        subjectActive = subject.get('subject_active');

        let service = COOL.new('WebService', WebService);

        subjectSyncSuccess = ({ res, syncID, isSubjectActive }) => {
            let subjectSave = () => {
                let deviceSyncID = parseInt(localStorage.getItem('PHT_syncID'), 10),
                    serverSyncID = parseInt(syncID, 10),
                    saveSyncID = () => {
                        if (!lStorage.getItem('outOfSync')) {
                            localStorage.setItem('PHT_syncID', serverSyncID);
                            return Q();
                        }

                        if (deviceSyncID !== serverSyncID) {
                            lStorage.removeItem('outOfSync');
                            localStorage.setItem('PHT_syncID', serverSyncID);
                            return Q();
                        }

                        return Spinner.hide()
                        .then(() => LF.Actions.confirm({
                            dialog: MessageRepo.Dialog && MessageRepo.Dialog.SYNC_FAILED
                        }))
                        .tap(() => Spinner.show())
                        .then((result) => {
                            if (result) {
                                return service.querySubjectData(subject.get('subject_id'), queryList, null)
                                .catch(subjectSyncError)
                                .then(subjectSyncSuccess);
                            }

                            logger.operational('Subject canceled synchronization with the server.');
                            return Q();
                        });
                    },
                    updateLanguage = (language) => {
                        return CurrentContext().fetchUsers()
                        .then((users) => {
                            let subjectUser = users.findWhere({ id: subject.get('user') });
                            let currentUserID = parseInt(lStorage.getItem('User_Login'), 10);
                            localStorage.setItem('preferredLanguageLocale', language);

                            if (currentUserID === subject.get('user')) {
                                CurrentContext().setContextLanguage(language);
                            }

                            return subjectUser.save({ language });
                        })
                        .then(saveSyncID);
                    };

                currentSite = JSON.parse(lStorage.getItem('site'));

                // If device site number matches with incoming site number, always update krdom in the localstorage
                if (incomingSite && (incomingSite.sitecode === currentSite.sitecode)) {
                    lStorage.setItem('site', JSON.stringify(incomingSite));
                } else {
                    // reset PHT_lastUpdatedUsers so that all users of 'site' sync level are pulled correctly from server during sync
                    localStorage.removeItem('PHT_lastUpdatedUsers');
                }

                // Don't update subject unless we get a response or device has been replaced
                if (!_.isEmpty(res) || subjectActive !== isSubjectActive) {
                    let mappedJSON = {
                        subject_active: isSubjectActive,
                        phaseTriggered: 'false',
                        initials: res.I || subject.get('initials'),
                        log_level: res.L || subject.get('log_level'),
                        site_code: res.C || subject.get('site_code'),
                        subject_number: res.N || subject.get('subject_number'),
                        enrollmentDate: res.E || subject.get('enrollmentDate'),
                        activationDate: res.A || subject.get('activationDate')
                    };

                    // Update phase data only if incoming timestamp is greater than the current phaseStartDateTZOffset.
                    // Otherwise ignore phase data recevived for the subject. See DE19851
                    if (res.T && new Date(res.T) > new Date(subject.get('phaseStartDateTZOffset'))) {
                        mappedJSON.phase = res.P || subject.get('phase');
                        mappedJSON.phaseStartDateTZOffset = res.T || subject.get('phaseStartDateTZOffset');
                    }

                    // If site number is changed and we received corresponding krdom, update krdom in the localstorage
                    if (incomingSite && (incomingSite.sitecode === res.C)) {
                        lStorage.setItem('site', JSON.stringify(incomingSite));
                    }

                    mappedJSON.subject_password = syncedSubjectData.clientPassword;

                    if (LF.StudyDesign.askSecurityQuestion === true) {
                        mappedJSON.secret_answer = syncedSubjectData.securityAnswer;
                        mappedJSON.secret_question = `${syncedSubjectData.securityQuestion}`;
                    }

                    mappedJSON.custom10 = syncedSubjectData.custom10;

                    return subject.save(mappedJSON)
                    .then(() => Users.fetchCollection())
                    .then(users => users.updateSubjectUser(subject))
                    .then(() => {
                        CurrentContext().get('users').reset();

                        if (res.X) {
                            CurrentContext().get('subjects').reset();
                            return updateLanguage(res.X);
                        }

                        return saveSyncID();
                    });
                }

                return saveSyncID();
            };

            let cancelAllAlarms = () => {
                return Q.Promise((resolve) => {
                    LF.Wrapper.exec({
                        execWhenWrapped () {
                            let alarms = new ActiveAlarms();
                            alarms.cancelAllAlarms()
                            .catch(e => logger.error('An error occurred during canceling all alarms', e))
                            .finally(resolve)
                            .done();
                        },
                        execWhenNotWrapped () {
                            resolve();
                        }
                    });
                });
            };

            return service.getSingleSubject(subject.get('krpt'), null)
            .then(({ res }) => {
                syncedSubjectData = res;

                if (!isSubjectActive) {
                    logger.operational('Subject\'s device has been replaced.');

                    return cancelAllAlarms()
                    .then(() => ELF.trigger('Termination', {
                        endParticipation: false,
                        subjectActive: !!isSubjectActive,
                        deviceID: subject.get('device_id')
                    }, LF.Actions))
                    .catch((e) => {
                        logger.error('An error occurred ending device use', e);
                        return Q.reject(e);
                    })
                    .finally(subjectSave);
                }

                return subjectSave();
            });
        };

        subjectSyncError = ({ errorCode, httpCode, isSubjectActive }) => {
            let participationEnded = (httpCode === LF.ServiceErr.HTTP_FORBIDDEN && errorCode === LF.ServiceErr.SUBJECT_NOT_FOUND);

            if (participationEnded || !isSubjectActive) {
                if (participationEnded) {
                    logger.operational('Subject participation has ended.');
                }

                // Delete clinical data
                return ELF.trigger('Termination', {
                    endParticipation: true,
                    subjectActive: !!isSubjectActive,
                    deviceID: subject.get('device_id')
                }, LF.Actions);
            }

            return Q.reject();
        };

        return service.querySubjectData(subject.get('subject_id'), queryList, null)
        .catch(subjectSyncError)
        .then(subjectSyncSuccess);
    });
}
