import Sites from 'core/collections/Sites';
import CurrentSubject from 'core/classes/CurrentSubject';
import syncHistoricalData from 'core/actions/syncHistoricalData';
import ELF from 'core/ELF';

/**
 * Syncs site access code with the Web Service.
 * @memberOf ELF.actions/logpad
 * @method siteAccessCodeSync
 * @returns {Q.Promise}
 * @example
 * resolve: [{ action: 'siteAccessCodeSync' }]
 */
export default function siteAccessCodeSync () {
    let sync = () => {
        return syncHistoricalData({
            collection: 'Sites',
            webServiceFunction: 'getSiteAccessCode',
            activation: false
        });
    };

    if (LF.StudyDesign.siteMode !== true) {
        return Q();
    }

    return Q.all([Sites.fetchFirstEntry(), CurrentSubject.getSubject()])
    .spread((site, subject) => {
        if (!site || site.get('site_code') !== subject.get('site_code')) {
            return sync();
        }

        return Q();
    });
}

ELF.action('siteAccessCodeSync', siteAccessCodeSync);
