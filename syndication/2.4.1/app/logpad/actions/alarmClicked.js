// TODO: This file should be moved to expressions folder.
// Not making the change in 2.4.1 to avoid regression
import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions/logpad
 * @method alarmClicked
 * @description
 * Determines if the given alarm was actually clicked by the user.  Used only
 * in combination with the isIOS evaluation.
 * @param {object} alarm - Event data passed along by the triggered event.
 * @param {string} alarm.wasClicked - Whether the alarm was clicked by the user
 * @returns {Q.Promise<boolean>}
 * @example
 * trigger: 'QUESTIONNAIRE:Navigate/Activate_User/ACTIVATE_USER_S_1',
 * evaluate: 'alarmClicked'
 */
export function alarmClicked (alarm) {
    return Q(alarm.alarmConfig.wasClicked || false);
}

ELF.expression('alarmClicked', alarmClicked);
