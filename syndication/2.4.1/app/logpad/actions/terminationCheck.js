import ELF from 'core/ELF';
import Logger from 'core/Logger';
import CurrentSubject from 'core/classes/CurrentSubject';

let log = new Logger('TerminationCheck');

/**
 * @memberOf ELF.actions/logpad
 * @method terminationCheck
 * @description
 * Check if the subject has been terminated
 * @param {null|undefined} params Additional parameters passed. NOT used in this action.
 * @param {Function} callback Callback function to be invoked when the process has completed.
 * @example
 * resolve: [{ action: 'terminationCheck' }]
 */
export default function terminationCheck (params, callback) {
    CurrentSubject.getSubject()
    .then((subject) => {
        if (subject.get('phase') === LF.StudyDesign.terminationPhase) {
            log.operational('Subject participation has ended.');

            ELF.trigger('Termination', {
                endParticipation: true,
                subjectActive: true,
                deviceID: subject.get('device_id')
            }, this)
            .done(callback);
        } else {
            callback();
        }
    })
    .catch(callback);
}

ELF.action('terminationCheck', terminationCheck);
