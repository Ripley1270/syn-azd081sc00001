// TODO - This is being imported, but not used.  Instead LF.Collection.Indicators is.
// I left this as is for now, but should be investigated.
// eslint-disable-next-line no-unused-vars
import Indicators from 'core/collections/Schedules';
import Subjects from 'core/collections/Subjects';
import Users from 'core/collections/Users';
import * as lStorage from 'core/lStorage';
import CurrentContext from 'core/CurrentContext';
import Logger from 'core/Logger';
import CoreLogin from 'core/views/LoginView';
import CurrentSubject from 'core/classes/CurrentSubject';
import Spinner from 'core/Spinner';
import { rescheduleAlarms, getCurrentProtocol } from 'core/Helpers';

const logger = new Logger('LoginView');

export default class LoginView extends CoreLogin {
    constructor (options = {}) {
        super(options);

        /**
         * @property {string} template A selector that points to the template for the view.
         * @readonly
         * @default '#login-tpl'
         */
        this.template = '#login-tpl';

        /**
         * @property {object} events A list of DOM events.
         * @readonly
         */
        this.events = _.defaults({
            'click #sync-button': 'transmit'
        }, this.events);

        /**
         * @property {object} templateStrings A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = _.defaults({
            header: 'APPLICATION_HEADER',
            sync: 'SYNC',
            subjectId: 'SUBJECT_ID',
            study: 'PROTOCOL'
        }, this.templateStrings);

        /**
         * @property {Object<string, string>} stringValues - Override strings with passed in values
         */
        this.stringValues = options.stringValues || {};

        logger.traceEnter('Constructor');

        // TODO: Check if this.subjects is being used somewhere? Removed if not being used.
        /**
         * @property {Subjects} subjects A collection of subjects.
         */
        this.subjects = new Subjects();

        logger.traceExit('Constructor');
    }

    /**
     * @property {string} id The ID of the view.
     * @readonly
     * @default 'login-view'
     */
    get id () {
        return 'login-view';
    }

    /**
     * @property {Object<string,string>} selectors - A list of selectors to generate.
     */
    get selectors () {
        return _.defaults({
            sync: '#sync-button'
        }, super.selectors);
    }

    /**
     * performss anyasynchronoussinitializationn
     * @returns {Q.Promise<void>}
     */
    resolve () {
        this.resetModels();

        if (lStorage.getItem('rescheduleAlarms')) {
            rescheduleAlarms();
            lStorage.removeItem('rescheduleAlarms');
        }

        return Q.all([this.users.fetch(), this.subjects.fetch()])
        .then(() => CurrentSubject.getSubject())
        .then((subject) => {
            this.user = this.users.findWhere({ role: 'subject' });
            this.subject = subject;
            LF.DynamicText.subject = subject;
            return CurrentContext().setContextByUser(this.user);
        })
        .then(() => {
            if (this.subject) {
                localStorage.removeItem('Forgot_Password');

                this.listenTo(CurrentContext().get('schedules'), 'add change remove', () => {
                    this.indicatorSchedules = new LF.Collection.Schedules(this.filterSchedules(CurrentContext().get('schedules')));

                    LF.schedule.determineSchedules(this.indicatorSchedules, {
                        subject: this.subject
                    }, (availableSchedules) => {
                        LF.schedule.availableSchedules.set(this.filterSchedules(availableSchedules));
                    });
                });
            }
        });
    }

    /**
     * Navigate to the forgot password view.
     */
    forgotPassword () {
        this.close();
        super.forgotPassword();
    }

    /**
     * Render the view to the DOM.
     * example this.render();
     * @returns {Q.Promise<LoginView>} promise resolved when render is complete
     */
    render () {
        const { siteCode, subjectNumber } = this.stringValues;

        return Spinner.show()
        .then(() => {
            this.close();
        })
        .then(() => this.buildHTML({
            siteNumber: siteCode || this.subject.get('site_code'),
            version: LF.coreVersion,
            studyName: getCurrentProtocol(this.subject),
            subjectNumber: subjectNumber || this.subject.get('subject_number'),
            inputType: this.getRoleInputType(this.user && this.user.get('role'))
        }, true))
        .then(() => this.getPendingReportCount())
        .then((count) => {
            this.$pendingTransmissionCount = this.$('#pending-transmission-count');
            this.$pendingTransmissionCount.html(count);
        })
        .then(() => {
            let indicatorSchedules = new LF.Collection.Schedules(this.filterSchedules(CurrentContext().get('schedules')));

            this.delegateEvents();

            LF.schedule.determineSchedules(indicatorSchedules, {
                subject: this.subject
            }, (availableSchedules) => {
                let schedules = LF.schedule.availableSchedules;

                this.stopListening(LF.schedule.availableSchedules);

                if (availableSchedules) {
                    schedules.set(this.filterSchedules(availableSchedules));

                    LF.Schedule.DisplayItem.addToView(this, (views) => {
                        this.listViews = views;
                    });
                }
            });
        })
        .then(() => this.userLoginSetup())
        .then(() => Spinner.hide());
    }

    /**
     * Clear listViews then close.
     */
    close () {
        this.clear();

        super.remove();
    }

    /**
     * Cleanly close all listViews to make sure any listeners are destroyed.
     */
    clear () {
        this.listViews && Object.keys(this.listViews).forEach((index) => {
            this.listViews[index].close();
        });
    }

    /**
     * Reset the schedulable models.
     */
    resetModels () {
        let indicators = JSON.parse(JSON.stringify(LF.StudyDesign.indicators));

        // reset indicator models before new schedule evaluation
        if (this.indicators) {
            this.indicators.reset(indicators);
        } else {
            this.indicators = new LF.Collection.Indicators(indicators);
        }
    }

    /**
     * Filters the schedules to those allowed on this view.
     * @param {Object} availableSchedules - the schedules to be filtered
     * @returns {Object} The filtered schedules.
     */
    filterSchedules (availableSchedules) {
        let indicatorSchedules;

        if (availableSchedules) {
            indicatorSchedules = availableSchedules.filter(schedule => schedule.get('target').objectType === 'indicator' && schedule.get('target').showOnLogin === true);
        }

        return indicatorSchedules;
    }

    /**
     * Filters the schedules to those allowed on this view.
     * @returns {Q.Promise<void>}
     */
    transmit () {
        let selectedUserId;

        LF.security.pauseSessionTimeOut();

        return this.triggerTransmitRule()
        .then(() => Spinner.show())
        .finally(() => {
            LF.security.restartSessionTimeOut();
        })
        .then((flags) => {
            if (flags && flags.preventDefault) {
                return Q();
            }

            // TODO: Instead clear the CurrentSubject cache in subject sync action
            CurrentSubject.clearSubject();
            return CurrentSubject.getSubject()
            .then((subject) => {
                this.users = new Users();
                this.subject = subject;
                LF.DynamicText.subject = subject;
            })
            .then(() => this.users.fetch())

            // Hacky fix for iOS race condition -- DE19389
            .delay(500)
            .then(() => {
                selectedUserId = parseInt(this.$user.val(), 10);
                this.user = this.users.findWhere({ id: selectedUserId });
                this.undelegateEvents();
                return this.render();
            });
        });
    }

    /**
     * Calls the transmit rule for this view.
     * @returns {Q.Promise<void>}
     */
    triggerTransmitRule () {
        return CurrentSubject.getSubject()
        .then(subject => ELF.trigger(`LOGIN:Transmit/${CurrentContext().role}`, { subject }, this));
    }
}
