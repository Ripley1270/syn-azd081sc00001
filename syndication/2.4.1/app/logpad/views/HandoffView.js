import PageView from 'core/views/PageView';
import CurrentContext from 'core/CurrentContext';
import { checkTimeZoneSet } from 'core/Helpers';
import EULA from 'core/classes/EULA';

const accept = 'accept';

export default class HandoffView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {string} The flags used for page redirection.
         * @default ''
         */
        this.patientEULA = options && options.patientEULA || '';
        this.siteEULA = options && options.siteEULA || '';

        /**
         * @property {Object<string,string>} events - The view's events.
         * @readonly
         */
        this.events = {

            // Click event for back button
            'click #back': 'back',

            // Click event for next button
            'click #next': 'next'
        };

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            handoffMessage: 'HANDOFF_MESSAGE',
            back: 'BACK',
            next: 'NEXT'
        };
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'handoff-page'
     */
    get id () {
        return 'handoff-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#handoff-template'
     */
    get template () {
        return '#handoff-template';
    }

    /**
     * Navigates back to the dashboard view.
     */
    back () {
        ELF.trigger('HANDOFF:Back', {}, this)
        .then((evt) => {
            if (!evt.preventDefault) {
                // navigates to the previous page
                this.navigate('set_time_zone_activation', true);
            }
        })
        .done();
    }

    /**
     * Navigates forward to pricacy policy or EULA or reactivation
     */
    next () {
        LF.Data.activation = JSON.parse(localStorage.getItem('activationFlag'));
        CurrentContext().setContextLanguage(localStorage.getItem('preferredLanguageLocale'));
        if (LF.Data.activation) {
            // if site user accepted EULA, navigates to privacy policy page, otherwise to EULA
            if (this.siteEULA === accept || EULA.isAccepted()) {
                this.navigate('privacy_policy_activation', true);
            } else {
                this.navigate('end_user_license_agreements', true, { patientEULA: this.patientEULA, siteEULA: this.siteEULA });
            }
        } else {
            this.navigate('reactivation', true);
        }
    }

    /**
     * Renders the view.
     */
    render () {
        // TODO - should return promise.
        this.buildHTML({}, true)

        // fix for DE17475 and DE17420
        .then(checkTimeZoneSet)
        .then(() => this.delegateEvents());
    }
}
