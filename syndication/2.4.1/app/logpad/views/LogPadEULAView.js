import COOL from 'core/COOL';
import EULAView from 'core/views/EULAView';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';
import CurrentContext from 'core/CurrentContext';

let logger = new Logger('LogPadEULAView');
const accept = 'accept';
const decline = 'decline';

/**
 * View for EULA page, user's accepttance is required for activation.
 * @class LogPadEULAView
 * @extends EULAView
 */
export default class LogPadEULAView extends COOL.getClass('EULAView', EULAView) {
    constructor (options) {
        super(options);

        /**
         * @property {Object} events A list of DOM events.
         * @readonly
         */
        this.events = _.defaults({
            'click #back': 'backHandler'
        }, this.events);

        /**
         * @property {string} patientEULA - The flag used for page redirection upon patient acceptance.
         * @default ''
         */
        this.patientEULA = options && options.patientEULA || '';

        /**
         * @property {string} siteEULA - The flag used for page redirection upon site acceptance.
         * @default ''
         */
        this.siteEULA = options && options.siteEULA || '';
    }

    /**
     * Navigate to the next view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    next (evt) {
        super.next(evt);

        this.patientEULA = this.patientEULA || accept;
        this.siteEULA = this.patientEULA === accept ? '' : accept;

        if (this.patientEULA === accept) {
            // EULA accepted by patient, directs to privacy policy
            logger.operational('EULA is accepted by patient.');
            this.navigate('privacy_policy_activation');
        } else {
            // EULA accepted by site user, directs to handoff
            logger.operational('EULA is accepted by site user.');
            this.navigate('handoff', true, { patientEULA: this.patientEULA, siteEULA: this.siteEULA });
        }
    }


    /**
     * Handles the click event on the back (decline) button.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    backHandler (evt) {
        super.back(evt);

        // Since the back method returns a promise (mostly for testability),
        // we need to call the method from a handler function, invoking .done()
        // to prevent any errors from being swallowed.
        this.back()
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Navigate back to the previous view.
     * @returns {Q.Promise<void>}
     */
    back () {
        this.siteEULA = this.patientEULA === decline ? decline : '';
        this.patientEULA = this.patientEULA || decline;

        if (this.siteEULA === decline) {
            // EULA declined by site user, directs to code entry
            logger.operational('EULA is declined by site user.');

            return ELF.trigger('SiteEULA:Backout', {}, this);
        }

        // EULA declined by patient, directs to site EULA
        logger.operational('EULA is declined by patient.');
        return MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog.HAND_DEVICE_SITE)
        .then(() => {
            CurrentContext().setContextLanguage(`${LF.StudyDesign.defaultLanguage}-${LF.StudyDesign.defaultLocale}`);
            this.undelegateEvents();
            this.render();
        });
    }
}

COOL.add('LogPadEULAView', LogPadEULAView);
