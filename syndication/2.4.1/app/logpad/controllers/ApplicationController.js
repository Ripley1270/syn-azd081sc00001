import LoginView from 'logpad/views/LoginView';
import DashboardView from 'logpad/views/DashboardView';
import UnlockCodeView from 'logpad/views/UnlockCodeView';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import QuestionnaireView from 'core/views/QuestionnaireView';
import QuestionnaireCompletionView from 'core/views/QuestionnaireCompletionView';
import BlankView from 'core/views/BlankView';
import BaseController from 'core/controllers/BaseController';
import * as lStorage from 'core/lStorage';
import ForgotPasswordView from 'core/views/ForgotPasswordView';
import ResetPasswordView from 'core/views/ResetPasswordView';
import UniversalLogin from 'core/classes/UniversalLogin';
import Data from 'core/Data';
import Logger from 'core/Logger';
import { checkInstall, checkTimeZoneSet } from 'core/Helpers';
import COOL from 'core/COOL';
import CurrentSubject from 'core/classes/CurrentSubject';

let logger = new Logger('ApplicationController');

/**
 * Controller for LogPad App application workflow.
 * @class ApplicationController
 * @extends BaseController
 */
export default class ApplicationController extends BaseController {
    constructor (options) {
        logger.traceSection('ApplicationController');
        logger.traceEnter('constructor');
        super(options);
        logger.traceExit('constructor');
    }

    /**
     * Displays the login view.
     * @param {Object} [options] - Parameters provided by Router.flash().
     * @example /application/#login
     * @returns {Q.Promise<void>}
     */
    login (options) {
        let force = options && options.force,
            successfulLogin = () => {
                this.navigate('dashboard');
            };

        return checkInstall()
        // eslint-disable-next-line consistent-return
        .then((isInstalled) => {
            if (isInstalled || force) {
                // DE16561:  Clean up the previous view before showing login.
                this.clear();

                return checkTimeZoneSet()
                .then(() => CurrentSubject.getSubject())
                .then((subject) => {
                    let universalLogin = new UniversalLogin({
                        loginView: LoginView,

                        // To fix the hard dependency on login view in scheduling
                        appController: this,
                        subject
                    });

                    return universalLogin.newUserLogin({ successfulLogin, options });
                });
            }
            this.navigate('code_entry', true);
        });
    }

    /**
     * Displays the dashboard view.
     * @param {Object} [options] - Parameters provided by Router.flash().
     * @example /application/#dashboard
     * @returns {Q.Promise<void>}
     */
    dashboard (options) {
        logger.traceEnter('dashboard');

        // fix for DE16473 - removing this flag after user has reset their SQ and A
        // rather than doing it on core/ResetSecretQuestionView which would cause SitePad issues
        localStorage.removeItem('Reset_Password_Secret_Question');

        return this.authenticateThenGo('DashboardView', DashboardView, options)
        .catch(e => logger.error(e))
        .finally(() => {
            logger.traceExit('dashboard');
        });
    }

    /**
     * Display the forgot password view.
     * @param {Object} [options] - Parameters provided by Router.flash().
     * @example /application/#forgot_password
     * @returns {Q.Promise<void>}
     */
    forgotPassword (options) {
        logger.traceEnter('forgotPassword');
        const forgotPassword = localStorage.getItem('Forgot_Password');
        const userLogin = lStorage.getItem('User_Login');

        return Q()
        .then(() => {
            if (options.force || (forgotPassword && userLogin)) {
                return this.go('ForgotPasswordView', ForgotPasswordView, options)
                .catch(e => logger.error(e));
            }

            return this.navigate('login');
        })
        .finally(() => {
            logger.traceExit('forgotPassword');
        });
    }

    /**
     * Display the Unlock Code view.
     * @param {Object} [options] - Parameters provided by Router.flash().
     * @example /application/#unlock_code
     * @returns {Q.Promise<void>}
     */
    unlockCode (options) {
        logger.traceEnter('unlockCode');

        return Q()
        .then(() => {
            if (options.force || localStorage.getItem('Unlock_Code')) {
                return this.go('UnlockCodeView', UnlockCodeView, options)
                .catch(e => logger.error(e));
            }

            return this.navigate('login');
        })
        .finally(() => {
            logger.traceExit('unlockCode');
        });
    }

    /**
     * Display the reset password view.
     * @param {Object} [options] - Parameters provided by Router.flash().
     * @example /application/#reset_password
     * @returns {Q.Promise<void>}
     */
    resetPassword (options) {
        logger.traceEnter('resetPassword');

        return Q()
        .then(() => {
            if (options.force || localStorage.getItem('Reset_Password')) {
                return this.go('ResetPasswordView', ResetPasswordView, options)
                .catch(e => logger.error(e));
            }

            return this.navigate('login');
        })
        .finally(() => {
            logger.traceExit('resetPassword');
        });
    }

    /**
     * Display the questionnaire completion view.
     * @param {Object} [options] - Parameters provided by Router.flash().
     * @example /application/#questionnaire-completion
     * @returns {Q.Promise<void>}
     */
    questionnaireCompletion (options) {
        logger.traceEnter('questionnaireCompletion');
        this.clear();
        options = options || {};

        return Q()
        .then(() => {
            if (!Data.Questionnaire && !options.force) {
                return this.navigate('dashboard', true);
            }

            this.view = new QuestionnaireCompletionView(options);
            return this.view.render();
        })
        .finally(() => {
            logger.traceExit('questionnaireCompletion');
        });
    }

    /**
     * Displays the questionnaire view.
     * @param {string} questionnaire - The id of the questionnaire to load.
     * @param {string} scheduleId - unique schedule configuration id for this questionnaire.
     * @param {Object} [options] - Parameters provided by Router.flash().
     * @returns {Q.Promise<void>}
     * @example /application/#questionnaire/:questionnaire_id/:schedule_id
     */
    questionnaire (questionnaire, scheduleId) {
        this.clear();

        return CurrentSubject.getSubject()
        .then((subject) => {
            // If the Questionnaire namespace doesn't exist, navigate back to the dashboard.
            if (!Data.Questionnaire) {
                this.navigate('dashboard', true);
                return Q();
            }

            let params = {
                id: questionnaire,
                schedule_id: scheduleId,
                subject
            };

            // Stakutis; see if a class name is specified in the study design
            let className = LF.StudyDesign.questionnaires.find({ id: questionnaire }).get('syndicationClass') || 'QuestionnaireView';

            this.view = COOL.new(className, QuestionnaireView, params);
            Data.Questionnaire = this.view;

            return this.view.resolve()

            // TODO: This all should be a part of the resolve/render workflow.
            .then(() => this.view.render())
            .catch((e) => {
                // log since backbone won't handle it if backbone called this method.
                logger.error('unexpected exception in ApplicationController.questionnaire():', e);
                throw e;
            });
        });
    }

    /**
     * Display the Time_Confirmation questionnaire.
     * @param {Object} [options] - Parameters provided by Router.flash().
     * @returns {Q.Promise<void>}
     */
    timeConfirmation (options) {
        return this.go('BaseQuestionnaireView', BaseQuestionnaireView, _.extend(options || {}, {
            id: 'Time_Confirmation'
        }))
        .catch(e => logger.error(e));
    }

    /**
     * Display the Blank page.
     * @param {Object} [options] - Parameters provided by Router.flash().
     * @returns {Q.Promise<void>}
     */
    blank (options) {
        return this.go('BlankView', BlankView, options)
        .catch(e => logger.error(e));
    }

    /**
     * Removes the current view.
     */
    clear () {
        logger.traceEnter('clear');

        if (this.view) {
            if (this.view.id === 'dashboard-page') {
                LF.schedule.stopTimer();
            }

            if (this.view.close) {
                this.view.close();
            } else if (this.view.remove) {
                this.view.remove();
            }

            this.view = null;
        }

        logger.traceExit('clear');
    }
}
