import QuestionnairesList from 'logpad/views/QuestionnairesList';
import IndicatorsList from 'logpad/views/IndicatorsList';
import Schedules from 'core/collections/Schedules';

/**
 * Add available objects to the dashboard. And set up all view collection event listeners
 * @param {Object} parentView parent view that is calling this function
 * @example LF.Schedule.DisplayItem.addToView(view, function () {...});
 */
export function addToView (parentView) {
    let objectTypes = _.chain(LF.schedule.availableSchedules.toJSON())
            .pluck('target')
            .pluck('objectType')
            .intersection()
            .value();

    const displayItem = (objectType, schedules) => {
        // @TODO: We need a new way of allowing PDE customizations of this without LF namespace
        return LF.Schedule.DisplayItem[objectType](parentView, schedules)
        .then((view) => {
            if (view) {
                view.listenTo(LF.schedule.availableSchedules, 'add', (schedule) => {
                    view.addInOrder(schedule, (newSubview) => {
                        if (newSubview) {
                            (view.refresh || $.noop)();
                        }
                    })
                    .done();
                });
                view.listenTo(LF.schedule.availableSchedules, 'remove', view.removeSubview);
            }

            return view;
        });
    };

    // When a new schedule is available, create a new listView for the objectType if it does not already exist.
    parentView.listenTo(LF.schedule.availableSchedules, 'add change', (schedule) => {
        let matchedView = _.findWhere(parentView.listViews, {
            objectType: schedule.get('target').objectType
        });

        if (!matchedView) {
            displayItem(schedule.get('target').objectType, new Schedules(schedule))
            .then((view) => {
                // saving logs
                if (view) {
                    parentView.listViews[view.id] = view;
                }
            })
            .done();
        }
    });

    if (parentView.resetModels) {
        parentView.listenTo(LF.schedule.availableSchedules, 'beforeEvaluate', () => {
            // When we evaluate schedules we apply custom classes, so we need to start from clean models
            parentView.resetModels();
        });
    }

    if (objectTypes.length) {
        const views = {};
        return objectTypes.reduce((chain, objectType) => {
            return chain
            .then(() => displayItem(objectType, LF.schedule.availableSchedules))
            .then((view) => {
                if (view) {
                    views[view.id] = view;
                }
            });
        }, Q())
        .then(() => views);
    }

    return Q();
}

LF.Schedule.DisplayItem.addToView = addToView;

/**
 * Adds a scheduled questionnairesList view to the display
 * @param {Object} parentView - parent view that is calling this function
 * @param {Object} schedules - The list of available schedules whose items are to be rendered.
 * @returns {Q.Promise} returns promise that resolves with the rendered questionnaireslist or nothing
 */
export function questionnaire (parentView, schedules) {
    let questionnaireList = parentView.$('#questionnaires');

    if (questionnaireList.length > 0 && questionnaireList.children().length === 0) {
        const questionnaires = new QuestionnairesList({
            collection: parentView.questionnaires,
            schedules,
            container: questionnaireList
        });

        return questionnaires.render()
        .then(() => questionnaires);
    }

    return Q();
}

LF.Schedule.DisplayItem.questionnaire = questionnaire;

/**
 * Adds a scheduled indicatorsList view to the display
 * @param {Object} parentView - parent view that is calling this function
 * @param {Object} schedules - The list of available schedules whose items are to be rendered.
 * @returns {Q.Promise} returns promise that resolves with the rendered IndicatorsList or nothing
 */
export function indicator (parentView, schedules) {
    let indicatorsList = parentView.$('#indicators');

    if (indicatorsList.length > 0 && indicatorsList.children().length === 0) {
        const indicators = new IndicatorsList({
            collection: parentView.indicators,
            schedules,
            container: indicatorsList
        });

        return indicators.render()
        .then(() => indicators);
    }

    return Q();
}

LF.Schedule.DisplayItem.indicator = indicator;

/**
 * Add an autodial alarm
 * @returns {Q.Promise}
 */
export function autodial () {
    return Q();
}

LF.Schedule.DisplayItem.autodial = autodial;
