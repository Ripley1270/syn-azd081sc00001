import ELF from 'core/ELF';
import Logger from 'core/Logger';
import Transmission from 'core/models/Transmission';
import Transmissions from 'core/collections/Transmissions';

let logger = new Logger('editUserStatus');

let dflt = () => {
    logger.trace('No status was provided. Defaulting to active.');

    return { active: 1 };
};

/**
 * @memberOf ELF.actions/sitepad
 * @method editUserStatus
 * @description
 * Activate or deactivate a user.
 * @param {Object} data - Parameters provided by the rule configuration.
 * @param {number} data.active - The status of the user. 1 for Active, 0 for Deactivated.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'editUserStatus', data: { active: 0 } }]
 */
export default function editUserStatus (data = dflt()) {
    const { active } = data;
    const editUserMethod = 'transmitUserEdit';
    const newUserMethod = 'transmitUserData';

    // Find an existing edit or new user transmission for the target user.
    // returns {Transmission}
    let findExistingTransmission = (transmissions) => {
        return transmissions.find((transmission) => {
            // We only care about edit user and new user transmission methods.
            if (transmission.get('method') === editUserMethod) {
                // Parse out the user's ID from the transmission params.
                let { id } = JSON.parse(transmission.get('params'));

                return this.user.get('id') === id;

            // In the case of a new user method existing, it may be a failed transmission due to duplication of username.
            } else if (transmission.get('method') === newUserMethod) {
                // Parse out the user's ID from the transmission params.
                // The new user transmission record stores the user's ID as userId instead of id.
                let { userId } = JSON.parse(transmission.get('params'));

                return this.user.get('id') === userId;
            }

            return false;
        });
    };


    let resolveTransmission = () => {
        return Transmissions.fetchCollection()
        .then((transmissions) => {
            let existingTransmission = findExistingTransmission(transmissions);

            if (existingTransmission) {
                logger.trace(`An existing transmission record exists for user ${this.user.get('username')}.`, existingTransmission);

                // Check if the existing transmission is in a failed state.
                if (existingTransmission.get('status') === 'failed') {
                    logger.trace('Removing failed status from transmission record.');

                    // To get the failed transmission to executed, null out the status field.
                    existingTransmission.set('status', null);

                    return existingTransmission.save();
                }

                // We don't need to modify the existing transmission if it's not in a failed
                // state, as it's already queued for transmission.
                // Return a promise for consistent-return.
                return Q();
            }
            logger.trace(`Creating a new ${editUserMethod} transmission for user ${this.user.get('username')}.`);

            // There is no existing transmission for the target user, so we can assume the
            // user exists within SW/Expert and can queue up an edit user transmission.
            let transmission = new Transmission({
                method: editUserMethod,
                params: JSON.stringify({ id: this.user.get('id') }),
                created: new Date().getTime()
            });

            return transmission.save();
        });
    };

    this.user.set('active', active);

    return this.user.save()
    .then(() => resolveTransmission());
}

ELF.action('editUserStatus', editUserStatus);
