import Users from 'core/collections/Users';
import State from 'sitepad/classes/State';
import Logger from 'core/Logger';
import COOL from 'core/COOL';
import { MessageRepo } from 'core/Notify';
import Spinner from 'core/Spinner';
import { checkTimeZoneSet } from 'core/Helpers';
import EULA from 'core/classes/EULA';

// Action is being imported as part of an object for testing.
import * as sync from 'core/actions/syncUsers';

// Logger is declared, but not yet used.
// eslint-disable-next-line no-unused-vars
let logger = new Logger('skipOrDisplayFirstSiteUser');

/**
 * Exported for testing purpose.
 * @private
 * @param {User} user - The user to check.
 * @returns {boolean}
 */
export function isAdminUser (user) {
    return user.get('role') === 'admin' && user.get('active') === 1;
}

/**
 * @memberOf ELF.actions/sitepad
 * @method skipOrDisplayFistSiteUser
 * @description
 * After Unlock Code entry is completed this action will be called to determine
 * if First Site User Creation page should be displayed or skipped
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'skipOrDisplayFirstSiteUser' }]
 */
export default function skipOrDisplayFirstSiteUser () {
    // Displays a confirmation modal with retry logic.
    let dialog = (key) => {
        return Spinner.hide()
        .then(() => {
            return MessageRepo.display(MessageRepo.Dialog[key]);
        })
        .then(() => {
            return Q.delay(1000)
            .then(() => {
                return skipOrDisplayFirstSiteUser.call(this);
            });

        // The ConfirmView makes some bad use of promises...
        }, () => {
            if (EULA.isAccepted()) {
                State.set(State.states.setupUser);
                this.navigate('setupUser');
            } else {
                State.set(State.states.endUserLicenseAgreements);
                this.navigate('endUserLicenseAgreements');
            }

            // Not entirely sure this needs to be called twice...
            // Not removing in case it's required.
            return Spinner.hide();
        });
    };

    return Spinner.show()
    .then(checkTimeZoneSet)
    .then(() => {
        return COOL.getClass('Utilities').isOnline();
    })
    .then((onlineStatus) => {
        if (onlineStatus) {
            let users = new Users();

            // Passing true to the syncUsers action forces a reject on error.
            return sync.syncUsers(true)
            .then(() => users.fetch())
            .then(() => {
                let filterFunction = LF.StudyDesign.sitePad.adminUserFilter || isAdminUser,
                    adminUsers = users.filter(filterFunction);

                if (adminUsers.length > 0) {
                    State.set(State.states.activated);
                    this.navigate('login');
                } else {
                    // US7741 if no adminUsers found, direct to EULA
                    if (EULA.isAccepted()) {
                        State.set(State.states.setupUser);
                        this.navigate('setupUser');
                    } else {
                        State.set(State.states.endUserLicenseAgreements);
                        this.navigate('endUserLicenseAgreements');
                    }
                }
            })
            .catch(() => {
                return dialog('CONFIRM_TRANSMIT_RETRY');
            })
            .finally(() => Spinner.hide());
        }
        return dialog('NO_INTERNET_FIRST_USER');
    });
}

ELF.action('skipOrDisplayFirstSiteUser', skipOrDisplayFirstSiteUser);
