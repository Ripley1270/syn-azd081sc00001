import BaseStartup from 'core/classes/BaseStartup';
import trial from 'sitepad/trialMerge'; //PDE UPDATE: use modality specific trial

import ELF from 'core/ELF';
import Logger from 'core/Logger';
import { setServiceBase } from 'core/utilities/coreUtilities';

import COOL from 'core/COOL';
import StudyDesign from 'core/classes/StudyDesign';

/*
* PDE UPDATES: Modified to use modality-specific asset merge from app/sitepad/trialMerge,
* studyStartup moved here from BaseStartup for this purpose
*/

// I believe this is here to be bootstraped at startup.
// eslint-disable-next-line no-unused-vars
import Transmit from 'sitepad/transmit';
import WebService from 'sitepad/classes/WebService';
import Router from 'sitepad/Router';
import RegistrationController from 'sitepad/controllers/RegistrationController';
import ApplicationController from 'sitepad/controllers/ApplicationController';
import SettingsController from 'sitepad/controllers/SettingsController';

import setupSitepadMessages from 'sitepad/resources/Messages';

let logger = new Logger('Startup');

export default class Startup extends BaseStartup {
    /**
     * Constructs key study components such as the Study Design, Templates and Web Service.
     * @returns {Q.Promise<void>}
     */
    studyStartup () {
        return Q.Promise((resolve) => {
            logger.traceEnter('studyStartup');

            // Set the study design in the global namespace.
            logger.trace(`Processing study design: ${trial.assets.studyDesign}`);
            LF.StudyDesign = COOL.new('StudyDesign', StudyDesign, trial.assets.studyDesign);

            setServiceBase();

            // Add the custom trial-specific study rules
            trial.assets.trialRules(ELF.rules);

            // Add the custom study templates to the global collection.
            logger.trace('Adding templates...');
            LF.templates.add(trial.assets.studyTemplates);

            // Add the custom study web service to the global namespace.
            LF.studyWebService = COOL.new('WebService', trial.assets.studyWebService);

            // PDE UPDATE: add custom study messages
            trial.assets.studyMessages();

            logger.traceExit('studyStartup');
            resolve();
        });
    }

    /**
     * Create the router.
     * @param {Object} [options={}] options passed into the Router's constructor.
     * @returns {Q.Promise<Router>}
     */
    createRouter (options = {}) {
        logger.traceEnter('routerStartup');

        return Q()
        .then(() => {
            const registration = new RegistrationController();
            const application = new ApplicationController();
            const settings = new SettingsController();
            const study = new trial.assets.StudyController();

            const routerOpts = _.extend({}, options.routerOpts, {
                controllers: _.extend({}, options.controllers, {
                    application, registration, settings, study
                })
            });

            return new Router(routerOpts);
        });
    }

    /**
     * Setup the application's router.
     * @param {Object} [options={}] - Options passed into the Router's constructor.
     * @returns {Q.Promise<void>}
     */
    backboneStartup (options = {}) {
        logger.traceEnter('backboneStartup');

        return Q()
        .then(() => this.createRouter(options))
        .then((router) => {
            LF.router = router;

            // Add the web service to the LF namespace.
            // As much as I don't like doing this, we still have a lot of code
            // using the LF.webService namespace.
            LF.webService = COOL.new('WebService', WebService);
        })
        .catch((err) => {
            logger.error('Error on Backbone startup.', err);
        })
        .finally(() => {
            logger.traceExit('backboneStartup');
        });
    }

    /**
     * Start Backbone.history
     */
    startUI () {
        logger.traceEnter('startUI');

        Backbone.history.start({ pushState: false, silent: true });

        // hack so backbone things route it changing.  Unfortunately it sets fragment to ''
        // if silent: true, even though it never actually navigated anywhere.
        Backbone.history.fragment = undefined;
        logger.traceExit('startUI');
    }

    /**
     * Startup the application.
     * @returns {Q.Promise<void>}
     */
    startup () {
        setupSitepadMessages();

        return this.preSystemStartup()
        .then(() => this.jQueryStartup())
        .then(() => this.cordovaStartup())
        .then(() => this.studyStartup())
        .then(() => this.backboneStartup())
        .then(() => this.setupUsersAndContext())
        .then(() => this.startUI())
        .then(() => ELF.trigger('APPLICATION:Loaded'))
        .then((res) => {
            // TODO remove the usage of this flag.
            localStorage.removeItem('questionnaireCompleted');

            if (!res.preventDefault) {
                LF.router.navigate('', true);
            }
        })
        .catch(err => err && logger.error('Error Loading Application', err));
    }
}
