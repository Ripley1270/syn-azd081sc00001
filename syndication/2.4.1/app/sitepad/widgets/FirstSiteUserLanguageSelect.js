import LanguageSelectWidget from '../../core/widgets/LanguageSelectWidget';

export default class FirstSiteUserLanguageSelect extends LanguageSelectWidget {
    /**
     * Extension of SelectWidgetBase for a particular instance of a Select Widget control.
     * @param {String} divToAppendTo the select ID that the options should be appended to
     */

    renderOptions (divToAppendTo) {
        let responseObj,
            languages = LF.strings.getLanguages(),

            // this 'answer' used to give widget ability to retain selected answer when moving between screens.
            answer = this.answer ? JSON.parse(this.answer.get('response'))[this.model.get('field')] : false,
            template = _.template('<option id="{{ language }}-{{ locale }}" value="{{ language }}-{{ locale }}">{{ localized }}</option>');
        return this.filterLanguagesByLocale(languages).then((languages) => {
            let addItemPromiseFactories = languages.map((resource) => {
                return () => {
                    resource.localized = `${resource.language}-${resource.locale}`.toLocaleUpperCase();
                    return LF.strings.display.call(LF.strings, resource.localized)
                    .then((string) => {
                        resource.localized = string;
                        let el = template(resource);

                        // During initial setup, the Add First Site User diary screen should default the language to the study design.
                        if (
                            (LF.StudyDesign.defaultLanguage === resource.language) &&
                            (LF.StudyDesign.defaultLocale === resource.locale) && !this.completed) {
                            el = $(el).attr('selected', 'selected');
                            responseObj = { value: `${resource.language}-${resource.locale}` };
                        }

                        if ((answer && answer === `${resource.language}-${resource.locale}`) || this.$(divToAppendTo).children().size() === 0) {
                            el = $(el).attr('selected', 'selected');
                        }

                        // NOTE: webstorm debugger may fail to resolve 'this' because of the => function.
                        // chrome debugger seems to handle it ok.
                        this.$(divToAppendTo).append(el);
                        this.needToRespond = responseObj;
                    });
                };
            });
            return addItemPromiseFactories.reduce(Q.when, Q());
        });
    }
}

window.LF.Widget.FirstSiteUserLanguageSelect = FirstSiteUserLanguageSelect;
