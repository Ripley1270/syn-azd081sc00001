import EULAView from 'core/views/EULAView';
import Logger from 'core/Logger';
import COOL from 'core/COOL';
import State from 'sitepad/classes/State';

const logger = new Logger('SitePadEULAView');

/*
 * View for EULA page, user's accepttance is required for activation.
 * @class SitePadEULAView
 * @extends EULAView
 */
export default class SitePadEULAView extends COOL.getClass('EULAView', EULAView) {
    /**
     * Navigate to the next view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    next (evt) {
        super.next(evt);

        logger.operational('EULA is accepted by the first site user.');
        return ELF.trigger('REGISTRATION:SkipOrDisplayFirstSiteUser', {}, this);
    }

    /**
     * Navigate back to the previous view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    back (evt) {
        super.back(evt);

        logger.operational('EULA is declined by the first site user.');

        State.set(State.states.setTimeZone);
        this.navigate('setTimeZone');
    }
}

COOL.add('SitePadEULAView', SitePadEULAView);
