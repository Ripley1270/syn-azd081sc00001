import ModalView from 'core/views/ModalView';
import * as lStorage from 'core/lStorage';

export default class QrCodeView extends ModalView {
    constructor () {
        super({

            /**
             * @property {Object} events - A list of DOM events.
             * @readonly
             */
            events: {
                'click #ok': 'resolve',
                'hidden.bs.modal': 'teardown'
            }
        });

        /**
         * @property {string} codeString - Stringified JSON representing the subject.
         */
        this.codeString = '';

        /**
         * @property {string} template - A selector that points to the template, within the DOM, to render.
         */
        this.template = '#qrcode-template';

        /**
         * Invoked when the user accepts the confirmation.
         */
        this.resolve = _.debounce(() => {
            this.hide()
            .then(() => {
                this.deferred.resolve(true);
            })
            .done();
        }, 500, true);
    }

    /**
     * Show the notification dialog.
     * @param {Object} options - Strings to pass into the modal's template.
     * @param {string} options.header - The header text to display.
     * @returns {Q.Promise<void>}
     */
    show (options) {
        this.deferred = Q.defer();
        super.show(options);

        this.$('#modal-qr-code').empty()
            .qrcode({ width: 150, height: 150, text: this.codeString });

        return this.deferred.promise;
    }

    /**
     *  Generate the QR code to be set for the view.
     *  See NetPro 1.6 FS on QR string format.
     *  @param {Subject} subject - the Subject to generate the QR code for.
     */
    setQRCodeString (subject) {
        let study;

        if (localStorage.getItem('trainer')) {
            study = 'trainer';
        } else if (lStorage.getItem('environment') === 'custom') {
            // DE21289 - use url when generating QR code for custom environment.
            study = lStorage.getItem('serviceBase');
        } else {
            let environment,
                id;
            id = lStorage.getItem('environment');
            environment = LF.StudyDesign.environments.findWhere({ id });
            study = environment.get('url').split('//')[1].split('.')[0];
        }

        this.codeString = JSON.stringify({
            study,
            setupcode: subject.get('setupCode'),
            language: subject.get('language')
        });
    }

    /*
     * Return the QR code string for this view
     * @returns {string} The current value of the QR Code string for this view.
     */
    getQRCodeString () {
        return this.codeString;
    }
}
