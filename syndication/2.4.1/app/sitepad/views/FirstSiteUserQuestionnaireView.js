import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';

export default class FirstSiteUserQuestionnaireView extends BaseQuestionnaireView {
    get defaultRouteOnExit () {
        return 'unlock-code';
    }

    get defaultFlashParamsOnExit () {
        return {};
    }
}
