import RowView from './RowView';
import * as dateTimeUtil from 'core/DateTimeUtil';
import UserVisit from 'sitepad/models/UserVisit';

/**
 * Creates visit row and handle events on specific visit.
 * @class VisitRowView
 * @extends RowView
 */
export default class VisitRowView extends RowView {
    /**
     * Render the view to the DOM.
     * example this.render();
     * @returns {Q.Promise<VisitRowView>}
     */
    render () {
        let { visit, userVisit, action } = this.model.toJSON();

        let visitStatus,
            visitStatusKey = 'AVAILABLE',
            visitDate,
            visitDateString = '-';

        if (action === LF.StudyDesign.visitAction.hide) {
            return this.i18n({
                name: visit.get('displayName')
            })
            .then(({ name }) => {
                // set name to enable proper skip visit workflow.
                visit.set('name', name);
            });
        } else if (action === LF.StudyDesign.visitAction.notAvailable) {
            visitStatusKey = 'NOT_AVAILABLE';
            let newUserVisit = userVisit || new UserVisit();
            newUserVisit.set({
                state: LF.VisitStates.NOT_AVAILABLE
            });
            this.model.set({
                userVisit: newUserVisit
            });
        } else if (userVisit != null) {
            visitDate = dateTimeUtil.parseDateTimeIsoNoOffset(userVisit.get('dateModified'));

            // Visit state is available in userVisit table
            visitStatus = userVisit.get('state');

            if (visitStatus === LF.VisitStates.COMPLETED) {
                visitStatusKey = 'COMPLETED';
            } else if (visitStatus === LF.VisitStates.IN_PROGRESS) {
                visitStatusKey = 'IN_PROGRESS';
                visitDate = dateTimeUtil.parseDateTimeIsoNoOffset(userVisit.get('dateStarted'));
            } else if (visitStatus === LF.VisitStates.SKIPPED) {
                visitStatusKey = 'SKIPPED';
            } else if (visitStatus === LF.VisitStates.NOT_AVAILABLE) {
                visitStatusKey = 'NOT_AVAILABLE';
            } else if (visitStatus === LF.VisitStates.INCOMPLETE) {
                visitStatusKey = 'INCOMPLETE';
            }

            if (visit.get('visitType') !== 'container') {
                visitDateString = dateTimeUtil.getLocalizedDate(visitDate, { includeTime: false, useShortFormat: true });
            }
        }

        return this.i18n({
            status: visitStatusKey,
            name: visit.get('displayName')
        })
        .then(({ name, status }) => {
            // Setting the name to the visit model allows our dynamic text function
            // to display the translated visit name.
            visit.set('name', name);

            // We have to set these properties on the model so they can be displayed.
            this.model.set({
                name,
                status,
                statusDate: visitDateString
            });

            let template = `<td>${name}</td>
            <td>${status}</td>
            <td>${visitDateString}</td>`;

            this.$el.html(template);

            return super.render();
        });
    }
}
