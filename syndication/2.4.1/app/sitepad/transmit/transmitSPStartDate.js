import COOL from 'core/COOL';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';

const logger = new Logger('Transmit.transmitSPStartDate');

/**
 * Handles the SPStartDate update transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise}
 */
export default function transmitSPStartDate (transmissionItem) {
    let params = JSON.parse(transmissionItem.get('params')),
        dataJSON = {
            K: params.krpt,
            M: params.ResponsibleParty,
            U: 'Assignment',
            S: params.dateStarted,
            C: params.dateCompleted,
            R: params.reportDate,
            P: params.phase,
            E: LF.coreVersion,
            V: LF.StudyDesign.studyVersion,
            T: params.phaseStartDate,
            L: params.batteryLevel,
            J: params.sigID
        },
        sendSPStartDateSuccess = ({ res }) => {
            logger.operational(`SPStartDate transmitted: res: ${res}`);
            return this.destroy(transmissionItem.get('id'));
        },
        sendSPStartDateError = ({ errorCode, httpCode, isSubjectActive }) => {
            logger.trace(`transmitSPStartDate: SPStartDate error: removing transmissionItem errorCode: ${errorCode} httpCode: ${httpCode}`);
            this.remove(transmissionItem);
            return Q.reject({ errorCode, httpCode, isSubjectActive });
        };

    logger.traceEnter('transmitSPStartDate');

    dataJSON.A = [];

    dataJSON.A.push({
        G: '1',
        F: 'SPStartDateAffidavit',
        Q: 'AFFIDAVIT'
    });

    dataJSON.A.push({
        G: params.SPStartDate,
        F: 'PT.SPStartDate',
        Q: ''
    });

    // compress answers collection with JSONH
    if (LF.StudyDesign.jsonh !== false) {
        dataJSON.A = JSONH.pack(dataJSON.A);
        logger.trace('Remapped after JSONH.pack');
    }

    let service = COOL.new('WebService', WebService);

    return service.sendEditPatient(dataJSON, '')
    .then(sendSPStartDateSuccess)
    .catch(sendSPStartDateError)
    .finally(() => {
        logger.traceExit('transmitSPStartDate');
    });
}
