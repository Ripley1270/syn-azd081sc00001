import SitepadController from './SitepadController';

import SettingsView from '../views/SettingsView';
import AboutView from '../views/AboutView.js';
import ConsoleLogView from '../views/ConsoleLogView';
import CustomerSupportView from '../views/CustomerSupportView';
import SetTimeZoneView from '../views/SetTimeZoneView';

import Logger from 'core/Logger';

let logger = new Logger('SettingsController');

export default class SettingsController extends SitepadController {
    /**
     * Navigate to the settings view.
     */
    index () {
        this.authenticateThenGo('SettingsView', SettingsView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Navigate to the about view.
     */
    about () {
        this.authenticateThenGo('AboutView', AboutView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Navigate to the SetTimeZone view.
     */
    timeZoneSettings () {
        this.authenticateThenGo('SetTimeZoneView', SetTimeZoneView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the customer support view.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /settings/#customerSupport
     * @returns {Q.Promise<void>}
     */
    customerSupport (options) {
        this.clear();

        return this.authenticateThenGo('CustomerSupportView', CustomerSupportView, options)
        .catch(e => logger.error(e));
    }

    /**
     * Navigate to the console log view.
     */
    consoleLog () {
        this.authenticateThenGo('ConsoleLogView', ConsoleLogView)
        .catch(e => logger.error(e))
        .done();
    }
}
