var express = require('express'),
    _ = require('underscore'),
    data = require('../data/users'),
    router = express.Router();

router.post('/GetDataClin', function (req, res, next) {

    var proc = req.body.StoredProc,
        params = req.body.Params;

    switch (proc) {

        case 'LPA_SyncUsers':

            res.send(data);

            break;
        case 'LPA_SyncUsers_XML':
            res.send("<Results></Results>" );
            break;

        case 'LPA_AddUser':

            var user = {
                type       : params[0],
                username   : params[1],
                language   : params[2],
                password   : params[3],
                salt       : params[4],
                role       : params[4],
                synchLevel : params[6]
                // @todo KRDom
            };

            data.push(user);

            res.send('S');

            break;

        case 'LPA_UpdateUserCredentials':
            res.send('LPA_UpdateUserCredentials');
            break;

        case 'LPA_getKrdom':
             res.send('[{"krdom":"DOM.244494.147", "sitecode":"100-002"}]' );
//            krdom: 'DOM.244494.147'
//            sitecode: '0001000'
            break;

        case 'LPA_getRoleAccessCodes':
            // todo hash a pwd.
            res.send('{"0": ""}');
            break;

        case 'lastDiaryTableSync':
            res.send('"[]"');
            break;

        case 'LPA_getKrdom_XML':
            res.send('"<Results><site sitecode=\'100-002\' krdom=\'DOM.287206.246\'/></Results>"');
            break;

        case 'lastDiaryTableSync_XML':
//            res.send(null);
            res.send('"<Results><diary SU=\'Activation\' lastStartedDate=\'2016-03-07T15:45:51-05:00\' lastCompletedDate=\'2016-03-07T20:46:07Z\' deleted=\'0\'/><diary SU=\'AnalysisPeriodLabel\' lastStartedDate=\'2016-03-02T12:17:56-05:00\' lastCompletedDate=\'2016-03-02T17:17:56Z\' deleted=\'0\'/><diary SU=\'Assignment\' lastStartedDate=\'2016-03-02T12:16:39-05:00\' lastCompletedDate=\'2016-03-02T17:17:56Z\' deleted=\'0\'/></Results>"');
            break;

        case 'LPA_getRoleAccessCodes_XML':
            var md5sum = crypto.createHash('md5');

            md5sum.update(studyName + '11111');

            res.send('<Results><accessCode role="Admin" hash="' +
                md5sum.digest('hex') + '" key="whatever"></accessCode></Results>');
            break;



        default:
            console.log('LocalServer ERROR in /server/routes/swapi.js: GetDataClin - unimplmented call: ' + proc + ', params = ' + JSON.stringify(params) );
            break;

    };

});

module.exports = router;
