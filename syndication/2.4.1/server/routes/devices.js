var express = require('express'),
    _ = require('underscore'),
    fs = require('fs'),
    data = require('../data/subjects'),
    crypto = require('crypto'),
    router = express.Router(),
    createGUID = function () {

        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });

    };

router.route('/').get(function (req, res, next) {

    var pin = req.query.UserID,
        subject = _.findWhere(data, { setupCode : pin });

    res.send({ Q : subject.securityQuestion });

}).post(function (req, res, next) {

    var pin = req.body.U,
        password = req.body.W,
        question = req.body.Q,
        answer = req.body.A,
        subject = _.findWhere(data, { setupCode : pin }),
        md5sum = crypto.createHash('md5');

        console.log(subject);

    // If there is no question posted, the device is being activated for the first time...
    if (question != null) {

        // Activate the subject...
        subject.securityQuestion = question;
        subject.securityAnswer = answer;
        subject.deviceId = createGUID();
        subject.krpt = createGUID();
        subject.active = true;

        // Create the service encrypted password.
        md5sum.update(password + subject.krpt);
        subject.servicePassword = md5sum.digest('hex');

        res.send({
            D : subject.deviceId,
            W : subject.servicePassword
        });

    // Otherwise, this is a re-activation.
    } else {

        res.send({
            D : subject.deviceId,
            W : subject.servicePassword,
            Q : subject.securityQuestion,
            A : subject.securityAnswer
        });

    }

});

module.exports = router;