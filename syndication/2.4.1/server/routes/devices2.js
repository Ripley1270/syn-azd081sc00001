var express = require('express'),
    _ = require('underscore'),
    crypto = require('crypto'),
    data = require('../data/sites'),
    router = express.Router(),
    LENGHT_OF_UNLOCK_CODES = 8,
    hashString,
    padString,
    convertHextoDec,
    createGUID;

createGUID  = function () {

    var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });

    return guid;

};

hashString = function (input) {

    var md5sum = crypto.createHash('md5');

    md5sum.update(input);

    return md5sum.digest('hex').substring(0, LENGHT_OF_UNLOCK_CODES);

};

convertHextoDec = function (input) {

    var unlockCode =  (parseInt(input, 16)).toString();

    if (unlockCode.length < 10) {
        unlockCode = padString(unlockCode, 10, '0', false);
    }

    return unlockCode;

};

padString = function (source, length, padChar, padRight) {

    var split = source.split(''),
        diff = length - split.length,
        i;

    if (diff > 0) {

        for (i = 0; i < diff; i += 1) {

            split[padRight ? 'push' : 'unshift'](padChar);

        }

    };

    return split.join('');

};

createStartupUnlockCode = function (siteCode, studyName) {

        var output;

        console.log('Generating unlock code.');
        console.log('Site Code: ' + siteCode);
        console.log('Study Name: ' + studyName);

        output = hashString(studyName + siteCode);
        output = convertHextoDec(output);

        console.log('Unlock code: ' + output);

        return output;

}

/*
 * POST /api/v1/devices2
 * {
 *    deviceUuid  : {number},
 *    imei        : {number},
 *    siteId      : {string},
 *    startupCode : {string},
 *    studyName   : {string}
 * }
 */
router.post('/', function (req, res, next) {
    // The Site ID (really the KrDOM). ex. DOM.01231201231.
    var krdom = req.body.krdom,

        // The startup code (Site Unlock Code) as entered by the user.
        startupCode = req.body.startupCode,

        // The study name (or database name) as defined in the environments section of the study design.
        studyName = req.body.studyName,

        // Find the site that matches the provided krdom.
        site = _.where(data, { krdom : krdom }),
        unlockCode;

    if( site.length == 0 ){
        res.sendStatus(400);
        return;
    }


    console.log( '-' + req.body.krdom + '-, siteCode: ' + site[0].siteCode + ', studyName: ' + studyName );

    if (site) {

        // Generate the Site Unlock Code from the matched site's code, and the provided study name.
        unlockCode = createStartupUnlockCode(site[0].siteCode, studyName);

        // IF the generated code matches the one provide by the client...
        if (startupCode === unlockCode) {

            res.send({
                deviceId : createGUID(),
                apiToken : createGUID()
            });

        } else {

            res.sendStatus(400);

        }

    } else {

        res.sendStatus(400);

    }

});

module.exports = router;
