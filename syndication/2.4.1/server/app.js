var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var userData = require('./data/users');
var _ = require('underscore');
var session = require('express-session');

var routes = require('./routes/index');

var api = {
    sites           : require('./routes/sites'),
    subjects        : require('./routes/subjects'),
    devices2        : require('./routes/devices2'),
    devices         : require('./routes/devices'),
    diaries         : require('./routes/diaries'),
    logs            : require('./routes/logs'),
    swapi           : require('./routes/swapi')
};

var app = express();

process.env.PORT = 3000;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    resave : true,
    saveUninitialized: true,
    secret : 'keyboard cat'
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

// Configure express to use passport sessions.



passport.use(new LocalStrategy(function(username, password, done) {

    var user = _.findWhere(userData, { username : username });

    console.log(username, password);

    process.nextTick(function () {

        if (!user) {
            return done(null, false, { message: 'Incorrect username.' });
        }

        return done(null, user);

    });


}));

passport.serializeUser(function(user, done) {

    console.log('serializeUser', user);

    done(null, user.id);

});

passport.deserializeUser(function(id, done) {

    var user = _.findWhere(userData, { id : id });

    console.log('deserializeUser', user);

    done(null, user);

});

app.use('/', routes);

// Simple ping to check if server is up.
app.get('/api/v1', function (req, res) {

    res.send('1');

});


app.use('/api/v1/sites', api.sites);
app.use('/api/v1/subjects', api.subjects);
app.use('/api/v1/devices', api.devices);
app.use('/api/v1/devices2', api.devices2);
app.use('/api/v1/diaries', api.diaries);
app.use('/api/v1/logs', api.logs);
app.use('/api/v1/SWAPI', api.swapi);


app.post('/api/v1/login', passport.authenticate('local', { failureRedirect: '/login', failureFlash: false }), function (req, res) {

    res.send(req.user);

});

app.get('/api/v1/logout', function (req, res) {

    req.logout();

    res.send();

});

app.get('/ActivateResourceStrings.aspx', function (req, res) {

    res.set('Content-Type', 'application/javascript');
    res.send('LF.Preferred = { language : "en", locale : "US" };');

});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
