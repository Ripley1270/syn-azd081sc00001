﻿using NancyConsole.log;
using System;

namespace NancyConsole
{
    public class SettingsModule : Nancy.NancyModule
    {
        public SettingsModule()
        {
            Get["/"] = _ => "Received GET request";

            Post["/"] = _ => "Received POST request";

            Post["/setTimeZone"] = _ =>
            {
                string client = this.Request.UserHostAddress;
                Boolean isLocal = client.Equals("127.0.0.1") || client.Equals("::1");

                LogHelper.Log("SetTimeZone request from: " + client + ". Is local request: " + isLocal);
                
                int statusCode = 200;

                if (isLocal)
                {
                    var id = this.Request.Body;
                    var length = this.Request.Body.Length;
                    var data = new byte[length];
                    id.Read(data, 0, (int)length);
                    var body = System.Text.Encoding.Default.GetString(data);
                    
                    LogHelper.Log("Setting time zone: " + body);

                    TimeUtil.SetSystemTimeZone(body);
                }
                else {                    
                    statusCode = 403;
                }

                LogHelper.Log("Setting time zone result code: " + statusCode);

                return statusCode;
            };

            Get["/getTimeZoneList"] = parameters =>
            {
                string client = this.Request.UserHostAddress;
                Boolean isLocal = client.Equals("127.0.0.1") || client.Equals("::1");
                LogHelper.Log("GetTimeZoneList request from: " + client + ". Is local request: " + isLocal);
                int statusCode = 403;               

                if (isLocal)
                {
                    string time = this.Request.Query["time"];                    
                    long unixTimestamp = (long)Convert.ToDouble(time);

                    string result = TimeUtil.GetTimeZoneList(unixTimestamp);

                    if (result != null)
                    {
                        return result;
                    }
                    else {
                        return 500;
                    }
                }

                LogHelper.Log("GetTimeZoneList result code: " + statusCode);

                return statusCode;
            };

            Get["/getCurrentTimeZone"] = _ =>
            {
                string client = this.Request.UserHostAddress;
                Boolean isLocal = client.Equals("127.0.0.1") || client.Equals("::1");
                LogHelper.Log("GetTimeZoneList request from: " + client + ". Is local request: " + isLocal);
                int statusCode = 403;

                if (isLocal)
                {
                    string result = TimeUtil.getCurrentTimeZone();

                    if (result != null)
                    {
                        return result;
                    }
                    else
                    {
                        return 500;
                    }
                }

                LogHelper.Log("GetCurrentTimeZone result code: " + statusCode);

                return statusCode;
            };
        }
    }
}
