﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyConsole.log
{
    public abstract class BaseLogger
    {
        protected readonly object lockObj = new object();

        public abstract void Log(string message);
    }
}
