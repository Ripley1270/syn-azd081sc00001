﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyConsole.log
{
    class LogHelper
    {
        private static BaseLogger logger = null;
        private static LogTarget activeLogger = LogTarget.EventLog;//just one for now

       public static void Log(LogTarget target, string message)
        {
            //@todo more loggers
            Log(message);
        }
        
        public static void Log(string message)
        {
            switch(activeLogger)
            {
                case LogTarget.EventLog:
                    logger = new EventLogger();
                    logger.Log(message);
                    break;
            }
            
        }

    }
}
