# NLS Validation Tool
# @@@ Authors: Saurin Shah and Prem Shah
# Description: 


# Import packages:

import glob
import os
import codecs
import json
import sys


# INPUT: Path to a 'coreStrings.json' file for a particular language.
# RETURNS: A dictionary structure representing the JSON contents of the 'Resources' key in given file.

def language_dictionary(path):
    en_us_op = codecs.open(path, "r", "utf-8")
    en_us_dict = json.loads(en_us_op.read())
    ref_dict = en_us_dict['resources']
    en_us_op.close()
    return ref_dict


# INPUT: Folder path (for Sitepad or Logpad), the dictionary structure of the reference 'en-US' 
#(coreStrings.json) file, desired output result file path.
# RETURNS: A dictionary of the differences in the Property Names and values of the 'resources' key 
# in the given reference file and all other language files. 

def validate_languages(path,ref_dict,outputFile):
    count = 0
    print(path)
    resultDict = {}
    for root, dirs, files in os.walk(path, topdown=False):
        if(len(files)!=0):
            pth = str(root) + '\\' + str(files[0])
            dir_name = str(root).split("\\")[-1]
            try: 
                lang_dict = language_dictionary(pth)
                dic = compare_dict(lang_dict,ref_dict)
                resultDict[dir_name] = dic
                count = count + 1
            except ValueError as e:
                #print('\nInvalid json: %s' % e)
                print('\nException in the following file handled successfully')
                print(pth)
                file = codecs.open(pth,"r", "utf-8")
                data = "".join(file.readlines()[2:])
                file.close()
                pth = path + '\\temporary.json'
                filet = codecs.open(pth, "w+", "utf-8")
                filet.write(data)
                filet.close()
                lang_dict = language_dictionary(pth)
                dic = compare_dict(lang_dict,ref_dict)
                resultDict[dir_name] = dic
                count = count + 1
    print("\nChecking completed for the directory:  " + sys.argv[1])
    print("Number of files (languages) checked: " + str(count))
    print("\nFind the result file at the following path- " + outputFile)
    return resultDict
		
		
# INPUT: Dictionary representation of two language files
# RETURNS: A dictionary of 'keys' (or 'Property Names') that are found in the English language reference file 
# but not in the other, or vice-versa; under the 'resources' property name.

def compare_dict(lang_dict,ref_dict):
    dict = {}
    dict['Keys not in US lang'] = check_with_ref_dict(lang_dict,ref_dict)
    dict['Additional keys in reference language file (en-US)'] = check_with_ref_dict(ref_dict,lang_dict)
    return dict
		
		

# INPUT: Dictionary structure of two language files.
# RETURNS: A list containing the property names (keys) that are present in 'lang_dict' but not in 'ref_dict'

def check_with_ref_dict(lang_dict,ref_dict):
    differentKeys = []
    for key in lang_dict:
        if(key not in ref_dict):
            differentKeys.append(key)
        else:
            if key is None:
                differentKeys.append(key)
    return differentKeys



# INPUT: JSON result data, Folder path (for Sitepad or Logpad), desired output result file path.
# RETURNS: -NA-
# Description: Saves the result of the Language key checker as a JSON file named 'Sitepad_Test_Result.json' 
# or 'Logpad_Test_Result.json' at the path provided by the user and deletes the 'temporary.json' file 
# created earlier by the tool to deal with Exceptions occuring in any files.

def saveToFile(resultDict,path,outputFile):
	os.remove(path + '\\temporary.json')
	with open(outputFile, 'w+') as f:
		json.dump(resultDict, f, indent=4, sort_keys=True)
	f.close()


	
# Main Function
# INPUT: Arguments from command line while running: (1) Folder path (Logpad/Sitepad), (2) Outfile file 
# (3) Optional reference English language file path (coreStrings.json) 

def main():
	path = sys.argv[1]
	outputFile = sys.argv[2]
	
	if len(sys.argv)>3:									  # If reference language filepath is not specified, then use default
		en_us_path = sys.argv[3] + '\\coreStrings.json'
	else:
		en_us_path = path + "\\en-US\\coreStrings.json"

	ref_dict = language_dictionary(en_us_path)
	resultDict = validate_languages(path,ref_dict,outputFile)
	saveToFile(resultDict, path, outputFile)


# Run the Language Key Checker Tool:

main()