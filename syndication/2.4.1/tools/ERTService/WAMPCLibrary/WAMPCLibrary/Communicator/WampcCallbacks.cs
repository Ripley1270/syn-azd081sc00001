﻿namespace invivodata.Interop.WAMPC
{
    public static class WampcCallbacks
    {
        public delegate void GetDeviceInformationCallback(WampcCommunicationResult<WampcDeviceInformation> callbackResult);
        public delegate void PairingCallback(WampcCommunicationResult<int?> callbackResult);
        public delegate void ProgramDeviceCallback(WampcCommunicationResult<int> newAcquisitionId);
        public delegate void DeleteDeviceDataCallback(WampcCommunicationResult<bool> callbackResult);
        public delegate void PdfLoadedCallback(WampcCommunicationResult<ECGDownloadData> pdfByteArray);
    }
}
