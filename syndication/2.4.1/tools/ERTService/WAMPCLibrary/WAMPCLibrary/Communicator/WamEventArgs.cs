﻿using System;

namespace invivodata.Interop.WAMPC
{
    public class WamEventArgs : EventArgs
    {
        private readonly Exception _exception;
        private readonly string _message;
        private readonly WamStatus _status;

        /// <summary>
        /// Enum representing potential statuses of the WAM PC during an operation
        /// </summary>
        public enum WamStatus
        {
            None, NotConnected, Deleting, DownloadingEcg, RenderingPdf, Pairing, Analyzing,
            Preparing,
            Failure,
            Success,
            ECGDataRendered,
            DeleteSuccess
        }

        /// <summary>
        /// Status message returned by the WAM PC communication event
        /// </summary>
        public string Message { get { return _message; } }

        /// <summary>
        /// Exception thrown during the WAM PC communication event
        /// </summary>
        public Exception Exception { get { return _exception; } }

        /// <summary>
        /// Current status of the WAM PC during a communication event
        /// </summary>
        public WamStatus Status { get { return _status; } }

        /// <summary>
        /// Creates a instance of WamEventArgs
        /// </summary>
        /// <param name="msg">Message to return to event handler</param>
        /// <param name="status">Status of communication</param>
        public WamEventArgs(string msg, WamStatus status)
        {
            _message = msg;
            _exception = null;
            _status = status;
        }

        /// <summary>
        /// Creates a instance of WamEventArgs when an exception is thrown
        /// </summary>
        /// <param name="msg">Message to return to event handler</param>
        /// <param name="exc">Exception thrown during communication event</param>
        /// <param name="status">Status of communication</param>
        public WamEventArgs(string msg, Exception exc, WamStatus status)
        {
            _message = msg;
            _exception = exc;
            _status = status;
        }
    }
}
