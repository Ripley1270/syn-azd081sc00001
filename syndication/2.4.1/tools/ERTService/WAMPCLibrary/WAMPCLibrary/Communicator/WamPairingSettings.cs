﻿namespace invivodata.Interop.WAMPC
{
    /// <summary>
    /// Defines configurable WAM Settings used when pairing the WAM and UTK
    /// </summary>
    public class WamPairingSettings
    {
        /// <summary>
        /// The duration of the data to acquire in seconds
        /// </summary>
        /// <remarks>Must be in multiples of 10</remarks>
        public int MaxDuration;

        /// <summary>
        /// The minimal duration of consecutive good data to to be considered a successful acquisition
        /// </summary>
        /// <remarks>Must be in multiples of 10</remarks>
        public int MinDuration;

        /// <summary>
        /// Number of seconds the user needs to keep the acquisition button pressed to initiate a test acquisition
        /// </summary>
        public int TestAcq;

        /// <summary>
        /// Number of minutes to self power-off
        /// </summary>
        public int SelfPowerOff;
    }
}
