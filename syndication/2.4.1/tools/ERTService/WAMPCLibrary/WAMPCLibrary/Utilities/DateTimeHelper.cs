﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Runtime.InteropServices;

namespace invivodata.Interop.WAMPC.Utilities
{
	internal class DateTimeHelper
	{
		/// <summary>
		/// Returns system time in UTC.
		/// </summary>
		public static DateTime UtcNow
		{
			get
			{
				return DateTime.UtcNow;
			}
		}
	}
}
