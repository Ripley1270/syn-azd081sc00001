import NotifyView from 'core/views/NotifyView';

/**
 * Create a notification dialog.
 * @param  {[Object]} params - parameters
 * @returns {RuntimeNotifyCreator}
 */
export const notifyDialogCreatorWithNamespace = (params = {}) => (runtimeParams = {}) => {
    _.extend(params, runtimeParams);

    // Was actions/notify.js

    let modal = new NotifyView(),
        stringsToFetch = {
            header: params.header || 'PLEASE_CONFIRM',
            ok: params.ok || 'OK'
        };

    if (!params.coreString) {
        stringsToFetch.message = params.message;
    }

    return LF.getStrings(stringsToFetch, $.noop, { namespace : params.namespace || 'STUDY'})
             .then((strings) => {
                 // @TODO SCREENSHOT Maybe we should use dynamic text instead for these built messages, that way we can screenshot them better
                 let showMessage = () => {
                     let httpErrorMsg,
                         errorObj = params.httpRespCode || '',
                         message = strings.message;

                     if (_.isArray(params.message)) {
                         message = strings.message.join('<br /><br />');
                     }

                     if (errorObj.httpCode && errorObj.errorCode) {
                         httpErrorMsg = `HTTP Error: ${errorObj.httpCode}-${errorObj.errorCode}`;
                     } else if (errorObj.httpCode) {
                         httpErrorMsg = `HTTP Error: ${errorObj.httpCode}`;
                     }

                     message = `${params.coreString || message} <br/> ${httpErrorMsg || ''}`;

                     return message;
                 };

                 return modal.show({
                     header: strings.header,
                     body: showMessage(),
                     ok: strings.ok,
                     type: params.type || ''
                 });
             });
};
