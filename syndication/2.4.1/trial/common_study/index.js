/*
 * PDE_TODO:
 * import files that don't contribute to assets object, but need to ensure the content executes
 * for example, a file that assigns a function into the global LF namespace
 */
// import './exampleBranchingFunctions';
// import './exampleScheduleFunctions';

import './utils';

import './MessageHelpers';
import './Messages';

import './checkRepeatByDateAvailability';

import { mergeObjects } from 'core/utilities/languageExtensions';

let studyDesign = {};

/*
 * PDE_TODO:
 * Collect and merge studyDesign objects
 */
// import rules from './exampleRules';
// import messages from './exampleMessages';
// import templates from './exampleTemplates';
// import dynamicText from './exampleDynamicText'

import SD from './study-design';


studyDesign = mergeObjects(studyDesign, SD /* , diary, rules, templates, dynamicText*/);


// export the assets object
export default { studyDesign };
