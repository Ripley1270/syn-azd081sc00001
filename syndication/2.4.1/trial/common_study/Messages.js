import { MessageRepo } from 'core/Notify';
import { notifyDialogCreatorWithNamespace } from './MessageHelpers';

MessageRepo.add({
    type: 'Dialog',
    key: 'BPI_SF_ALERT',
    message: notifyDialogCreatorWithNamespace({
        header: 'BPI_SF_ALERT_HEADER',
        message: 'BPI_SF_ALERT',
        type: 'default'
    })
});

MessageRepo.add({
    type: 'Dialog',
    key: 'MED_QTY_INPUT_ERR_1_T0_99',
    message: notifyDialogCreatorWithNamespace({
        header: 'MED_QTY_INPUT_ERR_HEADER',
        message: 'MED_QTY_INPUT_ERR_1_T0_99',
        type: 'warning'
    })
});

MessageRepo.add({
    type: 'Dialog',
    key: 'MED_QTY_INPUT_ERR_1_T0_9',
    message: notifyDialogCreatorWithNamespace({
        header: 'MED_QTY_INPUT_ERR_HEADER',
        message: 'MED_QTY_INPUT_ERR_1_T0_9',
        type: 'warning'
    })
});
