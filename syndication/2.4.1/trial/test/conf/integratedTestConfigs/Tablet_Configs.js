export default {
    /*
    If your database tests are all failing due to credential issues,
    add your personal staging sql management studio credentials here to override.
 */
    user: '',
    password: '',

    // add string IGs to this array to exclude them from database validation
    excludeIGs: ['PT', 'Activate_User', 'Add_User', 'Deactivate_User', 'Add_Patient', 'Edit_Patient', 'Edit_User', 'First_Site_User', 'RaterTraining', 'SkipVisit', 'Time_Confirmation', 'TRANSCRIPTION_REPORT_DATE'],

    // add string IGs to this array to explicitly include them in database validation
    includeIGs: ['VisitStartDate', 'VisitEndDate', 'Protocol'],

    // add string SUs to this array to exclude them from database validation
    excludeSUs: ['Activate_User', 'New_User', 'Deactivate_User', 'Edit_Patient', 'Edit_User', 'First_Site_User', 'New_Patient', 'Rater_Training', 'Skip_Visits', 'TC'],

    // add string SUs to this array to explicitly include them in database validation
    includeSUs: ['Assignment'],

    /*
     * add objects containing a string IG and
     * an array of string ITs to exclude those ITs from database validation
     *
     * IGs listed in excludeIGs will be automatically ignored for the validateITs test
    */
    excludeITs: [
        {
            IG: 'RaterTraining',
            ITs: ['RATER_TRAINING_Q1']
        }
    ],

    // add objects containing a string IG and an array of string ITs to explicitly include those ITs in database
    // validation
    includeITs: [
        {
            IG: 'Protocol',
            ITs: ['Protocol']
        }
    ]
};
