This location should house all jasmine spec.js files for trial/common_study items.
This will ensure that the tests are ran for all modalities during their respective test suite grunt tasks.

You may make any subdirectories to improve organization, or leave all specs at the top level of this directory.

The specs in this directory and any subdirectories will be executed as part of the 'trial.common_study' package.

To comply with the html report generation, the first parameter in describe should be a dot path to the component
    being tested, starting in the common_study directory.

For example, a unit test spec for a file 'trial/common_study/branching/MyCommonBranchFunction.js' should have the
    following describe statement:

    describe('branching.MyCommonBranchFunction', () => { ... };

The reporting feature will prepend the package name,
    to a full descriptor of 'trial.common_study.branching.MyCommonBranchFunction'

Then, when the html report is generated, the tests will be organized by these dot paths, improving readability.
