import { getNested } from 'core/utilities/coreUtilities';

export function affidavitTests (studyDesign) {
    let affidavits = getNested('affidavits', studyDesign);
    it('affidavits should be defined', () => {
        expect(affidavits).toBeDefined();
        expect(affidavits).toBeAnArray();
    });
    it('affidavits should contain DEFAULT', () => {
        expect(_(affidavits).find({ id: 'DEFAULT' })).toBeTruthy();
    });
    it('affidavits should contain CheckBoxAffidavit', () => {
        expect(_(affidavits).find({ id: 'CheckBoxAffidavit' })).toBeTruthy();
    });
    it('affidavits should contain SiteCheckBoxAffidavit', () => {
        expect(_(affidavits).find({ id: 'SiteCheckBoxAffidavit' })).toBeTruthy();
    });
    it('affidavits should contain P_CheckBoxAffidavit', () => {
        expect(_(affidavits).find({ id: 'P_CheckBoxAffidavit' })).toBeTruthy();
    });
    it('affidavits should contain CustomAffidavit', () => {
        expect(_(affidavits).find({ id: 'CustomAffidavit' })).toBeTruthy();
    });
    it('affidavits should contain SignatureAffidavit', () => {
        expect(_(affidavits).find({ id: 'SignatureAffidavit' })).toBeTruthy();
    });
    it('affidavits should contain P_SignatureAffidavit', () => {
        expect(_(affidavits).find({ id: 'P_SignatureAffidavit' })).toBeTruthy();
    });
    it('affidavits should contain FirstUserSignatureAffidavit', () => {
        expect(_(affidavits).find({ id: 'FirstUserSignatureAffidavit' })).toBeTruthy();
    });
    it('affidavits should contain NewUserAffidavit', () => {
        expect(_(affidavits).find({ id: 'NewUserAffidavit' })).toBeTruthy();
    });
    it('affidavits should contain PasswordAffidavit', () => {
        expect(_(affidavits).find({ id: 'PasswordAffidavit' })).toBeTruthy();
    });
    it('affidavits should contain P_PasswordAffidavit', () => {
        expect(_(affidavits).find({ id: 'P_PasswordAffidavit' })).toBeTruthy();
    });
}
