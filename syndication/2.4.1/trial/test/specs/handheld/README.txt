This location should house all jasmine spec.js files for trial/handheld items.
This will ensure that the tests are ran for only the handheld modality specific unit test suite.

You may make any subdirectories to improve organization, or leave all specs at the top level of this directory.

The specs in this directory and any subdirectories will be executed as part of the 'trial.handheld' package.

To comply with the html report generation, the first parameter in describe should be a dot path to the component
    being tested, starting in the handheld directory.

For example, a unit test spec for a file 'trial/handheld/STUDY/branching/MyHHBranchFunction.js' should have the
    following describe statement:

    describe('STUDY.branching.MyHHBranchFunction', () => { ... };

The reporting feature will prepend the package name,
    to a full descriptor of 'trial.handheld.STUDY.branching.MyHHBranchFunction'

Then, when the html report is generated, the tests will be organized by these dot paths, improving readability.
