import trialImport from 'trial/handheld';
import customMatchers from 'PDE_Core/test/customMatchers';
import { getNested } from 'core/utilities/coreUtilities';
import { commonStudyDesign } from 'trial/test/specs/common_study/validateStudyDesign/StudyDesign.common.partialspec';
import { hhStudyDesign } from 'trial/test/specs/handheld/validateStudyDesign/StudyDesign.hh.partialspec';
import { environmentsTests } from 'trial/test/specs/common_study/validateStudyDesign/StudyDesign.environments.partialspec';
import { raterTrainingConfigTests } from 'trial/test/specs/common_study/validateStudyDesign/StudyDesign.raterTraning.partialspec';
import { timeZoneConfigTests } from 'trial/test/specs/common_study/validateStudyDesign/StudyDesign.timeZoneConfig.partialspec';
import { affidavitTests } from 'trial/test/specs/common_study/validateStudyDesign/StudyDesign.affidavits.partialspec';
import { rolesTests } from 'trial/test/specs/common_study/validateStudyDesign/StudyDesign.roles.partialspec';

let studyDesign = getNested('assets.studyDesign', trialImport);

beforeAll(() => {
    jasmine.addMatchers(customMatchers);
});

describe('validateAssets', () => {
    describe('StudyDesign_common', () => {
        commonStudyDesign(studyDesign);
    });
    describe('StudyDesign_handheld', () => {
        hhStudyDesign(studyDesign);
    });
    describe('environments', () => {
        environmentsTests(studyDesign);
    });
    describe('raterTrainingConfig', () => {
        raterTrainingConfigTests(studyDesign, 'logpad');
    });
    describe('timeZoneConfig', () => {
        timeZoneConfigTests(studyDesign);
    });
    describe('affidavits', () => {
        affidavitTests(studyDesign);
    });
    describe('roles', () => {
        rolesTests(studyDesign, 'handheld');
    });
});
