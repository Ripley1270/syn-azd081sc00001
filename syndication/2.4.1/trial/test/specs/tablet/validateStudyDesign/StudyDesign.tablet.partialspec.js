import { getNested } from 'core/utilities/coreUtilities';

/**
 * series of unit tests to validate trial/handheld/STUDY/study-design/handheld_study-design settings
 */
export function tabletStudyDesign (studyDesign) {
    it('studyVersion should be defined', () => {
        expect(getNested('studyVersion', studyDesign)).toBeDefined();
    });
    it('studyDbVersion should be defined', () => {
        expect(getNested('studyDbVersion', studyDesign)).toBeDefined();
    });
    it('enablePartialSync should be defined', () => {
        expect(getNested('enablePartialSync', studyDesign)).toBeDefined();
    });
    it('sessionTimeout should be defined', () => {
        expect(getNested('sessionTimeout', studyDesign)).toBeDefined();
    });
    it('questionnaireTimeout should be defined', () => {
        expect(getNested('questionnaireTimeout', studyDesign)).toBeDefined();
    });
    it('subjectAssignmentPhase should be defined', () => {
        expect(getNested('subjectAssignmentPhase', studyDesign)).toBeDefined();
    });
    describe('participantSettings', () => {
        let pSet = getNested('participantSettings', studyDesign);
        it('participantSettings should be defined', () => {
            expect(pSet).toBeDefined();
        });
        it('participantSettings.participantID should be defined', () => {
            expect(getNested('participantID', pSet)).toBeDefined();
        });
        it('participantSettings.participantID.seed should be defined', () => {
            let seed = getNested('participantID.seed', pSet);
            expect(seed).toBeDefined();
            expect(seed).toEqual(jasmine.any(Number));
        });
        it('participantSettings.participantID.steps should be defined', () => {
            let steps = getNested('participantID.steps', pSet);
            expect(steps).toBeDefined();
            expect(steps).toEqual(jasmine.any(Number));
        });
        it('participantSettings.participantID.max should be defined', () => {
            let max = getNested('participantID.max', pSet);
            expect(max).toBeDefined();
            expect(max).toEqual(jasmine.any(Number));
        });
        describe('formatID', () => {
            let participantIDFormat = getNested('participantIDFormat', pSet);
            let participantNumberPortion = getNested('participantNumberPortion', pSet);
            let siteNumberPortion = getNested('siteNumberPortion', pSet);
            it('participantSettings.participantIDFormat should be defined', () => {
                expect(participantIDFormat).toBeDefined();
                expect(participantIDFormat).toEqual(jasmine.any(String));
            });
            describe('participantNumberPortion', () => {
                it('participantSettings.participantNumberPortion should be defined', () => {
                    expect(participantNumberPortion).toBeDefined();
                    expect(participantNumberPortion).toEqual(jasmine.any(Array));
                });
                it('participantSettings.participantNumberPortion should contain a min and max value only', () => {
                    expect(participantNumberPortion.length).toBe(2);
                    expect(participantNumberPortion[0]).toEqual(jasmine.any(Number));
                    expect(participantNumberPortion[1]).toEqual(jasmine.any(Number));
                    expect(participantNumberPortion[0]).toBeLessThan(participantNumberPortion[1]);
                });
                it('participantSettings.participantNumberPortion values should be within the length of participantIDFormat', () => {
                    let pIDlength = participantIDFormat.length;
                    expect(participantNumberPortion[0]).toBeLessThan(pIDlength);
                    expect(participantNumberPortion[0]).toBeGreaterThanOrEqual(0);
                    expect(participantNumberPortion[1]).toBeLessThanOrEqual(pIDlength);
                    expect(participantNumberPortion[1]).toBeGreaterThan(0);
                });
            });
            describe('siteNumberPortion', () => {
                it('participantSettings.siteNumberPortion should be defined', () => {
                    expect(siteNumberPortion).toBeDefined();
                    expect(siteNumberPortion).toEqual(jasmine.any(Array));
                });
                it('participantSettings.siteNumberPortion should contain a min and max value only', () => {
                    expect(siteNumberPortion.length).toBe(2);
                    expect(siteNumberPortion[0]).toEqual(jasmine.any(Number));
                    expect(siteNumberPortion[1]).toEqual(jasmine.any(Number));
                    expect(siteNumberPortion[0]).toBeLessThan(siteNumberPortion[1]);
                });
                it('participantSettings.siteNumberPortion values should be within the length of participantIDFormat', () => {
                    let pIDlength = participantIDFormat.length;
                    expect(siteNumberPortion[0]).toBeLessThan(pIDlength);
                    expect(siteNumberPortion[0]).toBeGreaterThanOrEqual(0);
                    expect(siteNumberPortion[1]).toBeLessThanOrEqual(pIDlength);
                    expect(siteNumberPortion[1]).toBeGreaterThan(0);
                });
            });
            it('siteNumberPortion and participantNumberPortion ranges should not overlap', () => {
                if (siteNumberPortion[0] < participantNumberPortion[0]) {
                    expect(siteNumberPortion[1]).toBeLessThanOrEqual(participantNumberPortion[0]);
                } else {
                    expect(siteNumberPortion[0]).toBeGreaterThanOrEqual(participantNumberPortion[1]);
                }
            });
        });
    });
    describe('subjectAssignmentPhase', () => {
        let sAphase = getNested('subjectAssignmentPhase', studyDesign);
        it('subjectAssignmentPhase should be defined', () => {
            expect(sAphase).toBeDefined();
            expect(sAphase).toEqual(jasmine.any(Number));
        });
        it('subjectAssignmentPhase should reference an existing phase in studyPhase', () => {
            let studyPhase = getNested('studyPhase', studyDesign),
                foundPhase = false;
            for (let prop in studyPhase) {
                if (studyPhase[prop] === sAphase) {
                    foundPhase = true;
                    break;
                }
            }
            expect(foundPhase).toBeTruthy();
        });
    });
}
