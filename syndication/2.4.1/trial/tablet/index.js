import { mergeObjects } from 'core/utilities/languageExtensions';

/*
 * Initial import order must be PDE_Core -> trial/common -> trial/{{{modality}}}study -> individual diaries
 * This allows any global function overwrites to take place properly
 * For example, if PDE_Core defines LF.Utilities.someFunction,
 * then trial/common_study redefined LF.Utilities.someFunction,
 * then trial/{{{modality}}}/study again overwrote the function,
 * then finally trial/{{{modality}}}/someDiary overwrote the function,
 * then importing in this order ensures the someDiary definition is read last, and will be the definition used at runtime
 */

// Import PDE_Core/tablet:
import PDE_CoreTabletAssets from 'PDE_Core/tablet';

// import common study assets
import commonAssets from 'trial/common_study';

// import tablet study assets
import TabletStudyAssets from './STUDY';

let diaryAssets = {};

// PDE_TODO: Import diaries and merge diary assets
// import diary from './diary';

diaryAssets = mergeObjects(diaryAssets/* ,diary*/);

// merge assets from all sources
let assets = mergeObjects(PDE_CoreTabletAssets, commonAssets, TabletStudyAssets, diaryAssets);

/*
 *collect rules into function to be processed at startup.
 *This ensures PDE defined rules will overwrite core defined rules if they have the same id,
 *and because of the order we merged objects, rule IDs from PDE_Core are replaced by identical ids in
 *commonAssets, which are replaced by identical ids in HHStudyAssets, which are replaced by identical ids
 *in individual diary assets.
 *If mutliple diaries have the same rule id,
 *then the last diary imported will be the rule that exists after startup.
 */

// first, pull out the array of rules from studyDesign
let trialRules = assets.studyDesign.rules || [];

// delete the rules property from the studyDesign so the rules are only processed by our function
delete assets.studyDesign.rules;

// the assets object will have a 'studyRules' property that core expects to be a function with one parameter
// it executes this function by giving the parameter ELF.rules, to populate the ELF engine with trial defined rules
assets.studyRules = (rules) => {
    _(trialRules).forEach((rule) => {
        if (rules.find(rule.id)) {
            // if the rule already exists, remove it to be overwritten with the trial level rule
            rules.remove(rule.id);
        }
        rules.add(rule);
    });
};

/*
 * Collect templates into the appropriate property to be processed at startup
 * Moving the array here simplifies the addition of diary-specific templates since
 * they can be added directly to the studyDesign object in lower index files
 */
// first, create the expected reference to studyTemplates in the assets object
assets.studyTemplates = assets.studyDesign.templates || [];

// then, delete the studyDesign reference
delete assets.studyDesign.templates;


/*
 * Collect message objects into function to be processed at startup
 * Setting up the assets parameter here simplifies the addition of diary-specific messages
 * since they can be added directly to the studyDesign object in lower index files
 */
// first, pull out the array of messages from studyDesign
let trialMessages = assets.studyDesign.messages || [];

// then, delete the studyDesign reference
delete assets.studyDesign.messages;

// the assets object will have a 'studyMessages' property that core expects to be a function with no parameters
// it executes this function at study startup to populate the MessageRepo with trial defined messages
import { MessageRepo } from 'core/Notify';

assets.studyMessages = () => {
    MessageRepo.add(...trialMessages);
};


/*
 * Collect dynamicText objects and add them to the dynamicText module
 * Performing the dynamic text module setup here simplifies the addition of diary-specific dynamic text
 * since they can follow the same pattern as rules, templates, and messages and be added directly to the
 * studyDesign object in lower index files.
 */
// first, pull out the array of dynamic text objects from studyDesign
let trialDynamicText = assets.studyDesign.dynamicText || [];

// then, delete the studyDesign reference
delete assets.studyDesign.dynamicText;

// we just need to import the core/dynamicText module for the correct instance of the DynamicText class to be created,
// then add our trial level dynamic text to the module
import dynamicText from 'core/dynamicText';

dynamicText.add(...trialDynamicText);

// export trial object with an assets property (the name is important, don't change it)
export default { assets };
