import * as Battery from './BatteryLevel_Tools';
import CurrentSubject from 'core/classes/CurrentSubject';

export default {
    rules: [
        {
            id: 'GenerateBatteryLevel',
            trigger: 'ALARM:Trigger/AutoDial',
            evaluate: (filter, resume) => resume(!localStorage.getItem('screenshot')),
            salience: 999,
            resolve: [
                {
                    action () {
                        return CurrentSubject.getSubject()
                        .then((subject) => {
                            if (subject.get('phase') <= LF.StudyDesign.studyPhase.FOLLOWUP_2) {
                                return Battery.generateBatteryDiary();
                            }
                            return Q();
                        });
                    }
                }
            ]
        },
        {
            id: 'BatteryLevelSave',
            trigger: 'QUESTIONNAIRE:Completed/BatteryLevel',
            evaluate (filter, resume) {
                resume(!localStorage.getItem('screenshot'));
            },
            salience: 999,
            resolve: [
                {
                    action () {
                        /*
                         * STYPRD1L and STYDAY1N added from generic study rules
                         */
                        const subject = LF.Data.Questionnaire.subject, // should always be available
                            today = new Date(),
                            follup2Phase = LF.StudyDesign.studyPhase.FOLLOWUP_2;

                        Battery.getDaysAssessmentsAvailable(subject, today)
                            .then((daysAvailable) => {
                                if (daysAvailable) {
                                    LF.Data.Questionnaire.addIT({
                                        question_id: 'DAYAVL1N',
                                        questionnaire_id: 'BatteryLevel',
                                        response: daysAvailable,
                                        IG: 'BatteryLevel',
                                        IGR: 0,
                                        IT: 'DAYAVL1N'
                                    });
                                }
                            })
                            .then(() => {
                                let batLevel = LF.Data.Questionnaire.data.dashboard.get('battery_level');
                                if (batLevel) {
                                    LF.Data.Questionnaire.addIT({
                                        question_id: 'BRYLVL1N',
                                        questionnaire_id: 'BatteryLevel',
                                        response: String(batLevel),
                                        IG: 'BatteryLevel',
                                        IGR: 0,
                                        IT: 'BRYLVL1N'
                                    });
                                }
                                return Battery.getFirstDayWithinWindow(subject);
                            })
                            .then((windowDay) => {
                                if (windowDay) {
                                    LF.Data.Questionnaire.addIT({
                                        question_id: 'STRDAY1N',
                                        questionnaire_id: 'BatteryLevel',
                                        response: windowDay,
                                        IG: 'BatteryLevel',
                                        IGR: 0,
                                        IT: 'STRDAY1N'
                                    });
                                }

                                if (Battery.isPhaseChangeYesterday(subject, follup2Phase)) {
                                    LF.Data.Questionnaire.addIT({
                                        question_id: 'STDEND1B',
                                        questionnaire_id: 'BatteryLevel',
                                        response: true,
                                        IG: 'BatteryLevel',
                                        IGR: 0,
                                        IT: 'STDEND1B'
                                    });
                                }
                            });
                    }
                }
            ]
        }
    ]
};
