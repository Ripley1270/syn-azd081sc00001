import { mergeObjects } from 'core/utilities/languageExtensions';

import BatteryLevel from './BatteryLevel';
import rules from './BatteryLevel_Rules';

let studyDesign = {};
studyDesign = mergeObjects(studyDesign, BatteryLevel, rules);

export default { studyDesign };
