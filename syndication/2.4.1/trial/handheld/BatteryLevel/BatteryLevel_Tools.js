import QuestionnaireView from 'core/views/QuestionnaireView';
import Data from 'core/Data';
import CurrentSubject from 'core/classes/CurrentSubject';
import { getAssessmentDay, getWindowParams } from 'trial/handheld/STUDY/study-tools';

export function generateBatteryDiary () {
    return CurrentSubject.getSubject()
        .then((subject) => {
            let options = { id: 'BatteryLevel', subject },
                view = new QuestionnaireView(options);

            Data.Questionnaire = view;

            return Q()
                .then(() => {
                    return view.prepDashboardData(options.id);
                })
                .then(() => {
                    return view.prepScreens();
                })
                .then(() => {
                    return view.prepQuestions();
                })
                .then(() => {
                    return view.configureAffidavit();
                })
                .then(() => {
                    return view.prepAnswerData(options.id, 1); // weird that this needs a number
                })
                .then(() => Q.Promise((resolve) => {
                    LF.Wrapper.Utils.getBatteryLevel((batteryLevel) => {
                        view.data.dashboard && view.data.dashboard.set({ battery_level: batteryLevel });

                        // This affidavit variable must be sent or the form will be rejected
                        let model = new view.Answer({
                            question_id: 'AFFIDAVIT',
                            questionnaire_id: 'BatteryLevel',
                            response: '1',
                            SW_Alias: 'SubmitForm'
                        });

                        view.answers.add(model);

                        resolve();
                    });
                }))
                .then(() => {
                    // Fire questionnaire completion rule so PDE can add custom data
                    // TODO: some code may reference LF.router.view, which wont be here and could cause crashes
                    return view.completeDiary();
                });
        });
}


/*
 * Gets the earliest assessment day for any assessment with the current window
 * Returns null if any values are not available i.e. assessment day or not within window
 * @param {Subject} the subject model
 * @return {Q.Promise} - promise with assessment day
*/
export function getFirstDayWithinWindow (subject) {
    let diaries = ['BPID', 'FACTP', 'EQ5D5L'],
        pendingParams  = diaries.map(diary => getWindowParams(diary, subject));
    return Q.all(pendingParams).then((params) => {
        let startVals = _.pluck(params, 'start');
        if (startVals.size) {
            return Math.min(...startVals.filter(value => value));
        }
        return null;
    });
}

/*
 * Gets the numbers of days that assessments have been available in the current window
 * or null if randomization day is not set or we are outside of the window
 * @param {Model} The subject model
 * @param {Date} Today's date
 * @return {Q.Promise} Number with days available
*/
export function getDaysAssessmentsAvailable (subject, today) {
    return Q.all([
        getAssessmentDay(today),
        getFirstDayWithinWindow(subject)
    ])
        .then(([assessmentDay, firstDayInWindow]) => {
            if (!assessmentDay || !firstDayInWindow) {
                return null;
            }
            return assessmentDay - firstDayInWindow;
        });
}

const isYesterday = (moment) => {
    if (moment) {
        let yesterday = moment(new Date()).subtract(1, 'days').startOf('day');
        return moment.isSame(yesterday, 'day');
    }
    return false;
};

/*
 * Check if phase was entered yesterday.
 * @param {Model} backbone model (subject) object to check values from.
 * @param {Number} phase to check against.
 * @return {Boolean} if yesterday or not.
 */
export function isPhaseChangeYesterday (subject, phase) {
    // are we in the current phase?
    if (subject.get('phase') === phase) {
        let phaseDt = moment(subject.get('phaseStartDateTZOffset'));
        return isYesterday(phaseDt);
    }
    return false;
}
