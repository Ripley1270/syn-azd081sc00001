export default {
    questionnaires: [
        {
            id: 'BatteryLevel',
            SU: 'BatteryLevel',
            displayName: 'DISPLAY_NAME',
            className: 'BatteryLevel',
            previousScreen: true,
            affidavit: 'DEFAULT',
            screens: [
                'BatteryLevel010'
            ],
            branches: [],
            initialScreen: []
        }
    ],
    screens: [
        {
            id: 'BatteryLevel010',
            classname: 'BatteryLevel010',
            questions: [
                {
                    id: 'BatteryLevel010_Q0',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'BatteryLevel010_Q0',
            text: 'BatteryLevel010_Q0',
            className: 'BatteryLevel010_Q0'
        }
    ]
};
