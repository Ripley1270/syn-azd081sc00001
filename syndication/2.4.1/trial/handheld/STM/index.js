import { mergeObjects } from 'core/utilities/languageExtensions';

import SubjectTrainingModule from './SubjectTrainingModule';
import Rules from './SubjectTrainingModule_Rules';

let studyDesign = {}; // object
studyDesign = mergeObjects(studyDesign, SubjectTrainingModule, Rules);

export default { studyDesign };
