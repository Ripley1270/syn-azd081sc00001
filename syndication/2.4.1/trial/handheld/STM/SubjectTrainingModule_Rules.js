import { removeITByIG } from 'trial/handheld/STUDY/study-tools';
import PDE from 'PDE_Core/common/PDE';

export default {
    rules: [
        {
            id: 'SubjectTrainingModuleCompleted',
            trigger: 'QUESTIONNAIRE:Completed/STM',
            evaluate: true,
            resolve: [{
                action: () => {
                    LF.Data.Questionnaire.addIT({
                        question_id: 'TRNDAT1S',
                        questionnaire_id: 'STM',
                        response: moment(new Date()).format('DD-MMM-YYYY HH:mm'),
                        IG: 'SubjectTrainingModule',
                        IGR: 0,
                        IT: 'TRNDAT1S'
                    });
                }
            }]
        }
    ]
};

