import getRaterTrainingURL from 'core/widgets/param-functions/getRaterTrainingURL';

export default {
    questionnaires: [
        {
            id: 'STM',
            SU: 'STM',
            displayName: 'DISPLAY_NAME',
            className: 'questionnaire',
            previousScreen: false,
            affidavit: 'STMAffidavit',
            screens: [
                'SubjectTrainingModule_STM010'
            ],
            branches: [],
            initialScreen: [],
            accessRoles: [
                'subject',
                'site'
            ]
        }],
    screens: [
        {
            id: 'SubjectTrainingModule_STM010',
            className: 'SubjectTrainingModule_STM010',
            questions: [
                {
                    id: 'SubjectTrainingModule_STM010_Q1',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'SubjectTrainingModule_STM010_Q1',
            text: 'SubjectTrainingModule_STM010_Q1_TEXT',
            IG: 'SubjectTrainingModule',
            IT: 'SubjectTrainingModule_STM010_Q1',
            localOnly: true,
            title: '',
            className: 'SubjectTrainingModule_STM010_Q1',
            widget: {
                id: 'SubjectTrainingModule_STM010_1_Q1_W1',
                type: 'Browser',
                className: 'SKIP_VISIT',
                linkText: 'SubjectTrainingModule_STM010_1_Q1_W1',
                url: () => {
                    return getRaterTrainingURL('default');
                },
                browserCloseURLPattern: /goodbye.html$/
            }
        }
    ]
};
