import Logger from 'core/Logger';

/*
 * Branch if current date corresponds to a day between day 1 and day 26 from
 * randomization date entered on screen VC050 (randomization date = Day1)
 */
LF.Branching.branchFunctions.branchIfBeforeDay27 = (branchObj, view) => {
    const logger = new Logger('branchIfBeforeDay27');
    return Q.promise((resolve) => {
        let [rndDTAns] = LF.router.view().queryAnswersByID(branchObj.branchParams.questionId),
            rndDate = moment(rndDTAns.get('response')),
            day27 = rndDate.add(26, 'days').startOf('day'), // add 26 as rand date is day 1
            result = day27.isAfter(new Date());

        logger.warn(result ? 'Today IS before rand + 27' : 'Today IS NOT before rand + 27');
        resolve(result); // branch if within rand + 27
    });
};
