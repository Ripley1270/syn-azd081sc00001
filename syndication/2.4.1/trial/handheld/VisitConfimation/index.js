import { mergeObjects } from 'core/utilities/languageExtensions';

import VisitConfirmation from './VisitConfirmation';
import Rules from './VisitConfirmation_Rules';

import './VisitConfirmation_Branching';

let studyDesign = {}; // object
studyDesign = mergeObjects(studyDesign, VisitConfirmation, Rules);

export default { studyDesign };
