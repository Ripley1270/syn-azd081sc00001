export const VST1L_RE_SCCREENING = '10';
export const VST1L_START_TREATMENT = '20';
export const VST1L_TREATMENT_DISCONTINUATION = '30';
export const VST1L_PROG_AND_TREAT_DISCONTUNATION = '40';
export const VST1L_PROGRESSION = '50';
export const VST1L_STUDY_DISCONTINUATION = '60';

export const VisitToPhaseMap = () => {
    let Phases = LF.StudyDesign.studyPhase;
    return new Map([
        [VST1L_RE_SCCREENING, Phases.RE_SCREENING_1], // Re-screening
        [VST1L_START_TREATMENT, Phases.TREATMENT], // Start Treatment
        [VST1L_TREATMENT_DISCONTINUATION, Phases.FOLLOWUP_1], // Treatment Discontinuation
        [VST1L_PROG_AND_TREAT_DISCONTUNATION, Phases.FOLLOWUP_2], // Progression & Treatment Discontinuation
        [VST1L_PROGRESSION, Phases.FOLLOWUP_2], // Progression
        [VST1L_STUDY_DISCONTINUATION, Phases.PRE_TERMINATION] // Study Discontinuation
    ]);
};
