import { VisitToPhaseMap } from './VisitConfirmation_Tools';
import BranchHelpers from 'core/branching/branchingHelpers';
import PDE from 'PDE_Core/common/PDE';
import Logger from 'core/Logger';
import { buildStudyWorksDateTimeString } from 'PDE_Core/common/utilities/QuestionnaireUtils';
import { calculateStudyDay } from 'trial/handheld/STUDY/study-tools';
import VisitConfirmationModel from 'trial/handheld/STUDY/models/VisitConfirmationModel';

function updateReScreening (visitPhase, phase) {
    const studyPhases = LF.StudyDesign.studyPhase;
    switch (phase) {
        case studyPhases.SCREENING:
            return studyPhases.RE_SCREENING_1;
        case studyPhases.RE_SCREENING_1:
            return studyPhases.RE_SCREENING_2;
        case studyPhases.RE_SCREENING_2:
            return studyPhases.RE_SCREENING_3;
        case studyPhases.RE_SCREENING_3:
            return studyPhases.RE_SCREENING_4;
        case studyPhases.RE_SCREENING_4:
            return studyPhases.RE_SCREENING_5;
        case studyPhases.RE_SCREENING_5:
            return studyPhases.RE_SCREENING_6;
        case studyPhases.RE_SCREENING_6:
            return studyPhases.RE_SCREENING_7;
        case studyPhases.RE_SCREENING_7:
            return studyPhases.RE_SCREENING_8;
        case studyPhases.RE_SCREENING_8:
            return studyPhases.RE_SCREENING_9;
        case studyPhases.RE_SCREENING_9:
            return studyPhases.RE_SCREENING_10;
        default:
            throw new Error('Update ReScreening does not match any phase!');
    }
}

export default {
    rules: [
        {
            id: 'VisitConfirmationInitBranch',
            trigger: 'QUESTIONNAIRE:Before/VisitConfirmation/VC010',
            evaluate: true,
            resolve: [
                {
                    action (input, done) {
                        // Pre-fetch the lower boundary for the DateSpinner on VC050 because we can't use promises
                        // there.
                        PDE.CollectionModelUtils.getCollection('DiaryCompletionCollection')
                            .then((diaryCompletions) => {
                                diaryCompletions.comparator = diary => new Date(diary.get('dateTime'));
                                diaryCompletions.sort();
                                let pDiares = diaryCompletions.where({ diary: 'HHTrainingModule' }),
                                    bpDiaries = diaryCompletions.where({ diary: 'BPID' }),
                                    firstPracticeDiary,
                                    lastBPISF,
                                    pdDate,
                                    bpDate;

                                if (pDiares.length) {
                                    firstPracticeDiary = _.first(pDiares);
                                    pdDate = new Date(firstPracticeDiary.get('dateTime'));
                                }
                                if (bpDiaries.length) {
                                    lastBPISF = _.last(bpDiaries);
                                    bpDate = new Date(lastBPISF.get('dateTime'));
                                }
                                // Default to pdDate
                                PDE.VC050_minDate = pdDate;

                                if (pdDate && bpDate && PDE.DateTimeUtils.dateDiffInDays(bpDate, pdDate, false) > 0) {
                                    PDE.VC050_minDate = bpDate;
                                } else if (!pdDate && bpDate) {
                                    PDE.VC050_minDate = bpDate;
                                } else if (!pdDate && !bpDate) {
                                    PDE.VC050_minDate = new Date();
                                }
                                done();
                            });
                    }
                },
                {
                    action: () => {
                        const logger = new Logger('VisitConfirmationOpen'),
                            view = LF.router.view();
                        let targetScreen;

                        switch (view.subject.get('phase')) {
                            case 100: // SCREENING
                            case 200: // RE_SCREENING_1 to RE_SCREENING_10
                            case 210:
                            case 220:
                            case 230:
                            case 240:
                            case 250:
                            case 260:
                            case 270:
                            case 280:
                                return; // we are already on the correct screen!
                            case 290:
                                targetScreen = 'VC011';
                                break;
                            case 300: // TREATMENT
                                targetScreen = 'VC020';
                                break;
                            case 400: // FOLLOWUP_1
                                targetScreen = 'VC030';
                                break;
                            case 500: // FOLLOWUP_2
                                targetScreen = 'VC031';
                                break;
                            default:
                                logger.error('No case for initial branching has been hit!');
                        }
                        BranchHelpers.navigateToScreen(targetScreen);
                    }
                }
            ]
        },
        {
            id: 'VisitConfirmationClearInit',
            trigger: [
                'QUESTIONNAIRE:Displayed/VisitConfirmation/VC011',
                'QUESTIONNAIRE:Displayed/VisitConfirmation/VC020',
                'QUESTIONNAIRE:Displayed/VisitConfirmation/VC030',
                'QUESTIONNAIRE:Displayed/VisitConfirmation/VC031'
            ],
            evaluate: true,
            resolve: [{
                action: () => {
                    LF.router.view().screenStack.pop();
                }
            }]
        },
        {
            id: 'VisitConfirmationCompleted',
                trigger: 'QUESTIONNAIRE:Completed/VisitConfirmation',
            evaluate: () => !localStorage.getItem('ScreenshotMode'),
            salience: 900,
            resolve: [{
                action: (params, done) => {

                    LF.Utilities.getCollections(['VisitConfirmationCollection'])
                        .then((allCollections) => {

                        let qView = LF.router.view().questionnaire,
                            VST1L = qView.queryAnswersByIT('VST1L')[0],
                            phase = qView.subject.get('phase'),
                            visitConfirmationCollection = allCollections['VisitConfirmationCollection'],
                            visitConfirmationModel,
                            RNDMDT1D,
                            RESCRN1D,
                            STYDAY1N;

                        VST1L = VST1L? VST1L.get('response') : VST1L;

                        // if VST1L = Re-Screening, send RESCRN1D (re-screening date)
                        if (VST1L == '10') {
                            RESCRN1D = LF.Data.Questionnaire.data.dashboard.get('started');
                            if (RESCRN1D) {
                                LF.Data.Questionnaire.addIT({
                                    question_id: 'RESCRN1D',
                                    questionnaire_id: 'VisitConfirmation',
                                    response: PDE.QuestionnaireUtils.buildStudyWorksDateString(new Date(RESCRN1D)),
                                    IG: 'VisitConfirmation',
                                    IGR: 0,
                                    IT: 'RESCRN1D'
                                });
                            }
                        }

                        // if VST1L = Start Treatment
                        if (VST1L == '20') {
                            RNDMDT1D = qView.queryAnswersByIT('RNDMDT1D')[0];
                            RNDMDT1D = RNDMDT1D? RNDMDT1D.get('response') : RNDMDT1D;

                            // send STYDAY1D (study day 1 date) = RNDMDT1D
                            if (RNDMDT1D) {
                                LF.Data.Questionnaire.addIT({
                                    question_id: 'STYDAY1D',
                                    questionnaire_id: 'VisitConfirmation',
                                    response: PDE.QuestionnaireUtils.buildStudyWorksDateString(new Date(RNDMDT1D)),
                                    IG: 'VisitConfirmation',
                                    IGR: 0,
                                    IT: 'STYDAY1D'
                                });

                                STYDAY1N = calculateStudyDay(new Date(), new Date(RNDMDT1D));
                                if (STYDAY1N) {
                                    LF.Data.Questionnaire.addIT({
                                        question_id: 'STYDAY1N',
                                        questionnaire_id: 'VisitConfirmation',
                                        response: STYDAY1N,
                                        IG: 'VisitConfirmation',
                                        IGR: 0,
                                        IT: 'STYDAY1N'
                                    });
                                }
                            }
                        }

                        // if phase != Screening, send STYDAY1N (study day), using RNDMDT1D from visitConfirmationCollection
                        if (phase != LF.StudyDesign.studyPhase.SCREENING) {
                            if (visitConfirmationCollection && visitConfirmationCollection.models) {
                                visitConfirmationModel = _.find(visitConfirmationCollection.models, function(model) {
                                    return model.get('RNDMDT1D') != null;
                                });
                                if (visitConfirmationModel) {
                                    RNDMDT1D = visitConfirmationModel.get('RNDMDT1D');
                                    if (RNDMDT1D) {
                                        STYDAY1N = calculateStudyDay(new Date(), new Date(RNDMDT1D));
                                        if (STYDAY1N) {
                                            LF.Data.Questionnaire.addIT({
                                                question_id: 'STYDAY1N',
                                                questionnaire_id: 'VisitConfirmation',
                                                response: STYDAY1N,
                                                IG: 'VisitConfirmation',
                                                IGR: 0,
                                                IT: 'STYDAY1N'
                                            });
                                        }
                                    }
                                }
                            }
                        }

                        // spec id P832 - Send VST1L = Study Discontinuation, if screen VC031 is displayed
                        if (phase == LF.StudyDesign.studyPhase.FOLLOWUP_2) {
                            LF.Data.Questionnaire.addIT({
                                question_id: 'VST1L',
                                questionnaire_id: 'VisitConfirmation',
                                response: '60',
                                IG: 'VisitConfirmation',
                                IGR: 0,
                                IT: 'VST1L'
                            });
                        }

                        done();
                    });
                }
            }]
        },
        {
            id: 'VisitConfirmationChangePhase',
            trigger: 'QUESTIONNAIRE:Completed/VisitConfirmation',
            evaluate: () => !localStorage.getItem('ScreenshotMode'),
            salience: 100,
            resolve: [{
                action: 'changePhase',
                data (input, done) {
                    const Qview = LF.router.view().questionnaire,
                        [randAnswer] = Qview.queryAnswersByIT('RNDMDT1D'),
                        randdate = randAnswer ? randAnswer.get('response') : null,
                        subject = this.subject,
                        [answer] = LF.router.view().questionnaire.queryAnswersByIT('VST1L'),
                        visitReponse = answer.get('response'),
                        now = new Date(),
                        source = 'LogPadApp'; // to match from db

                    new VisitConfirmationModel({
                        VST1L: visitReponse,
                        RNDMDT1D: randdate,
                        source,
                        dateTime: buildStudyWorksDateTimeString(now, true)
                    })
                    .save()
                    .then(() => {
                        let visitPhase = new VisitToPhaseMap().get(visitReponse),
                            phaseToChange;
                        phaseToChange = visitPhase === LF.StudyDesign.studyPhase.RE_SCREENING_1 ?
                            updateReScreening(visitPhase, subject.get('phase')) :
                            visitPhase;

                        if (subject.get('phase') === LF.StudyDesign.studyPhase.FOLLOWUP_2) {
                            phaseToChange = LF.StudyDesign.studyPhase.PRE_TERMINATION;
                        }

                        done({
                            change: true,
                            phase: phaseToChange,
                            phaseStartDateTZOffset: LF.Utilities.timeStamp(new Date())
                        });
                    });
                }
            }]
        }
    ]
}
;
