export default {
    questionnaires: [
        {
            id: 'VisitConfirmation',
            SU: 'VisitConfirmation',
            displayName: 'DISPLAY_NAME',
            className: 'VisitConfirmation',
            previousScreen: false,
            affidavit: 'SignatureAffidavit',
            screens: [
                'VC010',
                'VC011',
                'VC020',
                'VC030',
                'VC031',
                'VC040',
                'VC050',
                'VC051',
                'VC060'
            ],
            branches: [
                {
                    id: 'B810',
                    branchFrom: 'VC010',
                    branchTo: 'VC040',
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'VC010_Q1',
                        value: '60'
                    }
                },
                {
                    id: 'B814',
                    branchFrom: 'VC010',
                    branchTo: 'VC060',
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'VC010_Q1',
                        value: '10'
                    }
                },
                {
                    id: 'B821',
                    branchFrom: 'VC010',
                    branchTo: 'VC050',
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'VC010_Q1',
                        value: '20'
                    }
                },
                {
                    id: 'B969',
                    branchFrom: 'VC011',
                    branchTo: 'VC040',
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'VC011_Q1',
                        value: '60'
                    }
                },
                {
                    id: 'B970',
                    branchFrom: 'VC011',
                    branchTo: 'VC050',
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'VC011_Q1',
                        value: '20'
                    }
                },
                {
                    id: 'B816',
                    branchFrom: 'VC020',
                    branchTo: 'VC040',
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'VC020_Q1',
                        value: '60'
                    }
                },
                {
                    id: 'B817',
                    branchFrom: 'VC020',
                    branchTo: 'VC060',
                    branchFunction: 'compare',
                    branchParams: {
                        questionId: 'VC020_Q1',
                        value: '60',
                        operand: '!='
                    }
                },
                {
                    id: 'B818',
                    branchFrom: 'VC030',
                    branchTo: 'VC040',
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'VC030_Q1',
                        value: '60'
                    }
                },
                {
                    id: 'B819',
                    branchFrom: 'VC030',
                    branchTo: 'VC060',
                    branchFunction: 'equal',
                    branchParams: {
                        questionId: 'VC030_Q1',
                        value: '50'
                    }
                },
                {
                    id: 'B833',
                    branchFrom: 'VC031',
                    branchTo: 'AFFIDAVIT',
                    branchFunction: 'always'
                },
                {
                    id: 'B820',
                    branchFrom: 'VC040',
                    branchTo: 'AFFIDAVIT',
                    branchFunction: 'always'
                },
                {
                    id: 'B1170',
                    branchFrom: 'VC050',
                    branchTo: 'VC060',
                    branchFunction: 'branchIfBeforeDay27',
                    branchParams: {
                        questionId: 'VC050_Q1'
                    }
                }
            ]
        }],
    screens: [
        {
            id: 'VC010',
            className: 'VC010',
            questions: [
                {
                    id: 'VC010_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'VC011',
            className: 'VC011',
            questions: [
                {
                    id: 'VC011_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'VC020',
            className: 'VC020',
            questions: [
                {
                    id: 'VC020_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'VC030',
            className: 'VC030',
            questions: [
                {
                    id: 'VC030_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'VC031',
            className: 'VC031',
            questions: [
                {
                    id: 'VC031_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'VC040',
            className: 'VC040',
            questions: [
                {
                    id: 'VC040_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'VC050',
            className: 'VC050',
            questions: [
                {
                    id: 'VC050_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'VC051',
            className: 'VC051',
            questions: [
                {
                    id: 'VC051_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'VC060',
            className: 'VC060',
            questions: [
                {
                    id: 'VC060_Q1',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'VC010_Q1',
            IG: 'VisitConfirmation',
            IT: 'VST1L',
            text: 'VC010_Q1_TEXT',
            title: '',
            className: 'VC010_Q1',
            widget: {
                id: 'VC010_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '10',
                        text: 'VC010_Q1_W0_A0'
                    },
                    {
                        value: '20',
                        text: 'VC010_Q1_W0_A1'
                    },
                    {
                        value: '60',
                        text: 'VC010_Q1_W0_A2'
                    }
                ]
            }
        }, {
            id: 'VC011_Q1',
            IG: 'VisitConfirmation',
            IT: 'VST1L',
            text: 'VC011_Q1_TEXT',
            title: '',
            className: 'VC011_Q1',
            widget: {
                id: 'VC011_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '20',
                        text: 'VC011_Q1_W0_A0'
                    },
                    {
                        value: '60',
                        text: 'VC011_Q1_W0_A1'
                    }
                ]
            }
        }, {
            id: 'VC020_Q1',
            IG: 'VisitConfirmation',
            IT: 'VST1L',
            text: 'VC020_Q1_TEXT',
            title: '',
            className: 'VC020_Q1',
            widget: {
                id: 'VC020_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '30',
                        text: 'VC020_Q1_W0_A0'
                    },
                    {
                        value: '40',
                        text: 'VC020_Q1_W0_A1'
                    },
                    {
                        value: '60',
                        text: 'VC020_Q1_W0_A2'
                    }
                ]
            }
        }, {
            id: 'VC030_Q1',
            IG: 'VisitConfirmation',
            IT: 'VST1L',
            text: 'VC030_Q1_TEXT',
            title: '',
            className: 'VC030_Q1',
            widget: {
                id: 'VC030_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '50',
                        text: 'VC030_Q1_W0_A0'
                    },
                    {
                        value: '60',
                        text: 'VC030_Q1_W0_A1'
                    }
                ]
            }
        }, {
            id: 'VC031_Q1',
            text: 'VC031_Q1_TEXT',
            title: '',
            className: 'VC031_Q1'
        }, {
            id: 'VC040_Q1',
            text: 'VC040_Q1_TEXT',
            title: '',
            className: 'VC040_Q1'
        }, {
            id: 'VC050_Q1',
            IG: 'VisitConfirmation',
            IT: 'RNDMDT1D',
            text: 'VC050_Q1_TEXT',
            title: '',
            className: 'VC050_Q1',
            widget: {
                id: 'VC050_Q1_W0',
                className: 'VC050_Q1_W0',
                type: 'DateSpinner',
                modalTitle: 'VC050_Q1_W0_modalTitle',
                okButtonText: 'VC050_Q1_W0_okButtonText',
                label: 'VC050_Q1_W0_label',
                dateFormat: 'DD-MMM-YYYY',
                displayFormat: 'DD-MMM-YYYY',
                storageFormat: 'DD-MMM-YYYY',
                minDate: () => {
                    if (!PDE.VC050_minDate) {
                        throw new Error('PDE.VC050_minDate not defined!');
                    }
                    return PDE.VC050_minDate;
                },
                maxDate: () => {
                    return new Date();
                }
            }
        }, {
            id: 'VC051_Q1',
            text: 'VC051_Q1_TEXT',
            title: '',
            className: 'VC051_Q1'
        }, {
            id: 'VC060_Q1',
            text: 'VC060_Q1_TEXT',
            title: '',
            className: 'VC060_Q1'
        }
    ]
};
