export default {
    rules: [
        {
            id: 'VisitConfirmationCompleted',
            trigger: 'QUESTIONNAIRE:Completed/VisitConfirmation',
            evaluate: () => !localStorage.getItem('ScreenshotMode'),
            resolve: [{
                action: (params) => {

                    const [randDT] = LF.router.view().questionnaire.queryAnswersByIT('RNDMDT1D'),
                        visit = LF.router.view().questionnaire.queryAnswersByIT('VST1L'),
                        phase = params.subject.get('phase');
                    let studyDay = null,
                        dayOne;

                    // visit is start treatment then STYDAY1D is RNDMDT1D
                    if (visit === '20' && randDT) {
                        dayOne = moment(randDT.get('response'));

                        LF.Data.Questionnaire.addIT({
                            question_id: 'STYDAY1D',
                            questionnaire_id: 'VisitConfirmation',
                            response: dayOne.toDate().ISOLocalTZStamp(),
                            IG: 'VisitConfirmation',
                            IGR: 0,
                            IT: 'STYDAY1D'
                        });
                    }

                    if (phase !== LF.StudyDesign.studyPhase.SCREENING) {
                        studyDay = moment(new Date()).add(1, 'days').diff(dayOne, 'days');

                        LF.Data.Questionnaire.addIT({
                            question_id: 'STYDAY1N',
                            questionnaire_id: 'VisitConfirmation',
                            response: studyDay,
                            IG: 'VisitConfirmation',
                            IGR: 0,
                            IT: 'STYDAY1N'
                        });
                    }

                    if (visit === LF.StudyDesign.studyPhase.RE_SCREENING) {
                        LF.Data.Questionnaire.addIT({
                            question_id: 'RESCRN1D',
                            questionnaire_id: 'VisitConfirmation',
                            response: new Date().ISOLocalTZStamp(),
                            IG: 'VisitConfirmation',
                            IGR: 0,
                            IT: 'RESCRN1D'
                        });
                    }
                }
            }]
        }
    ]
};
