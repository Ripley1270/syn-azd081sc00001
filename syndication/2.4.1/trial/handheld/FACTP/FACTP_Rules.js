export default {
    rules: [
        {
            id: 'FP023DisableNext',
            trigger: [
                'QUESTIONNAIRE:Displayed/FACTP/FP023',
                'QUESTIONNAIRE:Answered/FACTP/FP023_Q1',
                'QUESTIONNAIRE:Answered/FACTP/FP023_Q2'
            ],
            evaluate: true,
            resolve: [{
                action: (params) => {
                    const view = LF.router.view(),
                        radio = LF.Helpers.getWidget('FP023_Q1'),
                        checkBox = LF.Helpers.getWidget('FP023_Q2');
                    radio.answers.length || (checkBox.answer && checkBox.answer.get('response') === '1') ?
                        view.enableButton('#nextItem') :
                        view.disableButton('#nextItem');

                    if (params.question === 'FP023_Q1') {
                        checkBox.answers.first().set('response', '0');
                        checkBox.render();
                    }

                    if (params.question === 'FP023_Q2' && radio.answer) {
                        radio.answer.set('response', '');
                        radio.render();
                    }
                }
            }]
        }]
};
