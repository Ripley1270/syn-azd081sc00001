import { mergeObjects } from 'core/utilities/languageExtensions';

import './widget';

import FACTP from './FACTP';
import Rules from './FACTP_Rules';

let studyDesign = {}; // object
studyDesign = mergeObjects(studyDesign, FACTP, Rules);

export default { studyDesign };
