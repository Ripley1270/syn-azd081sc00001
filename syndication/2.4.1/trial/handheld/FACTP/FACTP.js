export default {
    questionnaires: [
        {
            id: 'FACTP',
            SU: 'FACTP',
            displayName: 'DISPLAY_NAME',
            className: 'questionnaire',
            previousScreen: false,
            affidavit: 'DEFAULT',
            screens: [
                'FP005',
                'FP010',
                'FP011',
                'FP012',
                'FP013',
                'FP014',
                'FP015',
                'FP016',
                'FP017',
                'FP018',
                'FP019',
                'FP020',
                'FP021',
                'FP022',
                'FP023',
                'FP040',
                'FP050',
                'FP060',
                'FP070',
                'FP080',
                'FP090',
                'FP100',
                'FP110',
                'FP120',
                'FP130',
                'FP140',
                'FP150',
                'FP160',
                'FP170',
                'FP180',
                'FP190',
                'FP200',
                'FP210',
                'FP220',
                'FP230',
                'FP240',
                'FP250',
                'FP260',
                'FP270',
                'FP280'
            ],
            branches: [],
            initialScreen: [],
            accessRoles: ['subject']
        }],
    screens: [
        {
            id: 'FP005',
            className: 'FP005',
            questions: [
                {
                    id: 'FP005_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP010',
            className: 'FP010',
            questions: [
                {
                    id: 'FP010_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP011',
            className: 'FP011',
            questions: [
                {
                    id: 'FP011_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP012',
            className: 'FP012',
            questions: [
                {
                    id: 'FP012_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP013',
            className: 'FP013',
            questions: [
                {
                    id: 'FP013_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP014',
            className: 'FP014',
            questions: [
                {
                    id: 'FP014_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP015',
            className: 'FP015',
            questions: [
                {
                    id: 'FP015_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP016',
            className: 'FP016',
            questions: [
                {
                    id: 'FP016_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP017',
            className: 'FP017',
            questions: [
                {
                    id: 'FP017_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP018',
            className: 'FP018',
            questions: [
                {
                    id: 'FP018_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP019',
            className: 'FP019',
            questions: [
                {
                    id: 'FP019_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP020',
            className: 'FP020',
            questions: [
                {
                    id: 'FP020_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP021',
            className: 'FP021',
            questions: [
                {
                    id: 'FP021_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP022',
            className: 'FP022',
            questions: [
                {
                    id: 'FP022_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP023',
            className: 'FP023',
            questions: [
                {
                    id: 'FP023_Q1',
                    mandatory: true
                },
                {
                    id: 'FP023_Q2',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP040',
            className: 'FP040',
            questions: [
                {
                    id: 'FP040_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP050',
            className: 'FP050',
            questions: [
                {
                    id: 'FP050_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP060',
            className: 'FP060',
            questions: [
                {
                    id: 'FP060_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP070',
            className: 'FP070',
            questions: [
                {
                    id: 'FP070_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP080',
            className: 'FP080',
            questions: [
                {
                    id: 'FP080_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP090',
            className: 'FP090',
            questions: [
                {
                    id: 'FP090_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP100',
            className: 'FP100',
            questions: [
                {
                    id: 'FP100_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP110',
            className: 'FP110',
            questions: [
                {
                    id: 'FP110_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP120',
            className: 'FP120',
            questions: [
                {
                    id: 'FP120_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP130',
            className: 'FP130',
            questions: [
                {
                    id: 'FP130_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP140',
            className: 'FP140',
            questions: [
                {
                    id: 'FP140_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP150',
            className: 'FP150',
            questions: [
                {
                    id: 'FP150_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP160',
            className: 'FP160',
            questions: [
                {
                    id: 'FP160_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP170',
            className: 'FP170',
            questions: [
                {
                    id: 'FP170_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP180',
            className: 'FP180',
            questions: [
                {
                    id: 'FP180_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP190',
            className: 'FP190',
            questions: [
                {
                    id: 'FP190_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP200',
            className: 'FP200',
            questions: [
                {
                    id: 'FP200_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP210',
            className: 'FP210',
            questions: [
                {
                    id: 'FP210_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP220',
            className: 'FP220',
            questions: [
                {
                    id: 'FP220_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP230',
            className: 'FP230',
            questions: [
                {
                    id: 'FP230_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP240',
            className: 'FP240',
            questions: [
                {
                    id: 'FP240_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP250',
            className: 'FP250',
            questions: [
                {
                    id: 'FP250_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP260',
            className: 'FP260',
            questions: [
                {
                    id: 'FP260_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP270',
            className: 'FP270',
            questions: [
                {
                    id: 'FP270_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'FP280',
            className: 'FP280',
            questions: [
                {
                    id: 'FP280_Q1',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'FP005_Q1',
            text: 'FP005_Q1_TEXT',
            title: '',
            className: 'FP005_Q1'
        }, {
            id: 'FP010_Q1',
            IG: 'FACTP',
            IT: 'FCTLOE1L',
            text: 'FP010_Q1_TEXT',
            title: 'FP010_Q1_TITLE',
            className: 'FP010_Q1',
            widget: {
                id: 'FP010_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP010_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP010_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP010_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP010_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP010_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP011_Q1',
            IG: 'FACTP',
            IT: 'FCTHNS1L',
            text: 'FP011_Q1_TEXT',
            title: 'FP011_Q1_TITLE',
            className: 'FP011_Q1',
            widget: {
                id: 'FP011_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP011_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP011_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP011_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP011_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP011_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP012_Q1',
            IG: 'FACTP',
            IT: 'FCTTMN1L',
            text: 'FP012_Q1_TEXT',
            title: 'FP012_Q1_TITLE',
            className: 'FP012_Q1',
            widget: {
                id: 'FP012_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP012_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP012_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP012_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP012_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP012_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP013_Q1',
            IG: 'FACTP',
            IT: 'FCTHPN1L',
            text: 'FP013_Q1_TEXT',
            title: 'FP013_Q1_TITLE',
            className: 'FP013_Q1',
            widget: {
                id: 'FP013_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP013_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP013_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP013_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP013_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP013_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP014_Q1',
            IG: 'FACTP',
            IT: 'FCTBSE1L',
            text: 'FP014_Q1_TEXT',
            title: 'FP014_Q1_TITLE',
            className: 'FP014_Q1',
            widget: {
                id: 'FP014_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP014_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP014_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP014_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP014_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP014_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP015_Q1',
            IG: 'FACTP',
            IT: 'FCTFIL1L',
            text: 'FP015_Q1_TEXT',
            title: 'FP015_Q1_TITLE',
            className: 'FP015_Q1',
            widget: {
                id: 'FP015_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP015_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP015_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP015_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP015_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP015_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP016_Q1',
            IG: 'FACTP',
            IT: 'FCTFST1L',
            text: 'FP016_Q1_TEXT',
            title: 'FP016_Q1_TITLE',
            className: 'FP016_Q1',
            widget: {
                id: 'FP016_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP016_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP016_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP016_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP016_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP016_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP017_Q1',
            IG: 'FACTP',
            IT: 'FCTFCF1L',
            text: 'FP017_Q1_TEXT',
            title: 'FP017_Q1_TITLE',
            className: 'FP017_Q1',
            widget: {
                id: 'FP017_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP017_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP017_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP017_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP017_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP017_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP018_Q1',
            IG: 'FACTP',
            IT: 'FCTESF1L',
            text: 'FP018_Q1_TEXT',
            title: 'FP018_Q1_TITLE',
            className: 'FP018_Q1',
            widget: {
                id: 'FP018_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP018_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP018_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP018_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP018_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP018_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP019_Q1',
            IG: 'FACTP',
            IT: 'FCTSBF1L',
            text: 'FP019_Q1_TEXT',
            title: 'FP019_Q1_TITLE',
            className: 'FP019_Q1',
            widget: {
                id: 'FP019_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP019_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP019_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP019_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP019_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP019_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP020_Q1',
            IG: 'FACTP',
            IT: 'FCTFAS1L',
            text: 'FP020_Q1_TEXT',
            title: 'FP020_Q1_TITLE',
            className: 'FP020_Q1',
            widget: {
                id: 'FP020_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP020_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP020_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP020_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP020_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP020_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP021_Q1',
            IG: 'FACTP',
            IT: 'FCTSFC1L',
            text: 'FP021_Q1_TEXT',
            title: 'FP021_Q1_TITLE',
            className: 'FP021_Q1',
            widget: {
                id: 'FP021_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP021_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP021_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP021_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP021_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP021_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP022_Q1',
            IG: 'FACTP',
            IT: 'FCTFCP1L',
            text: 'FP022_Q1_TEXT',
            title: 'FP022_Q1_TITLE',
            className: 'FP022_Q1',
            widget: {
                id: 'FP022_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP022_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP022_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP022_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP022_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP022_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP023_Q1',
            IG: 'FACTP',
            IT: 'FCTSSL1L',
            text: 'FP023_Q1_TEXT',
            title: 'FP023_Q1_TITLE',
            className: 'FP023_Q1',
            widget: {
                id: 'FP023_Q1_W0',
                type: 'RadioButtonNoVal',
                answers: [
                    {
                        value: '0',
                        text: 'FP023_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP023_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP023_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP023_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP023_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP023_Q2',
            IG: 'FACTP',
            IT: 'FCTSSL1B',
            text: 'FP023_Q2_TEXT',
            title: '',
            className: 'FP023_Q2',
            widget: {
                id: 'FP023_Q2_W0',
                type: 'CheckBoxNoVal',
                answers: [
                    {
                        IT: 'FCTSSL1B',
                        text: 'FP023_Q2_W0_A0',
                        value: '1'
                    }
                ]
            }
        }, {
            id: 'FP040_Q1',
            IG: 'FACTP',
            IT: 'FCTSAD1L',
            text: 'FP040_Q1_TEXT',
            title: 'FP040_Q1_TITLE',
            className: 'FP040_Q1',
            widget: {
                id: 'FP040_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP040_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP040_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP040_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP040_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP040_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP050_Q1',
            IG: 'FACTP',
            IT: 'FCTCWI1L',
            text: 'FP050_Q1_TEXT',
            title: 'FP050_Q1_TITLE',
            className: 'FP050_Q1',
            widget: {
                id: 'FP050_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP050_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP050_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP050_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP050_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP050_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP060_Q1',
            IG: 'FACTP',
            IT: 'FCTLHP1L',
            text: 'FP060_Q1_TEXT',
            title: 'FP060_Q1_TITLE',
            className: 'FP060_Q1',
            widget: {
                id: 'FP060_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP060_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP060_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP060_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP060_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP060_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP070_Q1',
            IG: 'FACTP',
            IT: 'FCTFNR1L',
            text: 'FP070_Q1_TEXT',
            title: 'FP070_Q1_TITLE',
            className: 'FP070_Q1',
            widget: {
                id: 'FP070_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP070_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP070_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP070_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP070_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP070_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP080_Q1',
            IG: 'FACTP',
            IT: 'FCTWAD1L',
            text: 'FP080_Q1_TEXT',
            title: 'FP080_Q1_TITLE',
            className: 'FP080_Q1',
            widget: {
                id: 'FP080_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP080_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP080_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP080_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP080_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP080_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP090_Q1',
            IG: 'FACTP',
            IT: 'FCTWCW1L',
            text: 'FP090_Q1_TEXT',
            title: 'FP090_Q1_TITLE',
            className: 'FP090_Q1',
            widget: {
                id: 'FP090_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP090_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP090_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP090_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP090_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP090_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP100_Q1',
            IG: 'FACTP',
            IT: 'FCTATW1L',
            text: 'FP100_Q1_TEXT',
            title: 'FP100_Q1_TITLE',
            className: 'FP100_Q1',
            widget: {
                id: 'FP100_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP100_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP100_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP100_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP100_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP100_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP110_Q1',
            IG: 'FACTP',
            IT: 'FCTWFF1L',
            text: 'FP110_Q1_TEXT',
            title: 'FP110_Q1_TITLE',
            className: 'FP110_Q1',
            widget: {
                id: 'FP110_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP110_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP110_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP110_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP110_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP110_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP120_Q1',
            IG: 'FACTP',
            IT: 'FCTAEL1L',
            text: 'FP120_Q1_TEXT',
            title: 'FP120_Q1_TITLE',
            className: 'FP120_Q1',
            widget: {
                id: 'FP120_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP120_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP120_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP120_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP120_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP120_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP130_Q1',
            IG: 'FACTP',
            IT: 'FCTHAI1L',
            text: 'FP130_Q1_TEXT',
            title: 'FP130_Q1_TITLE',
            className: 'FP130_Q1',
            widget: {
                id: 'FP130_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP130_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP130_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP130_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP130_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP130_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP140_Q1',
            IG: 'FACTP',
            IT: 'FCTSWL1L',
            text: 'FP140_Q1_TEXT',
            title: 'FP140_Q1_TITLE',
            className: 'FP140_Q1',
            widget: {
                id: 'FP140_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP140_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP140_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP140_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP140_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP140_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP150_Q1',
            IG: 'FACTP',
            IT: 'FCTETD1L',
            text: 'FP150_Q1_TEXT',
            title: 'FP150_Q1_TITLE',
            className: 'FP150_Q1',
            widget: {
                id: 'FP150_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP150_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP150_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP150_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP150_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP150_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP160_Q1',
            IG: 'FACTP',
            IT: 'FCTCQL1L',
            text: 'FP160_Q1_TEXT',
            title: 'FP160_Q1_TITLE',
            className: 'FP160_Q1',
            widget: {
                id: 'FP160_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP160_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP160_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP160_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP160_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP160_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP170_Q1',
            IG: 'FACTP',
            IT: 'FCTLWG1L',
            text: 'FP170_Q1_TEXT',
            title: 'FP170_Q1_TITLE',
            className: 'FP170_Q1',
            widget: {
                id: 'FP170_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP170_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP170_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP170_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP170_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP170_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP180_Q1',
            IG: 'FACTP',
            IT: 'FCTHGA1L',
            text: 'FP180_Q1_TEXT',
            title: 'FP180_Q1_TITLE',
            className: 'FP180_Q1',
            widget: {
                id: 'FP180_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP180_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP180_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP180_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP180_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP180_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP190_Q1',
            IG: 'FACTP',
            IT: 'FCTHAP1L',
            text: 'FP190_Q1_TEXT',
            title: 'FP190_Q1_TITLE',
            className: 'FP190_Q1',
            widget: {
                id: 'FP190_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP190_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP190_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP190_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP190_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP190_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP200_Q1',
            IG: 'FACTP',
            IT: 'FCTBEP1L',
            text: 'FP200_Q1_TEXT',
            title: 'FP200_Q1_TITLE',
            className: 'FP200_Q1',
            widget: {
                id: 'FP200_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP200_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP200_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP200_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP200_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP200_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP210_Q1',
            IG: 'FACTP',
            IT: 'FCTPKT1L',
            text: 'FP210_Q1_TEXT',
            title: 'FP210_Q1_TITLE',
            className: 'FP210_Q1',
            widget: {
                id: 'FP210_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP210_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP210_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP210_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP210_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP210_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP220_Q1',
            IG: 'FACTP',
            IT: 'FCTSCL1L',
            text: 'FP220_Q1_TEXT',
            title: 'FP220_Q1_TITLE',
            className: 'FP220_Q1',
            widget: {
                id: 'FP220_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP220_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP220_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP220_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP220_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP220_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP230_Q1',
            IG: 'FACTP',
            IT: 'FCTFLM1L',
            text: 'FP230_Q1_TEXT',
            title: 'FP230_Q1_TITLE',
            className: 'FP230_Q1',
            widget: {
                id: 'FP230_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP230_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP230_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP230_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP230_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP230_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP240_Q1',
            IG: 'FACTP',
            IT: 'FCTTMB1L',
            text: 'FP240_Q1_TEXT',
            title: 'FP240_Q1_TITLE',
            className: 'FP240_Q1',
            widget: {
                id: 'FP240_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP240_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP240_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP240_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP240_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP240_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP250_Q1',
            IG: 'FACTP',
            IT: 'FCTDUR1L',
            text: 'FP250_Q1_TEXT',
            title: 'FP250_Q1_TITLE',
            className: 'FP250_Q1',
            widget: {
                id: 'FP250_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP250_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP250_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP250_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP250_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP250_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP260_Q1',
            IG: 'FACTP',
            IT: 'FCTUMF1L',
            text: 'FP260_Q1_TEXT',
            title: 'FP260_Q1_TITLE',
            className: 'FP260_Q1',
            widget: {
                id: 'FP260_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP260_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP260_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP260_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP260_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP260_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP270_Q1',
            IG: 'FACTP',
            IT: 'FCTULA1L',
            text: 'FP270_Q1_TEXT',
            title: 'FP270_Q1_TITLE',
            className: 'FP270_Q1',
            widget: {
                id: 'FP270_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP270_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP270_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP270_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP270_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP270_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'FP280_Q1',
            IG: 'FACTP',
            IT: 'FCTAHE1L',
            text: 'FP280_Q1_TEXT',
            title: 'FP280_Q1_TITLE',
            className: 'FP280_Q1',
            widget: {
                id: 'FP280_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'FP280_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'FP280_Q1_W0_A1'
                    },
                    {
                        value: '2',
                        text: 'FP280_Q1_W0_A2'
                    },
                    {
                        value: '3',
                        text: 'FP280_Q1_W0_A3'
                    },
                    {
                        value: '4',
                        text: 'FP280_Q1_W0_A4'
                    }
                ]
            }
        }
    ]
};
