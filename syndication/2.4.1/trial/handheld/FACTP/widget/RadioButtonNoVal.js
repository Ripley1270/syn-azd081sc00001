import CustomRadioButton from 'core/widgets/CustomRadioButton';

/**
 * RadioButtonNoVal is identical to the CustomRadioButton except that is does that it is always valid (i.e. doesn't not validate)
 * Validation should be done within the rules class instead.
 */
class RadioButtonNoVal extends CustomRadioButton {

    validate () {
        this.completed = true;
        return true;
    }
}

window.LF.Widget.RadioButtonNoVal = RadioButtonNoVal;
