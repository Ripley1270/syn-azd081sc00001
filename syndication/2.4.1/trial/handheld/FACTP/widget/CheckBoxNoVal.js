import CheckBox from 'core/widgets/CheckBox';

/**
 * CheckBoxNoVal is identical to the CheckBox except that it is always valid (i.e. doesn't not validate)
 * Validation should be done within the rules class instead.
 */
class CheckBoxNoVal extends CheckBox {
    validate () {
        this.completed = true;
        return true;
    }
}

window.LF.Widget.CheckBoxNoVal = CheckBoxNoVal;
