export default {
    questionnaires: [
        {
            id: 'SubjectTrainingQuiz',
            SU: 'STQ',
            displayName: 'DISPLAY_NAME',
            className: 'questionnaire',
            previousScreen: false,
            affidavit: 'DEFAULT',
            screens: [
                'SubjectTrainingQuiz_STQ010'
            ],
            branches: [],
            initialScreen: [],
            accessRoles: [
                'subject',
                'site'
            ]
        }],
    screens: [
        {
            id: 'SubjectTrainingQuiz_STQ010',
            className: 'SubjectTrainingQuiz_STQ010',
            questions: [
                {
                    id: 'SubjectTrainingQuiz_STQ010_Q1',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'SubjectTrainingQuiz_STQ010_Q1',
            IG: 'SubjectTrainingQuiz',
            IT: 'VIDCMP1B',
            text: 'SubjectTrainingQuiz_STQ010_Q1_TEXT',
            title: '',
            className: 'SubjectTrainingQuiz_STQ010_Q1',
            widget: {
                id: 'SubjectTrainingQuiz_STQ010_Q1_TEXT_W0',
                type: 'CheckBox',
                answers: [
                    {
                        IT: 'VIDCMP1B',
                        text: 'SubjectTrainingQuiz_STQ010_Q1_TEXT_W0_A0',
                        value: '1'
                    }
                ]
            }
        }
    ]
};
