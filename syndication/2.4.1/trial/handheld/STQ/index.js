import { mergeObjects } from 'core/utilities/languageExtensions';

import SubjectTrainingQuiz from './SubjectTrainingQuiz';

let studyDesign = {}; // object
studyDesign = mergeObjects(studyDesign, SubjectTrainingQuiz);

export default { studyDesign };
