
PDE.medModule.MedLoopReviewItems = (widget) => {
    return PDE.LoopingUtils.loadLoopScreen(
        PDE.medModule.MedLoopInfo.IG,
        PDE.medModule.MedLoopInfo.ReviewScreen,
        PDE.medModule.MedTakenItems,
        widget
    );
};

PDE.medModule.MedTakenItems = function (IG, widget) {
    let result = [],
        medFormat = LF.Utilities.getString('MEDLOOP_0100_MED_FORMAT'),
        timeFormat = LF.Utilities.getString('MEDLOOP_0100_TIME_FORMAT'),
        medTakenModels = PDE.medModule.getMedTakenReviewData(IG, true),
        //Format the text for a med taken
        formatMedItem = (medTakenModel) => {
            let medCode = medTakenModel.get('MEDCOD1C'),
                medTime = moment(medTakenModel.get('MEDTKN1S'), PDE.MomentUtils.getSWDateTimeFormat()),
                medAmt = medTakenModel.get('MEDAMT1N'),
                SBJALS1C = PDE.medModule.getSubjectMedAlias(medCode),
                result;

            if(!SBJALS1C){
                medCode = PDE.medModule.medModuleConfig.MED_CODE_OTHER;    
            }

            result = PDE.medModule.getMedDisplayText(medCode, 
                                                    medFormat, 
                                                    {'SBJALS1C': SBJALS1C,
                                                    'MEDAMT1N': medAmt,
                                                    'MEDTKN1S': PDE.MomentUtils.formatDateTime(medTime)});

            return result;
        },
        formatMedTime = (medTakenModel) => {
            let medTime = moment(medTakenModel.get('MEDTKN1S'), PDE.MomentUtils.getSWDateTimeFormat());

            return timeFormat.replace('{MEDTKN1S}', PDE.MomentUtils.formatDateTime(medTime));
        };

    if(PDE.DeviceStatusUtils.isScreenshotMode()){
        result.push({
            'classes' : 'not-selectable',
            'id'      : '-1',
            'text'    : [LF.Utilities.getString('NO_MEDS'), '']
        });    
    }

     //If no items, display message
     if(medTakenModels.length === 0){
        result.push({
            'classes' : 'not-selectable',
            'id'      : '-1',
            'text'    : [LF.Utilities.getString('NO_MEDS'), '']
        });
     }else{
        _.each(medTakenModels, (medTakenModel) => {
            let igr = medTakenModel.get('igr'),
                item  = {classes : ''};

            //If no igr, push to negative number so that the item cannot be picked in the
            // review screen
            if(!igr){
                igr = -1;
                item.id = '-1';
                item.classes += ' not-selectable';
            }
            else {
                item.id = igr;
            }

            item.text = [formatMedItem(medTakenModel), formatMedTime(medTakenModel)];

            result.push(item);
        });
     }

     return Q.resolve(result);
};

PDE.medModule.getMedTakenReviewData = function (IG, includeHistorialData) {
    let cache = PDE.medModule.getMedModuleCache(),
        medTakenModels = cache.medTakenModels,
        maxIGR = PDE.LoopingUtils.getMaxIGR(IG),
        filter = PDE.medModule.MedLoopInfo.ReviewDataFilter,
        result = [],
        medTakenModel,
        medAmt,
        medTime,
        medCode,
        subjectAlias,
        i;

    //Add meds taken in the current diary
    for (i = 1; i < maxIGR; i++) {
        medCode = PDE.medModule.getMedCode(i);
        medTime = PDE.medModule.getMedTime(i);
        medAmt = PDE.medModule.getMedAmount(i);
        subjectAlias = PDE.medModule.getSubjectMedAlias(medCode);

        medTakenModel = new LF.Model.MedTakenRecord({
            MEDCOD1C: medCode,
            MEDTKN1S: PDE.QuestionnaireUtils.buildStudyWorksDateTimeString(medTime),
            MEDAMT1N: medAmt,
            SBJALS1C: subjectAlias,
            igr: i
        });

        result.push(medTakenModel);
    }

    //Add meds taken from historical data
    if(includeHistorialData){
        result = result.concat(medTakenModels);
    }

    if(filter){
        filter = PDE.commonUtils.getPath(filter);

        if(_.isFunction(filter)){
            result = filter(result);
        }
        else {
            throw new Error('ReviewDataFilter is not a function.');
        }
    }

    //Sort result in descending order base on date time
    result = _.sortBy(result, (medTaken) => {
        let medTime = medTaken.get('MEDTKN1S');

        return -(new Date(medTime).getTime());
    });

    return result;
};

PDE.medModule.reviewDataFilter = (reviewItems) => {
    let result,
        minTime = PDE.QuestionnaireUtils.getReportStartDateTime();

    //Set min time to 24 hours before report start time
    minTime = minTime.setDate(minTime.getDate() - 1);

    result = _.filter(reviewItems, (reviewItem) => {
        let medID = reviewItem.get('MED_ID1N'),
            medTime = new Date(reviewItem.get('MEDTKN1S')).getTime(),
            isValid = false;

        //If med ID exist, this is historical data
        if(medID){
            //If med time is after the min time
            if(medTime >= minTime){
                isValid = true;
            }
        }
        //Else item is entered from the current questionnaire
        else {
            isValid = true;
        }

        return isValid;
    });

    return result;
};

PDE.medModule.addNewMed = function (params) {
    let answers = PDE.QuestionnaireUtils.getQuestionnaireView().queryAnswersByIGAndIGR(PDE.medModule.MedLoopInfo.IG, PDE.medModule.MedLoopInfo.Max_Loops);
    if(answers.length > 0){
        PDE.QuestionnaireUtils.navigateToScreen(PDE.medModule.MedLoopInfo.MaxLoopWarningScreen);
    } else{
        PDE.QuestionnaireUtils.navigateToScreen(PDE.medModule.MedLoopInfo.StartOfLoop);
    }
};

PDE.medModule.editMed = function (params) {
    let IGR = LF.Utilities.getReviewScreenItemID(params.item);

    PDE.LoopingUtils.beginEditLoop(PDE.medModule.MedLoopInfo.IG, PDE.medModule.MedLoopInfo.StartOfLoop, IGR);
};

PDE.medModule.deleteMed = function (params) {
    let IGR = LF.Utilities.getReviewScreenItemID(params.item);

    PDE.LoopingUtils.beginEditLoop(PDE.medModule.MedLoopInfo.IG, PDE.medModule.MedLoopInfo.DeleteScreen, IGR);
};

PDE.medModule.editNotAvailable = function (widget) {
    //Return the opposite of checkIfRowIsEditable
    return Q().then(() => {
        if(widget.activeItem){
            let IGR = LF.Utilities.getReviewScreenItemID(widget.activeItem);
            return IGR <= 0;
        }else{
            return true;
        }
    });
};

PDE.medModule.checkIfRowIsEditable = function (widget) {
    return Q().then(() => {
            if(widget.activeItem){
                let IGR = LF.Utilities.getReviewScreenItemID(widget.activeItem);
                return IGR > 0;
            }else{
                return false;
            }
        }
    );
};

PDE.medModule.checkIfRowIsSelected = function (widget) {
    return Q().then(() => {
            if(widget.activeItem){
                let IGR = LF.Utilities.getReviewScreenItemID(widget.activeItem);
                if(IGR > 0){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    );
};