import PDE from 'PDE_Core/common/PDE';

PDE.medModule = {
    medModuleConfig: {
        // Default Effective date use to determine master med list is out of date
        MASTER_MED_DEFAULT_EFFECTIVE_DATE: '2017-01-01T00:00:00.000Z',
        // Med Code for "Other"
        MED_CODE_OTHER: '99999999',
        // IDs of questionnaires that contain a Med Loop, it is used to trigger the Med Loop related
        // ELF rules for the questionnaires
        QUESTIONNAIRE_IDS: ['DiaryMed'],
        // Default Master Med List XML Path
        // This should match the file listed in app\logpad\script.ejs
        DEFAULT_MASTER_MED_LIST_PATH: 'trial/handheld/medModule/masterMedList.xml',
        // Master Med properties that needs to be URL encoded when it is saved to collections/models from XML
        MASTER_MED_URL_ENCODE_PROPERTIES: ['MEDNAM1C', 'MEDDOS2C', 'MEDUNT1L', 'MEDLBL1C', 'MEDLBL2C', 'MEDLBL3C',
            'MEDLBL4C', 'MEDLBL5C', 'MEDLBL6C', 'MEDLBL7C', 'MEDLBL8C', 'MEDLBL9C'],
        // Default function to load the master med list for active subject (integrated to StudyWorks)
        // This is intended to seed the master med list on a device and minimize the
        // need to download the complete master med list from StudyWorks.
        // This can be comment out if not used
        // DEFAULT_MASTER_MED_LOADER_ACTIVE: 'PDE.medModule.masterMedListLoaderByXml',
        // Default function to load the master med list for trainers
        DEFAULT_MASTER_MED_LOADER_TRAINER: 'PDE.medModule.masterMedListLoaderByXml',
        MED_FORM_LIST: [
            // RESOURCE ID: the resource ID in study JSON for the med form
            // VALUE: Codelist value that is sent to StudyWorks
            // MAX AMT: The max amount the subject is allow to take/enter in DD670
            {
                RESOURCE_ID: 'MEDFRM1L1',
                VALUE: '1'
            },
            {
                RESOURCE_ID: 'MEDFRM1L2',
                VALUE: '2'
            },
            {
                RESOURCE_ID: 'MEDFRM1L3',
                VALUE: '3'
            },
            {
                RESOURCE_ID: 'MEDFRM1L4',
                VALUE: '4'
            },
            {
                RESOURCE_ID: 'MEDFRM1L5',
                VALUE: '5'
            },
            {
                RESOURCE_ID: 'MEDFRM1L6',
                VALUE: '6'
            },
            {
                RESOURCE_ID: 'MEDFRM1L7',
                VALUE: '7'
            },
            {
                RESOURCE_ID: 'MEDFRM1L8',
                VALUE: '8'
            },
            {
                RESOURCE_ID: 'MEDFRM1L9',
                VALUE: '9'
            },
            {
                RESOURCE_ID: 'MEDFRM1L10',
                VALUE: '10'
            },
            {
                RESOURCE_ID: 'MEDFRM1L11',
                VALUE: '11'
            }
        ],
        // MEDFRM2L Codelist
        MED_FORM2_LIST: [
            // VALUE: Codelist value that is sent to StudyWorks
            // MAX AMT: The max amount the subject is allow to take/enter in MED1100
            {
                VALUE: '1',
                MAX_AMT: 9
            },
            {
                VALUE: '2',
                MAX_AMT: 9
            },
            {
                VALUE: '3',
                MAX_AMT: 99
            },
            {
                VALUE: '4',
                MAX_AMT: 99
            },
            {
                VALUE: '5',
                MAX_AMT: 99
            },
            {
                VALUE: '6',
                MAX_AMT: 9
            },
            {
                VALUE: '7',
                MAX_AMT: 9
            },
            {
                VALUE: '8',
                MAX_AMT: 9
            },
            {
                VALUE: '9',
                MAX_AMT: 9
            },
            {
                VALUE: '10',
                MAX_AMT: 9
            },
            {
                VALUE: '11',
                MAX_AMT: 9
            },
            {
                VALUE: '12',
                MAX_AMT: 9
            },
            {
                VALUE: '13',
                MAX_AMT: 9
            },
            {
                VALUE: '14',
                MAX_AMT: 9
            },
            {
                VALUE: '15',
                MAX_AMT: 9
            }
        ],
        // Function to display the master med attribute base on value
        MED_ATTRIBUTE_TEXT_FUNCTION:
            {
                MEDFRM1L: 'PDE.medModule.getMEDFRM1LText',
                MEDFRM2L: 'PDE.medModule.getMEDFRM2LText'
            },
        TRAINER_SUBJECT_MED: [
            {
                MEDCOD1C: '01050105',
                SBJALS1C: 'Dark liquid'
            },
            {
                MEDCOD1C: '01050115',
                SBJALS1C: 'White med'
            },
            {
                MEDCOD1C: '01050120',
                SBJALS1C: 'Blue pills'
            },
            {
                MEDCOD1C: '01050125',
                SBJALS1C: 'Small pills in the morning'
            },
            {
                MEDCOD1C: '01050130',
                SBJALS1C: 'White and red'
            },
            {
                MEDCOD1C: '01050205',
                SBJALS1C: 'Brown pills'
            },
            {
                MEDCOD1C: '01050805',
                SBJALS1C: 'Inhaler in the evening'
            },
            {
                MEDCOD1C: '01051405',
                SBJALS1C: 'Green plastic spray'
            },
            {
                MEDCOD1C: '01051410',
                SBJALS1C: 'White powder'
            }
        ]
    },
    /*
     * Med Loop Information
     */
    MedLoopInfo: {
        // IG of Med Loop
        IG: 'DiaryMedRepeating',
        // Max number of loops
        Max_Loops: 10,
        ReviewScreen: 'MEDLOOP_0100',
        StartOfLoop: 'MEDLOOP_0700',
        EndOfLoop: 'MEDLOOP_1400',
        DeleteScreen: 'MEDLOOP_1400',
        MaxLoopWarningScreen: 'MEDLOOP_1300',
        Collection: 'MedTaken',
        Model: 'MedTakenRecord',
        ReviewDataFilter: 'PDE.medModule.reviewDataFilter'
    }
};

export default PDE.medModule;
