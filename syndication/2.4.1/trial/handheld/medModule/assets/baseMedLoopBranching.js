LF.Branching.branchFunctions.isOtherMed = (branchObj, view) => {
    let selectedMedCode = PDE.medModule.getMedCode(),
        result = false;

    if(selectedMedCode === PDE.medModule.medModuleConfig.MED_CODE_OTHER){
        result = true;
    }

    return Q.resolve(result);
};

LF.Branching.branchFunctions.isConflictedMed = (branchObj, view) => {
    let conflictedMed = PDE.medModule.getConflictedMed(),
        result = false;

    if(conflictedMed){
        result = true;
    }

    return Q.resolve(result);
};

LF.Branching.branchFunctions.MEDLOOP_0900 = (branchObj, view) => {
    let MEDLOOP_0900 = PDE.AnswerUtils.getAnswerResponseByQuestionID('MEDLOOP_0900_Q');

    //If MEDLOOP_0900 = Change the date/time for this dose
    if(MEDLOOP_0900 === '1'){
        branchObj.branchTo = 'MEDLOOP_0800';
        return LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view);
    }
    //If MEDLOOP_0900 = Delete this dose, it’s a duplicate
    else if(MEDLOOP_0900 === '2'){
        branchObj.branchTo = 'MEDLOOP_0910';
    }
    //If MEDLOOP_0900 = Keep this dose, it’s correct
    else if(MEDLOOP_0900 === '3'){
        branchObj.branchTo = 'MEDLOOP_1100';
    }

    return Q.resolve(true);
};

LF.Branching.branchFunctions.MEDLOOP_0910 = (branchObj, view) => {
    let MEDLOOP_0910 = PDE.AnswerUtils.getAnswerResponseByQuestionID('MEDLOOP_0910_Q'),
        screenStack = PDE.QuestionnaireUtils.getScreenStack(),
        IG = PDE.medModule.MedLoopInfo.IG;

    //If MEDLOOP_0910 = No, go back to the previous screen
    if(MEDLOOP_0910 === '0'){
        //Go back to the previous screen
        screenStack.pop();

        //Just pop to the previous screen
        branchObj.branchTo = screenStack.pop();

        return Q.resolve(true);
    }
    //If MEDLOOP_0910 = Yes, complete the deletion
    else if(MEDLOOP_0910 === '1'){
        //Delete Loop
        PDE.LoopingUtils.deleteLoop(IG);
     
        branchObj.branchTo = PDE.medModule.MedLoopInfo.ReviewScreen;

        return LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view);
    }
};

LF.Branching.branchFunctions.MEDLOOP_1200 = (branchObj, view) => {
    let MEDLOOP_1200 = PDE.AnswerUtils.getAnswerResponseByQuestionID('MEDLOOP_1200_Q'),
        selectedMedCode = PDE.medModule.getMedCode(),
        screenStack = PDE.QuestionnaireUtils.getScreenStack(),
        questionnaireView = PDE.QuestionnaireUtils.getQuestionnaireView(),
        wasEditing = questionnaireView.editing,
        IG = PDE.medModule.MedLoopInfo.IG,
        maxLoop = PDE.medModule.MedLoopInfo.Max_Loops,
        maxIGR = PDE.LoopingUtils.getMaxIGR(IG);

    //Clear MEDLOOP_1200 response. MEDLOOP_1200 is NOT sent to SW
    PDE.AnswerUtils.removeAnswerModels({questionID: 'MEDLOOP_1200_Q'}, true);

    //Save loop
    PDE.LoopingUtils.saveLoop(IG);

    if(maxLoop <= maxIGR && (MEDLOOP_1200 === '1' || MEDLOOP_1200 === '2')){
        branchObj.branchTo = PDE.medModule.MedLoopInfo.MaxLoopWarningScreen;

        //Pop all screens between the review screen and MaxLoopWarningScreen
        while(screenStack[screenStack.length - 1] !== PDE.medModule.MedLoopInfo.ReviewScreen){
            screenStack.pop();
        }

        return Q.resolve(true);
    }
    //If MEDLOOP_1200 = Enter a new dose for: 
    else if(MEDLOOP_1200 === '1'){
        //Med code for the next loop
        PDE.AnswerUtils.addAnswerResponseByQuesID('MEDLOOP_0700_Q', IG, 'MEDCOD1C', selectedMedCode, maxIGR + (wasEditing ? 0: 1));

        branchObj.branchTo = 'MEDLOOP_0800';
    }
    //If MEDLOOP_1200 = Select a new medication
    else if(MEDLOOP_1200 === '2'){
        branchObj.branchTo = 'MEDLOOP_0700';        
    }
    //If MEDLOOP_1200 = Return to the review screen
    else if(MEDLOOP_1200 === '3'){
        branchObj.branchTo = PDE.medModule.MedLoopInfo.ReviewScreen;
    }

    //branch back
    return LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view);
};

LF.Branching.branchFunctions.MEDLOOP_1400 = (branchObj, view) => {
    let MEDLOOP_1400 = PDE.AnswerUtils.getAnswerResponseByQuestionID('MEDLOOP_1400_Q'),
        screenStack = PDE.QuestionnaireUtils.getScreenStack(),
        IG = PDE.medModule.MedLoopInfo.IG;

    //If MEDLOOP_1400 = No, go back to the previous screen
    if(MEDLOOP_1400 === '0'){
        //Go back to the previous screen
        screenStack.pop();

        //Just pop to the previous screen
        branchObj.branchTo = screenStack.pop();

        return Q.resolve(true);
    }
    //If MEDLOOP_1400 = Yes, complete the deletion
    else if(MEDLOOP_1400 === '1'){
        PDE.LoopingUtils.deleteLoop(IG);
     
        branchObj.branchTo = PDE.medModule.MedLoopInfo.ReviewScreen;

        return LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view);
    }
};

LF.Branching.branchFunctions.exitMedLoop = (branchObj, view) => {    
    branchObj.branchTo = 'AFFIDAVIT';

    return Q.resolve(true);
};