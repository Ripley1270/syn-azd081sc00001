import DynamicText from 'core/dynamicText';

let getSelectedMedNameForm = () => {
    let medCode = PDE.medModule.getMedCode(),
        format = LF.Utilities.getString('DEFAULT_MED_FORMAT'),
        SBJALS1C = PDE.medModule.getSubjectMedAlias(medCode);

    if(!SBJALS1C){
        medCode = PDE.medModule.medModuleConfig.MED_CODE_OTHER;    
    }

    return Q.resolve(PDE.medModule.getMedDisplayText(medCode, format, {'SBJALS1C': SBJALS1C}));    
};

DynamicText.add({
    id : 'selectedMedNameForm',
    evaluate: () => {
        return getSelectedMedNameForm();
    },
    screenshots: {
        getValues () {
            return getSelectedMedNameForm();
        }
    }
});

let getSelectedMedDate = () => {
    let medDate = PDE.medModule.getMedTime();

    return Q.resolve(PDE.MomentUtils.formatDateTime(medDate));
};

DynamicText.add({
    id : 'selectedMedDate',
    evaluate: () => {
        return getSelectedMedDate();
    },
    screenshots: {
        getValues () {
            return PDE.MomentUtils.formatDateTime(moment());
        }
    }
});

let getConflictedMedNameForm = () => {
    let conflictedMed = PDE.medModule.getConflictedMed(),
        medCode = conflictedMed.get('MEDCOD1C'),
        format = LF.Utilities.getString('DEFAULT_MED_FORMAT'),
        SBJALS1C = PDE.medModule.getSubjectMedAlias(medCode);

    if(!SBJALS1C){
        medCode = PDE.medModule.medModuleConfig.MED_CODE_OTHER;    
    }        

return Q.resolve(PDE.medModule.getMedDisplayText(medCode, format, {'SBJALS1C': SBJALS1C}));
};

DynamicText.add({
    id : 'conflictedMedNameForm',
    evaluate: () => {
        return getConflictedMedNameForm();
    },
    screenshots: {
        getValues () {
            return getConflictedMedNameForm();
        }
    }
});

let getConflictedMedDate = () => {
    let conflictedMed = PDE.medModule.getConflictedMed(),
        medTime = moment(conflictedMed.get('MEDTKN1S'), PDE.MomentUtils.getSWDateTimeFormat());

    return Q.resolve(PDE.MomentUtils.formatDateTime(medTime));
};

DynamicText.add({
    id : 'conflictedMedDate',
    evaluate: () => {
        return getConflictedMedDate();
    },
    screenshots: {
        getValues () {
            return PDE.MomentUtils.formatDateTime(moment());
        }
    }
});

DynamicText.add({
    id : 'selectedMEDFRM1L',
    evaluate: () => {
        let selectedMed = PDE.medModule.getMedCode();
        return Q.resolve(PDE.medModule.getMedDisplayText(selectedMed, '{MEDFRM1L}'));
    },
    screenshots: {
        getValues () {
            let medForms = PDE.medModule.medModuleConfig.MED_FORM_LIST,
                result = [];

            $.each(medForms, (index, medForm) => {
                result.push(PDE.medModule.getMEDFRM2LText(medForm.VALUE));
            });

            return result;
        }
    }
});

DynamicText.add({
    id : 'selectedMEDFRM2L',
    evaluate: () => {
        let selectedMed = PDE.medModule.getMedCode();
        return Q.resolve(PDE.medModule.getMedDisplayText(selectedMed, '{MEDFRM2L}'));
    },
    screenshots: {
        getValues () {
            let medForms = PDE.medModule.medModuleConfig.MED_FORM2_LIST,
                result = [];

            $.each(medForms, (index, medForm) => {
                result.push(PDE.medModule.getMEDFRM2LText(medForm.VALUE));
            });

            return result;
        }
    }
});

let getQuestionnaireName = () => {
    return Q.resolve(LF.Utilities.getString('DISPLAY_NAME'));
};

DynamicText.add({
    id : 'getQuestionnaireName',
    evaluate: () => {
        return getQuestionnaireName();
    },
    screenshots: {
        getValues () {
            return getQuestionnaireName();
        }
    }
});
