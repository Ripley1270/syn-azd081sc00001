import { MessageRepo } from 'core/Notify';
import Logger from 'core/Logger';

LF.Widget.ParamFunctions.MEDLOOP_0800Config = (questionId) => {
    let now = new Date(),
        min = new Date(),
        deferred = Q.defer(),
        params = {
            min: '',
            max: LF.Utilities.timeStamp(now)
        };

    min.setDate(min.getDate() - 1);
    params.min = LF.Utilities.timeStamp(min);
    deferred.resolve(params);
    return deferred.promise;
};

PDE.medModule.getMEDLOOP_0800Min = () => {
    let min = new Date();
    min.setDate(min.getDate() - 1);
    return min;
};

PDE.medModule.getMEDLOOP_0700SubjectMedItems = () => {
    let subjectMedModels = PDE.medModule.getSubjectMedsUpdatedOrder(),
        result = [];

    $.each(subjectMedModels, (index, subjectMedModel) => {
        let medCode = subjectMedModel.get('MEDCOD1C'),
            format = LF.Utilities.getString('DEFAULT_MED_FORMAT'),
            SBJALS1C = PDE.medModule.getSubjectMedAlias(medCode),
            newText;

        if (!SBJALS1C) {
            medCode = PDE.medModule.medModuleConfig.MED_CODE_OTHER;
        }

        newText = PDE.medModule.getMedDisplayText(medCode, format, { 'SBJALS1C': SBJALS1C });

        result.push({
            'id': medCode,
            'text': [newText]
        });
    });

    //Add other
    result.push({
        'id': PDE.medModule.medModuleConfig.MED_CODE_OTHER,
        'text': [PDE.medModule.getMedDisplayText(PDE.medModule.medModuleConfig.MED_CODE_OTHER)]
    });

    return Q.resolve(result);
};

PDE.medModule.getSelectedMedMaxUnits = () => {
    let medCode = PDE.medModule.getMedCode(),
        masterMed = PDE.medModule.getMasterMedByMedCode(medCode),
        medFormValue,
        max;

    if (masterMed) {
        medFormValue = masterMed.get('MEDFRM2L');

        if (medFormValue) {
            $.each(PDE.medModule.medModuleConfig.MED_FORM2_LIST, (index, medForm) => {
                if (Number(medForm.VALUE) === Number(medFormValue)) {
                    max = medForm.MAX_AMT;
                    return false;
                }
            });
        }
    }

    return max;
};

PDE.medModule.getUnitsErrorMessage = () => {
    let max = PDE.medModule.getSelectedMedMaxUnits();
    if (max === 99) {
        return 'MED_QTY_INPUT_ERR_1_T0_99';
    } else if (max === 9) {
        return 'MED_QTY_INPUT_ERR_1_T0_9';
    } else {
        logger.error(`Unexpected max value returned for getSelectedMedMaxUnits: ${max}`);
        return 'MED_QTY_INPUT_ERR_1_T0_9';
    }
};

PDE.medModule.getSubjectMedModel = (medCode) => {
    let cache = PDE.medModule.getMedModuleCache(),
        subjectMedModels = cache.subjectMedModels;

    return _.first(_.filter(subjectMedModels, (model) => {
        return model.get('MEDCOD1C') === medCode;
    }));
};

PDE.medModule.getSubjectMedAlias = (medCode) => {
    let subjectModel = PDE.medModule.getSubjectMedModel(medCode),
        result = '';

    if (subjectModel) {
        result = subjectModel.get('SBJALS1C');
        result = PDE.medModule.decodeHtml(result);
    }

    return result;
};

PDE.medModule.getMedCode = (igr) => {
    return PDE.AnswerUtils.getAnswerResponseByQuestionID('MEDLOOP_0700_Q', igr);
};

PDE.medModule.setMedCode = (value, igr) => {
    PDE.AnswerUtils.addAnswerResponseByQuesID('MEDLOOP_0700_Q', PDE.medModule.MedLoopInfo.IG, 'MEDCOD1C', value, igr);
};

PDE.medModule.getMedTime = (igr) => {
    let medDate = PDE.AnswerUtils.getAnswerResponseByQuestionID('MEDLOOP_0800_Q', igr);

    return medDate && new Date(medDate);
};

PDE.medModule.setMedTime = (value, igr) => {
    PDE.AnswerUtils.addAnswerResponseByQuesID('MEDLOOP_0800_Q', PDE.medModule.MedLoopInfo.IG, 'MEDTKN1S', PDE.QuestionnaireUtils.buildStudyWorksDateTimeString(value), igr);
};

PDE.medModule.getMedAmount = (igr) => {
    return PDE.AnswerUtils.getAnswerResponseByQuestionID('MEDLOOP_1100_Q', igr);
};

PDE.medModule.setMedAmount = (value, igr) => {
    PDE.AnswerUtils.addAnswerResponseByQuesID('MEDLOOP_1100_Q', PDE.medModule.MedLoopInfo.IG, 'MEDAMT1N', value, igr);
};

PDE.medModule.getConflictedMed = () => {
    let currentMedCode = PDE.medModule.getMedCode(),
        currentMedTime = moment(PDE.medModule.getMedTime()),
        minTime = moment(currentMedTime).add(-15, 'm'),
        maxTime = moment(currentMedTime).add(15, 'm'),
        IG = PDE.medModule.MedLoopInfo.IG,
        questionnaireView = PDE.QuestionnaireUtils.getQuestionnaireView(),
        currentIGR = questionnaireView.editing || PDE.LoopingUtils.getCurrentIGR(IG),
        conflictedMed;

    if (PDE.medModule.medModuleConfig.MED_CODE_OTHER !== currentMedCode) {
        let medTakenRecords = PDE.medModule.getMedTakenReviewData(IG, true);

        $.each(medTakenRecords, (index, medTaken) => {
            let igr = medTaken.get('igr'),
                medCode = medTaken.get('MEDCOD1C'),
                medTime = moment(medTaken.get('MEDTKN1S'), PDE.MomentUtils.getSWDateTimeFormat());

            //if no igr (it is historical data) or the same IGR AND
            //Med code is not other
            if ((typeof igr === 'undefined' || igr !== currentIGR) &&
                PDE.medModule.medModuleConfig.MED_CODE_OTHER !== medCode) {

                if (medTime.isBetween(minTime, maxTime, 'm', '()')) {

                    if (conflictedMed === undefined) {
                        conflictedMed = medTaken;
                    }
                    else {
                        let conflictedMedTime = moment(conflictedMed.get('MEDTKN1S'), PDE.MomentUtils.getSWDateTimeFormat());

                        //If the time differece with for this conflicted med is less the last conflicted med time
                        if (Math.abs(currentMedTime.diff(medTime)) < Math.abs(currentMedTime.diff(conflictedMedTime))) {
                            conflictedMed = medTaken;
                        }
                    }
                }
            }
        });
    }

    return conflictedMed;
};

//Get the subject meds ordered by frequency
PDE.medModule.getSubjectMedsUpdatedOrder = () => {
    let cache = PDE.medModule.getMedModuleCache(),
        origSubjectMeds = cache.subjectMedModels,
        savedMedTakenModels = cache.medTakenModels,
        currentMedTakenModels = savedMedTakenModels.slice(0),
        maxIGR = PDE.LoopingUtils.getMaxIGR(PDE.medModule.MedLoopInfo.IG),
        newSubjectMeds,
        subjectMedMap = {},
        i;

    //Add current med taken in current questionnaire
    for (i = 1; i <= maxIGR; i++) {
        let MEDCOD1C = PDE.medModule.getMedCode(i),
            medTaken = new LF.Model.MedTakenRecord({ MEDCOD1C: MEDCOD1C });

        currentMedTakenModels.push(medTaken);
    }

    //Create a map of subject med models using the MEDCOD1C as key
    $.each(origSubjectMeds, (index, subjectMed) => {
        let MEDCOD1C = subjectMed.get('MEDCOD1C');

        if (MEDCOD1C !== PDE.medModule.medModuleConfig.MED_CODE_OTHER) {
            subjectMedMap[MEDCOD1C] = subjectMed;
        }
    });

    //Iterate through all the medTaken models and update the numUsed for eaching subject med
    $.each(currentMedTakenModels, (index, medTakenModel) => {
        let MEDCOD1C = medTakenModel.get('MEDCOD1C');

        //If med taken is in the subject med list
        if (subjectMedMap[MEDCOD1C] !== undefined) {
            let numUsed = subjectMedMap[MEDCOD1C].get('numUsed');

            //Increment 1 for each med taken for the this subject med
            if (numUsed === undefined) {
                numUsed = 1;
            }
            else {
                numUsed = Number(numUsed) + 1;
            }

            //Update subject model for number of usage
            subjectMedMap[MEDCOD1C].set('numUsed', numUsed.toString());
        }
    });

    //Sort subject med based on the number useds in descending order
    newSubjectMeds = _.sortBy(origSubjectMeds, (subjectMed) => {
        let numUsed = subjectMed.get('numUsed');

        if (numUsed === undefined) {
            numUsed = 0;
        }

        return -numUsed;
    });

    return newSubjectMeds;
};

PDE.medModule.setMedModuleCache = (cache) => {
    LF.Data.Questionnaire.medModuleCache = cache;
};

PDE.medModule.getMedModuleCache = () => {
    return LF.Data.Questionnaire.medModuleCache;
};

PDE.medModule.decodeHtml = (html) => {
    return $('<div>').html(html).text();
};

PDE.medModule.encodeHtml = (value) => {
    return $('<div/>').text(value).html();
};

PDE.medModule.getMedDisplayText = (medCode, format, additionalText) => {
    let logger = new Logger('getMedDisplayText'),
        masterMed = PDE.medModule.getMasterMedByMedCode(medCode),
        attributes = [],
        //Reg exp to find all the attributes in the format
        //Any text between {}
        re = /{(\w+)}/g,
        match,
        newText;

    //If med code is other med or if there is no master med for med code
    if (medCode === PDE.medModule.medModuleConfig.MED_CODE_OTHER || !masterMed) {
        newText = LF.Utilities.getString('OTHER_MEDICATION');
    }
    else {
        newText = format;

        //Find all attributes names between { }
        match = re.exec(format);

        while (match) {
            attributes.push(match[1]);
            match = re.exec(format);
        }

        if (masterMed) {
            _.each(attributes, (attribute) => {
                let textFuncValue = PDE.medModule.medModuleConfig.MED_ATTRIBUTE_TEXT_FUNCTION,
                    value = masterMed.get(attribute),
                    textFunc;

                if (additionalText && additionalText[attribute]) {
                    newText = newText.replace(`{${attribute}}`, PDE.medModule.decodeHtml(additionalText[attribute]));
                }
                else if (typeof value !== 'undefined') {
                    if (textFuncValue[attribute]) {
                        textFunc = PDE.commonUtils.getPath(textFuncValue[attribute]);

                        if (textFunc) {
                            newText = newText.replace(`{${attribute}}`, PDE.medModule.decodeHtml(textFunc(value)));
                        }
                        else {
                            logger.error(`Unable to find text function: ${textFuncValue}`);
                        }
                    }
                    else {
                        logger.error(`Missing text function for ${attribute}`);
                    }
                }
                else {
                    logger.error(`No value for master meed ${attribute}`);
                }
            });
        }
    }

    return newText;
};

PDE.medModule.getMasterMedByMedCode = (medCode) => {
    let cache = PDE.medModule.getMedModuleCache(),
        masterMeds = cache.masterMedModels,
        masterMed;

    //Find master med base on med code
    masterMed = _.first(_.filter(masterMeds, (model) => {
        return (model.get('MEDCOD1C') === medCode);
    }));

    return masterMed;
};

PDE.medModule.getMEDFRM1LText = (value) => {
    let medForm,
        text;

    medForm = _.first(_.filter(PDE.medModule.medModuleConfig.MED_FORM_LIST, (medform) => {
        return Number(medform.VALUE) === Number(value);
    }));

    if (medForm) {
        text = LF.Utilities.getString(medForm.RESOURCE_ID);
    }

    return text;
};

PDE.medModule.getMEDFRM2LText = (value) => {
    let stringID,
        text;

    switch (value) {
        case '1':
            stringID = 'MEDFRM2L1';
            break;
        case '2':
            stringID = 'MEDFRM2L2';
            break;
        case '3':
            stringID = 'MEDFRM2L3';
            break;
        case '4':
            stringID = 'MEDFRM2L4';
            break;
        case '5':
            stringID = 'MEDFRM2L5';
            break;
        case '6':
            stringID = 'MEDFRM2L6';
            break;
        case '7':
            stringID = 'MEDFRM2L7';
            break;
        case '8':
            stringID = 'MEDFRM2L8';
            break;
        case '9':
            stringID = 'MEDFRM2L9';
            break;
        case '10':
            stringID = 'MEDFRM2L10';
            break;
        case '11':
            stringID = 'MEDFRM2L11';
            break;
        case '12':
            stringID = 'MEDFRM2L12';
            break;
        case '13':
            stringID = 'MEDFRM2L13';
            break;
        case '14':
            stringID = 'MEDFRM2L14';
            break;
        case '15':
            stringID = 'MEDFRM2L15';
            break;
    }

    if (stringID) {
        text = LF.Utilities.getString(stringID);
    }

    return text;
};

