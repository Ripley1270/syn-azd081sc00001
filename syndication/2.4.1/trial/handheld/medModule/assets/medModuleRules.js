import COOL from 'core/COOL';
import Logger from 'core/Logger';

(function (rules) {

    let Utilities = COOL.getClass('Utilities');

    let getQuestionnaireTriggers = (trigger) => {
        let questionnaireIDs = PDE.medModule.medModuleConfig.QUESTIONNAIRE_IDS,
            triggers = [];

        _.each(questionnaireIDs, (questionnaireID) => {
            triggers.push(`${trigger}${questionnaireID}`);
        });

        return triggers;
    };

    let onOpenQuestionnaire = () => {
        let logger = new Logger('MedModule_openMedLoopQuestionnaire');

        return LF.Utilities.getCollections(['MasterMeds', 'SubjectMeds', PDE.medModule.MedLoopInfo.Collection]).then((collections) => {
            let cache = {},
                masterMedCollection = collections['MasterMeds'],
                masterMedModels = (masterMedCollection && masterMedCollection.models.length > 0)? masterMedCollection.models: [],
                subjectMedCollection = collections['SubjectMeds'],
                subjectMedModels = (subjectMedCollection && subjectMedCollection.models.length > 0)? subjectMedCollection.models: [],
                medTakenCollection = collections[PDE.medModule.MedLoopInfo.Collection],
                medTakenModels = (medTakenCollection && medTakenCollection.models.length > 0)? medTakenCollection.models: [];

            cache.masterMedModels = masterMedModels;
            cache.subjectMedModels = subjectMedModels;
            cache.medTakenModels = medTakenModels;

            PDE.medModule.setMedModuleCache(cache);

            // Note: This must be executed after the setMedModuleCache()
            $.each(medTakenModels, (index, medTakenModel) => {
                let SBJALS1C = medTakenModel.get('SBJALS1C'),
                    MEDCOD1C = medTakenModel.get('MEDCOD1C');

                // Update subject alias for resolved meds if the med exist in the Subject Medication list
                if(SBJALS1C === undefined && MEDCOD1C !== PDE.medModule.medModuleConfig.MED_CODE_OTHER){
                    medTakenModel.set('SBJALS1C', PDE.medModule.getSubjectMedAlias(MEDCOD1C));
                }
            });

        }).fail((err) => {
            logger.error('Unable to load med module related collections.', err);
        });
    };

    rules.add({
        id: 'openMedLoopQuestionnaire',
        trigger: (() => {

            let logger = new Logger('MedModule_OpenTriggers');

            let result = getQuestionnaireTriggers('QUESTIONNAIRE:Open/');

            result.push('NAVIGATE:screenshot/takeScreenshots');

            logger.info(JSON.stringify(result));

            return result;
        })(),
        salience: 999,
        evaluate: function (input, resume) {
            resume(true);
        },
        resolve: [
            {
                action () {

                    if (PDE.DeviceStatusUtils.isScreenshotMode()) {
                        // Set flag to load screenshot data
                        localStorage.setItem('BASE_MED_LOOP_LOAD_SCREENSHOT_DATA', '1');
                        return;
                    }

                    return onOpenQuestionnaire();
                }
            }
        ]
    });

    // Load screenshot data for Base Med Loop
    rules.add({
        id: 'baseMedLoopLoadScreenshotData',
        trigger: (() => {
                    let result = getQuestionnaireTriggers('QUESTIONNAIRE:Rendered/');

                    return result;
                })(),
        salience: 999,
        evaluate: function (input, resume) {
            resume(localStorage.getItem('BASE_MED_LOOP_LOAD_SCREENSHOT_DATA') === '1');
        },
        resolve: [
            {
                action () {

                    return onOpenQuestionnaire().then(() => {
                        let cache = PDE.medModule.getMedModuleCache(),
                            medTakenModels = [];

                        // Load dummy answer responses to med loop
                        $.each(cache.subjectMedModels, (index, subjectMed) => {
                            PDE.medModule.setMedCode(subjectMed.get('MEDCOD1C'), index + 1);
                            PDE.medModule.setMedTime(new Date(), index + 1);
                            PDE.medModule.setMedAmount(index + 1, index + 1);

                            medTakenModels.push(new LF.Model.MedTakenRecord({
                                MEDCOD1C: subjectMed.get('MEDCOD1C'),
                                MED_ID1N: index,
                                MEDTKN1S: PDE.QuestionnaireUtils.buildStudyWorksDateTimeString(new Date()),
                                MEDAMT1N: (index + 1).toString(),
                                SBJALS1C: subjectMed.get('SBJALS1C')
                            }));
                        });

                        cache.medTakenModels = medTakenModels;

                        localStorage.removeItem('BASE_MED_LOOP_LOAD_SCREENSHOT_DATA');
                    });
                }
            }
        ]
    });

    rules.add({
        id: 'completedMedLoopQuestionnaire',
        trigger: (() => {
                    return getQuestionnaireTriggers('QUESTIONNAIRE:Completed/');
                })(),
        salience: 999,
        evaluate: function (input, resume) {
            resume(!PDE.DeviceStatusUtils.isScreenshotMode());
        },
        resolve: [
            {
                action () {
                    let logger = new Logger('MedModule_completedMedLoopQuestionnaire'),
                        modelList = new LF.Utilities.ModelList(),
                        maxIGR = PDE.LoopingUtils.getMaxIGR(PDE.medModule.MedLoopInfo.IG),
                        cache = PDE.medModule.getMedModuleCache(),
                        IG = PDE.medModule.MedLoopInfo.IG,
                        MEDLOOP_0900,
                        medCode,
                        medTime,
                        medAmt,
                        subjectAlias,
                        medID,
                        i;

                    for(i = 1; i <= maxIGR; i++){
                        medCode = PDE.medModule.getMedCode(i);
                        medTime = PDE.medModule.getMedTime(i);
                        medAmt = PDE.medModule.getMedAmount(i);
                        subjectAlias = PDE.medModule.getSubjectMedAlias(medCode);
                        medID = PDE.QuestionnaireUtils.generateNextRecordID(true);
                        MEDLOOP_0900 = PDE.AnswerUtils.getAnswerResponseByQuestionID('MEDLOOP_0900_Q', i);

                        if(medCode && medTime && medID){
                            modelList.addNewModel('MedTakenRecord', {
                                MEDCOD1C: medCode,
                                MEDTKN1S: PDE.QuestionnaireUtils.buildStudyWorksDateTimeString(medTime),
                                MEDAMT1N: medAmt,
                                SBJALS1C: subjectAlias,
                                MED_ID1N: medID
                            });

                            if(medCode === PDE.medModule.medModuleConfig.MED_CODE_OTHER){
                                PDE.AnswerUtils.addAnswerResponseByIT(IG, 'OTHTRT1B', '1', i);
                            }
                            else {
                                PDE.AnswerUtils.addAnswerResponseByIT(IG, 'SBJALS1C', subjectAlias, i);
                            }

                            if(MEDLOOP_0900 === '3'){
                                PDE.AnswerUtils.addAnswerResponseByIT(IG, 'ADDETY1B', '1', i);
                            }

                            PDE.AnswerUtils.addAnswerResponseByIT(IG, 'MED_ID1N', medID, i);
                        }
                    }

                    // Add medTakenModels to cache; for use in other ELF rules
                    cache.newMedTakenModels = modelList.getModels();

                    // Remove unneeded answers
                    // Clear MEDLOOP_1200 response. MEDLOOP_1200 is NOT sent to SW
                    PDE.AnswerUtils.removeAnswerModels({questionID: 'MEDLOOP_1200_Q'});
                    PDE.AnswerUtils.removeAnswerModels({questionID: 'MEDLOOP_0900_Q'});

                    return modelList.save()
                        .fail((err) => {
                            logger.error('Failed to save medTakenRecords.', err);
                        });
                }
            }
        ]
    });

    rules.add({
        id: 'MedModule_LoadTrainerMasterMedList',
        trigger: [
            'INSTALL:ModelsInstalled',
            'NAVIGATE:screenshot/screenshot'
        ],
        evaluate: function (input, resume) {
            let result = ((PDE.DeviceStatusUtils.isTrainer() || PDE.DeviceStatusUtils.isScreenshotMode()) &&
                            localStorage.getItem('MASTER_MED_LIST_DEFAULT_LOADED') !== '1' &&
                            typeof PDE.medModule.medModuleConfig.DEFAULT_MASTER_MED_LOADER_TRAINER !== 'undefined');

            resume(result);
        },
        resolve: [
            {
                action () {
                    let logger = new Logger('MedModule_LoadTrainerMasterMedList'),
                        medLoader = PDE.commonUtils.getPath(PDE.medModule.medModuleConfig.DEFAULT_MASTER_MED_LOADER_TRAINER);

                    // If med loader is defined
                    if(medLoader){
                        return medLoader();
                    }

                    logger.error('Failed to load Master Med List. Med Loader is not defined');
                }
            }
        ]
    });

    rules.add({
        id: 'MedModule_LoadActiveMasterMedList',
        trigger: [
            'INSTALL:ModelsInstalled'
        ],
        evaluate: function (input, resume) {
            let result = ((PDE.DeviceStatusUtils.isTrainer() && !PDE.DeviceStatusUtils.isScreenshotMode()) &&
                            localStorage.getItem('MASTER_MED_LIST_DEFAULT_LOADED') !== '1' &&
                            typeof PDE.medModule.medModuleConfig.DEFAULT_MASTER_MED_LOADER_ACTIVE === 'function');

            resume(result);
        },
        resolve: [
            {
                action () {
                    let logger = new Logger('MedModule_LoadTrainerMasterMedList'),
                        medLoader = PDE.commonUtils.getPath(PDE.medModule.medModuleConfig.DEFAULT_MASTER_MED_LOADER_ACTIVE);

                    if(medLoader){
                        return medLoader();
                    }

                    logger.error('Failed to load Master Med List. Med Loader is not defined');
                }
            }
        ]
    });

    rules.add({
        id: 'MedModule_LoadTrainerSubjectMedList',
        trigger: [
            'INSTALL:ModelsInstalled',
            'NAVIGATE:screenshot/screenshot'
        ],
        evaluate: function (input, resume) {
            let result = (localStorage.getItem('SUBJECT_MED_LIST_DEFAULT_LOADED') !== '1' &&
                (PDE.DeviceStatusUtils.isTrainer() || PDE.DeviceStatusUtils.isScreenshotMode()));

            resume(result);
        },
        resolve: [
            {
                action () {
                    localStorage.setItem('SUBJECT_MED_LIST_DEFAULT_LOADED', '1');

                    return PDE.medModule.loadTrainerSubjectMedList(PDE.medModule.medModuleConfig.TRAINER_SUBJECT_MED);
                }
            }
        ]
    });

    rules.add({
        id: 'SubjectMasterMedSync',
        trigger: [
            'INSTALL:ModelsInstalled',
            'TOOLBOX:Transmit',
            'LOGIN:Sync'
        ],
        evaluate: function(filter, resume) {
            if (LF.Utilities.isTrainer()) {
                resume(false);
            } else {
                Utilities.isOnline((isOnline) => {
                    resume(isOnline);
                });
            }
        },
        resolve: [
            {
                action: 'customSyncDataRequest',
                data: {
                    webServiceFunction: 'getSubjMasterMedListSyncData',
                    activation: true
                }
            }
        ]
    });

    rules.add({
        id: 'SubjectMasterMedSyncDataReceived',
        trigger: [
            'CUSTOMDATASYNC:Received/getSubjMasterMedListSyncData'
        ],
        evaluate : true,
        resolve : [
            {
                action (input, done) {
                    let logger = new Logger('MedModuleRules:SubjectMasterMedSyncDataReceived'),
                        xmlDoc;

                        try{
                            // Parse xml
                            if (input.res) {
                                xmlDoc = $.parseXML(input.res);
                            } else {
                                xmlDoc = $.parseXML(input);
                            }
                        }catch(e){
                            logger.error(`Exception processing custom sync. Saving models to collection is aborted: ${e}`);
                            done();
                            return;
                        }

                    if(xmlDoc){
                        return PDE.medModule.updateSubjectMedsByXml(xmlDoc).then(() => {
                            return PDE.medModule.updateMasterMedListByXml(xmlDoc);
                        }).
                        then(() => {
                            logger.info('SubjectMasterMedSync completed successfully');
                            done();
                        })
                        .fail((err) => {
                            logger.error('Failed to complete SubjectMasterMedSync', err);
                            done();
                        });
                    }
                    else {
                        logger.error('Failed to complete SubjectMasterMedSync. Sync result is empty.');
                        done();
                    }
                },
                data: function (input, done) {
                    done(input);
                }
            }
        ]
    });

}(ELF.rules));
