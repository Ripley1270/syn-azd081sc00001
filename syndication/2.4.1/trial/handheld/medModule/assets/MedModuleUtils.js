import Logger from 'core/Logger';

PDE.medModule.getMasterMedURLEncodeProperties = () => {
    return PDE.medModule.medModuleConfig.MASTER_MED_URL_ENCODE_PROPERTIES;
};

PDE.medModule.loadTrainerSubjectMedList = (subjectMeds) => {
    let logger = new Logger('MedModuleUtils:loadDefaultSubjectMedList'),
        modelList = new PDE.CollectionModelUtils.ModelList();

    if(subjectMeds && subjectMeds.length > 0){
        _.each(subjectMeds, (subjectMed) => {
            modelList.addNewModel('SubjectMed', {
                                                    MEDCOD1C: subjectMed.MEDCOD1C,
                                                    SBJALS1C: subjectMed.SBJALS1C
                                                });
        });
    }

    return modelList.save(() => {
        logger.info('Successfully loaded Subject Med List for trainer');
    }).fail((err) => {
        logger.error('Failed to load Trainer Subject Med List', err);
    });            
};

PDE.medModule.updateSubjectMedsByXml = (xmlDoc) => {
    return LF.Utilities.getCollections(['SubjectMedSync']).then((collections) => {
        let logger = new Logger('MedModuleUtils:updateSubjectMedsByXml'),  
            subjectMedSync = collections['SubjectMedSync'],
            subjectMedSyncRecord = (subjectMedSync && subjectMedSync.models.length > 0)?  
                                _.first(subjectMedSync.models): 
                                new LF.Model.SubjectMedSyncRecord(),            
            modelList = new PDE.CollectionModelUtils.ModelList(),                                      
            subjectMedsEl = $(xmlDoc).find('subjectmeds'),
            updateNeeded = subjectMedsEl.attr('updateNeeded');
        
        //If update is needed            
        if(updateNeeded === '1'){
            let newEffectiveDate = subjectMedsEl.attr('effectiveDate'),
                medElementArray = subjectMedsEl.children();

            //Update master med sync record
            if(newEffectiveDate){
                subjectMedSyncRecord.set('effectiveDate', newEffectiveDate);
            }
            
            modelList.addModel(subjectMedSyncRecord);

            if(medElementArray){
                //Build up a list of Master Meds to be save
                $.each(medElementArray, (i, medElement) => {
                    let subjectMed = {};

                    //Convert each master med element to a master med model
                    $.each(medElement.attributes, function (j, attr) {
                        let name = attr.name,
                            value = attr.value;
                        
                        //Check if attribute needs to be html decoded
                        if(_.contains(PDE.medModule.getMasterMedURLEncodeProperties(), name)){
                            subjectMed[name] = $('<div/>').html(value).text();
                        }
                        else {
                            subjectMed[name] = value;
                        }
                    });

                    modelList.addNewModel('SubjectMed', subjectMed);
                });

                return PDE.CollectionModelUtils.clearCollections(['SubjectMeds']).
                    then(() => {
                        return modelList.save();
                    }).
                    then(() => {
                        logger.info('Subject Med List save successfully.');
                    }).
                    fail((err) => {
                        logger.error('Failed to save Subject Med List', err);
                    });    
            }
        }

        logger.info('No update to subject med list.');
        return Q.resolve();
    });
};

PDE.medModule.updateMasterMedListByXml = (xmlDoc, updateNeeded) => {    
        
    return LF.Utilities.getCollections(['MasterMedSync', 'MasterMeds']).then((collections) => {
        let logger = new Logger('MedModuleUtils:updateMasterMedListByXml'),
            masterMeds = collections['MasterMeds'],
            masterMedMap,
            //Initialize master med sync record
            masterMedSync = collections['MasterMedSync'],
            masterMedSyncRecord = (masterMedSync && masterMedSync.models.length > 0)?  
                                    _.first(masterMedSync.models): 
                                    new LF.Model.MasterMedSyncRecord(),
            modelList = new PDE.CollectionModelUtils.ModelList(),            
            masterMedsEl = $(xmlDoc).find('mastermeds');

        updateNeeded = masterMedsEl.attr('updateNeeded');
                           
        //If update is needed            
        if(updateNeeded === '1'){
            let cleanUpdate = masterMedsEl.attr('cleanUpdate'),
                syncVersion = masterMedsEl.attr('syncVersion'),
                medElementArray,
                newEffectiveDate = masterMedsEl.attr('effectiveDate'),
                numNewMeds = 0;

            //Build a map of Master Meds base on MEDCOD1C as key
            if(masterMeds && masterMeds.models.length > 0){
                masterMedMap = {};

                $.each(masterMeds.models, (i, masterMed) => {
                    let MEDCOD1C = masterMed.get('MEDCOD1C');

                    masterMedMap[MEDCOD1C] = masterMed;
                });
            }

            //if there are at least 1 new master med
            if(masterMedsEl.length > 0){                
                medElementArray = masterMedsEl.children();
            }

            //Update master med sync record
            if(newEffectiveDate){
                masterMedSyncRecord.set('effectiveDate', newEffectiveDate);
            }

            if(syncVersion){
                masterMedSyncRecord.set('syncVersion', syncVersion);
            }

            modelList.addModel(masterMedSyncRecord);

            if(medElementArray){
                //Build up a list of Master Meds to be save
                $.each(medElementArray, (i, medElement) => {
                    let masterMed = {};

                    //Convert each master med element to a master med model
                    $.each(medElement.attributes, function (j, attr) {
                        let name = attr.name,
                            value = attr.value;
                        
                        //Check if attribute needs to be html decoded
                        if(_.contains(PDE.medModule.getMasterMedURLEncodeProperties(), name)) {
                            masterMed[name] = $('<div/>').html(value).text();
                        }
                        else {
                            masterMed[name] = value;
                        }
                    });

                    //Check if this is an existing master map
                    if(masterMedMap && masterMedMap[masterMed.MEDCOD1C] !== undefined) {
                        let existingMasterMed = masterMedMap[masterMed.MEDCOD1C],
                            modelID = existingMasterMed.id;
                        
                        //Clear existing master med attributes
                        existingMasterMed.clear();

                        for(let key in masterMed){
                            existingMasterMed.set(key, masterMed[key]);
                        }

                        //Restore model ID because clear() remove attributes and id of the model
                        existingMasterMed.id = modelID;
                        existingMasterMed.set('id', modelID);

                        //Update existing med
                        modelList.addModel(existingMasterMed);
                    }
                    else {
                        modelList.addNewModel('MasterMed', masterMed);
                    }
                    
                    numNewMeds++;
                });

                let saveModels = () => {
                    return modelList.save().then(() => {
                        logger.info(`Master Med List save successfully. ${numNewMeds} new master meds added.`);
    
                        localStorage.setItem('MASTER_MED_LIST_DEFAULT_LOADED', '1');
                    }).fail((err) => {
                        logger.error('Failed to save Master Med List', err);
                    });
                };

                if(cleanUpdate === '1'){
                    return PDE.CollectionModelUtils.clearCollections(['MasterMeds']).then(() => {
                        return saveModels();    
                    });
                }
                else {
                    return saveModels();
                }
            }
        }
        
        logger.info('No update to master med list.');
        localStorage.setItem('MASTER_MED_LIST_DEFAULT_LOADED', '1');
        return Q.resolve();
    });        
};

PDE.medModule.masterMedListLoaderByXml = () => {
    let deferred = Q.defer();

    //Get xml
    $.ajax({
        type: 'GET',
        url: PDE.medModule.medModuleConfig.DEFAULT_MASTER_MED_LIST_PATH,
        dataType: 'xml',
        success: (xml) => {
            deferred.resolve(xml);
        }
    });

    return deferred.promise.then((xml) => {
        return PDE.medModule.updateMasterMedListByXml(xml, '1');
    });
};