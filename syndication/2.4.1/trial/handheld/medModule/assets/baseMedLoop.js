import medModule from './MedModuleConfig';

let IG = medModule.MedLoopInfo.IG;

let baseMedLoop = {
    screenIDs: [
        'MEDLOOP_0100',
        'MEDLOOP_0700',
        'MEDLOOP_0800',
        'MEDLOOP_0900',
        'MEDLOOP_0910',
        'MEDLOOP_1000',
        'MEDLOOP_1100',
        'MEDLOOP_1200',
        'MEDLOOP_1300',
        'MEDLOOP_1400'
    ],
    branches: [
        {
            branchFrom: 'MEDLOOP_0100',
            branchTo: 'MEDLOOP_0100',
            branchFunction: 'exitMedLoop',
            branchParams: {}
        },
        {
            branchFrom: 'MEDLOOP_0800',
            branchTo: 'MEDLOOP_1000',
            branchFunction: 'isOtherMed',
            branchParams: {}
        },
        {
            branchFrom: 'MEDLOOP_0800',
            branchTo: 'MEDLOOP_0900',
            branchFunction: 'isConflictedMed',
            branchParams: {}
        },
        {
            branchFrom: 'MEDLOOP_0800',
            branchTo: 'MEDLOOP_1100',
            branchFunction: 'always',
            branchParams: {}
        },
        {
            branchFrom: 'MEDLOOP_0900',
            branchTo: 'MEDLOOP_1100',
            branchFunction: 'MEDLOOP_0900', // dynamic
            branchParams: {}
        },
        {
            branchFrom: 'MEDLOOP_0910',
            branchTo: 'MEDLOOP_0900',
            branchFunction: 'MEDLOOP_0910', // dynamic
            branchParams: {}
        },
        {
            branchFrom: 'MEDLOOP_1000',
            branchTo: 'MEDLOOP_1200',
            branchFunction: 'always',
            branchParams: {}
        },
        {
            branchFrom: 'MEDLOOP_1200',
            branchTo: 'MEDLOOP_1300',
            branchFunction: 'MEDLOOP_1200', // dynamic
            branchParams: {}
        },
        {
            branchFrom: 'MEDLOOP_1300',
            branchTo: 'MEDLOOP_0100',
            branchFunction: 'alwaysBranchBack',
            branchParams: {}
        },
        {
            branchFrom: 'MEDLOOP_1400',
            branchTo: 'MEDLOOP_0100',
            branchFunction: 'MEDLOOP_1400', // dynamic
            branchParams: {}
        }
    ],
    screens: [
        {
            id: 'MEDLOOP_0100',
            className: 'BaseMedLoop',
            questions: [
                {
                    id: 'MEDLOOP_0100_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MEDLOOP_0700',
            className: 'BaseMedLoop',
            questions: [
                {
                    id: 'MEDLOOP_0700_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MEDLOOP_0800',
            className: 'BaseMedLoop',
            questions: [
                {
                    id: 'MEDLOOP_0800_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MEDLOOP_0900',
            className: 'BaseMedLoop',
            questions: [
                {
                    id: 'MEDLOOP_0900_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MEDLOOP_0910',
            className: 'BaseMedLoop',
            questions: [
                {
                    id: 'MEDLOOP_0910_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MEDLOOP_1000',
            className: 'BaseMedLoop',
            questions: [
                {
                    id: 'MEDLOOP_1000_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MEDLOOP_1100',
            className: 'BaseMedLoop',
            questions: [
                {
                    id: 'MEDLOOP_1100_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MEDLOOP_1200',
            className: 'BaseMedLoop',
            questions: [
                {
                    id: 'MEDLOOP_1200_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MEDLOOP_1300',
            className: 'BaseMedLoop',
            questions: [
                {
                    id: 'MEDLOOP_1300_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MEDLOOP_1400',
            className: 'BaseMedLoop',
            questions: [
                {
                    id: 'MEDLOOP_1400_Q',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'MEDLOOP_0100_Q',
            IG: '',  // Do NOT set looping IG here
            IT: '',
            text: [
                'MEDLOOP_0100_TITLE'
            ],
            className: 'MEDLOOP_0100_Q',
            widget: {
                id: 'MEDLOOP_0100_Q_W',
                type: 'PDEScrollableReviewScreen',
                screenFunction: 'PDE.medModule.MedLoopReviewItems',
                templates: {
                    itemsTemplate: 'PDEScrollableReviewScreen:TwoLineItems',
                    container: 'PDEScrollableReviewScreen:Container'
                },
                headers: [],
                buttons: [
                    {
                        id: '1',
                        availability: 'always',
                        text: 'MEDLOOP_0100_ADD',
                        actionFunction: 'PDE.medModule.addNewMed'
                    },
                    {
                        id: '2',
                        availability: 'PDE.medModule.checkIfRowIsEditable',
                        text: 'MEDLOOP_0100_EDIT',
                        actionFunction: 'PDE.medModule.editMed'
                    },
                    {
                        id: '3',
                        availability: 'PDE.medModule.checkIfRowIsEditable',
                        text: 'MEDLOOP_0100_Q_DELETE',
                        actionFunction: 'PDE.medModule.deleteMed'
                    }
                ],
                editInstructions: 'MEDLOOP_0100_ADD_EDIT'
            }
        },
        {
            id: 'MEDLOOP_0700_Q',
            IG: IG,
            repeating: true,
            IT: 'MEDCOD1C',
            text: [
                'MEDLOOP_0700_Q_TEXT'
            ],
            className: 'MEDLOOP_0700_Q',
            widget: {
                id: 'MEDLOOP_0700_Q_W',
                type: 'PDEListBox',
                screenFunction: () => {
                    return PDE.medModule.getMEDLOOP_0700SubjectMedItems();
                }
            }
        },
        {
            id: 'MEDLOOP_0800_Q',
            IG: IG,
            IT: 'MEDTKN1S',
            text: [
                'MEDLOOP_0800_Q_TEXT'
            ],
            className: 'MEDLOOP_0800_Q',
            widget: {
                id: 'MEDLOOP_0800_Q_W',
                type: 'DateTimePicker',
                className: 'MEDLOOP_0800_Q_W',
                showLabels: true,
                templates: {},
                configFunc: 'MEDLOOP_0800Config'
            }
        },
        {
            id: 'MEDLOOP_0900_Q',
            IG: IG,
            IT: 'MEDLOOP_0900_Q',
            text: [
                'MEDLOOP_0900_Q_TEXT'
            ],
            className: 'MEDLOOP_0900_Q',
            widget: {
                id: 'MEDLOOP_0900_Q_W',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    {
                        text: 'MEDLOOP_0900_A1',
                        value: '1'
                    }, {
                        text: 'MEDLOOP_0900_A2',
                        value: '2'
                    }, {
                        text: 'MEDLOOP_0900_A3',
                        value: '3'
                    }
                ]
            }
        },
        {
            id: 'MEDLOOP_0910_Q',
            IG: IG,
            IT: '',
            text: [
                'MEDLOOP_0910_Q_TEXT'
            ],
            className: 'MEDLOOP_0910_Q',
            widget: {
                id: 'MEDLOOP_0910_Q_W',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    {
                        text: 'MEDLOOP_0910_A1',
                        value: '1'
                    }, {
                        text: 'MEDLOOP_0910_A2',
                        value: '0'
                    }
                ]
            }
        },
        {
            id: 'MEDLOOP_1000_Q',
             // Replace this with localOnly was upgraded to 2.4
            IG: IG + '_',  // [PDE] Hack to resolve an error of copyIGR for editing the loop.
            IT: '',
            text: [
                'MEDLOOP_1000_Q_TEXT'
            ]
        },
        {
            id: 'MEDLOOP_1100_Q',
            IG: IG,
            IT: 'MEDAMT1N',
            text: [
                'MEDLOOP_1100_Q_TEXT'
            ],
            className: 'MEDLOOP_1100_Q',
            widget: {
                id: 'MEDLOOP_1100_Q_W',
                type: 'PDENumericTextBox',
                label: 'MEDLOOP_1100_FORM',
                okButtonText: 'MEDLOOP_1100_OK',
                min: 1,
                max: 99,
                maxFunction: 'PDE.medModule.getSelectedMedMaxUnits',
                step: 1,
                errorMessageFunction: 'PDE.medModule.getUnitsErrorMessage'
            }
        },
        {
            id: 'MEDLOOP_1200_Q',
            IG: IG,
            IT: '',
            text: [
                'MEDLOOP_1200_Q_TEXT'
            ],
            className: 'MEDLOOP_1200_Q',
            widget: {
                id: 'MEDLOOP_1200_Q_W',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    {
                        text: 'MEDLOOP_1200_A1',
                        value: '1'
                    }, {
                        text: 'MEDLOOP_1200_A2',
                        value: '2'
                    }, {
                        text: 'MEDLOOP_1200_A3',
                        value: '3'
                    }
                ]
            }
        },
        {
            id: 'MEDLOOP_1300_Q',
            IG: IG,
            IT: '',
            text: [
                'MEDLOOP_1300_Q_TEXT'
            ]
        },
        {
            id: 'MEDLOOP_1400_Q',
            IG: IG,
            IT: '',
            text: [
                'MEDLOOP_1400_Q_TEXT'
            ],
            className: 'MEDLOOP_1400_Q',
            widget: {
                id: 'MEDLOOP_1400_Q_W',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    {
                        text: 'MEDLOOP_1400_A1',
                        value: '0'
                    }, {
                        text: 'MEDLOOP_1400_A2',
                        value: '1'
                    }
                ]
            }
        }
    ]
};

export default baseMedLoop;

PDE.medModule.baseMedLoop = baseMedLoop;
