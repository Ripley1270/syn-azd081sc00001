import COOL from 'core/COOL';
import Data from 'core/Data';

class MedModuleWebService extends COOL.getClass('WebService') {

    getSyncData (procedureName, params, onSuccess, onError, additionalParams) {
        let ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            },
            krpt = params.krpt || localStorage.getItem('krpt'),
            deviceID = params.subject ? params.subject.device_id : Data.deviceID,
            payload = {
                'StoredProc' : procedureName,
                'Params' : [
                    krpt,
                    deviceID,
                    new Date().ISOStamp(),
                    new Date().getTimezoneOffset() * 60 * 1000
                ]
            };

        if(additionalParams){
            //Add additional parameters
            payload.Params = payload.Params.concat(additionalParams);
        }            

        return this.transmit(ajaxConfig, payload, onSuccess, onError);
    }

    getSubjMasterMedListSyncData (params, onSuccess, onError) {

        return LF.Utilities.getCollections(['MasterMedSync', 'SubjectMedSync']).then((collections) => {
            let masterMedSyncRecord = _.first(collections['MasterMedSync'].models),
                subjectMedSyncRecord = _.first(collections['SubjectMedSync'].models),
                masterMedSyncEffectiveDate,
                subjectMedSyncEffectiveDate,
                syncVersion,
                additionalParams = [];
                
            if(!masterMedSyncRecord){
                masterMedSyncRecord = new LF.Model.MasterMedSyncRecord();
            }
            
            if(!subjectMedSyncRecord){
                subjectMedSyncRecord = new LF.Model.SubjectMedSyncRecord();
            }

            //Add Master Med Effective as a param
            masterMedSyncEffectiveDate = masterMedSyncRecord.get('effectiveDate');
            
            if(!masterMedSyncEffectiveDate){                    
                masterMedSyncEffectiveDate = PDE.medModule.medModuleConfig.MASTER_MED_DEFAULT_EFFECTIVE_DATE;
            }
            
            additionalParams.push(masterMedSyncEffectiveDate);

            //Add sync version as a param
            syncVersion = masterMedSyncRecord.get('syncVersion');

            if(syncVersion){
                additionalParams.push(syncVersion);
            }

            //Add Subject Med Effective as a param
            subjectMedSyncEffectiveDate = subjectMedSyncRecord.get('effectiveDate');
                          
            if(subjectMedSyncEffectiveDate){
                additionalParams.push(subjectMedSyncEffectiveDate);
            }

            return this.getSyncData('PDE_LPASubjMasterMedListSyncSQLV1', params, onSuccess, onError, additionalParams);
        });
    }
}

COOL.add('WebService', MedModuleWebService);