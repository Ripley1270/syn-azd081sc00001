
import assets from './assets';
import collections from './collections';
import models from './models';
import './widgets';

import {mergeObjects} from 'core/utilities/languageExtensions';

export default mergeObjects ({}, assets, collections, models);
