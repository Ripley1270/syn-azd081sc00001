import NumericTextBox from 'core/widgets/NumericTextBox';
import { MessageRepo } from 'core/Notify';

/**
 *  PDENumericTextBox Widget extends the NumericTextBox class.
 *
 *  This is a textbox that limits input to numeric characters only,
 *  and forces a numeric keyboard on the device.  The widget can be created via the "NumericTextBox" widget name.
 */
class PDENumericTextBox extends NumericTextBox {

    /**
     * Constructs the <strong>NumberPad</strong> widget.
     * @param {Object} options Options for the widget.
     * @param {Object} options.model The model containing the specific widget configuration.  This represents the "widget" node in the study design for the question.
     * @param {string} options.model.id {dev-only} The ID associated with the widget.
     * @param {number} options.model.min  The minimum acceptable numeric value.  This property is required, and must be a number.  If these conditions are not met, an exception will be thrown.
     * @param {number} options.model.max The maximum acceptable numeric value.  This property is required, and must be a number.  If these conditions are not met, an exception will be thrown.
     * @param {function} options.model.maxFunction PDE custom field: The max function.
     * @param {function} options.model.errorMessageFunction PDE custom field: Function that returns the message to display on validation error.
     * @param {number} options.model.step Indicates the number of decimal places allowed (this attribute name is taken from the existing HTML5 number input element).   To only allow whole numbers, the step value should be 1.  To allow 2 decimal places, the step should be .01. This property must be a number.  If this condition is not met, an exception will be thrown.
     * @param {Array} options.model.validationErrors {dev-only} This should contain a single element, checking the <code>isInputValid</code> property.
     * @param {string} options.model.className {dev-only} The CSS classname for the widget.
     * @param {string<translated>} options.model.label {dev-only} The text label to be attached to the widget.
     * @param {string<translated>} options.model.placeholder The "placeholder" text that can be rendered within the widget as a hint to the user.  This text will be removed as soon as the usser interacts with the widget.
     */
    constructor (options) {
        super(options);
    }

    // PDE fix for issue with editing and looping
    get displayText () {
        let answer;
        // PDE custom fix for loop editing
        if (!this.answer && this.answers.size()) {
            this.answer = this.answers.models[0];
        }
        // End of PDE custom code
        if (this.answer) {
            answer = this.answer.get('response');
        }
        return answer || '';
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise<void>}
     */
    render () {
        let templates = this.model.get('templates') || {},
            textBox = templates.input || this.defaultInputTemplate,
            toTranslate = {},
            answer = null;

        this.$el.empty();

        // Optionally populate the toTranslate object.
        this.model.get('label') && (toTranslate.label = this.model.get('label'));
        this.model.get('placeholder') && (toTranslate.placeholder = this.model.get('placeholder'));

        return this.i18n(toTranslate, () => null, { namespace: this.getQuestion().getQuestionnaire().id })
            .then((strings) => {
                let wrapperElement,
                    labelElement,
                    textBoxElement;

                wrapperElement = this.renderTemplate(templates.wrapper || this.defaultWrapperTemplate);

                // If a template label is configured, render it.
                if (strings.label) {
                    labelElement = this.renderTemplate(templates.label || this.defaultLabelTemplate, {
                        link: this.model.get('id'),
                        text: strings.label
                    });
                }

                let max = this.model.get('max');

                if (this.model.get('maxFunction')) {
                    let func = PDE.commonUtils.getPath(this.model.get('maxFunction'));
                    if (_.isFunction(func)) {
                        max = func();
                    }
                }

                // Note: the min/max values aren't really required by the
                // element, as we can't rely on the input element for validation.  But, if, in the future, we can, we may as well chuck them in there.
                textBoxElement = this.renderTemplate(textBox, {
                    id: this.model.get('id'),
                    placeholder: strings.placeholder || '',
                    name: `${this.model.get('id')}-${this.uniqueKey}`,
                    min: this.model.get('min'),
                    max,
                    className: this.model.get('className'),
                    step: this.step
                });

                // Append the wrapper to the local DOM and find where to append other elements.
                let $wrapper = this.$el.append(wrapperElement).find('[data-container]');

                if (labelElement) {
                    $wrapper.append(labelElement);
                }

                // Add the textBox to the wrapper, then add the wrapper to the widget's DOM.
                $wrapper.append(textBoxElement)
                    .appendTo(this.$el);

                // Append the widget to the parent element and trigger a create event.
                this.$el.appendTo(this.parent.$el)
                    .trigger('create');

                this.$(`#${this.model.get('id')}`).val(this.displayText);

                this.delegateEvents();

                if (answer !== null) {
                    // Trigger the key-up event so that the answer is properly formatted:
                    this.$(`#${this.model.get('id')}`).input();
                }
            });
    }

    /**
     * respond - Handles the respond to widget input.
     * Includes PDE fix for issue with editing loops
     *
     * @param  {Event} e description
     * @returns {Q.Promise<void>}
     */
    respond (e) {
        let target = this.$(e.target),
            value = target.val() || '';

        if (this.step === 1) {
            value = parseInt(value, 10);
        } else {
            value = parseFloat(value);
        }

        value = isNaN(value) ? '' : value.toString();

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        // PDE custom code for editing looping
        } else if (!this.answer) {
            this.answer = this.answers.models[0];
        // End of PDE custom code
        }

        if (value.length === 0) {
            target.val('');
        }

        return this.respondHelper(this.answer, value);
    }

    /**
     * validate - Adds a PDE custom level on top of standard validation
     * to enable the display of an error message.
     *
     * @return {boolean} - True if the widget response is va`lid
     */
    validate () {
        let isValid;

        if (this.model.get('errorMessageFunction')) {
            let value = this.answer && this.answer.get('response'),
                min = this.model.get('min'),
                max = this.model.get('max');

            if (this.model.get('maxFunction')) {
                let func = PDE.commonUtils.getPath(this.model.get('maxFunction'));
                if (_.isFunction(func)) {
                    max = func();
                }
            }

            if (this.model.get('minFunction')) {
                let func = PDE.commonUtils.getPath(this.model.get('minFunction'));
                if (_.isFunction(func)) {
                    min = func();
                }
            }

            isValid = !isNaN(value) && min <= Number(value) && max >= Number(value);
        } else {
            isValid = super.validate();
        }

        let func = PDE.commonUtils.getPath(this.model.get('errorMessageFunction'));

        if (_.isFunction(func)) {
            let msg = func();
            if (!isValid) {
                MessageRepo.display(MessageRepo.Dialog[msg], { namespace: 'STUDY' });
            }
        }

        return isValid;
    }
}

window.LF.Widget.PDENumericTextBox = PDENumericTextBox;
export default PDENumericTextBox;
