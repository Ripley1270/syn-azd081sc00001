import Base from 'core/models/StorageBase';
export default class SubjectMed extends Base {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'SubjectMed'
     */
    get name () {
        return 'SubjectMed';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            MEDCOD1C : {
                type     : String,
                required : true
            },
            SBJALS1C : {
                type     : String,
                required : true
            },
            //This is not return from a sync. It is calculated located on the device
            numUsed : {
                type     : String,
                required : false
            }
        };
    }
}
window.LF.Model.SubjectMed = SubjectMed;
