import Base from 'core/models/StorageBase';

export default class MasterMed extends Base {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'MasterMed'
     */
    get name () {
        return 'MasterMed';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            MEDCOD1C: {
                type: String,
                required: true
            },
            MEDNAM1C: {
                type: String,
                required: true
            },
            MEDDOS1C: {
                type: String,
                required: true
            },
            MEDRTE1L: {
                type: String,
                required: true
            },
            MEDFRM1L: {
                type: String,
                required: true
            },
            MEDFLG1B: {
                type: String,
                required: false
            },
            MEDFLG2B: {
                type: String,
                required: false
            },
            MEDFLG3B: {
                type: String,
                required: false
            },
            MEDFLG4B: {
                type: String,
                required: false
            },
            MEDFLG5B: {
                type: String,
                required: false
            },
            MEDFLG6B: {
                type: String,
                required: false
            },
            response5: {
                type: String,
                required: false
            },
            MEDFLG7B: {
                type: String,
                required: false
            },
            MEDFLG8B: {
                type: String,
                required: false
            },
            MEDFLG9B: {
                type: String,
                required: false
            },
            MEDVAL1N: {
                type: String,
                required: false
            },
            MEDVAL2N: {
                type: String,
                required: false
            },
            MEDVAL3N: {
                type: String,
                required: false
            },
            MEDVAL4N: {
                type: String,
                required: false
            },
            MEDVAL5N: {
                type: String,
                required: false
            },
            MEDVAL6N: {
                type: String,
                required: false
            },
            MEDVAL7N: {
                type: String,
                required: false
            },
            MEDVAL8N: {
                type: String,
                required: false
            },
            MEDVAL9N: {
                type: String,
                required: false
            },
            MEDLBL1N: {
                type: String,
                required: false
            },
            MEDLBL2N: {
                type: String,
                required: false
            },
            MEDLBL3N: {
                type: String,
                required: false
            },
            MEDLBCL4N: {
                type: String,
                required: false
            },
            MEDLBL5N: {
                type: String,
                required: false
            },
            MEDLBL6N: {
                type: String,
                required: false
            },
            MEDLBL7N: {
                type: String,
                required: false
            },
            MEDLBL8N: {
                type: String,
                required: false
            },
            MEDLBL9N: {
                type: String,
                required: false
            },
            MEDGRP1L: {
                type: String,
                required: false
            }
        };
    }
}
window.LF.Model.MasterMed = MasterMed;
