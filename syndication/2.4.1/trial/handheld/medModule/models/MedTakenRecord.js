import Base from 'core/models/StorageBase';
export default class MedTakenRecord extends Base {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'MedTakenRecord'
     */
    get name () {
        return 'MedTakenRecord';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            MEDCOD1C : {
                type     : String,
                required : true
            },
            MED_ID1N : {
                type     : String,
                required : true
            },
            MEDTKN1S : {
                type     : String,
                required : true
            },
            SBJALS1C : {
                type     : String,
                required : false
            },
            MEDAMT1N : {
                type     : String,
                required : false
            }
        };
    }
}
window.LF.Model.MedTakenRecord = MedTakenRecord;
