import Base from 'core/models/StorageBase';
export default class SubjectMedSyncRecord extends Base {
    /**
     * @property {string} name - The model's name
     * @readonly
     * @default 'LastDiary'
     */
    get name () {
        return 'SubjectMedSyncRecord';
    }

    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            effectiveDate : {
                type     : String,
                required : false
            }
        };
    }
}
window.LF.Model.SubjectMedSyncRecord = SubjectMedSyncRecord;
