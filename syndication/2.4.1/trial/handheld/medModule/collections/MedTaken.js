
import BaseCollection from 'core/collections/StorageBase';
import MedTakenRecord from '../models/MedTakenRecord';

/**
 * A Master Meds collection
 * @class MedTakens
 * @extends BaseCollection
 */
export default class MedTaken extends BaseCollection {

    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return MedTakenRecord;
    }
}

window.LF.Collection.MedTaken = MedTaken;
