
import BaseCollection from 'core/collections/StorageBase';
import MasterMedSyncRecord from '../models/MasterMedSyncRecord';

/**
 * A MasterMedSync collection
 * @class MasterMedSync
 * @extends BaseCollection
 */
export default class MasterMedSync extends BaseCollection {

    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return MasterMedSyncRecord;
    }
}

window.LF.Collection.MasterMedSync = MasterMedSync;
