
import BaseCollection from 'core/collections/StorageBase';
import SubjectMed from '../models/SubjectMed';

/**
 * A Subject Meds collection
 * @class SubjectMeds
 * @extends BaseCollection
 */
export default class SubjectMeds extends BaseCollection {

    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return SubjectMed;
    }
}

window.LF.Collection.SubjectMeds = SubjectMeds;
