
import BaseCollection from 'core/collections/StorageBase';
import MasterMed from '../models/MasterMed';

/**
 * A Master Meds collection
 * @class MasterMeds
 * @extends BaseCollection
 */
export default class MasterMeds extends BaseCollection {

    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return MasterMed;
    }
}

window.LF.Collection.MasterMeds = MasterMeds;
