
import BaseCollection from 'core/collections/StorageBase';
import SubjectMedSyncRecord from '../models/SubjectMedSyncRecord';

/**
 * A SubjectMedSync collection
 * @class SubjectMedSync
 * @extends BaseCollection
 */
export default class SubjectMedSync extends BaseCollection {

    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model () {
        return SubjectMedSyncRecord;
    }
}

window.LF.Collection.SubjectMedSync = SubjectMedSync;
