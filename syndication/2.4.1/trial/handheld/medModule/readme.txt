- MasterMeds / MasterMed
- / MasterMedSyncRecord
- / SubjectMed
- / SubjectMedSyncRecord
- / MedTakenRecord : This is optional if the study doesn't need any historical data after the subject have taken a med
- app/logpad/script.ejs : add masterMedList.xml

In Med Module questionnaires ()
- Insert(by splice) Screen IDs
- Add Screens
- Add Questions
- Add Branching

IMPORT INSTRUCTIONS

Add MedModule folder
- Copy trial\assets\medModule\ to project
- In trial\index.js add:

import 'trial/medModule';

Add Resources
- Copy trial\nls\en-US\studyStrings.STUDY.json to project

Add Style
- Copy trial\less\baseMedLoop.less

Add Reference to master med list xml
- In app\logpad\script.ejs, add:

<!-- START:Study -->
<!-- [MedModule] Reference master med list -->
<script src="./trial/medModule/masterMedList.xml" type="text/xml"></script>
<!-- END:Study -->

Integrate Medication Loop (BaseMedLoop) to study questionnaire
- See trial/assets/study-design/diaries/MedicationDiary.js or trial/assets/study-design/diaries/DiaryMed.js for examples
- Add baseMedLoop ScreenIDs
- Add baseMedLoop Branching
- Add baseMedLoop Screens
- Add baseMedLoop Questions

Update trial/assets/medModule/assets/medModuleConfig.js for study requirement
- Update "QUESTIONNAIRE_IDS" with the questionnaire IDs that requires Medication Loop (BaseMedLoop)
- Update "MED_FORM_LIST" if there are any changes the MEDFRM1L codelist
- Update "MedLoopInfo" if there are any changes to the Medication Loop (BaseMedLoop)


TODO
- Display Med selection base on usage (from Omri)
