import WidgetBase from 'core/widgets/WidgetBase';
import BodyImageMapStorageHH from './BodyImageMapStorageHH';
import BranchHelpers from 'core/branching/branchingHelpers';

class HHBodyDiagram extends WidgetBase {


    // noinspection JSUnusedGlobalSymbols
    get checkboxContainer () {
        return 'DEFAULT:VerticalButtonGroup';
    }

    // noinspection JSMethodCanBeStatic
    get bodyImageMapCheckbox () {
        return 'CUSTOM:BPISFCheckbox';
    }

    get configContainerTemplate() {
        let templates = this.model.get('templates');
        return templates['container'];
    }

    // noinspection JSUnusedGlobalSymbols
    get containerHeight() {
        return this.model.get('height');
    }

    // noinspection JSUnusedGlobalSymbols
    get containerWidth() {
        return this.model.get('width');
    }

    // noinspection JSUnusedGlobalSymbols
    get bodyParts() {
        return this.model.get('bodyParts');
    }

    // noinspection JSUnusedGlobalSymbols
    get stringsToFetch() {
        return this.model.get('labels');
    }

    // noinspection JSUnusedGlobalSymbols
    get labelNamespace() {
        return this.model.get('labelNamespace') || 'HHTrainingModule';
    }

    // noinspection FunctionWithMoreThanThreeNegationsJS
    constructor(options) {

        super(options);

        if (!this.model.get('height')) {
            throw new Error('Missing property "height" in HHBodyDiagram.');
        }

        if (!this.model.get('labels')) {
            throw new Error('Missing property "labels" in HHBodyDiagram.');
        }

        if(!this.model.get('templates')) {
            throw new Error('Missing property "templates" in HHBodyDiagram.');
        }

        this.events = {
            'click svg path': 'bodyPartSelectionHandler',
            'click #noPainButton' : 'noPainButtonHandler'
        };

        this.updateTemplate();
    }

    updateTemplate() {
        let templates = this.configContainerTemplate;
        this.model.set('templates', templates);
    }

    render() {

        let that = this;

        return this.buildHTML()
                    .then(() => {
                        return Q.Promise((resolve) => {
                            that.$el.appendTo(that.getQuestion().$el)
                                    .ready(() => {
                                        resolve();
                                    });
                        });
                    })
                    .then(()=> {
                        this.appendCheckbox();
                    })
                    .then(() => {
                        that.delegateEvents();
                    })
                    .then(() => {
                        return this.updateSelectedAnswers();
                    })
                    .then(() => {
                        this.setCompleted();
                    });
    }

    buildHTML() {
        return Q()
            .then(() => {
                return Q.Promise((resolve) => {

                    let templates = this.model.get('templates'),
                        content = this.renderTemplate(templates, {});

                    this.$el.html(content);
                    resolve();
                });
            })
            .then(() => {
                 if(!BodyImageMapStorageHH.bodyParts) {
                    let bodyPartList = this.bodyParts;
                     BodyImageMapStorageHH.constructBodyPartsFromList(bodyPartList);
                     BodyImageMapStorageHH.constructSelectedBodyIDSSets();
                 }
                 return this.translateAllStrings();
             })
             .then((translatedStrings) => {
                 return Q.Promise((resolve) => {
                     if(this.$('#txtFront')) {
                         this.$('#txtFront').text(translatedStrings["front"]);
                         this.$('#txtFront_L').text(translatedStrings["frontLeft"]);
                         this.$('#txtFront_R').text(translatedStrings["frontRight"]);
                     }
                     if(this.$('#txtBack')) {
                         this.$('#txtBack').text(translatedStrings["back"]);
                         this.$('#txtBack_L').text(translatedStrings["backLeft"]);
                         this.$('#txtBack_R').text(translatedStrings["backRight"]);
                     }
                     resolve(translatedStrings);
                 });
             })
             .then((translatedStrings) => {
                 // reposition labels in SVG if translation is too long
                 var txtFrontLDefLength = 4,
                 txtBackLDefLength = 5,
                 origX;

                 // need optimization later
                 if(translatedStrings["frontLeft"] && (translatedStrings["frontLeft"].length > txtFrontLDefLength)) {
                     origX = this.$('#txtFront_L').attr('x');
                     this.$('#txtFront_L').attr('x', Number(origX) - (((translatedStrings["frontLeft"].length) - txtFrontLDefLength) * 9));
                 }

                 if(translatedStrings["backLeft"] && (translatedStrings["backLeft"].length > txtBackLDefLength)) {
                     origX = this.$('#txtBack_L').attr('x');
                     this.$('#txtBack_L').attr('x', Number(origX) - (((translatedStrings["backLeft"].length) - txtFrontLDefLength) * 9) );
                 }
             });
    }

    /**
     * This function appends checkbox to the bodyImageMap
     * @returns {*}
     */
    appendCheckbox() {

        let bodyImageMapCheckbox = this.bodyImageMapCheckbox;

        return Q()
                .then(() => {
                    let noPain = "";
                    if(this.stringsToFetch.no_pain)
                        noPain = this.stringsToFetch.no_pain;

                    return this.i18n({
                            'noPainText': noPain
                        },
                        $.noop,
                        {namespace: this.getQuestion().getQuestionnaire().id});
                })
                .then((translatedStrings) => {
                    return Q.Promise((resolve) => {
                        let bodyImageMapCheckboxTemplate = this.renderTemplate(bodyImageMapCheckbox, {
                            "NoPainText" : translatedStrings.noPainText
                        });
                        this.$el.append(bodyImageMapCheckboxTemplate);
                        resolve();
                    });// BodyImageMapStorageWithoutLatestFeatures.getNoPainSelected()
                })
                .then(() => {
                    return Q.Promise((resolve) => {
                        let noPainSelected = BodyImageMapStorageHH.getNoPainSelected();
                        if(noPainSelected == true) {
                            $("#noPain_Q2_W1_checkbox_0").prop("checked", true);
                            $(".btn-noPain_Q2_W1_checkbox_0").addClass("active");
                        }
                        resolve();
                    });// BodyImageMapStorageWithoutLatestFeatures.getNoPainSelected()
                });
    }

    // noinspection JSValidateJSDoc
    /**
     * This function does translates all the text
     * @returns {Promise.<TResult>|*} - contains translated strings.
     */
    translateAllStrings() {
        // noinspection FunctionWithInconsistentReturnsJS
        return Q()
            .then(() => {

                let back, backLeft, backRight, front, frontLeft, frontRight;

                if(this.stringsToFetch.back)
                    back = this.stringsToFetch.back;

                if(this.stringsToFetch.back_left)
                    backLeft = this.stringsToFetch.back_left;

                if(this.stringsToFetch.back_right)
                    backRight = this.stringsToFetch.back_right;

                if(this.stringsToFetch.front)
                    front = this.stringsToFetch.front;

                if(this.stringsToFetch.front_left)
                    frontLeft = this.stringsToFetch.front_left;

                if(this.stringsToFetch.front_right)
                    frontRight = this.stringsToFetch.front_right;

                if(front && back) {
                    return this.i18n({
                            'back': back,
                            'backLeft': backLeft,
                            'backRight': backRight,
                            'front': front,
                            'frontLeft': frontLeft,
                            'frontRight': frontRight
                        },
                        $.noop,
                        {namespace: this.getQuestion().getQuestionnaire().id}
                    );
                }
                else if(front) {
                    return this.i18n({
                            'front': front,
                            'frontLeft': frontLeft,
                            'frontRight': frontRight
                        },
                        $.noop,
                        {namespace: this.getQuestion().getQuestionnaire().id}
                    );
                }
                else if(back) {
                    return this.i18n({
                            'back': back,
                            'backLeft': backLeft,
                            'backRight': backRight
                        },
                        $.noop,
                        {namespace: this.getQuestion().getQuestionnaire().id}
                    );
                }
            });
    }

    // noinspection JSValidateJSDoc
    /**
     * Update the image map with previously selected values
     * @returns {*|Promise.<TResult>}
     */
    updateSelectedAnswers() {
        return Q()
            .then(() => {

                if (BodyImageMapStorageHH.selectedFrontTopIDs.length > 0) {
                    _.each(BodyImageMapStorageHH.selectedFrontTopIDs, (selectedBodyID) => {
                        if(this.$("path[id$=':" + selectedBodyID + "']")[0]) {
                            this.$("path[id$=':" + selectedBodyID + "']")[0].classList.add("body-part-selected");
                        }
                    });
                }

                if (BodyImageMapStorageHH.selectedFrontBotIDs.length > 0) {
                    _.each(BodyImageMapStorageHH.selectedFrontBotIDs, (selectedBodyID) => {
                        if(this.$("path[id$=':" + selectedBodyID + "']")[0]) {
                            this.$("path[id$=':" + selectedBodyID + "']")[0].classList.add("body-part-selected");
                        }
                    });
                }

                if (BodyImageMapStorageHH.selectedBackTopIDs.length > 0) {
                    _.each(BodyImageMapStorageHH.selectedBackTopIDs, (selectedBodyID) => {
                        if(this.$("path[id$=':" + selectedBodyID + "']")[0]) {
                            this.$("path[id$=':" + selectedBodyID + "']")[0].classList.add("body-part-selected");
                        }
                    });
                }

                if (BodyImageMapStorageHH.selectedBackBotIDs.length > 0) {
                    _.each(BodyImageMapStorageHH.selectedBackBotIDs, (selectedBodyID) => {
                        if(this.$("path[id$=':" + selectedBodyID + "']")[0]) {
                            this.$("path[id$=':" + selectedBodyID + "']")[0].classList.add("body-part-selected");
                        }
                    });
                }
            });
    }

    // noinspection JSValidateJSDoc
    /**
     * This function determines whether the widget is completed or not
     * @returns {*|Promise.<TResult>}
     */
    setCompleted() {

        let noPainVal;

        return Q()
            .then(() => {
                noPainVal = BodyImageMapStorageHH.getNoPainSelected();

                this.completed = !!((BodyImageMapStorageHH.selectedFrontTopIDs && BodyImageMapStorageHH.selectedFrontTopIDs.length > 0) ||
                (BodyImageMapStorageHH.selectedFrontBotIDs && BodyImageMapStorageHH.selectedFrontBotIDs.length > 0) ||
                (BodyImageMapStorageHH.selectedBackTopIDs && BodyImageMapStorageHH.selectedBackTopIDs.length > 0) ||
                (BodyImageMapStorageHH.selectedBackBotIDs && BodyImageMapStorageHH.selectedBackBotIDs.length > 0) ||
                noPainVal);
            });

    }

    bodyPartSelectionHandler(e) {
        let id = e.currentTarget.id;

        $("#noPainButton").attr("checked", false);
        BodyImageMapStorageHH.setNoPainSelected(false);

        if (id.indexOf(':') != -1) {

            //  ID format = <name:id> ex: "frontHead:1"  - cache selected value for study rule to use
            let bodyPartID = id.split(':')[1];
            let nextScreenID = BodyImageMapStorageHH.getBodyPartNextScreenInfo(bodyPartID);

            BranchHelpers.navigateToScreen(nextScreenID);
        }
        this.setCompleted();
    }

    // noinspection JSUnusedLocalSymbols
    noPainButtonHandler(e) {
        let bodyPortionsToClear = BodyImageMapStorageHH.allBodyPortionText,
            isChecked = $(e.currentTarget).find('input:first').is(':checked');

        BodyImageMapStorageHH.clearAllSelectBodyPartsForBodyPortion(bodyPortionsToClear);

        if(isChecked) {
            BodyImageMapStorageHH.setNoPainSelected(false);
        } else {
            BodyImageMapStorageHH.setNoPainSelected(true);
        }

        this.clearAllSelectedFromUI();
        this.setCompleted();
    }

    clearAllSelectedFromUI() {
        _.each(BodyImageMapStorageHH.bodyParts, (bodyPart) => {
            if(this.$("path[id$=':" + bodyPart.id + "']")[0]) {
                this.$("path[id$=':" + bodyPart.id + "']")[0].classList.remove("body-part-selected");
            }
        });
    }
}

window.LF.Widget.HHBodyDiagram = HHBodyDiagram;

export default HHBodyDiagram;
