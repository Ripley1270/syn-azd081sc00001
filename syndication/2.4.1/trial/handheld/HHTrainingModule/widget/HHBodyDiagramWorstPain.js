import HHBodyDiagram from './HHBodyDiagram';
import BodyImageMapStorageHH from './BodyImageMapStorageHH';
import BranchHelpers from 'core/branching/branchingHelpers';

class HHBodyDiagramWorstPain extends HHBodyDiagram {

    render() {

        let that = this;

        return this.buildHTML()
            .then(() => {
                return Q.Promise((resolve) => {
                    that.$el.appendTo(that.getQuestion().$el)
                        .ready(() => {
                            resolve();
                        });
                });
            })
            .then(() => {
                that.delegateEvents();
            })
            .then(() => {
                return this.updateSelectedAnswers();
            })
            .then(() => {
                this.setCompleted();
            });
    }

    //noinspection JSValidateJSDoc
    /**
     * Update the image map with previously selected values
     * @returns {*|Promise.<TResult>}
     */
    updateSelectedAnswers() {
        return Q()
            .then(() => {
                this.clearAllSelectedFromUI();
            })
            .then(() => {

                let selectedWorstPainID = BodyImageMapStorageHH.getWorstPainSelectedIDs();

                if (selectedWorstPainID) {
                    _.each(selectedWorstPainID, (selectedID) => {
                        this.$("path[id$=':" + selectedID + "']")[0].classList.add("body-part-selected");
                    });
                    this.completed = true;
                }
                else {
                    this.completed = false;
                }
            });
    }

    bodyPartSelectionHandler(e) {

        let selectedWorstPainID = e.currentTarget.id;
        selectedWorstPainID = selectedWorstPainID.split(':')[1];
        //BodyImageMapStorageWithoutLatestFeatures.addRecentlySelectedWorstPainIDs(selectedWorstPainID);
        this.setCompleted()
            .then(() => {
                if(selectedWorstPainID) {
                    let nextScreen = BodyImageMapStorageHH.getBodyPartNextScreen2Info(selectedWorstPainID);
                    BranchHelpers.navigateToScreen(nextScreen);
                }
            });

    }

    //noinspection JSValidateJSDoc
    /**
     * This function determines whether the widget is completed or not
     * @returns {*|Promise.<TResult>}
     */
    setCompleted() {

        return Q()
            .then(() => {

                let worstPainArr = BodyImageMapStorageHH.getWorstPainSelectedIDs();
                if(worstPainArr && worstPainArr.length > 0) {
                    this.completed = true;
                } else {
                    this.completed = false;
                }

            });
    }

}

window.LF.Widget.HHBodyDiagramWorstPain = HHBodyDiagramWorstPain;

export default HHBodyDiagramWorstPain;
