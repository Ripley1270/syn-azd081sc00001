import BodyImageMapStorageHH from './BodyImageMapStorageHH';
import HHBodyDiagram from './HHBodyDiagram';

class HHBodyDiagramZoom extends HHBodyDiagram {

    render () {

        let that = this;

        return this.buildHTML()
            .then(() => {
                return Q.Promise((resolve) => {
                    that.$el.appendTo(that.getQuestion().$el)
                        .ready(() => {
                            resolve();
                        });
                });
            })
            .then(() => {
                that.delegateEvents();
            })
            .then(() => {
                return this.updateSelectedAnswers();
            })
            .then(() => {
                this.completed = true;
                // this.setCompleted();
            });
    }

    bodyPartSelectionHandler (e) {
        let id = e.currentTarget.id;

        if (id.indexOf(':') != -1) {

            // ID format = <name:id> ex: "frontHead:1"  - cache selected value for study rule to use
            let bodyPartID = id.split(':')[1];
            let previouslySelected = BodyImageMapStorageHH.getBodyPartSelectedInfo(bodyPartID),
                previouslyRecentlySelected = BodyImageMapStorageHH.previouslyRecentlySelectedID(bodyPartID);

            let bodyPortion = BodyImageMapStorageHH.getBodyPartBodyPortionInfo(bodyPartID);

            if (previouslyRecentlySelected) {
                e.currentTarget.classList.remove('body-part-selected');
                BodyImageMapStorageHH.removeRecentlySelectedID(bodyPartID);
            } else if (previouslySelected) {
                e.currentTarget.classList.remove('body-part-selected');
                // first detect if the ID was selected in  exists
                let detectedIDPresenceInSelectedList = BodyImageMapStorageHH.detectIfIDPresentInSelectedList(bodyPortion, bodyPartID);
                if (detectedIDPresenceInSelectedList) {
                    BodyImageMapStorageHH.removeSelectedFromBodyPortion(bodyPortion, bodyPartID);
                    //  if ID selected in previous answers not just in recent answers.
                    BodyImageMapStorageHH.addRecentlySelectedIDsToRemove(bodyPartID);
                }
            } else {
                e.currentTarget.classList.add('body-part-selected');
                BodyImageMapStorageHH.addRecentlySelectedIDs(bodyPartID);
            }
        }

        this.completed = true;
        // this.setCompleted();
    }

    setCompleted () {

        return Q()
            .then(() => {
                let modelTemplate = this.model.get('templates');

                if (modelTemplate == 'BPISF:FrontTorsoMostPain') {
                    if (BodyImageMapStorageHH.selectedFrontTopIDs.length > 0) {
                        this.completed = true;
                    } else {
                        this.completed = false;
                    }
                } else if (modelTemplate == 'BPISF:FrontLegsMostPain') {
                    if (BodyImageMapStorageHH.selectedFrontBotIDs.length > 0) {
                        this.completed = true;
                    } else {
                        this.completed = false;
                    }
                } else if (modelTemplate == 'BPISF:BackTorsoMostPain') {
                    if (BodyImageMapStorageHH.selectedBackTopIDs.length > 0) {
                        this.completed = true;
                    } else {
                        this.completed = false;
                    }
                } else if (modelTemplate == 'BPISF:BackLegsMostPain') {
                    if (BodyImageMapStorageHH.selectedBackBotIDs.length > 0) {
                        this.completed = true;
                    } else {
                        this.completed = false;
                    }
                }
            });

    }
}

window.LF.Widget.HHBodyDiagramZoom = HHBodyDiagramZoom;

export default HHBodyDiagramZoom;
