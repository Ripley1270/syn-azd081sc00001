import BodyImageMapStorageHH from './BodyImageMapStorageHH';
import HHBodyDiagramWorstPain from './HHBodyDiagramWorstPain';

class HHBodyDiagramWorstPainZoom extends HHBodyDiagramWorstPain {

    render() {
        return super.render()
            .then(() => {
                return this.updateSelectedAnswers();
            })
            .then(() => {
                this.completed = true;
            });
    }

    //noinspection JSValidateJSDoc
    /**
     * Update the image map with previously selected values
     * @returns {*|Promise.<TResult>}
     */
    updateSelectedAnswers() {
        return Q()
            .then(() => {
                this.clearAllSelectedFromUI();
            })
            .then(() => {

                let selectedWorstPainID = BodyImageMapStorageHH.getWorstPainSelectedIDs();

                if (selectedWorstPainID) {
                    _.each(selectedWorstPainID, (selectedID) => {
                        if(this.$("path[id$=':" + selectedID + "']")[0]) {
                            this.$("path[id$=':" + selectedID + "']")[0].classList.add("body-part-selected");
                        }
                    });
                }
            });
    }

    bodyPartSelectionHandler(e) {

        let id = e.currentTarget.id;

        if (id.indexOf(':') != -1) {

            //ID format = <name:id> ex: "frontHead:1"  - cache selected value for study rule to use
            let bodyPartID = id.split(':')[1];

            let previouslySelected = BodyImageMapStorageHH.checkIfWorstPainArrayContainsID(bodyPartID),
                previouslyRecentlySelected = BodyImageMapStorageHH.previouslyRecentlySelectedWorstPainID(bodyPartID);

            if (previouslyRecentlySelected){
                e.currentTarget.classList.remove("body-part-selected");
                BodyImageMapStorageHH.removeRecentlySelectedWorstPainID(bodyPartID);
            }
            else if(previouslySelected) {
                //first detect if the ID was selected in  exists
                let detectedIDPresenceInWorstPainSelectedList = BodyImageMapStorageHH.detectIfIDPresentInWorstPainSelectedList(bodyPartID);
                if(detectedIDPresenceInWorstPainSelectedList) {
                    BodyImageMapStorageHH.removeWorstPainSelectedID(bodyPartID);
                    e.currentTarget.classList.remove("body-part-selected");
                    BodyImageMapStorageHH.addRecentlySelectedToRemoveWorstPainIDs(bodyPartID);
                }
            }
            else {
                e.currentTarget.classList.add("body-part-selected");
                BodyImageMapStorageHH.addRecentlySelectedWorstPainIDs(bodyPartID);
            }
        }
        //this.setCompleted();
    }

    setCompleted() {

        return Q()
            .then(() => {
                let bodyPortion,
                    modelTemplate = this.model.get("templates"),
                    bodyPortionSelectedPreviously;

                if(modelTemplate == "BPISF:FrontTorsoMostPain") {
                    bodyPortion = BodyImageMapStorageHH.frontTopBodyPortionText;
                } else if(modelTemplate == "BPISF:FrontLegsMostPain") {
                    bodyPortion = BodyImageMapStorageHH.frontBotBodyPortionText;
                } else if(modelTemplate == "BPISF:BackTorsoMostPain") {
                    bodyPortion = BodyImageMapStorageHH.backTopBodyPortionText;
                } else if(modelTemplate == "BPISF:BackLegsMostPain") {
                    bodyPortion = BodyImageMapStorageHH.backBotBodyPortionText;
                }

                bodyPortionSelectedPreviously = BodyImageMapStorageHH.checkIfWorsePainArryContainsBodyRegion(bodyPortion);

                if(bodyPortionSelectedPreviously) {
                    this.completed = true;
                } else {
                    this.completed = false;
                }

            });

    }
}

window.LF.Widget.HHBodyDiagramWorstPainZoom = HHBodyDiagramWorstPainZoom;

export default HHBodyDiagramWorstPainZoom;
