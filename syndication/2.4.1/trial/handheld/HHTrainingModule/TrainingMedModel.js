import StorageBase from 'core/models/StorageBase';

/**
 * @class A model that stores meds from the training diary
 * @augments StorageBase
 * @example var model = new TrainingMedModel();
 */
export default class TrainingMedModel extends StorageBase {

    constructor (options) {
        super(options);

        /**
         * The model's name
         * @readonly
         * @type String
         */
        this.name = 'TrainingMedModel';
    }

    /**
     * The model's schema used for validation and storage construction.
     * @readonly
     * @type Object
     */
    static get schema () {
        return {
            id: {
                type: Number
            },
            medType: {
                type: String,
                required: true,
                encrypt: false
            },
            dateTime: {
                type: String,
                required: true,
                encrypt: false
            },
            amount: {
                type: String,
                required: true,
                encrypt: false
            }
        };
    }
}

window.LF.Model.TrainingMedModel = TrainingMedModel;
