import { mergeObjects } from 'core/utilities/languageExtensions';

import HHTrainingModule from './HHTrainingModule';

import './widget';
import './HHTrainingModule_Branching';
import './HHTrainingModule_Looping';
import './TrainingMedModel';
import './TrainingMedCollection';
import './HHTrainingModule_DynamicText';

import Template from './HH_Template';
import Rules from './HHTrainingModule_Rules';
import BodyMapRules from './HHTrainingModule_BodyMapRules';

let studyDesign = {}; // object
studyDesign = mergeObjects(studyDesign, HHTrainingModule, Template, Rules, BodyMapRules);

export default { studyDesign };
