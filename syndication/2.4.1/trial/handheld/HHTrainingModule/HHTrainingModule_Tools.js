export function getMaxDate () {
    return new Date().ISOLocalTZStamp();
}

export function getMinDate () {
    return new Date(moment(new Date()).subtract(2, 'days').startOf('day')).ISOLocalTZStamp();
}
