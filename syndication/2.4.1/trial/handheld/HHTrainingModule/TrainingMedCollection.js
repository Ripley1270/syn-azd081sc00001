import BaseCollection from 'core/collections/StorageBase';
import TrainingMedModel from './TrainingMedModel';

/**
 * A collection of IA Events
 * @class Visits
 * @extends BaseCollection
 */
export default class TrainingMedCollection extends BaseCollection {
    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model() {
        return TrainingMedModel;
    }
}

window.LF.Collection.TrainingMedCollection = TrainingMedCollection;
