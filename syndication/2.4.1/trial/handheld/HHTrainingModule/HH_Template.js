export default {
    templates: [
        // PDE Looping
        {
            name: 'CustomReviewScreen',
            namespace: 'TwoRows',
            template: '<ul id="{{ id }}_Items" class="reviewScreen_Items" data-role="listview">' +
            '</ul>' +
            '<ul id="{{ id }}_Buttons" class="reviewScreen_Buttons" data-role="listview"></ul>' +
            '<p class="edit-instructions">{{editInstructions}}</p>' +
            '<p class="additional-instructions">{{header0}}</p>'
        },
        {
            name: 'CustomScreenItems',
            namespace: 'TwoRows',
            template: '<li class="review-list">' +
            '<div class="ui-grid-a" style="display:flex; flex-direction: column;">' +
            '<div class="ui-block-a" text-align: center;">{{ text0 }}</div>' +
            '<div class="ui-block-b" text-align: center;">{{ text1 }}</div>' +
            '</div>' +
            '</li>'
        }
    ]
};
