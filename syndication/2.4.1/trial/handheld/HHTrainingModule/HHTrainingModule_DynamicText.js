import DynamicText from 'core/dynamicText';

DynamicText.add({
    id: 'HHTM200_4_MED',
    evaluate: () => {
        const namespace = 'HHTrainingModule';
        return LF.getStrings([
            { key: 'HHTM200_5_Q1_W0_A0', namespace },
            { key: 'HHTM200_5_Q1_W0_A1', namespace }
        ])
            .then((result) => {
                const qView = LF.router.view(),
                    [meds] = qView.queryAnswersByQuestionIDAndIGR('HHTM200_5_Q1', -1),
                    [dateTime] = qView.queryAnswersByQuestionIDAndIGR('HHTM200_2_Q1', -1),
                    [qty] = qView.queryAnswersByQuestionIDAndIGR('HHTM200_3_Q1', -1),
                    // dateTimeFmt = moment(dateTime.get('response')).format('DD MMM YYYY HH:mm');
                    dateTimeFmt = PDE.DateTimeUtils.formatDateTime(new Date(dateTime.get('response')));

                return meds.get('response') === '1' ?
                    `${result[0]}<br/>${dateTimeFmt}` :
                    `${result[1]}<br/>${dateTimeFmt}`;
            });
    },
    screenshots: {
        getValues () {
            const namespace = 'HHTrainingModule';
            return LF.getStrings([
                { key: 'HHTM200_5_Q1_W0_A0', namespace },
                { key: 'HHTM200_5_Q1_W0_A1', namespace }
            ]).then((result) => {
                // const dateTimeFmt = moment(new Date()).format('DD MMM YYYY HH:mm');
                const dateTimeFmt = PDE.DateTimeUtils.formatDateTime(new Date());

                return [`${result[0]}<br/>${dateTimeFmt}`,
                    `${result[1]}<br/>${dateTimeFmt}`
                ];
            });
        }
    }
});
