import BodyImageMapStorageHH from './widget/BodyImageMapStorageHH';
import Logger from 'core/Logger';

export default {
    rules: [
        {
            id: 'NavigateFromHHTM162',
            trigger: 'QUESTIONNAIRE:Navigate/HHTrainingModule/HHTM161',
            evaluate (filter, resume) {
                let direction = filter.direction;

                if (direction === 'previous') {
                    let bodyPortionToClear = BodyImageMapStorageHH.allBodyPortionText;
                    BodyImageMapStorageHH.clearAllSelectBodyPartsForBodyPortion(bodyPortionToClear);
                    BodyImageMapStorageHH.removeAllWorstPainSelectedID();
                    BodyImageMapStorageHH.setNoPainSelected(false);
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'OpenHHTrainingModule',
            trigger: 'QUESTIONNAIRE:Open/HHTrainingModule',
            evaluate (filter, resume) {
                let bodyPortionToClear = BodyImageMapStorageHH.allBodyPortionText;
                BodyImageMapStorageHH.clearAllSelectBodyPartsForBodyPortion(bodyPortionToClear);
                BodyImageMapStorageHH.removeAllWorstPainSelectedID();
                BodyImageMapStorageHH.setNoPainSelected(false);
                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromHHTM163',
            trigger: 'QUESTIONNAIRE:Navigate/HHTrainingModule/HHTM163',
            evaluate (filter, resume) {
                let direction = filter.direction;

                if (direction === 'previous') {
                    let bodyPortionToClear = BodyImageMapStorageHH.frontTopBodyPortionText;
                    BodyImageMapStorageHH.removeRecentlySelectedFromBodyPortion(bodyPortionToClear);
                    BodyImageMapStorageHH.removeRecentlySelectedToRemoveFromBodyPortion(bodyPortionToClear);
                } else {
                    BodyImageMapStorageHH.addSelectedFromRecentlySelected();
                    LF.Data.Questionnaire.screenStack.pop();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromHHTM164',
            trigger: 'QUESTIONNAIRE:Navigate/HHTrainingModule/HHTM164',
            evaluate (filter, resume) {
                let direction = filter.direction;

                if (direction === 'previous') {
                    let bodyPortionToClear = BodyImageMapStorageHH.frontBotBodyPortionText;
                    BodyImageMapStorageHH.removeRecentlySelectedFromBodyPortion(bodyPortionToClear);
                    BodyImageMapStorageHH.removeRecentlySelectedToRemoveFromBodyPortion(bodyPortionToClear);
                } else {
                    BodyImageMapStorageHH.addSelectedFromRecentlySelected();
                    LF.Data.Questionnaire.screenStack.pop();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromHHTM165',
            trigger: 'QUESTIONNAIRE:Navigate/HHTrainingModule/HHTM165',
            evaluate (filter, resume) {
                let direction = filter.direction;

                if (direction === 'previous') {
                    let bodyPortionToClear = BodyImageMapStorageHH.backTopBodyPortionText;
                    BodyImageMapStorageHH.removeRecentlySelectedFromBodyPortion(bodyPortionToClear);
                    BodyImageMapStorageHH.removeRecentlySelectedToRemoveFromBodyPortion(bodyPortionToClear);
                } else {
                    BodyImageMapStorageHH.addSelectedFromRecentlySelected();
                    LF.Data.Questionnaire.screenStack.pop();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromHHTM166',
            trigger: 'QUESTIONNAIRE:Navigate/HHTrainingModule/HHTM166',
            evaluate (filter, resume) {
                let direction = filter.direction;

                if (direction === 'previous') {
                    let bodyPortionToClear = BodyImageMapStorageHH.backBotBodyPortionText;
                    BodyImageMapStorageHH.removeRecentlySelectedFromBodyPortion(bodyPortionToClear);
                    BodyImageMapStorageHH.removeRecentlySelectedToRemoveFromBodyPortion(bodyPortionToClear);
                } else {
                    BodyImageMapStorageHH.addSelectedFromRecentlySelected();
                    LF.Data.Questionnaire.screenStack.pop();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'BPIDCompletion',
            trigger: [
                'QUESTIONNAIRE:Completed/HHTrainingModule'
            ],
            evaluate (filter, resume) {
                resume(!localStorage.getItem('ScreenshotMode'));
            },
            resolve: [
                {
                    action (input, done) {
                        let logger = new Logger('StudyRules:HHTrainingModule');

                        let bodyPortionToClear = BodyImageMapStorageHH.allBodyPortionText;
                        BodyImageMapStorageHH.clearAllSelectBodyPartsForBodyPortion(bodyPortionToClear);
                        BodyImageMapStorageHH.removeAllWorstPainSelectedID();
                        BodyImageMapStorageHH.setNoPainSelected(false);
                        done();
                    }
                }
            ]
        }
    ]
};
