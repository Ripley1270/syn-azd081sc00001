import * as Tools from './HHTrainingModule_Tools';

export default {
    questionnaires: [
        {
            id: 'HHTrainingModule',
            SU: 'HHTrainingModule',
            displayName: 'DISPLAY_NAME',
            className: 'questionnaire',
            previousScreen: false,
            affidavit: 'DEFAULT',
            screens: [
                'HHTM010',
                'HHTM020',
                'HHTM030',
                'HHTM050_CUSTOM',
                'HHTM060',
                'HHTM080_BOT',
                'HHTM090_POST',
                'HHTM140_lead',
                'HHTM140',
                'HHTM160',
                'HHTM161',
                'HHTM162',
                'HHTM163',
                'HHTM164',
                'HHTM165',
                'HHTM166',
                'HHTM190',
                'HHTM200_1',
                'HHTM200_5',
                'HHTM200_2',
                'HHTM200_3',
                'HHTM200_4',
                'HHTM980',
                'HHTM990'
            ],
            branches: [
                {
                    id: 'B1173',
                    branchFrom: 'HHTM162',
                    branchTo: 'HHTM190',
                    branchFunction: 'always'
                },
                /*
                    B1174, B1175, B1176, B1181 handled by library
                 */
                {
                    id: 'B1177',
                    branchFrom: 'HHTM163',
                    branchTo: 'HHTM162',
                    clearBranchedResponses: true,
                    branchFunction: 'branchPopScreen'
                },
                {
                    id: 'B1178',
                    branchFrom: 'HHTM164',
                    branchTo: 'HHTM162',
                    clearBranchedResponses: true,
                    branchFunction: 'branchPopScreen'
                },
                {
                    id: 'B1179',
                    branchFrom: 'HHTM165',
                    branchTo: 'HHTM162',
                    clearBranchedResponses: true,
                    branchFunction: 'branchPopScreen'
                },
                {
                    id: 'B1180',
                    branchFrom: 'HHTM166',
                    branchTo: 'HHTM162',
                    clearBranchedResponses: true,
                    branchFunction: 'branchPopScreen'
                },
                {
                    id: 'B471',
                    branchFrom: 'HHTM200_1',
                    branchTo: 'HHTM980',
                    branchFunction: 'always'
                },
                {
                    id: 'B1162',
                    branchFrom: 'HHTM200_2',
                    branchTo: 'HHTM200_1',
                    clearBranchedResponses: true,
                    branchFunction: 'HHTrainingModuleLoopCompareFirst',
                    branchParams: {
                        questionId: 'HHTM200_5_Q1',
                        value: '2',
                        IG: 'LPTM1'
                    }
                },
                {
                    id: 'B476',
                    branchFrom: 'HHTM200_3',
                    branchTo: 'HHTM200_1',
                    branchFunction: 'HHTrainingModuleLoop'
                },
                {
                    id: 'B477',
                    branchFrom: 'HHTM200_4',
                    branchTo: 'HHTM200_1',
                    branchFunction: 'deleteLoop',
                    branchParams: {
                        questionId: 'HHTM200_4_Q1',
                        value: '1',
                        IG: 'LPTM1'
                    }
                },
                {
                    id: 'B479',
                    branchFrom: 'HHTM990',
                    branchTo: 'HHTM010',
                    clearBranchedResponses: true,
                    branchFunction: 'equalBranchBackClearHHAnswers',
                    branchParams: {
                        questionId: 'HHTM990_Q1',
                        value: '1'
                    }
                }
            ],
            initialScreen: [],
            accessRoles: [
                'subject',
                'site'
            ]
        }],
    screens: [
        {
            id: 'HHTM010',
            className: 'HHTM010',
            questions: [
                {
                    id: 'HHTM010_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM020',
            className: 'HHTM020',
            questions: [
                {
                    id: 'HHTM020_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM030',
            className: 'HHTM030',
            questions: [
                {
                    id: 'HHTM030_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM050_CUSTOM',
            className: 'HHTM050_CUSTOM',
            questions: [
                {
                    id: 'HHTM050_CUSTOM_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM060',
            className: 'HHTM060',
            questions: [
                {
                    id: 'HHTM060_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM080_BOT',
            className: 'HHTM080_BOT',
            questions: [
                {
                    id: 'HHTM080_BOT_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM090_POST',
            className: 'HHTM090_POST',
            questions: [
                {
                    id: 'HHTM090_POST_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM140_lead',
            className: 'HHTM140_lead',
            questions: [
                {
                    id: 'HHTM140_lead_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM140',
            className: 'HHTM140',
            questions: [
                {
                    id: 'HHTM140_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM160',
            className: 'HHTM160',
            questions: [
                {
                    id: 'HHTM160_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM161',
            className: 'HHTM161',
            questions: [
                {
                    id: 'HHTM161_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM162',
            className: 'HHTM162',
            questions: [
                {
                    id: 'HHTM162_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM163',
            className: 'HHTM163',
            questions: [
                {
                    id: 'HHTM163_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM164',
            className: 'HHTM164',
            questions: [
                {
                    id: 'HHTM164_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM165',
            className: 'HHTM165',
            questions: [
                {
                    id: 'HHTM165_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM166',
            className: 'HHTM166',
            questions: [
                {
                    id: 'HHTM166_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM190',
            className: 'HHTM190',
            questions: [
                {
                    id: 'HHTM190_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM200_1',
            className: 'HHTM200_1',
            questions: [
                {
                    id: 'HHTM200_1_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM200_5',
            className: 'HHTM200_5',
            questions: [
                {
                    id: 'HHTM200_5_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM200_2',
            className: 'HHTM200_2',
            questions: [
                {
                    id: 'HHTM200_2_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM200_3',
            className: 'HHTM200_3',
            questions: [
                {
                    id: 'HHTM200_3_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM200_4',
            className: 'HHTM200_4',
            questions: [
                {
                    id: 'HHTM200_4_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM980',
            className: 'HHTM980',
            questions: [
                {
                    id: 'HHTM980_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'HHTM990',
            className: 'HHTM990',
            questions: [
                {
                    id: 'HHTM990_Q1',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'HHTM010_Q1',
            text: 'HHTM010_Q1_TEXT',
            title: '',
            className: 'HHTM010_Q1'
        }, {
            id: 'HHTM020_Q1',
            text: 'HHTM020_Q1_TEXT',
            title: '',
            className: 'HHTM020_Q1'
        }, {
            id: 'HHTM030_Q1',
            text: 'HHTM030_Q1_TEXT',
            title: '',
            className: 'HHTM030_Q1'
        }, {
            id: 'HHTM050_CUSTOM_Q1',
            IG: 'REMOVE',
            IT: '',
            localOnly: true,
            text: 'HHTM050_CUSTOM_Q1_TEXT',
            title: '',
            className: 'HHTM050_CUSTOM_Q1',
            widget: {
                id: 'HHTM050_CUSTOM_Q1_W0',
                type: 'CustomRadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'HHTM050_CUSTOM_Q1_W0_A0'
                    },
                    {
                        value: '2',
                        text: 'HHTM050_CUSTOM_Q1_W0_A1'
                    },
                    {
                        value: '3',
                        text: 'HHTM050_CUSTOM_Q1_W0_A2'
                    },
                    {
                        value: '4',
                        text: 'HHTM050_CUSTOM_Q1_W0_A3'
                    }
                ]
            }
        }, {
            id: 'HHTM060_Q1',
            IG: 'REMOVE',
            IT: '',
            localOnly: true,
            text: 'HHTM060_Q1_TEXT',
            title: '',
            className: 'HHTM060_Q1',
            widget: {
                id: 'HHTM060_Q1_W0',
                type: 'DropDownList',
                items: [
                    {
                        value: '1',
                        text: 'HHTM060_Q1_W0_A0'
                    },
                    {
                        value: '2',
                        text: 'HHTM060_Q1_W0_A1'
                    },
                    {
                        value: '3',
                        text: 'HHTM060_Q1_W0_A2'
                    },
                    {
                        value: '4',
                        text: 'HHTM060_Q1_W0_A3'
                    },
                    {
                        value: '5',
                        text: 'HHTM060_Q1_W0_A4'
                    }
                ]
            }
        }, {
            id: 'HHTM080_BOT_Q1',
            IG: 'REMOVE',
            IT: '',
            localOnly: true,
            text: 'HHTM080_BOT_Q1_TEXT',
            title: '',
            className: 'HHTM080_BOT_Q1',
            widget: {
                id: 'HHTM080_BOT_Q1_W0',
                type: 'NumericRatingScale',
                width: '100',
                displayText: 'HHTM080_BOT_Q1_W0_DisplayText',
                reverseOnRtl: true,
                text: 'HHTM080_BOT_Q1_W0_Text',
                answers: [
                    {
                        value: 'A',
                        text: 'HHTM080_BOT_Q1_W0_A0'
                    },
                    {
                        value: 'B',
                        text: 'HHTM080_BOT_Q1_W0_A1'
                    },
                    {
                        value: 'C',
                        text: 'HHTM080_BOT_Q1_W0_A2'
                    },
                    {
                        value: 'D',
                        text: 'HHTM080_BOT_Q1_W0_A3'
                    },
                    {
                        value: 'E',
                        text: 'HHTM080_BOT_Q1_W0_A4'
                    },
                    {
                        value: 'F',
                        text: 'HHTM080_BOT_Q1_W0_A5'
                    },
                    {
                        value: 'G',
                        text: 'HHTM080_BOT_Q1_W0_A6'
                    },
                    {
                        value: 'H',
                        text: 'HHTM080_BOT_Q1_W0_A7'
                    },
                    {
                        value: 'I',
                        text: 'HHTM080_BOT_Q1_W0_A8'
                    },
                    {
                        value: 'J',
                        text: 'HHTM080_BOT_Q1_W0_A9'
                    },
                    {
                        value: 'k',
                        text: 'HHTM080_BOT_Q1_W0_A10'
                    }
                ],
                markers: {
                    left: 'HHTM080_BOT_Q1_W0_LeftMarker',
                    right: 'HHTM080_BOT_Q1_W0_RightMarker',
                    justification: 'inside',
                    position: 'below',
                    spaceAllowed: 2
                }
            }
        }, {
            id: 'HHTM090_POST_Q1',
            IG: 'REMOVE',
            IT: '',
            localOnly: true,
            text: 'HHTM090_POST_Q1_TEXT',
            title: '',
            className: 'HHTM090_POST_Q1',
            widget: {
                id: 'HHTM090_POST_Q1_W0',
                type: 'VAS',
                displayAs: 'Vertical',
                initialCursorDisplay: false,
                reverseOnRtl: true,
                anchors: {
                    max: {
                        text: 'HHTM090_POST_Q1_W0_LEFT',
                        value: 0
                    },
                    min: {
                        text: 'HHTM090_POST_Q1_W0_RIGHT',
                        value: 100
                    }
                },
                pointer: {
                    isVisible: true
                },
                customProperties: {
                    labels: {
                        position: 'below',
                        arrow: true
                    }
                },
                selectedValue: {
                    isVisible: true
                }
            }
        }, {
            id: 'HHTM140_lead_Q1',
            text: 'HHTM140_lead_Q1_TEXT',
            title: '',
            className: 'HHTM140_lead_Q1'
        }, {
            id: 'HHTM140_Q1',
            IG: 'REMOVE',
            IT: '',
            localOnly: true,
            text: 'HHTM140_Q1_TEXT',
            title: '',
            className: 'HHTM140_Q1',
            widget: {
                id: 'HHTM140_Q1_W0',
                className: 'HHTM140_Q1_W0',
                type: 'DateTimePicker',
                modalTitle: 'HHTM140_Q1_W0_modalTitle',
                showLabels: false,
                min: Tools.getMinDate,
                max: Tools.getMaxDate
            }
        }, {
            id: 'HHTM160_Q1',
            IG: 'REMOVE',
            IT: '',
            localOnly: true,
            text: 'HHTM160_Q1_TEXT',
            title: '',
            className: 'HHTM160_Q1',
            widget: {
                id: 'HHTM160_Q1_W0',
                className: 'HHTM160_Q1_W0',
                type: 'NumericTextBox',
                label: 'HHTM160_Q1_W0_label',
                placeholder: 'HHTM160_Q1_W0_placeHolder',
                min: 0,
                max: 99,
                step: '1'
            }
        }, {
            id: 'HHTM161_Q1',
            text: 'HHTM161_Q1_TEXT',
            title: '',
            className: 'HHTM161_Q1'
        }, {
            id: 'HHTM162_Q1',
            IG: 'REMOVE',
            IT: '',
            localOnly: true,
            text: 'HHTM162_Q1_TEXT',
            title: '',
            className: 'HHTM162_Q1',
            widget: {
                id: 'HHTM162_Q1_W0',
                type: 'HHBodyDiagram',
                templates: {
                    container: 'BPISF:FrontBackBodyMostPain'
                },
                height: 375,
                labelNamespace: 'BPID',
                labels: {
                    front: 'HHTM162_Q1_1',
                    front_left: 'HHTM162_Q1_2',
                    front_right: 'HHTM162_Q1_3',
                    back: 'HHTM162_Q1_4',
                    back_left: 'HHTM162_Q1_5',
                    back_right: 'HHTM162_Q1_6',
                    no_pain: 'HHTM162_Q2_A1'
                },
                bodyParts: [
                    { value: '1', IT: 'FR_HEA1B' },
                    { value: '2', IT: 'FR_SHL1B' },
                    { value: '4', IT: 'FR_SHL2B' },
                    { value: '3', IT: 'FR_THR1B' },
                    { value: '6', IT: 'FR_THR2B' },
                    { value: '7', IT: 'FR_THR3B' },
                    { value: '8', IT: 'FR_THR4B' },
                    { value: '10', IT: 'FR_THR5B' },
                    { value: '11', IT: 'FR_THR6B' },
                    { value: '12', IT: 'FR_THR7B' },
                    { value: '13', IT: 'FR_THR8B' },
                    { value: '14', IT: 'FR_THR9B' },
                    { value: '5', IT: 'FR_ARM1B' },
                    { value: '9', IT: 'FR_ARM2B' },
                    { value: '34', IT: 'FR_ARM3B' },
                    { value: '35', IT: 'FR_ARM4B' },
                    { value: '50', IT: 'FR_ARM5B' },
                    { value: '51', IT: 'FR_ARM6B' },
                    { value: '15', IT: 'FR_LEG1B' },
                    { value: '16', IT: 'FR_LEG2B' },
                    { value: '36', IT: 'FR_LEG3B' },
                    { value: '37', IT: 'FR_LEG4B' },
                    { value: '38', IT: 'FR_LEG5B' },
                    { value: '39', IT: 'FR_LEG6B' },
                    { value: '40', IT: 'FR_LEG7B' },
                    { value: '41', IT: 'FR_LEG8B' },
                    { value: '17', IT: 'BK_HEA1B' },
                    { value: '18', IT: 'BK_NCK1B' },
                    { value: '19', IT: 'BK_SHL1B' },
                    { value: '21', IT: 'BK_SHL2B' },
                    { value: '20', IT: 'BK_THR1B' },
                    { value: '23', IT: 'BK_THR2B' },
                    { value: '24', IT: 'BK_THR3B' },
                    { value: '25', IT: 'BK_THR4B' },
                    { value: '27', IT: 'BK_THR5B' },
                    { value: '28', IT: 'BK_THR6B' },
                    { value: '29', IT: 'BK_THR7B' },
                    { value: '30', IT: 'BK_THR8B' },
                    { value: '31', IT: 'BK_THR9B' },
                    { value: '22', IT: 'BK_ARM1B' },
                    { value: '26', IT: 'BK_ARM2B' },
                    { value: '42', IT: 'BK_ARM3B' },
                    { value: '43', IT: 'BK_ARM4B' },
                    { value: '52', IT: 'BK_ARM5B' },
                    { value: '53', IT: 'BK_ARM6B' },
                    { value: '32', IT: 'BK_LEG1B' },
                    { value: '33', IT: 'BK_LEG2B' },
                    { value: '44', IT: 'BK_LEG3B' },
                    { value: '45', IT: 'BK_LEG4B' },
                    { value: '46', IT: 'BK_LEG5B' },
                    { value: '47', IT: 'BK_LEG6B' },
                    { value: '48', IT: 'BK_LEG7B' },
                    { value: '49', IT: 'BK_LEG8B' }
                ]
            }
        }, {
            id: 'HHTM163_Q1',
            IG: 'REMOVE',
            IT: '',
            localOnly: true,
            text: 'HHTM163_Q1_TEXT',
            title: '',
            className: 'HHTM163_Q1',
            widget: {
                id: 'HHTM163_Q1_W0',
                type: 'HHBodyDiagramZoom',
                templates: {
                    container: 'BPISF:FrontTorsoMostPain'
                },
                height: 375,
                labels: {
                    front: 'HHTM163_Q1_1',
                    front_right: 'HHTM163_Q1_2',
                    front_left: 'HHTM163_Q1_3'
                }
            }
        }, {
            id: 'HHTM164_Q1',
            IG: 'REMOVE',
            IT: '',
            localOnly: true,
            text: 'HHTM164_Q1_TEXT',
            title: '',
            className: 'HHTM164_Q1',
            widget: {
                id: 'HHTM164_Q1_W0',
                type: 'HHBodyDiagramZoom',
                templates: {
                    container: 'BPISF:FrontLegsMostPain'
                },
                height: 375,
                labels: {
                    front: 'HHTM164_Q1_1',
                    front_right: 'HHTM164_Q1_2',
                    front_left: 'HHTM164_Q1_3'
                }
            }
        }, {
            id: 'HHTM165_Q1',
            IG: 'REMOVE',
            IT: '',
            localOnly: true,
            text: 'HHTM165_Q1_TEXT',
            title: '',
            className: 'HHTM165_Q1',
            widget: {
                id: 'HHTM165_Q1_W0',
                type: 'HHBodyDiagramZoom',
                templates: {
                    container: 'BPISF:BackTorsoMostPain'
                },
                height: 375,
                labels: {
                    back: 'HHTM165_Q1_1',
                    back_left: 'HHTM165_Q1_2',
                    back_right: 'HHTM165_Q1_3'
                }
            }
        }, {
            id: 'HHTM166_Q1',
            IG: 'REMOVE',
            IT: '',
            localOnly: true,
            text: 'HHTM166_Q1_TEXT',
            title: '',
            className: 'HHTM166_Q1',
            widget: {
                id: 'HHTM166_Q1_W0',
                type: 'HHBodyDiagramZoom',
                templates: {
                    container: 'BPISF:BackLegsMostPain'
                },
                height: 375,
                labels: {
                    back: 'HHTM166_Q1_1',
                    back_left: 'HHTM166_Q1_2',
                    back_right: 'HHTM166_Q1_3'
                }
            }
        }, {
            id: 'HHTM190_Q1',
            text: 'HHTM190_Q1_TEXT',
            title: '',
            className: 'HHTM190_Q1'
        }, {
            id: 'HHTM200_1_Q1',
            IG: 'LPTM1',
            repeating: true,
            IT: '',
            localOnly: true,
            text: 'HHTM200_1_Q1_TEXT',
            title: '',
            className: 'HHTM200_1_Q1',
            widget: {
                id: 'HHTM200_1_Q1_W0',
                type: 'ScrollableReviewScreen',
                screenFunction: 'HHTrainingModuleLoopFunction',
                templates: {
                    itemsTemplate: 'TwoRows:CustomScreenItems',
                    container: 'TwoRows:CustomReviewScreen'
                },
                //headers: ['HHTM200_1_Q1_ADDITIONAL_INSTRUCTIONS'],
                headers: ['SPACE'],
                editInstructions: 'HHTM200_1_Q1_EDIT_INSTRUCTIONS',
                buttons: [
                    {
                        id: 1,
                        availability: 'checkForRemainingLoops',
                        text: 'HHTM200_ReviewScreen_AddButton',
                        actionFunction: 'addHHTrainingModuleLoop'
                    },
                    {
                        id: 2,
                        availability: 'checkIfRowIsEditable',
                        text: 'HHTM200_ReviewScreen_EditButton',
                        actionFunction: 'editHHTrainingModuleLoop'
                    },
                    {
                        id: 3,
                        availability: 'checkIfRowIsEditable',
                        text: 'HHTM200_ReviewScreen_DeleteButton',
                        actionFunction: 'deleteHHTrainingModuleLoop'
                    }
                ]
            }
        }, {
            id: 'HHTM200_5_Q1',
            IG: 'LPTM1',
            repeating: true,
            IT: '',
            localOnly: true,
            text: 'HHTM200_5_Q1_TEXT',
            title: '',
            className: 'HHTM200_5_Q1',
            widget: {
                id: 'HHTM200_5_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'HHTM200_5_Q1_W0_A0'
                    },
                    {
                        value: '2',
                        text: 'HHTM200_5_Q1_W0_A1'
                    }
                ]
            }
        }, {
            id: 'HHTM200_2_Q1',
            IG: 'LPTM1',
            repeating: true,
            IT: '',
            localOnly: true,
            text: 'HHTM200_2_Q1_TEXT',
            title: '',
            className: 'HHTM200_2_Q1',
            widget: {
                id: 'HHTM200_2_Q1_W0',
                className: 'HHTM200_2_Q1_W0',
                type: 'DateTimePicker',
                modalTitle: 'HHTM200_2_Q1_W0_modalTitle',
                showLabels: false,
                min: Tools.getMinDate,
                max: Tools.getMaxDate
            }
        }, {
            id: 'HHTM200_3_Q1',
            IG: 'LPTM1',
            repeating: true,
            IT: '',
            localOnly: true,
            text: 'HHTM200_3_Q1_TEXT',
            title: '',
            className: 'HHTM200_3_Q1',
            widget: {
                id: 'HHTM200_3_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '1',
                        text: 'HHTM200_3_Q1_W0_A0'
                    },
                    {
                        value: '2',
                        text: 'HHTM200_3_Q1_W0_A1'
                    },
                    {
                        value: '3',
                        text: 'HHTM200_3_Q1_W0_A2'
                    },
                    {
                        value: '4',
                        text: 'HHTM200_3_Q1_W0_A3'
                    }
                ]
            }
        }, {
            id: 'HHTM200_4_Q1',
            IG: 'LPTM1',
            repeating: true,
            IT: '',
            localOnly: true,
            text: 'HHTM200_4_Q1_TEXT',
            title: '',
            className: 'HHTM200_4_Q1',
            widget: {
                id: 'HHTM200_4_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'HHTM200_4_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'HHTM200_4_Q1_W0_A1'
                    }
                ]
            }
        }, {
            id: 'HHTM980_Q1',
            text: 'HHTM980_Q1_TEXT',
            title: '',
            className: 'HHTM980_Q1'
        }, {
            id: 'HHTM990_Q1',
            IG: 'HHTrainingModule',
            IT: 'TRNCNF1L',
            text: 'HHTM990_Q1_TEXT',
            title: '',
            className: 'HHTM990_Q1',
            widget: {
                id: 'HHTM990_Q1_W0',
                type: 'RadioButton',
                answers: [
                    {
                        value: '0',
                        text: 'HHTM990_Q1_W0_A0'
                    },
                    {
                        value: '1',
                        text: 'HHTM990_Q1_W0_A1'
                    }
                ]
            }
        }
    ]
};
