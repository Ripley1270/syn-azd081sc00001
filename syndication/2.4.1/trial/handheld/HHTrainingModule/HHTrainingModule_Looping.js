import BranchHelpers from 'core/branching/branchingHelpers';
import TrainingMedModel from './TrainingMedModel';

/**
 * Created by uadhikari on 12/21/2017.
 */
// //////////////////////////////////////////////////////
// Define Loop Information
// ////////////////////////////////////////////////////
const LoopInfo = {
    IG: 'LPTM1', // Looping IG
    namespace: 'HHTrainingModule',
    ReviewScreen: 'HHTM200_1_Q1',
    StartOfLoop: 'HHTM200_5',
    EndOfLoop: 'HHTM980', // NextScreen After Last Screen,
    DeleteScreen: 'HHTM200_4', // Delete Conf Screen
    Max_Loops: 5, // Max Loops Allowed
    HistoricData: false,
    Questions: ['HHTM200_5_Q1', 'HHTM200_2_Q1', 'HHTM200_3_Q1']// List of Questions to Display
};

function loadLoopScreen (IG, screenID, itemsFunction, params, widget) {
    let currentIGR,
        answersByIGR;
    // if backing out from edit mode, discard edited version as user backed out
    if (LF.Data.Questionnaire.editing) {
        LF.Data.Questionnaire.removeLoopEvent(IG, -1);
    }
    LF.Data.Questionnaire.editing = null;
    LF.Data.Questionnaire.updateIGR(IG);
    // This determines if the an event was started and then abandoned by backing out.
    // In this case the partly answered event is removed.
    currentIGR = LF.Data.Questionnaire.getCurrentIGR(IG);
    answersByIGR = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, currentIGR);
    if (answersByIGR.length > 1) {
        // Removes partly answered event
        LF.Data.Questionnaire.clearLoopScreenFromStack(IG);
        LF.Data.Questionnaire.removeLoopEvent(IG, currentIGR);
        BranchHelpers.navigateToScreen(screenID);
        return Q.promise();
    }
    return Q.Promise((resolve) => {
        itemsFunction(IG).then((items) => {
            resolve(items);
        });
    });
}

LF.Widget.ReviewScreen.HHTrainingModuleLoopFunction = function (widget) {
    return new Q.promise((resolve) => {
        loadLoopScreen(
            LoopInfo.IG,
            LoopInfo.ReviewScreen,
            itemsFunction,
            widget
        )
            .then((items) => {
                resolve(items);
            });
    });
};

function getReviewData (IG, addIGR) {
    return Q.Promise((resolve) => {
        let lastIGR = LF.Data.Questionnaire.getIGR(IG),
            questions = LoopInfo.Questions,
            ansArray = Array(...Array(lastIGR)),
            questionnaire = LF.Data.Questionnaire,
            questionMap = new Map([
                [questions[0], 'medType'],
                [questions[1], 'dateTime'],
                [questions[2], 'amount']
            ]);

        resolve(ansArray
            .map(answer => questions)
            .map((questions, idx) => {
                return questions.reduce((accum, question) => {
                    let [answer] = questionnaire.queryAnswersByQuestionIDAndIGR(question, idx),
                        prop = questionMap.get(question);
                    accum[prop] = answer ? answer.get('response') : null;
                    accum.igr = idx;
                    return accum;
                }, {});
            })
            .map(medAnswer => new TrainingMedModel(medAnswer))
        );
    });
}

function getItemStrings () {
    let stringsToFetch = {
        // addStrings to Fetch
        PAIN_MED: 'HHTM200_5_Q1_W0_A0',
        OTHER_MED: 'HHTM200_5_Q1_W0_A1',
        PILLS: 'PILLS'
    };
    return LF.getStrings(stringsToFetch, $.noop(), { namespace: 'HHTrainingModule' });
}

function itemsFunction (IG) {
    return Q.spread([getItemStrings(), getReviewData(IG, true)], (resStrings, medModels) => {
        return medModels.sort((a, b) => {
            let aDT = moment(a.get('dateTime')),
                bDT = moment(b.get('dateTime'));
            if (aDT.isBefore(bDT)) {
                return 1;
            }
            if (bDT.isBefore(aDT)) {
                return -1;
            }
            return 0;
        }).map((medModel) => {
            let timeFormat = LF.Utilities.getString('MEDLOOP_0100_TIME_FORMAT'),
                igr = medModel.get('igr'),
                qty = medModel.get('amount'),
                type = medModel.get('medType'),
                medDate = medModel.get('dateTime'),
                obj = {},
                medText = resStrings.OTHER_MED,
                text;
            medText = type === '1' ? `${qty} ${resStrings.PILLS} - ${resStrings.PAIN_MED}` : medText;
            text = [medText, timeFormat.replace('{MEDTKN1S}', PDE.MomentUtils.formatDateTime(medDate))];
            obj.id = igr;
            obj.classes = igr ? '' : 'not-selectable';
            obj.text = text;
            return obj;
        });
    });
}

LF.Widget.ReviewScreen.addHHTrainingModuleLoop = function (params) {
    let answers = LF.Data.Questionnaire.queryAnswersByIGAndIGR(LoopInfo.IG, LoopInfo.Max_Loops);
    if (answers.length > 0) {
        LF.Branching.Helpers.navigateToScreen(LoopInfo.EndOfLoop);
    } else {
        LF.Branching.Helpers.navigateToScreen(LoopInfo.StartOfLoop);
    }
};

LF.Widget.ReviewScreen.editHHTrainingModuleLoop = function (params) {
    LF.Widget.ReviewScreen.beginEditLoop(LoopInfo.IG, LoopInfo.StartOfLoop, params);
};

LF.Widget.ReviewScreen.deleteHHTrainingModuleLoop = function (params) {
    /* //This section is to delete the entry instantly; no confirmation screen necessary
     LF.Widget.ReviewScreen.deleteLoop(LoopInfo.IG, LF.Utilities.getReviewScreenItemID(params.item));
     params.question.widget.render();*/
    LF.Widget.ReviewScreen.beginEditLoop(LoopInfo.IG, LoopInfo.DeleteScreen, params);
};


LF.Widget.ValidationFunctions.always = function (answer, params, callback) {
    callback(true);
};

LF.Widget.ReviewScreen.editNotAvailable = function (widget) {
    // Return the opposite of checkIfRowIsEditable
    return Q().then((resolve) => {
        if (widget.activeItem) {
            let IGR = LF.Utilities.getReviewScreenItemID(widget.activeItem);
            return IGR <= 0;
        } else {
            return true;
        }
    });
};

LF.Widget.ReviewScreen.checkIfRowIsEditable = function (widget) {
    return Q().then(() => {
            if (widget.activeItem) {
                let IGR = LF.Utilities.getReviewScreenItemID(widget.activeItem);
                return IGR > 0;
            } else {
                return false;
            }
        }
    );
};

LF.Widget.ReviewScreen.checkIfRowIsSelected = function (widget) {
    return Q().then(() => {
            if (widget.activeItem) {
                let IGR = LF.Utilities.getReviewScreenItemID(widget.activeItem);
                if (IGR > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    );
};
LF.Widget.ReviewScreen.checkForRemainingLoops = function (widget) {
    let maxLoops = LoopInfo.Max_Loops,
        view = LF.router.view(),
        currentIGR = view.getCurrentIGR(LoopInfo.IG);
    return Q(currentIGR <= maxLoops);
};


LF.Widget.ReviewScreen.saveLoop = function (IG) {
    let currentIGR,
        answersByIGR;
    // If the editing was happening, then this is the place where edited answers are
    // commited and saved into Answers collection
    if (LF.Data.Questionnaire.editing) {
        // Remove the original version
        LF.Data.Questionnaire.removeLoopEvent(IG, LF.Data.Questionnaire.editing);
        // change the edited version to original version
        LF.Data.Questionnaire.changeIGR(IG, -1, LF.Data.Questionnaire.editing);
        // sets the flag to null which indicates the editing has been completed
        LF.Data.Questionnaire.editing = null;
    }
    // Update the IGR incase navigating to review screen from edit mode.
    LF.Data.Questionnaire.updateIGR(IG);
    //clears the stack before review screen is put on the stack.
    LF.Utilities.clearLoopScreenFromStack(IG);
    // get the current IGR
    currentIGR = LF.Data.Questionnaire.getCurrentIGR(IG);
    // gets all answers for the current IGR
    answersByIGR = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, currentIGR);
    if (answersByIGR.length) {
        // increments if the current IGR is already completed, so next available IGR becomes current
        LF.Data.Questionnaire.incrementIGR(IG);
    }
};

LF.Widget.ReviewScreen.beginEditLoop = function (IG, startScreen, params) {
    // getting id from the selected item and using it as IGR. This works because
    // when creating review screen item, IGRs were used as item ids which makes
    // it easier and doesn't require any mapping.
    let IGR = LF.Utilities.getReviewScreenItemID(params.item);
    // Preserve the IGR which is being edited
    LF.Data.Questionnaire.editing = IGR;
    // Copy all the Answers and Question Views from IGR which is being
    // edited to placeholder (-1)
    LF.Data.Questionnaire.copyIGR(IG, IGR, -1);
    // set the IGR to current and navigate to screen 3.
    // setting IGR will fetch the correct answer previously recorded.
    LF.Data.Questionnaire.setCurrentIGR(IG, -1);
    BranchHelpers.navigateToScreen(startScreen);
};

LF.Widget.ReviewScreen.deleteLoop = function (IG, IGR) {
    // Removing the event, this utility function removes answers from
    // LF.Data.Questionnaire.Answers and also removes Question views instances
    // from LF.Data.Questionnaire.QuestionViews for IGR to be deleted.
    LF.Data.Questionnaire.removeLoopEvent(IG, IGR);
    // Ordering IGR must be done so all IGR are in sequence. this utility function
    // order igr on all Answer records and also changes the igr property on
    // all question view instances
    LF.Data.Questionnaire.orderIGR(IG, IGR);
    // decrement the igr to reflect the change in the count
    LF.Data.Questionnaire.decrementIGR(IG);
};


