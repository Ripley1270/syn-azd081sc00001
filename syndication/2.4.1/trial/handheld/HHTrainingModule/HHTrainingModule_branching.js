import BodyImageMapStorageHH from './widget/BodyImageMapStorageHH';

LF.Branching.branchFunctions.branchPopScreen = () => {
    return new Q.Promise((resolve) => {
        LF.Data.Questionnaire.screenStack.pop();
        BodyImageMapStorageHH.emptyRecentlySelectedIDs();
        resolve(true);
    });
};

LF.Branching.branchFunctions.equalBranchBackClearHHAnswers = (branchObj, view) => {
    return LF.Branching.branchFunctions.equalBranchBack(branchObj, view)
        .then((result) => {
            if (result) {
                const view = LF.router.view(),
                    IG = 'LPTM1',
                    bodyPortionsToClear = BodyImageMapStorageHH.allBodyPortionText;
                BodyImageMapStorageHH.clearAllSelectBodyPartsForBodyPortion(bodyPortionsToClear);
                BodyImageMapStorageHH.setNoPainSelected(false);

                let igr = view.getIGR(IG);
                while (igr > 1) {
                    view.removeLoopEvent(IG, igr);
                    igr--;
                    view.decrementIGR(IG);
                }
                // actually decreases the loop too far?
                view.incrementIGR(IG);
            }
            return result;
        });
};

/**
 * @file Handheld Training Module Looping .
 * @author <a href="mailto:udit.adhikari@ert.com">Udit Adhikari</a>
 * @version 2.3.0
 */

LF.Branching.branchFunctions.checkLoops = function (branchObj) {
    return new Q.Promise((resolve) => {
        resolve(LF.Data.Questionnaire.getCurrentIGR('LPTM1') === 1);
    });
};

LF.Branching.branchFunctions.HHTrainingModuleLoopCompareFirst = function (branchObj, view) {
    return LF.Branching.branchFunctions.equal(branchObj, view)
        .then((result) => {
            if (!result) {
                return false;
            }
            return LF.Branching.branchFunctions.HHTrainingModuleLoop(branchObj);
        });
};

/**
 * This always branch after doing necessary steps for Loop1 Diary.
 * @param {Object} branchObj - extended branch object from the configuration
 * @returns {Q.Promise<Boolean>}
 */
LF.Branching.branchFunctions.HHTrainingModuleLoop = function (branchObj) {
    return new Q.Promise((resolve) => {
        let currentIGR,
            answersByIGR,
            IG = 'LPTM1';

        // If the editing was happening, then this is the place where edited answers are
        // commited and saved into Answers collection
        if (LF.Data.Questionnaire.editing) {
            // Remove the original version
            LF.Data.Questionnaire.removeLoopEvent(IG, LF.Data.Questionnaire.editing);

            // change the edited version to original verison
            LF.Data.Questionnaire.changeIGR(IG, -1, LF.Data.Questionnaire.editing);

            // sets the flag to null which indicates the editing has been completed
            LF.Data.Questionnaire.editing = null;
        }

        if (LF.Data.Questionnaire.inserting) {
            // frees up the IGR we're inserting to and reorders the others to make space
            LF.Data.Questionnaire.makeIGRAvailable(IG, LF.Data.Questionnaire.targetInsertionIndex);

            // change the IGR from -1 (which it was initialized to for insertion) to the target that we are
            // inserting to
            LF.Data.Questionnaire.changeIGR(IG, -1, LF.Data.Questionnaire.targetInsertionIndex);
            LF.Data.Questionnaire.inserting = false;
        }

        // Update the IGR incase navigating to review screen from edit mode.
        LF.Data.Questionnaire.updateIGR(IG);

        // clears the stack before review screen is put on the stack.
        LF.Data.Questionnaire.clearLoopScreenFromStack(IG);

        // get the surrent IGR
        currentIGR = LF.Data.Questionnaire.getCurrentIGR(IG);

        // gets all answers for the current IGR
        answersByIGR = LF.Data.Questionnaire.queryAnswersByIGR(currentIGR);
        if (answersByIGR.length) {
            // increments if the current IGR is already completed, so next available IGR becomes current
            LF.Data.Questionnaire.incrementIGR(IG);
        }
        resolve(true);
    });
};

LF.Branching.branchFunctions.deleteLoop = function (branchObj, view) {
    return new Q.Promise((resolve) => {
        /*
        Q().then(() => {
            let response,
                questionId = branchObj.branchParams.questionId,
                operand = branchObj.branchParams.operand,
                questionIT = branchObj.branchParams.IT,
                result = false;
            if (branchObj.branchParams.IG) {
                let igr = view.editing || view.getCurrentIGR(branchObj.branchParams.IG);
                if (questionId) {
                    response = view.queryAnswersByQuestionIDAndIGR(questionId, igr)[0];
                } else if (questionIT) {
                    response = view.queryAnswersByITAndIGR(questionIT, igr)[0];
                }
            } else {
                if (questionId) {
                    response = view.queryAnswersByID(questionId)[0];
                } else if (questionIT) {
                    response = view.queryAnswersByIT(questionIT)[0];
                }
            }
            if (response) {
                response = response.get('response');
            }
            return response === branchObj.branchParams.value;
        })*/
        LF.Branching.branchFunctions.equal(branchObj, view)
            .then((result) => {
                // Clear loop if equal rule returns true
                if (result) {
                    let IG = branchObj.branchParams.IG,

                        // getting id from the selected item and using it as IGR. This works because
                        // when creating review screen item, IGRs were used as item ids which makes
                        // it easier and doesn't require any mapping.
                        IGR = LF.Data.Questionnaire.editing;
                    LF.Widget.ReviewScreen.deleteLoop(IG, IGR);
                }

                // Always branch back
                resolve(
                    LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view)
                        .then((result) => {
                            return new Q.Promise((resolve) => {
                                resolve(result);
                            });
                        })
                );
            });
    });
};

LF.Branching.branchFunctions.HHTM_equalBranchBack = function (branchObj, view) {
    branchObj.branchParams.operand = '=';
    return LF.Branching.branchFunctions.compare(branchObj, view)
        .then((result) => {
            if (result) {
                // remove the loop questions
                let answers = view.queryAnswersByID('HHTrainingModule_HHTM200_1_28_Q1')
                    .concat(view.queryAnswersByID('HHTrainingModule_HHTM200_2_27_Q1'))
                    .concat(view.queryAnswersByID('HHTrainingModule_HHTM200_3_29_Q1'))
                    .concat(view.queryAnswersByID('HHTrainingModule_HHTM200_4_30_Q1'));
                view.answers.remove(answers);
                return LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view);
            }
            return false;
        });
};


