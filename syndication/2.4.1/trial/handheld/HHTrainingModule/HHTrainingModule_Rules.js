import { prePopulateQuestion } from 'trial/common_study/utils/prePopulateQuestion';
import { removeITByIG } from 'trial/handheld/STUDY/study-tools';
import PDE from 'PDE_Core/common/PDE';

export default {
    rules: [
        {
            id: 'HHTrainingModuleOpened',
            trigger: 'QUESTIONNAIRE:Open/HHTrainingModule',
            evaluate: true,
            resolve: [{
                action: () => {
                    const view = LF.router.view(),
                        tDayDate = moment(new Date()).subtract(1, 'day').set({ hour: 23, minute: 0 }).toDate();

                    let params1 = {
                            question_id: 'HHTM200_5_Q1',
                            screen_id: 'HHTM200_5',
                            questionnaire_id: 'HHTrainingModule',
                            response: '1',
                            IG: 'LPTM1',
                            IGR: 0,
                            IT: 'DUM1',
                            markAsCompleted: true
                        },
                        params2 = {
                            question_id: 'HHTM200_2_Q1',
                            screen_id: 'HHTM200_2',
                            questionnaire_id: 'HHTrainingModule',
                            response: new Date().ISOLocalTZStamp(),
                            IG: 'LPTM1',
                            IGR: 0,
                            IT: 'DUM2',
                            markAsCompleted: true
                        },
                        params3 = {
                            question_id: 'HHTM200_3_Q1',
                            screen_id: 'HHTM200_3',
                            questionnaire_id: 'HHTrainingModule',
                            response: '3',
                            IG: 'LPTM1',
                            IGR: 0,
                            IT: 'DUM3',
                            markAsCompleted: true
                        },
                        params4 = {
                            question_id: 'HHTM200_5_Q1',
                            screen_id: 'HHTM200_5',
                            questionnaire_id: 'HHTrainingModule',
                            response: '2',
                            IG: 'LPTM1',
                            IGR: 1,
                            IT: 'AUTO_MED',
                            markAsCompleted: true
                        },
                        params5 = {
                            question_id: 'HHTM200_2_Q1',
                            screen_id: 'HHTM200_2',
                            questionnaire_id: 'HHTrainingModule',
                            response: tDayDate.ISOLocalTZStamp(),
                            IG: 'LPTM1',
                            IGR: 1,
                            IT: 'AUTO_DATE',
                            markAsCompleted: true
                        };

                    Q.all([
                        prePopulateQuestion(params1),
                        prePopulateQuestion(params2),
                        prePopulateQuestion(params3),
                        prePopulateQuestion(params4),
                        prePopulateQuestion(params5)
                    ]).then(() => {
                        view.incrementIGR('LPTM1');
                    });
                }
            }]
        },
        {
            id: 'HHTrainingModuleCompleted',
            trigger: 'QUESTIONNAIRE:Completed/HHTrainingModule',
            evaluate: true,
            salience: 90,
            resolve: [{
                action: () => {
                    const qView = LF.router.view().questionnaire;
                    removeITByIG('LPTM1', qView);
                    removeITByIG('REMOVE', qView);

                    PDE.CollectionModelUtils.getCollection('DiaryCompletionCollection')
                        .then((diaryCompletions) => {
                            diaryCompletions.comparator = diary => new Date(diary.get('dateTime'));
                            diaryCompletions.sort();
                            let pDiares = diaryCompletions.where({ diary: 'HHTrainingModule' }),
                                firstPracticeDiary = _.first(pDiares),
                                pdDate = new Date(firstPracticeDiary.get('dateTime'));

                            LF.Data.Questionnaire.addIT({
                                question_id: 'PRCDAT1D',
                                questionnaire_id: 'HHTrainingModule',
                                response: PDE.QuestionnaireUtils.buildStudyWorksDateString(pdDate),
                                IG: 'HHTrainingModule',
                                IGR: 0,
                                IT: 'PRCDAT1D'
                            });
                        })
                        .done();
                }
            }]
        }
    ]
};
