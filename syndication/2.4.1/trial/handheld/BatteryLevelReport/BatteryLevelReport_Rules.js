import * as Battery from './BatteryLevelReport_Tools';

export default {
    rules: [
        {
            id: 'GenerateBatteryLevelReport',
            trigger: 'ALARM:Trigger/AutoDial',
            evaluate: (filter, resume) => resume(!localStorage.getItem('screenshot')),
            salience: 999,
            resolve: [
                {
                    action () {
                        return Battery.generateBatteryDiary();
                    }
                }
            ]
        },
        {
            id: 'BatteryLevelReportSave',
            trigger: 'QUESTIONNAIRE:Completed/BatteryLevelReport',
            evaluate (filter, resume) {
                resume(!localStorage.getItem('screenshot'));
            },
            salience: 999,
            resolve: [
                {
                    action (params) {
                        /*
                         * STYPRD1L and STYDAY1N added from generic study rules
                         */
                        const subject = params.subject(),
                            today = new Date(),
                            follup2Phase = LF.StudyDesign.studyPhase.FOLLOWUP_2;

                        Battery.getDaysAssessmentsAvailable(subject, today)
                            .then((daysAvailable) => {
                                if (daysAvailable) {
                                    LF.Data.Questionnaire.addIT({
                                        question_id: 'DAYAVL1N',
                                        questionnaire_id: 'BatteryLevelReport',
                                        response: daysAvailable,
                                        IG: 'BatteryLevel',
                                        IGR: 0,
                                        IT: 'DAYAVL1N'
                                    });
                                }
                            })
                            .then(() => {
                                let batLevel = LF.Data.Questionnaire.data.dashboard.get('battery_level');
                                if (batLevel) {
                                    LF.Data.Questionnaire.addIT({
                                        question_id: 'BRYLVL1N',
                                        questionnaire_id: 'BatteryLevelReport',
                                        response: String(batLevel),
                                        IG: 'BatteryLevel',
                                        IGR: 0,
                                        IT: 'BRYLVL1N'
                                    });
                                }
                                return Battery.getFirstDayWithinWindow(subject);
                            })
                            .then((windowDay) => {
                                if (windowDay) {
                                    LF.Data.Questionnaire.addIT({
                                        question_id: 'STRDAY1N',
                                        questionnaire_id: 'BatteryLevelReport',
                                        response: windowDay,
                                        IG: 'BatteryLevel',
                                        IGR: 0,
                                        IT: 'STRDAY1N'
                                    });
                                }

                                if (Battery.isPhaseChangeYesterday(subject, follup2Phase)) {
                                    LF.Data.Questionnaire.addIT({
                                        question_id: 'STDEND1B',
                                        questionnaire_id: 'BatteryLevelReport',
                                        response: true,
                                        IG: 'BatteryLevel',
                                        IGR: 0,
                                        IT: 'STDEND1B'
                                    });
                                }
                            });
                    }
                }
            ]
        }
    ]
};
