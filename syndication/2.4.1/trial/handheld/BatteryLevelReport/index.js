import { mergeObjects } from 'core/utilities/languageExtensions';

import BatteryLevelReport from './BatteryLevelReport';
import rules from './BatteryLevelReport_Rules';

let studyDesign = {};
studyDesign = mergeObjects(studyDesign, BatteryLevelReport, rules);

export default { studyDesign };
