export default {
    questionnaires: [
        {
            id: 'BatteryLevelReport',
            SU: 'BatteryLevel',
            displayName: 'DISPLAY_NAME',
            className: 'BatteryLevelReport',
            previousScreen: true,
            affidavit: 'DEFAULT',
            screens: [
                'BatteryLevelReport010'
            ],
            branches: [],
            initialScreen: []
        }
    ],
    screens: [
        {
            id: 'BatteryLevelReport010',
            classname: 'BatteryLevelReport010',
            questions: [
                {
                    id: 'BatteryLevelReport010_Q0',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'BatteryLevelReport010_Q0',
            text: 'BatteryLevelReport010_Q0',
            className: 'BatteryLevelReport010_Q0'
        }
    ]
};
