import BaseCollection from 'core/collections/StorageBase';
import VisitConfirmationModel from 'trial/handheld/STUDY/models/VisitConfirmationModel';

/**
 * A collection of Diary Completion Models
 * @class Visits
 * @extends BaseCollection
 */
export default class VisitConfirmationCollection extends BaseCollection {
    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model() {
        return VisitConfirmationModel;
    }
}
window.LF.Collection.VisitConfirmationCollection = VisitConfirmationCollection;
