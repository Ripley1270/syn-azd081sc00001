import BaseCollection from 'core/collections/StorageBase';
import DiaryCompletionModel from 'trial/handheld/STUDY/models/DiaryCompletionModel';

/**
 * A collection of Diary Completion Models
 * @class Visits
 * @extends BaseCollection
 */
export default class DiaryCompletionCollection extends BaseCollection {
    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model() {
        return DiaryCompletionModel;
    }
}
window.LF.Collection.DiaryCompletionCollection = DiaryCompletionCollection;
