import BaseCollection from 'core/collections/StorageBase';
import BPIModel from 'trial/handheld/STUDY/models/BPIModel';

/**
 * A collection of BPI Models
 * @class Visits
 * @extends BaseCollection
 */
export default class BPICollection extends BaseCollection {
    /**
     * @property {Visit} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Visit}'
     */
    get model() {
        return BPIModel;
    }
}
window.LF.Collection.BPICollection = BPICollection;
