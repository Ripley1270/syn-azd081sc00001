import * as Tools from './handheld-schedule-tools';
import Logger from 'core/Logger';
import PDE from 'PDE_Core/common/PDE';
import { /*getModelWindowNumber, */progressionTimePassed, getRandomizationDate, getWindowParams, getAssessmentDayHelper, isScreeningRescreening } from 'trial/handheld/STUDY/study-tools';

/*
 * Checks that a specified diary (predecessor) has already been completed and that
 * the diary itself hasn't been completed as well
 */
LF.Schedule.schedulingFunctions.availableAfterDiaryOnce = function (schedule, completedQuestionnaires, parentView, context, callback) {
    let completedDiary = completedQuestionnaires.findWhere({ questionnaire_id: schedule.attributes.target.id }),
        predecessor = completedQuestionnaires.findWhere({ questionnaire_id: schedule.attributes.scheduleParams.predecessor });
    if (PDE.verbose) {
        let logger = new Logger('Schedule Functions|availableAfterDiaryOnce'),
            diaryId,
            predecessorId;
        if (completedDiary) {
            diaryId = completedDiary.get('questionnaire_id');
        }
        if (predecessor) {
            predecessorId = predecessor.get('questionnaire_id');
        }
        logger.info(`completedDiary questionnaire_id: ${diaryId}\npredecessor questionnaire_id: ${predecessorId}`)
    }
    callback(!completedDiary && predecessor);
};


LF.Schedule.schedulingFunctions.subjectTrainingModuleAvailability = function (schedule, completedQuestionnaires, parentView, context, callback) {
    PDE.CollectionModelUtils.getCollection('DiaryCompletionCollection')
    .then((diaryCompletions) => {
        diaryCompletions.comparator = diary => new Date(diary.get('dateTime'));
        diaryCompletions.sort();
        let hhtraining = completedQuestionnaires.findWhere({ questionnaire_id: 'HHTrainingModule' }),
            stmDiaries = diaryCompletions.where({ diary: 'STM' }),
            firstSTM,
            firstSTMMoment,
            firstSTMCompletedBeforeToday = false;
        if (stmDiaries.length) {
            firstSTM = _.first(stmDiaries);
            firstSTMMoment = moment(firstSTM.get('dateTime'));
            firstSTMCompletedBeforeToday = !firstSTMMoment.isSame(moment(new Date()), 'day');
        }
        let res = (hhtraining && !firstSTM) || firstSTMCompletedBeforeToday;
        if (PDE.verbose) {
            let logger = new Logger('Schedule Functions|subjectTrainingModuleAvailability');
            logger.info(`hhtraining: ${hhtraining}, firstSTM: ${firstSTM}, firstSTMCompletedBeforeToday: ${firstSTMCompletedBeforeToday}, res: ${res} callback(${res})`);
        }
        callback(res);
    });
};

/*
 * Checks BPID Availability
 *
 * Evaluates if we are in a window for BPID or (screening/re-screening). This also includes checks for maximum completions
 * and that the first diary for the window was not more than 7 days ago.
 * Then checks to see if the SubjectTrainingQuiz has been completed and that the diary has not been completed already today (using
 * standard callback function)
 */
LF.Schedule.schedulingFunctions.checkBPIDAvailability = function (schedule, completedQuestionnaires, parentView, context, callback, isAlarm) {
    const logger = new Logger('Schedule Functions|checkBPIDAvailability'),
        subject = context.subject,
        phase = subject.get('phase');

    if (progressionTimePassed(phase, completedQuestionnaires)) {
        if (PDE.verbose) {
            logger.info('progressionTimePassed(...) returned true. callback(false)');
        }
        callback(false);
        return;
    }

    Tools.windowAvailability('BPID', subject)
        .then((inWindow) => {
            if (PDE.verbose) {
                logger.info(`inWindow: ${inWindow}`);
            }
            // are we in a window (which also checks screening and re-screening and has training quiz been completed?
            if (inWindow && (phase !== LF.StudyDesign.studyPhase.SCREENING ||
                completedQuestionnaires.findWhere({ questionnaire_id: 'SubjectTrainingQuiz' }))) {
                logger.info('BPI is available - checking already completed');
                LF.Schedule.schedulingFunctions.checkRepeatByDateAvailability(schedule, completedQuestionnaires, parentView, context, callback, isAlarm);
            } else {
                logger.info('BPI not available. callback(false)');
                callback(false);
            }
        })
        .catch((error) => {
            logger.error(error);
            callback(false);
        })
        .done();
};

/*
 * Checks DiaryMed (Analgesic) availability.
 *
 * First makes the same check for BPID Availability as the diarymed follows the same schedule.
 * Also checks that at least one BPID has been completed within the current window at least once.
 */
LF.Schedule.schedulingFunctions.checkDiaryMedAvailability = function (schedule, completedQuestionnaires, parentView, context, callback, isAlarm) {
    const logger = new Logger('Schedule Functions|checkDiaryMedAvailability'),
        subject = context.subject,
        phase = subject.get('phase'),
        phaseStart = moment(subject.get('phaseStartDateTZOffset')),
        now = new Date();

    // belt and braces. Shouldn't be available anyway but saves the promise call
    if (progressionTimePassed(phase, completedQuestionnaires)) {
        if (PDE.verbose) {
            logger.info('progressionTimePassed(...) returned true. callback(false)');
        }
        callback(false);
        return;
    }
    Q.spread([
            Tools.windowAvailability('BPID', subject),
            PDE.CollectionModelUtils.getCollection('DiaryCompletionCollection'),
            /*getModelWindowNumber('BPID', subject)*/
            getRandomizationDate(),
            getWindowParams('BPID', subject),
            isScreeningRescreening()
        ],
        (inWindow, dcCollection, /*windowNum, */ randDate, windowParams, isScreeningRescreening) => {
            if (inWindow) {
                logger.info('BPI is available - Checking if has been completed at least once');
                let CurrWindow = dcCollection.where({ diary: 'BPID'/*, window: windowNum */});

                // Filter model that is older than the current window
                CurrWindow = CurrWindow.filter((model) => {
                        let reportTime = moment(model.get('dateTime'));

                        if (isScreeningRescreening) {
                            return reportTime.isSameOrAfter(phaseStart);
                        }
                        else {
                            let assessmentDay = getAssessmentDayHelper(reportTime, randDate);
                            return assessmentDay >= windowParams.start;
                        }
                    });

                let res = CurrWindow.length > 0;

                if (res && isScreeningRescreening) {
                    let oldestModel = CurrWindow.reduce((prev, curr) => moment(prev).isAfter(moment(curr)) ? curr : prev),
                        oldestDt = moment(oldestModel.get('dateTime')),
                        maxWindow = moment(oldestDt).add(7, 'd');

                    res = moment(now).isBefore(maxWindow, 'd');
                }

                if (PDE.verbose) {
                    logger.info(`inWindow is truthy. Will callback(res) where res = ${res}`);
                }
                callback(res);
            } else {
                logger.info('BPI is not available - no diary med. callback(false)');
                callback(false);
            }
        }).done();
};

/*
 * Checks for diaries which are available within a window and where the visitconfirmation has been completed ad-hoc.
 *
 * This applies to FACTP and EQ5D5L diaries.
 *
 * First we check if we are in an availability window for the diary. We then check if the visit confirmation has been
 * completed in the same day.
 * If we are within a window then we check that the diary has not already been completed for the day.
 * If the visit confirmation has been completed today then check that the last completion for FACTP/EQ5D5L was
 * greater than 6 days ago.
 */
LF.Schedule.schedulingFunctions.checkWindowedAvailability = function (schedule, completedQuestionnaires, parentView, context, callback, isAlarm) {
    const diary = schedule.get('target').id,
        logger = new Logger(`Schedule Functions|checkWindowedAvailability (${diary})`),
        subject = context.subject,
        phase = subject.get('phase');

    if (progressionTimePassed(phase, completedQuestionnaires)) {
        if (PDE.verbose) {
            logger.info('progressionTimePassed(...) returned true. callback(false)');
        }
        callback(false);
        return;
    }
    PDE.CollectionModelUtils.getCollection('LastDiaries')
        .then(lastDiaries => Tools.checkSingleDiaryWindowAvailability(subject, diary, lastDiaries))
        .then((available) => callback(available))
        .catch(() => {
            logger.info('Not available. Not within window and visitconfirmation not completed. callback(false)');
            callback(false);
        })
        .done();
};


LF.Schedule.schedulingFunctions.checkAvailableNow = function (schedule, completedQuestionnaires, parentView, context, callback, isAlarm) {
    const logger = new Logger('Schedule Functions|checkAvailableNow'),
        subject = context.subject,
        phase = subject.get('phase');

    if (progressionTimePassed(phase, completedQuestionnaires)) {
        if (PDE.verbose) {
            logger.info('progressionTimePassed(...) returned true. callback(false)');
        }
        callback(false);
        return;
    }
    if (completedQuestionnaires.findWhere({ questionnaire_id: 'SubjectTrainingQuiz' })) {
        if (PDE.verbose) {
            logger.info('Found SubjectTrainingQuiz in completedQuestionnaires');
        }
        Tools.checkDiariesAvailable(subject, completedQuestionnaires)
            .then((value) => {
                if (PDE.verbose) {
                    logger.info(`checkDiariesAvailable returned ${value}. callback(${value})`);
                }
                callback(value);
            })
            .catch((msg) => {
                if (msg) {
                    logger.info(`msg: ${msg}`);
                }
                logger.info('No diaries are available. callback(false)');
                callback(false);
            })
            .done();
    } else {
        if (PDE.verbose) {
            logger.info('Did not find SubjectTrainingQuiz in completedQuestionnaires. callback(false)');
        }
        callback(false); // we have no completed the training module
    }
};


LF.Schedule.schedulingFunctions.BPIDUpcoming = function (schedule, completedQuestionnaires, parentView, context, callback, isAlarm) {
    const logger = new Logger('Schedule Functions|BPIDUpcoming'),
        subject = context.subject,
        phase = subject.get('phase');

    if (progressionTimePassed(phase, completedQuestionnaires)) {
        if (PDE.verbose) {
            logger.info('progressionTimePassed(...) returned true. callback(false)');
        }
        callback(false);
        return;
    }
    if (PDE.verbose) {
        logger.info('delegating callback to Tools.showBPIDIndicator(days = 0)');
    }
    Tools.showBPIDIndicator(0)(subject, callback, logger);
};

LF.Schedule.schedulingFunctions.BPIDDueNow = function (schedule, completedQuestionnaires, parentView, context, callback, isAlarm) {
    const logger = new Logger('Schedule Functions|BPIDDueNow'),
        subject = context.subject,
        phase = subject.get('phase');

    if (progressionTimePassed(phase, completedQuestionnaires)) {
        if (PDE.verbose) {
            logger.info('progressionTimePassed(...) returned true. callback(false)');
        }
        callback(false);
        return;
    }
    if (PDE.verbose) {
        logger.info('delegating callback to Tools.showBPIDIndicator(days = 1)');
    }
    Tools.showBPIDIndicator(1)(subject, callback, logger);
};

/*
    Works on the assumption that:
     - if the BPI is activate then the analgesic diary is also active and therefore this will not be displayed
     - if BPIs for the study window are complete we still have FACTP/EQ5D5L to complete and therefore won't be displayed
     - if all assessments are complete then we can look ahead to the next window (unless we are in a screening/re-screening phase)
 */
LF.Schedule.schedulingFunctions.NextAssessmentIndcator = function (schedule, completedQuestionnaires, parentView, context, callback, isAlarm) {
    const logger = new Logger('Schedule Functions|NextAssessmentIndcator'),
        subject = context.subject,
        currentPhase = subject.get('phase'),
        phase = subject.get('phase');

    if (progressionTimePassed(phase, completedQuestionnaires)) {
        if (PDE.verbose) {
            logger.info('progressionTimePassed(...) returned true. callback(false)');
        }
        callback(false);
        return;
    }
    if (currentPhase < LF.StudyDesign.studyPhase.TREATMENT) {
        if (PDE.verbose) {
            logger.info(`currentPhase: ${currentPhase}, is before TREATMENT. callback(false)`);
        }
        callback(false);
        return;
    }
    Tools.checkDiariesAvailableNextAssessment(subject, completedQuestionnaires)
        .then((value) => {
            if (PDE.verbose) {
                logger.info(`checkDiariesAvailableNextAssessment returned ${value}. callback(!${value})`);
            }
            callback(!value);
        })
        .catch((msg) => {
            if (msg) {
                logger.info(`msg: ${msg}`);
            }
            logger.info('Diaries are available. callback(true)');
            callback(true);
        })
        .done();
};

/**
 * Practice Diary is available after activation.
 * Once it has been completed for the first time, it should not be available for the
 * rest of that day.
 * On following days it is always available.
 */

LF.Schedule.schedulingFunctions.checkHHTrainingModuleAvailability = function (schedule, completedQuestionnaires, parentView, context, callback, isAlarm) {

    PDE.CollectionModelUtils.getCollection('DiaryCompletionCollection')
    .then((diaryCompletions) => {
        diaryCompletions.comparator = diary => new Date(diary.get('dateTime'));
        diaryCompletions.sort();
        let pDiaries = diaryCompletions.where({ diary: 'HHTrainingModule' }),
            firstPracticeDiary,
            firstPDMoment;
        if (pDiaries.length) {
            firstPracticeDiary = _.first(pDiaries);
            firstPDMoment = moment(firstPracticeDiary.get('dateTime'));
            let res = !firstPDMoment.isSame(moment(new Date()), 'day');
            if (PDE.verbose) {
                let logger = new Logger('Schedule Functions|checkHHTrainingModuleAvailability');
                logger.info(`res: ${res}. callback(${res})`);
            }
            callback(res);
        } else {
            if (PDE.verbose) {
                let logger = new Logger('Schedule Functions|checkHHTrainingModuleAvailability');
                logger.info('pDiaries.length is falsy. callback(true)');
            }
            callback(true); // Diary has never been completed.
        }
    });
};
