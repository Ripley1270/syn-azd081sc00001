/* eslint-disable new-cap */
import CurrentSubject from 'core/classes/CurrentSubject';
import { VisitToPhaseMap } from 'trial/handheld/VisitConfimation/VisitConfirmation_Tools';

/**
 * After syncing with NetPRO visit confirmation, initiate a local
 * phase change on the HH.
 */
export default function changePhaseAfterNetPROVC () {
    // Get the visit confirmation collection
    return Q.all([
        PDE.CollectionModelUtils.getCollection('VisitConfirmationCollection'),
        CurrentSubject.getSubject()
    ]).spread((vcCollection, subject) => {
        vcCollection.comparator = model => new Date(model.get('dateTime'));
        vcCollection.sort();
        if (vcCollection.length) {
            // Get the most recent visit confirmed
            let lastVC = _.last(vcCollection.models),
                phase = subject.get('phase'),
                expectedPhase = VisitToPhaseMap().get(lastVC.get('VST1L'));

            // If the subject's phase does not correspond with
            // the phase that it should be given the most recently
            // confirmed visit, update the subject's phase
            if (phase !== expectedPhase) {
                return subject.save({
                    phase: expectedPhase,
                    phaseStartDateTZOffset: lastVC.get('dateTime')
                });
            }
        }
        return Q();
    });
}

ELF.action('changePhaseAfterNetPROVC', changePhaseAfterNetPROVC);

LF.Actions.changePhaseAfterNetPROVC = changePhaseAfterNetPROVC;
