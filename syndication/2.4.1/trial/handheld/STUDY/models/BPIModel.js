import StorageBase from 'core/models/StorageBase';

/**
 * @class A model that stores BPI diary completions
 * @augments StorageBase
 * @example var model = new BPIModel();
 */
export default class BPIModel extends StorageBase {

    constructor (options) {
        super(options);

        /**
         * The model's name
         * @readonly
         * @type String
         */
        this.name = 'BPIModel';
    }

    /**
     * The model's schema used for validation and storage construction.
     * @readonly
     * @type Object
     */
    static get schema () {
        return {
            id: {
                type: Number
            },
            // the date and time the diary has been completed
            dateTime: {
                type: String,
                required: true,
                encrypt: false
            },
            // the window in which the diary has been completed
            window: {
                type: String,
                required: true,
                encrypt: false
            }
        };
    }
}

window.LF.Model.BPIModel = BPIModel;
