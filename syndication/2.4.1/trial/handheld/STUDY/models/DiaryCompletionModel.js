import StorageBase from 'core/models/StorageBase';

/**
 * @class A model that stores diary completions for use in window calculations
 * @augments StorageBase
 * @example var model = new BPIModel();
 */
export default class DiaryCompletionModel extends StorageBase {

    constructor (options) {
        super(options);

        /**
         * The model's name
         * @readonly
         * @type String
         */
        this.name = 'DiaryCompletionModel';
    }

    /**
     * The model's schema used for validation and storage construction.
     * @readonly
     * @type Object
     */
    static get schema () {
        return {
            id: {
                type: Number
            },
            // diary which applies to this model
            diary: {
                type: String,
                required: true,
                encrypt: false
            },
            // the date and time the diary has been completed
            dateTime: {
                type: String,
                required: true,
                encrypt: false
            },
            // the window in which the diary has been completed
            /*window: {
                type: String,
                required: true,
                encrypt: false
            }*/
        };
    }
}

window.LF.Model.DiaryCompletionModel = DiaryCompletionModel;
