import StorageBase from 'core/models/StorageBase';

/**
 * @class A model that stores visit confirmation values for use in window calculations
 * @augments StorageBase
 * @example var model = new VisitConfirmationModel();
 */
export default class VisitConfirmationModel extends StorageBase {

    constructor (options) {
        super(options);

        /**
         * The model's name
         * @readonly
         * @type String
         */
        this.name = 'VisitConfirmationModel';
    }

    /**
     * The model's schema used for validation and storage construction.
     * @readonly
     * @type Object
     */
    static get schema () {
        return {
            id: {
                type: Number
            },
            // The visit type
            VST1L: {
                type: String,
                required: true,
                encrypt: false
            },
            // Randomization date
            RNDMDT1D: {
                type: String,
                encrypt: false
            },
            // Other LogPadApp or NetPRO
            // Not used but maybe useful for error detection
            source: {
                type: String,
                required: true,
                encrypt: false
            },
            // the date and time the diary has been completed
            dateTime: {
                type: String,
                required: true,
                encrypt: false
            }
        };
    }
}

window.LF.Model.VisitConfirmationModel = VisitConfirmationModel;
