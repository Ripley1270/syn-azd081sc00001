export default {
    schedules: [
        {
            id: 'AutoDial',
            target: {
                objectType: 'autodial'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            alarmFunction: 'addAlwaysAlarm',
            alarmParams: {
                id: 1,
                time: {
                    startTimeRange: '02:00',
                    endTimeRange: '03:00'
                },
                repeat: 'daily'
            }
        },
        {
            id: 'AddNewUser_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'New_User'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['admin', 'site']
        },
        {
            id: 'FACTP_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'FACTP'
            },
            scheduleFunction: 'checkWindowedAvailability',
            scheduleRoles: ['subject'],
            scheduleParams: {
                startAvailability: '00:00',
                endAvailability: '00:00'
            },
            phase: ['TREATMENT', 'FOLLOWUP_1', 'FOLLOWUP_2'],
            alarmFunction: 'addAlwaysAlarm',
            alarmParams: {
                id: 4,
                // PDE custom grouping settings
                isGroupedAlarm: true,
                isGroupMaster: false, // If not Group Master, does not show in Settings.
                // End of PDE custom grouping settings
                time: '09:00',
                repeat: 'daily',
                reminders: 3,
                reminderInterval: 15,
                subjectConfig: {
                    minAlarmTime: '04:30',
                    maxAlarmTime: '23:00',
                    alarmRangeInterval: 30,
                    alarmOffSubject: false
                }
            }
        },
        {
            id: 'BPID_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'BPID'
            },
            scheduleFunction: 'checkBPIDAvailability',
            scheduleRoles: ['subject'],
            sharedAcrossRoles: true,
            scheduleParams: {
                startAvailability: '04:00',
                endAvailability: '00:00'
            },
            phase: [
                'SCREENING',
                'RE_SCREENING_1',
                'RE_SCREENING_2',
                'RE_SCREENING_3',
                'RE_SCREENING_4',
                'RE_SCREENING_5',
                'RE_SCREENING_6',
                'RE_SCREENING_7',
                'RE_SCREENING_8',
                'RE_SCREENING_9',
                'RE_SCREENING_10',
                'TREATMENT',
                'FOLLOWUP_1',
                'FOLLOWUP_2'
            ],
            alarmFunction: 'addAlwaysAlarm',
            alarmParams: {
                id: 2,
                // PDE custom grouping settings
                alarmDisplayName: 'DAILY_ALARM_FOR_ASSESSMENTS',
                isGroupedAlarm: true,
                isGroupMaster: true, // Group Master is the one that shows in the Settings, and changing the alarm
                                     // time of this one will change the time of all groupWith alarms.
                groupWith: ['EQ5D5L_Schedule', 'FACTP_Schedule'],
                // End of PDE custom grouping settings
                time: '09:00',
                repeat: 'daily',
                reminders: 3,
                reminderInterval: 15,
                subjectConfig: {
                    minAlarmTime: '04:30',
                    maxAlarmTime: '23:00',
                    alarmRangeInterval: 30,
                    alarmOffSubject: false
                }
            }
        },
        {
            id: 'EQ5D5L_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'EQ5D5L'
            },
            scheduleFunction: 'checkWindowedAvailability',
            scheduleRoles: ['subject'],
            scheduleParams: {
                startAvailability: '00:00',
                endAvailability: '00:00'
            },
            phase: ['TREATMENT', 'FOLLOWUP_1', 'FOLLOWUP_2'],
            alarmFunction: 'addAlwaysAlarm',
            alarmParams: {
                id: 3,
                // PDE custom grouping settings
                isGroupedAlarm: true,
                isGroupMaster: false, // If not Group Master, does not show in Settings.
                // End of PDE custom grouping settings
                time: '09:00',
                repeat: 'daily',
                reminders: 3,
                reminderInterval: 15,
                subjectConfig: {
                    minAlarmTime: '04:30',
                    maxAlarmTime: '23:00',
                    alarmRangeInterval: 30,
                    alarmOffSubject: false
                }
            }
        },
        {
            id: 'DiaryMed_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'DiaryMed'
            },
            scheduleFunction: 'checkDiaryMedAvailability',
            scheduleRoles: ['subject'],
            phase: [
                'SCREENING',
                'RE_SCREENING_1',
                'RE_SCREENING_2',
                'RE_SCREENING_3',
                'RE_SCREENING_4',
                'RE_SCREENING_5',
                'RE_SCREENING_6',
                'RE_SCREENING_7',
                'RE_SCREENING_8',
                'RE_SCREENING_9',
                'RE_SCREENING_10',
                'TREATMENT',
                'FOLLOWUP_1',
                'FOLLOWUP_2'
            ]
        },
        {
            id: 'SubjectTrainingQuiz_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'SubjectTrainingQuiz'
            },
            scheduleFunction: 'availableAfterDiaryOnce',
            scheduleRoles: ['subject'],
            phase: ['SCREENING'],
            scheduleParams: {
                predecessor: 'STM'
            }
        },
        {
            id: 'HHTrainingModule_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'HHTrainingModule'
            },
            scheduleFunction: 'checkHHTrainingModuleAvailability',
            scheduleRoles: ['subject'],
            phase: [
                'SCREENING',
                'RE_SCREENING_1',
                'RE_SCREENING_2',
                'RE_SCREENING_3',
                'RE_SCREENING_4',
                'RE_SCREENING_5',
                'RE_SCREENING_6',
                'RE_SCREENING_7',
                'RE_SCREENING_8',
                'RE_SCREENING_9',
                'RE_SCREENING_10',
                'TREATMENT',
                'FOLLOWUP_1',
                'FOLLOWUP_2'
            ]
        },
        {
            id: 'SubjectTrainingModule_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'STM'
            },
            scheduleFunction: 'subjectTrainingModuleAvailability',
            scheduleRoles: ['subject'],
            scheduleParams: {
                predecessor: 'HHTrainingModule'
            },
            phase: [
                'SCREENING',
                'RE_SCREENING_1',
                'RE_SCREENING_2',
                'RE_SCREENING_3',
                'RE_SCREENING_4',
                'RE_SCREENING_5',
                'RE_SCREENING_6',
                'RE_SCREENING_7',
                'RE_SCREENING_8',
                'RE_SCREENING_9',
                'RE_SCREENING_10',
                'TREATMENT',
                'FOLLOWUP_1',
                'FOLLOWUP_2'
            ]
        },
        {
            id: 'VisitConfirmation_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'VisitConfirmation'
            },
            scheduleFunction: 'checkRepeatByDateAvailability',
            scheduleRoles: ['site'],
            phase: [
                'SCREENING',
                'RE_SCREENING_1',
                'RE_SCREENING_2',
                'RE_SCREENING_3',
                'RE_SCREENING_4',
                'RE_SCREENING_5',
                'RE_SCREENING_6',
                'RE_SCREENING_7',
                'RE_SCREENING_8',
                'RE_SCREENING_9',
                'RE_SCREENING_10',
                'TREATMENT',
                'FOLLOWUP_1',
                'FOLLOWUP_2'
            ],
            scheduleParams: {
                startAvailability: '00:00',
                endAvailability: '00:00'
            }
        },
        {
            id: 'NextAssessmentIndicator_Schedule',
            target: {
                objectType: 'indicator',
                id: 'NextAssessment_Indicator',
                showOnLogin: true
            },
            scheduleFunction: 'NextAssessmentIndcator',
            scheduleRoles: ['subject', 'site'],
            phase: [
                'SCREENING',
                'RE_SCREENING_1',
                'RE_SCREENING_2',
                'RE_SCREENING_3',
                'RE_SCREENING_4',
                'RE_SCREENING_5',
                'RE_SCREENING_6',
                'RE_SCREENING_7',
                'RE_SCREENING_8',
                'RE_SCREENING_9',
                'RE_SCREENING_10',
                'TREATMENT',
                'FOLLOWUP_1',
                'FOLLOWUP_2'
            ]
        }, {
            id: 'BPIDUpcoming_Schedule',
            target: {
                objectType: 'indicator',
                id: 'BPIDUpcoming_Indicator',
                showOnLogin: true
            },
            scheduleFunction: 'BPIDUpcoming',
            scheduleRoles: ['subject', 'site'],
            phase: [
                'SCREENING',
                'RE_SCREENING_1',
                'RE_SCREENING_2',
                'RE_SCREENING_3',
                'RE_SCREENING_4',
                'RE_SCREENING_5',
                'RE_SCREENING_6',
                'RE_SCREENING_7',
                'RE_SCREENING_8',
                'RE_SCREENING_9',
                'RE_SCREENING_10',
                'TREATMENT',
                'FOLLOWUP_1',
                'FOLLOWUP_2'
            ]
        }, {
            id: 'BPIDDueNow_Schedule',
            target: {
                objectType: 'indicator',
                id: 'BPIDDueNow_Indicator',
                showOnLogin: true
            },
            scheduleFunction: 'BPIDDueNow',
            scheduleRoles: ['subject', 'site'],
            phase: [
                'SCREENING',
                'RE_SCREENING_1',
                'RE_SCREENING_2',
                'RE_SCREENING_3',
                'RE_SCREENING_4',
                'RE_SCREENING_5',
                'RE_SCREENING_6',
                'RE_SCREENING_7',
                'RE_SCREENING_8',
                'RE_SCREENING_9',
                'RE_SCREENING_10',
                'TREATMENT',
                'FOLLOWUP_1',
                'FOLLOWUP_2'
            ]
        }, {
            id: 'AssessmentNowIndicator_Schedule',
            target: {
                objectType: 'indicator',
                id: 'AssessmentNow_Indicator',
                showOnLogin: true
            },
            scheduleFunction: 'checkAvailableNow',
            scheduleRoles: ['subject', 'site'],
            phase: [
                'SCREENING',
                'RE_SCREENING_1',
                'RE_SCREENING_2',
                'RE_SCREENING_3',
                'RE_SCREENING_4',
                'RE_SCREENING_5',
                'RE_SCREENING_6',
                'RE_SCREENING_7',
                'RE_SCREENING_8',
                'RE_SCREENING_9',
                'RE_SCREENING_10',
                'TREATMENT',
                'FOLLOWUP_1',
                'FOLLOWUP_2'
            ]
        }
    ]
};
