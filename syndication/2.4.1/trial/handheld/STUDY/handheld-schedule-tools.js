import { getLastConfirmedVisit, getWindowParams, dateDiffInDays, getAssessmentDayHelper, getRandomizationDate } from './study-tools';
import PDE from 'PDE_Core/common/PDE';
import Logger from 'core/Logger';
import { VST1L_PROGRESSION } from 'trial/handheld/VisitConfimation/VisitConfirmation_Tools';

export const checkCompletedToday = (diary, lastDiaries) => {
    let lastDiary = lastDiaries.findWhere({ questionnaire_id: diary });
    if (lastDiary) {
        let completedDt = moment(lastDiary.get('lastStartedDate')),
            now = moment(new Date()),
            res = completedDt.isSame(now, 'day');
        if (PDE.verbose) {
            let logger = new Logger('Schedule Tools|checkCompletedToday');
            logger.info(`diary: ${diary}, res: ${res}`);
        }
        return res;
    }
    if (PDE.verbose) {
        let logger = new Logger('Schedule Tools|checkCompletedToday');
        logger.info(`No lastDiary found for ${diary}. return false`);
    }
    return false;
};

const checkCompletionBeforeTodayOverSixDays = (diary, lastDiaries) => {
    let lastDiary = lastDiaries.findWhere({ questionnaire_id: diary }),
        now = new Date(),
        todayStart = moment(now).startOf('day');
    if (lastDiary) {
        let completedDt = moment(lastDiary.get('lastStartedDate')),
            sixDaysAgo = moment(todayStart).subtract(6, 'days');
            //we check if last completion is over six days ago
            //we don't want to include today's completion. We are
            //checking if the schedule says it could be open at this stage.
        return completedDt.isBefore(sixDaysAgo) || completedDt.isAfter(todayStart);
    }
    return true;
};

// Check if diary is completed with in a given window
export const checkDiaryCompletedInWindow = (windowParams, diaryID, lastDiaries, randDate) => {
    let lastDiary = lastDiaries.findWhere({ questionnaire_id: diaryID });

    if (lastDiary) {
        let completedDt = moment(lastDiary.get('lastStartedDate')),
            diaryAssessmentDay = getAssessmentDayHelper(completedDt, randDate);

        return diaryAssessmentDay >= windowParams.start;
    }

    return false;
};

/*
 * Makes a number of checks which the window diaries FACTP & EQ5D5L require in order to pass.
 *
 * By default this also checks if the FACTP & EQ5D5L have been completed today because they are available
 * once and this tests max completions as well.
 *
 * @param {Subject} subject - the subject model
 * @param {String} diary - the diary being evaluated
 * @param {Collection} - backbone collection of last diaries
 * @return {Q.promise} - boolean for diary availability
 */
export const checkSingleDiaryWindowAvailability = (subject, diary, lastDiaries) => {
    let phase = subject.get('phase');
    return Q.all([
        windowAvailability(diary, subject),
        getLastConfirmedVisit(subject),
        getRandomizationDate()
    ])
        .then(([inWindow, lastVisitType, randDate]) => {
            if (inWindow) {
                let predecessorDiary;

                switch (diary) {
                    case 'FACTP':
                        predecessorDiary = 'DiaryMed';
                        break;
                    case 'EQ5D5L':
                        predecessorDiary = 'FACTP';
                        break;
                }

                if (predecessorDiary) {
                    // Get predecessor window
                    return getWindowParams(predecessorDiary, subject).then((predecessorWindow) => {
                        // Check if predecessor is completed
                        return checkDiaryCompletedInWindow(predecessorWindow, predecessorDiary, lastDiaries, randDate);
                    });
                }
            }
            // Checks is diary has been initiated from Visit Confirmation
            // being completed and that diary hasn't just been completed in a window
            if (checkFACTPAdHocAvailability(phase, lastVisitType, lastDiaries)) {
                return diary === 'EQ5D5L' ? checkCompletedToday('FACTP', lastDiaries) : true;
            }
            // don't bother checking anything else just exit
            return Q.reject();
        })
        // is available and not completed today?
        .then(available => available && !checkCompletedToday(diary, lastDiaries));
};


/*
 * The FACTP & EQ5D5L could also be available if the visit confirmation
 * has been completed today.
 * We also check to make sure we are at least past the screening/re-screening phases
 * and that the last confirmed type was not 'progression' to allow this.
 */
export const checkFACTPAdHocAvailability = (phase, lastVisitType, lastDiaries) => {
    if (checkCompletedToday('VisitConfirmation', lastDiaries) &&
        phase >= LF.StudyDesign.studyPhase.TREATMENT &&
        lastVisitType !== VST1L_PROGRESSION) {
        return checkCompletionBeforeTodayOverSixDays('FACTP', lastDiaries);
    }
    return false;
};


const evaluateWindowParams = (Collection, diary, windowParams, subject, randDate) => {
    let CurrWindow = Collection.where({ diary/*, window: String(windowParams.window)*/ }),
        numCompleted,
        maxCompletions = windowParams.maxCompletions,
        maxDays = windowParams.maxDays,
        startDay = windowParams.start,
        oldestModel,
        oldestDt,
        now = new Date();
    if (PDE.verbose) {
        let logger = new Logger(`Schedule Tools|evaluateWindowParams (${diary})`);
        logger.info(`numCompleted: ${numCompleted}, maxCompletions: ${maxCompletions}, maxDays: ${maxDays}`);
    }

    // Filter model that is older than the current window
    CurrWindow = CurrWindow.filter((model) => {
        let reportTime = moment(model.get('dateTime')),
            assessmentDay = getAssessmentDayHelper(reportTime, randDate);

        return assessmentDay >= startDay;
    });

    numCompleted = CurrWindow.length;

    if (numCompleted) {
        oldestModel = CurrWindow.reduce((prev, curr) => moment(prev).isAfter(moment(curr)) ? curr : prev);
        oldestDt = moment(oldestModel.get('dateTime'));
    }

    // less than maxCompletions for window and not more that the maxDays apart
    // [ONEECOA-106343] Replaced moment.diff() with dateDiffInDays(). moment.diff() requires a full
    // 24 hour difference to count as 1 day. If the start time is 23:00 and end time is next day
    // at 1:00, moment.diff() returns 0.
    let res = numCompleted < maxCompletions && (!oldestDt || dateDiffInDays(oldestDt, now, true) < maxDays);
    if (PDE.verbose) {
        let logger = new Logger(`Schedule Tools|evaluateWindowParams (${diary})`);
        logger.info(`oldestDt: ${oldestDt}, res: ${res}, return ${res}`);
    }
    return res ? windowParams : false;
};

/*
   Checks availability of diaries which can be completed within the windows
   or within one of the screening phases.
   The sequence cannot be repeated within screening or the windows/phase.
   @param {Subject} the Subject Model
   @return {Q.promise} Either true of false for the window being available
*/
export const windowAvailability = (diary, subject) => {
    const logger = new Logger('windowAvailability'),
        now = new Date();
    return Q.all([
        PDE.CollectionModelUtils.getCollection('DiaryCompletionCollection'),
        getWindowParams(diary, subject),
        getRandomizationDate()
    ])
    .then(([Collection, windowParams, randDate]) => {

        let res = windowParams && Collection ? evaluateWindowParams(Collection, diary, windowParams, subject, randDate) : false;
        if (PDE.verbose) {
            let logger = new Logger(`Schedule Tools|windowAvailability ${diary}`);
            logger.info(`res: ${res}, return ${res}`);
        }
        return res;
    })
    .catch((error) => {
        logger.warn('Checks have failed - Error has been thrown');
        return false;
    });
};


/*
 * Have lifted this from the checkRepeatByDateAvailability function to simplify checking
 * for a one off BPID availability
 */
function checkBPIDAvailableNow () {
    let nowUtcMillis = new Date().getTime(),
        startTimeUtc = LF.Utilities.parseTime('04:00', true),
        endTime = LF.Utilities.parseTime('00:00', true),
        endTimeUtc;

    if (endTime <= startTimeUtc) {
        let correctedEndDate = new Date(endTime);
        correctedEndDate.setDate(correctedEndDate.getDate() + 1);
        endTimeUtc = correctedEndDate.getTime();
    } else {
        endTimeUtc = endTime;
    }

    return nowUtcMillis >= startTimeUtc && nowUtcMillis < endTimeUtc;
}

/*
 * Checks if any of the diaries are available currently.
 *
 * For BPID if the value is available even if we have completed it today then the DiaryMed will
 * be available.
 *
 * For FACTP & EQ5D5L the checkSingleDiaryWindowAvailability checks for us if they have been completed today
 * or not for us (because they only have one per window/day)
 *
 * @param {Subject}
 * @param {Collection} - backbone collection of last diaries
 * @return {Q.Promise} - true or false
 */
export const checkDiariesAvailable = (subject, lastDiaries) => {
    return Q.allSettled([
        windowAvailability('BPID', subject), // we check BPI as DiaryMed follows same schedule
        checkSingleDiaryWindowAvailability(subject, 'FACTP', lastDiaries),
        checkSingleDiaryWindowAvailability(subject, 'EQ5D5L', lastDiaries)
    ])
    .then(([bpid, factp, eq5d5l]) => {
        // FACTP & EQ5D5L have single completions so if they are within window they are still available
        if (factp.value || eq5d5l.value) {
            return true;
        }
        // If the BPID is available we'll check if one has been completed today (then analgesic will be too)
        // If not then we will check if the BPID is available to be complete (within the available times)
        if (bpid.value) {
            return checkCompletedToday('BPID', lastDiaries) || checkBPIDAvailableNow();
        }
        return false;
    });
};

/*
 * Checks if any of the diaries are available currently, except for Analgesic Diary.
 * Used for determining whether the Group Alarm should sound.
 *
 * @param {Subject}
 * @param {Collection} - backbone collection of last diaries
 * @return {Q.Promise} - true or false
 */
export const checkGroupAlarmDiariesAvailable = (subject, lastDiaries) => {
    return Q.allSettled([
        windowAvailability('BPID', subject),
        checkSingleDiaryWindowAvailability(subject, 'FACTP', lastDiaries),
        checkSingleDiaryWindowAvailability(subject, 'EQ5D5L', lastDiaries)
    ])
    .then(([bpid, factp, eq5d5l]) => {
        // FACTP & EQ5D5L have single completions so if they are within window they are still available
        if (factp.value || eq5d5l.value) {
            return true;
        }
        if (bpid.value) {
            return !checkCompletedToday('BPID', lastDiaries) && checkBPIDAvailableNow();
        }
        return false;
    });
};

/*
 * Checks if any of the diaries are available currently
 *      OR if the BPI will be available later today
 *
 * For BPID if the value is available even if we have completed it today then the DiaryMed will
 * be available.
 *
 * For FACTP & EQ5D5L the checkSingleDiaryWindowAvailability checks for us if they have been completed today
 * or not for us (because they only have one per window/day)
 *
 * @param {Subject}
 * @param {Collection} - backbone collection of last diaries
 * @return {Q.Promise} - true or false
 */
export const checkDiariesAvailableNextAssessment = (subject, lastDiaries) => {
    return Q.allSettled([
            windowAvailability('BPID', subject), // we check BPI as DiaryMed follows same schedule
            checkSingleDiaryWindowAvailability(subject, 'FACTP', lastDiaries),
            checkSingleDiaryWindowAvailability(subject, 'EQ5D5L', lastDiaries)
        ])
            .then(([bpid, factp, eq5d5l]) => {
            // FACTP & EQ5D5L have single completions so if they are within window they are still available
            if (factp.value || eq5d5l.value) {
                return true;
            }
            // if BPI is available now or later today, don't show NextAssessmentIndicator
            if (bpid.value) {
                return true;
            }
            return false;
        });
};

/*
 * Returns a function which evaluates is the BPID indicator should be shown for either
 * upcoming or due now based on which day it is due.
 */
export const showBPIDIndicator = (days) => {
    return (subject, callback, logger = new Logger('showBPIDIndicator')) => {
        if (!subject || !callback) {
            throw new Error('Invalid params');
        }
        return Q.spread([
            windowAvailability('BPID', subject),
            PDE.CollectionModelUtils.getCollection('LastDiaries')
        ], (available, lastDiaries) => {
            // check is BPI is available today and has not been completed
            if (available && lastDiaries.findWhere({ questionnaire_id: 'SubjectTrainingQuiz' })) {

                let BPIDDIary = lastDiaries.findWhere({ questionnaire_id: 'BPID' });
                if (BPIDDIary && moment(BPIDDIary.get('lastStartedDate')).isSame(new Date(), 'day')) {
                    return Q.reject();
                }
                if (PDE.verbose) {
                    logger.info('BPID is available today and SubjectTrainingQuiz has been completed. return true');
                }
            } else {
                if (PDE.verbose) {
                    logger.info(`BPID available: ${available}, ${available ? 'STQ not complete.' : ''}`);
                    logger.info('return Q.reject()');
                }
                return Q.reject();
            }
            return true;
        })
        .then(() => PDE.CollectionModelUtils.getCollection('SubjectAlarms'))
        .then((alarms) => {
            if (alarms) {
                let BPIDAlarm = _.filter(alarms.models, function (model) {
                        return model.get('schedule_id') == 'BPID_Schedule';
                    }),
                    alarmDT = new Date();

                if (BPIDAlarm.length > 0) {
                    let alarmTime = BPIDAlarm[0].attributes.time,
                        timeArray = alarmTime.split(":");
                    alarmDT = moment(new Date(alarmDT.setHours(timeArray[0], timeArray[1], 0, 0)));
                } else {
                    // use 9am default alarm time
                    alarmDT = moment(new Date(alarmDT.setHours(9,0,0,0)));
                }
                logger.info(`BPI alarm time ${alarmDT.format('D MMM YYYY, h:mm:ss a')}`);

                if (days == 1) { // BPIDDueNow
                    callback(moment(new Date()).isSameOrAfter(alarmDT));
                }
                else { // BPIDUpcoming
                    callback(moment(new Date()).isBefore(alarmDT));
                }

            } else {
                if (PDE.verbose) {
                    logger.info('showBPIDIndicator callback(false)');
                }
                callback(false);
            }
        })
        .catch(() => {
            logger.info('showBPIDIndicator - BPI not available. callback(false)');
            callback(false);
        });
    };
};

