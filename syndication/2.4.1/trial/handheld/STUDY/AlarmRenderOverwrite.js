import AlarmTimeView from 'logpad/views/AlarmTimeView';
import logger from 'core/Logger';

/**
 * Custom PDE overwrite of AlarmTimeView render function.
 * Adds features to support custom display names and grouped alarms.
 *
 * Grouped Alarms description of functionality:
 * Grouped Alarms are alarms that are grouped under 1 'Update alarms' settings item.
 * They will always have the same time, but can have different reminders and reminderInterval values.
 * For each Group, there should be one Group Master, defined by alarmParams.isGroupMaster: true.
 * The Group Master must have an array groupWith: ['ScheduleA', 'ScheduleB', etc] of schedule ids
 * with which to group it in the grouped alarm.
 * Group Master also needs an alarmDisplayName parameter, a STUDY string resource to display
 * on the Update Alarms item.
 *
 * All grouped alarms use addAlwaysAlarm, and then need a rule that triggers of any of
 * the grouped schedules, including group master. In this study, see GroupAlarm_AlarmTrigger.
 * There needs to be a way to determine whether any of the grouped schedules is currently available.
 * This is evaluated in the rule. In this study, we use checkDiariesAvailable.
 *
 * This AlarmRenderOverwrite.js file contains overwrites for the AlarmTimeView.prototype render and respond methods.
 * The render overwrite prevents schedules that are not group masters from displaying in the view.
 * The respond overwrite ensures that the grouped schedules' alarms are set, together with the master.
 * @returns {AlarmTimeView}
 */
AlarmTimeView.prototype.render = function () {
    let alarmPickerElement,
        alarmLbl,
        questionnaireModel = LF.StudyDesign.questionnaires.where({ id: this.model.get('target').id })[0];

    // this.templateStrings.diaryName.key = questionnaireModel.get('displayName');
    if (this.model.attributes.alarmParams.isGroupedAlarm) {
        if (!this.model.attributes.alarmParams.isGroupMaster) {
            // Don't render non Group Masters
            return;
        }
        this.templateStrings.diaryName.namespace = 'STUDY';
    } else {
        this.templateStrings.diaryName.namespace = questionnaireModel.get('id');
    }

    // PDE Custom modified for custom labels
    if (this.model.attributes.alarmParams.alarmDisplayName) {
        this.templateStrings.diaryName.key = this.model.attributes.alarmParams.alarmDisplayName;
    } else {
        this.templateStrings.diaryName.key = questionnaireModel.get('displayName');
    }
    // End PDE Custom

    LF.getStrings(this.templateStrings)
        .then((strings) => {
            this.config.minTime = this.subjectConfig.minAlarmTime;
            this.config.maxTime = this.subjectConfig.maxAlarmTime;

            this.config.overrideTitleTimeDialogLabel = strings.setTimeLblTxt;
            this.config.overrideSetTimeButtonLabel = strings.setTimeBtnTxt;

            if (this.config.useClearButton === true) {
                this.config.overrideClearButton = strings.clearBtnTxt;
            }

            this.alarmOffLbl = strings.alarmOffLabel;
            alarmLbl = this.alarmTime === '' ? strings.alarmOffLabel : this.alarmTime;

            alarmPickerElement = LF.Resources.Templates.display('DEFAULT:AlarmPicker', {
                id: `alarm${this.model.get('id')}`,
                alarmTime: alarmLbl,
                diaryName: strings.diaryName,
                configuration: _.escape(JSON.stringify(this.config))
            });

            this.$el.attr('data-icon', 'false');
            this.$el.html(alarmPickerElement);
            this.$('input').val(this.alarmTime);
            this.delegateEvents();
            this.$el.trigger('create');

            this.inputTime = this.$('input.alarm-input');
            this.inputTime.datebox(this.config);

            this.$alarmDisplayElem = this.$('span.direction-right > time');

            this.$iconContainer = this.$('span.direction-right > i');
            this.bellIconClass = 'fa fa-bell-o';

            if (this.alarmTime !== '') {
                this.$iconContainer.addClass(this.bellIconClass);
            }
        })
        .catch(err => logger.error('render Exception', err))
        .done();

    return this;
};

AlarmTimeView.prototype.respond = function () {

    let alarmInputVal = this.$('input').val(),
        bellIcon = this.bellIconClass,
        $alarmDisplayElem = this.$alarmDisplayElem,
        triggerAlarmChanged = (newAlarmTime) => {

            // Custom PDE Group Alarm functionality
            if (this.model.attributes.alarmParams.isGroupMaster) {
                // Group Master has groupWith schedule ids
                let groupWith = this.model.attributes.alarmParams.groupWith;
                if (!groupWith) {
                    throw new Error(`groupWith not defined for Group Master schedule ${this.model.get('id')}`);
                } else if (!groupWith.length) {
                    throw new Error(`groupWith is empty for Group Master schedule ${this.model.get('id')}`);
                } else if (groupWith.includes(this.model.get('id'))) {
                    throw new Error(`groupWith should not contain the schedule id of the Group Master (${this.model.get('id')})`);
                }
                // Get the schedules that are grouped with this Group Master
                let schedules = LF.content.get('schedules'),
                    groupedSchedules = schedules.filter((schedule) => {
                        return groupWith.includes(schedule.get('id'));
                    });
                if (!groupedSchedules || !groupedSchedules.length) {
                    throw new Error (`Error loading grouped schedules. Check their spelling in groupWith.`);
                }
                groupedSchedules.forEach((schedule) => {
                    this.$el.trigger('alarmChanged', {
                        id: schedule.get('alarmParams').id,
                        schedule,
                        time: newAlarmTime
                    })
                });
            }

            // Non-group alarm (standard code)
            this.$el.trigger('alarmChanged', {
                id: this.model.get('alarmParams').id,
                schedule: this.model,
                time: newAlarmTime
            });
        };

    if (alarmInputVal !== this.alarmTime) {
        this.alarmTime = alarmInputVal;

        this.displayAlarmValue(alarmInputVal, $alarmDisplayElem, bellIcon)
            .then(triggerAlarmChanged)
            .catch(err => logger.warn('An error occured', err))
            .done();
    }

};
