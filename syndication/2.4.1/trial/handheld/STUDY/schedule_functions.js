import * as Tools from './handheld-schedule-tools';
import Logger from 'core/Logger';

LF.Schedule.schedulingFunctions.availableOnce = function (schedule, completedQuestionnaires, parentView, context, callback) {
    let completedDiary = completedQuestionnaires.findWhere({ questionnaire_id: schedule.attributes.target.id });
    callback(!completedDiary);
};

LF.Schedule.schedulingFunctions.checkBPIDAvailability = function (schedule, completedQuestionnaires, parentView, context, callback, isAlarm) {
    const logger = new Logger('checkBPIDAvailability'),
        subject = context.subject;
    Tools.BPISFAvailability(subject)
        .then(() => {
            logger.info('PASSED BPI AVAILABILITY - CHECKING REPEAT');
            LF.Schedule.schedulingFunctions.checkRepeatByDateAvailability(schedule, completedQuestionnaires, parentView, context, callback, isAlarm);
        })
        .catch(() => {
            logger.info('NOT AVAILABLE');
            callback(false);
        });
};
