import QuestionnaireCompletionView from 'core/views/QuestionnaireCompletionView';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import * as Tools from './study-tools';
import { checkSingleDiaryWindowAvailability } from './handheld-schedule-tools';
import { MessageRepo } from 'core/Notify';
import DiaryCompletionModel from 'trial/handheld/STUDY/models/DiaryCompletionModel';
import DiaryCompletionCollection from 'trial/handheld/STUDY/collections/DiaryCompletionCollection';
import PDE from 'PDE_Core/common/PDE';
import CurrentContext from 'core/CurrentContext';
import Logger from 'core/Logger';
import { checkCompletedToday, checkGroupAlarmDiariesAvailable } from 'trial/handheld/STUDY/handheld-schedule-tools';

export default {
    rules: [
        /**
         * Add protocol to all forms
         */
        {
            id: 'SendProtocol',
            trigger: [
                'QUESTIONNAIRE:Completed'
            ],
            evaluate () {
                let view = LF.router.view();
                if (view instanceof QuestionnaireCompletionView) {
                    view = view.questionnaire;
                }
                return Q(Boolean(view.subject));
            },
            resolve: [
                {
                    action: () => {
                        let protocol = '1',
                            view = LF.router.view();
                        view = view instanceof BaseQuestionnaireView ? view : view.questionnaire;
                        if (!view) {
                            view = LF.Data.Questionnaire; // for battery level which can occur anywhere
                        }
                        // Send protocol
                        view.addIT({
                            question_id: 'Protocol',
                            questionnaire_id: view.id,
                            response: protocol,
                            IG: 'Protocol',
                            IGR: 0,
                            IT: 'Protocol'
                        });
                    }
                }
            ]
        },
        {
            id: 'SendStandardStudyDays',
            trigger: [
                'QUESTIONNAIRE:Completed/FACTP',
                'QUESTIONNAIRE:Completed/EQ5D5L',
                'QUESTIONNAIRE:Completed/BPID',
                'QUESTIONNAIRE:Completed/DiaryMed',
                'QUESTIONNAIRE:Completed/BatteryLevel'
            ],
            evaluate: true,
            resolve: [{
                action: (params) => {

                    let IG = params.questionnaire,
                        questionnaire = params.questionnaire;
                    if (IG === 'BPID') {
                        IG = 'BPISFD';
                    }

                    Tools.getRandomizationDate()
                        .then((randomization) => {
                            let today = new Date(),
                                studyDay = Tools.calculateStudyDay(today, randomization);

                            LF.Data.Questionnaire.addIT({
                                question_id: 'STYPRD1L',
                                questionnaire_id: questionnaire,
                                response: Tools.getStudyPeriod(params.subject.get('phase')),
                                IG,
                                IGR: 0,
                                IT: 'STYPRD1L'
                            });

                            if (studyDay) {
                                LF.Data.Questionnaire.addIT({
                                    question_id: 'STYDAY1N',
                                    questionnaire_id: questionnaire,
                                    response: studyDay,
                                    IG,
                                    IGR: 0,
                                    IT: 'STYDAY1N'
                                });
                            }
                        });
                }
            }]
        },
        {
            id: 'SendAssessmentWindow',
            trigger: [
                'QUESTIONNAIRE:Completed/FACTP',
                'QUESTIONNAIRE:Completed/EQ5D5L',
                'QUESTIONNAIRE:Completed/BPID',
                'QUESTIONNAIRE:Completed/DiaryMed'
            ],
            evaluate: true,
            resolve: [{
                action: (params) => {
                    const subject = params.subject,
                        phases = LF.StudyDesign.studyPhase;
                    let IG = params.questionnaire,
                        questionnaire = params.questionnaire;
                    if (IG === 'BPID') {
                        IG = 'BPISFD';
                    }

                    Tools.getAssessmentWindowValue(subject, phases, questionnaire)
                        .then((assessmentWindow) => {
                            if (assessmentWindow) {
                                LF.Data.Questionnaire.addIT({
                                    question_id: 'QSTWND1N',
                                    questionnaire_id: questionnaire,
                                    response: assessmentWindow,
                                    IG,
                                    IGR: 0,
                                    IT: 'QSTWND1N'
                                });
                            }
                        });
                }
            }]
        },
        {
            id: 'ruleId',
            trigger: 'DASHBOARD:Rendered',
            evaluate: true,
            resolve: [{
                action: () => localStorage.removeItem('BPI_POPUP')
            }]
        },
        {
            id: 'BPI-SF-PopUp',
            trigger: 'QUESTIONNAIRE:Displayed/BPID/BPID_BPID010',
            evaluate: (input, resume) => resume(!localStorage.getItem('BPI_POPUP')),
            resolve: [{
                action: () => {
                    let phaseStart = moment(LF.Data.Questionnaire.subject.attributes.phaseStartDateTZOffset);
                    // we will be in window to get here
                    Q.all([
                        PDE.CollectionModelUtils.getCollection('DiaryCompletionCollection'),
                        //Tools.getModelWindowNumber('BPID', LF.router.view().subject)
                        Tools.getRandomizationDate(),
                        Tools.getWindowParams('BPID', LF.router.view().subject),
                        Tools.isScreeningRescreening()

                    ]).then(([dcCollection,/*windowNumber,*/ randDate, windowParams, isScreeningRescreening]) => {
                            let diary = 'BPID';
                            // none within this window? - display the message
                            let CurrWindow = dcCollection.where({ diary });
                            // Filter model that is older than the current window
                            CurrWindow = CurrWindow.filter((model) => {
                                    let reportTime = moment(model.get('dateTime'));

                                    if (isScreeningRescreening) {
                                        return reportTime.isSameOrAfter(phaseStart);
                                    }
                                    else {
                                        let assessmentDay = Tools.getAssessmentDayHelper(reportTime, randDate);
                                        return assessmentDay >= windowParams.start;
                                    }
                                });

                            if (!CurrWindow.length) {
                                MessageRepo.display(
                                    MessageRepo.Dialog.BPI_SF_ALERT, { namespace: 'STUDY' }
                                );
                            }
                            localStorage.setItem('BPI_POPUP', true);
                        });
                }
            }]
        },
        {
            id: 'SaveDiaryCompletionModel',
            trigger: [
                'QUESTIONNAIRE:Completed/HHTrainingModule',
                'QUESTIONNAIRE:Completed/STM',
                'QUESTIONNAIRE:Completed/BPID',
                'QUESTIONNAIRE:Completed/FACTP',
                'QUESTIONNAIRE:Completed/EQ5D5L'
            ],
            salience: 100, //highest
            evaluate: true,
            resolve: [{
                action: (params) => {
                    const diary = params.questionnaire,                    
                        dateTime = new Date().ISOLocalTZStamp();

                    return new DiaryCompletionModel({ diary, dateTime }).save();
                }
            }]
        },
        {
            id: 'setAutoLaunch',
            trigger: [
                'QUESTIONNAIRE:Saved/BPID',
                'QUESTIONNAIRE:Saved/DiaryMed',
                'QUESTIONNAIRE:Saved/FACTP'
            ],
            evaluate: () => !PDE.DeviceStatusUtils.isScreenshotMode(),
            resolve: [{
                action: (params) => {
                     let logger = new Logger('setAutoLaunch'),
                         toAutoLaunch;
                    switch (params.questionnaire) {
                        case 'BPID':
                            toAutoLaunch = 'DiaryMed';
                            break;
                        case 'DiaryMed':
                            toAutoLaunch = 'FACTP';
                            break;
                        case 'FACTP':
                            toAutoLaunch = 'EQ5D5L';
                            break;
                        default:
                            throw new Error('Cannot find diary to autolanch!');
                    }

                    // Need to check FACTP and EQ5D5L are actually available
                    if (toAutoLaunch === 'FACTP' || toAutoLaunch === 'EQ5D5L') {
                        return Q.all([
                            CurrentContext().get('user').getSubject(),
                            PDE.CollectionModelUtils.getCollection('LastDiaries'),
                            Tools.getLastConfirmedVisitDT()
                        ])
                        .then(([subject, lastDiaries, lastConfirmedVisitDT]) => {
                            if (toAutoLaunch === 'FACTP' || toAutoLaunch === 'EQ5D5L') {
                                if (!checkCompletedToday('BPID', lastDiaries)) {
                                    // don't auto-launch if BPI wasn't completed today
                                    return false;
                                }
                                if (lastConfirmedVisitDT && PDE.DateTimeUtils.dateDiffInDays(new Date(), lastConfirmedVisitDT) == 0) {
                                    // don't auto-launch if a visit was confirmed today
                                    return false;
                                }
                            }
                            return checkSingleDiaryWindowAvailability(subject, toAutoLaunch, lastDiaries);
                        })
                        .catch((error) => {
                            if (error && error.message) {
                                logger.info(error.message);
                            }
                            return false;
                        })
                        .then((doAutoLaunch) => {
                            if (doAutoLaunch) {
                                localStorage.setItem('AUTOLAUNCH', toAutoLaunch);
                            }
                        });
                    } else if (toAutoLaunch === 'DiaryMed') {
                        localStorage.setItem('AUTOLAUNCH', toAutoLaunch);
                    }
                }
            }]
        },
        {
            id: 'AddFirstCompletion',
            trigger: [
                'QUESTIONNAIRE:Completed/BPID',
                'QUESTIONNAIRE:Completed/DiaryMed'
            ],
            evaluate: true,
            resolve: [{
                action: (params) => {
                    const diary = params.questionnaire,
                        subject = params.subject,
                        phase = params.subject.get('phase'),
                        phaseStart = moment(params.subject.get('phaseStartDateTZOffset'));

                    return Q.all([
                        PDE.CollectionModelUtils.getCollection('DiaryCompletionCollection'),
                        //Tools.getModelWindowNumber('BPID', subject)
                        Tools.getRandomizationDate(),
                        Tools.getWindowParams('BPID', subject), // The LF.router.view().subject returned undefined during autolaunch sequence.
                        Tools.isScreeningRescreening()
                    ])
                    .then(([collection,/*currWindow, */ randDate, windowParams, isScreeningRescreening]) => {
                            // we always used BPID as this is the same for DiaryMed as well
                            let diaryModels = collection.where({ diary: 'BPID'/*, window: currWindow*/ });

                            // Filter model that is older than the current window
                            diaryModels = diaryModels.filter((model) => {
                                    let reportTime = moment(model.get('dateTime'));

                                    if (isScreeningRescreening) {
                                        return reportTime.isSameOrAfter(phaseStart);
                                    }
                                    else {
                                        let assessmentDay = Tools.getAssessmentDayHelper(reportTime, randDate);
                                        return assessmentDay >= windowParams.start;
                                    }
                                });

                            // belt and braces. There should always be one,
                            if (!diaryModels || !diaryModels.length) {
                                return new Date().ISOLocalTZStamp();
                            }
                            // if more than one we need to get the oldest datetime first
                            if (diaryModels.length > 1) {
                                diaryModels = diaryModels.sort((a, b) => new Date(a.get('dateTime')) - new Date(b.get('dateTime')));
                            }
                            return _.first(diaryModels).get('dateTime');
                        })
                        .then((dateTime) => {
                            let completionDay = moment(new Date()).diff(dateTime, 'days') + 1;
                            LF.Data.Questionnaire.addIT({
                                question_id: 'CMPSTD1D',
                                questionnaire_id: diary,
                                response: moment(dateTime).format('DD MMM YYYY'),
                                IG: diary == 'BPID' ? 'BPIDiagram' : 'DiaryMed',
                                IGR: 0,
                                IT: 'CMPSTD1D'
                            });

                            if (phase >= LF.StudyDesign.studyPhase.TREATMENT) {
                                LF.Data.Questionnaire.addIT({
                                    question_id: 'CMPORD1N',
                                    questionnaire_id: diary,
                                    response: completionDay,
                                    IG: diary == 'BPID' ? 'BPISFD' : 'DiaryMed',
                                    IGR: 0,
                                    IT: 'CMPORD1N'
                                });
                            }
                        })
                        .done();
                }
            }]
        },
        {
            id: 'autoLaunchDiary',
            trigger: 'DASHBOARD:Rendered',
            evaluate: () => {
                return Q(Boolean(localStorage.getItem('AUTOLAUNCH')) && !PDE.DeviceStatusUtils.isScreenshotMode());
            },
            salience: 100,
            resolve: [
                {
                    action: (params) => {
                        let id = localStorage.getItem('AUTOLAUNCH'),
                            flashParams = {
                                subject: params.subject
                            };

                        LF.Data.Questionnaire = {};
                        localStorage.removeItem('AUTOLAUNCH');
                        return Q.delay(150)
                            .then(() => LF.router.flash(flashParams).navigate(`questionnaire/${id}`, true));
                    }
                }
            ]
        },
        {
            id: 'GroupAlarm_AlarmTrigger',
            trigger: ['ALARM:Trigger/BPID_Schedule',
                      'ALARM:Trigger/FACTP_Schedule',
                      'ALARM:Trigger/EQ5D5L_Schedule'],
            evaluate: (filter, resume) => resume(!localStorage.getItem('screenshot')),
            resolve: [
                {
                    action (input, callback) {

                        let logger = new Logger('StudyRules:GroupAlarm_AlarmTrigger');

                        if (!PDE.GroupAlarmWasProcessed) {
                            PDE.GroupAlarmWasProcessed = true;
                            LF.Utilities.getCollections(['LastDiaries', 'Subjects'])
                            .then((collections) => {
                                let completedQuestionnaires = collections.LastDiaries,
                                    subject = _.first(collections.Subjects.models);
                                // Call a function that checks if any diary in the group is available.
                                return checkGroupAlarmDiariesAvailable(subject, completedQuestionnaires);
                            }).then((available) => {
                                setTimeout(() => {
                                    // We need to remove the global flag after other group alarms
                                    // have been processed. The worst thing that could happen
                                    // if there is for some bizarre reason a delay longer than
                                    // 3 seconds, is that an alarm will sound twice (or more?!!)
                                    // in short succession.
                                    logger.info('Group Alarm flag was reset.');
                                    PDE.GroupAlarmWasProcessed = null;
                                }, 3000);
                                if (available) {
                                    logger.info('Diary in group is available. Group Alarm will fire.');
                                    callback(true);
                                } else {
                                    logger.info('No diary in group is available. Group Alarm will NOT fire.');
                                    callback({ preventDefault: true });
                                }
                            });
                        } else {
                            logger.info('Group Alarm was already processed.');
                            callback({ preventDefault: true });
                        }
                    }
                }
            ]
        },
        // [ONEECOA-105791] Hack to override core login language filter
        {
            id: 'LoginFilterLanguages',
            trigger: 'LOGIN:Rendered',
            evaluate: true,
            resolve: [
                {
                    action (input, callback) {
                        if (this.createLanguageSelectionData !== Tools.customLoginLanguageFilter) {
                            // Override core login language filter with out own filter
                            this.createLanguageSelectionData = Tools.customLoginLanguageFilter;
                            // Render again to update the language
                            this.render().then(callback);
                        } else {
                            callback();
                        }
                    }
                }
            ]
        },
        {
            id: 'buttonPriorityFix',
            trigger: [
                'DASHBOARD:AddedToDashboard'
            ],
            evalute: true,
            resolve: [
                {
                    action () {
                        let schedules = LF.Helpers.getScheduleModels(),
                            currentRole = LF.security.activeUser.get('role'),
                            list = $('#questionnairesList').children(),
                            thisSchedule,
                            currentDisplay,
                            lastIndex = 0,
                            thisIndex;
                        schedules = _.filter(schedules, function (schedule) {
                            return (!schedule.get('scheduleRoles') || schedule.get('scheduleRoles').indexOf(currentRole) !== -1)
                                && schedule.get('target').objectType === 'questionnaire';
                        });
                        //check if anything is displayed yet
                        if (list.length > 0 && schedules.length > 0) {
                            for (let i = 0; i < list.length; i++) {
                                currentDisplay = $('#questionnairesList').children()[i].id;
                                thisSchedule = _.find(schedules, function (schedule) {
                                    return schedule.get('target').id === currentDisplay;
                                });
                                thisIndex = schedules.indexOf(thisSchedule);
                                if (lastIndex > schedules.indexOf(thisSchedule)) {
                                    LF.Data.Questionnaire.render();
                                    break;
                                } else {
                                    lastIndex = thisIndex;
                                }
                            }
                        }
                    }
                }
            ]
        }
    ]
};
