PDE.verbose = false; // Temporary to assist debugging. Enables verbose logger.info output

export default {
    // The current handheld study version
    studyVersion: '09.01',

    // The current handheld study database version
    studyDbVersion: 0,

    // flag to enable partial sync for the handheld modality
    enablePartialSync: true,

    // flag to enable full sync for the handheld modality
    enableFullSync: true,

    // The amount of time in minutes that a user can be inactive before being logged out.
    sessionTimeout: 75,

    // The amount of time in minutes that a user is allowed to complete a diary within.
    // Should not be > sessionTimeout, since sessionTimeout will happen first and exit the diary anyway.
    // If 0, questionnaire itself doesn't time out, but sessionTimeout will still occur
    questionnaireTimeout: 60,

    // Default volume settings for alarms
    alarmVolumeConfig: {
        // Volume Percentage at which to sound the alarm (0-100)
        alarmVolume: 100,

        // Enable or Disable vibration for notifications
        vibrate: true,

        // Delay after alarm sounds to revert back to the users configured phone settings
        resetDelay: 2
    },

    // Configuration for the eSense feature
    eSenseConfig: {

        // Whether or not Bluetooth should be disabled automatically after an operation is complete
        disableBluetooth: true,

        // Timeout value in milliseconds for eSense all API calls (except findDevices and get records).
        apiCallTimeout: 10000,

        // Timeout value in milliseconds for eSense findDevices API calls (in milliseconds).
        findDevicesTimeout: 60000,

        // Timeout value in milliseconds for eSense get records API calls (in milliseconds).
        getRecordsTimeout: 90000,

        // AM1+ Device specific configurations
        AM1Plus: {

            // Timeout Window configuration for AM1+
            timeWindow: {
                windowNumber: 1,
                startHour: 0,
                startMinute: 0,
                endHour: 23,
                endMinute: 59,
                maxNumber: 15
            }
        }
    },

    // Configuration for rater training
    raterTrainingConfig: {
        logpad: {
            // set to true if using rater training, false otherwise
            useRaterTraining: false,
            // the domains should always be the same, only the {{{ROOT_DIRECTORY}}} should change between studies
            fallBackBaseURL: 'http://ert-training.s3.amazonaws.com/AZ_D081SC00001_HH',

            // should be the same as the directory following the domain in fallBackBaseURL
            rootDirectory: 'AZ_D081SC00001_HH',

            // These should never change unless explicitly changed by Science/IT
            awsKey: 'AKIAIVMTD47W6QHZNLXQ',
            awsSecretKey: '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',
            awsRegion: 'us-east-1',
            awsBucket: 'ert-training',
            pages: {
                default: 'index.html'
            },
            zipFile: true
        }
    },

    // A collection of indicator configurations
    indicators: [
        {
            id: 'NextAssessment_Indicator',
            className: 'indicator-blue-tall',
            label: 'NEXT_ASSESSMENT_INDICATOR',
            scheduleFunction: 'NextAssessmentIndicator_Schedule'
        }, {
            id: 'BPIDUpcoming_Indicator',
            className: 'indicator-blue',
            label: 'BPID_UPCOMING',
            scheduleFunction: 'BPIDUpcoming_Schedule'
        }, {
            id: 'BPIDDueNow_Indicator',
            className: 'indicator-red',
            label: 'BPID_DUE_NOW',
            scheduleFunction: 'BPIDDueNow_Schedule'
        }, {
            id: 'AssessmentNow_Indicator',
            className: 'indicator-red',
            label: 'ASSESSMENT_NOW_INDICATOR',
            scheduleFunction: 'AssessmentNowIndicator_Schedule'
        }
    ]
};
