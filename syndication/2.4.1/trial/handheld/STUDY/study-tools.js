import Logger from 'core/Logger';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import * as Utilities from 'core/utilities';

const OUT_OF_WINDOW_VAL = '-999';

export const getStudyPeriod = (currentPhase) => {
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|getStudyPeriod');
        logger.info(`currentPhase: ${currentPhase}`);
    }
    switch (currentPhase) {
        case LF.StudyDesign.studyPhase.SCREENING:
            return 0;
        case LF.StudyDesign.studyPhase.TREATMENT:
            return 2;
        case LF.StudyDesign.studyPhase.FOLLOWUP_1:
            return 3;
        case LF.StudyDesign.studyPhase.FOLLOWUP_2:
            return 4;
        default:
            return 1; // for all re-screening
    }
};

/*
 * Calculates the study day variable - STYDAY1N
 * @param {Date} todayDT - Today's Date
 * @param {Date} randDT - Subject Randomization date RNDMDT1D
 * @return {Number|null} study day or null
 */
export function calculateStudyDay (todayDt, randDt) {
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|calculateStudyDay');
        logger.info(`todayDt: ${todayDt}\nrandDt: ${randDt}`);
    }
    if (randDt === null || randDt > todayDt) {
        return null;
    }
    return PDE.DateTimeUtils.dateDiffInDays(todayDt, randDt) + 1;
}

export function getAssessmentDayHelper (today, randDate) {
    if (randDate) {
        // +1 as the randomization date starts at day 1 not day 0
        return moment(today).diff(randDate, 'days') + 1;
    }
    return null;
}

/*
 * Gets the current day of the study based on randomization date = day 1.
 * Returns null of randomization is not set
 * @param {Date} today's date
 * @return {Q.Promise} Number or null with assessment day
 */
export function getAssessmentDay (today) {
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|getAssessmentDay');
        logger.info(`today: ${today}`);
    }
    return getRandomizationDate().then((randDate) => {
        return getAssessmentDayHelper(today, randDate);
    });
}

const getOrderedVisitConfirmation = () => {
    return PDE.CollectionModelUtils.getCollection('VisitConfirmationCollection')
        .then((collection) => {

            if (collection && collection.length) {
                if (PDE.verbose) {
                    let logger = new Logger('Study Tools|getOrderedVisitConfirmation');
                    logger.info('sorting collection...');
                }
                collection.comparator = col => new Date(col.get('dateTime'));
                collection.sort();
                return collection;
            }
            if (PDE.verbose) {
                let logger = new Logger('Study Tools|getOrderedVisitConfirmation');
                logger.warn('collection was falsy or empty');
            }
            return null;
        });
};

/*
 * Gets the randomization date or null if it hasn't been set
 * @return {Q.Promise} promise with Date obj null
 */
export const getRandomizationDate = () => {
    return getOrderedVisitConfirmation()
        .then((collection) => {
            let dateTime;
            if (collection && collection.length) {
                collection = collection.filter(model => model.get('RNDMDT1D'));
                if (collection.length) {
                    dateTime = _.last(collection).get('RNDMDT1D');
                    if (dateTime) {
                        if (PDE.verbose) {
                            let logger = new Logger('Study Tools|getRandomizationDate');
                            logger.info(`Found randomization date: ${dateTime}`);
                        }
                        return new Date(dateTime);
                    }
                }
            }
            if (PDE.verbose) {
                let logger = new Logger('Study Tools|getRandomizationDate');
                logger.warn('Could not find a randomization date');
            }
            return null;
        });
};

/*
 * Gets the last visit confirmation visit type
 * @param {Subject} The subject Model
 * @return {Q.Promise} promise with last visit confirmation type
 */
export const getLastConfirmedVisit = () => {
    return getOrderedVisitConfirmation()
        .then((collection) => {
            if (collection) {
                if (PDE.verbose) {
                    let logger = new Logger('Study Tools|getLastConfirmedVisit');
                    logger.info('collection was truthy');
                }
                let visitNum = _.last(collection.models).get('VST1L');
                return visitNum ? visitNum : null;
            }
            if (PDE.verbose) {
                let logger = new Logger('Study Tools|getLastConfirmedVisit');
                logger.info('collection was falsy');
            }
        });
};

/*
 * Gets the last visit confirmation visit type
 * @param {Subject} The subject Model
 * @return {Q.Promise} promise with last visit confirmation type
 */
export const getLastConfirmedVisitDT = () => {
    return getOrderedVisitConfirmation()
            .then((collection) => {
            if (collection) {
                if (PDE.verbose) {
                    let logger = new Logger('Study Tools|getLastConfirmedVisitDT');
                    logger.info('collection was truthy');
                }
                let visitDT = _.last(collection.models).get('dateTime');
                return visitDT ? new Date(visitDT) : null;
            }
            if (PDE.verbose) {
        let logger = new Logger('Study Tools|getLastConfirmedVisitDT');
        logger.info('collection was falsy');
    }
});
};

const isPhaseChangeToday = (subject, today) => {
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|isPhaseChangeToday');
        logger.info(`today: ${today}`);
    }
    let phaseStart = moment(subject.get('phaseStartDateTZOffset'));
    return phaseStart.isSame(today, 'day');
};

export const isScreeningRescreening = () => {
    let phase = LF.Data.Questionnaire.subject.attributes.phase;

    if (PDE.verbose) {
        let logger = new Logger('Study Tools|isScreeningRescreening');
        logger.info(`phase: ${phase}`);
    }

    return phase < LF.StudyDesign.studyPhase.TREATMENT;
};

const getScheduledDay = (diary, subject) => {
    return getWindowParams(diary, subject).then(windowParams => String(windowParams.scheduled));
};

/*
    IF completed during screening/re-screening returns '0'
    IF on day of start treatment returns '1'
    IF on day of treatment discontinuation returns '9999'
    IF on day of progression and treatment discontinuation returns '9998'
    Otherwise returns the scheduled day within window.
    @param {Subject} The subject Model
    @param {Object} Phases
    @return {Q.Promise} Promise with string value for assessment window
 */
export const getAssessmentWindowValue = (subject, phases, diary) => {
    let logger = new Logger('Study Tools|getAssessmentWindowValue'),
        currPhase = subject.get('phase'),
        today = moment(new Date()),
        returnVal;

    if (PDE.verbose) {
        logger.info(`phases: ${phases}\ndiary: ${diary}\ncurrPhase: ${currPhase}`);
    }
    if (currPhase < LF.StudyDesign.studyPhase.TREATMENT) {
        returnVal = '0';
    }

    if (returnVal !== '0' && isPhaseChangeToday(subject, today)) {
        switch (currPhase) {
            case phases.TREATMENT:
                returnVal = '1';
                break;
            case phases.FOLLOWUP_1:
                returnVal = '9999'; // treatment discontinuation
                break;
            case phases.FOLLOWUP_2:
                returnVal = '9998'; // progression & treatment discontinuation
                break;
            default:
                logger.error('Unexpected phase!');
                returnVal = '-1';
        }
    }
    if (PDE.verbose) {
        logger.info(`returnVal: ${returnVal}`);
    }
    return returnVal ? Q(returnVal) : getScheduledDay(diary, subject);
};

const getInScreeningWindowParams = (now) => {
    return PDE.CollectionModelUtils.getCollections(['LastDiaries', 'DiaryCompletionCollection'])
        .then((collections) => {
            let visitConfirmation = collections['LastDiaries'].findWhere({ questionnaire_id: 'VisitConfirmation' }),
                diaryCompletion = collections['DiaryCompletionCollection'],
                visitStart = visitConfirmation && moment(visitConfirmation.get('lastStartedDate')),
                isInWindow = true,
                currentBPIDDiaries = diaryCompletion.filter((model) => {
                    let diaryID = model.get('diary'),
                        reportStart = moment(model.get('dateTime'));

                    return diaryID === 'BPID' && (!visitStart || reportStart.isAfter(visitStart));
                }),
                startDate = _.reduce(currentBPIDDiaries, (prev, curr) => {
                    let prevStart = moment(prev.get('dateTime')),
                        currStart = moment(curr.get('dateTime'));

                    return prevStart.isBefore(currStart) ? prev : curr;
                });

            if (!now) {
                now = new Date();
            }

            if (startDate) {
                let maxWindow = moment(startDate.get('dateTime')).add(7, 'd');

                isInWindow = moment(now).isBefore(maxWindow, 'd');
            }

            return isInWindow ? { startDate, maxCompletions: 7, maxDays: 7 } : null;
        });
};

const getInWindowParams = (diary, assessmentDay) => {
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|getInWindowParams');
        logger.info(`diary: ${diary}\nassessmentDay: ${assessmentDay}`);
    }
    let windowParams,
        windowStart,
        windowEnd,
        nextWindowDays,
        maxCompletions,
        maxDays,
        scheduled/*,
        windowNum*/;
    switch (diary) {
        case 'FACTP':
            windowStart = assessmentDay < 362 ? 26 : 362;
            windowEnd = assessmentDay < 362 ? 32 : 368;
            nextWindowDays = assessmentDay < 362 ? 28 : 56;
            maxCompletions = 1;
            maxDays = 7;
            scheduled = assessmentDay < 362 ? 29 : 365;
            //windowNum = assessmentDay < 362 ? 1 : 13;
            break;
        case 'EQ5D5L':
            windowStart = assessmentDay < 362 ? 54 : 362;
            windowEnd = assessmentDay < 362 ? 60 : 368;
            nextWindowDays = 56;
            maxCompletions = 1;
            maxDays = 7;
            scheduled = assessmentDay < 362 ? 57 : 365;
            //windowNum = assessmentDay < 362 ? 1 : 7;
            break;
        default:
            windowStart = 22;
            windowEnd = 36;
            nextWindowDays = 28;
            maxCompletions = 7;
            maxDays = 7;
            scheduled = 29;
            //windowNum = 1;
            break;
    }

    windowParams = generateWindowParams(assessmentDay, windowStart, windowEnd, nextWindowDays, scheduled/*, windowNum*/);
    if (windowParams) {
        windowParams.maxCompletions = maxCompletions;
        windowParams.maxDays = maxDays;
    }
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|getInWindowParams');
        logger.info(`windowParams: ${windowParams}`);
    }
    return windowParams;
};

export const generateWindowParams = (assessmentDay, start, end, nextWindowDays, scheduled/*, windowNum*/) => {
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|generateWindowParams');
        logger.info(`assessmentDate: ${assessmentDay}\nstart: ${start}\nend: ${end}\nnextWindowDays: ${nextWindowDays}\nscheduled: ${scheduled}/*`);
    }
    while (assessmentDay >= start) {
        if (assessmentDay <= end) {
            return { start, end,/* window: windowNum,*/ scheduled };
        }
        //windowNum++;
        scheduled += nextWindowDays;
        start += nextWindowDays;
        end += nextWindowDays;
    }
    return null;
};

/*
 * Returns the current window number which matches the Model window number.
 * This is returned as string for ease of use when checking through and saving to models;
 *
 * The number can either be a negative int (when in a screening/re-screening phase
 * Or a positive int matching the window.
 * @param {String} - diary to check
 * @param {Subject} - subject model
 * @return {Q.Promise} - String with model window number
 */
/*
export const getModelWindowNumber = (diary, subject) => {
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|getModelWindowNumber');
        logger.info(`diary: ${diary}`);
    }
    return getWindowParams(diary, subject)
        .then((params) => params ? String(params.windowNum) : OUT_OF_WINDOW_VAL);
};
*/

/*
    Returns a promise containing the parameters for evaluating if the current diary is within
    a certain window i.e:

    {
            diary: 'BPID',
            window: 1,
            start: 22,
            scheduled: 29,
            end: 38,
            maxCompletions: 7
    }
    @param {String} diary - the name of the diary to evaluate
    @param {Subject} subject - the subject model
    @return {Q.Promse} promise containing the params or null
*/
export const getWindowParams = (diary, subject) => {
    let phase = subject.get('phase'),
        logger = new Logger('Study Tools|getWindowParams'),
        screeningRescreening = phase < LF.StudyDesign.studyPhase.TREATMENT;
    // Function is used buy everything so need to make sure we are not testing
    // FACT-P/EQ5D5L here either.
    if (screeningRescreening && diary === 'BPID') {
        if (PDE.verbose) {
            logger.info('Rescreening with BPID...');
        }
        return getInScreeningWindowParams();
    } else if (!screeningRescreening) {
        if (PDE.verbose) {
            logger.info('Not rescreening...');
        }
        return getAssessmentDay(new Date())
            .then((assessmentDay) => {
                // belt and braces check here
                if (!assessmentDay) {
                    throw new Error('No assessment day provided - cannot get windowParams')
                }
                if (PDE.verbose) {
                    logger.info(`assessmentDay: ${assessmentDay}`);
                }
                let windowParams = getInWindowParams(diary, assessmentDay);
                logger.info(diary);
                logger.info(windowParams);
                return windowParams;
            }).catch((error) => {
                logger.error(error.message);
            })
    }
    if (PDE.verbose) {
        logger.info('No windowParams to return.');
    }
    return Q(null);
};

const getNextWindowParams = (diary, currAssessmentDay) => {
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|getNextWindowParams');
        logger.info(`diary: ${diary}\ncurrAssessmentDay: ${currAssessmentDay}`);
    }
    let windowParams;
    for (let i = 0; i < 28; i++) {
        windowParams = getInWindowParams(diary, currAssessmentDay + i);
        // ONEECOA-110961: HH - Incorrect date on Reminder Icon in specific scenario
        // This was incorrectly returning the start of the current window if we are in a window.
        // Added:    && params.start > currAssessmentDay
        if (windowParams && windowParams.start > currAssessmentDay) {
            return windowParams;
        }
    }
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|getNextWindowParams');
        logger.info('No params to return.');
    }
    return null;
};

/*
 * Gets the date of the next assessment window day.
 * @param {String} diary - name of diary to find
 * @param {Date} today - today's current date (or date to look from)
 * @return {Date} - date object representing the dateTime of the next window
 */
export const getNextAssessmentDate = (diary, today) => {
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|getNextAssessmentDate');
        logger.info(`diary: ${diary}\ntoday: ${today}`);
    }
    return getAssessmentDay(today)
        .then((currAssessmentDay) => {
            if (PDE.verbose) {
                let logger = new Logger('Study Tools|getNextAssessmentDate');
                logger.info(`currAssessmentDay: ${currAssessmentDay}`);
            }
            let nextWindow = getNextWindowParams(diary, currAssessmentDay),
                daysAway = nextWindow.start - currAssessmentDay;
            return moment(today).add(daysAway, 'days').toDate();
        });
};

/*
 * Spec section 2.3.1.2  4)
 *
 * Established if day 86 has been reached within "Progression and Treatment Discontinuation"
 * or “Progression"
 * @param {String} phase - the current phase
 * @param {Backbone.collection} lastDiaries - list of last diaries to look through for day 1
 * @return {boolean} - if progression time has expired
*/
export const progressionTimePassed = (phase, lastDiaries) => {
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|progressionTimePassed');
        logger.info(`phase: ${phase}`);
    }
    if (phase === LF.StudyDesign.studyPhase.FOLLOWUP_2) {
        let lastVcDate = lastDiaries.findWhere({ questionnaire_id: 'VisitConfirmation' }).get('lastStartedDate'),
            now = new Date();

        if (lastVcDate) {
            let progressionDay = dateDiffInDays(now, new Date(lastVcDate)) + 1,
                retVal = progressionDay > 85;

            if (PDE.verbose) {
                let logger = new Logger('Study Tools|progressionTimePassed');
                logger.info(`returning calculated value: ${retVal}`);
            }
            return retVal;
        }
    }
    if (PDE.verbose) {
        let logger = new Logger('Study Tools|progressionTimePassed');
        logger.info('returning false without calculation');
    }
    return false;
};

/**
 * removeITByIG queries for all answers with IG, and removes them from the current view
 * Copy from global LF version which only works if within the questionnaire view
 * @param {string} IG - the IG to remove
 * @param {BaseQuestionnaireView} view - the questionnaire view to remove answers from
 */
export const removeITByIG = (IG, view) => {
    let answers;
    if (view instanceof BaseQuestionnaireView) {
        if (typeof IG !== 'undefined') {
            answers = view.queryAnswersByIG(IG);
            view.data.answers.remove(answers);
        }
    }
};

export const dateDiffInDays = function (date1, date2, useAbsolute) {
    let date1Copy,
        date2Copy,
        date1Offset,
        date2Offset,
        msDiff;

    if (moment.isMoment(date1)) {
        date1 = new Date(date1.toString());
    }

    if (moment.isMoment(date2)) {
        date2 = new Date(date2.toString());
    }

    date1Copy = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate(), 0, 0, 0, 0);
    date2Copy = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate(), 0, 0, 0, 0);
    date1Offset = (date1Copy.getTimezoneOffset() * 60 * 1000);
    date2Offset = (date2Copy.getTimezoneOffset() * 60 * 1000);

    if (!useAbsolute) {
        msDiff = date1Copy - date2Copy + (date2Offset - date1Offset);
    } else {
        msDiff = Math.abs(date1Copy - date2Copy + (date2Offset - date1Offset));
    }

    return Math.floor(msDiff / 1000 / 60 / 60 / 24);
};

// ONEECOA-105791 Hack to override product login language filter
// This is base of LoginView.createLanguageSelectionData() with study specific logic
export const customLoginLanguageFilter = function () {
    let selectedRoleSupportedLanguages,
        localized = LF.strings.match({ namespace: 'CORE', localized: {} }),
        langs = [],
        logger = new Logger('Study/LoginView');

    logger.traceEnter('customLoginLanguageFilter');

    if (this.user) {
        // If user type is Admin, use role instead of user to determine supported languages
        if (this.user.get('role') === 'admin') {
            let role = LF.StudyDesign.roles.findWhere({ id: 'admin' });

            if (role) {
                selectedRoleSupportedLanguages = role.get('supportedLanguages');
            }
        }

        if (!selectedRoleSupportedLanguages) {
            selectedRoleSupportedLanguages = this.user.get('supportedLanguages') || 'ALL';
        }
    }

    // get the localized names from the resource string files in order of 'language' and 'locale' keys
    localized.sort((firstComparatorObject, secondComparatorObject) => {
        let lang1 = firstComparatorObject.get('language'),
            lang2 = secondComparatorObject.get('language'),
            locale1 = firstComparatorObject.get('locale'),
            locale2 = secondComparatorObject.get('locale');
        if (lang1 > lang2) {
            return 1;
        } else if (lang1 < lang2) {
            return -1;
        }
        return locale1 > locale2 ? 1 : -1;
    }).forEach((language) => {
        let langCode = `${language.get('language')}-${language.get('locale')}`,
            lang = {
                id: langCode,
                localized: language.get('localized'),
                fontFamily: Utilities.getFontFamily(langCode),
                dir: language.get('direction'),
                cssClass: language.get('direction') === 'rtl' ? 'right-direction text-right-absolute' : 'left-direction text-left-absolute',
                selected: langCode === this.user.get('language')
            };

        if (selectedRoleSupportedLanguages.toString() === 'ALL') {
            langs.push(lang);
        } else if (_.contains(selectedRoleSupportedLanguages, langCode)) {
            langs.push(lang);
            selectedRoleSupportedLanguages = _.without(selectedRoleSupportedLanguages, langCode);
        }
    });

    logger.traceExit('customLoginLanguageFilter');

    return langs;
};
