import DynamicText from 'core/dynamicText';
import { getNextAssessmentDate, getWindowParams } from './study-tools';
import PDE from 'PDE_Core/common/PDE';


DynamicText.add({
    id: 'NEXT_ASSESSMENT_INDICATOR',
    evaluate: () => {
        const today = new Date();

        return getNextAssessmentDate('BPID', today)
            .then((assessmentDt) => {
                let day = PDE.DateTimeUtils.format(assessmentDt, 'dayOfWeek'),
                    date = PDE.DateTimeUtils.format(assessmentDt, 'indicatorFormat');
                return `${day}<br/>${date}`;
            });
    },
    screenshots: {
        getValues () {
            return 'Thursday<br/>13 Sep 2018';
        }
    }
});

DynamicText.add({
    id: 'BPID_TIME',
    evaluate: () => {
        return PDE.CollectionModelUtils.getCollection('SubjectAlarms')
            .then((alarms) => {
                if (alarms) {
                    let BPIDAlarm = _.filter(alarms.models, function (model) {
                            return model.get('schedule_id') == 'BPID_Schedule';
                        }),
                        alarmDT = new Date();

                    if (BPIDAlarm.length > 0) {
                        let alarmTime = BPIDAlarm[0].attributes.time,
                            timeArray = alarmTime.split(":");
                        alarmDT = moment(new Date(alarmDT.setHours(timeArray[0], timeArray[1], 0, 0)));
                        return alarmDT.format('HH:mm');
                    } else {
                        // use 9am default alarm time
                        return '09:00';
                    }
                }
            });
    },
    screenshots: {
        getValues () {
            return '09:00';
        }
    }
});
