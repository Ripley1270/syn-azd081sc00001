/*
* PDE_TODO:
* import files that don't contribute to assets object, but need to ensure the content executes
* for example, a file that assigns a function into the global LF namespace
*/
// import './exampleBranchingFunctions';
// import './exampleUtilities';
import './AlarmRenderOverwrite';
import './handheld-schedule-tools';
import './handheld-schedule-functions';
import './study-dynamic-text';
import './models';
import './collections';
import './actions';

import { mergeObjects } from 'core/utilities/languageExtensions';

let studyDesign = {};

/*
* PDE_TODO:
* Collect and merge studyDesign objects
*/
// import rules from './exampleRules';
// import messages from './exampleMessages';
// import templates from './exampleTemplates';
import SD from './study-design';
import schedules from './handheld_study-schedules';
import indicatorSchedules from './indicator-schedules';
import rules from './handheld-study-rules';

studyDesign = mergeObjects(studyDesign, SD, schedules, indicatorSchedules, rules /* , templates, */);


/* export the assets object*/
export default { studyDesign };
