import BodyImageMapStorageWithoutLatestFeatures from './widget/BodyImageMapStorageWithoutLatestFeatures';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Logger from 'core/Logger';

export default {
    rules: [
        {
            id: 'NavigateFromBFISF010',
            trigger: 'QUESTIONNAIRE:Navigate/BPID/BPID_BPID010',
            evaluate: function (filter, resume) {
                var direction = filter.direction;

                if(direction == 'previous') {
                    let bodyPortionToClear = BodyImageMapStorageWithoutLatestFeatures.allBodyPortionText;
                    BodyImageMapStorageWithoutLatestFeatures.clearAllSelectBodyPartsForBodyPortion(bodyPortionToClear);
                    BodyImageMapStorageWithoutLatestFeatures.removeAllWorstPainSelectedID();
                    BodyImageMapStorageWithoutLatestFeatures.setNoPainSelected(false);
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'OpenBPIDQuestionnaire',
            trigger: 'QUESTIONNAIRE:Open/BPID',
            evaluate: function (filter, resume) {
                let bodyPortionToClear = BodyImageMapStorageWithoutLatestFeatures.allBodyPortionText;
                BodyImageMapStorageWithoutLatestFeatures.clearAllSelectBodyPartsForBodyPortion(bodyPortionToClear);
                BodyImageMapStorageWithoutLatestFeatures.removeAllWorstPainSelectedID();
                BodyImageMapStorageWithoutLatestFeatures.setNoPainSelected(false);
                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromBFISF030',
            trigger: 'QUESTIONNAIRE:Navigate/BPID/BPID_BPID030',
            evaluate: function (filter, resume) {
                var direction = filter.direction;

                if(direction == 'previous') {
                    let bodyPortionToClear = BodyImageMapStorageWithoutLatestFeatures.frontTopBodyPortionText;
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedFromBodyPortion(bodyPortionToClear);
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedToRemoveFromBodyPortion(bodyPortionToClear);
                } else {
                    BodyImageMapStorageWithoutLatestFeatures.addSelectedFromRecentlySelected();
                    LF.Data.Questionnaire.screenStack.pop();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromBFISF045',
            trigger: 'QUESTIONNAIRE:Navigate/BPID/BPID_BPID045',
            evaluate: function (filter, resume) {
                var direction = filter.direction;

                if(direction == 'previous') {
                    let bodyPortionToClear = BodyImageMapStorageWithoutLatestFeatures.frontBotBodyPortionText;
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedFromBodyPortion(bodyPortionToClear);
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedToRemoveFromBodyPortion(bodyPortionToClear);
                } else {
                    BodyImageMapStorageWithoutLatestFeatures.addSelectedFromRecentlySelected();
                    LF.Data.Questionnaire.screenStack.pop();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromBFISF050',
            trigger: 'QUESTIONNAIRE:Navigate/BPID/BPID_BPID050',
            evaluate: function (filter, resume) {
                var direction = filter.direction;

                if(direction == 'previous') {
                    let bodyPortionToClear = BodyImageMapStorageWithoutLatestFeatures.backTopBodyPortionText;
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedFromBodyPortion(bodyPortionToClear);
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedToRemoveFromBodyPortion(bodyPortionToClear);
                } else {
                    BodyImageMapStorageWithoutLatestFeatures.addSelectedFromRecentlySelected();
                    LF.Data.Questionnaire.screenStack.pop();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromBFISF065',
            trigger: 'QUESTIONNAIRE:Navigate/BPID/BPID_BPID065',
            evaluate: function (filter, resume) {
                var direction = filter.direction;

                if(direction == 'previous') {
                    let bodyPortionToClear = BodyImageMapStorageWithoutLatestFeatures.backBotBodyPortionText;
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedFromBodyPortion(bodyPortionToClear);
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedToRemoveFromBodyPortion(bodyPortionToClear);
                } else {
                    BodyImageMapStorageWithoutLatestFeatures.addSelectedFromRecentlySelected();
                    LF.Data.Questionnaire.screenStack.pop();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromBFISF110',
            trigger: 'QUESTIONNAIRE:Navigate/BPID/BPID_BPID110',
            evaluate: function (filter, resume) {
                var direction = filter.direction;

                if(direction == 'previous') {
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedWorstPainIDs();
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedToRemoveWorstPainIDs();
                } else {
                    BodyImageMapStorageWithoutLatestFeatures.addSelectedFromRecentlySelectedWorstPainID();
                    LF.Data.Questionnaire.screenStack.pop();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromBFISF125',
            trigger: 'QUESTIONNAIRE:Navigate/BPID/BPID_BPID125',
            evaluate: function (filter, resume) {
                var direction = filter.direction;

                if(direction == 'previous') {
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedWorstPainIDs();
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedToRemoveWorstPainIDs();
                } else {
                    BodyImageMapStorageWithoutLatestFeatures.addSelectedFromRecentlySelectedWorstPainID();
                    LF.Data.Questionnaire.screenStack.pop();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromBFISF130',
            trigger: 'QUESTIONNAIRE:Navigate/BPID/BPID_BPID130',
            evaluate: function (filter, resume) {
                var direction = filter.direction;

                if(direction == 'previous') {
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedWorstPainIDs();
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedToRemoveWorstPainIDs();
                } else {
                    BodyImageMapStorageWithoutLatestFeatures.addSelectedFromRecentlySelectedWorstPainID();
                    LF.Data.Questionnaire.screenStack.pop();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromBFISF145',
            trigger: 'QUESTIONNAIRE:Navigate/BPID/BPID_BPID145',
            evaluate: function (filter, resume) {
                var direction = filter.direction;

                if(direction == 'previous') {
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedWorstPainIDs();
                    BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedToRemoveWorstPainIDs();
                } else {
                    BodyImageMapStorageWithoutLatestFeatures.addSelectedFromRecentlySelectedWorstPainID();
                    LF.Data.Questionnaire.screenStack.pop();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'NavigateFromBFISF200',
            trigger: 'QUESTIONNAIRE:Navigate/BPID/BPID_BPID200',
            evaluate: function (filter, resume) {

                var direction = filter.direction;

                if(direction == 'next') {
                    BodyImageMapStorageWithoutLatestFeatures.removeAllWorstPainSelectedID();
                }

                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'Answered_BPID_BPID210',
            trigger: [
                'QUESTIONNAIRE:Navigate/BPID/BPID_BPID210'
            ],
            evaluate : function (filter, resume) {
                let logger = new Logger('StudyRules:BPID_BPID210_Q0');
                let questionnaire = LF.Data.Questionnaire.data.dashboard.get('questionnaire_id');
                let direction = filter.direction;

                if(direction == 'next') {
                    let questionId = 'BPID_BPID210_Q0',
                        answer = LF.Data.Questionnaire.queryAnswersByID(questionId)[0];

                    if (answer) {
                        let resp = answer.get('response');

                        if (resp == 0) {
                            BodyImageMapStorageWithoutLatestFeatures.removeAllWorstPainSelectedID();
                        }
                    }
                }
                resume();
            },
            resolve: [{ action: 'defaultAction' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'BPIDCompletion',
            trigger: [
                'QUESTIONNAIRE:Completed/BPID'
            ],
            evaluate : function (filter, resume) {
                resume(!localStorage.getItem("ScreenshotMode"));
            },
            resolve: [
                {
                    action (input, done) {
                        let logger = new Logger('StudyRules:BPID');
                        let view = LF.router.view();
                        let questionnaire = LF.Data.Questionnaire.data.dashboard.get('questionnaire_id');
                        let allBodyParts = BodyImageMapStorageWithoutLatestFeatures.getAllBodyPartAllInfo(),
                            worstPainBodyPart = BodyImageMapStorageWithoutLatestFeatures.getWorstPainSelectedIDs(),
                            worstPainBodyPartID,
                            bodyPart;

                        view = view.questionnaire;

                        //send ITs for selected body parts with response as 1
                        for(let index=0; index<allBodyParts.length; index++) {
                            bodyPart = allBodyParts[index];
                            if(bodyPart.selected) {
                                if(view instanceof BaseQuestionnaireView)
                                {
                                    view.addIT({
                                        question_id: bodyPart.IT,
                                        questionnaire_id: questionnaire,
                                        response: "1",
                                        IG: 'BPIDiagram',
                                        IGR: 0,
                                        IT: bodyPart.IT
                                    });
                                }
                            }
                        }

                        //send the worst pain IT with response as the number associated with the body part
                        if(!worstPainBodyPart || worstPainBodyPart.length == 0) {
                            worstPainBodyPartID = "";
                        } else {
                            worstPainBodyPartID = worstPainBodyPart[0];
                        }

                        //ID is also the value of the response
                        if(view instanceof BaseQuestionnaireView)
                        {
                            view.addIT({
                                question_id: 'BDYMST1L',
                                questionnaire_id: questionnaire,
                                response: worstPainBodyPartID,
                                IG: 'BPIDiagram',
                                IGR: 0,
                                IT: 'BDYMST1L'
                            });
                        }

                        let bodyPortionToClear = BodyImageMapStorageWithoutLatestFeatures.allBodyPortionText;
                        BodyImageMapStorageWithoutLatestFeatures.clearAllSelectBodyPartsForBodyPortion(bodyPortionToClear);
                        BodyImageMapStorageWithoutLatestFeatures.removeAllWorstPainSelectedID();
                        BodyImageMapStorageWithoutLatestFeatures.setNoPainSelected(false);
                        done();
                    }
                }
            ]
        }
    ]
};
