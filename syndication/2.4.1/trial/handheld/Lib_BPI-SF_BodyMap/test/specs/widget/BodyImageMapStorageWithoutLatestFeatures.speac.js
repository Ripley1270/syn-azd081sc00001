/**
 * BodyImageMapStorageWithoutLatestFeatures
 * @Created By - Udit Adhikari on August 16th, 2017
 */

import 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';
import 'core/resources/Templates';

import BodyImageMapStorageWithoutLatestFeatures from 'trial/assets/study-design/library/handheld/BPISF2/widget/BodyImageMapStorageWithoutLatestFeatures';

export default class BodyImageMapStorageWithoutLatestFeaturesTests {

    constructList() {

        this.list = [
            {"value": "1", "IT": "FR_HEA1B"},
            {"value": "2", "IT": "FR_SHL1B"},
            {"value": "4", "IT": "FR_SHL2B"},
            {"value": "3", "IT": "FR_THR1B"},
            {"value": "6", "IT": "FR_THR2B"},
            {"value": "7", "IT": "FR_THR3B"},
            {"value": "8", "IT": "FR_THR4B"},
            {"value": "10", "IT": "FR_THR5B"},
            {"value": "11", "IT": "FR_THR6B"},
            {"value": "12", "IT": "FR_THR7B"},
            {"value": "13", "IT": "FR_THR8B"},
            {"value": "14", "IT": "FR_THR9B"},
            {"value": "5", "IT": "FR_ARM1B"},
            {"value": "9", "IT": "FR_ARM2B"},
            {"value": "34", "IT": "FR_ARM3B"},
            {"value": "35", "IT": "FR_ARM4B"},
            {"value": "50", "IT": "FR_ARM5B"},
            {"value": "51", "IT": "FR_ARM6B"},
            {"value": "15", "IT": "FR_LEG1B"},
            {"value": "16", "IT": "FR_LEG2B"},
            {"value": "36", "IT": "FR_LEG3B"},
            {"value": "37", "IT": "FR_LEG4B"},
            {"value": "38", "IT": "FR_LEG5B"},
            {"value": "39", "IT": "FR_LEG6B"},
            {"value": "40", "IT": "FR_LEG7B"},
            {"value": "41", "IT": "FR_LEG8B"},
            {"value": "17", "IT": "BK_HEA1B"},
            {"value": "18", "IT": "BK_NCK1B"},
            {"value": "19", "IT": "BK_SHL1B"},
            {"value": "21", "IT": "BK_SHL2B"},
            {"value": "20", "IT": "BK_THR1B"},
            {"value": "23", "IT": "BK_THR2B"},
            {"value": "24", "IT": "BK_THR3B"},
            {"value": "25", "IT": "BK_THR4B"},
            {"value": "27", "IT": "BK_THR5B"},
            {"value": "28", "IT": "BK_THR6B"},
            {"value": "29", "IT": "BK_THR7B"},
            {"value": "30", "IT": "BK_THR8B"},
            {"value": "31", "IT": "BK_THR9B"},
            {"value": "22", "IT": "BK_ARM1B"},
            {"value": "26", "IT": "BK_ARM2B"},
            {"value": "42", "IT": "BK_ARM3B"},
            {"value": "43", "IT": "BK_ARM4B"},
            {"value": "52", "IT": "BK_ARM5B"},
            {"value": "53", "IT": "BK_ARM6B"},
            {"value": "32", "IT": "BK_LEG1B"},
            {"value": "33", "IT": "BK_LEG2B"},
            {"value": "44", "IT": "BK_LEG3B"},
            {"value": "45", "IT": "BK_LEG4B"},
            {"value": "46", "IT": "BK_LEG5B"},
            {"value": "47", "IT": "BK_LEG6B"},
            {"value": "48", "IT": "BK_LEG7B"},
            {"value": "49", "IT": "BK_LEG8B"}
        ];
    }

    execTests() {
        this.constructList();

        describe('Testing BodyImageMapStorageWithoutLatestFeatures class of custom body image map widget', () => {

            describe('Contants should be defined', () => {

                it('\'BODY_PORTION_FRONT_TOP\' constant text is defined', () => {
                    let frontTop = BodyImageMapStorageWithoutLatestFeatures.frontTopBodyPortionText,
                        length = frontTop.length;

                    expect(length).toBeGreaterThan(1);
                });

                it('\'BODY_PORTION_FRONT_BOT\' constant text is defined', () => {
                    let frontBot = BodyImageMapStorageWithoutLatestFeatures.frontBotBodyPortionText,
                        length = frontBot.length;

                    expect(length).toBeGreaterThan(1);
                });

                it('\'BODY_PORTION_BACK_TOP\' constant text is defined', () => {
                    let backTop = BodyImageMapStorageWithoutLatestFeatures.backTopBodyPortionText,
                        length = backTop.length;

                    expect(length).toBeGreaterThan(1);
                });

                it('\'BODY_PORTION_BACK_BOT\' constant text is defined', () => {
                    let backBot = BodyImageMapStorageWithoutLatestFeatures.backBotBodyPortionText,
                        length = backBot.length;

                    expect(length).toBeGreaterThan(1);
                });

                it('\'BODY_PORTION_ALL\' constant text is defined', () => {
                    let allBody = BodyImageMapStorageWithoutLatestFeatures.allBodyPortionText,
                        length = allBody.length;

                    expect(length).toBeGreaterThan(1);
                });

            });

            describe('Initialization functions should set properties correctly', () => {

                describe('After calling constructBodyPartsFromList() length of this.bodyParts array should be 53', () => {

                    let allBodyPartInfo;

                    beforeAll(() => {
                        BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);
                        allBodyPartInfo = BodyImageMapStorageWithoutLatestFeatures.getAllBodyPartAllInfo();
                    });

                    it("allBodyPartInfo should return length equal to 53", ()=>{
                        let length = allBodyPartInfo.length;
                        expect(length).toBe(53);
                    });
                });

                describe('After calling constructSelectedBodyIDSSets() selected arrays should be empty', ()=> {

                    it('selectedFrontTopIDs array is empty', ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                        let length = BodyImageMapStorageWithoutLatestFeatures.selectedFrontTopIDs.length;
                        expect(length).toBe(0);
                    });
                    it('selectedFrontBotIDs array is empty', ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                        let length = BodyImageMapStorageWithoutLatestFeatures.selectedFrontBotIDs.length;
                        expect(length).toBe(0);
                    });
                    it('selectedBackTopIDs array is empty', ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                        let length = BodyImageMapStorageWithoutLatestFeatures.selectedBackTopIDs.length;
                        expect(length).toBe(0);
                    });
                    it('selectedBackBotIDs array is empty', ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                        let length = BodyImageMapStorageWithoutLatestFeatures.selectedBackBotIDs.length;
                        expect(length).toBe(0);
                    });
                    it('worstPainSelectedID array is empty', ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                        let length = BodyImageMapStorageWithoutLatestFeatures.worstPainSelectedID.length;
                        expect(length).toBe(0);
                    });
                });
            });

            describe('getBodyPartAllInfo() should return correct info object for an ID', () => {
                //as a test we will pass in the id of 1 and we should get in return the object that looks like follows:
                //{"id" : "1", "IT": "FR_HEA1B", "bodyPortion" : "FrontTop", "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false}

                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);

                describe('Testing getBodyPartAllInfo() with the ID of "1" belonging to front top body portion', ()=> {

                    let bodyPartInfo = BodyImageMapStorageWithoutLatestFeatures.getBodyPartAllInfo("1");

                    it("'id' of the bodyPartInfo should be 1", ()=>{
                        expect(bodyPartInfo.id).toBe("1");
                    });
                    it("'IT' of the bodyPartInfo should be 'FR_HEA1B'", ()=>{
                        expect(bodyPartInfo.IT).toBe("FR_HEA1B");
                    });
                    it("'bodyPortion' of the bodyPartInfo should be 'FrontTop'", ()=>{
                        expect(bodyPartInfo.bodyPortion).toBe("FrontTop");
                    });
                    it("'nextScreen' of the bodyPartInfo should be 'BPID_BPID030'", ()=>{
                        expect(bodyPartInfo.nextScreen).toBe("BPID_BPID030");
                    });
                    it("'nextScreen2' of the bodyPartInfo should be 'BPID_BPID110'", ()=> {
                        expect(bodyPartInfo.nextScreen2).toBe("BPID_BPID110");
                    });
                    it("'selected' of the bodyPartInfo should be 'false'", ()=> {
                        expect(bodyPartInfo.selected).toBe(false);
                    });
                });

                describe('Testing getBodyPartAllInfo() with the ID of "14" belonging to front bottom body portion', ()=> {

                    let bodyPartInfo = BodyImageMapStorageWithoutLatestFeatures.getBodyPartAllInfo("14");

                    it("'id' of the bodyPartInfo should be 1", ()=>{
                        expect(bodyPartInfo.id).toBe("14");
                    });
                    it("'IT' of the bodyPartInfo should be 'FR_THR9B'", ()=>{
                        expect(bodyPartInfo.IT).toBe("FR_THR9B");
                    });
                    it("'bodyPortion' of the bodyPartInfo should be 'FrontBot'", ()=>{
                        expect(bodyPartInfo.bodyPortion).toBe("FrontBot");
                    });
                    it("'nextScreen' of the bodyPartInfo should be 'BPID_BPID045'", ()=>{
                        expect(bodyPartInfo.nextScreen).toBe("BPID_BPID045");
                    });
                    it("'nextScreen2' of the bodyPartInfo should be 'BPID_BPID125'", ()=> {
                        expect(bodyPartInfo.nextScreen2).toBe("BPID_BPID125");
                    });
                    it("'selected' of the bodyPartInfo should be 'false'", ()=> {
                        expect(bodyPartInfo.selected).toBe(false);
                    });
                });

                describe('Testing getBodyPartAllInfo() with the ID of "17" belonging to back top body portion', ()=> {

                    let bodyPartInfo = BodyImageMapStorageWithoutLatestFeatures.getBodyPartAllInfo("17");

                    it("'id' of the bodyPartInfo should be 17", ()=>{
                        expect(bodyPartInfo.id).toBe("17");
                    });
                    it("'IT' of the bodyPartInfo should be 'BK_HEA1B'", ()=>{
                        expect(bodyPartInfo.IT).toBe("BK_HEA1B");
                    });
                    it("'bodyPortion' of the bodyPartInfo should be 'BackTop'", ()=>{
                        expect(bodyPartInfo.bodyPortion).toBe("BackTop");
                    });
                    it("'nextScreen' of the bodyPartInfo should be 'BPID_BPID050'", ()=>{
                        expect(bodyPartInfo.nextScreen).toBe("BPID_BPID050");
                    });
                    it("'nextScreen2' of the bodyPartInfo should be 'BPID_BPID130'", ()=> {
                        expect(bodyPartInfo.nextScreen2).toBe("BPID_BPID130");
                    });
                    it("'selected' of the bodyPartInfo should be 'false'", ()=> {
                        expect(bodyPartInfo.selected).toBe(false);
                    });
                });

                describe('Testing getBodyPartAllInfo() with the ID of "29" belonging to back bottom body portion', ()=> {

                    let bodyPartInfo = BodyImageMapStorageWithoutLatestFeatures.getBodyPartAllInfo("29");

                    it("'id' of the bodyPartInfo should be 29", ()=>{
                        expect(bodyPartInfo.id).toBe("29");
                    });
                    it("'IT' of the bodyPartInfo should be 'BK_THR7B'", ()=>{
                        expect(bodyPartInfo.IT).toBe("BK_THR7B");
                    });
                    it("'bodyPortion' of the bodyPartInfo should be 'BackBot'", ()=>{
                        expect(bodyPartInfo.bodyPortion).toBe("BackBot");
                    });
                    it("'nextScreen' of the bodyPartInfo should be 'BPID_BPID065'", ()=>{
                        expect(bodyPartInfo.nextScreen).toBe("BPID_BPID065");
                    });
                    it("'nextScreen2' of the bodyPartInfo should be 'BPID_BPID145'", ()=> {
                        expect(bodyPartInfo.nextScreen2).toBe("BPID_BPID145");
                    });
                    it("'selected' of the bodyPartInfo should be 'false'", ()=> {
                        expect(bodyPartInfo.selected).toBe(false);
                    });
                });

            });

            describe("getBodyPartITInfo() should return correct 'IT' attribute for an ID", ()=> {

                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);

                describe('Testing getBodyPartITInfo() with the ID of "5"', ()=> {
                    let bodyPartIT = BodyImageMapStorageWithoutLatestFeatures.getBodyPartITInfo("5");
                    it("'IT' should be 'FR_ARM1B'", ()=>{
                        expect(bodyPartIT).toBe("FR_ARM1B");
                    });
                });

                describe('Testing getBodyPartITInfo() with the ID of "38"', ()=> {
                    let bodyPartIT = BodyImageMapStorageWithoutLatestFeatures.getBodyPartITInfo("38");
                    it("'IT' should be 'FR_LEG5B'", ()=>{
                        expect(bodyPartIT).toBe("FR_LEG5B");
                    });
                });

                describe('Testing getBodyPartITInfo() with the ID of "24"', ()=> {
                    let bodyPartIT = BodyImageMapStorageWithoutLatestFeatures.getBodyPartITInfo("24");
                    it("'IT' should be 'BK_THR3B'", ()=>{
                        expect(bodyPartIT).toBe("BK_THR3B");
                    });
                });

                describe('Testing getBodyPartITInfo() with the ID of "46"', ()=> {
                    let bodyPartIT = BodyImageMapStorageWithoutLatestFeatures.getBodyPartITInfo("46");
                    it("'IT' should be 'BK_LEG5B'", ()=>{
                        expect(bodyPartIT).toBe("BK_LEG5B");
                    });
                });

            });

            describe("getBodyPartBodyPortionInfo() should return correct 'body portion' attribute for an ID", ()=> {

                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);

                describe('Testing getBodyPartBodyPortionInfo() with the ID of "12"', ()=> {
                    let bodyPortion = BodyImageMapStorageWithoutLatestFeatures.getBodyPartBodyPortionInfo("12");
                    it("'bodyPortion' should be 'FrontTop'", ()=>{
                        expect(bodyPortion).toBe("FrontTop");
                    });
                });

                describe('Testing getBodyPartBodyPortionInfo() with the ID of "39"', ()=> {
                    let bodyPortion = BodyImageMapStorageWithoutLatestFeatures.getBodyPartBodyPortionInfo("39");
                    it("'bodyPortion' should be 'FrontBot'", ()=>{
                        expect(bodyPortion).toBe("FrontBot");
                    });
                });

                describe('Testing getBodyPartBodyPortionInfo() with the ID of "43"', ()=> {
                    let bodyPortion = BodyImageMapStorageWithoutLatestFeatures.getBodyPartBodyPortionInfo("43");
                    it("'bodyPortion' should be 'BackTop'", ()=>{
                        expect(bodyPortion).toBe("BackTop");
                    });
                });

                describe('Testing getBodyPartBodyPortionInfo() with the ID of "33"', ()=> {
                    let bodyPortion = BodyImageMapStorageWithoutLatestFeatures.getBodyPartBodyPortionInfo("33");
                    it("'bodyPortion' should be 'BackBot'", ()=>{
                        expect(bodyPortion).toBe("BackBot");
                    });
                });
            });

            describe("getBodyPartNextScreenInfo() should return correct 'next screen' attribute for an ID", ()=> {

                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);

                describe('Testing getBodyPartNextScreenInfo() with the ID of "34"', ()=> {
                    let nextScreen = BodyImageMapStorageWithoutLatestFeatures.getBodyPartNextScreenInfo("34");
                    it("'nextScreen' should be 'BPID_BPID030'", ()=>{
                        expect(nextScreen).toBe("BPID_BPID030");
                    });
                });

                describe('Testing getBodyPartNextScreenInfo() with the ID of "16"', ()=> {
                    let nextScreen = BodyImageMapStorageWithoutLatestFeatures.getBodyPartNextScreenInfo("16");
                    it("'nextScreen' should be 'BPID_BPID045'", ()=>{
                        expect(nextScreen).toBe("BPID_BPID045");
                    });
                });

                describe('Testing getBodyPartNextScreenInfo() with the ID of "27"', ()=> {
                    let nextScreen = BodyImageMapStorageWithoutLatestFeatures.getBodyPartNextScreenInfo("27");
                    it("'nextScreen' should be 'BPID_BPID050'", ()=>{
                        expect(nextScreen).toBe("BPID_BPID050");
                    });
                });

                describe('Testing getBodyPartNextScreenInfo() with the ID of "44"', ()=> {
                    let nextScreen = BodyImageMapStorageWithoutLatestFeatures.getBodyPartNextScreenInfo("44");
                    it("'nextScreen' should be 'BPID_BPID065'", ()=>{
                        expect(nextScreen).toBe("BPID_BPID065");
                    });
                });
            });

            describe("getBodyPartNextScreen2Info() should return correct 'next screen 2' attribute for an ID", ()=> {

                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);

                describe('Testing getBodyPartNextScreen2Info() with the ID of "51"', ()=> {
                    let nextScreen2 = BodyImageMapStorageWithoutLatestFeatures.getBodyPartNextScreen2Info("51");
                    it("'nextScreen2' should be 'BPID_BPID110'", ()=>{
                        expect(nextScreen2).toBe("BPID_BPID110");
                    });
                });

                describe('Testing getBodyPartNextScreen2Info() with the ID of "41"', ()=> {
                    let nextScreen2 = BodyImageMapStorageWithoutLatestFeatures.getBodyPartNextScreen2Info("41");
                    it("'nextScreen2' should be 'BPID_BPID125'", ()=>{
                        expect(nextScreen2).toBe("BPID_BPID125");
                    });
                });

                describe('Testing getBodyPartNextScreen2Info() with the ID of "23"', ()=> {
                    let nextScreen2 = BodyImageMapStorageWithoutLatestFeatures.getBodyPartNextScreen2Info("23");
                    it("'nextScreen2' should be 'BPID_BPID130'", ()=>{
                        expect(nextScreen2).toBe("BPID_BPID130");
                    });
                });

                describe('Testing getBodyPartNextScreen2Info() with the ID of "32"', ()=> {
                    let nextScreen2 = BodyImageMapStorageWithoutLatestFeatures.getBodyPartNextScreen2Info("32");
                    it("'nextScreen2' should be 'BPID_BPID145'", ()=>{
                        expect(nextScreen2).toBe("BPID_BPID145");
                    });
                });
            });

            describe("getBodyPartSelectedInfo() should return correct 'selected' attribute for an ID", ()=> {

                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);

                describe('Testing getBodyPartNextScreen2Info() with the ID of "51"', ()=> {
                    let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo("8");
                    it("'selected' should be 'false'", ()=>{
                        expect(selected).toBe(false);
                    });
                });

                describe('Testing getBodyPartNextScreen2Info() with the ID of "41"', ()=> {
                    let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo("39");
                    it("'selected' should be 'false'", ()=>{
                        expect(selected).toBe(false);
                    });
                });

                describe('Testing getBodyPartNextScreen2Info() with the ID of "23"', ()=> {
                    let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo("20");
                    it("'selected' should be 'false'", ()=>{
                        expect(selected).toBe(false);
                    });
                });

                describe('Testing getBodyPartNextScreen2Info() with the ID of "30"', ()=> {
                    let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo("30");
                    it("'selected' should be 'false'", ()=>{
                        expect(selected).toBe(false);
                    });
                });
            });

            describe("setBodyPartSelectedInfo() should set the value correctly", ()=> {

                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);

                it('Testing for body part with id of "5", "selected" should be "true"', ()=>{
                    BodyImageMapStorageWithoutLatestFeatures.setBodyPartSelectedInfo("5", true);
                    let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo("5");
                    expect(selected).toBe(true);
                });

                it('Testing for body part with id of "25", "selected" should be "true"', ()=>{
                    BodyImageMapStorageWithoutLatestFeatures.setBodyPartSelectedInfo("25", true);
                    let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo("25");
                    expect(selected).toBe(true);
                });

                it('Testing for body part with id of "38", "selected" should be "true"', ()=>{
                    BodyImageMapStorageWithoutLatestFeatures.setBodyPartSelectedInfo("38", true);
                    let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo("38");
                    expect(selected).toBe(true);
                });

                it('Testing for body part with id of "48", "selected" should be "true"', ()=>{
                    BodyImageMapStorageWithoutLatestFeatures.setBodyPartSelectedInfo("48", true);
                    let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo("48");
                    expect(selected).toBe(true);
                });

            });

            describe("getCorrectSelectedBodyPortionSetObject() should return the correct array for a body portion", ()=> {

                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);
                BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();

                describe("Testing getCorrectSelectedBodyPortionSetObject() for front top body portion correctly", ()=> {
                    BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();

                    it('Selected Front Top array should have length of one ', ()=>{
                        BodyImageMapStorageWithoutLatestFeatures.selectedFrontTopIDs.push("1");
                        let selectedFrontTopList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(BodyImageMapStorageWithoutLatestFeatures.frontTopBodyPortionText),
                            length = selectedFrontTopList.length;
                        expect(length).toBe(1);
                    });
                    it('The first element in Selected Front Top array should be ad id of "1"', ()=>{
                        BodyImageMapStorageWithoutLatestFeatures.selectedFrontTopIDs.push("1");
                        let selectedFrontTopList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(BodyImageMapStorageWithoutLatestFeatures.frontTopBodyPortionText),
                            value = selectedFrontTopList[0];
                        expect(value).toBe("1");
                    });
                });

                describe("Testing getCorrectSelectedBodyPortionSetObject() for front bottom body portion correctly", ()=> {
                    BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();

                    it('Selected Front Bottom array should have length of one ', ()=>{
                        BodyImageMapStorageWithoutLatestFeatures.selectedFrontBotIDs.push("15");
                        let selectedFrontBottomList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(BodyImageMapStorageWithoutLatestFeatures.frontBotBodyPortionText),
                            length = selectedFrontBottomList.length;
                        expect(length).toBe(1);
                    });
                    it('The first element in Selected Front Bottom array should be ad id of "15"', ()=>{
                        BodyImageMapStorageWithoutLatestFeatures.selectedFrontBotIDs.push("15");
                        let selectedFrontBottomList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(BodyImageMapStorageWithoutLatestFeatures.frontBotBodyPortionText),
                            value = selectedFrontBottomList[0];
                        expect(value).toBe("15");
                    });
                });

                describe("Testing getCorrectSelectedBodyPortionSetObject() for back top body portion correctly", ()=> {
                    BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();

                    it('Selected Back Top array should have length of one ', ()=>{
                        BodyImageMapStorageWithoutLatestFeatures.selectedBackTopIDs.push("21");
                        let selectedBackTopList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(BodyImageMapStorageWithoutLatestFeatures.backTopBodyPortionText),
                            length = selectedBackTopList.length;
                        expect(length).toBe(1);
                    });
                    it('The first element in Selected Back Top array should be ad id of "21"', ()=>{
                        BodyImageMapStorageWithoutLatestFeatures.selectedBackTopIDs.push("21");
                        let selectedBackTopList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(BodyImageMapStorageWithoutLatestFeatures.backTopBodyPortionText),
                            value = selectedBackTopList[0];
                        expect(value).toBe("21");
                    });
                });

                describe("Testing getCorrectSelectedBodyPortionSetObject() for back bottom portion correctly", ()=> {
                    BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();

                    it('Selected Back Bottom array should have length of one ', ()=>{
                        BodyImageMapStorageWithoutLatestFeatures.selectedBackBotIDs.push("46");
                        let selectedBackBotList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(BodyImageMapStorageWithoutLatestFeatures.backBotBodyPortionText),
                            length = selectedBackBotList.length;
                        expect(length).toBe(1);
                    });
                    it('The first element in Selected Back Bottom array should be ad id of "46"', ()=>{
                        BodyImageMapStorageWithoutLatestFeatures.selectedBackBotIDs.push("46");
                        let selectedBackBotList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(BodyImageMapStorageWithoutLatestFeatures.backBotBodyPortionText),
                            value = selectedBackBotList[0];
                        expect(value).toBe("46");
                    });
                });
            });

            describe("setCorrectSelectedBodyPortionSetObject() should set the array correctly for a body portion", ()=> {

                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);
                BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();

                describe("Testing setCorrectSelectedBodyPortionSetObject() for Front Top portion correctly", ()=> {

                    let arrayOfIDs = ["6", "12", "51"],
                        bodyPortion = BodyImageMapStorageWithoutLatestFeatures.frontTopBodyPortionText;

                    it('Selected Front Top array should have length of three', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedFromTopList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            length = selectedFromTopList.length;
                        expect(length).toBe(3);
                    });

                    it('Selected Front Top array should have first element with value of "6" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedFromTopList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            firstElem = selectedFromTopList[0];
                        expect(firstElem).toBe("6");
                    });

                    it('Selected Front Top array should have second element with value of "12" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedFromTopList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            secondElem = selectedFromTopList[1];
                        expect(secondElem).toBe("12");
                    });

                    it('Selected Front Top array should have third element with value of "51" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedFromTopList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            thirdElem = selectedFromTopList[2];
                        expect(thirdElem).toBe("51");
                    });
                });

                describe("Testing setCorrectSelectedBodyPortionSetObject() for Front Bottom portion correctly", ()=> {

                    let arrayOfIDs = ["16", "39", "41"],
                        bodyPortion = BodyImageMapStorageWithoutLatestFeatures.frontTopBodyPortionText;

                    it('Selected Front Bottom array should have length of three', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedFromBottomList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            length = selectedFromBottomList.length;
                        expect(length).toBe(3);
                    });

                    it('Selected Front Bottom array should have first element with value of "16" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedFromBottomList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            firstElem = selectedFromBottomList[0];
                        expect(firstElem).toBe("16");
                    });

                    it('Selected Front Bottom array should have second element with value of "39" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedFromBottomList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            secondElem = selectedFromBottomList[1];
                        expect(secondElem).toBe("39");
                    });

                    it('Selected Front Bottom array should have third element with value of "41" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedFromBottomList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            thirdElem = selectedFromBottomList[2];
                        expect(thirdElem).toBe("41");
                    });
                });

                describe("Testing setCorrectSelectedBodyPortionSetObject() for Back Top portion correctly", ()=> {

                    let arrayOfIDs = ["21", "28"],
                        bodyPortion = BodyImageMapStorageWithoutLatestFeatures.backTopBodyPortionText;

                    it('Selected Back Top array should have length of two', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedBackTopList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            length = selectedBackTopList.length;
                        expect(length).toBe(2);
                    });

                    it('Selected Back Top array should have first element with value of "21" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedBackTopList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            firstElem = selectedBackTopList[0];
                        expect(firstElem).toBe("21");
                    });

                    it('Selected Back Top array should have second element with value of "28" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedBackTopList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            secondElem = selectedBackTopList[1];
                        expect(secondElem).toBe("28");
                    });
                });

                describe("Testing setCorrectSelectedBodyPortionSetObject() for Back Bottom portion correctly", ()=> {

                    let arrayOfIDs = ["31", "48"],
                        bodyPortion = BodyImageMapStorageWithoutLatestFeatures.backBotBodyPortionText;

                    it('Selected Back Bottom array should have length of two ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedBackBotList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            length = selectedBackBotList.length;
                        expect(length).toBe(2);
                    });

                    it('Selected Back Bottom array should have first element with value of "31" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedBackBotList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            firstElem = selectedBackBotList[0];
                        expect(firstElem).toBe("31");
                    });

                    it('Selected Back Bottom array should have second element with value of "48" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let selectedBackBotList = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(bodyPortion),
                            secondElem = selectedBackBotList[1];
                        expect(secondElem).toBe("48");
                    });
                });
            });

            describe("checkSelectedBodyPartForID() should return correctly whether a body part was selected for a body portion", ()=> {

                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);
                BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();

                describe("Testing checkSelectedBodyPartForID() for All Body portion", ()=> {
                    it('checkSelectedBodyPartForID() for "All Body portion" should throw an error ', () => {
                        let bodyPortion = BodyImageMapStorageWithoutLatestFeatures.allBodyPortionText,
                            bodyPartID = "1";
                        expect(function() {
                            BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(bodyPortion, bodyPartID)
                        }).toThrow(new Error('You check all body areads add body part ID: ' + bodyPartID + '. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> checkSelectedBodyPartForID of Class -> BodyImageMapStorageWithoutLatestFeatures.'));
                    });
                });

                describe("Testing checkSelectedBodyPartForID() for Front Top portion", ()=> {
                    let arrayOfIDs = ["11", "6"],
                        bodyPortion = BodyImageMapStorageWithoutLatestFeatures.frontTopBodyPortionText;

                    it('checkSelectedBodyPartForID() should return true for id of "11" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let checked = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(bodyPortion, "11");
                        expect(checked).toBe(true);
                    });

                    it('checkSelectedBodyPartForID() should return true for id of "6" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let checked = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(bodyPortion, "6");
                        expect(checked).toBe(true);
                    });

                    it('checkSelectedBodyPartForID() should return false for id of "13" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let checked = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(bodyPortion, "13");
                        expect(checked).toBe(false);
                    });
                });

                describe("Testing checkSelectedBodyPartForID() for Front Bottom portion", ()=> {
                    let arrayOfIDs = ["37"],
                        bodyPortion = BodyImageMapStorageWithoutLatestFeatures.frontBotBodyPortionText;

                    it('checkSelectedBodyPartForID() should return true for id of "37" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let checked = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(bodyPortion, "37");
                        expect(checked).toBe(true);
                    });

                    it('checkSelectedBodyPartForID() should return false for id of "41" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let checked = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(bodyPortion, "41");
                        expect(checked).toBe(false);
                    });
                });

                describe("Testing checkSelectedBodyPartForID() for Back Top portion", ()=> {
                    let arrayOfIDs = ["23"],
                        bodyPortion = BodyImageMapStorageWithoutLatestFeatures.backTopBodyPortionText;

                    it('checkSelectedBodyPartForID() should return true for id of "23" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let checked = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(bodyPortion, "23");
                        expect(checked).toBe(true);
                    });

                    it('checkSelectedBodyPartForID() should return false for id of "52" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let checked = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(bodyPortion, "52");
                        expect(checked).toBe(false);
                    });
                });

                describe("Testing checkSelectedBodyPartForID() for Back Bottom portion", ()=> {
                    let arrayOfIDs = ["45", "31", "29"],
                        bodyPortion = BodyImageMapStorageWithoutLatestFeatures.backBotBodyPortionText;

                    it('checkSelectedBodyPartForID() should return true for id of "45" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let checked = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(bodyPortion, "45");
                        expect(checked).toBe(true);
                    });

                    it('checkSelectedBodyPartForID() should return true for id of "31" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let checked = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(bodyPortion, "31");
                        expect(checked).toBe(true);
                    });

                    it('checkSelectedBodyPartForID() should return true for id of "29" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let checked = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(bodyPortion, "29");
                        expect(checked).toBe(true);
                    });

                    it('checkSelectedBodyPartForID() should return false for id of "30" ', () => {
                        BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(bodyPortion, arrayOfIDs);
                        let checked = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(bodyPortion, "30");
                        expect(checked).toBe(false);
                    });
                });
            });

            describe("clearAllSelectBodyPartsForBodyPortion() should clear selected list for the specified body porition correctly", ()=> {

                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);
                BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();

                let frontTopIDs = ["7","12","51"],
                    frontBotIDs = ["15","39","41"],
                    backTopIDs = ["19","27", "24"],
                    backBotIDs = ["30","48", "33"],
                    allBodyText = BodyImageMapStorageWithoutLatestFeatures.allBodyPortionText,
                    frontTopBodyText = BodyImageMapStorageWithoutLatestFeatures.frontTopBodyPortionText,
                    frontBotBodyText = BodyImageMapStorageWithoutLatestFeatures.frontBotBodyPortionText,
                    backTopBodyText = BodyImageMapStorageWithoutLatestFeatures.backTopBodyPortionText,
                    backBotBodyText = BodyImageMapStorageWithoutLatestFeatures.backBotBodyPortionText;

                describe("Testing clearAllSelectBodyPartsForBodyPortion() for All Body portion", ()=> {

                    //have selected body part IDs for all body portions
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontTopBodyText, frontTopIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontBotBodyText, frontBotIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backTopBodyText, backTopIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backBotBodyText, backBotIDs);

                    //clear all the selected body portion array
                    BodyImageMapStorageWithoutLatestFeatures.clearAllSelectBodyPartsForBodyPortion(allBodyText);
                    let frontTopSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(frontTopBodyText),
                        frontBottomSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(frontBotBodyText),
                        backTopSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(backTopBodyText),
                        backBottomSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(backBotBodyText);

                    it('Selected Front Top IDs array should be empty', () => {
                        let length = frontTopSelectedArray.length;
                        expect(length).toBe(0);
                    });

                    it('Selected Front Bottom IDs array should be empty', () => {
                        let length = frontBottomSelectedArray.length;
                        expect(length).toBe(0);
                    });

                    it('Selected Back Top IDs array should be empty', () => {
                        let length = backTopSelectedArray.length;
                        expect(length).toBe(0);
                    });

                    it('Selected Back Bottom IDs array should be empty', () => {
                        let length = backBottomSelectedArray.length;
                        expect(length).toBe(0);
                    });
                });

                describe("Testing clearAllSelectBodyPartsForBodyPortion() for Front Top Body portion", ()=> {

                    //have selected body part IDs for all body portions
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontTopBodyText, frontTopIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontBotBodyText, frontBotIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backTopBodyText, backTopIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backBotBodyText, backBotIDs);

                    //clear only the selected front top body portion array
                    BodyImageMapStorageWithoutLatestFeatures.clearAllSelectBodyPartsForBodyPortion(frontTopBodyText);
                    let frontTopSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(frontTopBodyText),
                        frontBottomSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(frontBotBodyText),
                        backTopSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(backTopBodyText),
                        backBottomSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(backBotBodyText);

                    it('Selected Front Top IDs array should be empty', () => {
                        let length = frontTopSelectedArray.length;
                        expect(length).toBe(0);
                    });

                    it('Selected Front Bottom IDs array should have length of three', () => {
                        let length = frontBottomSelectedArray.length;
                        expect(length).toBe(3);
                    });

                    it('Selected Back Top IDs array should have length of three', () => {
                        let length = backTopSelectedArray.length;
                        expect(length).toBe(3);
                    });

                    it('Selected Back Bottom IDs array should have length of three', () => {
                        let length = backBottomSelectedArray.length;
                        expect(length).toBe(3);
                    });
                });

                describe("Testing clearAllSelectBodyPartsForBodyPortion() for Front Bottom Body portion", ()=> {

                    //have selected body part IDs for all body portions
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontTopBodyText, frontTopIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontBotBodyText, frontBotIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backTopBodyText, backTopIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backBotBodyText, backBotIDs);

                    //clear only the selected front bottom body portion array
                    BodyImageMapStorageWithoutLatestFeatures.clearAllSelectBodyPartsForBodyPortion(frontBotBodyText);
                    let frontTopSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(frontTopBodyText),
                        frontBottomSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(frontBotBodyText),
                        backTopSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(backTopBodyText),
                        backBottomSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(backBotBodyText);

                    it('Selected Front Top IDs array should have length of three', () => {
                        let length = frontTopSelectedArray.length;
                        expect(length).toBe(3);
                    });

                    it('Selected Front Bottom IDs array should be empty', () => {
                        let length = frontBottomSelectedArray.length;
                        expect(length).toBe(0);
                    });

                    it('Selected Back Top IDs array should have length of three', () => {
                        let length = backTopSelectedArray.length;
                        expect(length).toBe(3);
                    });

                    it('Selected Back Bottom IDs array should have length of three', () => {
                        let length = backBottomSelectedArray.length;
                        expect(length).toBe(3);
                    });
                });

                describe("Testing clearAllSelectBodyPartsForBodyPortion() for Back Top Body portion", ()=> {

                    //have selected body part IDs for all body portions
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontTopBodyText, frontTopIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontBotBodyText, frontBotIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backTopBodyText, backTopIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backBotBodyText, backBotIDs);

                    //clear only the selected back top body portion array
                    BodyImageMapStorageWithoutLatestFeatures.clearAllSelectBodyPartsForBodyPortion(backTopBodyText);
                    let frontTopSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(frontTopBodyText),
                        frontBottomSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(frontBotBodyText),
                        backTopSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(backTopBodyText),
                        backBottomSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(backBotBodyText);

                    it('Selected Front Top IDs array should have length of three', () => {
                        let length = frontTopSelectedArray.length;
                        expect(length).toBe(3);
                    });

                    it('Selected Front Bottom IDs array should have length of three', () => {
                        let length = frontBottomSelectedArray.length;
                        expect(length).toBe(3);
                    });

                    it('Selected Back Top IDs array should be empty', () => {
                        let length = backTopSelectedArray.length;
                        expect(length).toBe(0);
                    });

                    it('Selected Back Bottom IDs array should have length of three', () => {
                        let length = backBottomSelectedArray.length;
                        expect(length).toBe(3);
                    });
                });

                describe("Testing clearAllSelectBodyPartsForBodyPortion() for Back Bottom Body portion", ()=> {

                    //have selected body part IDs for all body portions
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontTopBodyText, frontTopIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontBotBodyText, frontBotIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backTopBodyText, backTopIDs);
                    BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backBotBodyText, backBotIDs);

                    //clear only the selected back bottom body portion array
                    BodyImageMapStorageWithoutLatestFeatures.clearAllSelectBodyPartsForBodyPortion(backBotBodyText);
                    let frontTopSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(frontTopBodyText),
                        frontBottomSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(frontBotBodyText),
                        backTopSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(backTopBodyText),
                        backBottomSelectedArray = BodyImageMapStorageWithoutLatestFeatures.getCorrectSelectedBodyPortionSetObject(backBotBodyText);

                    it('Selected Front Top IDs array should have length of three', () => {
                        let length = frontTopSelectedArray.length;
                        expect(length).toBe(3);
                    });

                    it('Selected Front Bottom IDs array should have length of three', () => {
                        let length = frontBottomSelectedArray.length;
                        expect(length).toBe(3);
                    });

                    it('Selected Back Top IDs array should have length of three', () => {
                        let length = backTopSelectedArray.length;
                        expect(length).toBe(3);
                    });

                    it('Selected Back Bottom IDs array should be empty', () => {
                        let length = backBottomSelectedArray.length;
                        expect(length).toBe(0);
                    });
                });
            });

            describe("addSelectedFromBodyPortion() should add a selected ID only to the specified body porition correctly", ()=> {

                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);
                BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();

                let frontTopBodyText = BodyImageMapStorageWithoutLatestFeatures.frontTopBodyPortionText,
                    frontBotBodyText = BodyImageMapStorageWithoutLatestFeatures.frontBotBodyPortionText,
                    backTopBodyText = BodyImageMapStorageWithoutLatestFeatures.backTopBodyPortionText,
                    backBotBodyText = BodyImageMapStorageWithoutLatestFeatures.backBotBodyPortionText;

                let frontTopID = "34",
                    frontBotID = "16",
                    backTopID = "26",
                    backBotID = "33";

                describe("Testing addSelectedFromBodyPortion() for All Body portion", ()=> {
                    it('addSelectedFromBodyPortion() for "All Body portion" should throw an error ', () => {
                        let bodyPortion = BodyImageMapStorageWithoutLatestFeatures.allBodyPortionText,
                            bodyPartID = "1";
                        expect(function() {
                            BodyImageMapStorageWithoutLatestFeatures.addSelectedFromBodyPortion(bodyPortion, bodyPartID)
                        }).toThrow(new Error('You cannot add body part ID: ' + bodyPartID + ' to all sets. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> addSelectedFromBodyPortion of Class -> BodyImageMapStorageWithoutLatestFeatures.'));
                    });
                });

                describe("Testing addSelectedFromBodyPortion() for Front Top portion", ()=> {
                    it('ID 34 of "Front Top portion" should be in the Front Top selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.addSelectedFromBodyPortion(frontTopBodyText, frontTopID);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(frontTopBodyText, frontTopID);
                        expect(selected).toBe(true);
                    });

                    it('BodyPart with ID 34 should have selected attribute set to true', () => {
                        BodyImageMapStorageWithoutLatestFeatures.addSelectedFromBodyPortion(frontTopBodyText, frontTopID);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(frontTopID);
                        expect(selected).toBe(true);
                    });
                });

                describe("Testing addSelectedFromBodyPortion() for Front Bottom portion", ()=> {
                    it('ID 16 of "Front Bottom portion" should be in the Front Bottom selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.addSelectedFromBodyPortion(frontBotBodyText, frontBotID);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(frontBotBodyText, frontBotID);
                        expect(selected).toBe(true);
                    });
                    it('BodyPart with ID 16 should have selected attribute set to true', () => {
                        BodyImageMapStorageWithoutLatestFeatures.addSelectedFromBodyPortion(frontTopBodyText, frontBotID);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(frontBotID);
                        expect(selected).toBe(true);
                    });
                });

                describe("Testing addSelectedFromBodyPortion() for Back Top portion", ()=> {
                    it('ID 26 of "Back Top portion" should be in the Back Top selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.addSelectedFromBodyPortion(backTopBodyText, backTopID);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(backTopBodyText, backTopID);
                        expect(selected).toBe(true);
                    });
                    it('BodyPart with ID 26 should have selected attribute set to true', () => {
                        BodyImageMapStorageWithoutLatestFeatures.addSelectedFromBodyPortion(frontTopBodyText, backTopID);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(backTopID);
                        expect(selected).toBe(true);
                    });
                });

                describe("Testing addSelectedFromBodyPortion() for Back Bottom portion", ()=> {
                    it('ID 33 of "Back Bottom portion" should be in the Back Bottom selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.addSelectedFromBodyPortion(backBotBodyText, backBotID);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(backBotBodyText, backBotID);
                        expect(selected).toBe(true);
                    });
                    it('BodyPart with ID 33 should have selected attribute set to true', () => {
                        BodyImageMapStorageWithoutLatestFeatures.addSelectedFromBodyPortion(frontTopBodyText, backBotID);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(backBotID);
                        expect(selected).toBe(true);
                    });
                });
            });

            describe("removeSelectedFromBodyPortion() should remove a selected ID only from the specified body porition correctly", ()=> {
                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);
                BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();

                let frontTopIDsToAdd = ["8","13","50"],
                    frontBotIDsToAdd = ["36","38","40"],
                    backTopIDsToAdd = ["19","26", "53"],
                    backBotIDsToAdd = ["32","46", "49"],
                    frontTopIDToRemove = "50",
                    frontBotIDToRemove = "38",
                    backTopIDToRemove = "19",
                    backBotIDToRemove = "32",
                    frontTopBodyText = BodyImageMapStorageWithoutLatestFeatures.frontTopBodyPortionText,
                    frontBotBodyText = BodyImageMapStorageWithoutLatestFeatures.frontBotBodyPortionText,
                    backTopBodyText = BodyImageMapStorageWithoutLatestFeatures.backTopBodyPortionText,
                    backBotBodyText = BodyImageMapStorageWithoutLatestFeatures.backBotBodyPortionText;

                //have selected body part IDs for all body portions
                BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontTopBodyText, frontTopIDsToAdd);
                BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontBotBodyText, frontBotIDsToAdd);
                BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backTopBodyText, backTopIDsToAdd);
                BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backBotBodyText, backBotIDsToAdd);

                describe("Testing removeSelectedFromBodyPortion() for Front Top portion", ()=> {
                    it('ID 50 should not be in the Front Top selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeSelectedFromBodyPortion(frontTopBodyText, frontTopIDToRemove);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(frontTopBodyText, frontTopIDToRemove);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 50 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeSelectedFromBodyPortion(frontTopBodyText, frontTopIDToRemove);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(frontTopIDToRemove);
                        expect(selected).toBe(false);
                    });
                });

                describe("Testing removeSelectedFromBodyPortion() for Front Bottom portion", ()=> {
                    it('ID 38 should not be in the Front Bottom selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeSelectedFromBodyPortion(frontBotBodyText, frontBotIDToRemove);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(frontBotBodyText, frontBotIDToRemove);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 38 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeSelectedFromBodyPortion(frontTopBodyText, frontBotIDToRemove);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(frontBotIDToRemove);
                        expect(selected).toBe(false);
                    });
                });

                describe("Testing removeSelectedFromBodyPortion() for Back Top portion", ()=> {
                    it('ID 19 should not be in the Back Top selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeSelectedFromBodyPortion(backTopBodyText, backTopIDToRemove);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(backTopBodyText, backTopIDToRemove);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 19 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeSelectedFromBodyPortion(frontTopBodyText, backTopIDToRemove);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(backTopIDToRemove);
                        expect(selected).toBe(false);
                    });
                });

                describe("Testing removeSelectedFromBodyPortion() for Back Bottom portion", ()=> {
                    it('ID 32 should not be in the Back Bottom selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeSelectedFromBodyPortion(backBotBodyText, backBotIDToRemove);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(backBotBodyText, backBotIDToRemove);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 32 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeSelectedFromBodyPortion(frontTopBodyText, backBotIDToRemove);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(backBotIDToRemove);
                        expect(selected).toBe(false);
                    });
                });
            });

            describe("removeAllSelectedFromBodyPortion() should remove all selected IDs only from the specified body porition correctly", ()=> {
                BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);
                BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();

                let frontTopBodyText = BodyImageMapStorageWithoutLatestFeatures.frontTopBodyPortionText,
                    frontBotBodyText = BodyImageMapStorageWithoutLatestFeatures.frontBotBodyPortionText,
                    backTopBodyText = BodyImageMapStorageWithoutLatestFeatures.backTopBodyPortionText,
                    backBotBodyText = BodyImageMapStorageWithoutLatestFeatures.backBotBodyPortionText;

                let frontTopIDsToAdd = ["4","9","34"],
                    frontBotIDsToAdd = ["15","39","41"],
                    backTopIDsToAdd = ["17","27", "43"],
                    backBotIDsToAdd = ["30","45", "49"];

                //have selected body part IDs for all body portions
                BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontTopBodyText, frontTopIDsToAdd);
                BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(frontBotBodyText, frontBotIDsToAdd);
                BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backTopBodyText, backTopIDsToAdd);
                BodyImageMapStorageWithoutLatestFeatures.setCorrectSelectedBodyPortionSetObject(backBotBodyText, backBotIDsToAdd);

                describe("Testing removeAllSelectedFromBodyPortion() for All Body portion", ()=> {
                    it('removeAllSelectedFromBodyPortion() for "All Body portion" should throw an error ', () => {
                        let bodyPortion = BodyImageMapStorageWithoutLatestFeatures.allBodyPortionText;
                        expect(function() {
                            BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(bodyPortion)
                        }).toThrow(new Error('You cannot remove body part IDs from all sets. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> removeAllSelectedFromBodyPortion of Class -> BodyImageMapStorageWithoutLatestFeatures.'));
                    });
                });

                describe("Testing removeAllSelectedFromBodyPortion() for Front Top portion", ()=> {
                    it('ID 4 should not be in the Front Top selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(frontTopBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(frontTopBodyText, frontTopIDsToAdd[0]);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 4 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(frontTopBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(frontTopIDsToAdd[0]);
                        expect(selected).toBe(false);
                    });
                    it('ID 9 should not be in the Front Top selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(frontTopBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(frontTopBodyText, frontTopIDsToAdd[1]);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 9 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(frontTopBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(frontTopIDsToAdd[1]);
                        expect(selected).toBe(false);
                    });
                    it('ID 34 should not be in the Front Top selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(frontTopBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(frontTopBodyText, frontTopIDsToAdd[2]);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 34 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(frontTopBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(frontTopIDsToAdd[2]);
                        expect(selected).toBe(false);
                    });
                });

                describe("Testing removeAllSelectedFromBodyPortion() for Front Bottom portion", ()=> {
                    it('ID 15 should not be in the Front Bottom selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(frontBotBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(frontBotBodyText, frontBotIDsToAdd[0]);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 15 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(frontBotBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(frontBotIDsToAdd[0]);
                        expect(selected).toBe(false);
                    });
                    it('ID 39 should not be in the Front Bottom selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(frontBotBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(frontBotBodyText, frontBotIDsToAdd[1]);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 39 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(frontBotBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(frontBotIDsToAdd[1]);
                        expect(selected).toBe(false);
                    });
                    it('ID 41 should not be in the Front Bottom selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(frontBotBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(frontBotBodyText, frontBotIDsToAdd[2]);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 41 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(frontBotBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(frontBotIDsToAdd[2]);
                        expect(selected).toBe(false);
                    });
                });

                describe("Testing removeAllSelectedFromBodyPortion() for Back Top portion", ()=> {
                    it('ID 17 should not be in the Back Top selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(backTopBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(backTopBodyText, backTopIDsToAdd[0]);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 17 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(backTopBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(backTopIDsToAdd[0]);
                        expect(selected).toBe(false);
                    });
                    it('ID 27 should not be in the Back Top selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(backTopBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(backTopBodyText, backTopIDsToAdd[1]);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 27 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(backTopBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(backTopIDsToAdd[1]);
                        expect(selected).toBe(false);
                    });
                    it('ID 43 should not be in the Back Top selected array', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(backTopBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(backTopBodyText, backTopIDsToAdd[2]);
                        expect(selected).toBe(false);
                    });
                    it('BodyPart with ID 43 should have selected attribute set to false', () => {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(backTopBodyText);
                        let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(backTopIDsToAdd[2]);
                        expect(selected).toBe(false);
                    });
                });

                describe("Testing removeAllSelectedFromBodyPortion() for Back Bottom portion", ()=> {
                    Async.it('ID 30 should not be in the Back Bottom selected array', () => {
                        return Q.Promise((resolve) => {
                            BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(backBotBodyText);
                            let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(backBotBodyText, backBotIDsToAdd[0]);
                            expect(selected).toBe(false);
                            resolve();
                        });
                    });
                    Async.it('BodyPart with ID 30 should have selected attribute set to false', () => {
                        return Q.Promise((resolve) => {
                            BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(backBotBodyText);
                            let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(backBotIDsToAdd[0]);
                            expect(selected).toBe(false);
                            resolve();
                        });
                    });
                    Async.it('ID 45 should not be in the Back Bottom selected array', () => {
                        return Q.Promise((resolve) => {
                            BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(backBotBodyText);
                            let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(backBotBodyText, backBotIDsToAdd[1]);
                            expect(selected).toBe(false);
                            resolve();
                        });
                    });
                    Async.it('BodyPart with ID 45 should have selected attribute set to false', () => {
                        return Q.Promise((resolve) => {
                            BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(backBotBodyText);
                            let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(backBotIDsToAdd[1]);
                            expect(selected).toBe(false);
                            resolve();
                        });
                    });

                    Async.it('ID 49 should not be in the Back Bottom selected array', () => {
                        return Q.Promise((resolve) => {
                            BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(backBotBodyText);
                            let selected = BodyImageMapStorageWithoutLatestFeatures.checkSelectedBodyPartForID(backBotBodyText, backBotIDsToAdd[2]);
                            expect(selected).toBe(false);
                            resolve();
                        });
                    });
                    Async.it('BodyPart with ID 49 should have selected attribute set to false', () => {
                        return Q.Promise((resolve) => {
                            BodyImageMapStorageWithoutLatestFeatures.removeAllSelectedFromBodyPortion(backBotBodyText);
                            let selected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(backBotIDsToAdd[2]);
                            expect(selected).toBe(false);
                            resolve();
                        });
                    });
                });
            });

            describe("getNoPainSelected() should return correct selected value for worst pain", ()=> {
                describe("Testing getNoPainSelected() after setting noPainSelected to either true or false", () => {
                    it("getNoPainSelected() should return true", ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.noPainSelected = true;
                        let val = BodyImageMapStorageWithoutLatestFeatures.getNoPainSelected();
                        expect(val).toBe(true);
                    });
                    it("getNoPainSelected() should return false", ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.noPainSelected = false;
                        let val = BodyImageMapStorageWithoutLatestFeatures.getNoPainSelected();
                        expect(val).toBe(false);
                    });
                });
            });

            describe("setNoPainSelected() should set value for worst pain correctly", ()=> {
                describe("Testing setNoPainSelected() after setting noPainSelected to either true or false", () => {
                    it("setNoPainSelected() should set noPainSelected to true", ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.setNoPainSelected(true);
                        let val = BodyImageMapStorageWithoutLatestFeatures.getNoPainSelected();
                        expect(val).toBe(true);
                    });
                    it("setNoPainSelected() should set noPainSelected to false", ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.setNoPainSelected(false);
                        let val = BodyImageMapStorageWithoutLatestFeatures.getNoPainSelected();
                        expect(val).toBe(false);
                    });
                });
            });

            describe("getWorstPainSelectedIDs() should return correct array for selected worst pain IDs", ()=> {
                describe("Testing getWorstPainSelectedIDs() after calling constructSelectedBodyIDSSets()", () => {
                    it("getWorstPainSelectedIDs() should return an empty array", ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                        let array = BodyImageMapStorageWithoutLatestFeatures.getWorstPainSelectedIDs(),
                            length = array.length;
                        expect(length).toBe(0);
                    });
                });
                describe("Testing getWorstPainSelectedIDs() after adding three ids to the selected worst pain IDs array", () => {

                    it("getWorstPainSelectedIDs() should return an array with length of 3", ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.worstPainSelectedID = ["3", "28", "49"];
                        let array = BodyImageMapStorageWithoutLatestFeatures.getWorstPainSelectedIDs(),
                            length = array.length;
                        expect(length).toBe(3);
                    });

                    it("getWorstPainSelectedIDs() should return an array with first element as \"3\"", ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.worstPainSelectedID = ["3", "28", "49"];
                        let array = BodyImageMapStorageWithoutLatestFeatures.getWorstPainSelectedIDs(),
                            elem = array[0];
                        expect(elem).toBe("3");
                    });

                    it("getWorstPainSelectedIDs() should return an array with second element as \"28\"", ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.worstPainSelectedID = ["3", "28", "49"];
                        let array = BodyImageMapStorageWithoutLatestFeatures.getWorstPainSelectedIDs(),
                            elem = array[1];
                        expect(elem).toBe("28");
                    });

                    it("getWorstPainSelectedIDs() should return an array with second element as \"49\"", ()=> {
                        BodyImageMapStorageWithoutLatestFeatures.worstPainSelectedID = ["3", "28", "49"];
                        let array = BodyImageMapStorageWithoutLatestFeatures.getWorstPainSelectedIDs(),
                            elem = array[2];
                        expect(elem).toBe("49");
                    });
                });
            });

            describe("addWorstPainSelectedID() should add a selected worst pain ID to the worstPainSelectedID array", ()=> {

                describe('Testing addWorstPainSelectedID() by adding one id of "15"', ()=>{

                    beforeAll(()=> {
                        BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                        BodyImageMapStorageWithoutLatestFeatures.addWorstPainSelectedID("15");
                    });

                    Async.it('worstPainSelectedID be an array of length 1', ()=> {

                        return Q.promise((resolve) => {
                            let array = BodyImageMapStorageWithoutLatestFeatures.getWorstPainSelectedIDs(),
                                length = array.length;
                            expect(length).toBe(1);
                            resolve();
                        });

                    });
                    Async.it('worstPainSelectedID should have first element as "15"', ()=> {
                        return Q.promise((resolve) => {
                            let array = BodyImageMapStorageWithoutLatestFeatures.getWorstPainSelectedIDs(),
                                elem = array[0];
                            expect(elem).toBe("15");
                            resolve();
                        });
                    });
                });
            });

            describe("checkIfWorstPainArrayContainsID() correctly checks the ID present in worstPainSelectedID array", ()=> {
                describe('Testing checkIfWorstPainArrayContainsID() by adding three ids "7", "31", "48"', ()=>{
                    beforeAll(()=> {
                        BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                        BodyImageMapStorageWithoutLatestFeatures.addWorstPainSelectedID("7");
                        BodyImageMapStorageWithoutLatestFeatures.addWorstPainSelectedID("31");
                        BodyImageMapStorageWithoutLatestFeatures.addWorstPainSelectedID("48");
                    });

                    Async.it('ID -> 7 should be present in the worstPainSelectedID array', ()=> {

                        return Q.promise((resolve) => {
                            let checked = BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainArrayContainsID("7");
                            expect(checked).toBe(true);
                            resolve();
                        });

                    });

                    Async.it('ID -> 31 should be present in the worstPainSelectedID array', ()=> {

                        return Q.promise((resolve) => {
                            let checked = BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainArrayContainsID("31");
                            expect(checked).toBe(true);
                            resolve();
                        });

                    });

                    Async.it('ID -> 48 should be present in the worstPainSelectedID array', ()=> {

                        return Q.promise((resolve) => {
                            let checked = BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainArrayContainsID("48");
                            expect(checked).toBe(true);
                            resolve();
                        });

                    });
                });
            });

            describe("removeWorstPainSelectedID() correctly removes the ID present in worstPainSelectedID array", ()=> {

                beforeAll(()=> {
                    BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                    BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                    BodyImageMapStorageWithoutLatestFeatures.addWorstPainSelectedID("13");
                    BodyImageMapStorageWithoutLatestFeatures.addWorstPainSelectedID("27");
                    BodyImageMapStorageWithoutLatestFeatures.addWorstPainSelectedID("48");
                });

                describe('Testing removeWorstPainSelectedID() by removing id "13"', ()=>{
                    beforeAll(()=> {
                        BodyImageMapStorageWithoutLatestFeatures.removeWorstPainSelectedID("13");
                    });

                    Async.it('ID -> 13 should not be present in the worstPainSelectedID array', ()=> {

                        return Q.promise((resolve) => {
                            let checked = BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainArrayContainsID("13");
                            expect(checked).toBe(false);
                            resolve();
                        });
                    });
                });

                describe('Testing removeWorstPainSelectedID() by removing id "27"', ()=>{
                    beforeAll(()=> {
                        BodyImageMapStorageWithoutLatestFeatures.removeWorstPainSelectedID("27");
                    });

                    Async.it('ID -> 27 should not be present in the worstPainSelectedID array', ()=> {

                        return Q.promise((resolve) => {
                            let checked = BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainArrayContainsID("27");
                            expect(checked).toBe(false);
                            resolve();
                        });
                    });
                });

                describe('Testing removeWorstPainSelectedID() by removing id "48"', ()=>{
                    beforeAll(()=> {
                        BodyImageMapStorageWithoutLatestFeatures.removeWorstPainSelectedID("48");
                    });

                    Async.it('ID -> 48 should not be present in the worstPainSelectedID array', ()=> {

                        return Q.promise((resolve) => {
                            let checked = BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainArrayContainsID("48");
                            expect(checked).toBe(false);
                            resolve();
                        });
                    });
                });
            });

            describe("removeAllWorstPainSelectedID() correctly removes all the ID present in worstPainSelectedID array", ()=> {

                let idArrForTest = ["9", "39", "21", "44"];

                beforeAll(()=> {
                    BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                    BodyImageMapStorageWithoutLatestFeatures.addWorstPainSelectedID(idArrForTest[0]);
                    BodyImageMapStorageWithoutLatestFeatures.addWorstPainSelectedID(idArrForTest[1]);
                    BodyImageMapStorageWithoutLatestFeatures.addWorstPainSelectedID(idArrForTest[2]);
                    BodyImageMapStorageWithoutLatestFeatures.addWorstPainSelectedID(idArrForTest[3]);
                });

                describe('Testing removeAllWorstPainSelectedID() by removing all IDS in the worstPainSelectedID array', ()=>{
                    beforeAll(()=> {
                        BodyImageMapStorageWithoutLatestFeatures.removeAllWorstPainSelectedID();
                    });

                    Async.it('ID -> ' + idArrForTest[0] + ' should not be present in the worstPainSelectedID array', ()=> {
                        return Q.promise((resolve) => {
                            let checked = BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainArrayContainsID(idArrForTest[0]);
                            expect(checked).toBe(false);
                            resolve();
                        });
                    });

                    Async.it('ID -> ' + idArrForTest[1] + ' should not be present in the worstPainSelectedID array', ()=> {
                        return Q.promise((resolve) => {
                            let checked = BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainArrayContainsID(idArrForTest[1]);
                            expect(checked).toBe(false);
                            resolve();
                        });
                    });

                    Async.it('ID -> ' + idArrForTest[2] + ' should not be present in the worstPainSelectedID array', ()=> {
                        return Q.promise((resolve) => {
                            let checked = BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainArrayContainsID(idArrForTest[2]);
                            expect(checked).toBe(false);
                            resolve();
                        });
                    });

                    Async.it('ID -> ' + idArrForTest[3] + ' should not be present in the worstPainSelectedID array', ()=> {
                        return Q.promise((resolve) => {
                            let checked = BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainArrayContainsID(idArrForTest[3]);
                            expect(checked).toBe(false);
                            resolve();
                        });
                    });
                });
            });

            describe("checkIfWorstPainSelectedIDExistsInBodySet() correctly checks the first ID in worstPainSelectedID array to against its checked value of the object in this.bodyParts array", ()=> {

                let idToTest = "48",
                    backBodyText = BodyImageMapStorageWithoutLatestFeatures.backBotBodyPortionText;

                describe('Testing checkIfWorstPainSelectedIDExistsInBodySet() if id ->' + idToTest + ' has been selected earlier', () => {
                    beforeAll(()=> {
                        BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(this.list);
                        BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                        BodyImageMapStorageWithoutLatestFeatures.addSelectedFromBodyPortion(backBodyText, idToTest);
                        BodyImageMapStorageWithoutLatestFeatures.addWorstPainSelectedID(idToTest);
                    });

                    afterAll(()=>{
                        BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                        BodyImageMapStorageWithoutLatestFeatures.removeAllWorstPainSelectedID();
                    });

                    Async.it('checkIfWorstPainSelectedIDExistsInBodySet() should return true for ' + idToTest, () => {
                        return Q.promise((resolve) => {
                            let checked = BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainSelectedIDExistsInBodySet();
                            expect(checked).toBe(true);
                            resolve();
                        });
                    });
                });
            });
        });
    }
}

let testerBodyImageMapStorageWithoutLatestFeatures = new BodyImageMapStorageWithoutLatestFeaturesTests();
testerBodyImageMapStorageWithoutLatestFeatures.execTests();
