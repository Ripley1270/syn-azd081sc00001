import './BodyImageMapStorage';
import './BodyImageMapStorageWithoutLatestFeatures';
import './BPISFBodyDiagram';
import './BPISFBodyDiagramZoom';
import './BPISFBodyDiagramWorstPain';
import './BPISFBodyDiagramWorstPainZoom';
