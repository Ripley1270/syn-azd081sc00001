const BODY_PORTION_FRONT_TOP = "FrontTop";
const BODY_PORTION_FRONT_BOT = "FrontBot";
const BODY_PORTION_BACK_TOP = "BackTop";
const BODY_PORTION_BACK_BOT = "BackBot";
const BODY_PORTION_ALL = "All";

class BodyImageMapStorageWithoutLatestFeatures {

    static get frontTopBodyPortionText() {
        return BODY_PORTION_FRONT_TOP;
    }

    static get frontBotBodyPortionText() {
        return BODY_PORTION_FRONT_BOT;
    }

    static get backTopBodyPortionText() {
        return BODY_PORTION_BACK_TOP;
    }

    static get backBotBodyPortionText() {
        return BODY_PORTION_BACK_BOT;
    }

    static get allBodyPortionText() {
        return BODY_PORTION_ALL;
    }

    constructor()  {
        this.bodyParts = [];
        this.selectedFrontTopIDs = [];
        this.selectedFrontBotIDs = [];
        this.selectedBackTopIDs = [];
        this.selectedBackBotIDs = [];
        this.worstPainSelectedID = [];
        this.recentlySelectedIDs = [];
        this.recentlySelectedIDsToRemove = [];
        this.recentlySelectedWorstPainIDs = [];
        this.recentlySelectedIDsToRemoveWorstPainIDs = [];
        this.noPainSelected = false;
    }

    emptyAllData() {
        this.bodyParts = [];
        this.selectedFrontTopIDs = [];
        this.selectedFrontBotIDs = [];
        this.selectedBackTopIDs = [];
        this.selectedBackBotIDs = [];
        this.worstPainSelectedID = [];
        this.recentlySelectedIDs = [];
        this.recentlySelectedIDsToRemove = [];
        this.recentlySelectedWorstPainIDs = [];
        this.recentlySelectedIDsToRemoveWorstPainIDs = [];
        this.noPainSelected = false;
    }

    static previouslyRecentlySelectedID(idToCheck) {
        let preivouslyRecentlySelected = false;
        _.each(this.recentlySelectedIDs, function(recentlySelectedID) {
            if(recentlySelectedID == idToCheck) {
                preivouslyRecentlySelected = true;
            }
        });
        return preivouslyRecentlySelected;
    }

    static addRecentlySelectedIDs(id) {
        if(!this.recentlySelectedIDs || this.recentlySelectedIDs.length  == 0) {
            this.recentlySelectedIDs = [];
        }
        this.recentlySelectedIDs.push(id);
    }

    static removeRecentlySelectedID(id) {
        let bodyPartIDArr = _.filter(this.recentlySelectedIDs, function(bodyPartID) {
            return bodyPartID !== id;
        });

        this.recentlySelectedIDs = bodyPartIDArr;
    }

    static removeRecentlySelectedIDs() {
        let bodyPartInfo;
        _.each(this.recentlySelectedIDs, (recentlySelectedID) => {
            this.setBodyPartSelectedInfo(recentlySelectedID, false);
        });
        this.emptyRecentlySelectedIDs();
    }

    static addRecentlySelectedIDsToRemove(id) {
        if(!this.recentlySelectedIDsToRemove || this.recentlySelectedIDsToRemove .length  == 0) {
            this.recentlySelectedIDsToRemove  = [];
        }
        this.recentlySelectedIDsToRemove.push(id);
    }

    static detectIfIDPresentInSelectedList(bodyPortionArrName, bodyPartIDToFind) {
        let idFound = false;
        if (bodyPortionArrName === this.allBodyPortionText) {

            throw new Error('You cannot detect body part ID: ' + bodyPartIDToFind + ' from all sets. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> removeSelectedFromBodyPortion of Class -> BodyImageMapStorageWithoutLatestFeatures.');

        } else {

            let arrayToSearch = this.getCorrectSelectedBodyPortionSetObject(bodyPortionArrName);

            //noinspection JSUnusedAssignment
            _.each(arrayToSearch, function(bodyPartIDFromArr) {
                if(bodyPartIDFromArr == bodyPartIDToFind) {
                    idFound = true;
                }
            });

            return idFound;
        }
    }

    static removeRecentlySelectedIDsToRemove() {
        let bodyPartInfo;
        _.each(this.recentlySelectedIDsToRemove, (recentlySelectedID) => {
            this.setBodyPartSelectedInfo(recentlySelectedID, true);
        });
        this.emptyRecentlySelectedIDs();
    }

    static emptyRecentlySelectedIDs() {
        this.recentlySelectedIDs = [];
        this.recentlySelectedIDsToRemove = [];
    }

    static addRecentlySelectedWorstPainIDs(id) {
        if(!this.recentlySelectedWorstPainIDs || this.recentlySelectedWorstPainIDs.length  == 0) {
            this.recentlySelectedWorstPainIDs = [];
        }
        this.recentlySelectedWorstPainIDs.push(id);
    }

    static addRecentlySelectedToRemoveWorstPainIDs(id) {
        if(!this.recentlySelectedIDsToRemoveWorstPainIDs || this.recentlySelectedIDsToRemoveWorstPainIDs.length  == 0) {
            this.recentlySelectedIDsToRemoveWorstPainIDs = [];
        }
        this.recentlySelectedIDsToRemoveWorstPainIDs.push(id);
    }

    static emptyRecentlySelectedWorstPainIDs() {
        this.recentlySelectedWorstPainIDs = [];
        this.recentlySelectedIDsToRemoveWorstPainIDs = [];
    }

    static constructBodyPartsFromList(list) {

        let id,
            value,
            IT,
            bodyPortion,
            nextScreen,
            nextScreen2,
            selected = false;

        this.bodyParts = [];

        _.each(list, (item) => {

            id = item.value;
            value = parseInt(id);
            IT = item.IT;

            if((value >= 1 && value <= 13 )  || (value == 34)|| (value == 35) || (value == 50) || (value == 51)) {

                bodyPortion = this.frontTopBodyPortionText;
                nextScreen = "BPID_BPID030";
                nextScreen2 = "BPID_BPID110";

            } else if((value == 14)|| (value == 15) || (value == 16) || (value >= 36 && value <= 41 )) {

                bodyPortion = this.frontBotBodyPortionText;
                nextScreen = "BPID_BPID045";
                nextScreen2 = "BPID_BPID125";

            } else if((value >= 17 && value <= 28 ) || (value == 42)|| (value == 43) || (value == 52) || (value == 53)) {

                bodyPortion = this.backTopBodyPortionText;
                nextScreen = "BPID_BPID050";
                nextScreen2 = "BPID_BPID130";

            } else if((value >= 29 && value <= 33 ) || (value >= 44 && value <= 49)) {

                bodyPortion = this.backBotBodyPortionText;
                nextScreen = "BPID_BPID065";
                nextScreen2 = "BPID_BPID145";

            }

            this.bodyParts.push({"id" : id, "IT" : IT, "bodyPortion" : bodyPortion, "nextScreen" : nextScreen, "nextScreen2" : nextScreen2, "selected" : selected});
        });
    }

    //noinspection JSUnusedGlobalSymbols
    static constructSelectedBodyIDSSets() {
        this.selectedFrontTopIDs = [];
        this.selectedFrontBotIDs = [];
        this.selectedBackTopIDs = [];
        this.selectedBackBotIDs = [];
        this.worstPainSelectedID = [];
        this.recentlySelectedIDs = [];
        this.recentlySelectedIDsToRemove = [];
        this.recentlySelectedWorstPainIDs = [];
        this.recentlySelectedIDsToRemoveWorstPainIDs = [];
    }

    /**
     * This function will return all of the info on all body parts
     * @returns {Array} - returns the body parts array
     */
    static getAllBodyPartAllInfo() {
        return this.bodyParts;
    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * This function will return all info for a given body part ID
     * @param bodyPartID - ID of the body part
     * @returns {V} = {"IT": , "bodyPortion" : "", "nextScreen" : ""}
     *          Example : for body ID '49', the return object will return - {"IT": "BK_LEG8B", "bodyPortion" : "BackBot", "nextScreen" : "BPID_BPID065"}
     */
    static getBodyPartAllInfo(bodyPartID) {

        let bodyPartArr = _.filter(this.bodyParts, function(bodyPart) {
            return bodyPart.id === bodyPartID;
        });

        let bodyPartObj = bodyPartArr[0];

        return bodyPartObj;
    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * This function will return 'IT' associated with a given body part ID
     * @param bodyPartID - ID of the body part
     * @returns {V} = "IT" of the given body part ID
     *          Example : for body ID '49', the return object will return - "BK_LEG8B"}
     */
    static getBodyPartITInfo(bodyPartID) {
        let bodyPartObj = this.getBodyPartAllInfo(bodyPartID);
        return bodyPartObj.IT;
    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * This function will return "body portion" info for a given body part ID
     * @param bodyPartID - ID of the body part
     * @returns {V} = "bodyPortion" of the given body part ID
     *          Example : for body ID '49', the return object will return - "BackBot"}
     */
    static getBodyPartBodyPortionInfo(bodyPartID) {
        let bodyPartObj = this.getBodyPartAllInfo(bodyPartID);
        return bodyPartObj.bodyPortion;
    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * This function will return "next screen" info for a given body part ID
     * @param bodyPartID - ID of the body part
     * @returns {V} = "bodyPortion" of the given body part ID
     *          Example : for body ID '49', the return object will return - "BPID_BPID065"}
     */
    static getBodyPartNextScreenInfo(bodyPartID) {
        let bodyPartObj = this.getBodyPartAllInfo(bodyPartID);
        return bodyPartObj.nextScreen;
    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * This function will return "next screen 2" info for a given body part ID
     * @param bodyPartID - ID of the body part
     * @returns {V} = "bodyPortion" of the given body part ID
     *          Example : for body ID '49', the return object will return - "BPID_BPID145"}
     */
    static getBodyPartNextScreen2Info(bodyPartID) {
        let bodyPartObj = this.getBodyPartAllInfo(bodyPartID);
        return bodyPartObj.nextScreen2;
    }

    //noinspection JSUnusedGlobalSymbols
    static getBodyPartSelectedInfo(bodyPartID) {
        let bodyPartObj = this.getBodyPartAllInfo(bodyPartID);
        return bodyPartObj.selected;
    }

    static setBodyPartSelectedInfo(bodyPartID, selectedVal) {
        let bodyPartObj = this.getBodyPartAllInfo(bodyPartID);
        bodyPartObj.selected = selectedVal;
    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * This function returns the correct set object for a given body portion
     * @param bodyPortionArrName - can be one of the following :
     *                      "FrontTop" - for front top,
     *                      "FrontBot" - for front bottom,
     *                      "BackTop"  - for front bottom,
     *                      "BackBot" - for front bottom,
     *                      "All" - for frontTop, frontBot, backTop, backBot
     * @returns {*} - one of the following : selectedFrontTopIDs, selectedFrontBotIDs, selectedBackTopIDs, selectedBackBotIDs
     */
    static getCorrectSelectedBodyPortionSetObject(bodyPortionArrName) {

        let bodyPortionArr = null;

        if (bodyPortionArrName === this.frontTopBodyPortionText) {
            bodyPortionArr = this.selectedFrontTopIDs;
        } else if (bodyPortionArrName === this.frontBotBodyPortionText) {
            bodyPortionArr = this.selectedFrontBotIDs;
        } else if (bodyPortionArrName === this.backTopBodyPortionText) {
            bodyPortionArr = this.selectedBackTopIDs;
        } else if (bodyPortionArrName === this.backBotBodyPortionText) {
            bodyPortionArr = this.selectedBackBotIDs;
        }

        return bodyPortionArr;
    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * This function sets the correct array object for a given body portion to a new array
     * @param bodyPortionArrName - can be one of the following :
     *                      "FrontTop" - for front top,
     *                      "FrontBot" - for front bottom,
     *                      "BackTop"  - for front bottom,
     *                      "BackBot" - for front bottom,
     *                      "All" - for frontTop, frontBot, backTop, backBot
     * @param newArray - new array
     */
    static setCorrectSelectedBodyPortionSetObject(bodyPortionArrName, newArray) {

        if (bodyPortionArrName === this.frontTopBodyPortionText) {
            this.selectedFrontTopIDs = newArray;
        } else if (bodyPortionArrName === this.frontBotBodyPortionText) {
            this.selectedFrontBotIDs = newArray;
        } else if (bodyPortionArrName === this.backTopBodyPortionText) {
            this.selectedBackTopIDs = newArray;
        } else if (bodyPortionArrName === this.backBotBodyPortionText) {
            this.selectedBackBotIDs = newArray;
        }
    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * This function returns true or false if bodyPartID has been selected. True if yes, false if no
     * @param bodyPortionArrName - can be one of the following :
     *                      "FrontTop" - for front top,
     *                      "FrontBot" - for front bottom,
     *                      "BackTop"  - for front bottom,
     *                      "BackBot" - for front bottom,
     *                      "All" - for frontTop, frontBot, backTop, backBot
     * @param bodyPartID - ID of the body part
     * @returns {boolean|*}
     */
    static checkSelectedBodyPartForID(bodyPortionArrName, bodyPartID) {

        if (bodyPortionArrName === this.allBodyPortionText) {

            throw new Error('You check all body areads add body part ID: ' + bodyPartID + '. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> checkSelectedBodyPartForID of Class -> BodyImageMapStorageWithoutLatestFeatures.');

        } else {

            let setObject = this.getCorrectSelectedBodyPortionSetObject(bodyPortionArrName);
            let arr = _.filter(setObject, function(bodyPartFromArr) {
                return bodyPartFromArr === bodyPartID;
            });
            let obj = arr[0];

            let exists = false;

            if(obj) {
                exists = true;
            }
            return exists;
        }
    }

    //noinspection JSUnusedGlobalSymbols
    /**
    * This function clears all the selected body part IDs for a given body portion
    * @param bodyPortion - can be one of the following :
    *                      "FrontTop" - for front top,
    *                      "FrontBot" - for front bottom,
    *                      "BackTop"  - for front bottom,
    *                      "BackBot" - for front bottom,
    *                      "All" - for frontTop, frontBot, backTop, backBot
    */
    static clearAllSelectBodyPartsForBodyPortion(bodyPortion) {

        if (bodyPortion === this.allBodyPortionText) {

            this.selectedFrontTopIDs = [];
            this.selectedFrontBotIDs = [];
            this.selectedBackTopIDs = [];
            this.selectedBackBotIDs = [];
            this.recentlySelectedIDs = [];
            this.recentlySelectedIDsToRemove = [];

            this.worstPainSelectedID = [];
            this.recentlySelectedWorstPainIDs = [];
            this.recentlySelectedIDsToRemoveWorstPainIDs = [];


            _.each(this.bodyParts, function(bodyPart) {
                bodyPart.selected = false;
            });

        } else {

            let arrayToClear = this.getCorrectSelectedBodyPortionSetObject(bodyPortion);
            let _that = this;

            _.each(arrayToClear, function(bodyPartID){
                _that.setBodyPartSelectedInfo(bodyPartID, false);
            });

            //noinspection JSUnusedAssignment
            this.setCorrectSelectedBodyPortionSetObject(bodyPortion, []);

        }
    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * This function adds a "body part ID" to a set of given body portion
     * @param bodyPortionArrName - can be one of the following :
     *                             "FrontTop" - for front top,
     *                             "FrontBot" - for front bottom,
     *                             "BackTop"  - for front bottom,
     *                             "BackBot" - for front bottom,
     * @param bodyPartID - ID of the body part
     */
    static addSelectedFromBodyPortion(bodyPortionArrName, bodyPartID) {

        if (bodyPortionArrName === this.allBodyPortionText) {

            throw new Error('You cannot add body part ID: ' + bodyPartID + ' to all sets. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> addSelectedFromBodyPortion of Class -> BodyImageMapStorageWithoutLatestFeatures.');

        } else {

            let arrayToManipulate = this.getCorrectSelectedBodyPortionSetObject(bodyPortionArrName);
            arrayToManipulate.push(bodyPartID);
            this.setBodyPartSelectedInfo(bodyPartID, true);

        }
    }

    static addSelectedFromRecentlySelected() {
        let _that = this,
            bodyPortionArrName,
            arrayToManipulate;

        _.each(this.recentlySelectedIDs, function(bodyPartID) {
            bodyPortionArrName = _that. getBodyPartBodyPortionInfo(bodyPartID);
            arrayToManipulate = _that.getCorrectSelectedBodyPortionSetObject(bodyPortionArrName);
            arrayToManipulate.push(bodyPartID);
            _that.setBodyPartSelectedInfo(bodyPartID, true);
        });
    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * This function removes a "body part ID" from a set of given body portion
     * @param bodyPortionArrName - can be one of the following :
     *                             "FrontTop" - for front top,
     *                             "FrontBot" - for front bottom,
     *                             "BackTop"  - for front bottom,
     *                             "BackBot" - for front bottom
     * @param bodyPartID - ID of the body part
     */
    static removeSelectedFromBodyPortion(bodyPortionArrName, bodyPartID) {

        if (bodyPortionArrName === this.allBodyPortionText) {

            throw new Error('You cannot remove body part ID: ' + bodyPartID + ' from all sets. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> removeSelectedFromBodyPortion of Class -> BodyImageMapStorageWithoutLatestFeatures.');

        } else {

            let arrayToManipulate = this.getCorrectSelectedBodyPortionSetObject(bodyPortionArrName);

            //noinspection JSUnusedAssignment
            let newArray = arrayToManipulate.filter(function( bodyPartIDFromArr ) {
                return bodyPartIDFromArr !== bodyPartID;
            });

            this.setCorrectSelectedBodyPortionSetObject(bodyPortionArrName, newArray);

            this.setBodyPartSelectedInfo(bodyPartID, false);
        }
    }

    static removeRecentlySelectedFromBodyPortion(bodyPortionArrName) {

        if (bodyPortionArrName === this.allBodyPortionText) {

            throw new Error('You cannot remove body part IDs from all sets. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> removeSelectedFromBodyPortion of Class -> BodyImageMapStorageWithoutLatestFeatures.');

        } else {

            let arrayToManipulate = this.getCorrectSelectedBodyPortionSetObject(bodyPortionArrName),
                _that = this,
                recentlySelectedID;

            for(let index=0; index < _that.recentlySelectedIDs.length; index++) {

                recentlySelectedID = _that.recentlySelectedIDs[index];

                for(let i=0; i<arrayToManipulate.length; i++) {
                    if(recentlySelectedID == arrayToManipulate[i]) {
                        _that.setBodyPartSelectedInfo(recentlySelectedID, false);
                        arrayToManipulate.splice(i, 1);
                        break;
                    }
                }
            }

            this.setCorrectSelectedBodyPortionSetObject(bodyPortionArrName, arrayToManipulate);
            this.recentlySelectedIDs = [];
        }
    }

    static removeRecentlySelectedToRemoveFromBodyPortion(bodyPortionArrName) {

        if (bodyPortionArrName === this.allBodyPortionText) {

            throw new Error('You cannot remove body part IDs from all sets. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> removeSelectedFromBodyPortion of Class -> BodyImageMapStorageWithoutLatestFeatures.');

        } else {

            let arrayToManipulate = this.getCorrectSelectedBodyPortionSetObject(bodyPortionArrName),
                _that = this,
                recentlySelectedIDToRemove;

            for(let index=0; index < _that.recentlySelectedIDsToRemove.length; index++) {

                recentlySelectedIDToRemove = _that.recentlySelectedIDsToRemove[index];
                _that.setBodyPartSelectedInfo(recentlySelectedIDToRemove, true);
                arrayToManipulate.push(recentlySelectedIDToRemove);

            }

            this.setCorrectSelectedBodyPortionSetObject(bodyPortionArrName, arrayToManipulate);
            this.recentlySelectedIDsToRemove = [];
        }
    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * This function removes all "body part IDs" from the set of given body portion
     * @param bodyPortionArrName - can be one of the following :
     *                             "FrontTop" - for front top,
     *                             "FrontBot" - for front bottom,
     *                             "BackTop"  - for front bottom,
     *                             "BackBot" - for front bottom
     */
    static removeAllSelectedFromBodyPortion(bodyPortionArrName) {

        if (bodyPortionArrName === this.allBodyPortionText) {

            throw new Error('You cannot remove body part IDs from all sets. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> removeAllSelectedFromBodyPortion of Class -> BodyImageMapStorageWithoutLatestFeatures.');

        } else {

            let setObject = this.getCorrectSelectedBodyPortionSetObject(bodyPortionArrName);
            let _that = this;

            _.each(setObject, function(bodyPartID) {
                _that.setBodyPartSelectedInfo(bodyPartID, false);
            });

            //noinspection JSUnusedAssignment
            this.setCorrectSelectedBodyPortionSetObject(bodyPortionArrName, []);
        }
    }

    //noinspection JSUnusedGlobalSymbols
    static getNoPainSelected() {
        return this.noPainSelected;
    }

    //noinspection JSUnusedGlobalSymbols
    static setNoPainSelected(val) {
        this.noPainSelected = val;
    }

    //noinspection JSUnusedGlobalSymbols
    static getWorstPainSelectedIDs() {
        return this.worstPainSelectedID;
    }

    //noinspection JSUnusedGlobalSymbols
    static addWorstPainSelectedID(selectedID) {
        this.worstPainSelectedID.push(selectedID);
    }

    //noinspection JSUnusedGlobalSymbols
    static checkIfWorstPainArrayContainsID(selectedID) {
        let worstPainArr = _.filter(this.worstPainSelectedID, function(worstPainID) {
            return worstPainID === selectedID;
        });

        let worstPainObj = worstPainArr[0];
        let exists = false;

        if(worstPainObj) {
            exists = true;
        }

        return exists;
    }

    static checkIfWorsePainArryContainsBodyRegion(bodyPortion) {

        let regionFound = false,
            bodyObj;

        _.each(this.worstPainSelectedID, (bodyPartID) => {

            bodyObj = this.getBodyPartAllInfo(bodyPartID);

            if(bodyObj.bodyPortion == bodyPortion) {
                regionFound = true;
            }
        });

        return regionFound;
    }

    //noinspection JSUnusedGlobalSymbols
    static removeWorstPainSelectedID(selectedID) {
        let newArr = _.filter(this.worstPainSelectedID, function(worstPainID) {
            return worstPainID !== selectedID;
        });

        this.worstPainSelectedID = newArr;
    }

    static detectIfIDPresentInWorstPainSelectedList(worstPainBodyPartIDToFind) {
        let idFound = false;

        _.each(this.worstPainSelectedID, function(worstPainID) {
            if(worstPainID == worstPainBodyPartIDToFind) {
                idFound = true;
            }
        });

        return idFound;
    }

    static previouslyRecentlySelectedWorstPainID(idToCheck) {
        let preivouslyRecentlySelected = false;
        _.each(this.recentlySelectedWorstPainIDs, function(recentlySelectedID) {
            if(recentlySelectedID == idToCheck) {
                preivouslyRecentlySelected = true;
            }
        });
        return preivouslyRecentlySelected;
    }

    static removeRecentlySelectedWorstPainID(idToRemove) {
        let newArray = _.filter(this.recentlySelectedWorstPainIDs, function(recentlyWorstPainID) {
            return recentlyWorstPainID !== idToRemove;
        });
        this.recentlySelectedWorstPainIDs = newArray;
    }

    static addSelectedFromRecentlySelectedWorstPainID() {
        let _that = this;

        if(!this.worstPainSelectedID || this.worstPainSelectedID.length==0) {
            this.worstPainSelectedID = [];
        }
        _.each(this.recentlySelectedWorstPainIDs, function(bodyPartID) {
            _that.worstPainSelectedID.push(bodyPartID);
        });
    }
    
    static removeRecentlySelectedWorstPainIDs() {

        let selectedID;

        for(let index=0; index<this.recentlySelectedWorstPainIDs.length; index++) {
            selectedID = this.recentlySelectedWorstPainIDs[index];
            this.removeWorstPainSelectedID(selectedID);
        }
        this.recentlySelectedWorstPainIDs = [];
    }

    static removeRecentlySelectedToRemoveWorstPainIDs() {
        let selectedID;

        for(let index=0; index<this.recentlySelectedIDsToRemoveWorstPainIDs.length; index++) {
            selectedID = this.recentlySelectedIDsToRemoveWorstPainIDs[index];
            this.addWorstPainSelectedID(selectedID);
        }
        this.recentlySelectedIDsToRemoveWorstPainIDs = [];
    }

    //noinspection JSUnusedGlobalSymbols
    static removeAllWorstPainSelectedID() {
        this.worstPainSelectedID = [];
    }

    static removeAllWorkstPainSelecedForBodyRegion(bodyPortion) {
        if (bodyPortion === this.allBodyPortionText) {

            this.worstPainSelectedID = [];
        } else {

            _.each(this.bodyParts, (bodyPart) => {
                if(bodyPart.bodyPortion == bodyPortion) {
                    this.removeWorstPainSelectedID(bodyPart.id);
                }
            });

        }
    }

    //noinspection JSUnusedGlobalSymbols
    /**
     * This function checks if the worst pain ID exists in the selected body sets
     * @returns {boolean|*} - true or false whether it was selected earlier
     */
    static checkIfWorstPainSelectedIDExistsInBodySet() {
        let bodyPartID = this.worstPainSelectedID[0];
        let bodyRegion = this.getBodyPartBodyPortionInfo(bodyPartID);
        let exists = this.checkSelectedBodyPartForID(bodyRegion, bodyPartID);
        return exists;
    }

}

export default BodyImageMapStorageWithoutLatestFeatures;
