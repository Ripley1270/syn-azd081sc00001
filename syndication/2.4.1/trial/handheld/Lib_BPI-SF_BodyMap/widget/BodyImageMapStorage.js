/*
const BODY_PORTION_FRONT_TOP = "FrontTop";
const BODY_PORTION_FRONT_BOT = "FrontBot";
const BODY_PORTION_BACK_TOP = "BackTop";
const BODY_PORTION_BACK_BOT = "BackBot";
const BODY_PORTION_ALL = "All";

class BodyImageMapStorage {

    static get frontTopBodyPortionText() {
        return BODY_PORTION_FRONT_TOP;
    }

    static get frontBotBodyPortionText() {
        return BODY_PORTION_FRONT_BOT;
    }

    static get backTopBodyPortionText() {
        return BODY_PORTION_BACK_TOP;
    }

    static get backBotBodyPortionText() {
        return BODY_PORTION_BACK_BOT;
    }

    static get allBodyPortionText() {
        return BODY_PORTION_ALL;
    }

    constructor()  {
        this.bodyParts = null;
        this.selectedFrontTopIDs = null;
        this.selectedFrontBotIDs = null;
        this.selectedBackTopIDs = null;
        this.selectedBackBotIDs = null;
        this.worstPainSelectedID = null;
        this.noPainSelected = false;
    }

    //noinspection FunctionTooLongJS
    static constructBodyParts() {

        this.bodyParts = new Map();

        //Following are Front Top values
        this.bodyParts.set("1", {"IT": "FR_HEA1B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("2", {"IT": "FR_SHL1B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("3", {"IT": "FR_THR1B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("4", {"IT": "FR_SHL2B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("5", {"IT": "FR_ARM1B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("6", {"IT": "FR_THR2B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("7", {"IT": "FR_THR3B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("8", {"IT": "FR_THR4B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("9", {"IT": "FR_ARM2B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("10", {"IT": "FR_THR5B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("11", {"IT": "FR_THR6B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("12", {"IT": "FR_THR7B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("13", {"IT": "FR_THR8B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("34", {"IT": "FR_ARM3B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("35", {"IT": "FR_ARM4B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("50", {"IT": "FR_ARM5B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});
        this.bodyParts.set("51", {"IT": "FR_ARM6B", "bodyPortion" : this.frontTopBodyPortionText, "nextScreen" : "BPID_BPID030", "nextScreen2" : "BPID_BPID110", "selected" : false});

        //Following are Front Bottom values
        this.bodyParts.set("14", {"IT": "FR_THR9B", "bodyPortion" : this.frontBotBodyPortionText, "nextScreen" : "BPID_BPID045", "nextScreen2" : "BPID_BPID125", "selected" : false});
        this.bodyParts.set("15", {"IT": "FR_LEG1B", "bodyPortion" : this.frontBotBodyPortionText, "nextScreen" : "BPID_BPID045", "nextScreen2" : "BPID_BPID125", "selected" : false});
        this.bodyParts.set("16", {"IT": "FR_LEG2B", "bodyPortion" : this.frontBotBodyPortionText, "nextScreen" : "BPID_BPID045", "nextScreen2" : "BPID_BPID125", "selected" : false});
        this.bodyParts.set("36", {"IT": "FR_LEG3B", "bodyPortion" : this.frontBotBodyPortionText, "nextScreen" : "BPID_BPID045", "nextScreen2" : "BPID_BPID125", "selected" : false});
        this.bodyParts.set("37", {"IT": "FR_LEG4B", "bodyPortion" : this.frontBotBodyPortionText, "nextScreen" : "BPID_BPID045", "nextScreen2" : "BPID_BPID125", "selected" : false});
        this.bodyParts.set("38", {"IT": "FR_LEG5B", "bodyPortion" : this.frontBotBodyPortionText, "nextScreen" : "BPID_BPID045", "nextScreen2" : "BPID_BPID125", "selected" : false});
        this.bodyParts.set("39", {"IT": "FR_LEG6B", "bodyPortion" : this.frontBotBodyPortionText, "nextScreen" : "BPID_BPID045", "nextScreen2" : "BPID_BPID125", "selected" : false});
        this.bodyParts.set("40", {"IT": "FR_LEG7B", "bodyPortion" : this.frontBotBodyPortionText, "nextScreen" : "BPID_BPID045", "nextScreen2" : "BPID_BPID125", "selected" : false});
        this.bodyParts.set("41", {"IT": "FR_LEG8B", "bodyPortion" : this.frontBotBodyPortionText, "nextScreen" : "BPID_BPID045", "nextScreen2" : "BPID_BPID125", "selected" : false});

        //Following are Back Top values
        this.bodyParts.set("17", {"IT": "BK_HEA1B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("18", {"IT": "BK_NCK1B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("19", {"IT": "BK_SHL1B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("20", {"IT": "BK_THR1B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("21", {"IT": "BK_SHL2B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("22", {"IT": "BK_ARM1B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("23", {"IT": "BK_THR2B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("24", {"IT": "BK_THR3B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("25", {"IT": "BK_THR4B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("26", {"IT": "BK_ARM2B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("27", {"IT": "BK_THR5B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("28", {"IT": "BK_THR6B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("42", {"IT": "BK_ARM3B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("43", {"IT": "BK_ARM4B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("52", {"IT": "BK_ARM5B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});
        this.bodyParts.set("53", {"IT": "BK_ARM6B", "bodyPortion" : this.backTopBodyPortionText, "nextScreen" : "BPID_BPID050", "nextScreen2" : "BPID_BPID130", "selected" : false});

        //Following are Back Bottom values
        this.bodyParts.set("29", {"IT": "BK_THR7B", "bodyPortion" : this.backBotBodyPortionText, "nextScreen" : "BPID_BPID065", "nextScreen2" : "BPID_BPID145", "selected" : false});
        this.bodyParts.set("30", {"IT": "BK_THR8B", "bodyPortion" : this.backBotBodyPortionText, "nextScreen" : "BPID_BPID065", "nextScreen2" : "BPID_BPID145", "selected" : false});
        this.bodyParts.set("31", {"IT": "BK_THR9B", "bodyPortion" : this.backBotBodyPortionText, "nextScreen" : "BPID_BPID065", "nextScreen2" : "BPID_BPID145", "selected" : false});
        this.bodyParts.set("32", {"IT": "BK_LEG1B", "bodyPortion" : this.backBotBodyPortionText, "nextScreen" : "BPID_BPID065", "nextScreen2" : "BPID_BPID145", "selected" : false});
        this.bodyParts.set("33", {"IT": "BK_LEG2B", "bodyPortion" : this.backBotBodyPortionText, "nextScreen" : "BPID_BPID065", "nextScreen2" : "BPID_BPID145", "selected" : false});
        this.bodyParts.set("44", {"IT": "BK_LEG3B", "bodyPortion" : this.backBotBodyPortionText, "nextScreen" : "BPID_BPID065", "nextScreen2" : "BPID_BPID145", "selected" : false});
        this.bodyParts.set("45", {"IT": "BK_LEG4B", "bodyPortion" : this.backBotBodyPortionText, "nextScreen" : "BPID_BPID065", "nextScreen2" : "BPID_BPID145", "selected" : false});
        this.bodyParts.set("46", {"IT": "BK_LEG5B", "bodyPortion" : this.backBotBodyPortionText, "nextScreen" : "BPID_BPID065", "nextScreen2" : "BPID_BPID145", "selected" : false});
        this.bodyParts.set("47", {"IT": "BK_LEG6B", "bodyPortion" : this.backBotBodyPortionText, "nextScreen" : "BPID_BPID065", "nextScreen2" : "BPID_BPID145", "selected" : false});
        this.bodyParts.set("48", {"IT": "BK_LEG7B", "bodyPortion" : this.backBotBodyPortionText, "nextScreen" : "BPID_BPID065", "nextScreen2" : "BPID_BPID145", "selected" : false});
        this.bodyParts.set("49", {"IT": "BK_LEG8B", "bodyPortion" : this.backBotBodyPortionText, "nextScreen" : "BPID_BPID065", "nextScreen2" : "BPID_BPID145", "selected" : false});
    }

    static constructBodyPartsFromList(list) {

        let id,
            value,
            IT,
            bodyPortion,
            nextScreen,
            nextScreen2,
            selected = false;

        this.bodyParts = [];

        _.each(list, (item) => {

            id = item.value;
            value = parseInt(id);
            IT = item.IT;

            if((value >= 1 && value <= 13 )  || (value == 34)|| (value == 35) || (value == 50) || (value == 51)) {

                bodyPortion = this.frontTopBodyPortionText;
                nextScreen = "BPID_BPID030";
                nextScreen2 = "BPID_BPID110";

            } else if((value == 14)|| (value == 15) || (value == 16) || (value >= 36 && value <= 41 )) {

                bodyPortion = this.frontBotBodyPortionText;
                nextScreen = "BPID_BPID045";
                nextScreen2 = "BPID_BPID125";

            } else if((value >= 17 && value <= 28 ) || (value == 42)|| (value == 43) || (value == 52) || (value == 53)) {

                bodyPortion = this.backTopBodyPortionText;
                nextScreen = "BPID_BPID050";
                nextScreen2 = "BPID_BPID130";

            } else if((value >= 29 && value <= 33 ) || (value >= 44 && value <= 49)) {

                bodyPortion = this.backBotBodyPortionText;
                nextScreen = "BPID_BPID065";
                nextScreen2 = "BPID_BPID145";

            }

            this.bodyParts.set(id, {"IT" : IT, "bodyPortion" : bodyPortion, "nextScreen" : nextScreen, "nextScreen2" : nextScreen2, "selected" : selected});
        });
    }

    //noinspection JSUnusedGlobalSymbols
    static constructSelectedBodyIDSSets() {
        this.selectedFrontTopIDs = new Set();
        this.selectedFrontBotIDs = new Set();
        this.selectedBackTopIDs = new Set();
        this.selectedBackBotIDs = new Set();
        this.worstPainSelectedID = new Set();
    }

    //noinspection JSUnusedGlobalSymbols
    /!**
     * This function will return all info for a given body part ID
     * @param bodyPartID - ID of the body part
     * @returns {V} = {"IT": , "bodyPortion" : "", "nextScreen" : ""}
     *          Example : for body ID '49', the return object will return - {"IT": "BK_LEG8B", "bodyPortion" : "BackBot", "nextScreen" : "BPID_BPID065"}
     *!/
    static getBodyPartAllInfo(bodyPartID) {
        return this.bodyParts.get(bodyPartID);
    }

    //noinspection JSUnusedGlobalSymbols
    /!**
     * This function will return 'IT' associated with a given body part ID
     * @param bodyPartID - ID of the body part
     * @returns {V} = "IT" of the given body part ID
     *          Example : for body ID '49', the return object will return - "BK_LEG8B"}
     *!/
    static getBodyPartITInfo(bodyPartID) {
        let bodyPart = this.bodyParts.get(bodyPartID);
        return bodyPart.IT;
    }

    //noinspection JSUnusedGlobalSymbols
    /!**
     * This function will return "body portion" info for a given body part ID
     * @param bodyPartID - ID of the body part
     * @returns {V} = "bodyPortion" of the given body part ID
     *          Example : for body ID '49', the return object will return - "BackBot"}
     *!/
    static getBodyPartBodyPortionInfo(bodyPartID) {
        let bodyPart = this.bodyParts.get(bodyPartID);
        return bodyPart.bodyPortion;
    }

    //noinspection JSUnusedGlobalSymbols
    /!**
     * This function will return "next screen" info for a given body part ID
     * @param bodyPartID - ID of the body part
     * @returns {V} = "bodyPortion" of the given body part ID
     *          Example : for body ID '49', the return object will return - "BPID_BPID065"}
     *!/
    static getBodyPartNextScreenInfo(bodyPartID) {
        let bodyPart = this.bodyParts.get(bodyPartID);
        return bodyPart.nextScreen;
    }

    //noinspection JSUnusedGlobalSymbols
    /!**
     * This function will return "next screen 2" info for a given body part ID
     * @param bodyPartID - ID of the body part
     * @returns {V} = "bodyPortion" of the given body part ID
     *          Example : for body ID '49', the return object will return - "BPID_BPID145"}
     *!/
    static getBodyPartNextScreen2Info(bodyPartID) {
        let bodyPart = this.bodyParts.get(bodyPartID);
        return bodyPart.nextScreen2;
    }

    static getBodyPartSelectedInfo(bodyPartID) {
        let bodyPart = this.bodyParts.get(bodyPartID);
        return bodyPart.selected;
    }

    static setBodyPartSelectedInfo(bodyPartID, selectedVal) {
        let bodyPartVal = this.bodyParts.get(bodyPartID);
        bodyPartVal.selected = selectedVal;
        this.bodyParts.set(bodyPartID, bodyPartVal);
    }

    //noinspection JSUnusedGlobalSymbols
    /!**
     * This function returns the correct set object for a given body portion
     * @param bodyPortionSetName - can be one of the following :
     *                      "FrontTop" - for front top,
     *                      "FrontBot" - for front bottom,
     *                      "BackTop"  - for front bottom,
     *                      "BackBot" - for front bottom,
     *                      "All" - for frontTop, frontBot, backTop, backBot
     * @returns {*} - one of the following : selectedFrontTopIDs, selectedFrontBotIDs, selectedBackTopIDs, selectedBackBotIDs
     *!/
    static getCorrectSelectedBodyPortionSetObject(bodyPortionSetName) {

        let bodyPortionSet = null;

        if (bodyPortionSetName === this.frontTopBodyPortionText) {
            bodyPortionSet = this.selectedFrontTopIDs;
        } else if (bodyPortionSetName === this.frontBotBodyPortionText) {
            bodyPortionSet = this.selectedFrontBotIDs;
        } else if (bodyPortionSetName === this.backTopBodyPortionText) {
            bodyPortionSet = this.selectedBackTopIDs;
        } else if (bodyPortionSetName === this.backBotBodyPortionText) {
            bodyPortionSet = this.selectedBackBotIDs;
        }

        return bodyPortionSet;
    }

    //noinspection JSUnusedGlobalSymbols
    /!**
     * This function returns true or false if bodyPartID has been selected. True if yes, false if no
     * @param bodyPortionSetName - can be one of the following :
     *                      "FrontTop" - for front top,
     *                      "FrontBot" - for front bottom,
     *                      "BackTop"  - for front bottom,
     *                      "BackBot" - for front bottom,
     *                      "All" - for frontTop, frontBot, backTop, backBot
     * @param bodyPartID - ID of the body part
     * @returns {boolean|*}
     *!/
    static checkSelectedBodyPartForID(bodyPortionSetName, bodyPartID) {

        if (bodyPortionSetName === this.allBodyPortionText) {

            throw new Error('You check all body areads add body part ID: ' + bodyPartID + '. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> checkSelectedBodyPartForID of Class -> BodyImageMapStorage.');

        } else {

            let setObject = this.getCorrectSelectedBodyPortionSetObject(bodyPortionSetName);
            let exists = setObject.has(bodyPartID);
            return exists;
        }
    }

    //noinspection JSUnusedGlobalSymbols
    /!**
    * This function clears all the selected body part IDs for a given body portion
    * @param bodyPortion - can be one of the following :
    *                      "FrontTop" - for front top,
    *                      "FrontBot" - for front bottom,
    *                      "BackTop"  - for front bottom,
    *                      "BackBot" - for front bottom,
    *                      "All" - for frontTop, frontBot, backTop, backBot
    *!/
    static clearAllSelectBodyPartsForBodyPortion(bodyPortion) {

        if (bodyPortion === this.allBodyPortionText) {

            this.selectedFrontTopIDs.clear();
            this.selectedFrontBotIDs.clear();
            this.selectedBackTopIDs.clear();
            this.selectedBackBotIDs.clear();

            this.bodyParts.forEach((bodyPartVal, bodyPartID) => {
                this.setBodyPartSelectedInfo(bodyPartID, false);
            });

        } else {

            let setObject = this.getCorrectSelectedBodyPortionSetObject(bodyPortion);
            setObject.clear();

        }
    }

    //noinspection JSUnusedGlobalSymbols
    /!**
     * This function adds a "body part ID" to a set of given body portion
     * @param bodyPortionSetName - can be one of the following :
     *                             "FrontTop" - for front top,
     *                             "FrontBot" - for front bottom,
     *                             "BackTop"  - for front bottom,
     *                             "BackBot" - for front bottom,
     * @param bodyPartID - ID of the body part
     *!/
    static addSelectedFromBodyPortion(bodyPortionSetName, bodyPartID) {

        if (bodyPortionSetName === this.allBodyPortionText) {

            throw new Error('You cannot add body part ID: ' + bodyPartID + ' to all sets. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> removeSelectedFromBodyPortion of Class -> BodyImageMapStorage.');

        } else {

            let setObject = this.getCorrectSelectedBodyPortionSetObject(bodyPortionSetName);
            setObject.add(bodyPartID);
            this.setBodyPartSelectedInfo(bodyPartID, true);

        }
    }

    //noinspection JSUnusedGlobalSymbols
    /!**
     * This function removes a "body part ID" from a set of given body portion
     * @param bodyPortionSetName - can be one of the following :
     *                             "FrontTop" - for front top,
     *                             "FrontBot" - for front bottom,
     *                             "BackTop"  - for front bottom,
     *                             "BackBot" - for front bottom
     * @param bodyPartID - ID of the body part
     *!/
    static removeSelectedFromBodyPortion(bodyPortionSetName, bodyPartID) {

        if (bodyPortionSetName === this.allBodyPortionText) {

            throw new Error('You cannot remove body part ID: ' + bodyPartID + ' from all sets. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> removeSelectedFromBodyPortion of Class -> BodyImageMapStorage.');

        } else {

            let setObject = this.getCorrectSelectedBodyPortionSetObject(bodyPortionSetName);
            setObject.delete(bodyPartID);
            this.setBodyPartSelectedInfo(bodyPartID, false);

        }
    }

    //noinspection JSUnusedGlobalSymbols
    /!**
     * This function removes all "body part IDs" from the set of given body portion
     * @param bodyPortionSetName - can be one of the following :
     *                             "FrontTop" - for front top,
     *                             "FrontBot" - for front bottom,
     *                             "BackTop"  - for front bottom,
     *                             "BackBot" - for front bottom
     *!/
    static removeAllSelectedFromBodyPortion(bodyPortionSetName) {

        if (bodyPortionSetName === this.allBodyPortionText) {

            throw new Error('You cannot remove body part IDs from all sets. The ID belongs to only one body portion set. Please pass in the correct body portion set name to Method -> removeAllSelectedFromBodyPortion of Class -> BodyImageMapStorage.');

        } else {

            let setObject = this.getCorrectSelectedBodyPortionSetObject(bodyPortionSetName);
            setObject.forEach((bodyPartID) => {
                this.setBodyPartSelectedInfo(bodyPartID, false);
            });
            setObject.clear();
        }
    }

    //noinspection JSUnusedGlobalSymbols
    static getNoPainSelected() {
        return this.noPainSelected;
    }

    //noinspection JSUnusedGlobalSymbols
    static setNoPainSelected(val) {
        this.noPainSelected = val;
    }

    //noinspection JSUnusedGlobalSymbols
    static getWorstPainSelectedIDs() {
        return this.worstPainSelectedID;
    }

    //noinspection JSUnusedGlobalSymbols
    static addWorstPainSelectedID(selectedID) {
        this.worstPainSelectedID.add(selectedID);
    }

    //noinspection JSUnusedGlobalSymbols
    static removeWorstPainSelectedID(selectedID) {
        this.worstPainSelectedID.delete(selectedID);
    }

    //noinspection JSUnusedGlobalSymbols
    static removeAllWorstPainSelectedID() {
        this.worstPainSelectedID.clear();
    }

    //noinspection JSUnusedGlobalSymbols
    /!**
     * This function checks if the worst pain ID exists in the selected body sets
     * @returns {boolean|*} - true or false whether it was selected earlier
     *!/
    static checkIfWorstPainSelectedIDExistsInBodySet() {
        let values = this.worstPainSelectedID.values();
        let bodyPartID = values.next().value;
        let bodyRegion = this.getBodyPartBodyPortionInfo(bodyPartID);
        let exists = this.checkSelectedBodyPartForID(bodyRegion, bodyPartID);
        return exists;
    }

}

export default BodyImageMapStorage;
*/
