import BodyImageMapStorageWithoutLatestFeatures from './BodyImageMapStorageWithoutLatestFeatures';
import BPISFBodyDiagram from './BPISFBodyDiagram';

class BPISFBodyDiagramZoom extends BPISFBodyDiagram {

    render() {

        let that = this;

        return this.buildHTML()
            .then(() => {
                return Q.Promise((resolve) => {
                    that.$el.appendTo(that.getQuestion().$el)
                        .ready(() => {
                            resolve();
                        });
                });
            })
            .then(() => {
                that.delegateEvents();
            })
            .then(() => {
                return this.updateSelectedAnswers();
            })
            .then(() => {
                this.completed = true;
                //this.setCompleted();
            });
    }
    
    bodyPartSelectionHandler(e) {
        let id = e.currentTarget.id;

        if (id.indexOf(':') != -1) {

            //ID format = <name:id> ex: "frontHead:1"  - cache selected value for study rule to use
            let bodyPartID = id.split(':')[1];
            let previouslySelected = BodyImageMapStorageWithoutLatestFeatures.getBodyPartSelectedInfo(bodyPartID),
                previouslyRecentlySelected = BodyImageMapStorageWithoutLatestFeatures.previouslyRecentlySelectedID(bodyPartID);

            let bodyPortion = BodyImageMapStorageWithoutLatestFeatures.getBodyPartBodyPortionInfo(bodyPartID);

            if (previouslyRecentlySelected){
                e.currentTarget.classList.remove("body-part-selected");
                BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedID(bodyPartID);
            } else if(previouslySelected) {
                e.currentTarget.classList.remove("body-part-selected");
                //first detect if the ID was selected in  exists
                let detectedIDPresenceInSelectedList = BodyImageMapStorageWithoutLatestFeatures.detectIfIDPresentInSelectedList(bodyPortion, bodyPartID);
                if(detectedIDPresenceInSelectedList) {
                    BodyImageMapStorageWithoutLatestFeatures.removeSelectedFromBodyPortion(bodyPortion, bodyPartID);
                    // if ID selected in previous answers not just in recent answers.
                    BodyImageMapStorageWithoutLatestFeatures.addRecentlySelectedIDsToRemove(bodyPartID);
                }
            } else {
                e.currentTarget.classList.add("body-part-selected");
                BodyImageMapStorageWithoutLatestFeatures.addRecentlySelectedIDs(bodyPartID);
            }
        }

        this.completed = true;
        //this.setCompleted();
    }

    setCompleted() {

        return Q()
            .then(() => {
                let modelTemplate = this.model.get("templates");

                if(modelTemplate == "BPISF:FrontTorsoMostPain") {
                    if (BodyImageMapStorageWithoutLatestFeatures.selectedFrontTopIDs.length > 0) {
                        this.completed = true;
                    } else {
                        this.completed = false;
                    }
                } else if(modelTemplate == "BPISF:FrontLegsMostPain") {
                    if (BodyImageMapStorageWithoutLatestFeatures.selectedFrontBotIDs.length > 0) {
                        this.completed = true;
                    } else {
                        this.completed = false;
                    }
                } else if(modelTemplate == "BPISF:BackTorsoMostPain") {
                    if (BodyImageMapStorageWithoutLatestFeatures.selectedBackTopIDs.length > 0) {
                        this.completed = true;
                    } else {
                        this.completed = false;
                    }
                } else if(modelTemplate == "BPISF:BackLegsMostPain") {
                    if (BodyImageMapStorageWithoutLatestFeatures.selectedBackBotIDs.length > 0) {
                        this.completed = true;
                    } else {
                        this.completed = false;
                    }
                }
            });

    }
}

window.LF.Widget.BPISFBodyDiagramZoom = BPISFBodyDiagramZoom;

export default BPISFBodyDiagramZoom;
