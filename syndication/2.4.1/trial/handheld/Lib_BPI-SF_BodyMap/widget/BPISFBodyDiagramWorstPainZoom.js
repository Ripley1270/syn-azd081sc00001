import BodyImageMapStorageWithoutLatestFeatures from './BodyImageMapStorageWithoutLatestFeatures';
import BPISFBodyDiagramWorstPain from './BPISFBodyDiagramWorstPain';

class BPISFBodyDiagramWorstPainZoom extends BPISFBodyDiagramWorstPain {

    render() {
        return super.render()
            .then(() => {
                return this.updateSelectedAnswers();
            })
            .then(() => {
                this.completed = true;
            });
    }

    //noinspection JSValidateJSDoc
    /**
     * Update the image map with previously selected values
     * @returns {*|Promise.<TResult>}
     */
    updateSelectedAnswers() {
        return Q()
            .then(() => {
                this.clearAllSelectedFromUI();
            })
            .then(() => {

                let selectedWorstPainID = BodyImageMapStorageWithoutLatestFeatures.getWorstPainSelectedIDs();

                if (selectedWorstPainID) {
                    _.each(selectedWorstPainID, (selectedID) => {
                        if(this.$("path[id$=':" + selectedID + "']")[0]) {
                            this.$("path[id$=':" + selectedID + "']")[0].classList.add("body-part-selected");
                        }
                    });
                }
            });
    }

    bodyPartSelectionHandler(e) {

        let id = e.currentTarget.id;

        if (id.indexOf(':') != -1) {

            //ID format = <name:id> ex: "frontHead:1"  - cache selected value for study rule to use
            let bodyPartID = id.split(':')[1];

            let previouslySelected = BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainArrayContainsID(bodyPartID),
                previouslyRecentlySelected = BodyImageMapStorageWithoutLatestFeatures.previouslyRecentlySelectedWorstPainID(bodyPartID);

            if (previouslyRecentlySelected){
                e.currentTarget.classList.remove("body-part-selected");
                BodyImageMapStorageWithoutLatestFeatures.removeRecentlySelectedWorstPainID(bodyPartID);
            }
            else if(previouslySelected) {
                //first detect if the ID was selected in  exists
                let detectedIDPresenceInWorstPainSelectedList = BodyImageMapStorageWithoutLatestFeatures.detectIfIDPresentInWorstPainSelectedList(bodyPartID);
                if(detectedIDPresenceInWorstPainSelectedList) {
                    BodyImageMapStorageWithoutLatestFeatures.removeWorstPainSelectedID(bodyPartID);
                    e.currentTarget.classList.remove("body-part-selected");
                    BodyImageMapStorageWithoutLatestFeatures.addRecentlySelectedToRemoveWorstPainIDs(bodyPartID);
                }
            }
            else {
                e.currentTarget.classList.add("body-part-selected");
                BodyImageMapStorageWithoutLatestFeatures.addRecentlySelectedWorstPainIDs(bodyPartID);
            }
        }
        //this.setCompleted();
    }

    setCompleted() {

        return Q()
            .then(() => {
                let bodyPortion,
                    modelTemplate = this.model.get("templates"),
                    bodyPortionSelectedPreviously;

                if(modelTemplate == "BPISF:FrontTorsoMostPain") {
                    bodyPortion = BodyImageMapStorageWithoutLatestFeatures.frontTopBodyPortionText;
                } else if(modelTemplate == "BPISF:FrontLegsMostPain") {
                    bodyPortion = BodyImageMapStorageWithoutLatestFeatures.frontBotBodyPortionText;
                } else if(modelTemplate == "BPISF:BackTorsoMostPain") {
                    bodyPortion = BodyImageMapStorageWithoutLatestFeatures.backTopBodyPortionText;
                } else if(modelTemplate == "BPISF:BackLegsMostPain") {
                    bodyPortion = BodyImageMapStorageWithoutLatestFeatures.backBotBodyPortionText;
                }

                bodyPortionSelectedPreviously = BodyImageMapStorageWithoutLatestFeatures.checkIfWorsePainArryContainsBodyRegion(bodyPortion);

                if(bodyPortionSelectedPreviously) {
                    this.completed = true;
                } else {
                    this.completed = false;
                }

            });

    }
}

window.LF.Widget.BPISFBodyDiagramWorstPainZoom = BPISFBodyDiagramWorstPainZoom;

export default BPISFBodyDiagramWorstPainZoom;
