import WidgetBase from 'core/widgets/WidgetBase';
import BodyImageMapStorageWithoutLatestFeatures from './BodyImageMapStorageWithoutLatestFeatures';
import BranchHelpers from 'core/branching/branchingHelpers';

class BPISFBodyDiagram extends WidgetBase {


    //noinspection JSUnusedGlobalSymbols
    get checkboxContainer () {
        return 'DEFAULT:VerticalButtonGroup';
    }

    //noinspection JSMethodCanBeStatic
    get bodyImageMapCheckbox () {
        return 'CUSTOM:BPISFCheckbox';
    }

    get configContainerTemplate() {
        let templates = this.model.get('templates');
        return templates['container'];
    }

    //noinspection JSUnusedGlobalSymbols
    get containerHeight() {
        return this.model.get('height');
    }

    //noinspection JSUnusedGlobalSymbols
    get containerWidth() {
        return this.model.get('width');
    }

    //noinspection JSUnusedGlobalSymbols
    get bodyParts() {
        return this.model.get('bodyParts');
    }

    //noinspection JSUnusedGlobalSymbols
    get stringsToFetch() {
        return this.model.get('labels');
    }

    //noinspection JSUnusedGlobalSymbols
    get labelNamespace() {
        return this.model.get('labelNamespace') || 'BPID';
    }

    //noinspection FunctionWithMoreThanThreeNegationsJS
    constructor(options) {

        super(options);

        if (!this.model.get('height')) {
            throw new Error('Missing property "height" in BPISFBodyDiagram.');
        }

        if (!this.model.get('labels')) {
            throw new Error('Missing property "labels" in BPISFBodyDiagram.');
        }

        if(!this.model.get('templates')) {
            throw new Error('Missing property "templates" in BPISFBodyDiagram.');
        }

        this.events = {
            'click svg path': 'bodyPartSelectionHandler',
            'click #noPainButton' : 'noPainButtonHandler'
        };

        this.updateTemplate();
    }

    updateTemplate() {
        let templates = this.configContainerTemplate;
        this.model.set('templates', templates);
    }

    render() {

        let that = this;

        return this.buildHTML()
                    .then(() => {
                        return Q.Promise((resolve) => {
                            that.$el.appendTo(that.getQuestion().$el)
                                    .ready(() => {
                                        resolve();
                                    });
                        });
                    })
                    .then(()=> {
                        this.appendCheckbox();
                    })
                    .then(() => {
                        that.delegateEvents();
                    })
                    .then(() => {
                        return this.updateSelectedAnswers();
                    })
                    .then(() => {
                        this.setCompleted();
                    });
    }

    buildHTML() {
        return Q()
            .then(() => {
                return Q.Promise((resolve) => {

                    let templates = this.model.get('templates'),
                        content = this.renderTemplate(templates, {});

                    this.$el.html(content);
                    resolve();
                });
            })
            .then(() => {
                 if(!BodyImageMapStorageWithoutLatestFeatures.bodyParts) {
                    let bodyPartList = this.bodyParts;
                     BodyImageMapStorageWithoutLatestFeatures.constructBodyPartsFromList(bodyPartList);
                     BodyImageMapStorageWithoutLatestFeatures.constructSelectedBodyIDSSets();
                 }
                 return this.translateAllStrings();
             })
             .then((translatedStrings) => {
                 return Q.Promise((resolve) => {
                     if(this.$('#txtFront')) {
                         this.$('#txtFront').text(translatedStrings["front"]);
                         this.$('#txtFront_L').text(translatedStrings["frontLeft"]);
                         this.$('#txtFront_R').text(translatedStrings["frontRight"]);
                     }
                     if(this.$('#txtBack')) {
                         this.$('#txtBack').text(translatedStrings["back"]);
                         this.$('#txtBack_L').text(translatedStrings["backLeft"]);
                         this.$('#txtBack_R').text(translatedStrings["backRight"]);
                     }
                     resolve(translatedStrings);
                 });
             })
             .then((translatedStrings) => {
                 //reposition labels in SVG if translation is too long
                 var txtFrontLDefLength = 4,
                 txtBackLDefLength = 5,
                 origX;

                 //need optimization later
                 if(translatedStrings["frontLeft"] && (translatedStrings["frontLeft"].length > txtFrontLDefLength)) {
                     origX = this.$('#txtFront_L').attr('x');
                     this.$('#txtFront_L').attr('x', Number(origX) - (((translatedStrings["frontLeft"].length) - txtFrontLDefLength) * 9));
                 }

                 if(translatedStrings["backLeft"] && (translatedStrings["backLeft"].length > txtBackLDefLength)) {
                     origX = this.$('#txtBack_L').attr('x');
                     this.$('#txtBack_L').attr('x', Number(origX) - (((translatedStrings["backLeft"].length) - txtFrontLDefLength) * 9) );
                 }
             });
    }

    /**
     * This function appends checkbox to the bodyImageMap
     * @returns {*}
     */
    appendCheckbox() {

        let bodyImageMapCheckbox = this.bodyImageMapCheckbox;

        return Q()
                .then(() => {
                    let noPain = "";
                    if(this.stringsToFetch.no_pain)
                        noPain = this.stringsToFetch.no_pain;

                    return this.i18n({
                            'noPainText': noPain
                        },
                        $.noop,
                        {namespace: this.getQuestion().getQuestionnaire().id});
                })
                .then((translatedStrings) => {
                    return Q.Promise((resolve) => {
                        let bodyImageMapCheckboxTemplate = this.renderTemplate(bodyImageMapCheckbox, {
                            "NoPainText" : translatedStrings.noPainText
                        });
                        this.$el.append(bodyImageMapCheckboxTemplate);
                        resolve();
                    });//BodyImageMapStorageWithoutLatestFeatures.getNoPainSelected()
                })
                .then(() => {
                    return Q.Promise((resolve) => {
                        let noPainSelected = BodyImageMapStorageWithoutLatestFeatures.getNoPainSelected();
                        if(noPainSelected == true) {
                            $("#noPain_Q2_W1_checkbox_0").prop("checked", true);
                            $(".btn-noPain_Q2_W1_checkbox_0").addClass("active");
                        }
                        resolve();
                    });//BodyImageMapStorageWithoutLatestFeatures.getNoPainSelected()
                });
    }

    //noinspection JSValidateJSDoc
    /**
     * This function does translates all the text
     * @returns {Promise.<TResult>|*} - contains translated strings.
     */
    translateAllStrings() {
        //noinspection FunctionWithInconsistentReturnsJS
        return Q()
            .then(() => {

                let back, backLeft, backRight, front, frontLeft, frontRight;

                if(this.stringsToFetch.back)
                    back = this.stringsToFetch.back;

                if(this.stringsToFetch.back_left)
                    backLeft = this.stringsToFetch.back_left;

                if(this.stringsToFetch.back_right)
                    backRight = this.stringsToFetch.back_right;

                if(this.stringsToFetch.front)
                    front = this.stringsToFetch.front;

                if(this.stringsToFetch.front_left)
                    frontLeft = this.stringsToFetch.front_left;

                if(this.stringsToFetch.front_right)
                    frontRight = this.stringsToFetch.front_right;

                if(front && back) {
                    return this.i18n({
                            'back': back,
                            'backLeft': backLeft,
                            'backRight': backRight,
                            'front': front,
                            'frontLeft': frontLeft,
                            'frontRight': frontRight
                        },
                        $.noop,
                        {namespace: this.getQuestion().getQuestionnaire().id}
                    );
                }
                else if(front) {
                    return this.i18n({
                            'front': front,
                            'frontLeft': frontLeft,
                            'frontRight': frontRight
                        },
                        $.noop,
                        {namespace: this.getQuestion().getQuestionnaire().id}
                    );
                }
                else if(back) {
                    return this.i18n({
                            'back': back,
                            'backLeft': backLeft,
                            'backRight': backRight
                        },
                        $.noop,
                        {namespace: this.getQuestion().getQuestionnaire().id}
                    );
                }
            });
    }

    //noinspection JSValidateJSDoc
    /**
     * Update the image map with previously selected values
     * @returns {*|Promise.<TResult>}
     */
    updateSelectedAnswers() {
        return Q()
            .then(() => {

                if (BodyImageMapStorageWithoutLatestFeatures.selectedFrontTopIDs.length > 0) {
                    _.each(BodyImageMapStorageWithoutLatestFeatures.selectedFrontTopIDs, (selectedBodyID) => {
                        if(this.$("path[id$=':" + selectedBodyID + "']")[0]) {
                            this.$("path[id$=':" + selectedBodyID + "']")[0].classList.add("body-part-selected");
                        }
                    });
                }

                if (BodyImageMapStorageWithoutLatestFeatures.selectedFrontBotIDs.length > 0) {
                    _.each(BodyImageMapStorageWithoutLatestFeatures.selectedFrontBotIDs, (selectedBodyID) => {
                        if(this.$("path[id$=':" + selectedBodyID + "']")[0]) {
                            this.$("path[id$=':" + selectedBodyID + "']")[0].classList.add("body-part-selected");
                        }
                    });
                }

                if (BodyImageMapStorageWithoutLatestFeatures.selectedBackTopIDs.length > 0) {
                    _.each(BodyImageMapStorageWithoutLatestFeatures.selectedBackTopIDs, (selectedBodyID) => {
                        if(this.$("path[id$=':" + selectedBodyID + "']")[0]) {
                            this.$("path[id$=':" + selectedBodyID + "']")[0].classList.add("body-part-selected");
                        }
                    });
                }

                if (BodyImageMapStorageWithoutLatestFeatures.selectedBackBotIDs.length > 0) {
                    _.each(BodyImageMapStorageWithoutLatestFeatures.selectedBackBotIDs, (selectedBodyID) => {
                        if(this.$("path[id$=':" + selectedBodyID + "']")[0]) {
                            this.$("path[id$=':" + selectedBodyID + "']")[0].classList.add("body-part-selected");
                        }
                    });
                }
            });
    }

    //noinspection JSValidateJSDoc
    /**
     * This function determines whether the widget is completed or not
     * @returns {*|Promise.<TResult>}
     */
    setCompleted() {

        let noPainVal;

        return Q()
            .then(() => {
                noPainVal = BodyImageMapStorageWithoutLatestFeatures.getNoPainSelected();

                this.completed = !!((BodyImageMapStorageWithoutLatestFeatures.selectedFrontTopIDs && BodyImageMapStorageWithoutLatestFeatures.selectedFrontTopIDs.length > 0) ||
                (BodyImageMapStorageWithoutLatestFeatures.selectedFrontBotIDs && BodyImageMapStorageWithoutLatestFeatures.selectedFrontBotIDs.length > 0) ||
                (BodyImageMapStorageWithoutLatestFeatures.selectedBackTopIDs && BodyImageMapStorageWithoutLatestFeatures.selectedBackTopIDs.length > 0) ||
                (BodyImageMapStorageWithoutLatestFeatures.selectedBackBotIDs && BodyImageMapStorageWithoutLatestFeatures.selectedBackBotIDs.length > 0) ||
                noPainVal);
            });

    }

    bodyPartSelectionHandler(e) {
        let id = e.currentTarget.id;

        $("#noPainButton").attr("checked", false);
        BodyImageMapStorageWithoutLatestFeatures.setNoPainSelected(false);

        if (id.indexOf(':') != -1) {

            //ID format = <name:id> ex: "frontHead:1"  - cache selected value for study rule to use
            let bodyPartID = id.split(':')[1];
            let nextScreenID = BodyImageMapStorageWithoutLatestFeatures.getBodyPartNextScreenInfo(bodyPartID);

            BranchHelpers.navigateToScreen(nextScreenID);
        }
        this.setCompleted();
    }

    //noinspection JSUnusedLocalSymbols
    noPainButtonHandler(e) {
        let bodyPortionsToClear = BodyImageMapStorageWithoutLatestFeatures.allBodyPortionText,
            isChecked = $(e.currentTarget).find('input:first').is(':checked');

        BodyImageMapStorageWithoutLatestFeatures.clearAllSelectBodyPartsForBodyPortion(bodyPortionsToClear);

        if(isChecked) {
            BodyImageMapStorageWithoutLatestFeatures.setNoPainSelected(false);
        } else {
            BodyImageMapStorageWithoutLatestFeatures.setNoPainSelected(true);
        }

        this.clearAllSelectedFromUI();
        this.setCompleted();
    }

    clearAllSelectedFromUI() {
        _.each(BodyImageMapStorageWithoutLatestFeatures.bodyParts, (bodyPart) => {
            if(this.$("path[id$=':" + bodyPart.id + "']")[0]) {
                this.$("path[id$=':" + bodyPart.id + "']")[0].classList.remove("body-part-selected");
            }
        });
    }
}

window.LF.Widget.BPISFBodyDiagram = BPISFBodyDiagram;

export default BPISFBodyDiagram;
