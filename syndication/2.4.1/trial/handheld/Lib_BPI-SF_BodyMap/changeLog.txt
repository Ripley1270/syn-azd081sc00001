Version info for BPI-SF:

This is the BPI-SF with body diagram.

==================================================
Known Issues:
-None
==================================================

LABEL    DATE           CHANGES
======   ===========    ============

00.09   16Jan2018       [uadhikari]
                        - LF.Utilities.navigateToScreen changed to BranchHelpers.navigateToScreen

00.08   08Jan2018       [uadhikari]
                        - ONEECOA-63797 - Stadnardization - HH - BPI-SF Body Diagram - Incorrect Question Text on BPID200 and BPID210

00.07   04Jan2018       [uadhikari]
                        - ONEECOA-63342 - CLONE - HH - Selections made on BPID110, BPID125, BPID130 and BPID145 are not cleared after BPID200 is displayed

00.06   29Dec2017       [uadhikari]
                        - ONEECOA-62754 - CLONE - Spec (BPI-sf) - Clarification regarding branching from Body Part Diagrams for none selection.

00.05   20Dec2017       [uadhikari]
                        - ONEECOA-61690 - Standardization - HH - Variable EDTMST1L shows "Yes" instead of Null after answer change
                        - ONEECOA-61691 - Standardization - LP - Selected areas are displayed in blue on BPID020 and BPID100 when back is pressed on screens for selecting body parts per specific scenario

00.04   4Dec2017       [uadhikari]
                        - Changed addIT to use BaseQuestionnaireView

00.03   17Nov2017       [uadhikari]
                        - ONEECOA-54821 - [LP][BPI-SF (Body Map)] The changes are updated when tap Back button
                        - ONEECOA-54915 - BPI-SF (Body Map) (HH) Branching issue

00.02   15Nov2017       [uadhikari]
                        - ONEECOA-54216 - [LP][BPI-SF (Body Map)] BDYMST1L is incorrect when tap back from BPID100

00.01   13Nov2017       [uadhikari]
                        - ONEECOA-54915 - BPI-SF (Body Map) (HH) Branching issue
