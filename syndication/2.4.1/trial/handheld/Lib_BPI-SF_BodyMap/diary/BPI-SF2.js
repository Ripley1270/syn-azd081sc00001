
export default {
    questionnaires: [
        {
            "id": "BPID",
            "SU": "BPID",
            "displayName": "DISPLAY_NAME",
            "className": "HANDHELD BPID",
            "affidavit": "DEFAULT",
            "previousScreen": true,
            "screens": [
                "BPID_BPID010",
                "BPID_BPID020",
                "BPID_BPID030",
                "BPID_BPID045",
                "BPID_BPID050",
                "BPID_BPID065",
                "BPID_BPID100",
                "BPID_BPID110",
                "BPID_BPID125",
                "BPID_BPID130",
                "BPID_BPID145",
                "BPID_BPID200",
                "BPID_BPID210",
                "BPID_BPID300",
                "BPID_BPID310",
                "BPID_BPID320",
                "BPID_BPID330",
                "BPID_BPID340",
                "BPID_BPID350",
                "BPID_BPID360",
                "BPID_BPID370",
                "BPID_BPID380",
                "BPID_BPID390",
                "BPID_BPID400",
                "BPID_BPID410",
                "BPID_BPID420"
            ],
            "branches": [
                {
                    "branchFrom": "BPID_BPID020",
                    "branchTo": "BPID_BPID100",
                    "clearBranchedResponses": true,
                    "branchFunction": "BPI020Branch" //Dynamic
                },
                {
                    "branchFrom": "BPID_BPID030",
                    "branchTo": "BPID_BPID020",
                    "clearBranchedResponses": false,
                    "branchFunction": "BPI030Branch" //Dynamic
                },
                {
                    "branchFrom": "BPID_BPID045",
                    "branchTo": "BPID_BPID020",
                    "clearBranchedResponses": false,
                    "branchFunction": "BPI045Branch" //Dynamic
                },
                {
                    "branchFrom": "BPID_BPID050",
                    "branchTo": "BPID_BPID020",
                    "clearBranchedResponses": false,
                    "branchFunction": "BPI050Branch" //Dynamic
                },
                {
                    "branchFrom": "BPID_BPID065",
                    "branchTo": "BPID_BPID020",
                    "clearBranchedResponses": false,
                    "branchFunction": "BPI065Branch" //Dynamic
                },
                {
                    "branchFrom": "BPID_BPID100",
                    "branchTo": "BPID_BPID300",
                    "clearBranchedResponses": true,
                    "branchFunction": "BPI100Branch" //Dynamic
                },
                {
                    "branchFrom": "BPID_BPID110",
                    "branchTo": "BPID_BPID100",
                    "clearBranchedResponses": false,
                    "branchFunction": "BPI110Branch" //Dynamic
                },
                {
                    "branchFrom": "BPID_BPID125",
                    "branchTo": "BPID_BPID100",
                    "clearBranchedResponses": false,
                    "branchFunction": "BPI125Branch" //Dynamic
                },
                {
                    "branchFrom": "BPID_BPID130",
                    "branchTo": "BPID_BPID100",
                    "clearBranchedResponses": false,
                    "branchFunction": "BPI130Branch" //Dynamic
                },
                {
                    "branchFrom": "BPID_BPID145",
                    "branchTo": "BPID_BPID100",
                    "clearBranchedResponses": false,
                    "branchFunction": "BPI145Branch" //Dynamic
                },
                {
                    "branchFrom": "BPID_BPID200",
                    "branchTo": "BPID_BPID100",
                    "clearBranchedResponses": true,
                    "branchFunction": "BPI200BranchBack"
                },
                {
                    "branchFrom": "BPID_BPID210",
                    "branchTo": "BPID_BPID100",
                    "clearBranchedResponses": true,
                    "branchFunction": "compareBranchBack",
                    "branchParams": {
                        "IG": "BPIDiagram",
                        questionId: "BPID_BPID210_Q0",
                        operand : "=",
                        value: "0"
                    }
                },
                {
                    "branchFrom": "BPID_BPID210",
                    "branchTo": "BPID_BPID300",
                    "clearBranchedResponses": true,
                    "branchFunction": "always"
                }
            ],
            "initialScreen": []
        }
    ],

    screens: [
        {
            "horizonId": "5942d2d665c6481695e6ca47",
            "id": "BPID_BPID010",
            "className": "BPID_BPID010",
            "questions": [
                {
                    "id": "BPID_BPID010_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca48",
            "id": "BPID_BPID020",
            "className": "BPID_BPID020",
            "questions": [
                {
                    "id": "BPID_BPID020_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca43",
            "id": "BPID_BPID030",
            "className": "BPID_BPID030",
            "questions": [
                {
                    "id": "BPID_BPID030_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca44",
            "id": "BPID_BPID045",
            "className": "BPID_BPID045",
            "questions": [
                {
                    "id": "BPID_BPID045_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca46",
            "id": "BPID_BPID050",
            "className": "BPID_BPID050",
            "questions": [
                {
                    "id": "BPID_BPID050_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca45",
            "id": "BPID_BPID065",
            "className": "BPID_BPID065",
            "questions": [
                {
                    "id": "BPID_BPID065_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca4d",
            "id": "BPID_BPID100",
            "className": "BPID_BPID100",
            "questions": [
                {
                    "id": "BPID_BPID100_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca49",
            "id": "BPID_BPID110",
            "className": "BPID_BPID110",
            "questions": [
                {
                    "id": "BPID_BPID110_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca4a",
            "id": "BPID_BPID125",
            "className": "BPID_BPID125",
            "questions": [
                {
                    "id": "BPID_BPID125_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca4b",
            "id": "BPID_BPID130",
            "className": "BPID_BPID130",
            "questions": [
                {
                    "id": "BPID_BPID130_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca4c",
            "id": "BPID_BPID145",
            "className": "BPID_BPID145",
            "questions": [
                {
                    "id": "BPID_BPID145_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca4e",
            "id": "BPID_BPID200",
            "className": "BPID_BPID200",
            "questions": [
                {
                    "id": "BPID_BPID200_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca4f",
            "id": "BPID_BPID210",
            "className": "BPID_BPID210",
            "questions": [
                {
                    "id": "BPID_BPID210_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca52",
            "id": "BPID_BPID300",
            "className": "BPID_BPID300",
            "orientation" : 'landscape',
            "questions": [
                {
                    "id": "BPID_BPID300_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca50",
            "id": "BPID_BPID310",
            "className": "BPID_BPID310",
            "orientation" : 'landscape',
            "questions": [
                {
                    "id": "BPID_BPID310_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca51",
            "id": "BPID_BPID320",
            "className": "BPID_BPID320",
            "orientation" : 'landscape',
            "questions": [
                {
                    "id": "BPID_BPID320_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d665c6481695e6ca53",
            "id": "BPID_BPID330",
            "className": "BPID_BPID330",
            "orientation" : 'landscape',
            "questions": [
                {
                    "id": "BPID_BPID330_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d765c6481695e6ca54",
            "id": "BPID_BPID340",
            "className": "BPID_BPID340",
            "orientation" : 'landscape',
            "questions": [
                {
                    "id": "BPID_BPID340_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d765c6481695e6ca55",
            "id": "BPID_BPID350",
            "className": "BPID_BPID350",
            "orientation" : 'landscape',
            "questions": [
                {
                    "id": "BPID_BPID350_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d765c6481695e6ca56",
            "id": "BPID_BPID360",
            "className": "BPID_BPID360",
            "orientation" : 'landscape',
            "questions": [
                {
                    "id": "BPID_BPID360_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d765c6481695e6ca57",
            "id": "BPID_BPID370",
            "className": "BPID_BPID370",
            "orientation" : 'landscape',
            "questions": [
                {
                    "id": "BPID_BPID370_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d765c6481695e6ca58",
            "id": "BPID_BPID380",
            "className": "BPID_BPID380",
            "orientation" : 'landscape',
            "questions": [
                {
                    "id": "BPID_BPID380_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d765c6481695e6ca59",
            "id": "BPID_BPID390",
            "className": "BPID_BPID390",
            "orientation" : 'landscape',
            "questions": [
                {
                    "id": "BPID_BPID390_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d765c6481695e6ca5b",
            "id": "BPID_BPID400",
            "className": "BPID_BPID400",
            "orientation" : 'landscape',
            "questions": [
                {
                    "id": "BPID_BPID400_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d765c6481695e6ca5a",
            "id": "BPID_BPID410",
            "className": "BPID_BPID410",
            "orientation" : 'landscape',
            "questions": [
                {
                    "id": "BPID_BPID410_Q0",
                    "mandatory": true
                }
            ]
        },
        {
            "horizonId": "5942d2d765c6481695e6ca5c",
            "id": "BPID_BPID420",
            "className": "BPID_BPID420",
            "questions": [
                {
                    "id": "BPID_BPID420_Q0",
                    "mandatory": true
                }
            ]
        }
    ],

    questions: [
        {
            "horizonId": "5942d2d365c6481695e6ca2a",
            "id": "BPID_BPID010_Q0",
            "IG": "BPISFD",
            "IT": "OTHPAN1L",
            "text": "BPID_BPID010_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID010_Q0",
            "widget": {
                "id": "BPID_BPID010_Q0_W0",
                "type": "CustomRadioButton",
                "answers": [
                    {
                        "value": "1",
                        "text": "BPID_BPID010_Q0_W0_A0"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID010_Q0_W0_A1"
                    }
                ]
            }
        },
        {
            "horizonId": "5942d2d365c6481695e6ca29",
            "id": "BPID_BPID020_Q0",
            "IG": "BPIDiagram",
            "IT": "",
            "text": "BPID_BPID020_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID020_Q0",
            "widget": {
                "id": "BPID_BPID020_Q0_W0",
                "type": "BPISFBodyDiagram",
                "templates": {
                    "container": "BPISF:FrontBackBodyMostPain"
                },
                "height": 375,
                "labelNamespace": "BPID",
                "labels": {
                    "front": "BPI020_Q1_2",
                    "front_left": "BPI020_Q1_4",
                    "front_right": "BPI020_Q1_3",
                    "back": "BPI020_Q1_5",
                    "back_left": "BPI020_Q1_6",
                    "back_right": "BPI020_Q1_7",
                    "no_pain" : "BPI020_Q2_A1"
                },
                "bodyParts": [
                    {"value": "1", "IT": "FR_HEA1B"},
                    {"value": "2", "IT": "FR_SHL1B"},
                    {"value": "4", "IT": "FR_SHL2B"},
                    {"value": "3", "IT": "FR_THR1B"},
                    {"value": "6", "IT": "FR_THR2B"},
                    {"value": "7", "IT": "FR_THR3B"},
                    {"value": "8", "IT": "FR_THR4B"},
                    {"value": "10", "IT": "FR_THR5B"},
                    {"value": "11", "IT": "FR_THR6B"},
                    {"value": "12", "IT": "FR_THR7B"},
                    {"value": "13", "IT": "FR_THR8B"},
                    {"value": "14", "IT": "FR_THR9B"},
                    {"value": "5", "IT": "FR_ARM1B"},
                    {"value": "9", "IT": "FR_ARM2B"},
                    {"value": "34", "IT": "FR_ARM3B"},
                    {"value": "35", "IT": "FR_ARM4B"},
                    {"value": "50", "IT": "FR_ARM5B"},
                    {"value": "51", "IT": "FR_ARM6B"},
                    {"value": "15", "IT": "FR_LEG1B"},
                    {"value": "16", "IT": "FR_LEG2B"},
                    {"value": "36", "IT": "FR_LEG3B"},
                    {"value": "37", "IT": "FR_LEG4B"},
                    {"value": "38", "IT": "FR_LEG5B"},
                    {"value": "39", "IT": "FR_LEG6B"},
                    {"value": "40", "IT": "FR_LEG7B"},
                    {"value": "41", "IT": "FR_LEG8B"},
                    {"value": "17", "IT": "BK_HEA1B"},
                    {"value": "18", "IT": "BK_NCK1B"},
                    {"value": "19", "IT": "BK_SHL1B"},
                    {"value": "21", "IT": "BK_SHL2B"},
                    {"value": "20", "IT": "BK_THR1B"},
                    {"value": "23", "IT": "BK_THR2B"},
                    {"value": "24", "IT": "BK_THR3B"},
                    {"value": "25", "IT": "BK_THR4B"},
                    {"value": "27", "IT": "BK_THR5B"},
                    {"value": "28", "IT": "BK_THR6B"},
                    {"value": "29", "IT": "BK_THR7B"},
                    {"value": "30", "IT": "BK_THR8B"},
                    {"value": "31", "IT": "BK_THR9B"},
                    {"value": "22", "IT": "BK_ARM1B"},
                    {"value": "26", "IT": "BK_ARM2B"},
                    {"value": "42", "IT": "BK_ARM3B"},
                    {"value": "43", "IT": "BK_ARM4B"},
                    {"value": "52", "IT": "BK_ARM5B"},
                    {"value": "53", "IT": "BK_ARM6B"},
                    {"value": "32", "IT": "BK_LEG1B"},
                    {"value": "33", "IT": "BK_LEG2B"},
                    {"value": "44", "IT": "BK_LEG3B"},
                    {"value": "45", "IT": "BK_LEG4B"},
                    {"value": "46", "IT": "BK_LEG5B"},
                    {"value": "47", "IT": "BK_LEG6B"},
                    {"value": "48", "IT": "BK_LEG7B"},
                    {"value": "49", "IT": "BK_LEG8B"}
                ]
            }
        },
        {
            "horizonId": "5942d2d365c6481695e6ca2b",
            "id": "BPID_BPID030_Q0",
            "IG": "BPIDiagram",
            "IT": "",
            "text": "BPID_BPID030_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID030_Q0",
            "widget": {
                "id": "BPID_BPID030_Q0_W0",
                "type": "BPISFBodyDiagramZoom",
                "templates": {
                    "container": "BPISF:FrontTorsoMostPain"
                },
                "height": 375,
                "labels": {
                    "front": "BPI030_Q1_2",
                    "front_right": "BPI030_Q1_3",
                    "front_left": "BPI030_Q1_4"
                }
            }
        },
        {
            "horizonId": "5942d2d365c6481695e6ca2c",
            "id": "BPID_BPID045_Q0",
            "IG": "BPIDiagram",
            "IT": "",
            "text": "BPID_BPID045_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID045_Q0",
            "widget": {
                "id": "BPID_BPID045_Q0_W0",
                "type": "BPISFBodyDiagramZoom",
                "templates": {
                    "container": "BPISF:FrontLegsMostPain"
                },
                "height": 375,
                "labels": {
                    "front": "BPI045_Q1_2",
                    "front_right": "BPI045_Q1_3",
                    "front_left": "BPI045_Q1_4"
                }
            }
        },
        {
            "horizonId": "5942d2d365c6481695e6ca2d",
            "id": "BPID_BPID050_Q0",
            "IG": "BPIDiagram",
            "IT": "",
            "text": "BPID_BPID050_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID050_Q0",
            "widget": {
                "id": "BPID_BPID050_Q0_W0",
                "type": "BPISFBodyDiagramZoom",
                "templates": {
                    "container": "BPISF:BackTorsoMostPain"
                },
                "height": 375,
                "labels": {
                    "back": "BPI050_Q1_2",
                    "back_left": "BPI050_Q1_3",
                    "back_right": "BPI050_Q1_4"
                }
            }
        },
        {
            "horizonId": "5942d2d365c6481695e6ca2e",
            "id": "BPID_BPID065_Q0",
            "IG": "BPIDiagram",
            "IT": "",
            "text": "BPID_BPID065_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID065_Q0",
            "widget": {
                "id": "BPID_BPID065_Q0_W0",
                "type": "BPISFBodyDiagramZoom",
                "templates": {
                    "container": "BPISF:BackLegsMostPain"
                },
                "height": 375,
                "labels": {
                    "back": "BPI065_Q1_2",
                    "back_left": "BPI065_Q1_3",
                    "back_right": "BPI065_Q1_4"
                }
            }
        },
        {
            "horizonId": "5942d2d465c6481695e6ca2f",
            "id": "BPID_BPID100_Q0",
            "IG": "BPIDiagram",
            "IT": "",
            "text": "BPID_BPID100_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID100_Q0",
            "widget": {
                "id": "BPID_BPID100_Q0_W0",
                "type": "BPISFBodyDiagramWorstPain",
                "templates": {
                    "container": "BPISF:FrontBackBodyMostPain"
                },
                "height": 375,
                "labelNamespace": "BPID",
                "labels": {
                    "front": "BPI100_Q1_2",
                    "front_left": "BPI100_Q1_4",
                    "front_right": "BPI100_Q1_3",
                    "back": "BPI100_Q1_5",
                    "back_left": "BPI100_Q1_6",
                    "back_right": "BPI100_Q1_7",
                    "no_pain" : "BPI020_Q2_A1"
                }
            }
        },
        {
            "horizonId": "5942d2d465c6481695e6ca30",
            "id": "BPID_BPID110_Q0",
            "IG": "-",
            "IT": "-",
            "text": "BPID_BPID110_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID110_Q0",
            "widget": {
                "id": "BPID_BPID110_Q0_W0",
                "type": "BPISFBodyDiagramWorstPainZoom",
                "templates": {
                    "container": "BPISF:FrontTorsoMostPain"
                },
                "height": 375,
                "labels": {
                    "front": "BPI110_Q1_2",
                    "front_left": "BPI110_Q1_4",
                    "front_right": "BPI110_Q1_3"
                }
            }
        },
        {
            "horizonId": "5942d2d465c6481695e6ca32",
            "id": "BPID_BPID125_Q0",
            "IG": "",
            "IT": "",
            "text": "BPID_BPID125_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID125_Q0",
            "widget": {
                "id": "BPID_BPID125_Q0_W0",
                "type": "BPISFBodyDiagramWorstPainZoom",
                "templates": {
                    "container": "BPISF:FrontLegsMostPain"
                },
                "height": 375,
                "labels": {
                    "front": "BPI125_Q1_2",
                    "front_left": "BPI125_Q1_4",
                    "front_right": "BPI125_Q1_3"
                }
            }
        },
        {
            "horizonId": "5942d2d465c6481695e6ca31",
            "id": "BPID_BPID130_Q0",
            "IG": "",
            "IT": "",
            "text": "BPID_BPID130_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID130_Q0",
            "widget": {
                "id": "BPID_BPID130_Q0_W0",
                "type": "BPISFBodyDiagramWorstPainZoom",
                "templates": {
                    "container": "BPISF:BackTorsoMostPain"
                },
                "height": 375,
                "labels": {
                    "back": "BPI130_Q1_2",
                    "back_left": "BPI130_Q1_3",
                    "back_right": "BPI130_Q1_4"
                }
            }
        },
        {
            "horizonId": "5942d2d465c6481695e6ca33",
            "id": "BPID_BPID145_Q0",
            "IG": "",
            "IT": "",
            "text": "BPID_BPID145_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID145_Q0",
            "widget": {
                "id": "BPID_BPID145_Q0_W0",
                "type": "BPISFBodyDiagramWorstPainZoom",
                "templates": {
                    "container": "BPISF:BackLegsMostPain"
                },
                "height": 375,
                "labels": {
                    "back": "BPI145_Q1_2",
                    "back_left": "BPI145_Q1_3",
                    "back_right": "BPI145_Q1_4"
                }
            }
        },
        {
            "horizonId": "5942d2d465c6481695e6ca34",
            "id": "BPID_BPID200_Q0",
            "text": "BPID_BPID200_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID200_Q0"
        },
        {
            "horizonId": "5942d2d465c6481695e6ca35",
            "id": "BPID_BPID210_Q0",
            "IG": "BPIDiagram",
            "IT": "EDTMST1L",
            "text": "BPID_BPID210_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID210_Q0",
            "widget": {
                "id": "BPID_BPID210_Q0_W0",
                "type": "CustomRadioButton",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID210_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID210_Q0_W0_A1"
                    }
                ]
            }
        },
        {
            "horizonId": "5942d2d465c6481695e6ca36",
            "id": "BPID_BPID300_Q0",
            "IG": "BPISFD",
            "IT": "WRSPAN1L",
            "text": "BPID_BPID300_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID300_Q0",
            "widget": {
                "id": "BPID_BPID300_Q0_W0",
                "type": "NumericRatingScale",
                "width": "100",
                "displayText": "BPID_BPID300_Q0_W0_DisplayText",
                "reverseOnRtl": true,
                "text": "BPID_BPID300_Q0_W0_Text",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID300_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID300_Q0_W0_A1"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID300_Q0_W0_A2"
                    },
                    {
                        "value": "3",
                        "text": "BPID_BPID300_Q0_W0_A3"
                    },
                    {
                        "value": "4",
                        "text": "BPID_BPID300_Q0_W0_A4"
                    },
                    {
                        "value": "5",
                        "text": "BPID_BPID300_Q0_W0_A5"
                    },
                    {
                        "value": "6",
                        "text": "BPID_BPID300_Q0_W0_A6"
                    },
                    {
                        "value": "7",
                        "text": "BPID_BPID300_Q0_W0_A7"
                    },
                    {
                        "value": "8",
                        "text": "BPID_BPID300_Q0_W0_A8"
                    },
                    {
                        "value": "9",
                        "text": "BPID_BPID300_Q0_W0_A9"
                    },
                    {
                        "value": "10",
                        "text": "BPID_BPID300_Q0_W0_A10"
                    }
                ],
                "markers": {
                    "left": "BPID_BPID300_Q0_W0_LeftMarker",
                    "right": "BPID_BPID300_Q0_W0_RightMarker",
                    "justification": "outside",
                    "position": "below",
                    "arrow": true,
                    "spaceAllowed": 2
                }
            }
        },
        {
            "horizonId": "5942d2d465c6481695e6ca37",
            "id": "BPID_BPID310_Q0",
            "IG": "BPISFD",
            "IT": "LSTPAN1L",
            "text": "BPID_BPID310_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID310_Q0",
            "widget": {
                "id": "BPID_BPID310_Q0_W0",
                "type": "NumericRatingScale",
                "width": "100",
                "displayText": "BPID_BPID310_Q0_W0_DisplayText",
                "reverseOnRtl": true,
                "text": "BPID_BPID310_Q0_W0_Text",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID310_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID310_Q0_W0_A1"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID310_Q0_W0_A2"
                    },
                    {
                        "value": "3",
                        "text": "BPID_BPID310_Q0_W0_A3"
                    },
                    {
                        "value": "4",
                        "text": "BPID_BPID310_Q0_W0_A4"
                    },
                    {
                        "value": "5",
                        "text": "BPID_BPID310_Q0_W0_A5"
                    },
                    {
                        "value": "6",
                        "text": "BPID_BPID310_Q0_W0_A6"
                    },
                    {
                        "value": "7",
                        "text": "BPID_BPID310_Q0_W0_A7"
                    },
                    {
                        "value": "8",
                        "text": "BPID_BPID310_Q0_W0_A8"
                    },
                    {
                        "value": "9",
                        "text": "BPID_BPID310_Q0_W0_A9"
                    },
                    {
                        "value": "10",
                        "text": "BPID_BPID310_Q0_W0_A10"
                    }
                ],
                "markers": {
                    "left": "BPID_BPID310_Q0_W0_LeftMarker",
                    "right": "BPID_BPID310_Q0_W0_RightMarker",
                    "justification": "outside",
                    "position": "below",
                    "arrow": true,
                    "spaceAllowed": 2
                }
            }
        },
        {
            "horizonId": "5942d2d465c6481695e6ca38",
            "id": "BPID_BPID320_Q0",
            "IG": "BPISFD",
            "IT": "AVGPAN1L",
            "text": "BPID_BPID320_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID320_Q0",
            "widget": {
                "id": "BPID_BPID320_Q0_W0",
                "type": "NumericRatingScale",
                "width": "100",
                "displayText": "BPID_BPID320_Q0_W0_DisplayText",
                "reverseOnRtl": true,
                "text": "BPID_BPID320_Q0_W0_Text",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID320_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID320_Q0_W0_A1"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID320_Q0_W0_A2"
                    },
                    {
                        "value": "3",
                        "text": "BPID_BPID320_Q0_W0_A3"
                    },
                    {
                        "value": "4",
                        "text": "BPID_BPID320_Q0_W0_A4"
                    },
                    {
                        "value": "5",
                        "text": "BPID_BPID320_Q0_W0_A5"
                    },
                    {
                        "value": "6",
                        "text": "BPID_BPID320_Q0_W0_A6"
                    },
                    {
                        "value": "7",
                        "text": "BPID_BPID320_Q0_W0_A7"
                    },
                    {
                        "value": "8",
                        "text": "BPID_BPID320_Q0_W0_A8"
                    },
                    {
                        "value": "9",
                        "text": "BPID_BPID320_Q0_W0_A9"
                    },
                    {
                        "value": "10",
                        "text": "BPID_BPID320_Q0_W0_A10"
                    }
                ],
                "markers": {
                    "left": "BPID_BPID320_Q0_W0_LeftMarker",
                    "right": "BPID_BPID320_Q0_W0_RightMarker",
                    "justification": "outside",
                    "position": "below",
                    "arrow": true,
                    "spaceAllowed": 2
                }
            }
        },
        {
            "horizonId": "5942d2d465c6481695e6ca39",
            "id": "BPID_BPID330_Q0",
            "IG": "BPISFD",
            "IT": "CURPAN1L",
            "text": "BPID_BPID330_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID330_Q0",
            "widget": {
                "id": "BPID_BPID330_Q0_W0",
                "type": "NumericRatingScale",
                "width": "100",
                "displayText": "BPID_BPID330_Q0_W0_DisplayText",
                "reverseOnRtl": true,
                "text": "BPID_BPID330_Q0_W0_Text",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID330_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID330_Q0_W0_A1"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID330_Q0_W0_A2"
                    },
                    {
                        "value": "3",
                        "text": "BPID_BPID330_Q0_W0_A3"
                    },
                    {
                        "value": "4",
                        "text": "BPID_BPID330_Q0_W0_A4"
                    },
                    {
                        "value": "5",
                        "text": "BPID_BPID330_Q0_W0_A5"
                    },
                    {
                        "value": "6",
                        "text": "BPID_BPID330_Q0_W0_A6"
                    },
                    {
                        "value": "7",
                        "text": "BPID_BPID330_Q0_W0_A7"
                    },
                    {
                        "value": "8",
                        "text": "BPID_BPID330_Q0_W0_A8"
                    },
                    {
                        "value": "9",
                        "text": "BPID_BPID330_Q0_W0_A9"
                    },
                    {
                        "value": "10",
                        "text": "BPID_BPID330_Q0_W0_A10"
                    }
                ],
                "markers": {
                    "left": "BPID_BPID330_Q0_W0_LeftMarker",
                    "right": "BPID_BPID330_Q0_W0_RightMarker",
                    "justification": "outside",
                    "position": "below",
                    "arrow": true,
                    "spaceAllowed": 2
                }
            }
        },
        {
            "horizonId": "5942d2d465c6481695e6ca3a",
            "id": "BPID_BPID340_Q0",
            "IG": "BPISFD",
            "IT": "PANRLF1L",
            "text": "BPID_BPID340_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID340_Q0",
            "widget": {
                "id": "BPID_BPID340_Q0_W0",
                "type": "NumericRatingScale",
                "width": "100",
                "displayText": "BPID_BPID340_Q0_W0_DisplayText",
                "reverseOnRtl": true,
                "text": "BPID_BPID340_Q0_W0_Text",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID340_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID340_Q0_W0_A1"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID340_Q0_W0_A2"
                    },
                    {
                        "value": "3",
                        "text": "BPID_BPID340_Q0_W0_A3"
                    },
                    {
                        "value": "4",
                        "text": "BPID_BPID340_Q0_W0_A4"
                    },
                    {
                        "value": "5",
                        "text": "BPID_BPID340_Q0_W0_A5"
                    },
                    {
                        "value": "6",
                        "text": "BPID_BPID340_Q0_W0_A6"
                    },
                    {
                        "value": "7",
                        "text": "BPID_BPID340_Q0_W0_A7"
                    },
                    {
                        "value": "8",
                        "text": "BPID_BPID340_Q0_W0_A8"
                    },
                    {
                        "value": "9",
                        "text": "BPID_BPID340_Q0_W0_A9"
                    },
                    {
                        "value": "10",
                        "text": "BPID_BPID340_Q0_W0_A10"
                    }
                ],
                "markers": {
                    "left": "BPID_BPID340_Q0_W0_LeftMarker",
                    "right": "BPID_BPID340_Q0_W0_RightMarker",
                    "justification": "outside",
                    "position": "below",
                    "arrow": true,
                    "spaceAllowed": 2
                }
            }
        },
        {
            "horizonId": "5942d2d465c6481695e6ca3b",
            "id": "BPID_BPID350_Q0",
            "IG": "BPISFD",
            "IT": "GENACT1L",
            "text": "BPID_BPID350_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID350_Q0",
            "widget": {
                "id": "BPID_BPID350_Q0_W0",
                "type": "NumericRatingScale",
                "width": "100",
                "displayText": "BPID_BPID350_Q0_W0_DisplayText",
                "reverseOnRtl": true,
                "text": "BPID_BPID350_Q0_W0_Text",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID350_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID350_Q0_W0_A1"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID350_Q0_W0_A2"
                    },
                    {
                        "value": "3",
                        "text": "BPID_BPID350_Q0_W0_A3"
                    },
                    {
                        "value": "4",
                        "text": "BPID_BPID350_Q0_W0_A4"
                    },
                    {
                        "value": "5",
                        "text": "BPID_BPID350_Q0_W0_A5"
                    },
                    {
                        "value": "6",
                        "text": "BPID_BPID350_Q0_W0_A6"
                    },
                    {
                        "value": "7",
                        "text": "BPID_BPID350_Q0_W0_A7"
                    },
                    {
                        "value": "8",
                        "text": "BPID_BPID350_Q0_W0_A8"
                    },
                    {
                        "value": "9",
                        "text": "BPID_BPID350_Q0_W0_A9"
                    },
                    {
                        "value": "10",
                        "text": "BPID_BPID350_Q0_W0_A10"
                    }
                ],
                "markers": {
                    "left": "BPID_BPID350_Q0_W0_LeftMarker",
                    "right": "BPID_BPID350_Q0_W0_RightMarker",
                    "justification": "outside",
                    "position": "below",
                    "arrow": true,
                    "spaceAllowed": 2
                }
            }
        },
        {
            "horizonId": "5942d2d565c6481695e6ca3c",
            "id": "BPID_BPID360_Q0",
            "IG": "BPISFD",
            "IT": "MODPAN1L",
            "text": "BPID_BPID360_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID360_Q0",
            "widget": {
                "id": "BPID_BPID360_Q0_W0",
                "type": "NumericRatingScale",
                "width": "100",
                "displayText": "BPID_BPID360_Q0_W0_DisplayText",
                "reverseOnRtl": true,
                "text": "BPID_BPID360_Q0_W0_Text",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID360_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID360_Q0_W0_A1"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID360_Q0_W0_A2"
                    },
                    {
                        "value": "3",
                        "text": "BPID_BPID360_Q0_W0_A3"
                    },
                    {
                        "value": "4",
                        "text": "BPID_BPID360_Q0_W0_A4"
                    },
                    {
                        "value": "5",
                        "text": "BPID_BPID360_Q0_W0_A5"
                    },
                    {
                        "value": "6",
                        "text": "BPID_BPID360_Q0_W0_A6"
                    },
                    {
                        "value": "7",
                        "text": "BPID_BPID360_Q0_W0_A7"
                    },
                    {
                        "value": "8",
                        "text": "BPID_BPID360_Q0_W0_A8"
                    },
                    {
                        "value": "9",
                        "text": "BPID_BPID360_Q0_W0_A9"
                    },
                    {
                        "value": "10",
                        "text": "BPID_BPID360_Q0_W0_A10"
                    }
                ],
                "markers": {
                    "left": "BPID_BPID360_Q0_W0_LeftMarker",
                    "right": "BPID_BPID360_Q0_W0_RightMarker",
                    "justification": "outside",
                    "position": "below",
                    "arrow": true,
                    "spaceAllowed": 2
                }
            }
        },
        {
            "horizonId": "5942d2d565c6481695e6ca3d",
            "id": "BPID_BPID370_Q0",
            "IG": "BPISFD",
            "IT": "WLKABL1L",
            "text": "BPID_BPID370_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID370_Q0",
            "widget": {
                "id": "BPID_BPID370_Q0_W0",
                "type": "NumericRatingScale",
                "width": "100",
                "displayText": "BPID_BPID370_Q0_W0_DisplayText",
                "reverseOnRtl": true,
                "text": "BPID_BPID370_Q0_W0_Text",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID370_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID370_Q0_W0_A1"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID370_Q0_W0_A2"
                    },
                    {
                        "value": "3",
                        "text": "BPID_BPID370_Q0_W0_A3"
                    },
                    {
                        "value": "4",
                        "text": "BPID_BPID370_Q0_W0_A4"
                    },
                    {
                        "value": "5",
                        "text": "BPID_BPID370_Q0_W0_A5"
                    },
                    {
                        "value": "6",
                        "text": "BPID_BPID370_Q0_W0_A6"
                    },
                    {
                        "value": "7",
                        "text": "BPID_BPID370_Q0_W0_A7"
                    },
                    {
                        "value": "8",
                        "text": "BPID_BPID370_Q0_W0_A8"
                    },
                    {
                        "value": "9",
                        "text": "BPID_BPID370_Q0_W0_A9"
                    },
                    {
                        "value": "10",
                        "text": "BPID_BPID370_Q0_W0_A10"
                    }
                ],
                "markers": {
                    "left": "BPID_BPID370_Q0_W0_LeftMarker",
                    "right": "BPID_BPID370_Q0_W0_RightMarker",
                    "justification": "outside",
                    "position": "below",
                    "arrow": true,
                    "spaceAllowed": 2
                }
            }
        },
        {
            "horizonId": "5942d2d565c6481695e6ca3e",
            "id": "BPID_BPID380_Q0",
            "IG": "BPISFD",
            "IT": "WRKINT1L",
            "text": "BPID_BPID380_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID380_Q0",
            "widget": {
                "id": "BPID_BPID380_Q0_W0",
                "type": "NumericRatingScale",
                "width": "100",
                "displayText": "BPID_BPID380_Q0_W0_DisplayText",
                "reverseOnRtl": true,
                "text": "BPID_BPID380_Q0_W0_Text",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID380_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID380_Q0_W0_A1"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID380_Q0_W0_A2"
                    },
                    {
                        "value": "3",
                        "text": "BPID_BPID380_Q0_W0_A3"
                    },
                    {
                        "value": "4",
                        "text": "BPID_BPID380_Q0_W0_A4"
                    },
                    {
                        "value": "5",
                        "text": "BPID_BPID380_Q0_W0_A5"
                    },
                    {
                        "value": "6",
                        "text": "BPID_BPID380_Q0_W0_A6"
                    },
                    {
                        "value": "7",
                        "text": "BPID_BPID380_Q0_W0_A7"
                    },
                    {
                        "value": "8",
                        "text": "BPID_BPID380_Q0_W0_A8"
                    },
                    {
                        "value": "9",
                        "text": "BPID_BPID380_Q0_W0_A9"
                    },
                    {
                        "value": "10",
                        "text": "BPID_BPID380_Q0_W0_A10"
                    }
                ],
                "markers": {
                    "left": "BPID_BPID380_Q0_W0_LeftMarker",
                    "right": "BPID_BPID380_Q0_W0_RightMarker",
                    "justification": "outside",
                    "position": "below",
                    "arrow": true,
                    "spaceAllowed": 2
                }
            }
        },
        {
            "horizonId": "5942d2d565c6481695e6ca3f",
            "id": "BPID_BPID390_Q0",
            "IG": "BPISFD",
            "IT": "INTREL1L",
            "text": "BPID_BPID390_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID390_Q0",
            "widget": {
                "id": "BPID_BPID390_Q0_W0",
                "type": "NumericRatingScale",
                "width": "100",
                "displayText": "BPID_BPID390_Q0_W0_DisplayText",
                "reverseOnRtl": true,
                "text": "BPID_BPID390_Q0_W0_Text",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID390_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID390_Q0_W0_A1"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID390_Q0_W0_A2"
                    },
                    {
                        "value": "3",
                        "text": "BPID_BPID390_Q0_W0_A3"
                    },
                    {
                        "value": "4",
                        "text": "BPID_BPID390_Q0_W0_A4"
                    },
                    {
                        "value": "5",
                        "text": "BPID_BPID390_Q0_W0_A5"
                    },
                    {
                        "value": "6",
                        "text": "BPID_BPID390_Q0_W0_A6"
                    },
                    {
                        "value": "7",
                        "text": "BPID_BPID390_Q0_W0_A7"
                    },
                    {
                        "value": "8",
                        "text": "BPID_BPID390_Q0_W0_A8"
                    },
                    {
                        "value": "9",
                        "text": "BPID_BPID390_Q0_W0_A9"
                    },
                    {
                        "value": "10",
                        "text": "BPID_BPID390_Q0_W0_A10"
                    }
                ],
                "markers": {
                    "left": "BPID_BPID390_Q0_W0_LeftMarker",
                    "right": "BPID_BPID390_Q0_W0_RightMarker",
                    "justification": "outside",
                    "position": "below",
                    "arrow": true,
                    "spaceAllowed": 2
                }
            }
        },
        {
            "horizonId": "5942d2d565c6481695e6ca40",
            "id": "BPID_BPID400_Q0",
            "IG": "BPISFD",
            "IT": "INTSLP1L",
            "text": "BPID_BPID400_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID400_Q0",
            "widget": {
                "id": "BPID_BPID400_Q0_W0",
                "type": "NumericRatingScale",
                "width": "100",
                "displayText": "BPID_BPID400_Q0_W0_DisplayText",
                "reverseOnRtl": true,
                "text": "BPID_BPID400_Q0_W0_Text",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID400_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID400_Q0_W0_A1"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID400_Q0_W0_A2"
                    },
                    {
                        "value": "3",
                        "text": "BPID_BPID400_Q0_W0_A3"
                    },
                    {
                        "value": "4",
                        "text": "BPID_BPID400_Q0_W0_A4"
                    },
                    {
                        "value": "5",
                        "text": "BPID_BPID400_Q0_W0_A5"
                    },
                    {
                        "value": "6",
                        "text": "BPID_BPID400_Q0_W0_A6"
                    },
                    {
                        "value": "7",
                        "text": "BPID_BPID400_Q0_W0_A7"
                    },
                    {
                        "value": "8",
                        "text": "BPID_BPID400_Q0_W0_A8"
                    },
                    {
                        "value": "9",
                        "text": "BPID_BPID400_Q0_W0_A9"
                    },
                    {
                        "value": "10",
                        "text": "BPID_BPID400_Q0_W0_A10"
                    }
                ],
                "markers": {
                    "left": "BPID_BPID400_Q0_W0_LeftMarker",
                    "right": "BPID_BPID400_Q0_W0_RightMarker",
                    "justification": "outside",
                    "position": "below",
                    "arrow": true,
                    "spaceAllowed": 2
                }
            }
        },
        {
            "horizonId": "5942d2d565c6481695e6ca41",
            "id": "BPID_BPID410_Q0",
            "IG": "BPISFD",
            "IT": "INTENJ1L",
            "text": "BPID_BPID410_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID410_Q0",
            "widget": {
                "id": "BPID_BPID410_Q0_W0",
                "type": "NumericRatingScale",
                "width": "100",
                "displayText": "BPID_BPID410_Q0_W0_DisplayText",
                "reverseOnRtl": true,
                "text": "BPID_BPID410_Q0_W0_Text",
                "answers": [
                    {
                        "value": "0",
                        "text": "BPID_BPID410_Q0_W0_A0"
                    },
                    {
                        "value": "1",
                        "text": "BPID_BPID410_Q0_W0_A1"
                    },
                    {
                        "value": "2",
                        "text": "BPID_BPID410_Q0_W0_A2"
                    },
                    {
                        "value": "3",
                        "text": "BPID_BPID410_Q0_W0_A3"
                    },
                    {
                        "value": "4",
                        "text": "BPID_BPID410_Q0_W0_A4"
                    },
                    {
                        "value": "5",
                        "text": "BPID_BPID410_Q0_W0_A5"
                    },
                    {
                        "value": "6",
                        "text": "BPID_BPID410_Q0_W0_A6"
                    },
                    {
                        "value": "7",
                        "text": "BPID_BPID410_Q0_W0_A7"
                    },
                    {
                        "value": "8",
                        "text": "BPID_BPID410_Q0_W0_A8"
                    },
                    {
                        "value": "9",
                        "text": "BPID_BPID410_Q0_W0_A9"
                    },
                    {
                        "value": "10",
                        "text": "BPID_BPID410_Q0_W0_A10"
                    }
                ],
                "markers": {
                    "left": "BPID_BPID410_Q0_W0_LeftMarker",
                    "right": "BPID_BPID410_Q0_W0_RightMarker",
                    "justification": "outside",
                    "position": "below",
                    "arrow": true,
                    "spaceAllowed": 2
                }
            }
        },
        {
            "horizonId": "5942d2d565c6481695e6ca42",
            "id": "BPID_BPID420_Q0",
            "text": "BPID_BPID420_Q0_TEXT",
            "title": "",
            "className": "BPID_BPID420_Q0"
        }
    ]
}

