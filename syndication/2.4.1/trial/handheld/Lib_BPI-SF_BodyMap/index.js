import {mergeObjects} from 'core/utilities/languageExtensions';

import BPISF_BodyMap_Diary_Config from './diary/BPI-SF2';
import './branching';
import './widget';

import rules from './rules';
import templates from './templates';

let studyDesign = {}; //object
studyDesign = mergeObjects (studyDesign, BPISF_BodyMap_Diary_Config, rules, templates);

export default {studyDesign};


