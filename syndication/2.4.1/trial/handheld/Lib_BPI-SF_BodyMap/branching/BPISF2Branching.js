import BodyImageMapStorageWithoutLatestFeatures from '../widget/BodyImageMapStorageWithoutLatestFeatures';

//noinspection JSUnusedLocalSymbols
LF.Branching.branchFunctions.BPI020Branch =  function (branchObj) {

    return new Q.Promise((resolve) => {

        let noPainSelected = BodyImageMapStorageWithoutLatestFeatures.getNoPainSelected();

        if(noPainSelected) {
            branchObj.branchTo = "BPID_BPID300";
        } else {
            branchObj.branchTo = "BPID_BPID100";
        }

        resolve(true);

    });
};

//noinspection JSUnusedLocalSymbols
LF.Branching.branchFunctions.BPI030Branch = function (branchObj) {

    return new Q.Promise((resolve) => {

        LF.Data.Questionnaire.screenStack.pop();

        BodyImageMapStorageWithoutLatestFeatures.emptyRecentlySelectedIDs();

        resolve(true);

    });
};

//noinspection JSUnusedLocalSymbols
LF.Branching.branchFunctions.BPI045Branch = function (branchObj, callback, view) {
    return new Q.Promise((resolve) => {

        LF.Data.Questionnaire.screenStack.pop();

        BodyImageMapStorageWithoutLatestFeatures.emptyRecentlySelectedIDs();

        resolve(true);

    });
};

//noinspection JSUnusedLocalSymbols
LF.Branching.branchFunctions.BPI050Branch = function (branchObj, callback, view) {
    return new Q.Promise((resolve) => {

        LF.Data.Questionnaire.screenStack.pop();

        BodyImageMapStorageWithoutLatestFeatures.emptyRecentlySelectedIDs();

        resolve(true);

    });
};

//noinspection JSUnusedLocalSymbols
LF.Branching.branchFunctions.BPI065Branch = function (branchObj, callback, view) {
    return new Q.Promise((resolve) => {

        LF.Data.Questionnaire.screenStack.pop();

        BodyImageMapStorageWithoutLatestFeatures.emptyRecentlySelectedIDs();

        resolve(true);

    });
};

//noinspection JSUnusedLocalSymbols
LF.Branching.branchFunctions.BPI100Branch = function (branchObj) {
    return new Q.Promise((resolve) => {

        let selectedWorstPainID = BodyImageMapStorageWithoutLatestFeatures.getWorstPainSelectedIDs();

        if(selectedWorstPainID.length > 1) {
            branchObj.branchTo = "BPID_BPID200";
        }
        else if(!BodyImageMapStorageWithoutLatestFeatures.checkIfWorstPainSelectedIDExistsInBodySet()) {
            branchObj.branchTo = "BPID_BPID210";
        }
        else {
            branchObj.branchTo = "BPID_BPID300";
        }

        resolve(true);

    });
};

//noinspection JSUnusedLocalSymbols
LF.Branching.branchFunctions.BPI110Branch = function (branchObj) {
    return new Q.Promise((resolve) => {

        LF.Data.Questionnaire.screenStack.pop();

        BodyImageMapStorageWithoutLatestFeatures.emptyRecentlySelectedWorstPainIDs();

        resolve(true);

    });
};

//noinspection JSUnusedLocalSymbols
LF.Branching.branchFunctions.BPI125Branch = function (branchObj) {
    return new Q.Promise((resolve) => {

        LF.Data.Questionnaire.screenStack.pop();

        BodyImageMapStorageWithoutLatestFeatures.emptyRecentlySelectedWorstPainIDs();

        resolve(true);

    });
};

//noinspection JSUnusedLocalSymbols
LF.Branching.branchFunctions.BPI130Branch = function (branchObj) {
    return new Q.Promise((resolve) => {

        LF.Data.Questionnaire.screenStack.pop();

        BodyImageMapStorageWithoutLatestFeatures.emptyRecentlySelectedWorstPainIDs();

        resolve(true);

    });
};

//noinspection JSUnusedLocalSymbols
LF.Branching.branchFunctions.BPI145Branch = function (branchObj) {
    return new Q.Promise((resolve) => {

        LF.Data.Questionnaire.screenStack.pop();

        BodyImageMapStorageWithoutLatestFeatures.emptyRecentlySelectedWorstPainIDs();

        resolve(true);

    });
};

//noinspection JSUnusedLocalSymbols
LF.Branching.branchFunctions.BPI200BranchBack = function (branchObj) {
    return new Q.Promise((resolve) => {

        LF.Data.Questionnaire.screenStack.pop();

        resolve(true);

    });
};

//noinspection JSUnusedLocalSymbols
LF.Branching.branchFunctions.BPI210BranchBack = function (branchObj, view) {

    return new Q.Promise((resolve) => {

        var questionId = branchObj.branchParams.questionId,
            response,
            screen,
            screenNum,
            questions;

        if (branchObj.currentScreen === branchObj.branchFrom) {

            if (branchObj.branchParams.IG) {
                var igr = view.getCurrentIGR(branchObj.branchParams.IG);
                response = view.queryAnswersByQuestionIDAndIGR(questionId, igr)[0];

                if (response) {
                    response = response.get('response');
                }
            } else {
                response = LF.Branching.Helpers.responseForQuestionId(branchObj.answers, questionId);
            }

            if ('undefined' !== typeof response && branchObj.branchParams.value == response) {

                branchObj.branchTo = "BPID_BPID100";

                if (branchObj.clearBranchedResponses !== false) {

                    screenNum = view.screen;
                    screen = view.screens[screenNum];

                    questions = _(screen.get('questions')).pluck('id');

                    // QuestionnaireView does not clear the responses of the
                    // 'branchFrom' screen's questions
                    view.resetAnswersSelected(questions);
                }

                if (branchObj.branchParams && branchObj.branchParams.simulateBackButton) {
                    view.screenStack.pop();

                    //Just pop to the previous screen
                    branchObj.branchTo = view.screenStack.pop();
                } else {
                    // Clear the screens stacked above the 'branchTo' target
                    //noinspection StatementWithEmptyBodyJS
                    while (view.screenStack.pop() != branchObj.branchTo);
                }

                BodyImageMapStorageWithoutLatestFeatures.removeAllWorstPainSelectedID();

            } else {
                branchObj.branchTo = "BPID_BPID300";
            }

            resolve(true);
        } else {
            resolve(false);
        }
    });
};
