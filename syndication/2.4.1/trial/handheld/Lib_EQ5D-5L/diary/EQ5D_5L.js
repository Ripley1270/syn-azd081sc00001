
export default {

    questionnaires: [
        {
            "horizonId": "58dbec25bd3a7d680465a884",
            "id": "EQ5D5L",
            "SU": "EQ5D5L",
            "displayName": "DISPLAY_NAME",
            "className" : "HANDHELD EQ5D",
            "previousScreen" : false,
            "affidavit" : "DEFAULT",
            "screens": [
                "EQ5D5L_EQ010_5L",
                "EQ5D5L_EQ020_5L",
                "EQ5D5L_EQ030_5L",
                "EQ5D5L_EQ040_5L",
                "EQ5D5L_EQ050_5L",
                "EQ5D5L_EQ060_5L",
                "EQ5D5L_EQ070_5L",
                "EQ5D5L_EQ080_5L",
                "EQ5D5L_EQ090_5L"
            ],
            "branches": [],
            "initialScreen": []
        }
    ],
    screens: [
        {
            "horizonId": "58ff865b65c6481695e6b6a0",
            "id": "EQ5D5L_EQ010_5L",
            "className": "EQ5D5L_EQ010_5L",
            "questions": [
                {
                    "id": "EQ5D5L_EQ010_5L_Q0",
                    "mandatory": true
                }
            ]
        },{
            "horizonId": "58ff869e65c6481695e6b6a3",
            "id": "EQ5D5L_EQ020_5L",
            "className": "EQ5D5L_EQ020_5L",
            "questions": [
                {
                    "id": "EQ5D5L_EQ020_5L_Q0",
                    "mandatory": true
                }
            ]
        },{
            "horizonId": "58dbec24bd3a7d680465a871",
            "id": "EQ5D5L_EQ030_5L",
            "className": "EQ5D5L_EQ030_5L",
            "questions": [
                {
                    "id": "EQ5D5L_EQ030_5L_Q0",
                    "mandatory": true
                }
            ]
        },{
            "horizonId": "58dbec24bd3a7d680465a874",
            "id": "EQ5D5L_EQ040_5L",
            "className": "EQ5D5L_EQ040_5L",
            "questions": [
                {
                    "id": "EQ5D5L_EQ040_5L_Q0",
                    "mandatory": true
                }
            ]
        },{
            "horizonId": "58dbec24bd3a7d680465a877",
            "id": "EQ5D5L_EQ050_5L",
            "className": "EQ5D5L_EQ050_5L",
            "questions": [
                {
                    "id": "EQ5D5L_EQ050_5L_Q0",
                    "mandatory": true
                }
            ]
        },{
            "horizonId": "58dbec24bd3a7d680465a87a",
            "id": "EQ5D5L_EQ060_5L",
            "className": "EQ5D5L_EQ060_5L",
            "questions": [
                {
                    "id": "EQ5D5L_EQ060_5L_Q0",
                    "mandatory": true
                }
            ]
        },{
            "horizonId": "58dbec24bd3a7d680465a87d",
            "id": "EQ5D5L_EQ070_5L",
            "className": "EQ5D5L_EQ070_5L",
            "questions": [
                {
                    "id": "EQ5D5L_EQ070_5L_Q0",
                    "mandatory": true
                }
            ]
        },{
            "horizonId": "58dbec24bd3a7d680465a87f",
            "id": "EQ5D5L_EQ080_5L",
            "className": "EQ5D5L_EQ080_5L",
            "questions": [
                {
                    "id": "EQ5D5L_EQ080_5L_Q0",
                    "mandatory": true
                }
            ]
        },{
            "horizonId": "58dbec24bd3a7d680465a882",
            "id": "EQ5D5L_EQ090_5L",
            "className": "EQ5D5L_EQ090_5L",
            "questions": [
                {
                    "id": "EQ5D5L_EQ090_5L_Q0",
                    "mandatory": true
                }
            ]
        }
    ],
    questions: [
        {
            "horizonId": "58ff868665c6481695e6b6a1",
            "id": "EQ5D5L_EQ010_5L_Q0",
            "text": "EQ5D5L_EQ010_5L_Q0_TEXT",
            "title": "",
            "className": "EQ5D5L_EQ010_5L_Q0"
        },{
            "horizonId": "58ff869d65c6481695e6b6a2",
            "id": "EQ5D5L_EQ020_5L_Q0",
            "IG": "EQ5D5L",
            "IT": "HLTMBL1L",
            "text": "EQ5D5L_EQ020_5L_Q0_TEXT",
            "title": "",
            "className": "EQ5D5L_EQ020_5L_Q0",
            "widget": {
                "id": "EQ5D5L_EQ020_5L_Q0_W0",
                "type": "CustomRadioButton",
                "answers": [
                    {
                        "value": "1",
                        "text": "EQ5D5L_EQ020_5L_Q0_W0_A0"
                    },
                    {
                        "value": "2",
                        "text": "EQ5D5L_EQ020_5L_Q0_W0_A1"
                    },
                    {
                        "value": "3",
                        "text": "EQ5D5L_EQ020_5L_Q0_W0_A2"
                    },
                    {
                        "value": "4",
                        "text": "EQ5D5L_EQ020_5L_Q0_W0_A3"
                    },
                    {
                        "value": "5",
                        "text": "EQ5D5L_EQ020_5L_Q0_W0_A4"
                    }
                ]
            }
        },{
            "horizonId": "58dbec24bd3a7d680465a86b",
            "id": "EQ5D5L_EQ030_5L_Q0",
            "IG": "EQ5D5L",
            "IT": "SLFCAR1L",
            "text": "EQ5D5L_EQ030_5L_Q0_TEXT",
            "title": "",
            "className": "EQ5D5L_EQ030_5L_Q0",
            "widget": {
                "id": "EQ5D5L_EQ030_5L_Q0_W0",
                "type": "CustomRadioButton",
                "answers": [
                    {
                        "value": "1",
                        "text": "EQ5D5L_EQ030_5L_Q0_W0_A0"
                    },
                    {
                        "value": "2",
                        "text": "EQ5D5L_EQ030_5L_Q0_W0_A1"
                    },
                    {
                        "value": "3",
                        "text": "EQ5D5L_EQ030_5L_Q0_W0_A2"
                    },
                    {
                        "value": "4",
                        "text": "EQ5D5L_EQ030_5L_Q0_W0_A3"
                    },
                    {
                        "value": "5",
                        "text": "EQ5D5L_EQ030_5L_Q0_W0_A4"
                    }
                ]
            }
        },{
            "horizonId": "58dbec24bd3a7d680465a865",
            "id": "EQ5D5L_EQ040_5L_Q0",
            "IG": "EQ5D5L",
            "IT": "USLACT1L",
            "text": "EQ5D5L_EQ040_5L_Q0_TEXT",
            "title": "",
            "className": "EQ5D5L_EQ040_5L_Q0",
            "widget": {
                "id": "EQ5D5L_EQ040_5L_Q0_W0",
                "type": "CustomRadioButton",
                "answers": [
                    {
                        "value": "1",
                        "text": "EQ5D5L_EQ040_5L_Q0_W0_A0"
                    },
                    {
                        "value": "2",
                        "text": "EQ5D5L_EQ040_5L_Q0_W0_A1"
                    },
                    {
                        "value": "3",
                        "text": "EQ5D5L_EQ040_5L_Q0_W0_A2"
                    },
                    {
                        "value": "4",
                        "text": "EQ5D5L_EQ040_5L_Q0_W0_A3"
                    },
                    {
                        "value": "5",
                        "text": "EQ5D5L_EQ040_5L_Q0_W0_A4"
                    }
                ]
            }
        },{
            "horizonId": "58dbec24bd3a7d680465a866",
            "id": "EQ5D5L_EQ050_5L_Q0",
            "IG": "EQ5D5L",
            "IT": "HLTPAN1L",
            "text": "EQ5D5L_EQ050_5L_Q0_TEXT",
            "title": "",
            "className": "EQ5D5L_EQ050_5L_Q0",
            "widget": {
                "id": "EQ5D5L_EQ050_5L_Q0_W0",
                "type": "CustomRadioButton",
                "answers": [
                    {
                        "value": "1",
                        "text": "EQ5D5L_EQ050_5L_Q0_W0_A0"
                    },
                    {
                        "value": "2",
                        "text": "EQ5D5L_EQ050_5L_Q0_W0_A1"
                    },
                    {
                        "value": "3",
                        "text": "EQ5D5L_EQ050_5L_Q0_W0_A2"
                    },
                    {
                        "value": "4",
                        "text": "EQ5D5L_EQ050_5L_Q0_W0_A3"
                    },
                    {
                        "value": "5",
                        "text": "EQ5D5L_EQ050_5L_Q0_W0_A4"
                    }
                ]
            }
        },{
            "horizonId": "58dbec24bd3a7d680465a867",
            "id": "EQ5D5L_EQ060_5L_Q0",
            "IG": "EQ5D5L",
            "IT": "HLTANX1L",
            "text": "EQ5D5L_EQ060_5L_Q0_TEXT",
            "title": "",
            "className": "EQ5D5L_EQ060_5L_Q0",
            "widget": {
                "id": "EQ5D5L_EQ060_5L_Q0_W0",
                "type": "CustomRadioButton",
                "answers": [
                    {
                        "value": "1",
                        "text": "EQ5D5L_EQ060_5L_Q0_W0_A0"
                    },
                    {
                        "value": "2",
                        "text": "EQ5D5L_EQ060_5L_Q0_W0_A1"
                    },
                    {
                        "value": "3",
                        "text": "EQ5D5L_EQ060_5L_Q0_W0_A2"
                    },
                    {
                        "value": "4",
                        "text": "EQ5D5L_EQ060_5L_Q0_W0_A3"
                    },
                    {
                        "value": "5",
                        "text": "EQ5D5L_EQ060_5L_Q0_W0_A4"
                    }
                ]
            }
        },{
            "horizonId": "58dbec24bd3a7d680465a868",
            "id": "EQ5D5L_EQ070_5L_Q0",
            "text": "EQ5D5L_EQ070_5L_Q0_TEXT",
            "title": "",
            "className": "EQ5D5L_EQ070_5L_Q0"
        }, {
            "horizonId": "58dbec24bd3a7d680465a869",
            "id": "EQ5D5L_EQ080_5L_Q0",
            "IG": "EQ5D5L",
            "IT": "HLTSCR1N",
            "text": "EQ5D5L_EQ080_5L_Q0_TEXT",
            "title": "",
            "className": "EQ5D5L_EQ080_5L_Q0",
            'widget': {
                'id': 'EQ080_5L_Q0_W0',
                'type': 'EQ5D5LPDECustom',
                'displayAs': 'EQ5D',
                'extends': 'EQ5D', //this is being added for automation. Whenever there is a core widget that is not 'WidgetBase' Class extended, we have to explicitly mention what core widget was extended.
                'posting': true,
                'initialCursorDisplay': false,
                'pointer': {
                    'isVisible': true,
                    'location': 'right'
                },
                'selectedValue': {
                    'isVisible': true,
                    'location': 'dynamic'
                },
                'customProperties': {
                    'instructionText' : '',
                    'answerLabelText' : 'EQ5D_ANSWER_LABEL_TEXT'
                },
                'anchors' : {  //<-------------- configures the min and max anchors on the VAS
                    'swapMinMaxLocation' : true,
                    'min' : { //<--------------- configures the min value anchor
                        'text': 'EQ5D5L_EQ080_5L_Q0_W0_LEFT' // the text
                    },
                    'max' : { //<--------------- configures the max value anchor
                        'text': 'EQ5D5L_EQ080_5L_Q0_W0_RIGHT' // the text
                    }
                }
            }
        }, {
            "horizonId": "58dbec24bd3a7d680465a86a",
            "id": "EQ5D5L_EQ090_5L_Q0",
            "text": "EQ5D5L_EQ090_5L_Q0_TEXT",
            "title": "",
            "className": "EQ5D5L_EQ090_5L_Q0"
        }
    ],
    rules: [
    ]
};
