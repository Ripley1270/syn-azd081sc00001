import EQ5D from 'core/widgets/VAS/EQ5D/EQ5D';

export default class EQ5D5LPDECustom extends EQ5D {

    render(){
        return super.render()
            .then(() => {
                return this.drawTheVas();

            })
            .then(() => {
                let vasHeight = this.$('#mainVasCanvas').height() - (this.customProperties.topBottomMargin * 2),
                    postingBoxHeight = vasHeight / 3;

                postingBoxHeight += "px !important";

                this.$('#answerDIV').attr('style', "height : " + postingBoxHeight + "; padding : 10px");

            });
    }

    drawTheEQ5D () {
        let middle_rect,
            middle_height,
            middle_width,
            vasCanvasWidth,
            vasLineLength;

        // Use getBoundingClientRect, because simple jQuery $.width() rounds and messes things up.
        middle_rect = this.$middle.get(0).getBoundingClientRect();
        //middle_height = middle_rect.height;
        middle_height = 303;
        middle_width = middle_rect.width;

        // grab a reference to the actual canvas that gets drawn to and set it's initial height and width
        this.cvs.height = Math.floor(middle_height);
        this.cvs.width = Math.floor(middle_width);

        vasCanvasWidth = this.cvs.width;
        this.theCenterX = vasCanvasWidth / 2;

        // this is the length of the line itself.  Give a values.margin of 20 on each end.
        vasLineLength = middle_height - (this.customProperties.topBottomMargin * 2);

        this.tatmp = parseInt(this.anchors.max.value, 10) - parseInt(this.anchors.min.value, 10);

        // very simple calculation to get the location to draw each tick
        this.tickFreq = vasLineLength / this.tatmp;

        this.firstOne = this.customProperties.topBottomMargin;
        this.lastOne = (this.tickFreq * this.anchors.max.value) + this.customProperties.topBottomMargin;

        this.mainVasCanvasContext.beginPath();

        if (this.customProperties.tickValuePosition === 'left') {
            this.mainVasCanvasContext.font = this.fontString(this.tickMarks.values.font);
            this.mainVasCanvasContext.textAlign = 'right';
            this.mainVasCanvasContext.textBaseline = 'middle';

            this.mainVasCanvasContext.fillText(this.anchors.max.value, this.theCenterX - this.tickMarks.majorSize,
                this.customProperties.topBottomMargin);

            this.mainVasCanvasContext.fillText(this.anchors.min.value, this.theCenterX - this.tickMarks.majorSize,
                (this.tickFreq * this.anchors.max.value) + this.customProperties.topBottomMargin);
        } else {
            this.mainVasCanvasContext.font = this.fontString(this.tickMarks.values.font);
            this.mainVasCanvasContext.textAlign = 'left';
            this.mainVasCanvasContext.textBaseline = 'middle';

            this.mainVasCanvasContext.fillText(this.anchors.max.value, this.theCenterX + this.tickMarks.majorSize,
                this.customProperties.topBottomMargin);

            this.mainVasCanvasContext.fillText(this.anchors.min.value, this.theCenterX + this.tickMarks.majorSize,
                (this.tickFreq * this.anchors.max.value) + this.customProperties.topBottomMargin);
        }

        this.mainVasCanvasContext.closePath();

        this.mainVasCanvasContext.lineWidth = this.tickMarks.thickness;

        this.mainVasCanvasContext.beginPath();
        this.mainVasCanvasContext.moveTo(this.theCenterX - (this.tickMarks.majorSize / 2), this.customProperties.topBottomMargin);
        this.mainVasCanvasContext.lineTo(this.theCenterX + (this.tickMarks.majorSize / 2), this.customProperties.topBottomMargin);
        this.mainVasCanvasContext.stroke();
        this.mainVasCanvasContext.closePath();

        this.mainVasCanvasContext.beginPath();
        this.mainVasCanvasContext.moveTo(this.theCenterX - (this.tickMarks.majorSize / 2),
            (this.tickFreq * this.anchors.max.value) + this.customProperties.topBottomMargin);
        this.mainVasCanvasContext.lineTo(this.theCenterX + (this.tickMarks.majorSize / 2),
            (this.tickFreq * this.anchors.max.value) + this.customProperties.topBottomMargin);
        this.mainVasCanvasContext.stroke();
        this.mainVasCanvasContext.closePath();

        if (this.tickMarks.isVisible) {
            for (let i = this.anchors.max.value - 1; i >= this.anchors.min.value + 1; i--) {
                if (i % this.tickMarks.frequency === 0) {
                    this.mainVasCanvasContext.beginPath();
                    this.mainVasCanvasContext.moveTo(this.theCenterX - (this.tickMarks.majorSize / 2),
                        (this.tickFreq * i) + this.customProperties.topBottomMargin);
                    this.mainVasCanvasContext.lineTo(this.theCenterX + (this.tickMarks.majorSize / 2),
                        (this.tickFreq * i) + this.customProperties.topBottomMargin);

                    this.mainVasCanvasContext.stroke();
                    this.mainVasCanvasContext.closePath();
                } else {
                    if (this.tickMarks.displayMinorTicks) {
                        this.mainVasCanvasContext.beginPath();
                        this.mainVasCanvasContext.moveTo(this.theCenterX - (this.tickMarks.minorSize / 2),
                            (this.tickFreq * i) + this.customProperties.topBottomMargin);
                        this.mainVasCanvasContext.lineTo(this.theCenterX + (this.tickMarks.minorSize / 2),
                            (this.tickFreq * i) + this.customProperties.topBottomMargin);

                        this.mainVasCanvasContext.stroke();
                        this.mainVasCanvasContext.closePath();
                    }
                }

                if (i % this.tickMarks.values.frequency === 0) {
                    let tickValue = Math.abs(this.anchors.max.value - i);

                    if (this.customProperties.tickValuePosition === 'left') {
                        this.mainVasCanvasContext.beginPath();
                        this.mainVasCanvasContext.textAlign = 'right';
                        this.mainVasCanvasContext.textBaseline = 'middle';
                        this.mainVasCanvasContext.font = this.fontString(this.tickMarks.values.font);
                        this.mainVasCanvasContext.fillText(tickValue.toString(),
                            this.theCenterX - this.tickMarks.majorSize,
                            (this.tickFreq * i) + this.customProperties.topBottomMargin);
                        this.mainVasCanvasContext.closePath();
                    } else {
                        this.mainVasCanvasContext.beginPath();
                        this.mainVasCanvasContext.textAlign = 'left';
                        this.mainVasCanvasContext.textBaseline = 'middle';
                        this.mainVasCanvasContext.font = this.fontString(this.tickMarks.values.font);
                        this.mainVasCanvasContext.fillText(tickValue.toString(),
                            this.theCenterX + this.tickMarks.majorSize,
                            (this.tickFreq * i) + this.customProperties.topBottomMargin);
                        this.mainVasCanvasContext.closePath();
                    }
                }

                if (parseInt(i, 10) === 0) {
                    this.firstOne = (this.tickFreq * i) + this.customProperties.topBottomMargin;
                }

                if (parseInt(i, 10) === parseInt(this.anchors.max.value, 10)) {
                    this.lastOne = (this.tickFreq * i) + this.customProperties.topBottomMargin;
                }
            }
        }

        this.mainVasCanvasContext.beginPath();
        this.mainVasCanvasContext.lineWidth = this.tickMarks.thickness;
        this.mainVasCanvasContext.moveTo(this.theCenterX, this.firstOne);
        this.mainVasCanvasContext.lineTo(this.theCenterX, this.lastOne);
        this.mainVasCanvasContext.stroke();
        this.mainVasCanvasContext.closePath();

        this.vasScaleVerticalCenter = (this.$topper.height() + (this.$middle.height() / 2)) - this.$logPadInstructionTextDIV.outerHeight();
    }

}

LF.Widget.EQ5D5LPDECustom = EQ5D5LPDECustom;
