import './widget';

import {mergeObjects} from 'core/utilities/languageExtensions';
import EQ5D_5L_Diary_Config from './diary/EQ5D_5L';

let studyDesign = {}; //object
studyDesign = mergeObjects (studyDesign, EQ5D_5L_Diary_Config);

export default {studyDesign};
