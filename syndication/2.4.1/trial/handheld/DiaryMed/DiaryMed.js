import baseMedLoop from 'trial/handheld/medModule/assets/baseMedLoop';

let questionnaireConfig = {
    questionnaires: [
        {
            id: 'DiaryMed',
            SU: 'DiaryMed',
            displayName: 'DISPLAY_NAME',
            className: 'MedicationDiary',
            affidavit: 'DEFAULT',
            previousScreen: true,
            screens: [
                'MED020',
                'MEDINS_040',
                'MEDINS_050',
                'MED040'
            ],
            branches: [
                {
                    branchFrom: 'MED020',
                    branchTo: 'AFFIDAVIT',
                    branchFunction: 'MED020',
                    branchParams: {}
                },
                {
                    branchFrom: 'MEDLOOP_0100',
                    branchTo: 'MEDLOOP_0100',
                    branchFunction: 'medEpisodicExitMedLoop',
                    branchParams: {}
                },
                {
                    branchFrom: 'MEDLOOP_1000',
                    branchTo: 'MEDLOOP_1150',
                    branchFunction: 'always',
                    branchParams: {}
                },
                {
                    branchFrom: 'MED040',
                    branchTo: 'MED040',
                    branchFunction: 'MED040',
                    branchParams: {}
                }
            ],
            initialScreen: []
        }
    ],
    screens: [
        {
            id: 'MED020',
            className: 'MED020',
            questions: [
                {
                    id: 'MED020_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MEDINS_040',
            className: 'MEDINS_040',
            questions: [
                {
                    id: 'MEDINS_040_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MEDINS_050',
            className: 'MEDINS_050',
            questions: [
                {
                    id: 'MEDINS_050_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MEDLOOP_1150',
            className: 'MEDLOOP_1150',
            questions: [
                {
                    id: 'MEDLOOP_1150_Q',
                    mandatory: true
                }
            ]
        },
        {
            id: 'MED040',
            className: 'MED040',
            questions: [
                {
                    id: 'MED040_Q',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'MED020_Q',
            IG: 'DiaryMed',
            IT: 'MEDANY1L',
            text: [
                'MED020_Q_TEXT'
            ],
            className: 'MED020_Q',
            widget: {
                id: 'MED020_Q_W',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    {
                        text: 'MED020_A1',
                        value: '1'
                    }, {
                        text: 'MED020_A2',
                        value: '0'
                    }
                ]
            }
        },
        {
            id: 'MEDINS_050_Q',
            IG: 'DiaryMed',
            text: [
                'MEDINS_050_Q_TEXT'
            ],
            className: 'MEDINS_050_Q'
        },
        {
            id: 'MEDLOOP_1150_Q',
            IG: 'DiaryMedRepeating',
            IT: 'MEDREA1L',
            text: [
                'MEDLOOP_1150_Q_TEXT'
            ],
            className: 'MEDLOOP_1150_Q',
            widget: {
                id: 'MEDLOOP_1150_Q_W',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    {
                        text: 'MEDLOOP_1150_A1',
                        value: '0'
                    }, {
                        text: 'MEDLOOP_1150_A2',
                        value: '1'
                    }
                ]
            }
        },
        {
            id: 'MED040_Q',
            IG: 'DiaryMed',
            IT: 'MEDCNF1L',
            text: [
                'MED040_Q_TEXT'
            ],
            className: 'MED040_Q',
            widget: {
                id: 'MED040_Q_W',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    {
                        text: 'MED040_A1',
                        value: '1'
                    }, {
                        text: 'MED040_A2',
                        value: '2'
                    }
                ]
            }
        },
        {
            id: 'MEDINS_040_Q',
            IG: 'DiaryMed',
            text: [
                'MEDINS_040_Q_TEXT'
            ],
            className: 'MEDINS_040_Q'
        }
    ]
};

// [MedModule] Add BaseMedLoop
// Add BaseMedLoop screens after MEDINS_050
let MedLoopStartIndex = questionnaireConfig.questionnaires[0].screens.indexOf('MEDINS_050');
questionnaireConfig.questionnaires[0].screens.splice(MedLoopStartIndex + 1, 0, ...baseMedLoop.screenIDs);

// Add MEDLOOP_1150 after MEDLOOP_1100
let MEDLOOP_1200Index = questionnaireConfig.questionnaires[0].screens.indexOf('MEDLOOP_1100');
questionnaireConfig.questionnaires[0].screens.splice(MEDLOOP_1200Index + 1, 0, 'MEDLOOP_1150');

questionnaireConfig.screens = questionnaireConfig.screens.concat(baseMedLoop.screens);
questionnaireConfig.questions = questionnaireConfig.questions.concat(baseMedLoop.questions);
questionnaireConfig.questionnaires[0].branches = questionnaireConfig.questionnaires[0].branches.concat(baseMedLoop.branches);

export default questionnaireConfig;
