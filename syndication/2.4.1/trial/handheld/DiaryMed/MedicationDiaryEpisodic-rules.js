import Logger from 'core/Logger';

(function (rules) {
    let questionnaireID = 'MedicationDiaryEpisodic';

    rules.add({
        id: `open${questionnaireID}`,
        trigger: [
            `QUESTIONNAIRE:Open/${questionnaireID}`
        ],
        evaluate: function (input, resume) {
            resume(!localStorage.getItem('ScreenshotMode'));
        },
        resolve: [
            {
                action (input, done) {
                    let medLoopInfo = PDE.medModule.MedLoopInfo;

                    medLoopInfo.Max_Loops = 10;

                    done();
                }
            }
        ]
    });

    rules.add({
        id: `completed${questionnaireID}`,
        trigger: [
            `QUESTIONNAIRE:Completed/${questionnaireID}`
        ],
        evaluate: function (input, resume) {
            resume(!localStorage.getItem('ScreenshotMode'));
        },
        resolve: [
            {
                action (input, done) {
                    //TODO
                    done();
                }
            }
        ]
    });

}(ELF.rules));
