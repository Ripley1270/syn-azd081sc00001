import PDE from 'PDE_Core/common/PDE';

LF.Branching.branchFunctions.MED020 = (branchObj, view) => {
    let MED020 = PDE.AnswerUtils.getAnswerResponseByQuestionID('MED020_Q');

    if (MED020 === '0') {
        branchObj.branchTo = 'AFFIDAVIT';
        PDE.LoopingUtils.deleteAllLoops(PDE.medModule.MedLoopInfo.IG);
    } else {
        branchObj.branchTo = 'MEDINS_040';
    }

    return Q.resolve(true);
};

LF.Branching.branchFunctions.medEpisodicExitMedLoop = (branchObj, questionnaireView) => {
    let medCode = PDE.medModule.getMedCode(1);

    // if there is at least one med is set
    if (medCode) {
        branchObj.branchTo = 'AFFIDAVIT';
    } else {
        branchObj.branchTo = 'MED040';
    }

    return Q.resolve(true);
};

LF.Branching.branchFunctions.MED040 = (branchObj, view) => {
    let MED040 = PDE.AnswerUtils.getAnswerResponseByQuestionID('MED040_Q');

    if (MED040 === '1') {
        branchObj.branchTo = 'MEDLOOP_0100';
        return LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view);
    }
    branchObj.branchTo = 'AFFIDAVIT';
    return Q.resolve(true);
};
