import { mergeObjects } from 'core/utilities/languageExtensions';

// Rules import from IIFE
import './DiaryMed-rules';
import './DiaryMedBranching';

import diary from './DiaryMed';

let studyDesign = {}; // object
studyDesign = mergeObjects(studyDesign, diary);

export default { studyDesign };
