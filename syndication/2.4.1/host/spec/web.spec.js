const mongoose = require('mongoose');
const httpProxy = require('http-proxy');
const fs = require('fs');
const Q = require('q');
const mockery = require('mockery');

// We're assigning these values later, prior to each test, so
// mockery can replace the require('@ert/logger') statements.
// See spec/helpers/mock-logger.js
let Web;
let Logger;

describe('Web', () => {
    beforeEach(() => {
        Web = require('../business/web');
        Logger = require('@ert/logger');
    });

    describe('method:stats', () => {
        it('should have a stats method.', () => {
            expect(Web.stats).toBeDefined();
        });

        it('should return the system stats.', () => {
            let stats = Web.stats();

            expect(stats.proxyAttempts).toBe(0);
            expect(stats.failedProxyRequests).toBe(0);
        });
    });

    // eslint-disable-next-line
    TRACE_MATRIX('US7837')
    .describe('method:validateUnlockCode', () => {
        let req,
            res;

        Async.beforeEach(() => {
            let proxySpy = jasmine.createSpyObj('proxySpy', ['web', 'on']);

            proxySpy.on.and.returnValue(proxySpy);
            spyOn(httpProxy, 'createProxyServer').and.callFake(() => {
                return proxySpy;
            });

            spyOn(mongoose, 'connect').and.stub();
            spyOn(mongoose.connection, 'once')
                .and.callFake((name, cb) => cb());

            req = jasmine.createSpyObj('req', ['get']);
            req.body = {};
            req.url = 'study-01/ust/api/v1/proxy/sites';
            res = jasmine.createSpyObj('res', ['status', 'send']);
            res.status.and.returnValue(res);

            mockery.registerMock('public\\study-01\\staging\\node\\environments.js', {
                environments: [{ id: 'staging', studyDbName: 'Syn24_10May2017' }]
            });

            return Web.start({ protocol: 'http', publicPath: 'public' });
        });

        Async.it('should respond with 401 Unauthorized', () => {
            Web.verifyUnlockCode(req, res);

            return Q()
            .then(() => {
                expect(res.status).toHaveBeenCalledWith(401);
                expect(res.send).toHaveBeenCalledWith('Unauthorized');
            });
        });

        Async.it('should respond with 400, "Missing argument: siteCode"', () => {
            req.get.and.callFake(() => 'study-01:ust');

            Web.verifyUnlockCode(req, res);

            return Q()
            .then(() => {
                expect(res.status).toHaveBeenCalledWith(400);
                expect(res.send).toHaveBeenCalledWith('Missing argument: siteCode');
            });
        });

        Async.it('should respond with 400 "Missing argument: unlockCode"', () => {
            req.body.siteCode = '0001';
            req.get.and.callFake(() => 'study-01:ust');

            Web.verifyUnlockCode(req, res);

            return Q()
            .then(() => {
                expect(res.status).toHaveBeenCalledWith(400);
                expect(res.send).toHaveBeenCalledWith('Missing argument: unlockCode');
            });
        });

        Async.xit('should response with false (incorrect code).', () => {
            req.body.siteCode = '0001';
            req.body.unlockCode = '1231231';
            req.get.and.callFake(() => 'study-01:staging');

            let stat = { isDirectory: () => true };
            spyOn(fs, 'stat').and.callFake((path, cb) => cb(null, stat));

            Web.verifyUnlockCode(req, res);

            return Q.delay()
            .then(() => {
                expect(res.send).toHaveBeenCalledWith(false);
            });
        });

        Async.xit('should responsd with true (valid code)', () => {
            req.body.siteCode = '0001';
            req.body.unlockCode = '3287334792';
            req.get.and.callFake(() => 'study-01:staging');

            let stat = { isDirectory: () => true };
            spyOn(fs, 'stat').and.callFake((path, cb) => cb(null, stat));

            Web.verifyUnlockCode(req, res);

            return Q.delay()
            .then(() => {
                expect(res.send).toHaveBeenCalledWith(true);
            });
        });
    });


    TRACE_MATRIX('US7734')
    .describe('method: checkLockout', () => {
        let req,
            res;

        beforeEach(() => {
            req = jasmine.createSpyObj('req', ['get']);
            req.query = {};
            req.url = 'study-01/staging/api/v1/proxy/sites';
            res = jasmine.createSpyObj('res', ['status', 'send']);
            res.status.and.returnValue(res);
        });

        Async.it('should respond with 401 Unauthorized', () => {
            Web.checkLockout(req, res);

            return Q()
            .then(() => {
                expect(res.status).toHaveBeenCalledWith(401);
                expect(res.send).toHaveBeenCalledWith('Unauthorized');
            });
        });

        Async.it('should respond with 400, "Missing argument: siteCode"', () => {
            req.get.and.callFake(() => 'ecoa-study:staging');

            Web.checkLockout(req, res);

            return Q()
            .then(() => {
                expect(res.status).toHaveBeenCalledWith(400);
                expect(res.send).toHaveBeenCalledWith('Missing argument: siteCode');
            });
        });

        Async.it('should respond with 400 "Missing argument: userName"', () => {
            req.query.siteCode = '0001';
            req.get.and.callFake(() => 'ecoa-study:staging');

            Web.checkLockout(req, res);

            return Q()
            .then(() => {
                expect(res.status).toHaveBeenCalledWith(400);
                expect(res.send).toHaveBeenCalledWith('Missing argument: userName');
            });
        });

        Async.it('should respond with 400 "Missing argument: role"', () => {
            req.query.siteCode = '0001';
            req.query.username = 'aa';
            req.get.and.callFake(() => 'ecoa-study:staging');

            Web.checkLockout(req, res);

            return Q()
            .then(() => {
                expect(res.status).toHaveBeenCalledWith(400);
                expect(res.send).toHaveBeenCalledWith('Missing argument: role');
            });
        });
    });


    TRACE_MATRIX('US7734')
    .describe('method: setLockout', () => {
        let req,
            res;

        beforeEach(() => {
            req = jasmine.createSpyObj('req', ['get']);
            req.body = {};
            req.url = 'study-01/staging/api/v1/proxy/sites';
            res = jasmine.createSpyObj('res', ['status', 'send']);
            res.status.and.returnValue(res);
        });

        Async.it('should respond with 401 Unauthorized', () => {
            Web.setLockout(req, res);

            return Q()
            .then(() => {
                expect(res.status).toHaveBeenCalledWith(401);
                expect(res.send).toHaveBeenCalledWith('Unauthorized');
            });
        });

        Async.it('should respond with 400, "Missing argument: siteCode"', () => {
            req.get.and.callFake(() => 'ecoa-study:staging');

            Web.setLockout(req, res);

            return Q()
            .then(() => {
                expect(res.status).toHaveBeenCalledWith(400);
                expect(res.send).toHaveBeenCalledWith('Missing argument: siteCode');
            });
        });

        Async.it('should respond with 400 "Missing argument: userName"', () => {
            req.body.siteCode = '0001';
            req.get.and.callFake(() => 'ecoa-study:staging');

            Web.setLockout(req, res);

            return Q()
            .then(() => {
                expect(res.status).toHaveBeenCalledWith(400);
                expect(res.send).toHaveBeenCalledWith('Missing argument: username');
            });
        });

        Async.it('should respond with 400 "Missing argument: lockoutTime"', () => {
            req.body.siteCode = '0001';
            req.get.and.callFake(() => 'ecoa-study:staging');
            req.body.username = 'aa';

            Web.setLockout(req, res);

            return Q()
            .then(() => {
                expect(res.status).toHaveBeenCalledWith(400);
                expect(res.send).toHaveBeenCalledWith('Missing argument: lockoutTime');
            });
        });

        Async.it('should respond with 400 "Missing argument: role"', () => {
            req.body.siteCode = '0001';
            req.body.username = 'aa';
            req.body.lockoutTime = 5;
            req.get.and.callFake(() => 'ecoa-study:staging');

            Web.setLockout(req, res);

            return Q()
            .then(() => {
                expect(res.status).toHaveBeenCalledWith(400);
                expect(res.send).toHaveBeenCalledWith('Missing argument: role');
            });
        });
    });


    // eslint-disable-next-line
    TRACE_MATRIX('US7783').
    describe('method:start', () => {
        let opts,
            proxySpy;

        beforeEach(() => {
            opts = {
                protocol: 'http',
                serviceConfiguration: {
                    user: 'admin',
                    pwd: '12345',
                    dbHosts: 'localhost:27017'
                }
            };

            spyOn(Logger.prototype, 'operational').and.stub();
            proxySpy = jasmine.createSpyObj('proxySpy', ['on', 'web']);
            spyOn(httpProxy, 'createProxyServer').and.callFake(() => {
                return proxySpy;
            });
            spyOn(mongoose, 'connect').and.stub();
            spyOn(mongoose.connection, 'once')
                .and.callFake((name, cb) => cb());
        });

        Async.it('should start the proxy server w/changeOrigin enabled.', () => {
            return Web.start(opts)
            .then(() => {
                expect(httpProxy.createProxyServer).toHaveBeenCalledWith({
                    changeOrigin: true,
                    ignorePath: true,
                    proxyTimeout: 60000
                });
            });
        });

        Async.it('should start the proxy server w/changeOrigin disabled.', () => {
            opts.protocol = 'https';

            return Web.start(opts)
            .then(() => {
                expect(httpProxy.createProxyServer).toHaveBeenCalledWith({
                    changeOrigin: false,
                    ignorePath: true,
                    proxyTimeout: 60000
                });

                expect(Logger.prototype.operational).toHaveBeenCalledWith('Connected to MongoDB instance (localhost:27017/Syndication)');
            });
        });

        Async.it('should connect to the MongoDB database with the correct authentication string.', () => {
            let url = 'mongodb://admin:12345@localhost:27017/Syndication';

            return Web.start(opts)
            .then(() => {
                expect(mongoose.connect).toHaveBeenCalledWith(url, {
                    useMongoClient: true,
                    connectTimeoutMS: 30 * 1000
                });
            });
        });

        Async.it('should connect to the MongoDB database w/o an authentication string.', () => {
            let url = 'mongodb://localhost:27017/Syndication';

            delete opts.serviceConfiguration.user;

            return Web.start(opts)
            .then(() => {
                expect(mongoose.connect).toHaveBeenCalledWith(url, jasmine.any(Object));
            });
        });

        Async.it('should connect to the MongoDB database w/ Replica Set.', () => {
            let url = 'mongodb://admin:12345@localhost:27017/Syndication?replicaSet=ReplicaStub';
            opts.serviceConfiguration.replicaSet = 'ReplicaStub';

            return Web.start(opts)
            .then(() => {
                expect(mongoose.connect).toHaveBeenCalledWith(url, {
                    useMongoClient: true,
                    connectTimeoutMS: 30 * 1000,
                    replset: { auto_reconnect: false }
                });
            });
        });

        Async.it('should fail to connect to the MongoDB database.', () => {
            // Need to snub out connection.once so the method isn't resolved.
            mongoose.connection.once.and.stub();
            spyOn(mongoose.connection, 'on').and.callFake((name, cb) => {
                cb('Error');
            });
            spyOn(Logger.prototype, 'error').and.stub();

            return Web.start(opts)
            .catch(() => {
                expect(Logger.prototype.error).toHaveBeenCalled('connection error:', 'Error');
            });
        });
    });

    describe('middleware', () => {
        let proxySpy;

        Async.beforeEach(() => {
            proxySpy = jasmine.createSpyObj('proxySpy', ['web', 'on']);
            proxySpy.on.and.returnValue(proxySpy);
            spyOn(httpProxy, 'createProxyServer').and.callFake(() => {
                return proxySpy;
            });

            spyOn(mongoose, 'connect').and.stub();
            spyOn(mongoose.connection, 'once')
                .and.callFake((name, cb) => cb());

            return Web.start({ protocol: 'http', publicPath: 'public' });
        });

        // eslint-disable-next-line
        TRACE_MATRIX('US7782').
        describe('method:proxyMiddleware', () => {
            let middleware,
                req,
                res;

            beforeEach(() => {
                middleware = Web.proxyMiddleware(/\/proxy\/.*/, 'public');
                req = jasmine.createSpyObj('req', ['get']);
                req.url = 'study-01/ust/api/v1/proxy/sites';
                res = jasmine.createSpyObj('res', ['status', 'send']);
                res.status.and.returnValue(res);

                mockery.registerMock('public\\study-01\\ust\\node\\environments.js', {
                    environments: [{ id: 'staging' }]
                });

                mockery.registerMock('public\\study-01\\staging\\node\\environments.js', {
                    environments: [{
                        id: 'staging',
                        url: 'https://pht-netpro-01.com'
                    }]
                });
            });

            it('should do nothing.', () => {
                let next = jasmine.createSpy('next');
                req.url = 'study-01/ust/api/v1/ping';

                middleware(req, res, next);

                expect(next).toHaveBeenCalled();
            });

            Async.it('should response with HTTP Status Code 401 Unauthorized.', () => {
                middleware(req, res);

                return Q().then(() => {
                    expect(res.status).toHaveBeenCalledWith(401);
                    expect(res.send).toHaveBeenCalledWith('Unauthorized');
                });
            });

            Async.it('should fail to find the environment configuration.', () => {
                let next = jasmine.createSpy('next');
                req.get.and.callFake(() => 'study-01:ust');

                spyOn(fs, 'stat').and.callFake((path, cb) => {
                    cb(true);
                });

                middleware(req, res, next);

                return Q.delay().then(() => {
                    expect(next).not.toHaveBeenCalled();
                    expect(res.status).toHaveBeenCalledWith(500);
                    expect(res.send).toHaveBeenCalledWith('Missing directory public\\study-01\\ust\\node');
                });
            });

            Async.it('should fail to find the correct environment.', () => {
                let stat = { isDirectory: () => true };
                spyOn(fs, 'stat').and.callFake((path, cb) => cb(null, stat));
                req.get.and.callFake(() => 'study-01:ust');

                middleware(req, res);

                return Q.delay().then(() => {
                    expect(res.status).toHaveBeenCalledWith(500);
                    expect(res.send).toHaveBeenCalledWith('Environment ust not configured.');
                });
            });

            Async.it('should proxy the request.', () => {
                let stat = { isDirectory: () => true };
                req.url = 'study-01/staging/api/v1/proxy/api/v1/sites';
                spyOn(fs, 'stat').and.callFake((path, cb) => cb(null, stat));
                req.get.and.callFake(() => 'study-01:staging');

                middleware(req, res);

                return Q.delay().then(() => {
                    expect(proxySpy.web).toHaveBeenCalledWith(jasmine.any(Object), jasmine.any(Object), {
                        target: 'https://pht-netpro-01.com/api/v1/sites'
                    });
                });
            });
        });

        // eslint-disable-next-line
        TRACE_MATRIX('US7852')
        .TRACE_MATRIX('DE21324')
        .describe('method:registerMiddleware', () => {
            let middleware,
                req,
                res;

            beforeEach(() => {
                middleware = Web.registerMiddleware(/\/register/);

                req = jasmine.createSpyObj('req', ['get', 'on']);
                req.url = 'api/v1/register';

                res = jasmine.createSpyObj('res', ['status', 'send']);
                res.status.and.returnValue(res);

                mockery.registerMock('public\\study-01\\staging\\node\\environments.js', {
                    environments: [{
                        id: 'staging',
                        url: 'https://pht-netpro-01.com'
                    }]
                });
            });

            it('should do nothing.', () => {
                let next = jasmine.createSpy('next');
                req.url = 'study-01/ust/api/v1/ping';

                middleware(req, res, next);

                expect(next).toHaveBeenCalled();
            });

            Async.it('should response with HTTP Status Code 401 Unauthorized.', () => {
                middleware(req, res);

                return Q().then(() => {
                    expect(res.status).toHaveBeenCalledWith(401);
                    expect(res.send).toHaveBeenCalledWith('Unauthorized');
                });
            });

            Async.it('should fail to find the environment configuration.', () => {
                req.get.and.callFake(() => 'study-01:ust');
                req.on.and.callFake((name, callback) => {
                    if (name === 'end') {
                        callback();
                    } else if (name === 'data') {
                        let data = JSON.stringify({
                            siteCode: '001',
                            studyName: 'ecoaStudy01'
                        });

                        callback(new Buffer(data));
                    }
                });

                spyOn(fs, 'stat').and.callFake((path, cb) => {
                    cb(true);
                });

                middleware(req, res);

                return Q.delay().then().then(() => {
                    expect(res.status).toHaveBeenCalledWith(500);
                    expect(res.send).toHaveBeenCalledWith('Missing directory public\\study-01\\ust\\node');
                });
            });

            Async.it('should fail to find the correct environment.', () => {
                let stat = { isDirectory: () => true };

                spyOn(fs, 'stat').and.callFake((path, cb) => cb(null, stat));
                req.get.and.callFake(() => 'study-01:ust');
                req.on.and.callFake((name, callback) => {
                    if (name === 'end') {
                        callback();
                    } else if (name === 'data') {
                        let data = JSON.stringify({
                            siteCode: '001',
                            studyName: 'ecoaStudy01'
                        });

                        callback(new Buffer(data));
                    }
                });

                middleware(req, res);

                return Q().delay()
                .then(() => {
                    expect(res.status).toHaveBeenCalledWith(500);
                    expect(res.send).toHaveBeenCalledWith('Environment ust not configured.');
                });
            });


            Async.it('should proxy the request', () => {
                let stat = { isDirectory: () => true };

                spyOn(fs, 'stat').and.callFake((path, cb) => cb(null, stat));
                req.get.and.callFake(() => 'study-01:staging');
                req.on.and.callFake((name, callback) => {
                    if (name === 'end') {
                        callback();
                    } else if (name === 'data') {
                        let data = JSON.stringify({
                            siteCode: '001',
                            studyName: 'ecoaStudy01'
                        });

                        callback(new Buffer(data));
                    }
                });

                middleware(req, res);

                return Q().delay()
                .then(() => {
                    expect(req.body.startupCode).toBe('3085176920');
                    expect(req.body.siteCode).toBe(undefined);
                    expect(proxySpy.web).toHaveBeenCalledWith(jasmine.any(Object), jasmine.any(Object), {
                        target: 'https://pht-netpro-01.com/api/v1/devices2'
                    });
                });
            });

            Async.it('should clear the require cache and proxy the correct request.', () => {
                let stat = { isDirectory: () => true };

                spyOn(fs, 'stat').and.callFake((path, cb) => cb(null, stat));
                req.get.and.callFake(() => 'study-01:staging');
                req.on.and.callFake((name, callback) => {
                    if (name === 'end') {
                        callback();
                    } else if (name === 'data') {
                        let data = JSON.stringify({
                            siteCode: '001',
                            studyName: 'ecoaStudy01'
                        });

                        callback(new Buffer(data));
                    }
                });

                middleware(req, res);

                return Q().delay()
                .then(() => {
                    expect(proxySpy.web).toHaveBeenCalledWith(jasmine.any(Object), jasmine.any(Object), {
                        target: 'https://pht-netpro-01.com/api/v1/devices2'
                    });

                    mockery.registerMock('public\\study-01\\staging\\node\\environments.js', {
                        environments: [{
                            id: 'staging',
                            url: 'https://pht-netpro-02.com'
                        }]
                    });

                    middleware(req, res);
                })
                .delay()
                .then(() => {
                    expect(proxySpy.web).toHaveBeenCalledWith(jasmine.any(Object), jasmine.any(Object), {
                        target: 'https://pht-netpro-02.com/api/v1/devices2'
                    });
                });
            });
        });
    });
});
