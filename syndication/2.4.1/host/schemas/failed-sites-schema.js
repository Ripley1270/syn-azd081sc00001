const mongoose = require('mongoose'),
    Logger = require('@ert/logger'),
    logger = new Logger('schemas:unlock-failed'),
    config = require('../business/unlock-code-config');

/**
 * Schema template for documents representing failed unlock attempts
 * @type {mongoose}
 */
const failedUnlockSchema = new mongoose.Schema({
    siteCode: {
        type: String,
        required: true
    },
    failedAttempts: {
        type: Number,
        default: 1
    },
    isLocked: {
        type: Boolean,
        default: false
    },
    lockExpires: {
        type: Number
    }
});

// 'this' reference points to global with arrow functions
// eslint-disable-next-line prefer-arrow-callback
const saveModel = function (model, callback) {
    return model.save((err, site) => {
        if (err) {
            return logger.error('An error occurred in model.reset', err);
        }

        logger.operational(`Site ${site.siteCode} successfully saved to database.`);
        return callback(site);
    });
};

/**
 * Removes a site from the failed sites collection
 * @param  {function} callback - calls resolve() when done
 * @returns {Object} null
 */
const removeSite = function (callback) {
    return this.remove((err, model) => {
        if (err) {
            return logger.error(err);
        }

        logger.operational(`Site ${model.siteCode} successfully removed from database.`);
        return callback(null);
    });
};

/**
 * Resets the properties to default.
 * This is called when the lockout time of a locked site expires and a
 * site then performs another unsuccessful unlock attempt
 * @param  {function} callback - calls resolve() when done
 * @returns {Object}  MongoDB document/model
 */
const reset = function (callback) {
    logger.operational(`Site ${this.siteCode} has been unlocked.`);

    this.failedAttempts = 1;
    this.isLocked = false;
    this.lockExpires = undefined;

    return saveModel(this, callback);
};

/**
 * Decides what to do with locked out site
 * @param  {boolean}  isValid  - valid/invalid unlock code entry
 * @param  {function} callback - calls resolve() when done
 * @returns {[type]}            [description]
 */
const handleLockedSite = function (isValid, callback) {
    const lockHasNotExpired = this.lockExpires > new Date().getTime();

    if (lockHasNotExpired) {
        return callback(this);
    }

    if (isValid) {
        return removeSite.call(this, callback);
    }

    return reset.call(this, callback);
};

/**
 * Locks out a site after config.maxUnlockAttempts is reached
 * @param  {function} callback - calls resolve() when done
 * @returns {Object}  MongoDB document/model
 */
const lockSite = function (callback) {
    logger.operational(`Site ${this.siteCode} has been locked out.`);

    this.isLocked = true;
    this.lockExpires = new Date().getTime() + (config.lockoutDuration * 60 * 1000);

    return saveModel(this, callback);
};

/**
 * Increments the failedAttempts property of the site
 * @param  {function} callback - calls resolve() when done
 * @returns {Object}  MongoDB document/model
 */
const incrementAttempts = function (callback) {
    this.failedAttempts += 1;

    if (config.maxAttempts === this.failedAttempts) {
        return lockSite.call(this, callback);
    }

    return saveModel(this, callback);
};

/**
 * Static method on the schema that updates, if exists,
 * or creates a new MongoDB document(model)
 * @param  {string} siteCode - the siteCode of the site
 * @param  {boolean} unlockCodeIsValid - result of unlock code validation
 * @param  {function} callback - calls resolve() when done
 */
failedUnlockSchema.statics.upsertCollection = function (siteCode, unlockCodeIsValid, callback) {
    const createModel = (siteCode, callback) => {
        return new this({ siteCode })
            .save((err, model) => {
                if (err) {
                    return logger.error(err);
                }

                return callback(model);
            });
    };
    const upsert = (model, isValid, callback) => {
        if (model.isLocked) {
            return handleLockedSite.call(model, isValid, callback);
        }

        if (isValid) {
            return removeSite.call(model, callback);
        }

        return incrementAttempts.call(model, callback);
    };

    this.find({ siteCode }, (err, sites) => {
        if (err) {
            return logger.error('An error occurred in schema.upsertCollection', err);
        }

        if (!sites.length) {
            return createModel(siteCode, callback);
        }

        return upsert(sites[0], unlockCodeIsValid, callback);
    });
};

/**
 * Static method on the schema that retrieves all locked out sites
 * @param  {function} callback - calls res.send
 * @returns {void}
 */
failedUnlockSchema.statics.getLockedOutSites = function (callback) {
    let lockedOutSites = [];

    this.find({ isLocked: true }, (err, sites) => {
        if (err) {
            return logger.error(err);
        }

        if (sites.length) {
            lockedOutSites = sites.map((site) => {
                return { siteCode: site.siteCode, lockExpires: site.lockExpires };
            });
        }

        return callback(lockedOutSites);
    });
};

module.exports = failedUnlockSchema;
