const Q = require('q'),
    _ = require('lodash'),
    path = require('path'),
    httpProxy = require('http-proxy'),
    hutils = require('@ert/hutils'),
    Logger = require('@ert/logger'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    logger = new Logger('business:web'),
    UnlockCode = require('./unlock-code'),
    userSchema = require('../schemas/user'),
    User = mongoose.model('User', userSchema),
    FailedSites = require('../models/failed-site'),
    UnlockCodeConfig = require('./unlock-code-config');

let serviceInfo,
    proxyServer;

mongoose.Promise = Q.Promise;

// A map of stats to expose via api/v1/ping.
let stats = {
    proxyAttempts: 0,
    failedProxyRequests: 0
};

// Check if a directory exists.
// returns {Q.Promise<boolean}
const dirExists = (path) => {
    return Q.Promise((resolve) => {
        fs.stat(path, (err, stats) => {
            if (err) {
                return resolve(false);
            }

            return resolve(stats.isDirectory());
        });
    });
};

const studyLockoutConfig = (token) => {
    if (UnlockCodeConfig.maxAttempts || UnlockCodeConfig.lockoutDuration) {
        return Q();
    }

    let [study, env] = token.split(':'),
        configPath = path.join(serviceInfo.publicPath, `${study}/${env}/node`);

    logger.trace('Study Directory:', study);
    logger.trace('Environment:', env);
    logger.trace('Study Design Path:', configPath);

    return dirExists(configPath)
    .then((exists) => {
        if (!exists) {
            return Q.reject(`Missing directory ${configPath}`);
        }

        // ESlint consistent-return
        return Q();
    })
    .then(() => {
        let config;

        try {
            config = require(path.join(configPath, 'unlock-code-config.js'));
        } catch (e) {
            logger.error(e);
            return Q.reject();
        }

        if (config != null) {
            logger.trace('Study design configuration found.');

            UnlockCodeConfig.maxAttempts = config.maxUnlockAttempts;
            UnlockCodeConfig.lockoutDuration = config.unlockCodeLockoutTime;

            return Q();
        }

        return Q.reject('Unable to locate study design configuration.');
    });
};

const getEnvironmentConfig = (token) => {
    let [study, env] = token.split(':'),
        nodePath = path.join(serviceInfo.publicPath, `${study}/${env}/node`);

    logger.trace('Study Directory:', study);
    logger.trace('Environment:', env);
    logger.trace('Node Path:', nodePath);

    return dirExists(nodePath)
    .then((exists) => {
        if (!exists) {
            return Q.reject(`Missing directory ${nodePath}`);
        }

        // ESlint consistent-return
        return Q();
    })
    .then(() => {
        let target = path.join(nodePath, 'environments.js');

        // DE21324 - Clear environment configuration from cache.
        if (require.cache[target]) {
            logger.trace(`Clearing require cache for module: ${target}`);

            delete require.cache[target];
        }

        return target;
    })
    .then((target) => {
        let config = require(target);

        if (config != null) {
            logger.trace('Environment configuration found.');

            let match = _.find(config.environments, (environment) => {
                return environment.id === env;
            });

            // If no match was found.
            if (match == null) {
                return Q.reject(`Environment ${env} not configured.`);
            }

            return match;
        }

        return Q.reject('Unable to locate environment configuration.');
    });
};

// The proxy middleware is run prior to BodyParser,
// so we need to parse out the body of request before it can
// be modified.
// See https://github.com/nodejitsu/node-http-proxy/issues/180
const parseBody = (message) => {
    let body = [];

    return new Q.Promise((resolve, reject) => {
        message.on('data', (data) => {
            body.push(data);
        });

        message.on('end', () => {
            resolve(body);
        });

        message.on('error', (err) => {
            reject(err);
        });
    });
};

/**
 * Web host business logic
 * @class Web
 */
class Web {
    /**
     * Start the API routes.
     * @param {Object} service - A reference to the host service configuration.
     * @returns {Q.Promise<Object>}
     */
    static start (service) {
        serviceInfo = service;

        // Start the proxy server.
        // NOTE: We may discover that we need to create a new server for each request.
        // I'm trying to avoid that if possible.
        proxyServer = httpProxy.createProxyServer({
            // In a development environment, this should be set to true
            // so requests can be proxied across protocols. e.g. http > https
            changeOrigin: service.protocol === 'http',

            // Prevents undesired path concat. e.g. api/v1/sites/pht-study-09.phtnetpro.com/api/va/sites
            ignorePath: true,

            // Timeout the proxy after 60 seconds.  If this isn't set, the request will never timeout...
            proxyTimeout: 60000

        }).on('proxyReq', (proxyReq, req) => {
            // Since we've parsed out the body of the request,
            // we need to rewrite it prior to forwarding the request.
            if (req.body) {
                let body = JSON.stringify(req.body);

                proxyReq.setHeader('Content-Type', 'application/json');

                // Need to ensure the Content-Length is correct, otherwise the receiving server will
                // throw an error.
                proxyReq.setHeader('Content-Length', Buffer.byteLength(body));

                proxyReq.write(body);
                proxyReq.end();
            }
        });

        // Attempt to connect to the Mongo database.
        let connect = () => {
            return Q.Promise((resolve, reject) => {
                let { user, pwd, dbHosts, replicaSet } = service.serviceConfiguration;
                let replset = replicaSet ? `?replicaSet=${replicaSet}` : '';
                let login = user ? `${user}:${pwd.replace(/@/g, '%40')}@` : '';

                let options = {
                    // This opts into MongoDB's new connection logic
                    useMongoClient: true,

                    // Timeout trying to connect to the database after 10 seconds.
                    connectTimeoutMS: 30 * 1000
                };

                if (replicaSet) {
                    options.replset = {
                        auto_reconnect: false
                    };
                }

                // Construct the MongoDB URL and attempt to connect.
                let url = `mongodb://${login}${dbHosts}/Syndication${replset}`;
                logger.info(`Attempting to connect to ${url}...`);
                mongoose.connect(url, options);

                let db = mongoose.connection;

                db.on('error', (err) => {
                    logger.error('connection error:', err);

                    reject(err);
                });

                // The database connection has been established.
                db.once('open', () => {
                    logger.operational(`Connected to MongoDB instance (${dbHosts}/Syndication${replset})`);

                    resolve();
                });
            });
        };

        return connect()
            .then(() => serviceInfo)
            .catch((err) => {
                logger.error(err);

                process.exit();
            });
    }

    /**
     * Returns the current service stats.
     * @returns {Object}
     */
    static stats () {
        return stats;
    }

    /**
     * Middleware to handle registration to NetPro/Expert.
     * @param {RegEx} pattern - A pattern to match against URLs.
     * @returns {function} middleware invoked by express routes
     */
    static registerMiddleware (pattern) {
        return (req, res, next) => {
            if (req.url.match(pattern)) {
                logger.trace('Incoming registration request...');
                logger.trace(`Request URL: ${req.url}`);

                // X-Web-API-Token is constructed by the client.
                // It takes the form of `${studyDirectory}:${environmentDirectory}`
                let token = req.get('X-Web-API-Token');

                let onError = (status, msg) => {
                    logger.error(msg);

                    res.status(status).send(msg);
                };

                if (token) {
                    let parse = () => {
                        return parseBody(req).then((body) => {
                            req.body = JSON.parse(body[0].toString('utf-8'));
                        });
                    };

                    let proxy = () => {
                        return getEnvironmentConfig(token)
                            .then((config) => {
                            // We need to generate the startup/unlock code, since the client can't for security reasons.
                                req.body.startupCode = UnlockCode.createStartupUnlockCode(req.body.siteCode, req.body.studyName);

                                // api/v1/devices2 doesn't take the siteCode property, so remove it.
                                delete req.body.siteCode;

                                let target = `${config.url}/api/v1/devices2`;
                                logger.trace(`ProxyURL: ${target}`);

                                return proxyServer.web(req, res, { target });
                            });
                    };

                    // Ensure the node path exists for the local directory.
                    parse()
                        .then(proxy)
                        .catch((err) => {
                            onError(500, err instanceof Error ? err.message : err);
                        })
                        .done();
                } else {
                    onError(401, 'Unauthorized');
                }
            } else {
                next();
            }
        };
    }

    /**
     * Verify a provided unlock code.
     * @param {Request} req - HTTP request
     * @param {Response} res - HTTP response
     */
    static verifyUnlockCode (req, res) {
        let { siteCode, unlockCode } = req.body,
            token = req.get('X-Web-API-Token'),
            onError = (status, msg) => {
                logger.error(msg);

                res.status(status).send(msg);
            };

        if (!token) {
            onError(401, 'Unauthorized');

            return;
        }

        if (siteCode == null) {
            onError(400, 'Missing argument: siteCode');

            return;
        }

        if (unlockCode == null) {
            onError(400, 'Missing argument: unlockCode');

            return;
        }

        studyLockoutConfig(token)
        .then(() => getEnvironmentConfig(token))
        .then((config) => {
            const validate = (unlockCode) => {
                let uCode = UnlockCode.createStartupUnlockCode(siteCode, config.studyDbName);

                return uCode === unlockCode;
            };

            // US7721 disabled until 2.5 release
            // const updateCollection = (siteCode, isValid) => {
            //     return Q.Promise((resolve) => {
            //         return FailedSites.upsertCollection(siteCode, isValid, resolve);
            //     });
            // };
            // const createResponse = (model) => {
            //     if (_.isNull(model)) {
            //         return true;
            //     }

            //     if (_.isObject(model)) {
            //         return model.isLocked ? { locked: true } : false;
            //     }

            //     return false;
            // };

            return Q()
            .then(() => validate(unlockCode))

            // US7721 disabled until 2.5 release
            // .then((isValid) => {
            //     return updateCollection(siteCode, isValid);
            // })
            // .then((model) => {
            //     return createResponse(model);
            // })
            .then((response) => {
                return res.send(response);
            })
            .catch((err) => {
                onError(500, err instanceof Error ? err.message : err);
            });
        })
        .catch((err) => {
            onError(500, err instanceof Error ? err.message : err);
        })
        .done();
    }

    /**
     * check if user provided is locked out of product
     * @param {Request} req - HTTP request
     * @param {Response} res - HTTP response
     */
    static checkLockout (req, res) {
        let { username, siteCode, role } = req.query,
            token = req.get('X-Web-API-Token'),
            onError = (status, msg) => {
                logger.error(msg);

                res.status(status).send(msg);
            };


        if (!token) {
            onError(401, 'Unauthorized');

            return;
        }

        if (siteCode == null) {
            onError(400, 'Missing argument: siteCode');

            return;
        }

        if (username == null) {
            onError(400, 'Missing argument: userName');

            return;
        }

        if (role == null) {
            onError(400, 'Missing argument: role');

            return;
        }

        User.find({ username, siteCode, role }).exec()
            .then((data) => {
                if (data.length) {
                    let u = data[0],
                        now = new Date().getTime(),
                        expireTime = u.timeStamp + (u.lockoutTime * 60000);
                    res.send(
                        { locked: !(now >= expireTime),
                            user: 1
                        });
                } else {
                    res.send({ locked: false, user: 0 });
                }
            })
            .catch((err) => {
                onError(500, err instanceof Error ? err.message : err);
            })
            .done();
    }

    /**
     * set the lockout values for a given user
     * @param {Request} req - HTTP request
     * @param {Response} res - HTTP response
     */
    static setLockout (req, res) {
        let { siteCode, username, lockoutTime, role } = req.body,
            token = req.get('X-Web-API-Token'),
            onError = (status, msg) => {
                logger.error(msg);

                res.status(status).send(msg);
            };

        if (!token) {
            onError(401, 'Unauthorized');

            return;
        }

        if (siteCode == null) {
            onError(400, 'Missing argument: siteCode');

            return;
        }

        if (username == null) {
            onError(400, 'Missing argument: username');

            return;
        }
        if (lockoutTime == null) {
            onError(400, 'Missing argument: lockoutTime');

            return;
        }
        if (role == null) {
            onError(400, 'Missing argument: role');

            return;
        }

        User.find({ username, siteCode, role }).exec()
            .then((data) => {
                if (data.length) {
                    data[0].timeStamp = new Date().getTime();
                    data[0].lockoutTime = lockoutTime;
                    return data[0].save()
                        .then(() => {
                            res.send(data[0]);
                        });
                }
                let nUser = new User({
                    username,
                    siteCode,
                    role,
                    timeStamp: new Date().getTime(),
                    lockoutTime
                });
                return nUser.save()
                    .then(() => {
                        res.send(nUser);
                    });
            })
            .catch((err) => {
                onError(500, err instanceof Error ? err.message : err);
            })
            .done();
    }

    /**
     * Proxy Middleware for api/v1/proxy routes.
     * Note: Due to some conflict, this needs to be called prior
     * ToDo: Determine how to handle sending client proxy errors.
     * to be called prior to the bodyParser middleware.
     * @param {RegExp} pattern - Regular Expression used to match URLs for proxy.
     * @returns {function} middleware invoked by express routes.
     */
    static proxyMiddleware (pattern) {
        // eslint-disable-next-line consistent-return
        return function (req, res, next) {
            if (req.url.match(pattern)) {
                logger.trace('Incoming proxy request...');
                logger.trace(`Request URL: ${req.url}`);

                // X-Web-API-Token is constructed by the client.
                // It takes the form of `${studyDirectory}:${environmentDirectory}`
                let token = req.get('X-Web-API-Token');

                let onError = (status, msg) => {
                    logger.error(msg);

                    res.status(status).send(msg);
                };

                // If there is no token, we need to
                if (!token) {
                    return onError(401, 'Unauthorized');
                }

                stats.proxyAttempts += 1;

                // Split the request URL at 'proxy/';
                let proxyPath = req.url.split('proxy/')[1];

                // eslint-disable-next-line consistent-return
                let proxy = (config) => {
                    let target = `${config.url}/${proxyPath}`;
                    logger.trace(`ProxyURL: ${target}`);

                    return proxyServer.web(req, res, { target });
                };

                getEnvironmentConfig(token)
                    .then(proxy)
                    .catch((err) => {
                        onError(500, err instanceof Error ? err.message : err);
                    })
                    .done();
            } else {
                next();
            }
        };
    }

    /**
     * Queries the collection and returns all locked out sites
     * @param {Request} req - HTTP request
     * @param {Response} res - HTTP response
     */
    static lockedSites (req, res) {
        FailedSites.getLockedOutSites((sites) => {
            res.send(sites);
        });
    }
}

// hutils takes a ping listener that can adjust the output, such as adding stats.
hutils.pingListener = (ping) => {
    ping.service = serviceInfo.name;
    ping.version = serviceInfo.version;
    ping.description = serviceInfo.description;
    ping.stats = stats;
};

module.exports = Web;
