
exports.handlers = {
    newDoclet: function(ev) {
        // modify tag
        var curType = ev.doclet.type;

            if (ev.doclet.kind === 'class' && ev.doclet.scope === 'global') {
                var indexesToRemove = [];
                // either a constructor or a class declaration.  If it has params, remove object and object.model
                for (var i = 0; ev.doclet.params && i < ev.doclet.params.length; ++i) {
                    var param = ev.doclet.params[i],
                        paramParts = param.name.split('.');

                    if (paramParts[0] === 'options') {
                        if (paramParts.length === 1) {
                            indexesToRemove.push(i);
                        } else if (paramParts[1] === 'model') {
                            paramParts.splice(0, 1);
                            paramParts[0] = 'widget';

                            if (paramParts.length > 1) {
                                if (param.description.indexOf('{dev-only}') === -1) {
                                    paramParts[1] = '<b>' + paramParts[1];
                                    paramParts[paramParts.length - 1] = paramParts[paramParts.length - 1] + '<sup>&nbsp;SD&nbsp;</sup></b>';
                                }
                                else {
                                    param.description = param.description.replace('{dev-only}', '');
                                }
                            }

                            param.name = paramParts.join('.');
                        }
                    }
                }

                for (var i = indexesToRemove.length - 1; i >= 0; --i) {
                    ev.doclet.params.splice(indexesToRemove[i], 1);
                }
            }
        }
    };