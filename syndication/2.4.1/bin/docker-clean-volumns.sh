#!/bin/bash
# Clear all docker volumes.
docker volume rm $(docker volume ls -qf dangling=true)