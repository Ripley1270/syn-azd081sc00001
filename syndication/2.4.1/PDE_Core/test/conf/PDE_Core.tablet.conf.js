'use strict';

// jscs:disable disallowKeywords
var manifest = require('../../../app/test/conf/manifest'),
    sharedConfig = require('../../../app/test/conf/shared.conf');

module.exports = function (config) {

    sharedConfig(config);

    config.set({

        // list of files / patterns to load in the browser
        // Order matters!
        files: manifest.mergeFilesFor('thirdparty', 'core', 'helpers', '~PDE_Core_tabletUnitTest'),

        // list of files to exclude
        exclude: manifest.mergeFilesFor('exclude'),

        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'core/templates/*.ejs': ['html2js'],
            '../PDE_Core/tablet/**/*.js': ['coverage'],
            '../PDE_Core/test/specs/tablet/**/*.js': ['browserify'],
            'test/helpers/*.js': ['browserify']
        },

        junitReporter: {
            outputDir: '../junit',
            outputFile: 'TEST-results-PDE_Core_tablet.xml',
            suite: 'PDE_Core.tablet',
            useBrowserName: false
        },

        coverageReporter: {
            type: 'cobertura',
            dir: '../coverage',
            file: 'PDE_Core.tablet-coverage.xml'
        }
    });
};
