import ArrayRef from 'core/classes/ArrayRef';

function buildArrayRefString (array) {
    let retStr = '[';
    for (let i = 0; i < array.length; i++) {
        retStr = retStr + array[i];
        if (i < array.length - 1) {
            retStr = `${retStr}, `;
        }
    }
    retStr = `${retStr}]`;
    return retStr;
}

export default {
    /**
     * custom jasmine matcher to check if item is an empty Array or Object.
     * @returns {Object} containing comparison
     */
    toBeEmpty () {
        return {
            compare (actual) {
                let result = {};

                if (actual instanceof ArrayRef) {
                    result.pass = actual.length === 0;
                    result.message = `Expected Array ${buildArrayRefString(actual)} to have no elements`;
                } else if (actual instanceof Array) {
                    result.pass = actual.length === 0;
                    result.message = `Expected Array ${actual} to have no elements`;
                } else if (typeof actual === 'object') {
                    result.pass = Object.keys(actual).length === 0;
                    result.message = `Expected Object ${actual} to have no properties`;
                } else {
                    result.pass = false;
                    result.message = `Expected ${actual} to be an Object or Array in order to check Empty status`;
                }
                return result;
            },
            negativeCompare (actual) {
                let result = {};

                if (actual instanceof ArrayRef) {
                    result.pass = actual.length !== 0;
                    result.message = `Expected Array ${buildArrayRefString(actual)} to have elements`;
                } else if (actual instanceof Array) {
                    result.pass = actual.length !== 0;
                    result.message = `Expected Array ${actual} to have elements`;
                } else if (typeof actual === 'object') {
                    result.pass = Object.keys(actual).length !== 0;
                    result.message = `Expected Object ${actual} to have properties`;
                } else {
                    result.pass = false;
                    result.message = `Expected ${actual} to be an Object or Array in order to check Empty status`;
                }
                return result;
            }
        };
    }
};
