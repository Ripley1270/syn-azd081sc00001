/* eslint-disable no-console */
import { getNested } from 'core/utilities/coreUtilities';
import ArrayRef from 'core/classes/ArrayRef';

const devServer = '10.101.41.52',
    stagingServer = '10.15.145.21';
let config,
    erroredConfig;


function checkIssue (error) {
    if (error.code === 'ELOGIN') {
        return `
        Failed to connect to the database, please verify these connection params and update if required:
        ${JSON.stringify(erroredConfig)}`;
    }
    return undefined;
}


function deepObjFindITVals (obj, retArray) {
    let objKeys = Object.keys(obj);
    _(objKeys).forEach((k) => {
        if (k === 'IT' && typeof obj[k] === 'string') {
            retArray = _.union(retArray, [obj[k]]);
        } else if (Array.isArray(obj[k]) || obj[k] instanceof ArrayRef) {
            let arrSearch = obj[k].reduce((retval, ele) => {
                if (ele === Object(ele) && typeof ele !== 'function') {
                    retval = _.union(retval, deepObjFindITVals(ele, retval));
                }
                return retval;
            }, []);
            retArray = _.union(retArray, arrSearch);
        } else if (obj[k] === Object(obj[k]) && typeof obj[k] !== 'function') {
            deepObjFindITVals(obj[k], retArray);
        }
    });
    return retArray;
}

export function validateITs (studyDesign, configs) {
    let initialITList = [];
    _(getNested('questions', studyDesign)).forEach((question) => {
        let IG = question.IG;
        if (IG) {
            let ITs = _(deepObjFindITVals(question, [])).without(undefined),
                alreadyFoundIG = _(initialITList).find(ele => ele.IG === IG);
            if (!alreadyFoundIG && ITs.length > 0) {
                initialITList.push({
                    IG,
                    ITs
                });
            } else if (ITs.length > 0) {
                alreadyFoundIG.ITs = _.union(alreadyFoundIG.ITs, ITs);
            }
        }
    });
    console.log(initialITList);
    if(configs.excludeIGs && Array.isArray(configs.excludeIGs)){
        _(configs.excludeIGs).forEach((exIG) => {
            let objToRemove = _(initialITList).find(obj => obj.IG === exIG);
            initialITList = _(initialITList).without(objToRemove);
        });
    }
    if (configs.excludeITs && Array.isArray(configs.excludeITs)) {
        _(configs.excludeITs).forEach((exITobj) => {
            let objToClean = _(initialITList).find(obj => obj.IG === exITobj.IG);
            if (objToClean) {
                objToClean.ITs = _(objToClean.ITs).without(...exITobj.ITs);
            }
        });
    }
    if (configs.includeITs && Array.isArray(configs.includeITs)) {
        _(configs.includeITs).forEach((inITobj) => {
            let objToClean = _(initialITList).find(obj => obj.IG === inITobj.IG);
            if (objToClean) {
                objToClean.ITs = _.union(objToClean.ITs, inITobj.ITs);
            } else {
                initialITList.push(inITobj);
            }
        });
    }

    // clean out any elements with no ITs after excludes/includes
    initialITList = _(initialITList).filter(ele => ele.ITs.length > 0);

    let generateITXML = (ITList) => {
            const makeITelement = (IT) => {
                    return `<IT name="${IT}" />`;
                },
                makeIGelement = (ITobj) => {
                    let ITelements = _(ITobj.ITs).map(IT => makeITelement(IT));
                    return `<IG name="${ITobj.IG}">${ITelements.join('')}</IG>`;
                };
            let tempList = _(ITList).map(ITobj => makeIGelement(ITobj));
            return `<ROOT>${tempList.join('')}</ROOT>`;
        },
        parseITXMLResonse = (responseData, resultsArray, doneCallback) => {
            let xmlDoc = $.parseXML(responseData);
            let root = $(xmlDoc).children();
            if (root.length) {
                let IGList = $(root).children();
                if (IGList.length) {
                    _(IGList).forEach((ig) => {
                        let resObj = {
                            IG: ig.attributes[0].value,
                            pass: ig.attributes[1].value,
                            ITs: []
                        };
                        if (resObj.pass === '1') {
                            let ITList = $(ig).children();
                            if (ITList.length) {
                                _(ITList).forEach((it) => {
                                    resObj.ITs.push({
                                        IT: it.attributes[0].value,
                                        pass: it.attributes[1].value
                                    });
                                });
                            } else {
                                console.log(`No IT elements for IG ${resObj.IG}`);
                            }
                        }
                        resultsArray.push(resObj);
                    });
                    doneCallback();
                } else {
                    console.log('no IG elements!');
                    doneCallback();
                }
            } else {
                console.log('no ROOT element!');
                doneCallback();
            }
        };

    let ITResults = [],
        originalInterval,
        error = false;
    beforeAll(() => {
        originalInterval = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;
    });
    beforeAll((done) => {
        let database = _(getNested('environments', studyDesign)).find({ id: configs.environment }).studyDbName;
        if (!database) {
            console.log(`Can\'t find database name from \'${configs.environment}\' environment config`);
            done();
        } else {
            let inputXML = generateITXML(initialITList);
           // console.log(inputXML);
            let server;
            switch (configs.environment) {
                case 'dev':
                    server = devServer;
                    break;
                case 'staging':
                default:
                    server = stagingServer;
            }
            config = {
                user: configs.user,
                password: configs.password,
                server,
                database
            };

            erroredConfig = {
                user: config.user,
                password: config.password,
                server: config.server,
                db: config.database
            };

            Q($.ajax({
                url: 'http://localhost:1337/IT',
                type: 'POST',
                data: JSON.stringify({
                    config,
                    inputXML
                })
            })).catch((jqXHR) => {
                console.error('ERROR', jqXHR);
                error = 'validateITs: Communication Failed';
                done();
            }).then((data) => {
                console.log('validateITs: gotResponse!', data);
                let regex = /^failure:/;
                if (regex.test(data) === false) {
                    parseITXMLResonse(data, ITResults, done);
                } else {
                    console.log('validateITs: Query Failed!');
                    console.error(data);
                    error = 'validateITs: Query Failed';
                    done();
                }
            });
        }
    });

    afterAll(() => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalInterval;
    });

    describe(`IT results from ${configs.environment}`, () => {
        it('should find a non-zero number of ITs to investigate', () => {
            expect(initialITList.length).toBeGreaterThan(0);
        });
        it('should not receive an error response from the query service', () => {
            expect(error).toBeFalsy(checkIssue(error));
        });

        function testResult (IGname, ITname) {
            it(`should find an IG.IT of ${IGname}.${ITname}`, () => {
                debugger;
                let IGresObj = _(ITResults).find(res => res.IG === IGname),
                    ITresObj = _(IGresObj.ITs).find(res => res.IT === ITname);
                expect(ITresObj.pass).toMatch('1');
            });
        }

        for (let i = 0; i < initialITList.length; i++) {
            describe(`looking within IG ${initialITList[i].IG}`, () => {
                it(`IG ${initialITList[i].IG} should exist`, () => {
                    expect(_(ITResults).find(res => res.IG === initialITList[i].IG).pass).toMatch('1');
                });
                for (let j = 0; j < initialITList[i].ITs.length; j++) {
                    testResult(initialITList[i].IG, initialITList[i].ITs[j]);
                }
            });
        }
    });
}

