import { getNested, isSitePad } from 'core/utilities/coreUtilities';
import Logger from 'core/Logger';

/**
 * always returns a promise that will always resolve true and execute the branch
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @returns {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.always = (branchObj, view) => {
    return Q(true);
};

// Added for backwards compatibility
/**
 * equal will compare a supplied value to a supplied question's response, and resolve true if the two match
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @returns {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.equal = (branchObj, view) => {
    branchObj.branchParams.operand = '=';
    return LF.Branching.branchFunctions.compare(branchObj, view);
};

/**
 * compare will compare a supplied value to a supplied question's response,
 * utilizing one of the following operands provided in branchParams.operand:
 * = , != , >= , <= , > , <
 * and resolve true if the two pass the comparison.
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @returns {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.compare = (branchObj, view) => {
    return Q.Promise((resolve) => {
        let response,
            questionId = branchObj.branchParams.questionId,
            operand = branchObj.branchParams.operand,
            questionIT = branchObj.branchParams.IT,
            result = false;

        if (branchObj.branchParams.IG) {
            // Get looping data
            let igr = view.getCurrentIGR(branchObj.branchParams.IG);

            if (questionId) {
                response = view.queryAnswersByQuestionIDAndIGR(questionId, igr)[0];
            } else if (questionIT) {
                response = view.queryAnswersByITAndIGR(questionIT, igr)[0];
            }
        } else {
            if (questionId) {
                response = view.queryAnswersByID(questionId)[0];
            } else if (questionIT) {
                response = view.queryAnswersByIT(questionIT)[0];
            }
        }

        if (response) {
            response = response.get('response');
        }

        switch (operand) {
            case '==':
            case '=': {
                result = response === branchObj.branchParams.value;
            }
                break;

            case '!=': {
                result = response !== branchObj.branchParams.value;
            }
                break;

            case '>=': {
                result = response != null && Number(response) >= Number(branchObj.branchParams.value);
            }
                break;

            case '<=': {
                result = response != null && Number(response) <= Number(branchObj.branchParams.value);
            }
                break;

            case '>': {
                result = response != null && Number(response) > Number(branchObj.branchParams.value);
            }
                break;

            case '<': {
                result = response != null && Number(response) < Number(branchObj.branchParams.value);
            }
                break;

            default: {
                // No operand or unknown operand
                result = false;
            }
                break;
        }

        resolve(result);
    });
};


/**
 * alwaysBranchBack will pop screens off the stack until it reaches the supplied branchTo
 * If branchObj.clearBranchedResponses is explicitly false, then the branchBack will maintain responses,
 * otherwise responses are cleared.
 * If branchObject.branchParams.simulatebackButton is true, the function will branch back only one screen in the stack,
 * as though the back button was pressed.
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @returns {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.alwaysBranchBack = (branchObj, view) => {
    return Q.Promise((resolve) => {
        if (branchObj.clearBranchedResponses !== false) {
            let screen = _.filter(view.data.screens, (screen) => {
                    return screen.id === branchObj.branchFrom;
                })[0],
                questions = _(screen.get('questions')).pluck('id');

            // QuestionnaireView does not clear the responses of the
            // 'branchFrom' screen's questions
            view.resetAnswersSelected(questions);
        }

        if (branchObj.branchParams && branchObj.branchParams.simulateBackButton) {
            view.screenStack.pop();

            // Just pop to the previous screen
            branchObj.branchTo = view.screenStack.pop();
        } else {
            // Clear the screens stacked above the 'branchTo' target
            let aBranch = view.screenStack.pop();
            while (aBranch !== branchObj.branchTo) {
                aBranch = view.screenStack.pop();
            }
        }

        resolve(true);
    });
};

// Added for backwards compatibility
/**
 * equalBranchBack calls compareBranchBack with an operand of '='
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @returns {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.equalBranchBack = (branchObj, view) => {
    branchObj.branchParams.operand = '=';
    return LF.Branching.branchFunctions.compareBranchBack(branchObj, view);
};

/**
 * compareBranchBack calls compare, then if compare resolves true, it calls alwaysBranchBack.
 * If compare resolves false, then compareBranchBack resolves false.
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @returns {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.compareBranchBack = (branchObj, view) => {
    return LF.Branching.branchFunctions.compare(branchObj, view)
        .then((result) => {
            if (result) {
                return LF.Branching.branchFunctions.alwaysBranchBack(branchObj, view);
            }
            return false;
        });
};

/**
 * exitDiary will execute always, or equal if the branchObj contains a branchParams.questionId
 * if the always or equal resolves as true, then it will exit the diary
 * If branchParams.promptBeforeExit == true, the backoutHandler will be called as though the back button was hit on the first screen
 * Else, diary will exit to dashboard without prompt.
 * @param {Object} branchObj contains the branching configuration
 * @param {QuestionnaireView} view current Questionnaire view
 * @returns {Promise<T>} returns a promise that resolves as true or false
 */
LF.Branching.branchFunctions.exitDiary = (branchObj, view) => {
    let branchFunction = LF.Branching.branchFunctions.always;

    // Check for conditional branching params.
    // Use getNested utility function to avoid checking if nested objects exist.
    if (getNested('branchParams.questionId', branchObj)) {
        branchFunction = LF.Branching.branchFunctions.equal;
    }

    // Execute branching function and exit diary if it returns true.
    // No need for creating a new promise chain with Q.Promise. The promise being created by
    // branchFunction is now being returned. It will decrease indentation and make it easier to read.
    return branchFunction(branchObj, view)
        .then((result) => {
            if (!result) {
                // Instead of calling resolve(false), just return a Q promise resolving with false.
                // The .then() blocks below don't always have to return a promise.  If we didn't
                // have a block below which is returning a promise, we could just return false.
                return Q(false);
            }

            // Use getNested utility function to avoid checking if nested objects exist.
            if (getNested('branchParams.promptBeforeExit', branchObj) === true) {
                // Return true instead of resolve. This way the promise chain started with view.backoutHandler()
                // resolves with the true value
                return view.backoutHandler()
                    .then(() => true);
            }

            LF.security.stopQuestionnaireTimeOut();
            localStorage.setItem('questionnaireToDashboard', true);

            if (isSitePad()) {
                LF.router.flash({
                    subject: view.subject,
                    visit: view.visit
                }).navigate('dashboard', {
                    trigger: true,
                    replace: true
                });
            } else {
                LF.router.flash({ subject: view.subject, visit: view.visit })
                    .navigate('dashboard', { trigger: true, replace: true });
            }

            return Q(true);
        });
};

/**
 * isCurrentPhase will compare current subject phase to phase supplied in branchParams.phase
 * if they match, resolves true, else resolves false
 * @param {Object} branchObj contains the branching configuration
 * @param {string|number} branchObj.branchParams.phase the phase to compare, can be either codelist value or string
 * @param {QuestionnaireView} view current Questionnaire view
 * @returns {Promise<T>} returns a promise that resolves as true or false
 * @example
 * {
 *     branchFrom: ED010
 *     branchTo: ED040
 *     branchFunction: 'isCurrentPhase'
 *     branchParams: {
 *                      phase: 'SCREENING'
 *                   }
 * },
 * {
 *     branchFrom: ED020
 *     branchTo: ED050
 *     branchFunction: 'isCurrentPhase'
 *     branchParams: {
 *                      phase: 30
 *                   }
 * }
 */
LF.Branching.branchFunctions.isCurrentPhase = (branchObj, view) => {
    let configPhase = branchObj.branchParams.phase,
        logger = new Logger('PDEBranching:isCurrentPhase');
    switch (typeof configPhase) {
        case 'number':
            if (_(Object.keys(LF.StudyDesign.studyPhase)).find(key => LF.StudyDesign.studyPhase[key] === configPhase)) {
                break;
            }
            logger.error(`Supplied phase number ${configPhase} is not a valid studyPhase code`);
            configPhase = null;
            break;
        case 'string':
            if (isNaN(configPhase)) {
                configPhase = LF.StudyDesign.studyPhase[configPhase];
                if (!configPhase) {
                    logger.error(`Supplied phase string ${configPhase} is not a valid studyPhase phase`);
                }
            } else {
                if (_(Object.keys(LF.StudyDesign.studyPhase)).find(key => LF.StudyDesign.studyPhase[key] === Number(configPhase))) {
                    configPhase = Number(configPhase);
                    break;
                }
                logger.error(`Supplied phase string ${configPhase} is not a valid studyPhase code`);
                configPhase = null;
            }
            break;
        default:
            logger.error(`Supplied phase ${configPhase} is not a number or string`);
            configPhase = null;
    }
    let phase = Number(view.subject.get('phase'));
    return Q(configPhase === phase);
};
