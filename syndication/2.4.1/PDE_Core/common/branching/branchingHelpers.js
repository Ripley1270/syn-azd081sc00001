import BranchHelpers from 'core/branching/branchingHelpers'; // BranchHelpers === LF.Branching.Helpers
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Logger from 'core/Logger';

const logger = new Logger('PDE_BranchHelpers');

/**
 * helper function to navigate to a screen in a questionnaire
 * @param {string} screenID the ID of the screen to navigate to
 * @returns {Q.Promise<void>} promise, resolves with the result of view.displayScreen if successful, rejects with error message otherwise.
 * @example LF.Branching.Helpers.navigateToScreen('AFFIDAVIT');
 */
export function navigateToScreen (screenID) {
    let view = LF.router.view();
    return Q().then(() => {
        if (view instanceof BaseQuestionnaireView) {
            if (_(view.data.screens).find(screen => screen.id === screenID)) {
                view.screenStack.push(screenID);
                return view.displayScreen(screenID);
            }
            logger.error(`navigateToScreen was handed the screenID ${screenID}, which does not exist in this questionnaire`);
        } else {
            logger.error('navigateToScreen was called in a view that is not extended from BaseQuestionnaireView.');
        }
        throw new Error(`attempt to navigate to screen ${screenID} failed`);
    });
}

BranchHelpers.navigateToScreen = navigateToScreen; // equivalent to adding to the LF.Branching.Helpers namespace
