export default {
    rules: [
        {
            id: 'QuestionnaireScreenSetOrientation',
            trigger: 'QUESTIONNAIRE:Before',
            evaluate: 'isWrapped',
            salience: 100,
            resolve: [{ action: 'setScreenOrientation' }],
            reject: [{ action: 'defaultAction' }]
        },
        {
            id: 'DashboardSetOrientation',
            trigger: [
                'LOGIN:Rendered',
                'DASHBOARD:Rendered'
            ],
            evaluate: 'isWrapped',
            salience: 100,
            resolve: [
                { action: 'removeMessage' },
                { action: 'setScreenOrientation', data: { setDefault: true } }
            ]
        }
    ]
};
