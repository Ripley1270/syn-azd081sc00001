import SOrules from './screenOrientationRules';
import syncRules from './PDE_SyncRules_Common';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects({}, SOrules, syncRules);
