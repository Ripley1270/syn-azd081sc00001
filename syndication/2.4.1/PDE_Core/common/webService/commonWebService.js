import COOL from 'core/COOL';
import Data from 'core/Data';
import WebService from 'core/classes/WebService';
import PDE from 'PDE_Core/common/PDE';
import { isLogPad, isSitePad } from 'core/utilities/coreUtilities';
import Logger from 'core/Logger';

class PDEWebService extends COOL.getClass('WebService', WebService) {
    getHandheldSyncData (procedureName, params) {
        let ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            },
            krpt = params.krpt || localStorage.getItem('krpt'),
            payload = {
                StoredProc: procedureName,
                Params: [
                    krpt,
                    Data.deviceID,
                    new Date().ISOStamp(),
                    new Date().getTimezoneOffset() * 60 * 1000 * -1 // negate because javascript does UTC - local for offset while ERT standard is local - UTC
                ]
            };
        return this.transmit(ajaxConfig, payload);
    }

    getTabletSyncData (procedureName, params) {
        return PDE.CollectionModelUtils.getCollection('Sites')
            .then(sitesCollection => sitesCollection.at(0))
            .then((site) => {
                let ajaxConfig = {
                        type: 'POST',
                        uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                        auth: params.auth || null
                    },
                    krdom = site.get('site_id'),
                    payload = {
                        StoredProc: procedureName,
                        Params: [
                            krdom,
                            localStorage.getItem('PHT_deviceId'),
                            new Date().ISOStamp(),
                            new Date().getTimezoneOffset() * 60 * 1000 * -1 // negate because javascript does UTC - local for offset while ERT standard is local - UTC
                        ]
                    },
                    logger = new Logger('getTabletSyncData');
                if (krdom) {
                    return this.transmit(ajaxConfig, payload);
                }
                logger.error('Unable to determine site id from Sites collection, rejecting promise');
                return Q.reject();
            });
    }

    getFullSyncData (params) {
        let logger = new Logger('getFullSyncData');
        if (isSitePad()) {
            return this.getTabletSyncData('PDE_TabletFullSyncSQLV1', params);
        } else if (isLogPad()) {
            return this.getHandheldSyncData('PDE_LPAFullSyncSQLV1', params);
        }
        logger.error('Device does not support PDE full sync, rejecting promise');
        return Q.reject();
    }

    getPartialSyncData (params) {
        let logger = new Logger('getPartialSyncData');
        if (isSitePad()) {
            return this.getTabletSyncData('PDE_TabletPartialSyncSQLV1', params);
        } else if (isLogPad()) {
            return this.getHandheldSyncData('PDE_LPAPartialSyncSQLV1', params);
        }
        logger.error('Device does not support PDE partial sync, rejecting promise');
        return Q.reject();
    }
}

class Trainer_PDEWebService extends PDEWebService {
    getFullSyncData (params) {
        if (PDE.DeviceStatusUtils.isTrainer()) {
            return Q('');
        }
        return super.getFullSyncData(params);
    }

    getPartialSyncData (params) {
        if (PDE.DeviceStatusUtils.isTrainer()) {
            return Q('');
        }
        return super.getPartialSyncData(params);
    }
}

COOL.add('WebService', Trainer_PDEWebService);
export default Trainer_PDEWebService;
