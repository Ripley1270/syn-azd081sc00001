import ObjectRef from 'core/classes/ObjectRef';

export default {
    // The current PDE_Core/template version
    PDE_CoreVersion: '241.3.2',

    // Set to false to use LPA 1.9 access codes, should always be set to true
    useAcccessCodesFor1dot10: true,

    // The default language of the study.
    defaultLanguage: 'en',

    // The default locale of the study
    defaultLocale: 'US',

    // The maximum number of consecutive unsuccessful unlock attempts in !!web!! modality.
    maxUnlockAttempts: 3,

    // Number of minutes after which the lockout for the unlock code expires
    unlockCodeLockoutTime: 5,

    // The maximum number of consecutive unsuccessful password attempts at login and reactivation.
    maxLoginAttempts: 3,

    // Number of minutes after which the account is automatically re-enabled
    // (where 0 means the account doesn't get re-enabled until a daily reset/override code is used).
    lockoutTime: 5,

    // The number of minutes to subtract from the updated timestamp when syncing users to close window of client->server time difference.
    userSyncTimeStampThreshold: 20,

    // A string array (of diary SUs) to manually specify the diaries that are unscheduled
    lastDiarySyncUnscheduledSUList: null,

    // The protocol name used for file paths during the build process, does not display in app
    protocolBuild: 'TestStudy',

    // The client name
    clientName: 'ERT',

    // The study Protocol
    studyProtocol: {
        defaultProtocol: 1,
        protocolList: new ObjectRef({
            1: 'Protocol1',
            2: 'Protocol2',
            3: 'Protocol3',
            4: 'Protocol4',
            5: 'Protocol5',
            6: 'Protocol6',
            7: 'Protocol7',
            8: 'Protocol8',
            9: 'Protocol9',
            10: 'Protocol10'
        })
    },

    // The phases used by the study
    studyPhase: new ObjectRef({
        SCREENING: 10,
        RANDOMIZATION: 20,
        TREATMENT: 30,
        FOLLOWUP: 40,
        TERMINATION: 999
    }),

    // The phase from studyPhase to be used as the termination phase
    terminationPhase: 999,

    // A Regular Expression or a String of valid characters for defining which characters are allowed for input fields.
    validInputCharacters: 'a-zA-Z0-9\\s\\.-',

    /*
     * List of supported browsers for the web modality.
     * If you do not care about the version number, use version: '*'
     */
    supportedBrowsers: [
        { name: 'Chrome', version: '*' },
        { name: 'Edge', version: '*' }
    ],

/*
    mapping list for the LanguageSelectWidget function 'filterLanguagesByLocale'
    Each key corresponds to a country for a site, with each value an array of languages that should be available
    to select in that country.
    If a country code is not defined in this list, then the 'default' array will be used instead.
*/
    sitelanguagelist: new ObjectRef({
        default: ['en-US'],
        ZA: ['en-US', 'af-ZA', 'xh-ZA'],
        EG: ['en-US', 'ar-EG'],
        BY: ['en-US', 'be-BY'],
        BG: ['en-US', 'bg-BG'],
        IN: ['en-US', 'ta-IN', 'pa-IN', 'kn-IN', 'hi-IN', 'gu-IN', 'bn-IN'],
        BA: ['en-US', 'bs-BA'],
        CZ: ['en-US', 'cs-CZ'],
        DK: ['en-US', 'da-DK'],
        AT: ['en-US', 'de-AT'],
        DE: ['en-US', 'de-DE'],
        GR: ['en-US', 'el-GR'],
        GB: ['en-US', 'en-GB'],
        US: ['en-US', 'es-US'],
        ES: ['en-US', 'es-ES'],
        PY: ['en-US', 'es-PY'],
        EE: ['en-US', 'et-EE'],
        FI: ['en-US', 'fi-FI'],
        CA: ['en-US', 'fr-CA'],
        FR: ['en-US', 'fr-FR'],
        IL: ['en-US', 'he-IL'],
        HR: ['en-US', 'hr-HR'],
        HU: ['en-US', 'hu-HU'],
        IT: ['en-US', 'it-IT'],
        JP: ['en-US', 'ja-JP'],
        GE: ['en-US', 'ka-GE'],
        KR: ['en-US', 'ko-KR'],
        LT: ['en-US', 'lt-LT'],
        LV: ['en-US', 'lv-LV'],
        MY: ['en-US', 'ms-MY'],
        NO: ['en-US', 'nb-NO'],
        BE: ['en-US', 'nl-BE'],
        NL: ['en-US', 'nl-NL'],
        PL: ['en-US', 'pl-PL'],
        BR: ['en-US', 'pt-BR'],
        PT: ['en-US', 'pt-PT'],
        RO: ['en-US', 'ro-RO'],
        RU: ['en-US', 'ru-RU'],
        SK: ['en-US', 'sk-SK'],
        AL: ['en-US', 'sq-AL'],
        RS: ['en-US', 'sr-RS'],
        SP: ['en-US', 'sr-SP'],
        SE: ['en-US', 'sv-SE'],
        PH: ['en-US', 'tl-PH'],
        TR: ['en-US', 'tr-TR'],
        UA: ['en-US', 'uk-UA'],
        VN: ['en-US', 'vi-VN'],
        CN: ['en-US', 'xh-CN'],
        TW: ['en-US', 'zh-TW'],
        ZU: ['en-US', 'zu-ZU']
    })

    // A list of supported devices for the study.
    /*
     supportedDevices : [{
        name: 'iPhone',
        match: [['Safari']]
    }, {
        name: 'Nexus S',
        match: [['Safari']]
    }, {
        name: 'Galaxy Nexus',
        match: [['Safari']]
    }],
    */

};
