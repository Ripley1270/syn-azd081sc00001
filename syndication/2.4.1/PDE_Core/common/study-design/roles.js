/**
 * @fileOverview This is an example roles study configuration file.
 * @author <a href='mailto:bill.calderwood@ert.com'>William A. Calderwood</a>
 * @version 2.1
 */

import ArrayRef from 'core/classes/ArrayRef';

export default {
    sitePad: {
        // What Roles are allowed to login to sitepad
        loginRoles: new ArrayRef(['admin', 'site']),

        // What Roles are allowed to access the visits view
        visitRoles: new ArrayRef(['admin', 'site']),

        // What Roles are allowed to transcribe a form.
        transcriptionRoles: new ArrayRef(['site']),

        // What Roles are allowed to access the homeview after backing out of a diary that has a context switch on diary entry.
        diaryBackoutRoles: new ArrayRef(['admin', 'site']),

        // What role is the 'subject' role only used in context switching right now...
        subjectRole: 'subject',

        // Reference to a filter function to query the admin users for SitePad.
        // If the config value is null, then the default filter is used which is active users with the 'admin' role
        adminUserFilter: null
    },

    // IG & IT for the saving of Role Data associated with Questionnaires
    lastDiaryRole: {
        IG: 'CG',
        IT: 'LPARole'
    },

    // If set to true, will show the password rules on password creation views.
    showPasswordRules: false,

    // if passwordFormat is not defined for specific role these settings will be used.
    defaultPasswordFormat: {
        max: 20,
        min: 4,
        lower: 0,
        upper: 0,
        alpha: 0,
        numeric: 0,
        special: 0,
        custom: [],
        allowRepeating: true,
        allowConsecutive: true
    },

    // Configuration for the unlock code feature defaults. Settings within role definitions will take precedence
    unlockCodeConfig: {
        // Seed code used in calculation for unlock code combined with current date.
        seedCode: 1111,

        // Sets the length of unlock code
        codeLength: 6
    },

    roles: new ArrayRef([
        {
            id: 'subject',
            displayName: 'SUBJECT_ROLE',
            lastDiaryRoleCode: 0,
            defaultAffidavit: 'P_SignatureAffidavit',
            userSelectIconClass: 'patient-icon',
            unlockCodeConfig: {
                seedCode: 1111,
                codeLength: 6
            },
            addPermissionsList: ['ALL'],
            passwordFormat: {
                min: 4,
                max: 8
            },
            product: ['logpad'],
            passwordInputType: 'password'
        }, {
            id: 'site',
            displayName: 'SITE_ROLE',
            lastDiaryRoleCode: 1,
            permissions: [],
            defaultAffidavit: 'SignatureAffidavit',
            syncLevel: 'site',
            offlineSync: false,
            userSelectIconClass: 'doctor-icon',
            dialogs: {
                deactivated: 'SITE_DEACTIVATED',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            text: {
                deactivated: 'SITE_DEACTIVATED_LABEL',
                header: 'SITE_GATEWAY_TITLE',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            addPermissionsList: [],
            passwordFormat: {
                min: 4,
                max: 8
            },
            product: ['logpad', 'sitepad', 'web'],
            passwordInputType: 'password'
        }, {
            id: 'admin',
            displayName: 'SITE_ADMINISTRATOR',
            lastDiaryRoleCode: 2,
            permissions: ['ALL'],
            defaultAffidavit: 'SignatureAffidavit',
            syncLevel: 'site',
            offlineSync: false,
            addPermissionsList: ['ALL'],
            userSelectIconClass: 'admin-icon',
            dialogs: {
                deactivated: 'SITE_DEACTIVATED',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            text: {
                deactivated: 'SITE_DEACTIVATED_LABEL',
                header: 'APPLICATION_HEADER',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            passwordFormat: {
                min: 4,
                max: 8
            },
            product: ['logpad', 'sitepad', 'web'],
            passwordInputType: 'password'
        }, {
            id: 'caregiver',
            displayName: 'CAREGIVER_ROLE',
            lastDiaryRoleCode: 3,
            permissions: ['ALL'],
            defaultAffidavit: 'SignatureAffidavit',
            syncLevel: 'subject',
            offlineSync: true,
            dialogs: {
                deactivated: 'SITE_DEACTIVATED',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            text: {
                deactivated: 'SITE_DEACTIVATED_LABEL',
                header: 'APPLICATION_HEADER',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            product: ['logpad'],
            passwordInputType: 'password'
        }, {
            id: 'caregiver2',
            displayName: 'CAREGIVER2_ROLE',
            lastDiaryRoleCode: 4,
            permissions: ['ALL'],
            defaultAffidavit: 'SignatureAffidavit',
            syncLevel: 'subject',
            offlineSync: true,
            dialogs: {
                deactivated: 'SITE_DEACTIVATED',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            text: {
                deactivated: 'SITE_DEACTIVATED_LABEL',
                header: 'APPLICATION_HEADER',
                activatedElsewhere: 'SITE_ACTIVATED_ELSEWHERE'
            },
            product: ['logpad'],
            passwordInputType: 'password'
        }
    ]),
    adminUser: { role: 'admin' },
    defaultUserValues: {
        password: 'temp',
        role: '',
        language: ''
    }
};
