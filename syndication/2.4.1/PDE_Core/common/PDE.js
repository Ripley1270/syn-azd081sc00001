/**
 * namespace for PDE additions, to help logically separate them from core features
 * @namespace PDE
 */
let PDE = {
    /**
     * namespace for utilities when working with collection and model storage
     * @namespace CollectionModelUtils
     * @memberof! PDE
     */
    CollectionModelUtils: {},

    /**
     * namespace for utilities when working with dates and times
     * @namespace PDE.DateTimeUtils
     */
    DateTimeUtils: {},

    /**
     * Utilities namespace for device status
     * @namespace PDE.DeviceStatusUtils
     * @memberOf PDE
     */
    DeviceStatusUtils: {},

    /**
     * Utilities namespace for Questionnaire-specific functions
     * @namespace PDE.QuestionnaireUtils
     * @memberOf PDE
     */
    QuestionnaireUtils: {},

    /**
     * Utilities namespace for Visit-specific functions
     * @namespace PDE.VisitUtils
     * @memberOf PDE
     */
    VisitUtils: {},

    // Any utils and helper functions related to answer models and question responses
    AnswerUtils: {},

    // Any utils and helper functions related to Moment date time
    MomentUtils: {},

    // Any utils and helper functions related to looping
    LoopingUtils: {},

    subjectUtils: {},

    // Designated namespace for study specific code
    Custom: {},

    commonUtils: {}
};

window.PDE = PDE;

export default PDE;
