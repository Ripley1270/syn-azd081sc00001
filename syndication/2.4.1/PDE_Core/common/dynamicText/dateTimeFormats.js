/**
 * format converts a date into one specifically formatted according to the current locality's dateConfig option.
 * Uses date passed in, or defaults to current date if a falsey value is passed into the first parameter.
 * @param {Date|string} [date] - OPTIONAL the date to format
 * @param {string} format - name of dateConfig item that contains the format
 * @return {string} the formatted string
 */
export function format (date, format) {
    const dateOptions = _.extend({}, LF.strings.dates({dates: {}}), LF.strings.dates({dateConfigs: {}})),
        dateBox = $('<input data-role=\"datebox\" />'),
        dateLang = {
            lang: {
                default: dateOptions,
                isRTL: (LF.strings.getLanguageDirection() === 'rtl')
            },
            mode: 'datebox'
        };

    dateBox.datebox(dateLang);

    if (date) {
        if (typeof date === "string") {
            date = new Date(date);
        }

        return dateBox.datebox('callFormat', dateOptions[format], date);
    } else {
        return dateBox.datebox('callFormat', dateOptions[format], dateBox.datebox('getTheDate'));
    }
}
LF.DynamicText.format = format;

/**
 * formatTime converts date object or string representation of time to the format
 * defined in dateConfig's timeOutput resource
 * Defaults to current time if first parameter is falsey
 * @param {Date|string} [time] - OPTIONAL date object or time string to format
 * @return {string} - the formatted string
 */
export function formatTime (time) {
    if(typeof time === 'string'){
        return format(`01 Jan 1900 ${time}`, 'timeOutput');
    }
    else if(time instanceof Date || !Boolean(time)){
        //format uses current time if first parameter is falsey
        return format(time, 'timeOutput');
    }
    else {
        throw new Error(`Parameter is not a valid time: ${time}`);
    }
}
LF.DynamicText.formatTime = formatTime;

/**
 * formatDate converts date object or string representation of date to the format
 * defined in dateConfig's dateFormat resource
 * Defaults to current date if first parameter is falsey
 * @param {Date|string} date - OPTIONAL date object or date string to format
 * @return {string} - the formatted string
 */
export function formatDate (date) {
    return format(date, 'dateFormat');
}
LF.DynamicText.formatDate = formatDate;

/**
 * formatDateTime converts date object or string representation of dateTime to the format
 * defined in dateConfig's dateTimeFormat resource
 * Defaults to current date and time if first parameter is falsey
 * @param {Date|string} date - OPTIONAL date object or dateTime string to format
 * @return {string} - the formatted string
 */
export function formatDateTime (date) {
    return LF.DynamicText.format(date, 'dateTimeFormat');
}
LF.DynamicText.formatDateTime = formatDateTime;
