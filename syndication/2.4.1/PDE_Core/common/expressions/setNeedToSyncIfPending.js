/* eslint-disable lines-around-comment */
import ELF from 'core/ELF';
import Logger from 'core/Logger';

/**
 * @memberOf ELF.expressions
 * @method  setNeedToSyncIfPending
 * @description
 * Checks the transmission queue for pending transmissions. Resolves false and sets the 'NeedToSync' flag if any exist.
 * @param {string} webServiceFunction the name of the webservice function we would set the 'NeedToSync' flag for
 * @returns {PromiseLike} resolves true if no pending transmissions, false otherwise.
 * @example
 * evaluate : {
 *   expression : 'setNeedToSyncIfPending',
 *   input  : 'getFullSyncData'
 * }
 */
export default function setNeedToSyncIfPending (webServiceFunction) {
    let logger = new Logger(`setNeedToSyncIfPending:${webServiceFunction}`);
    return LF.dataAccess.count('Transmission').then((count) => {
        // resume true action if no pending reports
        if (count === 0) {
            return true;
        }
        return Q.reject({ logtype: 'warn', message: 'Sync delayed because of stored reports' });
    }).catch((err) => {
        if (err && err.logtype && err.message) {
            logger[err.logtype](err.message);
            localStorage.setItem(`NeedToSync_${webServiceFunction}`, 'true');
            return false;
            /*
                consider inserting confirm dialog popup here to inform the user that sync was delayed,
                inform that another sync attempt has been scheduled,
                and that the user can optionally attempt another update.
                This means replacing the previous return false; with the following line,
                and registering the syncDelayed popup with the messageRepo

                MessageRepo.display('syncDelayed').then(() => {return false;});
             */
        }
        logger.error('Unhandled rejection occured, resolving false.', err);
        return false;
    });
}

ELF.expression('setNeedToSyncIfPending', setNeedToSyncIfPending);
