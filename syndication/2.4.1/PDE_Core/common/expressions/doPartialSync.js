import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @method  doPartialSync
 * @description
 * Determines if partialSync is enabled for the device
 * @returns {Q.Promise<boolean>} True if partial sync is enabled in the study design
 * @example
 * evaluate : 'doPartialSync'
 */
export default function doPartialSync () {
    return Q(Boolean(LF.StudyDesign.enablePartialSync));
}

ELF.expression('doPartialSync', doPartialSync);
