import Logger from 'core/Logger';
import Base from 'core/models/StorageBase';

let logger = new Logger('ModelList');

export default class ModelList {
    /**
     * Creates an instance of ModelList.
     * @memberof ModelList
     */
    constructor() {
        this.modelArray = [];
    }

    /**
     * Add a model to the PDE Model List
     * @param {Object} model Object
     */
    addModel(model) {
        try {
            //TODO Add validation support. This will require implemented a validation method in the Model that checks for unexpected attributes
            //if (model.isValid())
            {
                if(model instanceof Base){
                    this.modelArray.push(model);
                }
                else {
                    throw new Error(`Model is invalid. Model does NOT extend Base model, SAD!`);
                }
            }
        }
        catch (err) {
            logger.error('ModelList.addModel failed to add Model to Modelist: ' + err.message);
            throw err;
        }
    };

    /**
     * Add an array of models
     * @param {Array} models contain an array of models
     */
    addModels(models) {
        _.each(models, (model) => {
            this.addModel(model);
        });
    };

    /**
     * Create a New Model and Add it to the PDE Model List
     * @param {String} modelName is the name of the model
     * @param {Object} data is a object with properties that contains the key-values of the model
     */
    addNewModel(modelName, data) {
        this.addModel(new LF.Model[modelName](data));
    };

    /**
     * Get the list of models
     * @returns {Array} Return the array of models
     * @memberof ModelList
     */
    getModels() {
        return this.modelArray.slice(0);
    }

    /**
     * Add an array of models
     * @param {Array} models contain an array of models
     */
    save(success, error) {
        //TODO validate all arrays before saving

        return Q.all(_.map(this.modelArray, (model) => {
            try {
                model.save({}, {
                    onSuccess: () => {
                        logger.info(`Successfully saved model ${model.name}`);
                        Q.resolve();
                    },
                    onError: (err) => {
                        logger.error(`Failed to save model ${model.name}`, err);
                        Q.reject(err);
                    }
                });
            }
            catch (e) {
                return Q.reject(e);
            }
        })).then(() => {
            success && success();
            return Q.resolve();
        }).fail((e) => {
            logger.error(`ModelList.save failed to save model: ${e}`);
            error && error();
            return Q.reject(e);
        })
    }
};

LF.Utilities.ModelList = ModelList;
PDE.CollectionModelUtils.ModelList = ModelList;
