import Logger from 'core/Logger';
import PDE from '../PDE';

let logger = new Logger('AnswerUtils');

export function getAnswerIG (answerModel) {
    let swAlias = answerModel.get('SW_Alias').split('.');

    return swAlias[0];
}

PDE.AnswerUtils.getAnswerIG = getAnswerIG;

export function getAnswerIGR (answerModel) {
    let swAlias = answerModel.get('SW_Alias').split('.');

    return swAlias[1];
}

PDE.AnswerUtils.getAnswerIGR = getAnswerIGR;

export function getAnswerIT (answerModel) {
    let swAlias = answerModel.get('SW_Alias').split('.');

    return swAlias[2];
}

PDE.AnswerUtils.getAnswerIT = getAnswerIT;

export function getAnswerModels (options) {
    let result;

    try {
        let questionnaireView = PDE.QuestionnaireUtils.getQuestionnaireView();

        if(typeof options === 'undefined'){
            result = questionnaireView.answers.models;
        }
        else {
            result = _.filter(questionnaireView.answers.models, (answerModel) => {
                    let isValid = ((typeof options.questionID === 'undefined' || options.questionID === answerModel.get('question_id')) &&
                        (typeof options.IGR === 'undefined' || Number(options.IGR) === Number(getAnswerIGR(answerModel))) &&
                        (typeof options.IG === 'undefined' || options.IG === getAnswerIG(answerModel)) &&                        
                        (typeof options.IT === 'undefined' || options.IT === getAnswerIT(answerModel)));
               
                return isValid;
            });
        }
        
    } catch(err) {    
        logger.error('Unable to get Answer Models', err);
    }

    return result;
}

PDE.AnswerUtils.getAnswerModels = getAnswerModels;

export function removeAnswerModels (options, removeQuestionView) {
    let answerModelsToDelete = PDE.AnswerUtils.getAnswerModels(options),
        questionnaireView = PDE.QuestionnaireUtils.getQuestionnaireView(),
        questionViews = questionnaireView.questionViews,
        answers = questionnaireView.answers;
    
    $.each(answerModelsToDelete, (index, answerModel) => {
        //If the question view need to be remove along with the answer
        if(removeQuestionView){
            //Find all questions using the answer model
            let removeQuestionViews = _.filter(questionViews, (questionView) => {
                let viewAnswer = questionView && questionView.widget && questionView.widget.answer;

                return (viewAnswer === answerModel);
            });

            //Remove question view
            _.map(removeQuestionViews, (questionView) => {
                questionViews.splice(_(questionViews).indexOf(questionView), 1);
            });
        }

        answers.remove(answerModel);
    });
}

PDE.AnswerUtils.removeAnswerModels = removeAnswerModels;

export function getAnswerResponse (options) {
    let modelOptions = {},
        answerModels,
        response;

    if(typeof options.questionID === 'undefined' &&
        typeof options.IT === 'undefined'){
            throw new Error('Missing both question ID and IT. At least one must be set.');
        }

    if(typeof options.questionID !== 'undefined'){
        modelOptions.questionID = options.questionID;
    }

    if(typeof options.IG !== 'undefined'){
        modelOptions.IG = options.IG;
    }

    if(typeof options.IT !== 'undefined'){
        modelOptions.IT = options.IT;
    }

    if(typeof options.IGR === 'undefined'){

        if(typeof options.IG !== 'undefined'){
            modelOptions.IGR = PDE.QuestionnaireUtils.getQuestionnaireView().getCurrentIGR(options.IG);
        }
    }
    else {
        modelOptions.IGR = options.IGR;
    }

    answerModels = getAnswerModels(modelOptions);

    if(typeof modelOptions.IGR === 'undefined'){
        //Filter answer models based on the model's IGR and compared
        //to the answer IG's current IGR
        answerModels = _.filter(answerModels, (answerModel) => {
            let IG = getAnswerIG(answerModel),
                IGR = getAnswerIGR(answerModel),
                currentIGR = PDE.QuestionnaireUtils.getQuestionnaireView().getCurrentIGR(IG);

            return (Number(IGR) === Number(currentIGR));
        });
    }
    
    if(answerModels && answerModels.length > 0){
        response = answerModels[0].get('response');
    }

    return response;
}

PDE.AnswerUtils.getAnswerResponse = getAnswerResponse;

export function getAnswerResponseByQuestionIDAndIG (questionID, IG, IGR){
    let options = {};

    if(typeof IG === 'undefined' || typeof questionID === 'undefined'){
        throw new Error('Unable to get answer response. Missing IG.');
    }

    if(typeof questionID === 'undefined'){
        throw new Error('Unable to get answer response. Missing question ID.');
    }

    options.questionID = questionID;
    options.IG = IG;

    if(typeof IGR !== 'undefined'){
        options.IGR = IGR;
    }

    return getAnswerResponse(options);
}

PDE.AnswerUtils.getAnswerResponseByQuestionIDAndIG = getAnswerResponseByQuestionIDAndIG;

export function getAnswerResponseByQuestionID (questionID, IGR){
    let options = {};

    if(typeof questionID === 'undefined'){
        throw new Error('Unable to get answer response. Missing question ID.');
    }

    options.questionID = questionID;

    if(typeof IGR !== 'undefined'){
        options.IGR = IGR;
    }

    return getAnswerResponse(options);
}

PDE.AnswerUtils.getAnswerResponseByQuestionID = getAnswerResponseByQuestionID;

export function addAnswerResponse (params){
    let answerModels = getAnswerModels(params),
        view = PDE.QuestionnaireUtils.getQuestionnaireView();

    if(typeof params.response === 'undefined' || params.response === null){
        let err = 'addAnswer missing response.';
        logger.error(err);
        throw new Error(err);
    }

    //If answer model exist
    if(answerModels.length === 1 ){
        //Update existing values
        _.first(answerModels).set('response', String(params.response));
    }
    else if(answerModels.length >= 1 ){
        let err = `Failed to add Answer Response. Unexpected number answer models (${answerModels.length})`;
        logger.error(err);
        throw new Error(err);
    }
    //Else add new answer model
    else {
        if(view &&
            params.question_id != null && params.questionnaire_id != null &&
            params.response != null && params.IG != null && params.IGR != null && params.IT != null) {
            let model = new view.Answer({
                question_id: params.question_id,
                questionnaire_id: params.questionnaire_id,
                response: String(params.response),
                SW_Alias: `${params.IG}.${params.IGR}.${params.IT}`
            });
            view.answers.add(model);
        }
        else {
            logger.error('Attempt failed to insert an IT.');
            throw new Error('Attempt failed to insert an IT.');
        }
    }
}

PDE.AnswerUtils.addAnswerResponse = addAnswerResponse;

export function addAnswerResponseByIT (IG, IT, response, IGR){
    let options = {
            question_id: IT,
            questionnaire_id: PDE.QuestionnaireUtils.getQuestionnaireID(),
            response: response,
            IG: IG,
            IT: IT,
            IGR: (typeof IGR !== 'undefined')? IGR: PDE.AnswerUtils.getCurrentIGR(IG)
        };

    addAnswerResponse(options);
}

PDE.AnswerUtils.addAnswerResponseByIT = addAnswerResponseByIT;

export function addAnswerResponseByQuesID (quesID, IG, IT, response, IGR){
    let options = {
            question_id: quesID,
            questionnaire_id: PDE.QuestionnaireUtils.getQuestionnaireID(),
            response: response,
            IG: IG,
            IT: IT,
            IGR: (typeof IGR !== 'undefined')? IGR: PDE.AnswerUtils.getCurrentIGR(IG)
        };

    addAnswerResponse(options);
}

PDE.AnswerUtils.addAnswerResponseByQuesID = addAnswerResponseByQuesID;

export function removeAnswerByIG (IG){
    let view = PDE.QuestionnaireUtils.getQuestionnaireView(),
        answers = getAnswerModels({IG: IG});

    if(answers && answers.length > 0){
        _.each(answers, (answer) => {
            view.data.answers.remove(answer);
        });
    }
}

PDE.AnswerUtils.removeAnswerByIG = removeAnswerByIG;

