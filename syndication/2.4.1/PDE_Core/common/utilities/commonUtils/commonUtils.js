import Answer from 'core/models/Answer';
import Logger from 'core/Logger';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';

/**
 * commonUtils is written such that it can be imported into another file,
 * or the traditional global namespace LF.Utilities
 */

/**
 * addSysVar returns a promise that resolves true if a Sysvar is successfully added, or rejects if it fails.
 * @param {Object} params object containing parameters for sysvar to be added
 * @return {Promise<T>} resolves true if successful, else rejects
 */
export function addSysVar (params) {
    let model,
        logger = new Logger('PDE_Utilities.addSysvar');
    return Q().then(() => {
        if (params && params.view && params.questionnaire_id && params.response && params.IG && params.sysvar) {
            model = new Answer({
                response: params.response,
                SW_Alias: `${params.IG}.${params.sysvar}`,
                questionnaire_id: params.questionnaire_id,
                question_id: 'SYSVAR'
            });
            if (params.view.data.dashboard) {
                model.set('instance_ordinal', params.view.data.dashboard.get('instance_ordinal'));
            }
            params.view.data.answers.add(model);
            return true;
        } else {
            logger.error('Attempt to insert a sysVar failed: params are incomplete.');
            throw new Error('Attempt to insert a sysVar failed: params are incomplete.');
        }
    });
}

LF.Utilities.addSysvar = addSysVar;

if (!String.format) {
    String.format = function (format) {
        let args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] !== 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

/**
 * dateDiffInDays returns the number of days (24 hour periods) between date 1 and date 2, rounded down to the nearest
 * day, taking into account time zone offsets. If useAbsolute evaluates as false, then the return value is date1 -
 * date2. If useAbsolute evaluates as true, the return value is |date1 - date2| (the absolute value of the difference)
 * @param {Date} date1 first date to compare
 * @param {Date} date2 second date to compare
 * @param {boolean} [useAbsolute] if true, then returns the absolute value of calculation
 * @return {number} dateDiff
 */
export function dateDiffInDays (date1, date2, useAbsolute) {
    let date1Copy = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate(), 0, 0, 0, 0),
        date2Copy = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate(), 0, 0, 0, 0),
        date1Offset = (date1Copy.getTimezoneOffset() * 60 * 1000),
        date2Offset = (date2Copy.getTimezoneOffset() * 60 * 1000),
        msDiff;

    if (!useAbsolute) {
        msDiff = date1Copy - date2Copy + (date2Offset - date1Offset);
    } else {
        msDiff = Math.abs(date1Copy - date2Copy + (date2Offset - date1Offset));
    }

    return Math.floor(msDiff / 1000 / 60 / 60 / 24);
}

LF.Utilities.dateDiffInDays = dateDiffInDays;

/**
 * isTrainer checks local storage for a trainer flag
 * @return {boolean} returns true if in trainer mode
 */
export function isTrainer () {
    return Boolean(localStorage.getItem('trainer'));
}

LF.Utilities.isTrainer = isTrainer;

/**
 * isScreenshotMode checks local storage for a screenshot flag
 * @return {boolean} returns true if in screenshot mode
 */
export function isScreenshotMode () {
    return Boolean(localStorage.getItem('screenshot'));
}

LF.Utilities.isScreenshotMode = isScreenshotMode;

/**
 * getCollection returns a promise that resolves with the requested collection with data fetched
 * @param {string} collectionName the name of the collection to fetch
 * @return {Promise.<collection>} promise that resolves as collection populated with data
 */
export function getCollection (collectionName) {
    let logger = new Logger('PDE_Utilities.getCollection');
    return Q()
        .then(() => {
            if (typeof LF.Collection[collectionName] !== 'undefined') {
                return LF.Collection[collectionName].fetchCollection();
            }
            else {
                throw new Error(`Failed: ${collectionName} is not a valid collection.`);
            }
        }).then((fetchedCollection) => {
            logger.info(`Successfully fetched collection ${collectionName}`);
            return fetchedCollection;
        }, (err) => {
            logger.error(`Failed: unable to fetch data for ${collectionName}.`, err);
            throw err;
        });
}

LF.Utilities.getCollection = getCollection;

/**
 * getCollections returns a promise that resolves with an object,
 * where the keys are names of collections that contains values of the fetched collections.
 * @param {string[]} collectionNames array of collection names to fetch
 * @return {Promise.<Object<collections>>} object containing keys names for each fetched collection, with values of the
 *     collections
 */
export function getCollections (collectionNames) {
    let promiseFetches = [];

    if (!_.isArray(collectionNames)) {
        throw new Error('Fail to get collections. CollectionNames is not an array.')
    }

    _.forEach(collectionNames, (colName) => {
        promiseFetches.push(LF.Utilities.getCollection(colName));
    });
    return Q.all(promiseFetches)
        .then((fetchedColArray) => {
            let result = {};

            _.each(fetchedColArray, (collection, index) => {
                result[collectionNames[index]] = collection;
            });

            return result;
        });
}

LF.Utilities.getCollections = getCollections;

/**
 * saveModel returns a promise which executes the save function of a supplied model, with optional data object
 * @param {LF.Model.<T>} model the model to be saved
 * @param {Object} [data] data to be saved into the model
 * @return {Promise.<T>} resolves upon successful save, rejects if model DNE or cannot be saved
 */
export function saveModel (model, data = {}) {
    let logger = new Logger('PDE_Utilities.saveModel');
    return Q.Promise((resolve) => {
        model.save(data, {
            onSuccess: () => {
                logger.info(`Successfully saved model ${model.name}`);
                resolve();
            },
            onError: (err) => {
                logger.error(`Failed to save model ${model.name}`, err);
                resolve(err);
            }
        });
    }).then((err) => {
        //  this catches the onError error and transforms it into a reject without accidentally logging both
        //  the Failed to save model and Attempted to save non-model object messages
        if (err) {
            throw err;
        } else {
            return true;
        }
    }, (err) => {
        logger.error('Attempted to save non-model object', err);
        throw err;
    });
}

LF.Utilities.saveModel = saveModel;

/**
 * saveNewModel returns a promise that resolves after a model of type modelName
 * is successfully created and saved with optional data
 * @param {string} modelName name of model type to be saved
 * @param {Object} [data] data to be saved
 * @return {Promise.<TResult>} resolves upon successful save of model, rejects if modelName isn't a real model type or
 *     if the model fails to save.
 */
export function saveNewModel (modelName, data = {}) {
    let logger = new Logger('PDE_Utilities.saveNewModel');

    return Q.Promise((resolve) => {
        resolve(new LF.Model[modelName]());
    }).then((model) => {
        return saveModel(model, data);
    }, (err) => {
        logger.error(`Failed to create new model: ${modelName} is not a model.`, err);
        throw err;
    });
}

LF.Utilities.saveNewModel = saveNewModel;

/**
 * getLastStartedDate returns a promise that resolves with a date object of the last started date of diaryID,
 * or rejects with string message if diaryID cannot be found.
 * If stripTimeZone is true, lastStartedDate will be returned as UTC time
 * (ie, if the lastStartedDate is 12:00 at tz +6:00, and the device is in tz -4:00,
 * then getLastStartedDate will resolve with a date at 12:00 at tz -4:00)
 * @param {string} diaryID id of the diary
 * @param {boolean} [stripTimeZone] optional flag
 * @return {Promise.<Date>} resolves with Date object, or rejects with error message
 */
export function getLastStartedDate (diaryID, stripTimeZone) {
    let logger = new Logger('PDE_Utilities.getLastStartedDate');
    return LF.Utilities.getCollection('LastDiaries')
        .then((lastDiariesCol) => {
            let diary = _.last(lastDiariesCol.where({ questionnaire_id: diaryID }));
            if (diary) {
                let date;
                if (stripTimeZone) {
                    date = new Date(LF.Utilities.shiftToNewLocal(diary.get('lastStartedDate')));
                } else {
                    date = new Date(diary.get('lastStartedDate'));
                }
                return date;
            } else {
                let err = `No diary with id ${diaryID} has been started`;
                logger.error(err);
                throw new Error(err);
            }
        });
}

LF.Utilities.getLastStartedDate = getLastStartedDate;


/**
 * Modified function from:
 * http:// stackoverflow.com/questions/4912788/truncate-not-round-off-decimal-numbers-in-javascript
 * Takes a number/float, removes decimals after specified precision without applying rounding rules.
 * EX: truncate SBMTOT1N to 3 decimal places
 * SBMTOT1N = LF.Utilities.TruncateNumber(SBMTOT1N, 3);
 * @param {Number} number the value to truncate
 * @param {Number} precision the number of decimal places to truncate 'number' to
 * @return {string} the truncated value
 */
export function TruncateNumber (number, precision) {
    let re = new RegExp('(\\d+\\.\\d{' + precision + '})(\\d)'),
        m = number.toString().match(re);
    return m ? parseFloat(m[1]).toFixed(precision) : number.valueOf().toFixed(precision);
}

LF.Utilities.TruncateNumber = TruncateNumber;


// PDE_TODO: find out the use case for this function
/**
 *
 * @param num
 * @param numZeros
 * @return {string}
 */
export function pdeZeroPad (num, numZeros) {
    let n = Math.abs(num);
    let zeros = Math.max(0, numZeros - Math.floor(n).toString().length);
    let zeroString = Math.pow(10, zeros).toString().substr(1);
    if (num < 0) {
        zeroString = '-' + zeroString;
    }

    return zeroString + n;
}

LF.Utilities.pdeZeroPad = pdeZeroPad;


/**
 * removeITByIG queries for all answers with IG, and removes them from the current view
 * @param {string} IG - the IG to remove
 */
export function removeITByIG (IG) {
    let answers,
        view = LF.router.view();

    if (view instanceof BaseQuestionnaireView) {
        if (typeof IG !== 'undefined') {
            answers = view.queryAnswersByIG(IG);
            view.data.answers.remove(answers);
        }
    }
}

LF.Utilities.removeITByIG = removeITByIG;

/**
 * Find the resource associated to the string ID and return it synchronously.
 * It does NOT support resolving any dynamic text within the resource string
 * @param {String} stringID The key of the resource to find.
 * @param {Object|undefined} [options] Namespace, locale and language option for the stringID. If the namespace is not
 *     defined, it uses the questionnaire namespace. Else, it fallback to the "STUDY" namespace
 * @returns {String} The resource string of the stringID
 * @example let myString = getString("myStringID");
 * @example let myString = getString("myStringID", {namespace: "myNamespace"});
 * @example let myString = getString("myStringID", {namespace: "myNamespace", language: "en", locale: "US"});
 */
export function getString (stringID, options = {}) {
    let logger = new Logger('PDE_Utilities.getString'),
        questionnaireID = LF.Data.Questionnaire.id,
        findString = (stringID, options) => {
            let results = LF.strings.find({
                namespace: options && options.namespace,
                locale: options && options.locale,
                language: options && options.language
            });

            return !!results && results.get('resources')[stringID];
        },
        result;

    if (!stringID) {
        logger.error(`Invalid string ID: ${stringID}`);
    }

    // If no namespace is defined
    if (!options.namespace) {
        if (questionnaireID) {
            // Use questionnaire ID as default namespace
            options.namespace = questionnaireID;
            result = findString(stringID, options);
        }

        if (!result) {
            // Use STUDY as default namespace
            options.namespace = 'STUDY';
        }
    }

    // If no stringID is return
    if (!result) {
        result = findString(stringID, options);
    }

    if (typeof result === 'undefined') {
        logger.error(`Unable to resolved string ID (${stringID}) and namespace (${options.namespace})`);
    }

    return result || stringID;
}

LF.Utilities.getString = getString;

/**
 * Find the resources associated to the keys (string IDs) in the array and return it synchronously.
 * It does NOT support resolving any dynamic text within the resource string
 * @param {Array} stringIDs The array containing the keys for the resources that needed to return.
 * @param {Object|undefined} [options] Namespace, locale and language option for the stringID. If the namespace is not
 *     defined, it uses the questionnaire namespace. Else, it fallback to the "STUDY" namespace
 * @returns {Object} The object containing the resources for each key
 * @example let myStrings = getStrings(["myStringID1", "myStringID2", "myStringID3"]);
 * @example let myString1 = myStrings["myStringID1"];
 * @example let myString2 = myStrings["myStringID2"];
 * @example let myString3 = myStrings["myStringID3"];
 */
export function getStrings (stringIDs, options = {}) {
    let results = {};

    if (_.isArray(stringIDs)) {
        _.each(stringIDs, (stringID) => {
            results[stringID] = LF.Utilities.getString(stringID, options);
        });
    }
    else {
        results = LF.Utilities.getString(stringIDs, options);
    }

    return results;
}

LF.Utilities.getStrings = getStrings;

/**
 * Get the object specific from the given path
 * @param {string} path of the object to be retreived
 * @returns {Object} The object from the path
 */
export function getPath (path) {
    let logger = new Logger('CommonUtils.getPath'),
        scope = window,
        scopeSplit = path.split('.'),
        i;

    if (!path) {
        let err = `Path have no value. It is ${path}`;
        logger.error(err);
        throw new Error(err);
    }

    for (i = 0; i < scopeSplit.length - 1; i++) {
        scope = scope[scopeSplit[i]];

        if (typeof scope === 'undefined') {
            let err = `Unable to get path for "${path}"`;
            logger.error(err);
            throw new Error(err);
        }
    }

    return scope[scopeSplit[scopeSplit.length - 1]];
}

PDE.commonUtils.getPath = getPath;
