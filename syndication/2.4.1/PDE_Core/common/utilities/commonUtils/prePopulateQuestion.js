import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import QuestionView from 'core/views/QuestionView';
import DateTimeWidgetBase from 'core/widgets/DateTimeWidgetBase';
import { formatDate, formatTime } from 'PDE_Core/common/dynamicText/dateTimeFormats';

/**
 *prePopulateQuestion will take in a set of parameters defining a question, and will generate or fetch the appropriate question view, populate the answer, and add the IT to the QuestionnaireView for form submission
 * @param {Object} params - contains the parameters required for this function
 * @param {string} params.screen_id - required.
 * @param {string} params.question_id - required.
 * @param {string} params.questionnaire_id - required.
 * @param {string} params.response - required.
 * @param {string} params.IG - required.
 * @param {Number} params.IGR - required.
 * @param {string} params.IT - required.
 * @param {Boolean} [params.markAsCompleted] - OPTIONAL, defaults to true
 *
 * example params = {
 *                      question_id : 'HHTrainingModule_TM050_MC_BAR_Q0',
 *                      screen_id : 'HHTrainingModule_TM050_MC_BAR',
 *                      questionnaire_id : 'HHTrainingModule',
 *                      response : '2',
 *                      IG : 'HHTrainingModule',
 *                      IGR : 0,
 *                      IT : 'HHTrainingModule_TM050_MC_BAR',
 *                      markAsCompleted : false
 *                  }
 * @return {Promise.<Boolean>} resolves with a generic true value, rejects with errors if an issue is encountered.
 */
export function prePopulateQuestion (params) {
    return Q()
    // check for prerequisites
        .then(() => {
            let view = LF.router.view();

            if (!(params ||
                params.screen_id ||
                params.question_id ||
                params.questionnaire_id ||
                params.response ||
                params.IG ||
                params.IGR ||
                params.IT)) {
                throw new Error('Missing required parameters');
            }

            if (view instanceof BaseQuestionnaireView) {
                return view;
            } else {
                throw new Error('prePopulateQuestion should only be called while in a View extended from BaseQuestionnaireView');
            }
        })

        // find or make the questionView (the thing that actually displays)
        .then((view) => {

            // make sure the screen_id provided is a real screen
            let screens = view.data.screens,
                screen = _(screens).find((screenModel) => {
                    return screenModel.get('id') === params.screen_id;
                });

            if (!screen) {
                throw new Error(`Screen with id ${params.screen_id} does not exist in this questionnaire.`);
            }

            // make sure the question_id provided is a real question

            let questions = view.data.questions;
            let question = _(questions.models).find((questionModel) => {
                return questionModel.id === params.question_id;
            });

            if (!question) {
                throw new Error(`Question with id ${params.question_id} does not exist in this questionnaire.`);
            }

            let questionView = _(view.questionViews).find((item) => {
                return item.id === question.id;
            });

            //  If no view exists create a new one.
            if (!questionView) {
                questionView = new QuestionView({
                    id: question.get('id'),
                    screen: params.screen_id,
                    model: question,
                    parent: view,
                    mandatory: typeof question.mandatory === 'undefined' || question.mandatory,
                    ig: question.get('IG'),
                    igr: question.get('IG') ? view.getCurrentIGR(question.get('IG')) : null
                });

                // add it to the Questionnaire View
                view.questionViews.push(questionView);
            }
            return questionView;

            // add the IT, and populate the questionView with the answer created
        }).then((questionView) => {

            // add IT will create a new Answer model, or modify an existing one if there's a matching one found via
            // queryAnswersByITAndIGR
            questionView.parent.addIT({
                question_id: params.question_id,
                questionnaire_id: params.questionnaire_id,
                response: params.response,
                IG: params.IG,
                IGR: params.IGR,
                IT: params.IT
            });

            // query for the answer model just created/modified
            let answer = _.last(questionView.parent.queryAnswersByIGITAndIGR(params.IG, params.IT, params.IGR));

            // populate it into the answers collection of the widget it pertains to.
            questionView.widget.answers.add(answer);

            // add it as the current active answer to the widget
            questionView.widget.answer = answer;

            // mark question as completed
            questionView.completed = params.markAsCompleted !== false;
            questionView.widget.completed = questionView.completed;

            // Date and Time widgets require extra strings for display
            if (questionView.widget instanceof DateTimeWidgetBase) {
                let responseDate = new Date(answer.get('response'));

                questionView.widget.localizedDate = formatDate(responseDate);
                questionView.widget.localizedTime = formatTime(responseDate);
            }

            // generic return true value, signifies successful completion of pre-populate
            return true;
        });
}

LF.Utilities.prePopulateQuestion = prePopulateQuestion;
