/**
 * This index merges all common utilities files into one utilities object for use as an import where necessary.
 * One can write the following statement to access all commonUtils:
 *      import commonUtils from 'path_to_PDE_Common/utils/commonUtils'
 * and then use any function from the object, for example
 *      commonUtils.getCollection();
 * The utilities functions are also mapped into the LF.Utilities namespace for compatibility.
 */

import * as commonUtils from './commonUtils';
import * as subjectUtils from './subjectUtils';
import * as prePopulateQuestion from './prePopulateQuestion';

/*Unsure if this include is necessary, just want to make sure it gets loaded into the build*/
import './loadStrings';

import {mergeObjects} from 'core/utilities/languageExtensions';

export default mergeObjects({}, commonUtils, subjectUtils, prePopulateQuestion);
