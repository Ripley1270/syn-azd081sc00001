import * as Common from './commonUtils';
import Logger from 'core/Logger';
import CurrentSubject from 'core/classes/CurrentSubject';


/**
 * getSubjectInfo attempts to fetch one or more attributes from a subject
 * @param {string|string[]} info - a string or array of strings, the attribute(s) to fetch from the subject
 *
 * @return {Promise<string>} returns a promise.
 * If 'info' is a string, resolves with string value of that attribute, or undefined if unable to determine.
 * If 'info' is an array of strings, resolves with an object, where the keys are the strings supplied in 'info' array (does not populate keys for non-string elements of 'info' array), and the values are the string value of the corresponding attribute (or undefined if unable to determine).
 * For example:
 *
 * if info = "custom10", resolves with the protocol string, such as "1"
 *
 * if info = ["custom10", "custom3", "dob"], resolves with the object { custom10: "value1", custom3: "value2", dob: "value3" }
 */
export function getSubjectInfo (info) {
    let logger = new Logger('PDE_Utilities.getSubjectInfo'),

        /**
         * Helper function for getSubjectInfo
         * getSingleSubjectItem attempts to grab attribute 'info' from subject 'subject'
         * @param {string} singleItem - the attribute to fetch from this subject
         * @param {LF.Model.Subject} subject - this subject
         * @return {Promise.<T>} resolves with the fetched attribute's value, or undefined if unable to fetch attribute
         */
        getSingleSubjectItem = (singleItem, subject) => {
            return Q().then(() => {
                if (typeof singleItem !== 'string') {
                    throw new Error('Supplied info is not a string.');
                }
                else if (!(subject instanceof LF.Model.Subject)) {
                    throw new Error('\'Subject\' object is not an instance of LF.Model.Subject');
                }
                else {
                    let retVal = subject.get(singleItem);
                    if (typeof retVal === 'undefined') {
                        throw new Error(`'${singleItem}' does not have a value for this subject.`);
                    } else {
                        return retVal;
                    }
                }
            }).catch((e) => {
                logger.warn(`Returning \'undefined\'. Fetching item failed due to following error: ${e}`);
                return undefined;
            });
        };

    return CurrentSubject.getSubject().then((subject) => {

        /*if info is an array, we'll try to fetch each item and build a return object*/
        if (Array.isArray(info)) {
            logger.info('Array input, processing each request.');
            let promiseQueue = [];
            _.forEach(info, (item) => {
                promiseQueue.push(getSingleSubjectItem(item, subject));
            });

            return Q.all(promiseQueue).then((promiseArray) => {

                /*reduce the items into an object where keys are the strings passed in and values are the corresponding items*/
                return promiseArray.reduce((retObj, item, i) => {
                    let itemName = info[i];
                    if (typeof itemName === 'string') {
                        retObj[itemName] = item;
                    } else {
                        logger.warn(`Non-string item requested, not including in output object: ${itemName}`);
                    }
                }, {});
            });
        }

        /*if info is not an array, assume it is the name of a single item and attempt to fetch, resolving with the result*/
        else {
            return getSingleSubjectItem(info, subject);
        }
    });
}

LF.Utilities.getSubjectInfo = getSubjectInfo;

/**
 * getProtocol attempts to fetch the current protocol, which as of 2.3.2 is saved as custom10 in every subject model
 * @return {Promise<string>} returns a promise that resolves with a string value representing the subject's current protocol, or rejects if unable to determine protocol.
 */
export function getProtocol () {
    let logger = new Logger('PDE_Utilities.getProtocol');
    return getSubjectInfo('custom10').then((protocol) => {
        if (typeof protocol === 'undefined') {
            let err = 'Unable to find protocol for subject';
            logger.error(err);
            throw new Error(err);
        }
        else {
            return protocol;
        }
    });
}

LF.Utilities.getProtocol = getProtocol;

/**
 * getProtocolDisplay resolves with a textual representation of the current subject's protocol
 * @return {Promise.<string>} promise resolves with a string representation of the current protocol, or the default protocol if unable to determine protocol from subject.
 */
export function getProtocolDisplay () {
    let logger = new Logger('PDE_Utilities.getProtocolDisplay'),
        protDefs = LF.StudyDesign.studyProtocol;
    return getProtocol().then((protocol) => {
        let displayProt = protDefs.protocolList[protocol];

        if (displayProt) {
            logger.info('Successfully found protocol display name');
        } else {
            logger.info('Unable to find display name for subject\'s protocol, returning default protocol for display');
            displayProt = protDefs.protocolList[protDefs.defaultProtocol];
        }
        return displayProt;
    }, (err) => {
        logger.info('Unable to determine subject protocol, returning default protocol for display', err);
        return protDefs.protocolList[protDefs.defaultProtocol];
    });
}

LF.Utilities.getProtocolDisplay = getProtocolDisplay;

export function getSubjectLanguage () {
    let preferredLang = LF.Preferred.language,
        preferredLocale = LF.Preferred.locale,
        localeID;

    if(typeof preferredLang !== 'undefined' && typeof preferredLocale !== 'undefined'){
        localeID = LF.Preferred.language + '-' + LF.Preferred.locale;
    }
    else{
        localeID = LF.StudyDesign.defaultLanguage + '-' + LF.StudyDesign.defaultLocale;
    }
       
    return localeID;
}

PDE.subjectUtils.getSubjectLanguage = getSubjectLanguage;


/*


 //Override protocol display on login screen
 (function (LoginView) {
 let oldInit = LoginView.prototype.initialize;

 LoginView.prototype.initialize = function () {
 let _this = this,
 initArgs = arguments;

 LF.Utilities.getProtocolDisplay()
 .then((protocolDisp) => {
 LF.StudyDesign.studyProtocol = protocolDisp;

 oldInit.apply(_this, initArgs);
 });
 };
 })(LoginView);

 //Override protocol display on about screen
 (function (AboutView) {
 let oldInit = AboutView.prototype.initialize;

 AboutView.prototype.initialize = function () {
 let _this = this,
 initArgs = arguments;

 LF.Utilities.getProtocolDisplay()
 .then((protocolDisp) => {
 LF.StudyDesign.studyProtocol = protocolDisp;

 oldInit.apply(_this, initArgs);
 });
 };
 })(AboutView);
 */
