import Logger from 'core/Logger';
import PDE from '../PDE';

let logger = new Logger('MomentUtils');

const   SW_DATE = 'DD MMM YYYY',
        SW_DATE_TIME = 'DD MMM YYYY HH:mm:ss',
        SW_TIME = 'SW_TIME';

export function getSWDateFormat(){
    return SW_DATE;
}

PDE.MomentUtils.getSWDateFormat = getSWDateFormat;

export function getSWDateTimeFormat(){
    return SW_DATE_TIME;
}

PDE.MomentUtils.getSWDateTimeFormat = getSWDateTimeFormat;

export function getSWTimeFormat(){
    return SW_TIME;
}

PDE.MomentUtils.getSWTimeFormat = getSWTimeFormat;

export function loadLocaleToMoment (lang) {    
    let dateResource,
        locales = moment.locales(),
        langTokens,
        EN_US = 'en-US',
        loadLanguage = (lang, dateResource) => {
            moment.defineLocale(lang, {
                months : dateResource.monthsOfYear,
                monthsShort : dateResource.monthsOfYearShort,
                weekdays : dateResource.daysOfWeek,
                weekdaysMin : dateResource.daysOfWeekShort,
                meridiem : function (hours, minutes, isLower) {
                    return hours < 12 ? dateResource.meridiem[0] : dateResource.meridiem[1];
                }
            });
        };
    
    //If English US is not loaded
    if(!_.contains(locales, EN_US)){
        //load English US Resource to moment
        langTokens = EN_US.split('-');
        dateResource = LF.strings.dates({dates : {}, locale: langTokens[0], language: langTokens[1]});
        loadLanguage(EN_US, dateResource);
    }

    locales = moment.locales();
    
    //if current language is not loaded, load it
    if(!_.contains(locales, lang)){
        langTokens = lang.split('-');
        dateResource = LF.strings.dates({dates : {}});
        loadLanguage(lang, dateResource);
    }
    
    //Set default back to en-US
    moment.locale(EN_US);
}

PDE.MomentUtils.loadLocaleToMoment = loadLocaleToMoment;

export function getStudyDateTimeFormat () {  
    let stringID = 'STUDY_DATE_TIME_FORMAT',
        format = LF.Utilities.getString(stringID);
    
    //If format string ID is not found, used default
    if(format === stringID){
        format = 'D-MMM-YYYY HH:mm';
    }
    
    return format;    
}

PDE.MomentUtils.getStudyDateTimeFormat = getStudyDateTimeFormat;

export function getStudyDateFormat () {   
    let stringID = 'STUDY_DATE_FORMAT',
        format = LF.Utilities.getString(stringID);
    
    //If format string ID is not found, used default
    if(format === stringID){
        format = 'D-MMM-YYYY';
    }
    
    return format;
}

PDE.MomentUtils.getStudyDateFormat = getStudyDateFormat;

export function getStudyTimeFormat () {   
    let stringID = 'STUDY_TIME_FORMAT',
        format = LF.Utilities.getString(stringID);
    
    //If format string ID is not found, used default
    if(format === stringID){
        format = 'HH:mm';
    }
    
    return format;
}

PDE.MomentUtils.getStudyTimeFormat = getStudyTimeFormat;

export function format (datetime, format, lang) {    
    let mDateTime;
    
    if(!datetime){
        return;
    }
    
    if(typeof lang === 'undefined'){        
        lang = PDE.subjectUtils.getSubjectLanguage();        
    }
    
    if(moment.locale() !== lang){
        PDE.MomentUtils.loadLocaleToMoment(lang);
    }
    
    if(moment.isMoment(datetime)){
        mDateTime = moment(datetime);
    }
    else {
        if(!(datetime instanceof Date)){
            datetime = new Date(datetime);
        }

        mDateTime = moment(datetime.toISOString());
    }
    
    if(typeof format === 'undefined')
    {        
        let errMsg = 'Missing Moment date time format.';
        logger.error(errMsg);
        throw new Error(errMsg);
    }
    
    mDateTime.locale(lang);
    
    return mDateTime.format(format);
}

PDE.MomentUtils.format = format;

export function formatDateTime (datetime, lang) {
    let format = PDE.MomentUtils.getStudyDateTimeFormat();

    return PDE.MomentUtils.format(datetime, format, lang);
}

PDE.MomentUtils.formatDateTime = formatDateTime;

export function formatDate (datetime, lang) {
    let format = PDE.MomentUtils.getStudyDateFormat();
    
    return PDE.MomentUtils.format(datetime, format, lang);
}

PDE.MomentUtils.formatDate = formatDate;

export function formatTime (datetime, lang) {
    let format = PDE.MomentUtils.getStudyTimeFormat();
    
    return PDE.MomentUtils.format(datetime, format, lang);
}

PDE.MomentUtils.formatTime = formatTime;
