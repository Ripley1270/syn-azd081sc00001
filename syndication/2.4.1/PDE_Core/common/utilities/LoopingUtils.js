import PDE from '../PDE';
import Logger from 'core/Logger';

let logger = new Logger('LoopingUtils');

export function getCurrentIGR (IG) {
    let IGR = PDE.QuestionnaireUtils.getQuestionnaireView().data.currentIGR[IG];
    
    if (!_.isNumber(IGR)) {
        return 0;
    } else {
        return IGR;
    }
}

PDE.LoopingUtils.getCurrentIGR = getCurrentIGR;

export function getMaxIGR (IG) {
     return PDE.QuestionnaireUtils.getQuestionnaireView().getIGR(IG);
}

PDE.LoopingUtils.getMaxIGR = getMaxIGR;

export function loadLoopScreen (IG, screenID, itemsFunction, widget)  {
    let questionnaireView = PDE.QuestionnaireUtils.getQuestionnaireView(),
        currentIGR,
        answersByIGR;

    // if backing out from edit mode, discard edited version as user backed out
    if(questionnaireView.editing){
        questionnaireView.removeLoopEvent(IG, -1);
    }

    questionnaireView.editing = null;
    questionnaireView.updateIGR(IG);
    // This determines if the an event was started and then abandoned by backing out.
    // In this case the partly answered event is removed.
    currentIGR = questionnaireView.getCurrentIGR(IG);
    answersByIGR = questionnaireView.queryAnswersByIGAndIGR(IG, currentIGR);
    if(answersByIGR.length > 0 && !LF.Utilities.isScreenshotMode()){
        // Removes partly answered event
        questionnaireView.clearLoopScreenFromStack(IG);
        questionnaireView.removeLoopEvent(IG, currentIGR);
    }
    return Q.Promise((resolve) => {
        itemsFunction(IG, widget).then((items) => {
            resolve(items);
        });
    });
}

PDE.LoopingUtils.loadLoopScreen = loadLoopScreen;

export function saveLoop (IG) {
    let questionnaireView = PDE.QuestionnaireUtils.getQuestionnaireView(),
        currentIGR,
        answersByIGR;
    // If the editing was happening, then this is the place where edited answers are
    // commited and saved into Answers collection
    if(questionnaireView.editing){
        // Remove the original version
        questionnaireView.removeLoopEvent(IG, questionnaireView.editing);
        // change the edited version to original version
        questionnaireView.changeIGR(IG, -1, questionnaireView.editing);
        // sets the flag to null which indicates the editing has been completed
        questionnaireView.editing = null;
    }
    // Update the IGR incase navigating to review screen from edit mode.
    questionnaireView.updateIGR(IG);
    //clears the stack before review screen is put on the stack.
    LF.Utilities.clearLoopScreenFromStack(IG);
    // get the current IGR
    currentIGR = questionnaireView.getCurrentIGR(IG);
    // gets all answers for the current IGR
    answersByIGR = questionnaireView.queryAnswersByIGAndIGR(IG, currentIGR);
    if(answersByIGR.length){
        // increments if the current IGR is already completed, so next available IGR becomes current
        questionnaireView.incrementIGR(IG);
    }
}

PDE.LoopingUtils.saveLoop = saveLoop;

export function beginEditLoop (IG, startScreen, IGR) {
    let questionnaireView = PDE.QuestionnaireUtils.getQuestionnaireView();
    // Preserve the IGR which is being edited
    questionnaireView.editing = IGR;
    // Copy all the Answers and Question Views from IGR which is being
    // edited to placeholder (-1)
    questionnaireView.copyIGR(IG, IGR, -1);
    // set the IGR to current and navigate to screen 3.
    // setting IGR will fetch the correct answer previously recorded.
    questionnaireView.setCurrentIGR(IG, -1);
    PDE.QuestionnaireUtils.navigateToScreen(startScreen);
}

PDE.LoopingUtils.beginEditLoop = beginEditLoop;

export function deleteAllLoops (IG) {
    let questionnaireView = PDE.QuestionnaireUtils.getQuestionnaireView(),
        IGR = questionnaireView.getIGR(IG);
    while(IGR > 0){
        PDE.LoopingUtils.deleteLoop(IG, IGR);
        questionnaireView.decrementIGR(IG);
        IGR--;
    }
}

PDE.LoopingUtils.deleteAllLoops = deleteAllLoops;

export function deleteLoop (IG, IGR) {
    let questionnaireView = PDE.QuestionnaireUtils.getQuestionnaireView(),
        isEditingLoop;
    
    if(typeof IGR === 'undefined'){
        IGR = PDE.LoopingUtils.getCurrentIGR(IG);
    }

    isEditingLoop = (IGR < 0);
    
    //If this is a editing loop
    if(isEditingLoop){
        //Remove the current loop
        questionnaireView.removeLoopEvent(IG, IGR);
        //Update the IGR to the actual loop IGR
        IGR = questionnaireView.editing;
    }
    // Removing the event, this utility function removes answers from
    // LF.Data.Questionnaire.Answers and also removes Question views instances
    // from LF.Data.Questionnaire.QuestionViews for IGR to be deleted.
    questionnaireView.removeLoopEvent(IG, IGR);
    // Ordering IGR must be done so all IGR are in sequence. this utility function
    // order igr on all Answer records and also changes the igr property on
    // all question view instances
    questionnaireView.orderIGR(IG, IGR);
    
    //Only decrement the igr if this is a editing loop
    if(isEditingLoop){
        questionnaireView.decrementIGR(IG);
    }
}

PDE.LoopingUtils.deleteLoop = deleteLoop;