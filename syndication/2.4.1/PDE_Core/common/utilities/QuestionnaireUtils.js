import PDE from 'PDE_Core/common/PDE';
import Logger from 'core/Logger';
import QuestionnaireCompletionView from 'core/views/QuestionnaireCompletionView';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Answer from 'core/models/Answer';
import TrainerData from 'trainer/logpad/TrainerData';

let logger = new Logger('QuestionnaireUtils');

/**
 * addSysvar attempts to use the given params to add a sysvar to a questionnaire view
 * @param {Object} params={} object containing the parameters for this sysvar
 * @param {PageView} [params.view=LF.router.view()] view to add sysvar to, if missing defaults to current view found
 *     via LF.router.view()
 * @param {string} [params.questionnaire_id] questionnaire id for sysvar Answer, if missing attempts to pull id from
 *     params.view
 * @param {string} params.IG the IG to send this sysvar to (typically PT, for patient data)
 * @param {string} params.sysvar the sysvar being submitted
 * @param {!(string|number|boolean)} params.response value to be submitted for this sysvar
 * @returns {boolean} true if addSysvar executed without issue
 * @throws {Error} throws error if params fails validation. Logger error includes array of messages describing failed
 *     params.
 */
export function addSysvar (params = {}) {
    //  noinspection FunctionWithMoreThanThreeNegationsJS
    let model,
        logger = new Logger('PDE.QuestionnaireUitls.addSysvar'),
        validateParams = (p) => {
            let errors = [];
            if (!p.view) {
                logger.info('View not supplied, assuming current view.');
                p.view = LF.router.view();
            }
            if (p.view instanceof QuestionnaireCompletionView) {
                p.view = p.view.questionnaire;
            }
            if (!(p.view instanceof BaseQuestionnaireView)) {
                errors.push('View is not a questionnaire, unable to add sysvar');
            }
            if (!(p.questionnaire_id || p.view.id)) {
                errors.push('Unable to determine questionnaire id');
            }
            if (!p.IG) {
                errors.push('No IG supplied');
            }
            if (!p.sysvar) {
                errors.push('No sysvar supplied');
            }
            if (p.response === null || p.response === undefined) {
                errors.push('No response supplied');
            }
            return errors.length ? errors : true;
        },
        checkParams = validateParams(params);
    if (checkParams === true) {
        model = new Answer({
            response: params.response,
            SW_Alias: `${params.IG}.${params.sysvar}`,
            questionnaire_id: params.questionnaire_id || params.view.id,
            question_id: 'SYSVAR'
        });
        if (params.view.data.dashboard) {
            model.set('instance_ordinal', params.view.data.dashboard.get('instance_ordinal'));
        }
        params.view.data.answers.add(model);
        return true;
    }
    logger.error('Attempt failed to insert a sysvar.', { errorMessages: checkParams });
    throw new Error('Attempt failed to insert a sysvar.');
}

PDE.QuestionnaireUtils.addSysvar = addSysvar;

/**
 * Get the active Questionnaire ID
 * @returns {string} Questionnaire ID
 */
export function getQuestionnaireID () {
    let questionnaireView = PDE.QuestionnaireUtils.getQuestionnaireView();

    return questionnaireView.id;
}

PDE.QuestionnaireUtils.getQuestionnaireID = getQuestionnaireID;

/**
 * Get the Questionnaire View of the active questionnaire
 * @returns {object} Questiionnaire View of the active questionaire
 */
export function getQuestionnaireView () {
    let questionnaireView = LF.Data.Questionnaire;

    if (typeof questionnaireView === 'undefined') {
        let errMsg = 'QuestionnaireView does NOT exist.';
        logger.error(errMsg);
        throw new Error(errMsg);
    } else if (!(questionnaireView instanceof LF.View.QuestionnaireView)) {
        let errMsg = 'QuestionnaireView is NOT an instance of QuestionnaireView.';
        logger.error(errMsg);
        throw new Error(errMsg);
    }

    return questionnaireView;
}

PDE.QuestionnaireUtils.getQuestionnaireView = getQuestionnaireView;

/**
 * Set the report date of the active questionnaire
 * @param {date} date to be set to the active questionnaire
 */
export function setReportDate (date) {
    PDE.QuestionnaireUtils.setDashboardAttribute('report_date', LF.Utilities.convertToDate(date));
}

PDE.QuestionnaireUtils.setReportDate = setReportDate;

/**
 * Get the active questionnaire report start date time
 * @returns {date} report start date time
 */
export function getReportStartDateTime () {
    let result = getDashboardAttribute('started', true);

    return new Date(result);
}

PDE.QuestionnaireUtils.getReportStartDateTime = getReportStartDateTime;

/**
 * Get Dashboard Attribute value
 * @param {string} attribute value to be retrieved from the Dashboard model
 * @param {boolean} [isRequired=false] flag indicates that a value if expected to be return. If value is not return, an
 *     exception will be thrown
 * @return {string} value of the attribute
 */
export function getDashboardAttribute (attribute, isRequired = false) {
    let result;

    try {
        let dashboardModel = PDE.QuestionnaireUtils.getDashboardModel();

        result = dashboardModel.get(attribute);

        if (isRequired && typeof result === 'undefined') {
            throw new Error(`"${attribute}" attribute not defined in Dashboard model.`);
        }
    } catch (err) {
        logger.error(`Unable able to retrieved "${attribute}" attribute from Dashboard model. `, err);
    }

    return result;
}

PDE.QuestionnaireUtils.getDashboardAttribute = getDashboardAttribute;

/**
 * Get Dashboard Model containing questionnaire header info (e.g. questionnaire ID, report date)
 * Note: This can only be use in an active questionnaire. If the is in screenshot mode, a fake
 * Dashboard Model will be return
 * @returns {object} Dashboard Model of the Questionnaire
 */
export function getDashboardModel () {
    let dashboardModel = LF.Data.Questionnaire.data.dashboard;

    //  If screenshot mode, create a fake DashboardModel
    if (LF.Utilities.isScreenshotMode()) {
        let now = new Date();

        dashboardModel = new LF.Model.Dashboard({
            user_id: '1',
            subject_id: TrainerData.Configs.defaults.subject_id,
            device_id: TrainerData.Configs.defaults.device_id,
            questionnaire_id: PDE.QuestionnaireUtils.getQuestionnaireID(),
            SU: PDE.QuestionnaireUtils.getQuestionnaireView().model.get('SU'),
            instance_ordinal: 1,
            started: now.ISOStamp(),
            report_date: LF.Utilities.convertToDate(now),
            phase: TrainerData.Configs.defaults.phase,
            phaseStartDateTZOffset: LF.Utilities.timeStamp(now),
            study_version: LF.StudyDesign.studyVersion,
            ink: '',
            sig_id: LF.Utilities.convertToDate(now)
        });
    }

    if (typeof dashboardModel === 'undefined') {
        let errMsg = 'Unable to get Dashboard Model! Dashboard Model is only available in an active questionnaire.';
        logger.error(errMsg);
        throw new Error(errMsg);
    }

    return dashboardModel;
}

PDE.QuestionnaireUtils.getDashboardModel = getDashboardModel;

/**
 * Navigate to a specific screen in the active questionnaire
 * @param {string} screenID
 * @returns {Q.Promise<void>}  resolved when complete
 */
export function navigateToScreen (screenID) {
    try {
        let questionnaireView = getQuestionnaireView(),
            screenStack = getScreenStack();

        screenStack.push(screenID);

        return questionnaireView.displayScreen(screenID);
    } catch (err) {
        logger.error(`Unable to navigate to screen "${screenID}": `, err);
    }

    return Q.resolve();
}

PDE.QuestionnaireUtils.navigateToScreen = navigateToScreen;
LF.Utilities.navigateToScreen = navigateToScreen;

/**
 * Get Screen Stack of the active questionnaire
 * @returns {array} Array of screen IDs
 */
export function getScreenStack () {
    try {
        let questionnaireView = getQuestionnaireView();
        return questionnaireView.screenStack;
    } catch (err) {
        let errMsg = 'Unable to get screen stack: ';
        logger.error(errMsg, err);
        throw new Error(errMsg);
    }
}

PDE.QuestionnaireUtils.getScreenStack = getScreenStack;


/**
 * Generate the Next Record ID. It create a unique ID base on the the report start time of
 * the active questionnaire. Each use generates an ID value that is an increment of 1 from the
 * previous ID generated.
 * @param {boolean} prependSubjectID flag indicates if the subject ID should be prepended to the ID generated
 * @returns {string} ID
 */
export function generateNextRecordID (prependSubjectID) {
    let view = PDE.QuestionnaireUtils.getQuestionnaireView(),
        recordID,
        pad = function (num) {
            return `0000000000000${num}`.substr(-13); // V682: Unique ID must be 13 digits
        };


    // If ID counter is not set yet, initialize ID counter
    if (typeof view._recordIDCounter === 'undefined') {
        view._recordIDCounter = PDE.QuestionnaireUtils.getReportStartDateTime().getTime();
    } else {
    // Else, icrement ID counter
        view._recordIDCounter++;
    }

    recordID = pad(view._recordIDCounter);

    if (prependSubjectID) {
        // Prepend Subject ID to ID value
        let subjectModel = PDE.QuestionnaireUtils.getQuestionnaireView().subject,
            subjectID = subjectModel.get('subject_number');

        recordID = `${subjectID}${recordID}`;
    }

    return recordID.toString();
}

PDE.QuestionnaireUtils.generateNextRecordID = generateNextRecordID;

/**
 * Convert a date object to a string base of the standard StudyWorks date format
 * @param {Date} date (or moment) to be converted
 * @returns {string} date in string
 */
export function buildStudyWorksDateString (date) {
    let swMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    if (moment.isMoment(date)) {
        date = new Date(date.toString());
    }

    let swDate = (date.getDate() < 10) ? `0${date.getDate()}` : date.getDate(),
        swMonth = swMonths[date.getMonth()],
        swYear = date.getFullYear();
    return `${swDate} ${swMonth} ${swYear}`;
}

PDE.QuestionnaireUtils.buildStudyWorksDateString = buildStudyWorksDateString;

/**
 * Convert a date time object to a string base of the standard StudyWorks date format
 * @param {Date} date (or moment) to be converted
 * @param {boolean} includeSeconds flag indicates if return value include the actual seconds instead of rounded to 0.
 * @returns {string} date time in string
 */
export function buildStudyWorksDateTimeString (date, includeSeconds) {
    let swMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    if (moment.isMoment(date)) {
        date = new Date(date.toString());
    }

    let swDate = (date.getDate() < 10) ? `0${date.getDate()}` : date.getDate(),
        swMonth = swMonths[date.getMonth()],
        swYear = date.getFullYear(),
        swHours = (date.getHours() < 10) ? `0${date.getHours()}` : date.getHours(),
        swMinutes = (date.getMinutes() < 10) ? `0${date.getMinutes()}` : date.getMinutes(),
        swSeconds = (date.getSeconds() < 10) ? `0${date.getSeconds()}` : date.getSeconds(),
        swTime;

    if (includeSeconds) {
        swTime = `${swHours}:${swMinutes}:${swSeconds}`;
    } else {
        swTime = `${swHours}:${swMinutes}:00`;
    }

    return `${swDate} ${swMonth} ${swYear} ${swTime}`;
}

PDE.QuestionnaireUtils.buildStudyWorksDateTimeString = buildStudyWorksDateTimeString;
