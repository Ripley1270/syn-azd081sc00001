/*
 * PDE_TODO:
 * import files that don't contribute to assets object, but need to ensure the content executes
 * for example, a file that assigns a function into the global LF namespace
 */
// import './exampleBranchingFunctions';
// import './exampleUtilities';
// import './exampleScheduleFunctions';
import './actions';
import './branching';
import './expressions';
import './PDE';
import './utilities';
import './webService';
import './depreciated_support_common';
import './dynamicText';

import { mergeObjects } from 'core/utilities/languageExtensions';

let studyDesign = {};

/*
 * PDE_TODO:
 * Collect and merge studyDesign objects
 */
// import diary from './exampleDiary';
// import rules from './exampleRules';
// import messages from './exampleMessages';
// import templates from './exampleTemplates';
import SD from './study-design';
import rules from './rules';

studyDesign = mergeObjects(studyDesign, SD, rules /* , diary, rules, templates, dynamicText */);


// export the assets object
export default { studyDesign };
