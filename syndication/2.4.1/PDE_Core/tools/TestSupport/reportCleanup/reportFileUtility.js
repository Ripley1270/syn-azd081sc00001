/**
 * This file's original purpose is to make small alterations to unit test xml output to prevent errors during the
 * process of generating html. It is based on Udit Adhikari's fileUtility for json cleanup
 * @author Mason Blaschak
 * @email mason.blaschak@ert.com
 */

var fs = require('fs');
var path = require('path');

/**
 * This function calls other helper functions to replace unnecessary html tags or shorten them
 * @param fileContent - file content in string
 * @returns {*} - transformed cleaned up json file content
 */
function replaceFileContent (fileContent, params) {
    var replacedFileContent = fileContent;

    if(params && typeof params.newTitle === 'string') {
        replacedFileContent = replaceReportTitle(replacedFileContent, params.newTitle);
        replacedFileContent = replaceReportHeader(replacedFileContent, params.newTitle);
    } else {
        throw new Error('expected a title to be defined in grunt task options');
    }
    // replacedFileContent = fixupTestSuiteName(replacedFileContent);
    replacedFileContent = replacedFileContent.trim();
    return replacedFileContent;
}

/**
 * This function replaces ' &lt;- ' (an xml representation of ' <- ')
 * with ' -- ' to avoid a parsing error when generating html with junit-viewer
 * @param data - file line content to clean.
 * @returns {string} - return string without &nbsp; between two <br>
 */
function replaceReportTitle (data, newTitle) {
    var strToReplace = /<title>\w+<\/title>/;
    var replacement = `<title>${newTitle}</title>`;
    data = data.toString();
    var replacedString = data.split(strToReplace).join(replacement);
    return replacedString;
}
function replaceReportHeader (data, newTitle) {
    var strToReplace = /<h1 class="u-full-width">junit</;
    var replacement = `<h1 class="u-full-width">${newTitle}<`;
    data = data.toString();
    var replacedString = data.split(strToReplace).join(replacement);
    return replacedString;
}

/* /!**
 * This function replaces ' &lt;- ' (an xml representation of ' <- ')
 * with ' -- ' to avoid a parsing error when generating html with junit-viewer
 * @param data - file line content to clean.
 * @returns {string} - return string without &nbsp; between two <br>
 *!/
function fixupTestSuiteName (data) {
    var reg = /<testsuite name="(.*?)" package="(.*?)"/g,
        match,
        retString = data;
    while (match = reg.exec(data)) {
        var oldName = match[1],
            newName = match[2];
        retString = retString.replace('testsuite name="' + oldName + '"', 'testsuite name="' + newName + '"');
    }
    return retString;
}*/


// define all export functions
module.exports.replaceFileContent = replaceFileContent;
module.exports.replaceReportTitle = replaceReportTitle;
module.exports.replaceReportHeader = replaceReportHeader;

// module.exports.fixupTestSuiteName = fixupTestSuiteName;
