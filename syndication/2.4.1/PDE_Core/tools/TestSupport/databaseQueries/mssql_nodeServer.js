/* eslint-disable no-console */
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const Q = require('q');
const sql = require('mssql');


app.use(cors());
app.use(bodyParser.json());

const createIGQuery = (inputXMLstring) => {
    return `
Declare @InputXML as xml = N'${inputXMLstring}'
Declare @OutputXML as xml

Set @OutputXML =
(
	SELECT cast(Main.xmlOutput as XML) FROM
	(
		select  ig.value('(@name)[1]', 'varchar(100)') name
				,CASE WHEN TableExists.TABLE_NAME IS NOT NULL THEN '1' ELSE '0' END pass
		from (select 1 Blank) Tbl
		cross apply @InputXML.nodes('ROOT/IG') Igs(ig)
		LEFT JOIN ( SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE Table_Name like '%IG%' ) TableExists on TableExists.TABLE_NAME = 'ig_' + ig.value('(@name)[1]', 'varchar(100)')
		FOR XML RAW('IG'), ROOT
	) MAIN(xmlOutput)
)

Select @OutputXML as output
`;
};

const createITQuery = (inputXMLstring) => {
    return `
Declare @InputXML as xml = N'${inputXMLstring}'
Declare @OutputXML as xml

Set @OutputXML =
(
Select 
	name, pass
	,CAST(ITNodes as XML)
From
(
	select Distinct 
		  ig.value('(@name)[1]', 'varchar(100)') name
		, CASE WHEN mi.krig IS NOT NULL THEN 1 else 0 end pass
		, ChildNode.ChildNode as ITNodes
	from (select 1 Blank) Tbl
	cross apply @InputXML.nodes('ROOT/IG') Igs(ig)
	left join ( select DISTINCT KRIG, KRIT from meta_item ) mi on mi.krig = ig.value('(@name)[1]', 'varchar(100)')  
	OUTER APPLY
	(
		Select it.value('(@name)[1]', 'varchar(100)') name
			, CASE WHEN mic.krit IS NOT NULL THEN 1 else 0 end as pass
		from (select 1 Blank) Tbl
		cross apply @InputXML.nodes('ROOT/IG/IT') It(IT)
		left join ( select DISTINCT KRIG, KRIT from meta_item ) mic on mic.krit = it.value('(@name)[1]', 'varchar(100)') and mic.krig = ig.value('(@name)[1]', 'varchar(100)') 
		where IT.value('(../@name)[1]', 'varchar(100)') = ig.value('(@name)[1]', 'varchar(100)')
		for XML RAW ('IT')
	) as ChildNode (ChildNode)
) as Main
FOR XML RAW ('IG'), ROOT
)

Select @OutputXML as output`;
};

const createSUQuery = (inputXMLstring) => {
    return `
Declare @InputXML as xml = N'${inputXMLstring}'
Declare @OutputXML as xml

Set @OutputXML =
(
	SELECT cast(Main.xmlOutput as XML) FROM
	(
		select  ig.value('(@name)[1]', 'varchar(100)') name
				,CASE WHEN IGExists.krsu IS NOT NULL THEN '1' ELSE '0' END pass
		from (select 1 Blank) Tbl
		cross apply @InputXML.nodes('ROOT/SU') Igs(ig)
		LEFT JOIN ( SELECT Distinct krsu FROM meta_itemgroup ) IGExists on IGExists.krsu = ig.value('(@name)[1]', 'varchar(100)')
		FOR XML RAW('SU'), ROOT
	) MAIN(xmlOutput)
)

Select @OutputXML as output`;
};

app.post('/IG', (req, res) => {
    let body = '';
    req.on('data', (data) => {
        body += data;
    });
    req.on('end', () => {
        body = JSON.parse(body);

        // console.log(body);
        Q(sql.connect(body.config)).then((pool) => {
            return pool.request()
                .query(createIGQuery(body.inputXML));
        }).then((result) => {
            if (result.recordset[0] && result.recordset[0].output) {
                res.send(result.recordset[0].output);
            } else {
                res.send('failure:output');
            }
        }).catch((err) => {
            // console.log('I failed to connect but caught it');
            // console.log(err);
            res.send(`failure:query  ====${JSON.stringify(err)}`);
        }).finally(() => {
            sql.close();
        });
    });
});

app.post('/IT', (req, res) => {
    let body = '';
    req.on('data', (data) => {
        body += data;
    });
    req.on('end', () => {
        body = JSON.parse(body);

        // console.log(body);
        Q(sql.connect(body.config)).then((pool) => {
            return pool.request()
                .query(createITQuery(body.inputXML));
        }).then((result) => {
            if (result.recordset[0] && result.recordset[0].output) {
                res.send(result.recordset[0].output);
            } else {
                res.send('failure:output');
            }
        }).catch((err) => {
            // console.log('I failed to connect but caught it');
            // console.log(err);
            res.send(`failure:query  ====${JSON.stringify(err)}`);
        }).finally(() => {
            sql.close();
        });
    });
});

app.post('/SU', (req, res) => {
    let body = '';
    req.on('data', (data) => {
        body += data;
    });
    req.on('end', () => {
        body = JSON.parse(body);

        // console.log(body);
        Q(sql.connect(body.config)).then((pool) => {
            return pool.request()
                .query(createSUQuery(body.inputXML));
        }).then((result) => {
            if (result.recordset[0] && result.recordset[0].output) {
                res.send(result.recordset[0].output);
            } else {
                res.send('failure:output');
            }
        }).catch((err) => {
            // console.log('I failed to connect but caught it');
            // console.log(err);
            res.send(`failure:query  ====${JSON.stringify(err)}`);
        }).finally(() => {
            sql.close();
        });
    });
});

const server = app.listen(1337, () => {
    console.log('Server is running..');
});
