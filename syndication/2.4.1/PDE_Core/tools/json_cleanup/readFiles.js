/**
 * This file will read the content of all json files and perform transformations on it.
 * The transformation should clean up unnecessary html tags or replace them with shorter html tags.
 * @author - Udit Adhikari
 * @email - udit.adhikari@ert.com
 */

var fs = require('fs');
var path = require('path');
var fileUtility = require('./fileUtility');


/**
 * This function iterates over all json files and calls helper functions
 * to perform clean up transformation on them.
 * After cleaning up the json, this function will also call helper functions to find all color
 * codes in a language file and build a output log file with file name and color codes.
 * This can be used by PDE to cross check with SDAs on whehter this was intended.
 *
 * @param directoryPath - parent directory to all language json files
 */
function cleanFile(filePath) {

    var fileOutputPath;
    var colorsFoundArr = [];
    var colorsFoundStr;

    //repalce file content
    var fileContent = readFileContent(filePath);
    var replacedFileContent = fileUtility.replaceFileContent(fileContent);

    //write the file
    fileOutputPath = filePath;
    writeFileContent(fileOutputPath, replacedFileContent);

    //find colors in file
    var colorFoundForFile = fileUtility.findColors([filePath], replacedFileContent);

    return [replacedFileContent, colorFoundForFile];
}

/**
 * This function will get all filename in a directory
 * @param path - path to the parent directory of all json files
 * @returns {*} - array of file names in the directory
 */
function getAllFilesInPath(path) {
    var fileNames = fs.readdirSync(path);
    return fileNames;
}

/**
 * This function will get the content of a file
 * @param filePath - path to the file
 * @returns {*} - returns the content of the file as a string.
 */
function readFileContent(filePath){
    var fileContent = fs.readFileSync(filePath, 'utf8');
    return fileContent;
}

/**
 * This function will write to a file
 * @param filePath - path of the file to be written
 * @param fileContent - data to be writte to file
 */
function writeFileContent(filePath, fileContent) {
    fs.writeFileSync(filePath, fileContent);
}

module.exports.cleanFile = cleanFile;
