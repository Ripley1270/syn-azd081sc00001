// import the PDE_Core common assets
import PDE_Core_CommonAssets from 'PDE_Core/common';

/*
 * PDE_TODO:
 * import files that don't contribute to assets object, but need to ensure the content executes
 * for example, a file that assigns a function into the global LF namespace
 */
// import './exampleBranchingFunctions';
// import './exampleUtilities';
// import './exampleScheduleFunctions';
import './trainer';

import { mergeObjects } from 'core/utilities/languageExtensions';

let studyDesign = {};

/*
 * PDE_TODO:
 * Collect and merge studyDesign objects
 */
// import diary from './exampleDiary';
// import rules from './exampleRules';
// import messages from './exampleMessages';
import './templates';
import './widgets';
// import dynamicText from './exampleDynamicText';
import SD from './study-design';
import rules from './rules';

studyDesign = mergeObjects(studyDesign, SD, rules /* , diary, templates, dynamicText*/);

// create the PDE_Core handheld assets object
let PDE_Core_HHAssets = { studyDesign };

// import the PDE_CoreDiaries for handheld
import PDE_CoreDiares from './PDE_CoreDiaries';

// combine and export the assets for PDE_Core
export default mergeObjects(PDE_Core_CommonAssets, PDE_Core_HHAssets, PDE_CoreDiares);
