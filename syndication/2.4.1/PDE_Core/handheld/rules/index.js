import syncRules from './PDE_Sync_rules';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects({}, syncRules);
