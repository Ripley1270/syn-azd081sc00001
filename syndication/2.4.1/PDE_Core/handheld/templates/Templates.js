import Templates from 'core/resources/Templates';

// Templates for PDE Widgets

Templates.set([
    // PDEListBox
    {
        name: 'Container',
        namespace: 'PDEListBox',
        template: '<div id="{{ id }}" class="PDEListBox"><ul id="{{ id }}_Items" class="PDEListBox_Items" data-role="listview"></ul>'
    },
    {
        name: 'OneLineItems',
        namespace: 'PDEListBox',
        template: '<li><div class="ui-grid-a">' +
        '<div>{{ text0 }}</div>' +
        '</div></li>' +
        '<div><hr class="style-one"></div>'
    },
    {
        name: 'TwoLineItems',
        namespace: 'PDEListBox',
        template: '<li><div class="ui-grid-a">' +
        '<div>{{ text0 }}</div>' +
        '</div></li>' +
        '<div><hr class="style-one"></div>'
    },
    // Scrollable Review Screen
    {
        name: 'Container',
        namespace: 'PDEScrollableReviewScreen',
        template: '<ul id="{{ id }}_Items" class="reviewScreen_Items" data-role="listview">' +
        '</ul>' +
        '<hr />' +
        '<ul id="{{ id }}_Buttons" class="reviewScreen_Buttons" data-role="listview"></ul>' +
        '<p class="edit-instructions">{{editInstructions}}</p>'
    },
    {
        name: 'OneLineItems',
        namespace: 'PDEScrollableReviewScreen',
        template: '<li class="OneLineItems">' +
        '<div class="ui-grid-a">' +
        '   <div class="ui-block-a">{{ text0 }}</div>' +
        '</div></li>' +
        '<div><hr class="style-one"></div>'
    },
    {
        name: 'TwoLineItems',
        namespace: 'PDEScrollableReviewScreen',
        template: '<li class="TwoLineItems">' +
        '<div class="ui-grid-a">' +
        '   <div class="ui-block-a">{{ text0 }}</div>' +
        '   <div class="ui-block-b">{{ text1 }}</div>' +
        '</div></li>' +
        '<div><hr class="style-one"></div>'
    }
], {
    add: true,
    merge: true,
    remove: false
});

LF.Resources.Templates = Templates;
// A shortcut to make displaying templates easier.
LF.templates = LF.Resources.Templates;

export default Templates;
