import RadioButton from 'core/widgets/RadioButton';

export default class PDERadioButton extends RadioButton {
    //noinspection JSMethodCanBeStatic
    /**
     * The class of the root element.
     * @returns {String}
     */
    get className () {
        return 'PDERadioButton';
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise} resolved when element is finished being rendered
     */
    render () {
        return Q()
            .then(() => this.setupAnswers())
            .then(() => this.buildHTML('radio'))
            .then(() => Q.Promise((resolve) => {
                this.$el.appendTo(this.parent.$el)
                    .ready(() => {
                        resolve();
                    });
            }))
            .then(() => {
                //[PDE] If distributedAnswerHeight is NOT set OR it is set to truthy
                if(typeof this.model.get('distributedAnswerHeight') === 'undefined' || 
                    this.model.get('distributedAnswerHeight')){
                    //Redistribute Answer Height    
                    return this.setLabelHeight();
                }
            })
            .then(() => {
                this.delegateEvents();
            })
            .then(() => {
                let promises = [],
                    answer = this.answer ? this.answer.get('response') : false,
                    modelId = this.model.get('id');

                if (answer) {
                    _(this.model.get('answers')).each((item, index) => {
                        if (answer === item.value) {
                            promises.push(
                                new Q.Promise((resolve) => {
                                    this.$(`input[id=${modelId}_radio_${index}]`)
                                        .attr('checked', true)
                                        .closest('.btn')
                                        .addClass('active')
                                        .ready(() => {
                                            resolve();
                                        });
                                })
                                .then(() => {
                                    // flush queue so trigger handler will have run
                                })
                            );
                        }
                    });
                }

                return Q.allSettled(promises);
            });
        }
}

window.LF.Widget.PDERadioButton = PDERadioButton;
