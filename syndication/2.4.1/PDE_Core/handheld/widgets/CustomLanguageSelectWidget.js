import LanguageSelectWidget from 'core/widgets/LanguageSelectWidget';

export default class CustomLanguageSelectWidget extends LanguageSelectWidget {
    setItems () {
        return super.setItems().then(() => {
            // Hardcode to site. This will not work if their are multiple role with different language list
            let roleID = 'site',
                oldLocalizedResources = this.model.get('items'),
                selectedRoleSupportedLanguages,
                newLocalizedResources;

            if (roleID) {
                let roleModel = LF.StudyDesign.roles.findWhere({ id: roleID });

                if (roleModel) {
                    selectedRoleSupportedLanguages = roleModel.get('supportedLanguages');
                }

                if (selectedRoleSupportedLanguages) {
                    newLocalizedResources = oldLocalizedResources.filter((language) => {
                        return _.contains(selectedRoleSupportedLanguages, `${language.language}-${language.locale}`);                    });
                }
            }

            this.model.set('items', newLocalizedResources);
        });
    }
}

window.LF.Widget.LanguageSelectWidget = CustomLanguageSelectWidget;
