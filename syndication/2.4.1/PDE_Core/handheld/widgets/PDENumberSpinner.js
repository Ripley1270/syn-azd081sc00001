import NumberSpinner from 'core/widgets/NumberSpinner';
import NumberSpinnerInput from 'core/widgets/input/NumberSpinnerInput';
import Logger from 'core/Logger';

let logger = new Logger('PDENumberSpinner');

let {Model} = Backbone;

const DEFAULT_CLASS_NAME = 'PDENumberSpinner';

class PDENumberSpinner extends NumberSpinner {

    /**
     * default class name.
     */
    get defaultClassName () {
        return DEFAULT_CLASS_NAME;
    }

    /**
     * Place spinners into place.  For the NumberSpinner implementation, they go into elements
     * with the CSS class 'number-spinner-container'.
     */
    injectSpinnerInputs () {
        let foundSpinner = false,
            that = this;

        //jscs:disable requireArrowFunctions
        that.$modal.find('.number-spinner-container').each(function (i) {
            let spinnerTemplate = that.getSpinnerTemplate(i),
                spinnerItemTemplate = that.getSpinnerItemTemplate(i),
                spinnerElement,
                spinnerInputOptions;

            spinnerElement = that.renderTemplate(spinnerTemplate, {});

            // For Item, delay evaluate, so we can replace placeholders with actual values and display values
            spinnerInputOptions = that.model.get('spinnerInputOptions') || [];

            if(spinnerInputOptions[0].maxFunction){
                let func = PDE.commonUtils.getPath(spinnerInputOptions[0].maxFunction);

                if (_.isFunction(func)) {
                    spinnerInputOptions[0].max = func();
                }
            }

            if(spinnerInputOptions[0].minFunction){
                let func = PDE.commonUtils.getPath(spinnerInputOptions[0].minFunction);

                if (_.isFunction(func)) {
                    spinnerInputOptions[0].min = func();
                }
            }

            if(that.answer){
                let response = that.answer.get('response');

                //If answer is out of range clear out existing response
                if(Number(spinnerInputOptions[i].min) > Number(response) ||
                    Number(response) > Number(spinnerInputOptions[i].max)) {
                    that.answers.remove(that.answer);
                    delete that.answer;
                    that.value = null;
                    that.completed = false;
                }
            }

            foundSpinner = true;
            that.spinners[i] = new NumberSpinnerInput(
                _.extend({
                    model: new Model(),
                    parent: this,
                    template: spinnerTemplate,
                    itemTemplate: spinnerItemTemplate
                }, spinnerInputOptions[i] || {}
                )
            );
        });

        //jscs:enable requireArrowFunctions
        if (!foundSpinner) {
            throw new Error(`Invalid template for NumberSpinner widget.  Expected an element with
                "number-spinner-container" class`);
        }
    }

    render () {
        //Fix this.answer not being set properly if the answer model is initialize outside of the widget (e.g. editing a screen in looping)
        if(this.answers && this.answers.length > 0) {
            if(typeof this.answer === 'undefined'){
                this.answer = this.answers.first();
            }
        }

        super.render();
    }
}

window.LF.Widget.PDENumberSpinner = PDENumberSpinner;
export default PDENumberSpinner;
