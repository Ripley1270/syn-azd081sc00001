import ReviewScreen from 'core/widgets/ReviewScreen';

export default class PDEListBox extends ReviewScreen {
    
    constructor (options) {
        super(options);

        this.container = 'PDEListBox:Container';
        this.itemsTemplate = 'PDEListBox:OneLineItems';

        this.container = this.templates.container || this.container;
        this.reviewScreenItems = this.templates.itemsTemplate || this.itemsTemplate;
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise} resolved when the element has been rendered.
     */
    render () {
        // ID was originally being set to 'reviewScreen' rather the ID in the model.  Changed to allow the unit tests to pass --bmj
        let widgetView = LF.templates.display(this.container, {id: this.model.get('id')}),
            screenFunc = this.model.get('screenFunction');

            //If screen func is an array (due to function being evaluated in base widget)
            if(_.isArray(screenFunc)){
                //Wrap result in a function
                let result = screenFunc;
                screenFunc = () => {
                    return result;
                };
            }
            else if(!_.isFunction(screenFunc) && typeof screenFunc === 'string'){
                screenFunc = PDE.commonUtils.getPath(screenFunc);
            }

        return Q()
            .then(() => {

                this.$el.empty();
                this.$el.append(widgetView);
            })
            .then(() => {
                // passing empty array as nothing to exclude since no item is selected at this point.
                return this.addDefaultButtons([]);
            })
            .then(() => {
                this.$el.appendTo(this.parent.$el);
                this.$el.trigger('create');
            })
            .then(() => this.processScreenFunction(screenFunc))
            .then(() => {
                //If answer exist, select item base on response
                if(this.answers && this.answers.length > 0){
                    let answer = this.answers.first(),
                        response = answer.get('response');

                    //if widget's answer not set (due to answer model being set dynamically)
                    if(typeof this.answer === 'undefined'){
                        //Set widget's answer
                        this.answer = answer;    
                    }

                    if(response){
                        let item = this.$el.find(`li[data-id="${response}"]`);
                        
                        if(item.length > 0){
                            item.addClass('selected');
                            this.activeItem = item;
                        }
                    }
                }
                else {
                    this.completed = false;
                }
            })
            .then(() => {
                this.delegateEvents();
            });

    }

    /**
     * Selects an item on the screen and sets it as an active item.
     * Displays all buttons configured for the item.
     * @param {Event} e The event args
     * @returns {Q.Promise<jQuery>} Promise resolving with containing reference to jQuery container for $el
     */
    itemSelected (e) {

        let targetItem = this.$(e.currentTarget),
            value = this.$(e.currentTarget).attr('data-id');

        if (targetItem.hasClass('selected')) {          
            //Do nothing
        } else {
            // deselect any previously selected item
            if (this.activeItem) {
                this.activeItem.removeClass('selected');
            }
            // set new active item
            this.activeItem = targetItem;
            targetItem.addClass('selected');
        }

        this.$el.trigger('create');

        if (!this.answers.size() || typeof this.answer === 'undefined') {
            this.answer = this.addAnswer();
        }

        return this.respondHelper(this.answer, value);
    }

    /**
     *  Sets up the configured screenFunction property for the widget.
     *  @param {Function} screenFunc The screenFunction to be called
     *  @returns {Q.Promise<array>} Resolved Promise with an array of items to display.
     */
    processScreenFunction (screenFunc) {
        let that = this;

        return Q()
            .then(() => screenFunc.call(that))
            .then((items) => {
                if (items !== null) {
                    _(items).each((item) => {
                        that.reviewScreenList.push(item);
                        that.$(`#${that.model.get('id')}_Items`).append(that.addItem(item));
                    });
                }
            });
    }
}

PDEListBox.prototype.className = 'PDEListBox';

window.LF.Widget.PDEListBox = PDEListBox;
export default PDEListBox;