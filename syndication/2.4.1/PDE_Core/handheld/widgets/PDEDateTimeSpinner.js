import DateTimeSpinner from 'core/widgets/DateTimeSpinner';

export default class PDEDateTimeSpinner extends DateTimeSpinner {

    get defaultDisplayFormat () {
        return PDE.MomentUtils.getStudyDateTimeFormat();
    }

    get defaultTimeFormat () {
        return PDE.MomentUtils.getStudyTimeFormat();
    }

    get defaultDateFormat () {
        return PDE.MomentUtils.getStudyDateFormat();
    }

    get defaultDateTimeFormat () {
        return PDE.MomentUtils.getStudyDateTimeFormat();
    }

    constructor (options){
        super(options);

        let locale = PDE.subjectUtils.getSubjectLanguage();

        //Load core date resources to moment
        PDE.MomentUtils.loadLocaleToMoment(locale);
        this.moment = moment().locale(locale);
    }

    render () {
        //Fix this.answer not being set properly if the answer model is initialize outside of the widget (e.g. editing a screen in looping)
        if(this.answers && this.answers.length > 0) {
            if(typeof this.answer === 'undefined'){
                this.answer = this.answers.first();
            }
        }
        super.render();
    }
}

window.LF.Widget.PDEDateTimeSpinner = PDEDateTimeSpinner;
