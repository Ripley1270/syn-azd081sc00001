import ReviewScreen from 'core/widgets/ReviewScreen';
import Logger from 'core/Logger';

let logger = new Logger('PDEScrollableReviewScreen');

export default class PDEScrollableReviewScreen extends ReviewScreen {

    get defaultClassName () {
        return 'PDEScrollableReviewScreen';
    }
    //TODO: this is a copy of the ReviewScreen.js render function, with the ability to add resources to the template's context
    doRender (headerStrings) {
        let that = this;
        return Q()
            .then(() => {
                let _this          = that,
                    widgetView     = LF.templates.display(this.container, headerStrings),
                    screenFunction = this.model.get('screenFunction'),
                    modelId        = that.model.get('id'),
                    selectableEntries;

                //If screen func is an array (due to function being evaluated in base widget)
                if(_.isArray(screenFunction)){
                    //Wrap result in a function
                    let result = screenFunction;
                    screenFunction = () => {
                        return Q.resolve(result);
                    };
                }
                else if(!_.isFunction(screenFunction) && typeof screenFunction === 'string'){
                    screenFunction = PDE.commonUtils.getPath(screenFunction) ||
                                    LF.Widget.ReviewScreen[screenFunction];
                }

                that.$el.empty();
                that.$el.append(widgetView);
                if(_.isFunction(screenFunction)){
                    // calls the Screen function to get the Item list to display
                    screenFunction(that)
                        .then((items) => {
                            _this.reviewScreenList = items;
                            _(_this.reviewScreenList).each(function (item) {
                                // create and append items separately
                                _this.$(`#${modelId}_Items`).append(_this.addItem(item));
                            });
                            selectableEntries = !!(_.find(items, function (item) {
                                return (item.id !== '-1');
                            }));
                            if(selectableEntries){
                                that.$el.find('.edit-instructions').show();
                            }else{
                                that.$el.find('.edit-instructions').hide();
                            }
                            // passing empty array as nothing to exclude since no item is selected at this point.
                            that.addDefaultButtons([]);
                            that.$el.appendTo(that.parent.$el);
                            that.delegateEvents();
                            that.$el.trigger('create');
                        });
                }else{
                    logger.error('Screen Function not found in the Widget Configuration or its not defined.');
                    throw new Error('Screen Function not found in the Widget Configuration or its not defined.');
                }
            });
    }

    /**
     * Responsible for displaying the widget.
     * @returns '{@link LF.Widget.ScrollableReviewScreen}'
     */
    render () {
        let that = this;
        return Q()
            .then(() => {
                let config           = that.model.get('config'),
                    visibleRows      = config ? config.visibleRows : 4,
                    itemHeight,
                    itemList,
                    buttonList,
                    currentRows,
                    headers          = that.model.get('headers'),
                    editInstructions = that.model.get('editInstructions'),
                    headerResources  = [],
                    headerStrings    = {id : that.model.get('id')};
                that.activeItem = null;
                //Look for header strings to add to context
                if(_.isArray(headers)){
                    headerResources = headers;
                }
                //If edit instructions, add to end of header strings
                if(editInstructions){
                    headerResources.push(editInstructions);
                }else{
                    headerStrings['editInstructions'] = '';
                }
                LF.getStrings(headerResources, () => {
                }, {namespace : that.parent.parent.id})
                    .then((strings) => {
                        _(strings).each(function (str, index) {
                            if(!editInstructions || index < (strings.length - 1)){
                                headerStrings[`header${index}`] = str;
                            }else if(editInstructions && index === (strings.length - 1)){
                                //Add instructions
                                headerStrings['editInstructions'] = str;
                            }
                        });
                        //LF.Widget.ReviewScreen.prototype.render.call(this);
                        that.doRender(headerStrings)
                            .then(() => {
                                itemList = that.$('.reviewScreen_Items');
                                itemList.css('overflow-y', 'auto');
                            });
                    });
            });
    }

    addItem (item) {
        //kgonsalves: allow custom classes
        if(!item.classes){
            item.classes = '';
        }
        let strings = {};
        if(_.isArray(item.text)){
            _(item.text).each(function (str, index) {
                strings['text' + index] = str;
            });
        }else{
            logger.error('Text for the Review Screen Item is not configured in an Array.');
            throw new Error('Text for the Review Screen Item is not configured in an Array.');
        }
        return $(LF.templates.display(this.reviewScreenItems, strings)).addClass('item').addClass(item.classes).attr('data-id', item.id);
    }

    itemSelected (e) {
        //kgonsalves: do not highlight unselectable data
        let targetItem = this.$(e.currentTarget),
            id         = targetItem.data('id');
        if(id > 0){
            LF.Widget.ReviewScreen.prototype.itemSelected.call(this, e);
            if(this.activeItem){
                this.$el.find('.edit-instructions').hide();
            }else{
                this.$el.find('.edit-instructions').show();
            }
        }
        return Q().then(() => {
            return this.$el.trigger('create');
        });
    }

    /**
     * Calls the configured function when an button is clicked.
     * Function must be available in LF.Widget.ReviewScreen namespace
     * @param {Event} e The event args.
     * @returns {Q.Promise<void>}
     */
    handleAction (e) {
        let id = this.$(e.currentTarget).data('id'),
            selectedBtn = _.find(this.model.get('buttons'), (btn) => btn.id.toString() === id.toString()),
            func = selectedBtn.actionFunction;

        return Q()
            .then(() => {
                if (func) {
                    func = PDE.commonUtils.getPath(func) ||
                            LF.Widget.ReviewScreen[func] ||
                            func;
                }

                if (_.isFunction(func)) {
                    // This will/should return a Promise
                    return func({
                        question: this.parent,
                        button: selectedBtn,
                        item: this.activeItem
                    });
                } else {
                    throw new Error('Button Function not found in the widget Configuration.');
                }
            });
    }

    /**
     * Add all buttons which has availability property.
     * @param {Array} exclude An array with Button Ids that should not be rendered.
     * @example this.addDefaultButtons([3, 4]);
     * @returns {Q.Promise<void>}
     */
    addDefaultButtons (exclude) {

        let _this = this,
            allBtns = this.model.get('buttons') || [],
            btnAddPromiseFactories = [];

        // create and append default buttons
        _(allBtns).each((btn) => {
            let availabilityFunc;

            if(btn.availability){
                availabilityFunc = PDE.commonUtils.getPath(btn.availability) ||
                                    LF.Widget.ReviewScreen[btn.availability] ||
                                    btn.availability;
            }

            if ((btn.availability === 'always' && _.indexOf(exclude, btn.id) === -1) || LF.Utilities.isScreenshotMode()) {
                _this.selectable = true;
                btnAddPromiseFactories.push(() => {
                    return _this.addBtn(btn);
                });
            } else if (_.isFunction(availabilityFunc) && _.indexOf(exclude, btn.id) === -1) {
                btnAddPromiseFactories.push(() => {
                    return new Q.Promise((resolve) => {
                        availabilityFunc(_this)
                            .then((result) => {
                                if (result) {
                                    _this.selectable = true;
                                    _this.addBtn(btn).then(resolve).done();
                                } else {
                                    resolve();
                                }
                            });
                    });
                });
            }
        });

        // Run these promises in order.
        return btnAddPromiseFactories.reduce(Q.when, Q());
    }
}

/**
 * The default class of PDEScrollableReviewScreen widget.
 * @readonly
 * @type String
 * @default 'PDEScrollableReviewScreen'
 */
PDEScrollableReviewScreen.prototype.className = 'PDEScrollableReviewScreen';

export default PDEScrollableReviewScreen;
window.LF.Widget.PDEScrollableReviewScreen = PDEScrollableReviewScreen;
