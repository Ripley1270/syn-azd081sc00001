import TrainerData from 'trainer/logpad/TrainerData';

/* ONEECOA-70360
* Update handheld TrainerData custom10 to default to 1 instead of 3
*/
TrainerData.getSingleSubject.custom10 = '1';
