import PDE from 'PDE_Core/common/PDE';

/*
    IMPORTANT: These functions rely on being passed a UserVisit model to act on.
    The ELF trigger 'VISITGATEWAY:BeforeRenderVisits' params contain references these models (within VisitRow models),
    but to use these functions elsewhere requires manually fetching the UserVisit you wish to act on and passing it in.
 */

/**
 * Return whether the specified visit is completed.
 * @param {UserVisit} userVisit the user visit
 * @returns {boolean} true if visit is completed, otherwise false
 */
export function isVisitCompleted (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.COMPLETED);
}

PDE.VisitUtils.isVisitCompleted = isVisitCompleted;

/**
 * Return whether the specified visit has been touched.
 * A touched visit is one that has a UserVisit object associated with it.
 * That would include one that was skipped, expired, complete, or in-progress.
 * @param {UserVisit} userVisit the user visit
 * @returns {boolean} true if visit has been touched, otherwise false
 */
export function isVisitTouched (userVisit) {
    return (userVisit !== null) && (userVisit !== undefined);
}

PDE.VisitUtils.isVisitTouched = isVisitTouched;

/**
 * Return whether the specified visit is in progress.
 * @param {UserVisit} userVisit the user visit
 * @returns {boolean} true if visit is in progress, otherwise false
 */
export function isVisitInProgress (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.IN_PROGRESS);
}

PDE.VisitUtils.isVisitInProgress = isVisitInProgress;

/**
 * Return whether the specified visit is finished.
 * A visit is considered finished if a visit has dateEnded for the UserVisit object. This is
 * equivalent to a visit with incomplete, aborted, expired or completed visit status.
 * @param {UserVisit} userVisit the user visit
 * @returns {boolean} true if visit has ended, otherwise false
 */
export function isVisitFinished (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('dateEnded') !== null) && (userVisit.get('dateEnded') !== undefined);
}

PDE.VisitUtils.isVisitFinished = isVisitFinished;

/**
 * Return whether the specified visit has been skipped.
 * @param {UserVisit} userVisit the user visit
 * @returns {boolean} true if visit has been skipped, otherwise false
 */
export function isVisitSkipped (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.SKIPPED);
}

PDE.VisitUtils.isVisitSkipped = isVisitSkipped;

/**
 * Return whether the specified visit is available.
 * @param {UserVisit} userVisit the user visit
 * @returns {boolean} true if visit is available, otherwise false
 */
export function isVisitAvailable (userVisit) {
    return !isVisitTouched(userVisit) || (userVisit.get('state') === LF.VisitStates.AVAILABLE);
}

PDE.VisitUtils.isVisitAvailable = isVisitAvailable;

/**
 * Return whether the specified visit has expired.
 * @param {UserVisit} userVisit the user visit
 * @returns {boolean} true if visit has expired, otherwise false
 */
export function isVisitExpired (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.INCOMPLETE) &&
        (userVisit.get('expired') === true);
}

PDE.VisitUtils.isVisitExpired = isVisitExpired;

/**
 * Return whether the specified visit was aborted.
 * A visit is considered aborted if it was started and then prematurely closed
 * but has not expired.
 * @param {UserVisit} userVisit the user visit
 * @returns {boolean} true if visit was aborted, otherwise false
 */
export function isVisitAborted (userVisit) {
    return isVisitTouched(userVisit) && (userVisit.get('state') === LF.VisitStates.INCOMPLETE) && !isVisitExpired(userVisit);
}

PDE.VisitUtils.isVisitAborted = isVisitAborted;
