import extendSettingsViewPDE from 'PDE_Core/tablet/views/PDESettingsView';

export default {
    rules: [
        {
            id: 'addUpdateButtonToSettings',
            trigger: 'APPLICATION:Loaded',
            salience: 100,
            evaluate: true,
            resolve: [
                {
                    action: extendSettingsViewPDE
                }
            ]
        }
    ]
};
