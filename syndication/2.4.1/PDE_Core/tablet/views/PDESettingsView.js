import COOL from 'core/COOL';
import SettingsView from 'sitepad/views/SettingsView';

// This function is imported for a study rule, so it doesn't require an index import.

/**
 * @function extendSettingsViewPDE
 * Extends SettingsView to add Update button, to execute transmissions and fullsync
 */
export default function extendSettingsViewPDE () {
    /**
     * @class TrainerSettingsView
     * A class that extends the sitepad SettingsView view.
     */
    class PDESettingsView extends COOL.getClass('SettingsView', SettingsView) {
        /**
         * @method render
         * Overides the settings template and render the view to the DOM
         * @returns {Q.Promise<void>}
         */
        render () {
            return LF.getStrings('CHECK_FOR_UPDATES').then((string) => {
                let buttonHTML = LF.templates.display('PDE:settingsMenuUpdateButton', { update: string }),
                    settingsTemplate = $('<div>').append($('#settings-template').html()),
                    buttonContainer = settingsTemplate.find('#button-container');

                if (!buttonContainer.find('#update-button').length) {
                    buttonContainer.append(buttonHTML).html();
                    $('#settings-template').html(settingsTemplate.html());
                }

                // this.templateStrings.update = 'CHECK_FOR_UPDATES';

                this.events['click #update-button'] = 'updateTablet';


                return super.render();
            });
        }

        /**
         * @method exitTrainerMode
         * Handles the update button click event
         */
        updateTablet () {
            ELF.trigger('PDE:UpdateTablet', {}, this)
                .done();
        }
    }

    COOL.add('SettingsView', PDESettingsView);
}
