export default {
    templates: [
        {
            name: 'settingsMenuUpdateButton',
            namespace: 'PDE',
            template: '<button id="update-button" class="btn btn-default btn-block">{{ update }}</button>'
        }
    ]
};
