import updateButton from './settingsUpdatebutton';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects({}, updateButton);
