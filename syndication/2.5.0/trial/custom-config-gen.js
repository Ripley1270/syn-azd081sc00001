/* eslint-disable */
process.chdir(__dirname);
const fs = require('fs-extra');
const _ = require('lodash');
const widgetDoc = JSON.parse(fs.readFileSync('../doc/widgets.json'));
const targetDir = `${__dirname}/assets/study-design/diaries/custom-config`;

const LONG_TEXT_ITEM_1 = 'Item 1:  This is a very long string of text, used to demonstrate how a widget behaves for values that require line breaks.  This string may take up 2 or 3 lines, depending on the platform.';

if (!fs.existsSync(targetDir)) {
    fs.mkdirSync(targetDir);
}
process.chdir(targetDir);

const fileTemplate = `
import {getAnswerValue, getAnswerText, resourceAsStatic, extendModel, removeQuestionAndAnswer, initializeObject} from 'trial/widgets/param-functions/custom-config-functions';
let customParams = (() => {
    let attributes = {};
    {CONFIG_PARAMS}
    return attributes;
})();

export default {
    {SCHEDULE},
    {QUESTIONNAIRE},
    {SCREEN},
    {QUESTION},
    {RULES}
};`;

const scheduleTemplate = `
    schedules: [
        {
            id: 'P_{CLASS}_Diary_CC_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'P_{CLASS}_Diary_CC'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]`;

const questionnaireTemplate = `
    questionnaires: [
        {
            id: 'P_{CLASS}_Diary_CC',
            SU: '{CLASS}_Diary_CC',
            displayName: resourceAsStatic('{CLASS} (Custom Config)'),
            className: '{CLASS}_TEST Custom_Config',
            affidavit: false, // Choose default affidavit per user role
            previousScreen: true,
            screens: [
                '{CLASS}_CC_Config',
                '{CLASS}_CC_Result',
                '{CLASS}_CC_Review'
            ],
            branches: []
        }
    ]`;

const screenTemplate = `
    screens: [
        {
            id: '{CLASS}_CC_Config',
            className: 'Config_Input',
            questions: [
                {SCREEN_QUESTIONS}
            ]
        },
        {
            id: '{CLASS}_CC_Result',
            className: 'Config_Result',
            questions: [
                {
                    id: '{CLASS}_CC_Q_Result',
                    mandatory: false
                }
            ]
        },
        {
            id: '{CLASS}_CC_Review',
            className: 'Config_Review',
            questions: [
                {
                    id: '{CLASS}_CC_Q_Review',
                    mandatory: true
                }
            ]
        }
    ]`;

const screenQuestionTemplate = `
                {
                    id: '{ID}',
                    mandatory: {MANDATORY}
                },`;

const questionTemplate = `
    questions: [
        {INPUT_QUESTIONS}
        {CONFIG_QUESTION}
        {
            id: '{CLASS}_CC_Q_Review',
            IG: '{CLASS}_CC',
            IT: '{CLASS}_CC_Q_Review',
            title: resourceAsStatic('Summary'),
            text: [resourceAsStatic('{{{ getQuestionnaireAnswers }}}')],
            className: '{CLASS}_CC_Review'
        }
    ]`;

const inputQuestionTemplates = {
    'number':  `
        {
            id: '{ID}',
            IG: '{CLASS}_CC',
            IT: '{ID}',
            className: '{CLASS}_Q_{QNUM}',
            text: [],
            widget: {
                id: '{ID}_W',
                type: 'NumericTextBox',
                step: 0.01,
                min: Number.MIN_SAFE_INTEGER,
                max: Number.MAX_SAFE_INTEGER,
                placeholder: resourceAsStatic('{DEFAULT}'),
                label: function () { return resourceAsStatic(\`{LABEL}\`); }
            }
        },
    `,
    'boolean': `
        {
            id: '{ID}',
            IG: '{CLASS}_CC',
            IT: '{ID}',
            className: '{CLASS}_Q_{QNUM}',
            text: [resourceAsStatic(\`<label>{LABEL}</label>\`)],
            widget: {
                id: '{ID}_W',
                type: 'CustomRadioButton',
                className: 'CustomRadioButton YES_NO',
                templates: {
                    container: 'Welcome:HorizontalButtonGroup',
                    wrapper: 'DEFAULT:HorizontalWrapper'
                },
                answers: [
                    { text: resourceAsStatic('YES'), value: '1' },
                    { text: resourceAsStatic('NO'), value: '0' }
                ],
                answer: '{DEFAULT}'
            }
        },
    `,
    'string': `
        {
            id: '{ID}',
            IG: '{CLASS}_CC',
            IT: '{ID}',
            className: '{CLASS}_Q_{QNUM}',
            text: [],
            widget: {
                id: '{ID}_W',
                type: 'TextBox',
                placeholder: resourceAsStatic('{DEFAULT}'),
                label: function () { return resourceAsStatic(\`{LABEL}\`); }
            }
        },
    `,
    'string.<options>': `
        {
            id: '{ID}',
            IG: '{CLASS}_CC',
            IT: '{ID}',
            className: '{CLASS}_Q_{QNUM}',
            text: [resourceAsStatic(\`<label>{LABEL}</label>\`)],
            widget: {
                id: '{ID}_W',
                type: 'CustomRadioButton',
                className: 'CustomRadioButton',
                templates: {
                    container: 'Welcome:HorizontalButtonGroup',
                    wrapper: 'DEFAULT:HorizontalWrapper'
                },
                answers: {ANSWER_OPTIONS},
                answer: '{DEFAULT}'
            }
        },
    `,
    'string.<translated>': `
        {
            id: '{ID}',
            IG: '{CLASS}_CC',
            IT: '{ID}',
            className: '{CLASS}_Q_{QNUM}',
            text: [],
            widget: {
                id: '{ID}_W',
                type: 'TextBox',
                placeholder: resourceAsStatic('{DEFAULT}'),
                label: function () { return resourceAsStatic(\`{LABEL}\`); }
            }
        },
    `,    
    'Date': `
        {
            id: '{ID}',
            IG: '{CLASS}_CC',
            IT: '{ID}',
            className: '{CLASS}_Q_{QNUM}',
            text: [],
            widget: {
                id: '{ID}_W',
                type: 'DateSpinner',
                modalTitle: resourceAsStatic('{LABEL}'),
                okButtonText: resourceAsStatic('OK'),
                className: 'DateTimeSpinner',
                maxDate: () => {
                    return new Date(new Date().setFullYear(new Date().getFullYear() + 35));
                },
                minDate: () => {
                    return new Date(new Date().setFullYear(new Date().getFullYear() - 35));
                },
                label: function () { return resourceAsStatic(\`{LABEL}\`); }
            }
        },
    `,
    'Time': `
        {
            id: '{ID}',
            IG: '{CLASS}_CC',
            IT: '{ID}',
            className: '{CLASS}_Q_{QNUM}',
            text: [],
            widget: {
                id: '{ID}_W',
                type: 'TimeSpinner',
                dateFormat: 'LT',
                modalTitle: resourceAsStatic('{LABEL}'),
                okButtonText: resourceAsStatic('OK'),
                className: 'TimeSpinner',
                initialDate: () => {
                    return new Date('01 Jan 1970');
                },
                maxDate: () => {
                    return new Date('01 Jan 1970 23:59');
                },
                minDate: () => {
                    return new Date('01 Jan 1970 00:00');
                },
                label: function () { return resourceAsStatic(\`{LABEL}\`); }
            }
        },
    `,
    'DateTime': `
        {
            id: '{ID}',
            IG: '{CLASS}_CC',
            IT: '{ID}',
            className: '{CLASS}_Q_{QNUM}',
            text: [],
            widget: {
                id: '{ID}_W',
                type: 'DateTimeSpinner',
                modalTitle: resourceAsStatic('{LABEL}'),
                okButtonText: resourceAsStatic('OK'),
                className: 'DateTimeSpinner',
                storageFormat: 'YYYY-MM-DDTHH:mm:ss',
                maxDate: () => {
                    return new Date(new Date().setFullYear(new Date().getFullYear() + 35));
                },
                minDate: () => {
                    return new Date(new Date().setFullYear(new Date().getFullYear() - 35));
                },
                label: function () { return resourceAsStatic(\`{LABEL}\`); }
            }
        },
    `
};

const noInputQuestionTemplate = `
    {
        id: '{ID}',
        IG: '{CLASS}_CC',
        IT: '{ID}',
        className: '{CLASS}_Q_{QNUM}',
        text: [resourceAsStatic('No parameters to set.  Please click next to continue')]
    },`;

const configQuestionTemplate = `
        {
            id: '{CLASS}_CC_Q_Result',
            IG: '{CLASS}_CC',
            IT: '{CLASS}_CC_Q_Result',
            title: resourceAsStatic('{CLASS} Title'),
            text: resourceAsStatic('{CLASS} Text'),
            className: '{CLASS}_W_2',
            widget: {
                id: '{CLASS}_W_2',
                type: '{CLASS}',
                className: 'widget {CLASS}',
                label: function () { return getAnswerText.call(this, '{CLASS} label'); }
            }
        },
    `;

const rulesTemplate = `
    rules: [
        {
            id: '{CLASS}_CC_Config_Opened',
            trigger: 'QUESTIONNAIRE:Navigate/P_{CLASS}_Diary_CC/{CLASS}_CC_Config',
            // canSyncOffline is an expression that exists on the context of the questionnaire.
            // see UserStatusQuestionnaireView.js.
            resolve: [{
                action: () => {
                    let IG = '{CLASS}_CC',
                        IT = '{CLASS}_CC_Q_Result';
                    removeQuestionAndAnswer(IG, IT);
                    extendModel(IT, customParams);
                }
            }]
        }
    ]`;

const widgetExt = (className) => {
    switch(className) {
        case 'NumericRatingScale':
            return `
                _.extend(attributes, {
                    answers: [
                            { text: resourceAsStatic('1'), value: '1' },
                            { text: resourceAsStatic('2'), value: '2' },
                            { text: resourceAsStatic('3'), value: '3' },
                            { text: resourceAsStatic('4'), value: '4' },
                            { text: resourceAsStatic('5'), value: '5' },
                            { text: resourceAsStatic('6'), value: '6' },
                            { text: resourceAsStatic('7'), value: '7' },
                            { text: resourceAsStatic('8'), value: '8' },
                            { text: resourceAsStatic('9'), value: '9' },
                            { text: resourceAsStatic('10'), value: '10' }
                        ]
                });`;
        case 'RadioButton':
        case 'CheckBox':
        case 'CustomRadioButton':
        case 'CustomCheckBox':
            return `
                _.extend(attributes, {
                    answers: [
                            { text: resourceAsStatic('Item 1'), value: '1', IT: 'CUSTOM_CONFIG_IT_1' },
                            { text: resourceAsStatic('Item 2'), value: '2', IT: 'CUSTOM_CONFIG_IT_2' },
                            { text: resourceAsStatic('Item 3'), value: '3', IT: 'CUSTOM_CONFIG_IT_3' },
                            { text: resourceAsStatic('Item 4'), value: '4', IT: 'CUSTOM_CONFIG_IT_4' }
                        ]
                });`;
        case 'DropDownList':
            return `
                _.extend(attributes, {
                    items: [
                            { text: resourceAsStatic('${LONG_TEXT_ITEM_1}'), value: '1', IT: 'CUSTOM_CONFIG_IT_1' },
                            { text: resourceAsStatic('Item 2'), value: '2', IT: 'CUSTOM_CONFIG_IT_2' },
                            { text: resourceAsStatic('Item 3'), value: '3', IT: 'CUSTOM_CONFIG_IT_3' },
                            { text: resourceAsStatic('Item 4'), value: '4', IT: 'CUSTOM_CONFIG_IT_4' }
                        ]
                });`;
        case 'ImageWidget':
            return `
                _.extend(attributes, {
                    imageName: 'skeleton.svg',
                    imageText: {
                        legend0: resourceAsStatic('No Pain'),
                        legend1: resourceAsStatic('Some Pain'),
                    },
                    nodeIDRegex: /[rl]\\d+/
                });
                `;
        case 'MatrixRadioButton':
            return `
                _.extend(attributes, {
                    questions: [
                        {
                            IT: 'Radio_CC_Matrix_1',
                            text: resourceAsStatic('Question 1: ')
                        },
                        {
                            IT: 'Radio_CC_Matrix_2',
                            text: resourceAsStatic('Question 2: ')
                        },
                        {
                            IT: 'Radio_CC_Matrix_3',
                            text: resourceAsStatic('Question 3: ')
                        }, {
                            IT: 'Radio_CC_Matrix_4',
                            text: resourceAsStatic('Question 4: ')
                        }
                    ],
                    matrixAnswers: [
                        {
                            text: resourceAsStatic('Option 1'),
                            value: '1'
                        }, {
                            text: resourceAsStatic('Option 2'),
                            value: '2'
                        }, {
                            text: resourceAsStatic('Option 3'),
                            value: '3'
                        }, {
                            text: resourceAsStatic('Option 4'),
                            value: '4'
                        }
                    ]
                });`;
        default:
            return '';
    }
};

/**
 * get full list of params for the constructor in JSDoc JSON.
 * @param {Object} widgetJSON Full widget JSON
 * @param {string} className class of the object for which we are retrieving info
 * @returns {Array} array of JSDoc param objects.
 */
function getConstructorParams (widgetJSON, className) {
    let ret = [];
    _.each(widgetJSON.docs, (docPart) => {
        if ((docPart.name || '').toLowerCase() === className.toLowerCase() && docPart.kind === 'class') {
            ret = docPart.params;
            return;
        }
    });
    return ret;
}

/**
 * Get widget parameters marked with SD.
 * @param {Object} widgetJSON Full widget JSON
 * @param {string} className class of the object for which we are retrieving info
 * @returns {Array} array of JSDoc param objects, altered to remove SD-specific flags.
 */
function getSDParams (widgetJSON, className) {
    let params = getConstructorParams(widgetJSON, className);
    let ret = [];

    let sdRegex = /.*<b>(.*)<sup>&nbsp;SD&nbsp;<\/sup><\/b>/;

    _.each(params, (param) => {
        let m = param.name.match(sdRegex);
        if (m && m.length > 1) {
            let newParam = _.clone(param);
            newParam.name = m[1];
            ret.push(newParam);
        }
    });
    return ret;
}

/**
 * Replace a template with values.
 * @param {string} string template string
 * @param {Object} patterns key value pair of patterns (surrounded by {}) to be replaced.
 * @returns {string} input string with patterns replaced by appropriate values.
 */
function replacePatterns (string, patterns) {
    return _.keys(patterns).reduce((initialVal, key) => {
        return initialVal.replace(new RegExp(`{${key}}`, 'gm'), patterns[key]);
    }, string);
}

/**
 * 
 * @param {Object} widgetJSON Full widget JSON
 * @param {string} className class of the object for which we are retrieving info
 * @param {Object} index object, passed by reference,
 *  containing imports, export variables, and IDs
 *  used to index our classes as we process them.
 */
function generateQuestionnaire (widgetJSON, className, index) {
    let params = getSDParams(widgetJSON, className);

    let questionnaire = replacePatterns(questionnaireTemplate, {
        CLASS: className
    });

    let screenQuestions = '';
    let inputQuestions = '';
    let configParams = '';
    let qNum = 0;
    _.each(params, (param) => {
        let paramName = param.name.replace(/\[\]/g, '[0]'),
            paramID = `${className}_${paramName}`.replace(/[\.\[\]]/g, '_'),
            nameParts = paramName.split(/\./),
            paramType = param.type.names[0],
            defaultVal = param.defaultvalue,
            paramDescription = param.description,
            optionListRegex = /^\(([a-zA-Z0-9\|]*)\)/,
            inputQuestionTemplate;

        // parse a boolean value from our defaultVal string.
        if (paramType === 'boolean' && typeof defaultVal === 'boolean') {
            defaultVal = defaultVal ? 1 : 0;
        }

        qNum += 1;

        // if string is a finite set of options, list them
        let matches = optionListRegex.exec(paramDescription);
        let answerOptions = '';
        if (matches && matches.length > 1 && paramType === 'string') {
            let optionList = matches[1].split('|');
            let answerOptionArray = [];
            _.each(optionList, (option) => {
                answerOptionArray.push(`{text: resourceAsStatic('${option}'), value:'${option}' }`);
            });
            answerOptions = `[${answerOptionArray.join(', ')}]`;
            paramType = 'string.<options>';
        }

        if (matches && matches.length > 1 && paramType === 'Date') {
            switch(matches[1].toLowerCase()) {
                case 'date':
                    paramType = 'Date';
                    break;
                case 'time':
                    paramType = 'Time';
                    break;
                case 'datetime':
                    paramType = 'DateTime';
                    break;
            }
        }

        paramDescription = paramDescription.replace(/\'/g, '&apos;').replace(/\"/g, '&quot;').replace(/\<[\/\s]*strong\>/g, '').replace(optionListRegex, '');

        inputQuestionTemplate = inputQuestionTemplates[paramType];

        // create a screen question
        if (inputQuestionTemplate) {
            screenQuestions += replacePatterns(screenQuestionTemplate, {
                ID: paramID,
                MANDATORY: !param.optional
            });

            inputQuestions += replacePatterns(inputQuestionTemplate, {
                ID: paramID,
                CLASS: className,
                QNUM: qNum,
                DEFAULT: (typeof defaultVal !== 'undefined') ? defaultVal : '',
                LABEL: `<span title="${paramDescription}">${paramName}${param.optional ? '' : ' *'}</span>`,
                ANSWER_OPTIONS: answerOptions
            });

            // create a configured question
            configParams += `initializeObject(attributes, '${nameParts.join('\', \'')}') &&
                (attributes.${paramName} = function () { return getAnswerValue.call(this, '${paramID}', '${paramType}'); });\n\t`;
        }
    });

    configParams += widgetExt(className);

    if (inputQuestions === '') {
        let paramID = `${className}_CONFIG_EMPTY`.replace(/[\.\[\]]/g, '_');
        inputQuestions = replacePatterns(noInputQuestionTemplate, {
                ID: paramID,
                CLASS: className,
                QNUM: 0
            });
        screenQuestions += replacePatterns(screenQuestionTemplate, {
                ID: paramID,
                MANDATORY: false
            });
    }

    // remove trailing comma
    screenQuestions = screenQuestions.substring(0, screenQuestions.length - 1);

    let schedule = replacePatterns(scheduleTemplate, {
        CLASS: className
    });

    let screen = replacePatterns(screenTemplate, {
        SCREEN_QUESTIONS: screenQuestions,
        CLASS: className
    });

    let configQuestion = replacePatterns(configQuestionTemplate, {
        CLASS: className
    });

    let question = replacePatterns(questionTemplate, {
        INPUT_QUESTIONS: inputQuestions,
        CONFIG_QUESTION: configQuestion,
        CLASS: className
    });

    let rules = replacePatterns(rulesTemplate, {
        CLASS: className
    });

    let contents = replacePatterns(fileTemplate, {
        SCHEDULE: schedule,
        CONFIG_PARAMS: configParams,
        QUESTIONNAIRE: questionnaire,
        SCREEN: screen,
        QUESTION: question,
        RULES: rules
    });

    let fileName = `${className.toLowerCase()}-cc.js`;
    fs.writeFileSync(fileName, contents);

    index.imports.push(`import ${className}CustomConfig from './${fileName.replace('.js', '')}';`);
    index.exportVars.push(`${className}CustomConfig`);
    index.ids.push(`P_${className}_Diary_CC`);
}

/**
 * Write our index file for the custom config directory, based on our index object.
 * @param {Object} index Object containing import statements, import variable names, and ids.
 */
function writeIndex (index) {
    let indexContents = `${index.imports.join('\n')}`;
    indexContents += `\n\nimport {mergeObjects} from 'core/utilities/languageExtensions';`;
    indexContents += `\n\nexport default mergeObjects(${index.exportVars.join(', ')});`;
    fs.writeFileSync('index.js', indexContents);
}

/**
 * Create a ".bak" version for this file (if it does not exist), and replace the contents of the original file
 * with new contents.
 * @param {string} fileName file name to backup and re-write
 * @param {string} contents contents to write to new copy of file (after it is backed up)
 */
function replaceAndBackup (fileName, contents) {
    let bak = `${fileName}.bak`;
    if (!fs.existsSync(bak)) {
        fs.copySync(fileName, bak);
    }
    fs.writeFileSync(fileName, contents);
}

/**
 * Write the diary-level index file, creating a backup for the clean operation.
 */
function writeDiaryIndex () {
    let file = '../index.js',
        contents = `import customConfig from './custom-config';
            export default customConfig;`.replace(/\t/g, '');

    replaceAndBackup(file, contents);
}

/**
 * Write the schedules-level index file, creating a backup for the clean operation.
 */
function removeOldSchedules () {
    let file = '../../schedules/index.js',
        contents = 'export default {};';
    
    replaceAndBackup(file, contents);
}

/**
 * Write new visits file with a new visit for our custom configs, 
 *  creating a backup of the old visits file for the clean operation.
 * @param {Object} index Object containing import statements, import variable names, and ids.
 */
function writeVisits (index) {
    let file = '../../visits.js',
        contents = `
        export default {
            visits: [{
                id    : 'custom_config_visit',
                studyEventId: '10',
                displayName  : 'VISIT_CUSTOM_CONFIG',
                visitType : 'ordered',
                visitOrder : 1,
                forms : ['${index.ids.join('\', \'')}'],
                delay: '0'
            }]
        };`;
    
    replaceAndBackup(file, contents);
}

/**
 * Generate our custom-config questionnaires, and alter the appropriate trial directories
 * to schedule them.
 * @param {Object} widgetJSON JSDoc Widget object.
 */
function generateQuestionnaires (widgetJSON) {
    let index = {
        imports: [],
        exportVars: [],
        ids: []
    },
        usedClasses = {};

    _.each(widgetJSON.docs, (docPart) => {
        if (docPart.kind === 'class' && !usedClasses[docPart.name]) {
            generateQuestionnaire(widgetJSON, docPart.name, index);
            usedClasses[docPart.name] = true;
        }
    });

    writeIndex(index);
    writeDiaryIndex();
    removeOldSchedules();
    writeVisits(index);
}

generateQuestionnaires(widgetDoc);