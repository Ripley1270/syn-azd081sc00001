/**
 * Use the subject's activation date as the default value for the widget
 */
LF.Widget.ParamFunctions.activationDate = () => {
    let activationDate = new Date(LF.Widget.ParamFunctions.subject.get('activationDate'));

    return Q({
        minDays: 0,
        maxDays: 0,
        defaultValue: LF.Utilities.convertToDate(activationDate)
    });
};

/**
 * Return params for DATE_DIARY_Q_8
 */
LF.Widget.ParamFunctions.dateDiaryQ8 = () => {
    let deferred = Q.defer(),
        params = {
            min: '2014-01-01T10:22:00',
            max: '2014-02-01T11:33:00'
        };

    deferred.resolve(params);
    return deferred.promise;
};

/**
 * Return params for DATE_DIARY_Q_5
 */
LF.Widget.ParamFunctions.dateDiaryQ5 = () => {
    let deferred = Q.defer(),
        params = {
            minHour: 5,
            maxHour: 10,
            defaultValue: '08:30:00'
        };

    deferred.resolve(params);
    return deferred.promise;
};
