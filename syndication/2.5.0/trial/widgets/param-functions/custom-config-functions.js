// TODO:  Remove this import when this widget is added to the core.  Right now it is a sneak preview.
import 'core/widgets/DateTimeSpinner';

import { createGUID } from 'core/utilities/coreUtilities';

let originalWidgetAttrs = {};

/**
 * For a field that is typically translated, use this method to use hardcoded/static text instead.
 * This method creates a new Language resource, and returns a unique key (GUID) referencing it.
 * @param {string} value value to be inserted into text resource.
 * @returns {string} GUID assigned to the new text resource, can be looked up to retrieve your static text value.
 */
function resourceAsStatic (value) {
    let tempJSON = {
        namespace: 'CORE',
        language: 'en',
        locale: 'US',
        localized: 'English (US)',
        direction: 'ltr',
        resources: {}
    };
    let key = createGUID();

    // Make a blank value a space, so something shows
    tempJSON.resources[key] = value || ' ';
    LF.strings.addLanguageResources(tempJSON);
    return key;
}

/**
 * Iterate through an object.  If it contains methods, execute them, and place their values in that key.
 * @param {Object} obj object to iterate.
 * @returns {Object} object with functions replaced by the result of executing the functions.
 */
function evaluateObjectMethods (obj) {
    let retObj = _.isArray(obj) ? [] : {};
    _.each(obj, (item, key) => {
        if (typeof item === 'function') {
            let val = item();
            if (typeof val !== 'undefined') {
                retObj[key] = val;
            }
        } else if (typeof item === 'object' && !(item instanceof RegExp)) {
            retObj[key] = evaluateObjectMethods(item);
        } else {
            retObj[key] = item;
        }
    });
    return retObj;
}

/**
 * Remove UI and answers assigned to a question.
 * This ensures that the next load of a question will be fresh
 * (not using old model values or previous answers.)
 * @param {string} IG IG for question
 * @param {string} IT IT for question
 */
function removeQuestionAndAnswer (IG, IT) {
    let questionViews = LF.Data.Questionnaire.questionViews;

    // Remove initial answer from collection.
    LF.Data.Questionnaire.removeIT(IG, IT, 0);

    // clear the queston containing our widget, so the widget is re-created.
    for (let i = questionViews.length - 1; i >= 0; --i) {
        if (questionViews[i].id === IT) {
            questionViews.splice(i);
            break;
        }
    }
}

/**
 * Extend widget model for a question with custom attributes.
 * @param {string} IT IT for question
 * @param {Object} attributes attributes to be added to question model.
 */
function extendModel (IT, attributes) {
    let model = LF.Data.Questionnaire.data.questions.get(IT);

    originalWidgetAttrs[IT] = originalWidgetAttrs[IT] || model.attributes.widget;

    model.attributes.widget = _.extend({}, originalWidgetAttrs[IT], evaluateObjectMethods(attributes));
}

/**
 * Prepare an object to contain all the attributes it needs.
 * Also does special processing for arrays (can be specified in attributes array as "arrayName[0]")
 * @param {Object} obj base object
 * @param {Object} attributes object attributes.  Each array index indicates a level.  For instance:
 * ["anchors", "text", "value"] initializes obj.anchors.text.value
 * @returns {Object} new version of object, initialized with the levels of attributes specified.
 */
function initializeObject (obj, ...attributes) {
    let ret = {};
    attributes.reduce((prevObj, key) => {
        let arrayNdx = null,
            arrayNumberRegex = /\[(\d*)\]/,
            matches = key.match(arrayNumberRegex),
            isArray = false,
            res;

        if (matches && matches.length > 1) {
            arrayNdx = parseInt(matches[1] || 0, 10);
            isArray = true;
        }

        // eslint-disable-next-line no-param-reassign
        key = key.replace(arrayNumberRegex, '');
        if (!prevObj[key] || (isArray && !_.isArray(prevObj[key]))) {
            prevObj[key] = isArray ? [] : {};
        }

        res = prevObj[key];
        if (isArray) {
            prevObj[key][arrayNdx] = prevObj[key][arrayNdx] || {};
            res = prevObj[key][arrayNdx];
        }
        return res;
    }, obj);
    return ret;
}

/**
 * Parse an answer to a type.
 * @param {string} value value returned by the question
 * @param {string} type type to convert it to.
 * @returns {*} answer parsed to type.
 */
function parseAnswer (value, type = '') {
    let ret = null,

        // offset in milliseconds
        tzoffset = new Date().getTimezoneOffset() * 60000;

    switch (type.toLowerCase()) {
        case 'number':
            if (value === 'Infinity') {
                ret = Number.POSITIVE_INFINITY;
            } else if (value === '-Infinity') {
                ret = Number.NEGATIVE_INFINITY;
            } else {
                ret = parseFloat(value);
            }
            break;
        case 'date':
            ret = new Date(new Date(value) - tzoffset).toISOString().slice(0, -1);
            break;
        case 'boolean':
            ret = value === '1';
            break;
        case 'string.<translated>':
            ret = resourceAsStatic(value);
            break;
        case 'string':
        default:
            ret = value;
    }
    return ret;
}

/**
 * Use the subject's activation date as the default value for the widget
 * @param {string} questionId - The ID of the question
 * @param {string} type - type of question.
 * @returns {*} value of answer, parsed in whatever type is required.
 */
function getAnswerValue (questionId, type) {
    let questionViews = LF.Data.Questionnaire.questionViews,
        returnValue;

    _.find(questionViews, (questionView) => {
        if (questionView.widget) {
            let answers = questionView.widget.answers || [];
            answers.find((answer) => {
                if (answer.get('question_id') === questionId) {
                    returnValue = parseAnswer(answer.get('response'), type);
                }
                return returnValue !== undefined;
            });
        }
        return returnValue !== undefined;
    });

    return returnValue;
}

/**
 * Get the question answer, and return it as a translated string.
 * @param {string} questionId ID of the question
 * @returns {string} guid referencing newly created text resource.
 */
function getAnswerText (questionId) {
    let answerValue = getAnswerValue(questionId);
    return resourceAsStatic(answerValue);
}

LF.Widget.ParamFunctions.getAnswerValue = getAnswerValue;
LF.Widget.ParamFunctions.getAnswerText = getAnswerText;
LF.Widget.ParamFunctions.resourceAsStatic = resourceAsStatic;
LF.Widget.ParamFunctions.evaluateObjectMethods = evaluateObjectMethods;
LF.Widget.ParamFunctions.removeQuestionAndAnswer = removeQuestionAndAnswer;
LF.Widget.ParamFunctions.extendModel = extendModel;
LF.Widget.ParamFunctions.initializeObject = initializeObject;

export default { getAnswerValue, getAnswerText, resourceAsStatic, evaluateObjectMethods, removeQuestionAndAnswer, extendModel, initializeObject };
