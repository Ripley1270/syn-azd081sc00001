LF.Widget.ParamFunctions.numberSpinnerTestInputOptions = () => Q.resolve([{
    min: 0,
    max: 400,
    step: 5
}]);

LF.Widget.ParamFunctions.numberSpinnerTestDefault = () => Q.resolve(5);


LF.Widget.ParamFunctions.Q9OverallMax = () => Q.resolve(1000);
