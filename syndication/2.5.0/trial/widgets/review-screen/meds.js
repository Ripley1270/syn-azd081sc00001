LF.Widget.ReviewScreen.medsList = () => {
    return Q.Promise((resolve) => {
        let text1 = LF.Helpers.getWidget('MEDS_Q_1').answer.get('response'),
            text2,
            items = [];

        if (text1 === '0') {
            text2 = 'Half a pill';
        } else {
            text2 = text1;
        }

        items.push({ id: '1', text: [text2] });

        resolve(items);
    });
};

LF.Widget.ReviewScreen.alertAction = () => {
    return Q.Promise((resolve) => {
        LF.Actions.notify({
            message: { key: 'ALERT_MESSAGE', namespace: 'P_Meds' }
        }, () => resolve());
    });
};

LF.Widget.ReviewScreen.addAction = () => {
    return Q.Promise((resolve) => {
        LF.Actions.notify({ message: { key: 'ADD_MESSAGE', namespace: 'P_Meds' } }, () => resolve());
    });
};

LF.Widget.ReviewScreen.editAction = () => {
    return Q.Promise((resolve) => {
        LF.Actions.notify({ message: { key: 'EDIT_MESSAGE', namespace: 'P_Meds' } }, () => resolve());
    });
};

LF.Widget.ReviewScreen.deleteAction = () => {
    return Q.Promise((resolve) => {
        LF.Actions.notify({ message: { key: 'DELETE_MESSAGE', namespace: 'P_Meds' } }, () => resolve());
    });
};

LF.Widget.ReviewScreen.medsList_Q4 = () => {
    return Q.Promise((resolve) => {
        let text1 = LF.Helpers.getWidget('MEDS_Q_1').answer.get('response'),
            items = [];

        items.push({
            id: '1',
            text: [text1]
        });

        resolve(items);
    });
};

LF.Widget.ReviewScreen.editAction_Q4 = () => {
    return Q.Promise((resolve) => {
        LF.router.view().screenStack = [];
        LF.router.view().screenStack.push('MEDS_S_1');

        // First screen
        LF.router.view().screen = 0;
        LF.router.view().displayScreen('MEDS_S_1');

        resolve();
    });
};
