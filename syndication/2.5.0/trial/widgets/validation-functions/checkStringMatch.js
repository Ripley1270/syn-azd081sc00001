/**
 * @file Example of a validation function which requires the first set of characters
 *       in a barcode to match a predefined string.
 * @author <a href="mailto:jhendy@phtcorp.com">James Hendy</a>
 * @version 1.6
 */

/**
 * Validate a barcode to make sure the starting characters in the barcode match a predefined string
 * @param {string} answer - The barcode answer string scanned or entered by user
 * @param {object} params - An object containing configuration parameters for the validation function
 * @callback({Boolean}) true - Entered barcode is valid
 *                      false - Entered barcode is invalid
 */
LF.Widget.ValidationFunctions.checkStringMatch = (answer, params, callback) => {
    let strLen = params.matchString.length,
        errString = {
            message: {
                namespace: 'P_BarcodeDiary',
                key: 'Q1_WRONG_ANS'
            }
        };

    if (answer.get('response').substring(0, strLen) === params.matchString) {
        callback(true);
    } else {
        LF.getStrings(errString, (string) => {
            LF.Notify.Banner.show({
                text: string.message,
                type: 'error'
            });
        });
        callback(false);
    }
};

export default LF.Widget.ValidationFunctions.checkStringMatch;
