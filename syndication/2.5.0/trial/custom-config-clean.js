/* eslint-disable */
process.chdir(__dirname);
const fs = require('fs-extra');

/**
 * Replace file contents with contents of ".bak" file, and remove ".bak" file.
 * @param {string} file file path of file to restore.
 */
function restoreBackup (file) {
    let bak = `${file}.bak`;
    if (fs.existsSync(bak)) {
        fs.copySync(bak, file);
        fs.removeSync(bak);
    }
}

/**
 * Reverse operations that created the custom configs.  Restore indexes, and visits,
 * and delete custom-config folder.
 */
function cleanCustomConfig () {
    fs.removeSync('./assets/study-design/diaries/custom-config');

    restoreBackup('./assets/study-design/visits.js');
    restoreBackup('./assets/study-design/schedules/index.js');
    restoreBackup('./assets/study-design/diaries/index.js');
}

cleanCustomConfig();
