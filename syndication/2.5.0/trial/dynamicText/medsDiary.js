import dynamicText from 'core/dynamicText';
import * as Helpers from 'core/Helpers';
import * as utilities from 'core/utilities';

/**
 * Get the response for MEDS_Q_1 from Meds current diary.
 */
dynamicText.add({
    id: 'getPills',
    evaluate: () => {
        let response = Helpers.getResponses('MEDS_Q_1')[0].response;

        if (response === '0') {
            return '1/2';
        }
        return response;
    },
    screenshots: {
        values: ['1/2', '2']
    }
});

/**
 * Return the string based on the response to MEDS_Q_1.
 * @param {string} namespace - The namespace to use in translating strings.
 * @returns {object} dynamic text settings.
 */
const pills = (namespace) => {
    return {
        id: 'pills',
        evaluate: () => {
            let response = Helpers.getResponses('MEDS_Q_1')[0].response;
            let stringKey = response === '2' ? 'PILLS' : 'PILL';

            return LF.getStrings(stringKey, $.noop, { namespace });
        },
        screenshots: {
            keys: {
                options: { namespace },
                pill: 'PILL',
                pills: 'PILLS'
            }
        }
    };
};

// Sitepad has it's own meds namespace.
let pillsNamespace = 'P_Meds';

// utilities.isSitePad throws an error in unit tests, ignore it.
try {
    if (utilities.isSitePad()) {
        pillsNamespace = `${pillsNamespace}_SitePad`;
    }
    // eslint-disable-next-line no-empty
} catch (err) {}

dynamicText.add(pills(pillsNamespace));

/**
 * Get the image to display.
 */
dynamicText.add({
    id: 'medsImage',
    evaluate: () => '<img src="media/images/valid.png" height=15px/>',
    screenshots: {
        values: ['<img src="media/images/valid.png" height=15px/>']
    }
});
