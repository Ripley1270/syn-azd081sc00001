import ArrayRef from 'core/classes/ArrayRef';
import ObjectRef from 'core/classes/ObjectRef';

export default {
     // The current study version
    studyVersion: '00.01',

    // The current study database version
    studyDbVersion: 0,

    // The default language of the study.
    defaultLanguage: 'en',

    // The default locale of the study
    defaultLocale: 'US',

    // Country code (locale) keyed object containing defined lists of site languages for each locale.
    // For filtering Language Select Widgets. Default value is empty, to be defined per study.
    // ex.
    // sitelanguagelist: {
    //      default: ['en-US'],
    //      US: ['en-US','de-DE'],
    //      HR: ['en-US']
    // }

    sitelanguagelist: {},

    // The default option to ask secret question.
    askSecurityQuestion: true,

    // If set to true, will show the password rules on password creation views.
    showPasswordRules: false,

    // The maximum number of consecutive unsuccessful unlock attempts in web modality.
    maxUnlockAttempts: 3,

    // Number of minutes after which the lockout for the unlock code expires
    unlockCodeLockoutTime: 3,

    // The maximum number of consecutive unsuccessful password attempts at login and reactivation.
    maxLoginAttempts: 3,

    // The maximum number of consecutive unsuccessful secret question attempts.
    maxSecurityQuestionAttempts: 5,

    // Number of minutes after which the account is automatically re-enabled
    // (where 0 means the account doesn't get re-enabled until a daily reset/override code is used).
    lockoutTime: 5,

    // The amount of time in minutes that a user can be inactive before being logged out.
    sessionTimeout: 30,

    // The amount of time in minutes before an alert appears telling the user that the session will timeout soon.
    sessionTimeoutAlert: 25,

    // The amount of time in minutes that a user is allowed to complete a diary within.
    questionnaireTimeout: 0,

    // The number of minutes to subtract from the updated timestamp when syncing users to close window of client->server time difference.
    userSyncTimeStampThreshold: 20,

    // Configuration for the unlock code feature
    unlockCodeConfig: {
        // Seed code used in calculation for unlock code combined with current date.
        seedCode: 1111,

        // Sets the length of unlock code
        codeLength: 6
    },

    // Default volume settings for alarms
    alarmVolumeConfig: {
        // Volume Percentage at which to sound the alarm (0-100)
        alarmVolume: 100,

        // Enable or Disable vibration for notifications
        vibrate: true,

        // Delay after alarm sounds to revert back to the users configured phone settings
        resetDelay: 2
    },

    // A string array (of diary SUs) to manually specify the diaries that are unscheduled
    lastDiarySyncUnscheduledSUList: null,

    // Configuration for the eSense feature
    eSenseConfig: {

        // Whether or not Bluetooth should be disabled automatically after an operation is complete
        disableBluetooth: true,

        // Timeout value in milliseconds for eSense all API calls (except findDevices and get records).
        apiCallTimeout: 10000,

        // Timeout value in milliseconds for eSense findDevices API calls (in milliseconds).
        findDevicesTimeout: 60000,

        // Timeout value in milliseconds for eSense get records API calls (in milliseconds).
        getRecordsTimeout: 30000,

        // AM1+ Device specific configurations
        AM1Plus: {

            // Timeout Window configuration for AM1+
            timeWindow: {
                windowNumber: 1,
                startHour: 0,
                startMinute: 0,
                endHour: 23,
                endMinute: 59,
                maxNumber: 15
            }
        }
    },

    // Configuration for participant/patient
    participantSettings: {
        participantID: {
            seed: 100,
            steps: 1,
            max: 999
        },
        participantIDFormat: '0000-0000',
        participantNumberPortion: new ArrayRef([5, 9]),
        siteNumberPortion: new ArrayRef([0, 4])
    },

    // Configuration for rater training
    raterTrainingConfig: {
        logpad: {
            useRaterTraining: true,
            awsKey: 'AKIAJMOYUG5VLGLGD7XQ',
            awsSecretKey: '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',
            awsRegion: 'us-east-1',
            awsBucket: 'ert-rater-training',
            rootDirectory: 'rater-hh-test-0ca72d69-923a-40e5-b332-f7925fe1e126',
            fallBackBaseURL: 'http://ert-rater-training.s3-website-us-east-1.amazonaws.com/rater-hh-test-0ca72d69-923a-40e5-b332-f7925fe1e126',
            pages: {
                default: 'index.html'
            },
            zipFile: true
        },
        sitepad: {
            useRaterTraining: true,
            awsKey: 'AKIAJMOYUG5VLGLGD7XQ',
            awsSecretKey: '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',
            awsRegion: 'us-east-1',
            awsBucket: 'ert-rater-training',
            rootDirectory: 'rater-tablet-test-8c051b15-a34f-43d0-a90e-6235f43779ab',
            fallBackBaseURL: 'http://ert-rater-training.s3-website-us-east-1.amazonaws.com/rater-tablet-test-8c051b15-a34f-43d0-a90e-6235f43779ab',
            pages: {
                default: 'index.html'
            },
            zipFile: true
        }
    },

    eConsent: { url: 'https://demo.secureconsent.com' },

    // The study Protocol
    studyProtocol: {
        defaultProtocol: 0,
        protocolList: new ObjectRef({
            0: 'TestStudy',
            1: 'Protocol1',
            2: 'Protocol2',
            3: 'Protocol3',
            4: 'Protocol4',
            5: 'Protocol5',
            6: 'Protocol6',
            7: 'Protocol7',
            8: 'Protocol8',
            9: 'Protocol9',
            10: 'Protocol10'
        })
    },

    // The build Protocol so if protocol for build
    protocolBuild: 'TestStudy',

    // The client name
    clientName: 'ERT',

    // A Regular Expression or a String of valid characters for defining which characters are allowed for input fields.
    validInputCharacters: 'a-zA-Z0-9\\s\\.-',

    // A list of supported devices for the study.
    /*
     supportedDevices : [{
     name: 'iPhone',
     match: [['Safari']]
     }, {
     name: 'Nexus S',
     match: [['Safari']]
     }, {
     name: 'Galaxy Nexus',
     match: [['Safari']]
     }],
     */

    // The phases used by the study
    studyPhase: new ObjectRef({
        SCREENING: 10,
        RANDOMIZATION: 20,
        TREATMENT: 30,
        FOLLOWUP: 40,
        TERMINATION: 999
    }),

    // The phase from studyPhase to be used as the phase after Subject Assignment
    subjectAssignmentPhase: 10,

    // The phase from studyPhase to be used as the termination phase
    terminationPhase: 999,

    allowEnvironmentSelect: true,

    // Whether subject QR codes should be enabled  and displayed on the Home Screen.
    displaySubjectQRCodes: true,

    // A collection of indicator configurations
    indicators: [
        {
            id: 'Take_Medication1',
            className: 'Take_Pill',
            image: 'PILL_IMAGE',
            label: 'TAKE_PILL'
        },

        // Example of a second indicator
        {
            id: 'Take_Medication2',
            className: 'Take_Pill',
            image: 'PILL_IMAGE',
            label: 'TAKE_PILL'
        },

        // Example of a third indicator
        {
            id: 'Take_Medication3',
            className: 'Take_Pill',
            image: 'PILL_IMAGE',
            label: 'TAKE_PILL'
        }
    ]
};
