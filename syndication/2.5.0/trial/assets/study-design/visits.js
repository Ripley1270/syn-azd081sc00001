/**
 * @fileOverview This is an example visits study configuration file.
 * @author <a href='mailto:bill.calderwood@ert.com'>William A. Calderwood</a>
 * @version 2.1
 */
export default {
    visits: [{
        id: 'visit1',
        studyEventId: '10',
        displayName: 'VISIT_1',
        visitType: 'ordered',
        visitOrder: 1,
        forms: ['P_Welcome_SitePad'],
        delay: '0'
    }, {
        id: 'visit2',
        studyEventId: '20',
        displayName: 'VISIT_2',
        visitType: 'ordered',
        visitOrder: 2,
        forms: ['P_Welcome_SitePad'],
        delay: '0'
    }, {
        id: 'visit3',
        studyEventId: '30',
        displayName: 'VISIT_3',
        visitType: 'ordered',
        visitOrder: 3,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit4',
        studyEventId: '40',
        displayName: 'VISIT_4',
        visitType: 'ordered',
        visitOrder: 4,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad', 'P_Date_Diary_SitePad', 'P_Daily_Diary_SitePad', 'P_Screening_Toss_SitePad'],
        delay: '0'
    }, {
        id: 'visit8',
        studyEventId: '80',
        displayName: 'VISIT_8',
        visitType: 'ordered',
        visitOrder: 5,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit9',
        studyEventId: '90',
        displayName: 'VISIT_9',
        visitType: 'ordered',
        visitOrder: 6,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit10',
        studyEventId: '100',
        displayName: 'VISIT_10',
        visitType: 'ordered',
        visitOrder: 7,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit11',
        studyEventId: '110',
        displayName: 'VISIT_11',
        visitType: 'ordered',
        visitOrder: 8,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad', 'P_Date_Diary_SitePad', 'P_Daily_Diary_SitePad'],
        delay: '0',
        expiration: {
            function: 'isVisitExpired',
            functionParams: { timeoutMinutes: 5 }
        }
    }, {
        id: 'visit12',
        studyEventId: '120',
        displayName: 'VISIT_12',
        visitType: 'ordered',
        visitOrder: 9,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad', 'P_Date_Diary_SitePad', 'P_Daily_Diary_SitePad'],
        delay: '0',
        expiration: {
            function: 'isVisitExpired',
            functionParams: { timeoutMinutes: 10 }
        }
    }, {
        id: 'visit13',
        studyEventId: '130',
        displayName: 'VISIT_13',
        visitType: 'ordered',
        visitOrder: 10,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit14',
        studyEventId: '140',
        displayName: 'VISIT_14',
        visitType: 'ordered',
        visitOrder: 11,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit15',
        studyEventId: '150',
        displayName: 'VISIT_15',
        visitType: 'ordered',
        visitOrder: 12,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit16',
        studyEventId: '160',
        displayName: 'VISIT_16',
        visitType: 'ordered',
        visitOrder: 13,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit17',
        studyEventId: '170',
        displayName: 'VISIT_17',
        visitType: 'ordered',
        visitOrder: 14,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit18',
        studyEventId: '180',
        displayName: 'VISIT_18',
        visitType: 'ordered',
        visitOrder: 15,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit19',
        studyEventId: '190',
        displayName: 'VISIT_19',
        visitType: 'ordered',
        visitOrder: 16,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit20',
        studyEventId: '200',
        displayName: 'VISIT_20',
        visitType: 'ordered',
        visitOrder: 17,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit21',
        studyEventId: '210',
        displayName: 'VISIT_21',
        visitType: 'ordered',
        visitOrder: 18,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit22',
        studyEventId: '220',
        displayName: 'VISIT_22',
        visitType: 'ordered',
        visitOrder: 19,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit23',
        studyEventId: '230',
        displayName: 'VISIT_23',
        visitType: 'ordered',
        visitOrder: 20,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit24',
        studyEventId: '240',
        displayName: 'VISIT_24',
        visitType: 'ordered',
        visitOrder: 21,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit25',
        studyEventId: '250',
        displayName: 'VISIT_25',
        visitType: 'ordered',
        visitOrder: 22,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit26',
        studyEventId: '260',
        displayName: 'VISIT_26',
        visitType: 'unscheduled',
        visitOrder: 23,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad', 'P_Date_Diary_SitePad', 'P_Daily_Diary_SitePad'],
        delay: '0'
    }, {
        id: 'visit27',
        studyEventId: '270',
        displayName: 'VISIT_27',
        visitType: 'container',
        visitOrder: 24,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit28',
        studyEventId: '280',
        displayName: 'VISIT_28',
        visitType: 'unscheduled',
        visitOrder: 25,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'visit29',
        studyEventId: '290',
        displayName: 'VISIT_29',
        visitType: 'container',
        visitOrder: 26,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }, {
        id: 'kitchenSinkVisit',
        studyEventId: '50',
        displayName: 'KITCHEN_SINK_VISIT',
        visitType: 'unscheduled',
        visitOrder: 0,
        forms: ['Rater_Training', 'P_EQ5D_Diary', 'P_RadioButton_Test', 'P_CustomRadioButton_Test', 'P_CustomCheckBox_Test', 'P_NRS_Diary', 'P_Checkbox_Diary', 'P_Meds', 'P_Welcome', 'P_BarcodeDiary', 'P_HorizontalVAS_Diary', 'P_VerticalVAS_Diary', 'P_NumberSpinner_Diary', 'P_Loop1', 'P_Loop2', 'P_Loop3', 'P_NumericTextBox_Diary', 'P_DropDownList_Diary', 'P_Toss_Diary', 'P_NoWidget', 'P_FreeText_Diary', 'P_Matrix_Diary', 'P_DateSpinner_Diary', 'P_Complex_Branching_Test', 'P_Browser_Diary', 'P_Image_Diary'],
        delay: '0'
    }, {
        id: 'visit6',
        studyEventId: '60',
        displayName: 'VISIT_6',
        visitType: 'unscheduled',
        visitOrder: 0,
        forms: ['P_RadioButton_Test', 'P_NRS_Diary'],
        delay: '0'
    }, {
        id: 'visit7',
        studyEventId: '70',
        displayName: 'VISIT_7',
        visitType: 'container',
        visitOrder: 0,
        forms: ['P_RadioButton_Test', 'P_NRS_Diary'],
        delay: '0'
    }, {
        id: 'visitsWorkflowDemo',
        studyEventId: '110',
        displayName: 'VISIT_DEMO',
        visitType: 'unscheduled',
        visitOrder: 0,
        forms: ['P_Welcome_SitePad', 'P_Meds_SitePad'],
        delay: '0'
    }],

    // Server value map. value maps to LF.VisitStates (see application.js)
    visitStatus: {
        1: 'SKIPPED',
        2: 'INCOMPLETE',
        3: 'COMPLETED'
    }
};
