export default {
    schedules: [
        {
            id: 'Custom_Checkbox_Test_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_CustomCheckBox_Test'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['admin', 'site', 'subject']
        }
    ]
};
