export default {
    schedules: [
        {
            id: 'SitePadDemoDiarySchedule',
            target: {
                objectType: 'questionnaire',
                id: 'SPDemo'
            },
            scheduleFunction: 'nTimesPerVisit',
            scheduleParams: {
                visits: [
                    { visitID: 'visit1', maxReports: '2' },
                    { visitID: 'visit2', maxReports: '3' }
                ]
            },
            scheduleRoles: ['subject', 'admin']
        },
        {
            id: 'SitePadSiteDemoDiarySchedule',
            target: {
                objectType: 'questionnaire',
                id: 'SPSiteDemo'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['site', 'subject', 'admin']
        },
        {
            id: 'SitePadSiteDemoDiary2Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'SPSiteDemo2'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['site', 'subject', 'admin']
        }
    ]
};
