export default {
    schedules: [
        {
            id: 'Patient_NumericTextBoxSchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_NumericTextBox_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
