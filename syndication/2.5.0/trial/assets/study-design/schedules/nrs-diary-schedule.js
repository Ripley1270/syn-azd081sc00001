export default {
    schedules: [
        {
            id: 'NRS_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_NRS_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['admin', 'site', 'subject']
        }
    ]
};
