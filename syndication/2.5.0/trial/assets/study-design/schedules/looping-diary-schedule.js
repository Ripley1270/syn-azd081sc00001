export default {
    schedules: [
        {
            id: 'Loop1_Schedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_Loop1'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
