export default {
    schedules: [{
        id: 'Screening_TossSchedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'P_Screening_Toss'
        },
        scheduleFunction: 'checkAlwaysAvailability',
        scheduleRoles: ['site', 'subject']
    }, {
        id: 'Screening_Toss_SP_CS',
        target: {
            objectType: 'questionnaire',
            id: 'P_Screening_Toss_SitePad'
        },
        scheduleFunction: 'nTimesPerVisit',
        scheduleParams: {
            visits: [
                { visitID: 'visit4', maxReports: '1' }
            ]
        },
        scheduleRoles: ['admin', 'site', 'subject']
    }]
};
