export default {
    schedules: [
        {
            id: 'Rater_Training',
            target: {
                objectType: 'questionnaire',
                id: 'Rater_Training'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
