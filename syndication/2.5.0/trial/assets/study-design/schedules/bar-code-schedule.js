export default {
    schedules: [
        {
            id: 'BarcodeSchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_BarcodeDiary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['admin', 'site', 'subject']
        }
    ]
};
