export default {
    schedules: [
        {
            id: 'New_User',
            target: {
                objectType: 'questionnaire',
                id: 'New_User'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['admin', 'subject']
        }
    ]
};
