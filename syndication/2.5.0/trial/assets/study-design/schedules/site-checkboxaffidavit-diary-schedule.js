export default {
    schedules: [
        {
            id: 'Site_CheckBoxAffidavit_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'Site_CheckBoxAffidavit_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['site']
        }
    ]
};
