export default {
    schedules: [
        {
            id: 'Multiple_Choice_Test_02',
            target: {
                objectType: 'questionnaire',
                id: 'P_Checkbox_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['admin', 'site', 'subject']
        }
    ]
};
