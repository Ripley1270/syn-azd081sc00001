export default {
    schedules: [
        {
            id: 'Toss_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_Toss_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['site', 'subject', 'admin']
        }
    ]
};
