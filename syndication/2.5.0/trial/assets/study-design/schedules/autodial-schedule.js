/**
 * @fileOverview This is an example study configuration of schedules used for core development.
 * @author <a href='mailto:bill.calderwood@ert.com'>William A. Calderwood</a>
 * @version 2.0
 */
// A collection of schedule configurations
export default {
    schedules: [
        {
            id: 'AutoDial',
            target: {
                objectType: 'autodial'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            alarmFunction: 'addAlwaysAlarm',
            alarmParams: {
                id: 2,
                time: {
                    startTimeRange: '00:00',
                    endTimeRange: '01:00'
                },
                repeat: 'daily'
            }
        }
    ]
};
