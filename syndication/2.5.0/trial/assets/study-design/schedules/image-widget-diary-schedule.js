export default {
    schedules: [
        {
            id: 'Patient_IMAGE_Test_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_Image_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
