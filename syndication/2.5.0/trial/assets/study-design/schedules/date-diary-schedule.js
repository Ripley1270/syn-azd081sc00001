export default {
    schedules: [{
        id: 'Date_DiarySchedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'P_Date_Diary'
        },
        scheduleFunction: 'checkAlwaysAvailability',
        scheduleRoles: ['site', 'subject']
    }, {
        id: 'Date_DiarySchedule_01_SitePad',
        target: {
            objectType: 'questionnaire',
            id: 'P_Date_Diary_SitePad'
        },
        scheduleFunction: 'nTimesPerVisit',
        scheduleParams: {
            visits: [
                { visitID: 'visit4', maxReports: '1' },
                { visitID: 'visit11', maxReports: '1' },
                { visitID: 'visit12', maxReports: '1' },
                { visitID: 'visit26', maxReports: '1' }
            ]
        },
        scheduleRoles: ['admin', 'site', 'subject']
    }]
};
