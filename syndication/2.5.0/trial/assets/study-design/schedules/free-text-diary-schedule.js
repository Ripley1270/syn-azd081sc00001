export default {
    schedules: [
        {
            id: 'FreeTextDiary_Schedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_FreeText_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['site', 'subject', 'admin']
        }
    ]
};
