export default {
    schedules: [
        {
            id: 'BrowserWidgetDiary_Schedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_Browser_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['site', 'subject', 'admin']
        }
    ]
};
