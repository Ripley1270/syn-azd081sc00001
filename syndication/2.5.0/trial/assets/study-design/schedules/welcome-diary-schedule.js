export default {
    schedules: [{
        id: 'WelcomeSchedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'P_Welcome'
        },
        scheduleRoles: ['admin', 'site', 'subject'],
        scheduleFunction: 'checkAlwaysAvailability',
        alarmFunction: 'addAlwaysAlarm',
        alarmParams: {
            id: 3,
            time: '9:00',
            repeat: 'daily'
        }
    }, {
        id: 'WelcomeSchedule_SitePad',
        target: {
            objectType: 'questionnaire',
            id: 'P_Welcome_SitePad'
        },
        scheduleFunction: 'nTimesPerVisit',
        scheduleParams: {
            visits: [
                { visitID: 'visit1', maxReports: '1' },
                { visitID: 'visit2', maxReports: '3' },
                { visitID: 'visit3', maxReports: '5' },
                { visitID: 'visit4', maxReports: '1' },
                { visitID: 'visit8', maxReports: '1' },
                { visitID: 'visit9', maxReports: '2' },
                { visitID: 'visit10', maxReports: '2' },
                { visitID: 'visit11', maxReports: '1' },
                { visitID: 'visit12', maxReports: '1' },
                { visitID: 'visit13', maxReports: '1' },
                { visitID: 'visit14', maxReports: '1' },
                { visitID: 'visit15', maxReports: '1' },
                { visitID: 'visit16', maxReports: '1' },
                { visitID: 'visit17', maxReports: '1' },
                { visitID: 'visit18', maxReports: '1' },
                { visitID: 'visit19', maxReports: '1' },
                { visitID: 'visit20', maxReports: '1' },
                { visitID: 'visit21', maxReports: '1' },
                { visitID: 'visit22', maxReports: '1' },
                { visitID: 'visit23', maxReports: '1' },
                { visitID: 'visit24', maxReports: '1' },
                { visitID: 'visit25', maxReports: '1' },
                { visitID: 'visit26', maxReports: '1' },
                { visitID: 'visit27', maxReports: '1' },
                { visitID: 'visit28', maxReports: '1' },
                { visitID: 'visit29', maxReports: '1' },
                { visitID: 'visitsWorkflowDemo', maxReports: '2' }
            ]
        },
        scheduleRoles: ['admin', 'site', 'subject']
    }]
};
