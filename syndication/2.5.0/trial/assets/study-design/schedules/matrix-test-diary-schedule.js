export default {
    schedules: [
        {
            id: 'Matrix_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_Matrix_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: [
                'admin',
                'site',
                'subject'
            ]
        }
    ]
};
