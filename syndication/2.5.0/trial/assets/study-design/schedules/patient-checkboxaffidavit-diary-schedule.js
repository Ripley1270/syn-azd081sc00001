export default {
    schedules: [
        {
            id: 'Patient_CheckBoxAffidavit_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_Patient_CheckBoxAffidavit_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject']
        }
    ]
};
