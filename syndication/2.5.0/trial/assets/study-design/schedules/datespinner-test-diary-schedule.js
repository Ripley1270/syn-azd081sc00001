export default {
    schedules: [
        {
            id: 'DateSpinner_Diary_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'P_DateSpinner_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
