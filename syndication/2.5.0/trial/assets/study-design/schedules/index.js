// Non-diary schedules
import AutoDial from './autodial-schedule';
import Indicator from './indicator-schedule';

// Core Diary Schedules
import Add_User_Diary from './add-user-schedule';
import Rater_Training_Diary from './rater-training-schedule';
import Deactivate_User from './deactivate-user-schedule';
import Activate_User from './activate-user-schedule';
import Edit_User from './edit-user-schedule';
import New_Patient_Diary from './new-patient-schedule';
import Skip_Visit_Diary from './skip-visit-schedule';
import Edit_Patient_Diary from './edit-patient-diary-schedule';
import First_Site_User from './first-site-user-schedule';
import Time_Confirmation from './time-confirmation-schedule';

// Test diary schedul3es
import Checkbox_Test_Diary from './checkbox-test-diary-schedule';
import Custom_Checkbox_Diary from './custom-checkbox-schedule';
import Meds_Diary from './meds-diary-schedule';
import NRS_Diary from './nrs-diary-schedule';
import Looping_Diary from './looping-diary-schedule';
import Looping_Diary_2 from './looping-diary-2-schedule';
import Looping_Diary_3 from './looping-diary-3-schedule';
import Bar_Code_Diary from './bar-code-schedule';
import Daily_Diary from './daily-diary-schedule';
import Date_Diary from './date-diary-schedule';
import No_Widget_Diary from './no-widget-schedule';
import Radio_Button_Diary from './radio-button-schedule';
import Custom_Radio_Button_Diary from './custom-radio-button-schedule';
import Screening_Toss_Diary from './screening-toss-schedule';
import Site_Demo_Diary from './site-demo-schedule';
import Site_Diary from './site-diary-schedule';
import Toss_Diary from './toss-diary-schedule';
import Welcome_Diary from './welcome-diary-schedule';
import HVAS_Test_Diary from './hvas-diary-schedule';
import VVAS_Test_Diary from './vvas-diary-schedule';
import EQ5D_Test_Diary from './eq5d-diary-schedule';
import NumberSpinner_Test_Diary from './numberspinner-test-diary-schedule';
import NumericTextbox_Diary from './numeric-textbox-diary-schedule';
import DropDownList from './dropdownlist-test-diary-schedule';
import FreeText_Diary from './free-text-diary-schedule';
import Matrix_Test_Diary from './matrix-test-diary-schedule';
import Date_Spinner from './datespinner-test-diary-schedule';
import Complex_Branching_Diary from './complex-branching-diary-schedule';
import Browser_Diary from './browser-widget-test-schedule';
import Image_Diary from './image-widget-diary-schedule';
import Patient_CheckBoxAffidavit_Diary from './patient-checkboxaffidavit-diary-schedule';
import Site_CheckBoxAffidavit_Diary from './site-checkboxaffidavit-diary-schedule';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects({}, AutoDial, Indicator,
    Add_User_Diary, Custom_Radio_Button_Diary, Custom_Checkbox_Diary, Rater_Training_Diary, New_Patient_Diary, Skip_Visit_Diary, Edit_Patient_Diary,
    First_Site_User, Deactivate_User, Activate_User, Edit_User, Time_Confirmation,
    EQ5D_Test_Diary, Radio_Button_Diary, Checkbox_Test_Diary, Daily_Diary, Welcome_Diary,
    Meds_Diary, No_Widget_Diary, Site_Demo_Diary, Bar_Code_Diary, Toss_Diary, Screening_Toss_Diary, Date_Diary, NRS_Diary,
    HVAS_Test_Diary, VVAS_Test_Diary, Looping_Diary, NumberSpinner_Test_Diary, Site_Diary, NumericTextbox_Diary,
    DropDownList, FreeText_Diary, Matrix_Test_Diary, Date_Spinner, Looping_Diary_2, Looping_Diary_3, Complex_Branching_Diary, Patient_CheckBoxAffidavit_Diary,
    Site_CheckBoxAffidavit_Diary, Browser_Diary, Image_Diary);
