export default {
    schedules: [
        {
            id: 'Patient_DropDownList_Test_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_DropDownList_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
