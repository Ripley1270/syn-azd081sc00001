export default {
    schedules: [
        {
            id: 'NumberSpinner_Diary_Schedule',
            target: {
                objectType: 'questionnaire',
                id: 'P_NumberSpinner_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
