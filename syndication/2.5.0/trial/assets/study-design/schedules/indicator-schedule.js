/**
 * @fileOverview This is an example study configuration of schedules used for core development.
 * @author <a href='mailto:bill.calderwood@ert.com'>William A. Calderwood</a>
 * @version 2.0
 */
// A collection of schedule configurations
export default {
    schedules: [
        {
            id: 'Take_Pill',
            target: {
                objectType: 'indicator',
                id: 'Take_Medication1',
                showOnLogin: true
            },
            scheduleFunction: 'checkRepeatByDateIndicatorAvailability',
            scheduleParams: {
                startAvailability: '02:00',
                endAvailability: '16:00'
            },
            scheduleRoles: ['caregiver']
        }, {
            id: 'Take_Pill_impending',
            target: {
                objectType: 'indicator',
                id: 'Take_Medication2',
                showOnLogin: false
            },
            scheduleFunction: 'checkRepeatByDateIndicatorAvailability',
            scheduleParams: {
                startAvailability: '02:00',
                endAvailability: '16:00',
                customClassname: 'impending'
            },
            scheduleRoles: ['caregiver', 'subject']
        }, {
            id: 'Take_Pill_urgent',
            target: {
                objectType: 'indicator',
                id: 'Take_Medication3',
                showOnLogin: true
            },
            scheduleFunction: 'checkRepeatByDateIndicatorAvailability',
            scheduleParams: {
                startAvailability: '02:00',
                endAvailability: '16:00',
                customClassname: 'urgent'
            }
        }
    ]
};
