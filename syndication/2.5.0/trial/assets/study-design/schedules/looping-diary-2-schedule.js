export default {
    schedules: [
        {
            id: 'Loop2_Schedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_Loop2'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
