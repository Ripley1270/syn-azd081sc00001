export default {
    schedules: [
        {
            id: 'Loop3_Schedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_Loop3'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
