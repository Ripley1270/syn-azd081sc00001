export default {
    schedules: [
        {
            id: 'Patient_HVAS_Test_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_HorizontalVAS_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
