export default {
    schedules: [
        {
            id: 'Complex_Branching_Test_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_Complex_Branching_Test'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['admin', 'site', 'subject']
        }
    ]
};
