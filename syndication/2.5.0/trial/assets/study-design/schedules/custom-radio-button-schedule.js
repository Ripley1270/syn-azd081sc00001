export default {
    schedules: [
        {
            id: 'Custom_Multiple_Choice_Test_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_CustomRadioButton_Test'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['admin', 'site', 'subject']
        }
    ]
};
