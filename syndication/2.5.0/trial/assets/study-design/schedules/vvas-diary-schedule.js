export default {
    schedules: [
        {
            id: 'Patient_VVAS_Test_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_VerticalVAS_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
