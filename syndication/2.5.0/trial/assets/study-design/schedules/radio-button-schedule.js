export default {
    schedules: [
        {
            id: 'Multiple_Choice_Test_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_RadioButton_Test'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['admin', 'site', 'subject']
        }
    ]
};
