export default {
    schedules: [
        {
            id: 'NoWidget_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_NoWidget'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['site', 'subject', 'admin']
        }
    ]
};
