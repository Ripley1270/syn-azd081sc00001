export default {
    schedules: [
        {
            id: 'Patient_EQ5D_Test_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_EQ5D_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ]
};
