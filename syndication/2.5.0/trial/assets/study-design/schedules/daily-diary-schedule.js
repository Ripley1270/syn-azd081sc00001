export default {
    schedules: [{
        id: 'Daily_DiarySchedule_01',
        target: {
            objectType: 'questionnaire',
            id: 'P_Daily_Diary'
        },
        scheduleFunction: 'checkAlwaysAvailability',
        scheduleRoles: ['site', 'subject'],
        alarmFunction: 'addAlwaysAlarm',
        alarmParams: {
            id: 1,
            time: '13:00',
            repeat: 'daily',
            reminders: 3,
            reminderInterval: 15,
            subjectConfig: {
                minAlarmTime: '00:00',
                maxAlarmTime: '19:00',
                alarmRangeInterval: 5,
                alarmOffSubject: true
            }
        }
    }, {
        id: 'Daily_DiarySchedule_01_SitePad',
        target: {
            objectType: 'questionnaire',
            id: 'P_Daily_Diary_SitePad'
        },
        scheduleFunction: 'nTimesPerVisit',
        scheduleParams: {
            visits: [
                { visitID: 'visit4', maxReports: '1' },
                { visitID: 'visit11', maxReports: '1' },
                { visitID: 'visit12', maxReports: '1' },
                { visitID: 'visit26', maxReports: '1' }
            ]
        },
        scheduleRoles: ['admin', 'site', 'subject']
    }]
};
