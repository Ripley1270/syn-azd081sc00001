export default {
    // when constructing a service name from study name, use this template
    serviceUrlFormat: 'https://{{study}}.phtnetpro.com',

    environments: [{
        id: 'individual',
        label: 'Local server (localhost : Syndication)',
        modes: ['provision'],
        url: 'http://localhost:3000',
        studyDbName: 'Syndication'
    }, {
        id: 'staging',
        label: 'Staging (pht-syn-study06 : SYN24_GSSO_02Nov2017)',
        modes: ['provision'],
        url: 'https://pht-syn-study06.phtnetpro.com',
        studyDbName: 'SYN24_GSSO_02Nov2017'
    }, {
        // Production is REQUIRED! Do not remove. // http://localhost:3000
        id: 'production',
        label: 'Production (pht-syn-study18 : MS22Syn_26Aug2016)',
        modes: ['provision', 'depot'],
        url: 'https://pht-syn-study18.phtnetpro.com',
        studyDbName: 'MS22Syn_26Aug2016'
    }, {
        id: 'dev-expert',
        label: 'Dev-Expert (londonbay-dev)',
        modes: ['provision'],
        url: 'http://londonbay-dev.ert.com/059427',
        studyDbName: 'STUDY_DEMO'
    }, {
        id: 'int-expert',
        label: 'Int-Expert (londonbay-int)',
        modes: ['provision'],
        url: 'http://londonbay-int.ert.com/059427',
        studyDbName: 'STUDY_DEMO'
    }, {
        id: 'automation1',
        label: 'lpaAutoStudy01',
        modes: ['provision'],
        url: 'https://combo2study01.phtnetpro.com',
        studyDbName: 'lpaAutoStudy01',
        timeTravel: true
    }, {
        id: 'automation2',
        label: 'lpaAutoStudy02',
        modes: ['provision'],
        url: 'https://combo2study02.phtnetpro.com',
        studyDbName: 'lpaAutoStudy02',
        timeTravel: true
    }, {
        id: 'automation3',
        label: 'lpaAutoStudy03',
        modes: ['provision'],
        url: 'https://combo2study03.phtnetpro.com',
        studyDbName: 'lpaAutoStudy03',
        timeTravel: true
    }, {
        id: 'automation4',
        label: 'lpaAutoStudy04',
        modes: ['provision'],
        url: 'https://combo2study04.phtnetpro.com',
        studyDbName: 'lpaAutoStudy04',
        timeTravel: true
    }, {
        id: 'automation5',
        label: 'lpaAutoStudy05',
        modes: ['provision'],
        url: 'https://combo2study05.phtnetpro.com',
        studyDbName: 'lpaAutoStudy05',
        timeTravel: true
    }, {
        id: 'automation6',
        label: 'lpaAutoStudy06',
        modes: ['provision'],
        url: 'https://combo2study06.phtnetpro.com',
        studyDbName: 'lpaAutoStudy06',
        timeTravel: true
    }, {
        id: 'pht-syn-study02',
        label: 'Restricted_For_PHT-SYN-STUDY02',
        modes: ['provision'],
        url: 'https://pht-syn-study02.phtnetpro.com',
        studyDbName: 'Restricted_For_PHT-SYN-STUDY02',
        timeTravel: true
    }, {
        id: 'syn24_final_23nov2017',
        label: 'Staging (pht-syn-study11 : SYN24_FINAL_23NOV2017)',
        modes: ['provision'],
        url: 'https://pht-syn-study11.phtnetpro.com',
        studyDbName: 'SYN24_FINAL_23NOV2017'
    }, {
        id: 'syn24_final_24nov2017',
        label: 'Staging (pht-syn-study12 : SYN24_FINAL_24NOV2017)',
        modes: ['provision'],
        url: 'https://pht-syn-study12.phtnetpro.com',
        studyDbName: 'SYN24_FINAL_24NOV2017'
    }, {
        id: 'eCOA241_Pulpos',
        label: 'Staging (pht-syn-study14 : eCOA241_Pulpos)',
        modes: ['provision'],
        url: 'https://pht-syn-study14.phtnetpro.com',
        studyDbName: 'eCOA241_Pulpos'
    }, {
        id: 'eCOA25_Pulpos',
        label: 'Staging (pht-syn-study01 : eCOA25_Pulpos)',
        modes: ['provision'],
        url: 'https://pht-syn-study01.phtnetpro.com',
        studyDbName: 'eCOA25_Pulpos'
    }, {
        id: 'SYN241_08FEB2018_FINAL',
        label: 'Staging (pht-syn-study04 : SYN241_08FEB2018_FINAL)',
        modes: ['provision'],
        url: 'https://pht-syn-study04.phtnetpro.com',
        studyDbName: 'SYN241_08FEB2018_FINAL'
    }, {
        id: 'SYN241_09FEB2018_FINAL',
        label: 'Staging (pht-syn-study16 : SYN241_09FEB2018_FINAL)',
        modes: ['provision'],
        url: 'https://pht-syn-study16.phtnetpro.com',
        studyDbName: 'SYN241_09FEB2018_FINAL'
    }, {
        id: 'eCOA25_03MAY2018',
        label: 'Staging (pht-syn-study08 : eCOA25_03MAY2018)',
        modes: ['provision'],
        url: 'https://pht-syn-study08.phtnetpro.com',
        studyDbName: 'eCOA25_03MAY2018'
    }, {
        id: 'eCOA25_28JUN2018',
        label: 'Staging (pht-syn-study19 : eCOA25_28JUN2018)',
        modes: ['provision'],
        url: 'https://pht-syn-study19.phtnetpro.com',
        studyDbName: 'eCOA25_28JUN2018'
    }, {
        id: 'eCOA25FINAL_15JUL2018',
        label: 'eCOA25FINAL_15JUL2018',
        modes: ['provision'],
        url: 'https://pht-syn-study11.phtnetpro.com',
        studyDbName: 'eCOA25FINAL_15JUL2018'
    }, {
        id: 'eCOA25FINAL_16JUL2018',
        label: 'eCOA25FINAL_16JUL2018',
        modes: ['provision'],
        url: 'https://pht-syn-study12.phtnetpro.com',
        studyDbName: 'eCOA25FINAL_16JUL2018'
    }]
};
