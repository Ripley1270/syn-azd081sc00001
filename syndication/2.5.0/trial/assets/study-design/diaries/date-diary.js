export default {
    questionnaires: [{
        id: 'P_Date_Diary',
        SU: 'Date',
        displayName: 'DISPLAY_NAME',
        className: 'DATE_DIARY',
        affidavit: undefined,
        product: ['logpad'],
        screens: [
            'DATE_DIARY_S_1',
            'DATE_DIARY_S_2',
            'DATE_DIARY_S_3',
            'DATE_DIARY_S_4',
            'DATE_DIARY_S_5',
            'DATE_DIARY_S_6',
            'DATE_DIARY_S_7',
            'DATE_DIARY_S_8',
            'DATE_DIARY_S_9'
        ]
    }, {
        id: 'P_Date_Diary_SitePad',
        SU: 'Date',
        displayName: 'DISPLAY_NAME',
        className: 'DATE_DIARY',
        accessRoles: ['admin', 'site'],
        affidavit: undefined,
        product: ['sitepad'],
        screens: [
            // 'DATE_DIARY_S_1',
            // 'DATE_DIARY_S_2',
            // 'DATE_DIARY_S_3',
            'DATE_DIARY_S_4',
            'DATE_DIARY_S_5',
            'DATE_DIARY_S_6',
            'DATE_DIARY_S_7',
            'DATE_DIARY_S_8',
            'DATE_DIARY_S_9'
        ]
    }],

    screens: [{
        id: 'DATE_DIARY_S_1',
        className: 'DATE_DIARY_S_1',
        questions: [
            { id: 'DATE_DIARY_Q_1', mandatory: true }
        ]
    }, {
        id: 'DATE_DIARY_S_2',
        className: 'DATE_DIARY_S_2',
        questions: [
            { id: 'DATE_DIARY_Q_2', mandatory: false }
        ]
    }, {
        id: 'DATE_DIARY_S_3',
        className: 'DATE_DIARY_S_3',
        questions: [
            { id: 'DATE_DIARY_Q_3', mandatory: true }
        ]
    }, {
        id: 'DATE_DIARY_S_4',
        className: 'DATE_DIARY_S_4',
        questions: [
            { id: 'DATE_DIARY_Q_4', mandatory: true }
        ]
    }, {
        id: 'DATE_DIARY_S_5',
        className: 'DATE_DIARY_S_5',
        questions: [
            { id: 'DATE_DIARY_Q_5', mandatory: true }
        ]
    }, {
        id: 'DATE_DIARY_S_6',
        className: 'DATE_DIARY_S_6',
        questions: [
            { id: 'DATE_DIARY_Q_6', mandatory: false }
        ]
    }, {
        id: 'DATE_DIARY_S_7',
        className: 'DATE_DIARY_S_7',
        questions: [
            { id: 'DATE_DIARY_Q_7', mandatory: true }
        ]
    }, {
        id: 'DATE_DIARY_S_8',
        className: 'DATE_DIARY_S_8',
        questions: [
            { id: 'DATE_DIARY_Q_8', mandatory: true }
        ]
    }, {
        id: 'DATE_DIARY_S_9',
        className: 'DATE_DIARY_S_9',
        questions: [
            { id: 'DATE_DIARY_Q_9', mandatory: true }
        ]
    }],

    questions: [{
        id: 'DATE_DIARY_Q_1',
        IG: 'Date',
        IT: 'DATE_1',
        text: ['QUESTION_1'],
        className: 'DATE_DIARY_Q_1',
        widget: {
            id: 'DATE_DIARY_W_1',
            type: 'DatePicker',
            className: 'DATE_DIARY_W_1',
            configuration: {
                maxDays: 10,
                minDays: 10,
                startOffsetYears: 1,
                defaultValue: '2014-04-01'
            }
        }
    }, {
        id: 'DATE_DIARY_Q_2',
        IG: 'Date',
        IT: 'DATE_2',
        skipIT: 'Date_2_SKP',
        text: ['QUESTION_2'],
        className: 'DATE_DIARY_Q_2',
        widget: {
            id: 'DATE_DIARY_W_2',
            type: 'DatePicker',
            className: 'DATE_DIARY_W_2',
            configuration: {
                overrideIsRTL: false,
                minDays: 0,
                startOffsetDays: 7
            }
        }
    }, {
        id: 'DATE_DIARY_Q_3',
        IG: 'Date',
        IT: 'DATE_3',
        text: ['QUESTION_3'],
        className: 'DATE_DIARY_Q_3',
        widget: {
            id: 'DATE_DIARY_W_3',
            type: 'DatePicker',
            className: 'DATE_DIARY_W_3',
            configuration: {
                maxDays: 0
            }
        }
    }, {
        id: 'DATE_DIARY_Q_4',
        IG: 'Date',
        IT: 'DATE_4',
        text: ['QUESTION_4'],
        className: 'DATE_DIARY_Q_4',
        widget: {
            id: 'DATE_DIARY_W_4',
            type: 'DatePicker',
            className: 'DATE_DIARY_W_4',
            configuration: {
                maxDays: 10,
                minDays: 10,
                startOffsetYears: 1,
                defaultValue: '2014-04-01'
            }
        }
    }, {
        id: 'DATE_DIARY_Q_5',
        IG: 'Date',
        IT: 'DATE_5',
        text: ['QUESTION_5'],
        className: 'DATE_DIARY_Q_5',
        widget: {
            id: 'DATE_DIARY_W_5',
            type: 'TimePicker',
            className: 'DATE_DIARY_W_5',
            configuration: {
                minHour: 2,
                maxHour: 12,
                defaultValue: '10:10:00',
                minuteStep: 15
            }
        }
    }, {
        id: 'DATE_DIARY_Q_6',
        IG: 'Date',
        IT: 'DATE_6',
        skipIT: 'DATE_6_SKP',
        text: ['QUESTION_6'],
        className: 'DATE_DIARY_Q_6',
        widget: {
            id: 'DATE_DIARY_W_6',
            type: 'TimePicker',
            className: 'DATE_DIARY_W_6',
            configuration: {
                minHour: 12,
                minuteStep: 15
            }
        }
    }, {
        id: 'DATE_DIARY_Q_7',
        IG: 'Date',
        IT: 'DATE_7',
        text: ['QUESTION_7'],
        className: 'DATE_DIARY_Q_7',
        widget: {
            id: 'DATE_DIARY_W_7',
            type: 'TimePicker',
            className: 'DATE_DIARY_W_7',
            configuration: {}
        }
    }, {
        id: 'DATE_DIARY_Q_8',
        IG: 'Date',
        IT: 'DATE_8',
        text: ['QUESTION_8'],
        className: 'DATE_DIARY_Q_8',
        widget: {
            id: 'DATE_DIARY_W_8',
            type: 'DateTimePicker',
            className: 'DATE_DIARY_W_8',
            min: '2014-01-01T10:22:00',
            max: '2014-02-01T11:33:00',
            showLabels: true,
            configuration: {
                timeConfiguration: {}
            }
        }
    }, {
        id: 'DATE_DIARY_Q_9',
        IG: 'Date',
        IT: 'DATE_9',
        text: ['QUESTION_9'],
        className: 'DATE_DIARY_Q_9',
        widget: {
            id: 'DATE_DIARY_W_9',
            type: 'DateTimePicker',
            className: 'DATE_DIARY_W_9',
            min: '2014-01-01T10:22:00',
            configuration: {
                defaultValue: '2014-02-01T10:34:00',
                timeConfiguration: {}
            }
        }
    }]
};
