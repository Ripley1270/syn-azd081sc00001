export default {
    questionnaires: [
        {
            id: 'P_Patient_CheckBoxAffidavit_Diary',
            SU: 'P_Patient_CheckBoxAffidavit_Diary',
            displayName: 'DISPLAY_NAME',
            className: 'P_Patient_CheckBoxAffidavit_Diary',
            previousScreen: true,
            affidavit: 'P_CheckBoxAffidavit',
            screens: [
                'Patient_CheckBoxAffidavit_DIARY_S_1'
            ]
        }
    ],

    screens: [
        {
            id: 'Patient_CheckBoxAffidavit_DIARY_S_1',
            className: 'Patient_CheckBoxAffidavit_DIARY_S_1',
            questions: [{
                id: 'Patient_CheckBoxAffidavit_DIARY_Q1',
                mandatory: true
            }]
        }
    ],

    questions: [
        {
            id: 'Patient_CheckBoxAffidavit_DIARY_Q1',
            IG: 'Patient_CheckBoxAffidavit',
            IT: 'Patient_CheckBoxAffidavit_Q1',
            title: 'Patient_CheckBoxAffidavit_TITLE',
            text: [
                'Patient_CheckBoxAffidavit_TEXT'
            ],
            widget: {
                id: 'PATIENT_CHECKBOXAFFIDAVIT_TEST_S1_Q1',
                type: 'CheckBox',
                label: 'Pick a number',
                templates: {},
                answers: [
                    { text: 'TEST_ANSWER', value: '0' }
                ]
            },
            className: 'Patient_CheckBoxAffidavit_DIARY_Q1'
        }
    ]
};
