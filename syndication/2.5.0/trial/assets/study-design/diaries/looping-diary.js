export default {
    questionnaires: [
        {
            id: 'P_Loop1',
            SU: 'Loop1',
            displayName: 'DISPLAY_NAME',
            previousScreen: true,
            affidavit: 'DEFAULT',
            accessRoles: ['subject'],
            noRoleFound: 'STUDY_NO_ROLE_FOUND',
            screens: [
                'LOOP1_S_1',
                'LOOP1_S_2',
                'LOOP1_S_3',
                'LOOP1_S_4',
                'LOOP1_S_5',

                // Uncomment the last two screens for TC15425
                'LOOP1_S_6',
                'LOOP1_S_7'
            ],
            branches: [
                {
                    branchFrom: 'LOOP1_S_2',
                    branchTo: 'LOOP1_S_7',
                    clearBranchedResponses: false,
                    branchFunction: 'always'
                }, {
                    branchFrom: 'LOOP1_S_4',
                    branchTo: 'LOOP1_S_2',
                    clearBranchedResponses: false,
                    branchFunction: 'toReviewScreenLoop1'
                }, {
                    branchFrom: 'LOOP1_S_6',
                    branchTo: 'LOOP1_S_2',
                    clearBranchedResponses: false,
                    branchFunction: 'toReviewScreenLoop1'
                }, {
                    branchFrom: 'LOOP1_S_5',
                    branchTo: 'LOOP1_S_2',
                    clearBranchedResponses: false,
                    branchFunction: 'toReviewScreenLoop1'
                }, {
                    branchFrom: 'LOOP1_S_3',
                    branchTo: 'LOOP1_S_5',
                    clearBranchedResponses: true,
                    branchFunction: 'selectedMotrin'
                }, {
                    branchFrom: 'LOOP1_S_3',
                    branchTo: 'LOOP1_S_6',
                    clearBranchedResponses: true,
                    branchFunction: 'selectedOxycontin'
                }
            ]
        }
    ],
    screens: [
        {
            id: 'LOOP1_S_1',
            className: 'LOOP1_S_1',
            questions: [{
                id: 'LOOP1_Q1',
                mandatory: true
            }]
        }, {
            id: 'LOOP1_S_2',
            className: 'LOOP1_S_2',
            questions: [{
                id: 'LOOP1_Q2',
                mandatory: false
            }]
        }, {
            id: 'LOOP1_S_3',
            className: 'LOOP1_S_3',
            questions: [{
                id: 'LOOP1_Q3',
                mandatory: true
            }]
        }, {
            id: 'LOOP1_S_4',
            className: 'LOOP1_S_4',
            questions: [{
                id: 'LOOP1_Q4',
                mandatory: true
            }]
        }, {
            id: 'LOOP1_S_5',
            className: 'LOOP1_S_5',
            accessRoles: ['subject'],
            questions: [{
                id: 'LOOP1_Q5',
                mandatory: true
            }]
        }, {
            id: 'LOOP1_S_6',
            className: 'LOOP1_S_6',
            questions: [{
                id: 'LOOP1_Q6',
                mandatory: true
            }]
        }, {
            id: 'LOOP1_S_7',
            className: 'LOOP1_S_7',
            questions: [{
                id: 'LOOP1_Q7',
                mandatory: true
            }]
        }
    ],
    questions: [
        {
            id: 'LOOP1_Q1',
            IG: 'Loop1A',
            IT: 'MedsToday',
            text: 'QUESTION_1',
            className: 'LOOP1_Q1',
            widget: {
                id: 'LOOP1_W1',
                type: 'RadioButton',
                templates: {},
                answers: [{
                    text: 'YES',
                    value: '1'
                }, {
                    text: 'NO',
                    value: '0'
                }]
            }
        }, {
            id: 'LOOP1_Q2',
            IG: 'Loop1B',
            repeating: true,
            text: 'QUESTION_2',
            className: 'LOOP1_Q2',
            widget: {
                id: 'LOOP1_W2',
                type: 'ReviewScreen',
                screenFunction: 'medsLoop1',
                templates: {
                    itemsTemplate: 'LOOP1:ScreenItems'
                },
                buttons: [{
                    id: 1,
                    availability: 'limitReached',
                    text: 'ADD_MEDS',
                    actionFunction: 'addNewMedsLoop1'
                }, {
                    id: 2,
                    text: 'EDIT_MEDS',
                    actionFunction: 'editMedLoop1'
                }, {
                    id: 3,
                    text: 'DELETE_MEDS',
                    actionFunction: 'deleteMedLoop1'
                },

                // UNCOMMENT THE FOLLOWING FOR TC15232
                {
                    id: 4,
                    text: 'INSERT_MEDS',
                    actionFunction: 'insertMedLoop1'
                }]
            }
        }, {
            id: 'LOOP1_Q3',
            IG: 'Loop1B',
            IT: 'MedLoop',
            text: 'QUESTION_3',
            className: 'LOOP1_Q3',
            widget: {
                id: 'LOOP1_W3',
                type: 'RadioButton',
                answers: [{
                    text: 'ADVIL',
                    value: '0'
                }, {
                    text: 'ALEVE',
                    value: '1'
                }, {
                    text: 'ASPIRIN',
                    value: '2'
                }, {
                    text: 'EXCEDRIN',
                    value: '3'
                }, {
                    text: 'MOTRIN',
                    value: '4'
                }, {
                    text: 'OXYCONTIN',
                    value: '5'
                }, {
                    text: 'TYLENOL',
                    value: '6'
                }, {
                    text: 'VICODIN',
                    value: '7'
                }]
            }
        }, {
            id: 'LOOP1_Q4',
            IG: 'Loop1B',
            IT: 'MedTime',
            text: 'QUESTION_4',
            className: 'LOOP1_Q4',
            widget: {
                id: 'LOOP1_W4',
                type: 'TimePicker',
                configuration: {
                }
            }
        }, {
            id: 'LOOP1_Q5',
            IG: 'Loop1B',
            IT: 'MedTime',
            text: 'QUESTION_5',
            className: 'LOOP1_Q5',
            widget: {
                id: 'LOOP1_W5',
                type: 'TimePicker',
                configuration: {
                }
            }
        }, {
            id: 'LOOP1_Q6',
            IG: 'Loop1B',
            IT: 'MedTime',
            text: 'QUESTION_6',
            className: 'LOOP1_Q6',
            widget: {
                id: 'LOOP1_W6',
                type: 'TimePicker',
                configuration: {
                }
            }
        }, {
            id: 'LOOP1_Q7',
            IG: 'Loop1B',
            text: 'QUESTION_7',
            className: 'LOOP1_Q7',
            widget: {
                id: 'LOOP1_W7',
                type: 'ReviewScreen',
                screenFunction: 'medsLoop1',
                templates: {
                    itemsTemplate: 'LOOP1:ScreenItems'
                }
            }
        }
    ]
};
