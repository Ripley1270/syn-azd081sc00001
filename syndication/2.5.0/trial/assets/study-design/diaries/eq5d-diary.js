export default {
    questionnaires: [
        {
            id: 'P_EQ5D_Diary',
            SU: 'EQ5D',
            displayName: 'DISPLAY_NAME',
            className: 'Custom_EQ5D_Class',
            affidavit: undefined,
            previousScreen: true,
            screens: [
                'EQ5D_TEST_S1',
                'EQ5D_TEST_S2',
                'EQ5D_TEST_S3',
                'EQ5D_TEST_S4'
            ]
        }
    ],
    screens: [
        {
            id: 'EQ5D_TEST_S1',
            className: 'EQ5D_TEST_S1',
            questions: [
                {
                    id: 'EQ5D_TEST_S1_Q1',
                    mandatory: false
                }
            ]
        },
        {
            id: 'EQ5D_TEST_S2',
            className: 'EQ5D_TEST_S2',
            questions: [
                {
                    id: 'EQ5D_TEST_S2_Q1',
                    mandatory: false
                }
            ]
        },
        {
            id: 'EQ5D_TEST_S3',
            className: 'EQ5D_TEST_S3',
            questions: [
                {
                    id: 'EQ5D_TEST_S3_Q1',
                    mandatory: false
                }
            ]
        },
        {
            id: 'EQ5D_TEST_S4',
            className: 'EQ5D_TEST_S4',
            questions: [
                {
                    id: 'EQ5D_TEST_S4_Q1',
                    mandatory: false
                }
            ]
        }
    ],
    questions: [
        {
            id: 'EQ5D_TEST_S1_Q1',
            IG: 'EQ5D',
            IT: 'EQ5D0',
            skipIT: 'EQ5D0_SKP',
            title: 'EQ5D_Q1_TITLE',
            text: ['EQ5D_Q0_TEXT'],
            className: 'EQ5D_TEST_S2_Q1'
        },

        // tests default anchor values, basic appearance
        {
            id: 'EQ5D_TEST_S2_Q1',
            IG: 'EQ5D',
            IT: 'EQ5D1',
            skipIT: 'EQ5D1_SKP',
            title: 'EQ5D_Q1_TITLE',
            text: ['EQ5D_LOGPAD_INSTRUCTION'],
            className: 'EQ5D_TEST_S2_Q1',
            widget: {
                id: 'EQ5D_TEST_W1',
                type: 'VAS',
                displayAs: 'EQ5D',
                anchors: {
                    min: {
                        text: 'WORST_HEALTH'
                    },
                    max: {
                        text: 'BEST_HEALTH'
                    }
                },
                customProperties: {
                    instructionText: '',
                    answerLabelText: 'EQ5D_ANSWER_LABEL_TEXT'
                }
            }
        },

        {
            id: 'EQ5D_TEST_S3_Q1',
            IG: 'EQ5D',
            IT: 'EQ5D2',
            skipIT: 'EQ5D2_SKP',
            title: 'EQ5D_Q2_TITLE',
            text: ['EQ5D_Q2_TEXT'],
            className: 'EQ5D_TEST_S3_Q1',
            widget: {
                id: 'EQ5D_TEST_W1',
                type: 'VAS',
                displayAs: 'EQ5D',
                anchors: {
                    min: {
                        text: 'WORST_HEALTH'
                    },
                    max: {
                        text: 'BEST_HEALTH'
                    }
                },
                selectedValue: {
                    selectionBox: {
                        fillColor: 'blue'
                    }
                },
                tickMarks: {
                    frequency: 5,
                    majorSize: 10,
                    minorSize: 5,
                    displayMinorTicks: true
                },
                customProperties: {
                    instructionText: 'EQ5D_SITEPAD_INSTRUCTION',
                    answerLabelText: 'EQ5D_ANSWER_LABEL_TEXT',
                    footerText: 'EQ5D_SITEPAD_FOOTER_TEXT',
                    topBottomMargin: 40,
                    tickValuePosition: 'left'
                }
            }
        },

        // tests default anchor values, basic appearance
        {
            id: 'EQ5D_TEST_S4_Q1',
            IG: 'EQ5D',
            IT: 'EQ5D4',
            skipIT: 'EQ5D4_SKP',
            title: 'EQ5D_Q4_TITLE',
            text: ['EQ5D_CUSTOM_CSS_INSTRUCTION'],
            className: 'EQ5D_TEST_S4_Q1',
            widget: {
                id: 'EQ5D_TEST_W4',
                type: 'VAS',
                displayAs: 'EQ5D',
                anchors: {
                    min: {
                        text: 'WORST_HEALTH'
                    },
                    max: {
                        text: 'BEST_HEALTH'
                    }
                },
                customProperties: {
                    instructionText: '',
                    answerLabelText: 'EQ5D_ANSWER_LABEL_TEXT'
                }
            }
        }
    ]
};
