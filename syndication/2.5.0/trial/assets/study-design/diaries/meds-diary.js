export default {
    questionnaires: [{
        id: 'P_Meds',
        SU: 'Meds',
        displayName: 'DISPLAY_NAME',
        className: 'medication',

        // Choose default affidavit per user role
        affidavit: undefined,
        triggerPhase: 'FOLLOWUP',
        product: ['logpad'],
        screens: [
            'MEDS_S_1',
            'MEDS_S_2',
            'MEDS_S_3',
            'MEDS_S_4'
        ]
    }, {
        id: 'P_Meds_SitePad',
        SU: 'Meds',
        displayName: 'DISPLAY_NAME',
        className: 'medication',
        accessRoles: ['admin', 'site'],

        // Choose default affidavit per user role
        affidavit: undefined,

        // triggerPhase: 'FOLLOWUP',
        product: ['sitepad'],
        screens: [
            'MEDS_S_1',
            'MEDS_S_2',
            'MEDS_S_3',
            'MEDS_S_4'
        ]
    }],

    screens: [{
        id: 'MEDS_S_1',
        className: 'MEDS_S_1',
        questions: [
            { id: 'MEDS_Q_1', mandatory: true }
        ]
    }, {
        id: 'MEDS_S_2',
        className: 'MEDS_S_2',
        questions: [
            { id: 'MEDS_Q_2', mandatory: false }
        ]
    }, {
        id: 'MEDS_S_3',
        className: 'MEDS_S_3',
        questions: [
            { id: 'MEDS_Q_3', mandatory: true }
        ]
    }, {
        id: 'MEDS_S_4',
        className: 'MEDS_S_4',
        questions: [
            { id: 'MEDS_Q_4', mandatory: true }
        ]
    }],

    questions: [{
        id: 'MEDS_Q_1',
        IG: 'Meds',
        IT: 'MEDS_Q1',
        text: 'QUESTION_1',
        className: 'RadioButton',
        widget: {
            type: 'RadioButton',
            className: 'PILLS',
            answers: [
                { text: 'HALF_PILL', value: '0' },
                { text: 'ONE_PILL', value: '1' },
                { text: 'TWO_PILLS', value: '2' }
            ]
        }
    }, {
        id: 'MEDS_Q_2',
        IG: 'Meds',
        IT: 'MEDS_Q2',
        text: ['QUESTION_2A', 'QUESTION_2B'],
        className: 'MEDS_Q_2'
    }, {
        id: 'MEDS_Q_3',
        IG: 'Meds',
        IT: 'MEDS_Q3',
        text: 'QUESTION_3A',
        className: 'MEDS_Q_3',
        widget: {
            id: 'MEDS_Q_3',
            type: 'ReviewScreen',
            className: 'MedsReview',
            screenFunction: 'medsList',
            buttons: [{
                id: 1,
                availability: 'always',
                text: 'ALERT',
                actionFunction: 'alertAction'
            }, {
                id: 2,
                availability: 'always',
                text: 'ADD',
                actionFunction: 'addAction'
            }, {
                id: 3,
                availability: 'always',
                text: 'EDIT',
                actionFunction: 'editAction'
            }, {
                id: 4,
                availability: 'always',
                text: 'DELETE',
                actionFunction: 'deleteAction'
            }]
        }
    }, {
        id: 'MEDS_Q_4',
        IG: 'Meds',
        IT: 'MEDS_Q3',
        text: 'QUESTION_4A',
        className: 'MEDS_Q_4',
        widget: {
            id: 'MEDS_Q_W4',
            type: 'ReviewScreen',
            className: 'MedsReview',
            screenFunction: 'medsList_Q4',
            buttons: [{
                id: 1,
                availability: 'always',
                text: 'ALERT',
                actionFunction: 'alertAction'
            }, {
                id: 2,
                availability: 'always',
                text: 'EDIT',
                actionFunction: 'editAction_Q4'
            }]
        }
    }],

    rules: [
        /*
         * Example: Logout after questionnaire is completed.
         */
        {
            id: 'LogoutQuestionnaire',
            trigger: 'QUESTIONNAIRE:Transmit/P_Meds_SitePad',
            evaluate: ['AND', 'isSitePad', { expression: 'isVisit', input: 'visitsWorkflowDemo' }],
            resolve: [
                { action: 'displayMessage' },
                { action: 'removeMessage' },
                { action: 'preventDefault' },
                { action: 'logout' }
            ]
        }
    ]
};
