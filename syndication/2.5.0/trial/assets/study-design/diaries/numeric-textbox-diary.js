export default {
    questionnaires: [
        {
            id: 'P_NumericTextBox_Diary',
            SU: 'NumericTextBox',
            displayName: 'DISPLAY_NAME',
            className: 'NumericTextBox',
            affidavit: undefined,
            previousScreen: true,
            screens: [
                'NTB_TEST_S1',
                'NTB_TEST_S2',
                'NTB_TEST_S3',
                'NTB_TEST_SReview'
            ]
        }
    ],
    screens: [
        {
            id: 'NTB_TEST_S1',
            className: 'NTB_TEST_S1',
            questions: [
                {
                    id: 'NTB_TEST_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'NTB_TEST_S2',
            className: 'NTB_TEST_S2',
            questions: [
                {
                    id: 'NTB_TEST_Q2',
                    mandatory: true
                }
            ]
        }, {
            id: 'NTB_TEST_S3',
            className: 'NTB_TEST_S3',
            questions: [
                {
                    id: 'NTB_TEST_Q3',
                    mandatory: false
                }
            ]
        }, {
            id: 'NTB_TEST_SReview',
            className: 'NTB_TEST_SReview',
            questions: [
                {
                    id: 'NTB_TEST_QReview',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'NTB_TEST_Q1',
            IG: 'NumericTextBox',
            IT: 'NumericTextBox1',
            title: 'NTB_Q1_TITLE',
            text: ['NTB_Q1_TEXT'],
            className: 'NTB_TEST_S1_Q1',
            widget: {
                id: 'NTB_W1',
                className: 'NTB_W1',
                type: 'NumericTextBox',
                min: 0,
                max: 100,
                label: 'NTB_Q1_LABEL',
                validationErrors: [
                    {
                        property: 'isInputValid',
                        errorType: 'popup',
                        errString: 'VALIDATION_MESSAGE_1',
                        header: 'VALIDATION_HEADER',
                        ok: 'VALIDATION_OK'
                    }
                ]
            }
        }, {
            id: 'NTB_TEST_Q2',
            IG: 'NumericTextBox',
            IT: 'NumericTextBox2',
            title: 'NTB_Q2_TITLE',
            text: ['NTB_Q2_TEXT'],
            className: 'NTB_TEST_S1_Q2',
            widget: {
                id: 'NTB_W2',
                className: 'NTB_W2',
                type: 'NumericTextBox',
                min: 1,
                max: 50,
                step: '.01',
                validationErrors: [
                    {
                        property: 'isInputValid',
                        errorType: 'popup',
                        errString: 'VALIDATION_MESSAGE_2',
                        header: 'VALIDATION_HEADER',
                        ok: 'VALIDATION_OK'
                    }
                ]
            }
        }, {
            id: 'NTB_TEST_Q3',
            IG: 'NumericTextBox',
            IT: 'NumericTextBox3',
            skipIT: 'NTB_TEST_Q3_SKP',
            title: 'NTB_Q3_TITLE',
            text: ['NTB_Q3_TEXT'],
            className: 'NTB_TEST_S1_Q3',
            widget: {
                id: 'NTB_W3',
                className: 'NTB_W3',
                type: 'NumericTextBox',
                min: 1,
                max: 50,
                step: 1,
                validationErrors: [
                    {
                        property: 'isInputValid',
                        errorType: 'popup',
                        errString: 'VALIDATION_MESSAGE_3',
                        header: 'VALIDATION_HEADER',
                        ok: 'VALIDATION_OK'
                    }
                ]
            }
        }, {
            id: 'NTB_TEST_QReview',
            IG: 'NumericTextBox',
            title: 'SUMMARY_TITLE',
            text: ['SUMMARY_QUESTION'],
            className: 'NTB_TEST_QReview'
        }
    ]
};
