export default {
    questionnaires: [
        {
            id: 'P_Loop2',
            SU: 'Loop2',
            displayName: 'DISPLAY_NAME',
            previousScreen: true,
            affidavit: 'DEFAULT',
            screens: [
                'LOOP2_S_1',
                'LOOP2_S_2',
                'LOOP2_S_3',
                'LOOP2_S_4',
                'LOOP2_S_5'
            ],
            branches: [
                {
                    branchFrom: 'LOOP2_S_1',
                    branchTo: 'LOOP2_S_2',
                    clearBranchedResponses: false,
                    branchFunction: 'toFoodReviewScreenLoop2'
                }, {
                    branchFrom: 'LOOP2_S_3',
                    branchTo: 'LOOP2_S_5',
                    clearBranchedResponses: false,
                    branchFunction: 'insertNumMedsIT'
                }, {
                    branchFrom: 'LOOP2_S_4',
                    branchTo: 'LOOP2_S_3',
                    clearBranchedResponses: false,
                    branchFunction: 'toMedsReviewScreenLoop2'
                }
            ]
        }
    ],
    screens: [
        {
            id: 'LOOP2_S_1',
            className: 'LOOP2_S_1',
            questions: [
                {
                    id: 'LOOP2_Q1',
                    mandatory: true
                }]
        }, {
            id: 'LOOP2_S_2',
            className: 'LOOP2_S_2',
            questions: [
                {
                    id: 'LOOP2_Q2',
                    mandatory: true
                }]
        }, {
            id: 'LOOP2_S_3',
            className: 'LOOP2_S_3',
            questions: [
                {
                    id: 'LOOP2_Q3',
                    mandatory: true
                }]
        }, {
            id: 'LOOP2_S_4',
            className: 'LOOP2_S_4',
            questions: [
                {
                    id: 'LOOP2_Q4',
                    mandatory: true
                }]
        }, {
            id: 'LOOP2_S_5',
            className: 'LOOP2_S_5',
            questions: [
                {
                    id: 'LOOP2_Q5',
                    mandatory: true
                }]
        }
    ],
    questions: [
        {
            id: 'LOOP2_Q1',
            IG: 'Loop2A',
            IT: 'FoodLoop',
            text: 'QUESTION_1',
            widget: {
                id: 'LOOP2_W1',
                type: 'RadioButton',
                template: { },
                answers: [
                    {
                        text: 'APPLE',
                        value: '0'
                    }, {
                        text: 'BANANA',
                        value: '1'
                    }, {
                        text: 'CHICKEN',
                        value: '2'
                    }, {
                        text: 'DONUT',
                        value: '3'
                    }, {
                        text: 'HAMBURGER',
                        value: '4'
                    }, {
                        text: 'PASTA',
                        value: '5'
                    }, {
                        text: 'PIZZA',
                        value: '6'
                    }, {
                        text: 'SALAD',
                        value: '7'
                    }
                ]
            }
        }, {
            id: 'LOOP2_Q2',
            IG: 'Loop2A',
            repeating: true,
            text: 'QUESTION_2',
            widget: {
                id: 'LOOP2_W2',
                type: 'ReviewScreen',
                screenFunction: 'foodLoop2',
                validation: {
                    validationFunc: 'checkForFood'
                },
                templates: {},
                buttons: [
                    {
                        id: 1,
                        availability: 'always',
                        text: 'ADD_FOOD',
                        actionFunction: 'addNewFoodLoop2'
                    }, {
                        id: 2,
                        text: 'DELETE_FOOD',
                        actionFunction: 'deleteFoodLoop2'
                    }, {
                        id: 3,
                        text: 'EDIT_FOOD',
                        actionFunction: 'editFoodLoop2'
                    }
                ]
            }
        }, {
            id: 'LOOP2_Q3',
            IG: 'Loop2B',
            repeating: true,
            text: 'QUESTION_3',
            widget: {
                id: 'LOOP2_W3',
                type: 'ReviewScreen',
                screenFunction: 'medsLoop2',
                templates: {},
                buttons: [
                    {
                        id: 1,
                        availability: 'always',
                        text: 'ADD_MEDS',
                        actionFunction: 'addNewMedsLoop2'
                    }, {
                        id: 2,
                        text: 'DELETE_MEDS',
                        actionFunction: 'deleteMedsLoop2'
                    }, {
                        id: 3,
                        text: 'EDIT_MEDS',
                        actionFunction: 'editMedsLoop2'
                    }
                ]
            }
        }, {
            id: 'LOOP2_Q4',
            IG: 'Loop2B',
            IT: 'PainMeds',
            text: 'QUESTION_4',
            widget: {
                id: 'LOOP2_W4',
                type: 'RadioButton',
                template: { },
                answers: [
                    {
                        text: 'ADVIL',
                        value: '0'
                    }, {
                        text: 'ALEVE',
                        value: '1'
                    }, {
                        text: 'ASPIRIN',
                        value: '2'
                    }, {
                        text: 'EXCEDRIN',
                        value: '3'
                    }, {
                        text: 'MOTRIN',
                        value: '4'
                    }, {
                        text: 'OXYCONTIN',
                        value: '5'
                    }, {
                        text: 'TYLENOL',
                        value: '6'
                    }, {
                        text: 'VICODIN',
                        value: '7'
                    }
                ]
            }
        }, {
            id: 'LOOP2_Q5',
            IG: 'Loop2C',
            IT: 'SympSelect',
            text: 'QUESTION_5',
            widget: {
                id: 'LOOP2_W5',
                type: 'RadioButton',
                template: { },
                answers: [
                    {
                        text: 'ABSENT',
                        value: '0'
                    }, {
                        text: 'MILD_SYMPTOMS',
                        value: '1'
                    }, {
                        text: 'MOD_SYMPTOMS',
                        value: '2'
                    }, {
                        text: 'SEV_SYMPTOMS',
                        value: '3'
                    }
                ]
            }
        }
    ]
};
