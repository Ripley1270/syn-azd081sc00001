export default {
    questionnaires: [
        {
            id: 'P_CustomRadioButton_Test',
            SU: 'CustomRadiobutton_Test',
            displayName: 'DISPLAY_NAME',
            className: 'CUSTOMRADIOBUTTON_TEST',
            affidavit: undefined,
            previousScreen: true,
            screens: ['CUSTOMRADIOBUTTON_TEST_S1', 'CUSTOMRADIOBUTTON_TEST_S2']
        }
    ],

    screens: [
        {
            id: 'CUSTOMRADIOBUTTON_TEST_S1',
            className: 'CUSTOMRADIOBUTTON_TEST_S1',
            questions: [
                { id: 'CUSTOMRADIOBUTTON_TEST_S1_Q1', mandatory: true }
            ]
        },
        {
            id: 'CUSTOMRADIOBUTTON_TEST_S2',
            className: 'CUSTOMRADIOBUTTON_TEST_S2',
            questions: [
                { id: 'CUSTOMRADIOBUTTON_TEST_S2_Q1', mandatory: true }
            ]
        }
    ],

    questions: [
        {
            id: 'CUSTOMRADIOBUTTON_TEST_S1_Q1',
            IG: 'CutomRadiobutton_Test',
            IT: 'CUSTOMRADIOBUTTON_TEST_S1_Q1',
            skipIT: '',
            title: 'QUESTION_1_TITLE',
            text: ['QUESTION_1'],
            className: 'CUSTOMRADIOBUTTON_TEST_S1_Q1',
            widget: {
                id: 'CUSTOMRADIOBUTTON_TEST_S1_Q1',
                type: 'CustomRadioButton',
                label: 'Pick a number',
                templates: {},
                answers: [
                    { text: 'TEXT1', value: 'Try Again' },
                    { text: 'TEXT2', value: 'Skip Communication(Warning: New PEs will not be captured if you choose this option.' }
                ]
            }
        },
        {
            id: 'CUSTOMRADIOBUTTON_TEST_S2_Q1',
            IG: 'CutomRadiobutton_Test',
            IT: 'CUSTOMRADIOBUTTON_TEST_S2_Q1',
            skipIT: '',
            title: 'QUESTION_1_TITLE',
            text: ['QUESTION_1'],
            className: 'CUSTOMRADIOBUTTON_TEST_S2_Q1',
            widget: {
                id: 'CUSTOMRADIOBUTTON_TEST_S2_Q1',
                type: 'CustomRadioButton',
                label: 'Pick a number',
                templates: {},
                answers: [
                    { text: 'TEXT1', value: 'Try Again' },
                    { text: 'TEXT3', value: 'Skip Communication(Warning: New PEs will not be captured if you choose this option.' }
                ]
            }
        }
    ]
};
