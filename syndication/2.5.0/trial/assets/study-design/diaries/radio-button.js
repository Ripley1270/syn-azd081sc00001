export default {
    questionnaires: [
        {
            id: 'P_RadioButton_Test',
            SU: 'Radiobutton_Test',
            displayName: 'DISPLAY_NAME',
            className: 'RADIOBUTTON_TEST',
            affidavit: undefined,
            previousScreen: true,
            screens: [
                'RADIOBUTTON_TEST_S1',
                'RADIOBUTTON_TEST_S2',
                'RADIOBUTTON_TEST_S3',
                'RADIOBUTTON_TEST_S4',
                'RADIOBUTTON_TEST_S5',
                'RADIOBUTTON_TEST_S6',
                'RADIOBUTTON_TEST_S7',
                'RADIOBUTTON_TEST_S8',
                'RADIOBUTTON_TEST_S9'
            ],
            branches: []
        }
    ],

    screens: [
        {
            id: 'RADIOBUTTON_TEST_S1',
            className: 'RADIOBUTTON_TEST_S1',
            questions: [
                { id: 'RADIOBUTTON_TEST_S1_Q1', mandatory: true }
            ]
        },

        // Non-sequential values
        {
            id: 'RADIOBUTTON_TEST_S2',
            className: 'RADIOBUTTON_TEST_S2',
            questions: [
                { id: 'RADIOBUTTON_TEST_S2_Q1', mandatory: true }
            ]
        },

        // Custom CSS
        {
            id: 'RADIOBUTTON_TEST_S3',
            className: 'RADIOBUTTON_TEST_S3',
            questions: [
                { id: 'RADIOBUTTON_TEST_S3_Q1', mandatory: true }
            ]
        },

        // Custom answer CSS
        {
            id: 'RADIOBUTTON_TEST_S4',
            className: 'RADIOBUTTON_TEST_S4',
            questions: [
                { id: 'RADIOBUTTON_TEST_S4_Q1', mandatory: true }
            ]
        },

        // Multiple heights
        {
            id: 'RADIOBUTTON_TEST_S5',
            className: 'RADIOBUTTON_TEST_S5',
            questions: [
                { id: 'RADIOBUTTON_TEST_S5_Q1', mandatory: true }
            ]
        },

        // Centered text
        {
            id: 'RADIOBUTTON_TEST_S6',
            className: 'RADIOBUTTON_TEST_S6',
            questions: [
                { id: 'RADIOBUTTON_TEST_S6_Q1', mandatory: true }
            ]
        },
        {
            id: 'RADIOBUTTON_TEST_S7',
            className: 'RADIOBUTTON_TEST_S7',
            questions: [
                { id: 'RADIOBUTTON_TEST_S7_Q1', mandatory: true }
            ]
        },

        // Display answers
        {
            id: 'RADIOBUTTON_TEST_S8',
            className: 'RADIOBUTTON_TEST_S8',
            questions: [
                {
                    id: 'RADIOBUTTON_TEST_S8_Q1',
                    mandatory: false
                }
            ]
        },

        // Display answers
        {
            id: 'RADIOBUTTON_TEST_S9',
            className: 'RADIOBUTTON_TEST_S9',
            questions: [
                { id: 'RADIOBUTTON_TEST_S9_Q1', mandatory: false }
            ]
        }
    ],

    questions: [
        {
            id: 'RADIOBUTTON_TEST_S1_Q1',
            IG: 'Radiobutton_Test',
            IT: 'RADIOBUTTON_TEST_S1_Q1',
            skipIT: '',
            title: 'QUESTION_1_TITLE',
            text: ['QUESTION_1'],
            className: 'RADIOBUTTON_TEST_S1_Q1',
            widget: {
                id: 'RADIOBUTTON_TEST_S1_Q1',
                type: 'RadioButton',
                label: 'Pick a number',
                templates: {},
                answer: () => {
                    return new Q.Promise((resolve) => {
                        resolve('3');
                    });
                },
                answers: [
                    { text: 'ONE_TWO_LINES', value: '1' },
                    { text: 'TWO_LONG', value: '2' },
                    { text: 'THREE', value: '3' },
                    { text: 'FOUR', value: '4' }
                ]
            }
        },

        // Radio button test Screen 2
        {
            id: 'RADIOBUTTON_TEST_S2_Q1',
            IG: 'Radiobutton_Test',
            IT: 'RADIOBUTTON_TEST_S2_Q1',
            skipIT: '',
            title: 'QUESTION_2_TITLE',
            text: ['QUESTION_2'],
            className: '',
            widget: {
                id: 'RADIOBUTTON_TEST_S2_Q1',
                type: 'RadioButton',
                label: 'Pick a number',
                templates: {},
                answers: [
                    { text: 'ONE', value: '2' },
                    { text: 'FIVE', value: '5' },
                    { text: 'SEVEN', value: '7' },
                    { text: 'EIGHT', value: '8' }
                ]
            }
        },

        // Radio button test Screen 3
        {
            id: 'RADIOBUTTON_TEST_S3_Q1',
            IG: 'Radiobutton_Test',
            IT: 'RADIOBUTTON_TEST_S3_Q1',
            skipIT: '',
            title: 'QUESTION_3_TITLE',
            text: ['QUESTION_3'],
            className: 'RadioButton',
            widget: {
                id: 'RADIOBUTTON_TEST_S3_Q1',
                type: 'RadioButton',
                label: 'Pick a number',
                templates: {},
                answers: [
                    { text: 'ONE', value: '1' },
                    { text: 'TWO', value: '2' },
                    { text: 'THREE', value: '3' },
                    { text: 'FOUR', value: '4' }
                ]
            }
        },
        {
            id: 'RADIOBUTTON_TEST_S4_Q1',
            IG: 'Radiobutton_Test',
            IT: 'RADIOBUTTON_TEST_S4_Q1',
            skipIT: '',
            title: 'QUESTION_4_TITLE',
            text: ['QUESTION_4'],
            className: 'RADIOBUTTON_TEST_S4_Q1',
            widget: {
                id: 'RADIOBUTTON_TEST_S4_Q1',
                type: 'RadioButton',
                label: 'Pick a number',
                templates: {},
                answers: [
                    { text: 'ONE', value: '1' },
                    { text: 'TWO', value: '2' },
                    { text: 'THREE', value: '3' },
                    { text: 'FOUR', value: '4' }
                ]
            }
        },
        {
            id: 'RADIOBUTTON_TEST_S5_Q1',
            IG: 'Radiobutton_Test',
            IT: 'RADIOBUTTON_TEST_S5_Q1',
            skipIT: '',
            title: 'QUESTION_5_TITLE',
            text: ['QUESTION_5'],
            className: 'RADIOBUTTON_TEST_S5_Q1',
            widget: {
                id: 'RADIOBUTTON_TEST_S5_Q1',
                type: 'RadioButton',
                label: 'Pick a number',
                templates: {},
                answers: [
                    { text: 'ONE', value: '1' },
                    { text: 'TWO', value: '2' },
                    { text: 'THREE', value: '3' },
                    { text: 'FOUR', value: '4' }
                ]
            }
        },
        {
            id: 'RADIOBUTTON_TEST_S6_Q1',
            IG: 'Radiobutton_Test',
            IT: 'RADIOBUTTON_TEST_S6_Q1',
            skipIT: '',
            title: 'QUESTION_6_TITLE',
            text: ['QUESTION_6'],
            className: 'RADIOBUTTON_TEST_S6_Q1',
            widget: {
                id: 'RADIOBUTTON_TEST_S6_Q1',
                type: 'RadioButton',
                label: 'Pick a number',
                templates: {},
                answers: [
                    { text: 'ONE', value: '1' },
                    { text: 'TWO', value: '2' },
                    { text: 'THREE', value: '3' },
                    { text: 'FOUR', value: '4' }
                ]
            }
        },
        {
            id: 'RADIOBUTTON_TEST_S7_Q1',
            IG: 'Radiobutton_Test',
            IT: 'RADIOBUTTON_TEST_S7_Q1',
            skipIT: 'RBSkip',
            title: 'QUESTION_7_TITLE',
            text: ['QUESTION_7'],
            className: 'RADIOBUTTON_TEST_S7_Q1',
            widget: {
                id: 'RADIOBUTTON_TEST_S7_Q1',
                type: 'RadioButton',
                label: 'Pick a number',
                templates: {},
                paramFunctions: [{
                    id: 'answerFunc',
                    evaluate: 'radioButtonAnswersTest',
                    screenshots: {
                        values: [
                            [
                                { text: 'DYNAMIC_ONE', value: '0' },
                                { text: 'DYNAMIC_TWO', value: '1' },
                                { text: 'DYNAMIC_THREE', value: '2' }
                            ],
                            [
                                { text: 'DYNAMIC_ONE', value: '0' },
                                { text: 'DYNAMIC_TWO', value: '1' }
                            ]
                        ]
                    }
                }, {
                    id: 'answer',
                    evaluate: 'radioButtonAnswer',
                    screenshots: {
                        values: ['1', '0']
                    }
                }]
            }
        }, {
            id: 'RADIOBUTTON_TEST_S8_Q1',
            IG: 'Radiobutton_Test',
            IT: 'RADIOBUTTON_TEST_S8_Q1',
            skipIT: '',
            title: 'QUESTION_8_TITLE',
            text: ['QUESTION_8'],
            className: 'RADIOBUTTON_TEST_S8_Q1',
            widget: {
                id: 'RADIOBUTTON_TEST_S8_Q1',
                type: 'RadioButton',
                label: 'Pick a number',
                templates: {},
                paramFunctions: [{
                    id: 'answerFunc',
                    evaluate: 'radioButtonAnswers2',
                    screenshots: {
                        values: [
                            [
                                { text: 'DYNAMIC_ONE', value: '0' },
                                { text: 'DYNAMIC_TWO', value: '1' }
                            ]
                        ]
                    }
                }]
            }
        },
        {
            id: 'RADIOBUTTON_TEST_S9_Q1',
            IG: 'Radiobutton_Test',
            IT: 'RADIOBUTTON_TEST_S9_Q1',
            skipIT: '',
            title: 'QUESTION_9_TITLE',
            text: ['QUESTION_9'],
            className: 'RADIOBUTTON_TEST_S9_Q1'
        }
    ]
};
