export default {
    questionnaires: [{
        id: 'P_Daily_Diary',
        SU: 'Daily',
        displayName: 'DISPLAY_NAME',
        className: 'DAILY_DIARY',
        affidavit: 'CustomAffidavit',
        previousScreen: true,
        triggerPhase: 'TREATMENT',
        product: ['logpad'],
        screens: [
            'DAILY_DIARY_S_1',
            'DAILY_DIARY_S_2',
            'DAILY_DIARY_S_3',
            'DAILY_DIARY_S_4',
            'DAILY_DIARY_S_5',
            'DAILY_DIARY_S_6'
        ],
        branches: [{
            branchFrom: 'DAILY_DIARY_S_4',
            branchTo: 'AFFIDAVIT',
            clearBranchedResponses: true,
            branchFunction: 'isCurrentPhase',
            branchParams: {
                value: 'SCREENING'
            }
        }]
    }, {
        id: 'P_Daily_Diary_SitePad',
        SU: 'Daily',
        displayName: 'DISPLAY_NAME',
        className: 'DAILY_DIARY',
        affidavit: 'CustomAffidavit',
        previousScreen: true,
        triggerPhase: 'TREATMENT',
        allowTranscriptionMode: true,
        product: ['sitepad'],
        screens: [
            'DAILY_DIARY_S_1',
            'DAILY_DIARY_S_2',
            'DAILY_DIARY_S_3',
            'DAILY_DIARY_S_4',
            'DAILY_DIARY_S_5',
            'DAILY_DIARY_S_6'
        ],
        branches: [{
            branchFrom: 'DAILY_DIARY_S_4',
            branchTo: 'AFFIDAVIT',
            clearBranchedResponses: true,
            branchFunction: 'isCurrentPhase',
            branchParams: {
                value: 'SCREENING'
            }
        }]
    }],

    screens: [{
        id: 'DAILY_DIARY_S_1',
        className: 'DAILY_DIARY_S_1',
        questions: [
            { id: 'DAILY_DIARY_Q_1', mandatory: false }
        ]
    }, {
        id: 'DAILY_DIARY_S_2',
        className: 'DAILY_DIARY_S_2',
        questions: [
            { id: 'DAILY_DIARY_Q_2', mandatory: true }
        ]
    }, {
        id: 'DAILY_DIARY_S_3',
        className: 'DAILY_DIARY_S_3',
        questions: [
            { id: 'DAILY_DIARY_Q_3', mandatory: true }
        ]
    }, {
        id: 'DAILY_DIARY_S_4',
        className: 'DAILY_DIARY_S_4',
        questions: [
            { id: 'DAILY_DIARY_Q_4', mandatory: true }
        ]
    }, {
        id: 'DAILY_DIARY_S_5',
        className: 'DAILY_DIARY_S_5',
        questions: [
            { id: 'DAILY_DIARY_Q_5', mandatory: true }
        ]
    }, {
        id: 'DAILY_DIARY_S_6',
        className: 'DAILY_DIARY_S_6',
        questions: [
            { id: 'DAILY_DIARY_Q_6', mandatory: true }
        ]
    }],

    questions: [{
        id: 'DAILY_DIARY_Q_1',
        IG: 'Daily',
        IT: 'DAILY_Q1',
        skipIT: 'DAILY_Q1_SKP',
        title: 'QUESTION_1_TITLE',
        text: [
            'HELP_TEXT',
            'QUESTION_1'
        ],
        className: 'DAILY_DIARY_Q_1',
        widget: {
            id: 'DAILY_DIARY_W_1',
            type: 'CustomRadioButton',
            templates: {},
            answers: [
                { text: 'NO_PROBLEMS_WALKING', value: '0' },
                { text: 'SOME_PROBLEMS_WALKING', value: '1' },
                { text: 'UNABLE_WALKING', value: '2', type: 'number' }
            ]
        }
    }, {
        id: 'DAILY_DIARY_Q_2',
        IG: 'Daily',
        IT: 'DAILY_Q2',
        title: 'QUESTION_2_TITLE',
        text: [
            'HELP_TEXT',
            'QUESTION_2'
        ],
        className: 'DAILY_DIARY_Q_2',
        widget: {
            id: 'DAILY_DIARY_W_2',
            type: 'RadioButton',
            className: 'RadioButton',
            templates: {},
            answers: [
                { text: 'NO_PROBLEMS_SELF_CARE', value: '0' },
                { text: 'SOME_PROBLEMS_SELF_CARE', value: '1' },
                { text: 'UNABLE_SELF_CARE', value: '2' }
            ]
        }
    }, {
        id: 'DAILY_DIARY_Q_3',
        IG: 'Daily',
        IT: 'DAILY_Q3',
        title: 'QUESTION_3_TITLE',
        text: [
            'HELP_TEXT',
            'QUESTION_3'
        ],
        className: 'DAILY_DIARY_Q_3',
        widget: {
            id: 'DAILY_DIARY_W_3',
            type: 'RadioButton',
            className: 'RadioButton',
            templates: {},
            answers: [
                { text: 'NO_PROBLEMS_USUAL_ACTIVITIES', value: '0' },
                { text: 'SOME_PROBLEMS_USUAL_ACTIVITIES', value: '1' },
                { text: 'UNABLE_USUAL_ACTIVITIES', value: '2' }
            ]
        }
    }, {
        id: 'DAILY_DIARY_Q_4',
        IG: 'Daily',
        IT: 'DAILY_Q4',
        title: 'QUESTION_4_TITLE',
        text: [
            'HELP_TEXT',
            'QUESTION_4'
        ],
        className: 'DAILY_DIARY_Q_4',
        widget: {
            id: 'DAILY_DIARY_W_4',
            type: 'RadioButton',
            className: 'RadioButton',
            templates: {},
            answers: [
                { text: 'NO_PAIN', value: '0' },
                { text: 'SOME_PAIN', value: '1' },
                { text: 'EXTREME_PAIN', value: '2' }
            ]
        }
    }, {
        id: 'DAILY_DIARY_Q_5',
        IG: 'Daily',
        IT: 'DAILY_Q5',
        title: 'QUESTION_5_TITLE',
        text: [
            'HELP_TEXT',
            'QUESTION_5'
        ],
        className: 'DAILY_DIARY_Q_5',
        widget: {
            id: 'DAILY_DIARY_W_5',
            type: 'RadioButton',
            className: 'RadioButton',
            templates: {},
            answers: [
                { text: 'NO_ANXIETY', value: '0' },
                { text: 'SOME_ANXIETY', value: '1' },
                { text: 'EXTREME_ANXIETY', value: '2' }
            ]
        }
    }, {
        id: 'DAILY_DIARY_Q_6',
        IG: 'Daily',
        IT: 'DAILY_Q6',
        text: 'QUESTION_6',
        title: 'QUESTION_6_TITLE',
        className: 'DAILY_DIARY_Q_6',
        widget: {
            id: 'DAILY_DIARY_W_6',
            type: 'RadioButton',
            className: 'RadioButton',
            templates: {},
            answers: [
                { text: 'BEST_HEALTHSTATE', value: '0' },
                { text: 'WORST_HEALTHSTATE', value: '10' }
            ]
        }
    }]
};
