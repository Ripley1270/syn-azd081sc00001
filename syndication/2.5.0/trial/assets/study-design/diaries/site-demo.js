export default {
    questionnaires: [
        {
            id: 'SPDemo',
            SU: 'SPDemo',
            displayName: 'DISPLAY_NAME',
            className: 'SPDemo',
            previousScreen: false,
            affidavit: undefined,
            screens: [
                'SPDEMO_S_1'
            ]
        },
        {
            id: 'SPSiteDemo',
            SU: 'SPSiteDemo',
            displayName: 'DISPLAY_NAME',
            className: 'SPSiteDemo',
            previousScreen: false,
            accessRoles: ['admin', 'site'],
            affidavit: undefined,
            screens: [
                'SPSITEDEMO_S_1',
                'SPSITEDEMO_S_2',
                'SPSITEDEMO_S_3'
            ]
        },
        {
            id: 'SPSiteDemo2',
            SU: 'SPSiteDemo2',
            displayName: 'DISPLAY_NAME',
            className: 'SPSiteDemo2',
            previousScreen: false,
            accessRoles: ['admin', 'site'],
            affidavit: undefined,
            screens: [
                'SPSITEDEMO2_S_1'
            ]
        }
    ],

    screens: [
        {
            id: 'SPDEMO_S_1',
            className: 'SPDEMO_S_1',
            questions: [
                {
                    id: 'SPDEMO_Q_1',
                    mandatory: true
                }
            ]
        }, {
            id: 'SPSITEDEMO2_S_1',
            className: 'SPSITEDEMO2_S_1',
            questions: [
                { id: 'SPSITEDEMO2_Q_1', mandatory: true }
            ]
        }, {
            id: 'SPSITEDEMO_S_1',
            className: 'SPSITEDEMO_S_1',
            questions: [
                {
                    id: 'SPSITEDEMO_Q_1'
                }
            ]
        }, {
            id: 'SPSITEDEMO_S_2',
            className: 'SPSITEDEMO_S_2',
            questions: [
                {
                    id: 'SPSITEDEMO_Q_2'
                }
            ]
        }, {
            id: 'SPSITEDEMO_S_3',
            className: 'SPSITEDEMO_S_3',
            questions: [
                {
                    id: 'SPSITEDEMO_Q_3'
                }
            ]
        }
    ],

    questions: [
        {
            id: 'SPDEMO_Q_1',
            IG: 'SPDemo',
            IT: 'SPDEMO_Q1',
            text: 'QUESTION_1',
            className: 'SPDEMO_Q1',
            widget: {
                type: 'RadioButton',
                className: 'PILLS',
                answers: [
                    { text: 'HALF_PILL', value: '0' },
                    { text: 'ONE_PILL', value: '1' },
                    { text: 'TWO_PILLS', value: '2' }
                ]
            }
        },
        {
            id: 'SPSITEDEMO_Q_1',
            IG: 'Date',
            IT: 'DATE_5',
            text: ['QUESTION_5'],
            className: 'DATE_DIARY_Q_5',
            widget: {
                id: 'DATE_DIARY_W_5',
                type: 'TimePicker',
                className: 'DATE_DIARY_W_5',
                showLabels: false,
                configuration: {
                    minHour: 8,
                    maxHour: 14,
                    defaultValue: '10:10:00',
                    timeFormat: '24'
                }
            }
        },
        {
            id: 'SPSITEDEMO_Q_2',
            IG: 'DatePicker',
            IT: 'DATE_PICKER',
            text: ['QUESTION_DATE_PICKER'],
            className: 'DATE_DIARY_Q_DATE_PICKER',
            widget: {
                id: 'DATE_DIARY_W_DATE_PICKER',
                type: 'DatePicker',
                className: 'DATE_DIARY_W_DATE_PICKER',
                configuration: {},
                configFunc: 'activationDate'
            }
        },
        {
            id: 'SPSITEDEMO_Q_3',
            IG: 'DateTimePicker',
            IT: 'DATE__TIME_PICKER',
            text: ['QUESTION_DATE_TIMEPICKER'],
            className: 'DATE_DIARY_Q_DATE_TIME_PICKER',
            widget: {
                id: 'DATE_DIARY_W_DATE_TIME_PICKER',
                type: 'DateTimePicker',
                className: 'DATE_DIARY_W_DATE__TIMEPICKER',
                configuration: {
                    timeConfiguration: {}
                },
                configFunc: 'activationDate'
            }
        },
        {
            id: 'SPDEMO2_Q_1',
            IG: 'SPDemo2',
            IT: 'SPDEMO2_Q1',
            text: 'QUESTION_1',
            className: 'SPDEMO2_Q1',
            widget: {
                type: 'RadioButton',
                className: 'RadioButton',
                answers: [
                    { text: 'HALF_PILL', value: '0' },
                    { text: 'ONE_PILL', value: '1' },
                    { text: 'TWO_PILLS', value: '2' }
                ]
            }
        },
        {
            id: 'SPSITEDEMO2_Q_1',
            IG: 'SPSiteDemo2',
            IT: 'SPSiteDEMO2_Q1',
            text: 'QUESTION_1',
            className: 'RadioButton',
            widget: {
                type: 'RadioButton',
                className: 'PILLS',
                answers: [
                    { text: 'HALF_PILL', value: '0' },
                    { text: 'ONE_PILL', value: '1' },
                    { text: 'TWO_PILLS', value: '2' }
                ]
            }
        }
    ]
};
