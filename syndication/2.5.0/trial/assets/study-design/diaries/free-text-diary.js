export default {
    questionnaires: [
        {
            id: 'P_FreeText_Diary',
            SU: 'Free_Text',
            displayName: 'DISPLAY_NAME',
            className: 'FreeText',
            previousScreen: true,
            accessRoles: ['subject', 'site', 'admin'],
            affidavit: 'P_SignatureAffidavit',
            screens: [
                'FreeText_S_0',
                'FreeText_S_1',
                'FreeText_S_2',
                'FreeText_S_3'
            ]
        }
    ],

    screens: [
        {
            id: 'FreeText_S_0',
            className: 'FreeText_S_0',
            questions: [
                { id: 'FreeText_Q_0', mandatory: true }
            ]
        }, {
            id: 'FreeText_S_1',
            className: 'FreeText_S_1',
            questions: [
                { id: 'FreeText_Q_1', mandatory: false }
            ]
        }, {
            id: 'FreeText_S_2',
            className: 'FreeText_S_2',
            questions: [
                { id: 'FreeText_Q_2', mandatory: true }
            ]
        }, {
            id: 'FreeText_S_3',
            className: 'FreeText_S_3',
            questions: [
                { id: 'FreeText_Q_3', mandatory: true }
            ]
        }
    ],

    questions: [
        {
            id: 'FreeText_Q_0',
            IT: 'FreeText_Q_0',
            IG: 'Free_Text',
            text: 'QUESTION_0',
            skipIT: 'FreeText_Q_0_SKP',
            className: 'FreeText_Q_0',
            widget: {
                id: 'FreeText_Q_0_W0',
                type: 'FreeTextBox',
                className: 'FreeText_Q_0_W0',
                text: 'CHARS_LEFT',
                maxLength: '50',
                showCount: 'false'
            }
        }, {
            id: 'FreeText_Q_1',
            IT: 'FreeText_Q_1',
            IG: 'Free_Text',
            text: 'QUESTION_1',
            skipIT: 'FreeText_Q_1_SKP',
            className: 'FreeText_Q_1',
            widget: {
                id: 'FreeText_Q_1_W0',
                type: 'FreeTextBox',
                className: 'FreeText_Q_1_W0',
                placeholder: 'PLACEHOLDER_Q_2',
                text: 'CHARS_LEFT',
                maxLength: '250',
                rows: '3',
                showCount: 'true',
                resize: 'vertical'
            }
        }, {
            id: 'FreeText_Q_2',
            IT: 'FreeText_Q_2',
            IG: 'Free_Text',
            text: 'QUESTION_2',
            skipIT: 'FreeText_Q_2_SKP',
            className: 'FreeText_Q_2',
            widget: {
                id: 'FreeText_Q_2_W0',
                type: 'FreeTextBox',
                className: 'FreeText_Q_2_W0',
                placeholder: 'PLACEHOLDER_Q_2',
                defaultVal: 'def',
                text: 'CHARS_LEFT',
                maxLength: '5',
                rows: '1',
                resize: 'none'
            }
        }, {
            id: 'FreeText_Q_3',
            IG: 'Free_Text',
            title: 'SUMMARY_TITLE',
            text: ['SUMMARY_QUESTION'],
            className: 'FreeText_Q_3'
        }
    ]
};
