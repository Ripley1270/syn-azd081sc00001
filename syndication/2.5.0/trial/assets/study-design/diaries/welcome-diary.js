export default {
    questionnaires: [{
        id: 'P_Welcome',
        SU: 'Welcome',
        displayName: 'DISPLAY_NAME',
        className: 'introduction',
        affidavit: 'CustomAffidavit',
        product: ['logpad'],
        screens: [
            'WELCOME_S_1',
            'WELCOME_S_1_A',
            'WELCOME_S_2',
            'WELCOME_S_2_A',
            'WELCOME_S_3',
            'WELCOME_S_3_A',
            'WELCOME_S_4'
        ],
        branches: [{
            branchFrom: 'WELCOME_S_1',
            branchTo: 'WELCOME_S_2',
            clearBranchedResponses: true,
            branchFunction: 'equal',
            branchParams: {
                questionId: 'WELCOME_Q_1',
                value: 1
            }
        }, {
            branchFrom: 'WELCOME_S_2',
            branchTo: 'WELCOME_S_3',
            clearBranchedResponses: true,
            branchFunction: 'equal',
            branchParams: {
                questionId: 'WELCOME_Q_2',
                value: 1
            }
        }, {
            branchFrom: 'WELCOME_S_3',
            branchTo: 'WELCOME_S_4',
            clearBranchedResponses: true,
            branchFunction: 'equal',
            branchParams: {
                questionId: 'WELCOME_Q_3',
                value: 1
            }
        }]
    }, {
        id: 'P_Welcome_SitePad',
        SU: 'Welcome',
        displayName: 'DISPLAY_NAME',
        className: 'introduction',
        affidavit: 'CustomAffidavit',
        accessRoles: ['subject'],
        allowTranscriptionMode: true,
        product: ['sitepad'],
        screens: [
            'WELCOME_S_1',
            'WELCOME_S_1_A',
            'WELCOME_S_2',
            'WELCOME_S_2_A',
            'WELCOME_S_3',
            'WELCOME_S_3_A',
            'WELCOME_S_4'
        ],
        branches: [{
            branchFrom: 'WELCOME_S_1',
            branchTo: 'WELCOME_S_2',
            clearBranchedResponses: true,
            branchFunction: 'equal',
            branchParams: {
                questionId: 'WELCOME_Q_1',
                value: 1
            }
        }, {
            branchFrom: 'WELCOME_S_2',
            branchTo: 'WELCOME_S_3',
            clearBranchedResponses: true,
            branchFunction: 'equal',
            branchParams: {
                questionId: 'WELCOME_Q_2',
                value: 1
            }
        }, {
            branchFrom: 'WELCOME_S_3',
            branchTo: 'WELCOME_S_4',
            clearBranchedResponses: true,
            branchFunction: 'equal',
            branchParams: {
                questionId: 'WELCOME_Q_3',
                value: 1
            }
        }]
    }],

    screens: [{
        id: 'WELCOME_S_1',
        className: 'WELCOME_S_1',
        questions: [
            { id: 'WELCOME_Q_1', mandatory: true }
        ]
    }, {
        id: 'WELCOME_S_1_A',
        className: 'WELCOME_S_1_A',
        questions: [
            { id: 'WELCOME_Q_1_A', mandatory: true }
        ]
    }, {
        id: 'WELCOME_S_2',
        className: 'WELCOME_S_2',
        questions: [
            { id: 'WELCOME_Q_2', mandatory: true }
        ]
    }, {
        id: 'WELCOME_S_2_A',
        className: 'WELCOME_S_2_A',
        questions: [
            { id: 'WELCOME_Q_2_A', mandatory: true }
        ]
    }, {
        id: 'WELCOME_S_3',
        className: 'WELCOME_S_3',
        questions: [
            { id: 'WELCOME_Q_3', mandatory: true }
        ]
    }, {
        id: 'WELCOME_S_3_A',
        className: 'WELCOME_S_3_A',
        questions: [
            { id: 'WELCOME_Q_3_A', mandatory: true }
        ]
    }, {
        id: 'WELCOME_S_4',
        className: 'WELCOME_S_4',
        questions: [
            { id: 'WELCOME_Q_4', mandatory: true }
        ]
    }],

    questions: [{
        id: 'WELCOME_Q_1',
        IG: 'Welcome',
        IT: 'WELCOME_Q1',
        text: 'QUESTION_1',
        templates: {
            question: 'Welcome:Question'
        },
        className: 'WELCOME_Q_1',
        widget: {
            id: 'WELCOME_W_1',
            type: 'CustomRadioButton',
            className: 'CustomRadioButton YES_NO',
            templates: {
                container: 'Welcome:HorizontalButtonGroup',
                wrapper: 'DEFAULT:HorizontalWrapper'
            },
            answers: [
                { text: 'YES', value: '0' },
                { text: 'NO', value: '1' }
            ]
        }
    }, {
        id: 'WELCOME_Q_1_A',
        IG: 'Welcome',
        IT: 'WELCOME_Q1A',
        text: 'QUESTION_1_A',
        templates: {
            question: 'Welcome:Question'
        },
        className: 'WELCOME_Q_1_A',
        widget: {
            id: 'WELCOME_W_1_A',
            type: 'RadioButton',
            className: 'RadioButton',
            templates: {
                container: 'Welcome:MiniRadioButtons'
            },
            answers: [
                { text: 'ALL_OF_THE_TIME', value: '0' },
                { text: 'MOST_OF_THE_TIME', value: '1' },
                { text: 'SOME_OF_THE_TIME', value: '2' },
                { text: 'LITTLE_OF_THE_TIME', value: '3' }
            ]
        }
    }, {
        id: 'WELCOME_Q_2',
        IG: 'Welcome',
        IT: 'WELCOME_Q2',
        text: 'QUESTION_2',
        templates: {
            question: 'Welcome:Question'
        },
        className: 'WELCOME_Q_2',
        widget: {
            id: 'WELCOME_W_2',
            type: 'CustomRadioButton',
            className: 'CustomRadioButton YES_NO',
            templates: {
                container: 'Welcome:HorizontalButtonGroup',
                wrapper: 'DEFAULT:HorizontalWrapper'
            },
            answers: [
                { text: 'YES', value: '0' },
                { text: 'NO', value: '1' }
            ]
        }
    }, {
        id: 'WELCOME_Q_2_A',
        IG: 'Welcome',
        IT: 'WELCOME_Q2A',
        text: 'QUESTION_2_A',
        templates: {
            question: 'Welcome:Question'
        },
        className: 'WELCOME_Q_2_A',
        widget: {
            id: 'WELCOME_W_2_A',
            type: 'RadioButton',
            className: 'RadioButton',
            templates: {
                container: 'Welcome:MiniRadioButtons'
            },
            answers: [
                { text: 'ALL_OF_THE_TIME', value: '0' },
                { text: 'MOST_OF_THE_TIME', value: '1' },
                { text: 'SOME_OF_THE_TIME', value: '2' },
                { text: 'LITTLE_OF_THE_TIME', value: '3' }
            ]
        }
    }, {
        id: 'WELCOME_Q_3',
        IG: 'Welcome',
        IT: 'WELCOME_Q3',
        text: 'QUESTION_3',
        templates: {
            question: 'Welcome:Question'
        },
        className: 'WELCOME_Q_3',
        widget: {
            id: 'WELCOME_W_3',
            type: 'CustomRadioButton',
            className: 'CustomRadioButton YES_NO',
            templates: {
                container: 'Welcome:HorizontalButtonGroup',
                wrapper: 'DEFAULT:HorizontalWrapper'
            },
            answers: [
                { text: 'YES', value: '0' },
                { text: 'NO', value: '1' }
            ]
        }
    }, {
        id: 'WELCOME_Q_3_A',
        IG: 'Welcome',
        IT: 'WELCOME_Q3A',
        text: 'QUESTION_3_A',
        templates: {
            question: 'Welcome:Question'
        },
        className: 'WELCOME_Q_3_A',
        widget: {
            id: 'WELCOME_W_3_A',
            type: 'RadioButton',
            className: 'RadioButton',
            templates: {
                container: 'Welcome:MiniRadioButtons'
            },
            answers: [
                { text: 'ALL_OF_THE_TIME', value: '0' },
                { text: 'MOST_OF_THE_TIME', value: '1' },
                { text: 'SOME_OF_THE_TIME', value: '2' },
                { text: 'LITTLE_OF_THE_TIME', value: '3' }
            ]
        }
    }, {
        id: 'WELCOME_Q_4',
        IG: 'Welcome',
        text: ['QUESTION_4'],
        className: 'WELCOME_Q_4',
        widget: {
            id: 'WELCOME_Q_4_W1',
            type: 'ReviewScreen',
            screenFunction: 'branchingReview',
            className: 'WelcomeReview',
            buttons: [{
                text: 'ALERT',
                id: '1',
                availability: 'always',
                actionFunction: 'alertAction_Welcome'
            }, {
                text: 'EDIT',
                id: '2',
                actionFunction: 'editAction_Welcome'
            }]
        }
    }],

    rules: [
        /*
         * Example: Launch another questionnaire after questionnaire is completed.
         */
        {
            id: 'LaunchNextQuestionnaire',
            trigger: 'QUESTIONNAIRE:Transmit/P_Welcome_SitePad',
            evaluate: ['AND', 'isSitePad', { expression: 'isVisit', input: 'visitsWorkflowDemo' }],
            resolve: [
                { action: 'displayMessage' },
                { action: 'removeMessage' },
                { action: 'preventDefault' },
                {
                    action: 'openQuestionnaireSitePad',
                    data: {
                        questionnaire_id: 'P_Meds_SitePad'
                    }
                }
            ]
        }
    ]
};
