export default {
    questionnaires: [{
        id: 'P_Screening_Toss',
        SU: 'ScrToss',
        displayName: 'DISPLAY_NAME',
        className: 'SCREENING_TOSS',
        affidavit: undefined,
        product: ['logpad'],
        screens: [
            'SCREENING_TOSS_S_1',
            'SCREENING_TOSS_S_2',
            'SCREENING_TOSS_S_3',
            'SCREENING_TOSS_S_4',
            'SCREENING_TOSS_S_5'
        ]
    }, {
        id: 'P_Screening_Toss_SitePad',
        SU: 'ScrToss',
        displayName: 'DISPLAY_NAME',
        className: 'SCREENING_TOSS',
        accessRoles: ['subject'],
        affidavit: undefined,
        product: ['sitepad'],
        screens: [
            'SCREENING_TOSS_S_1',
            'SCREENING_TOSS_S_2',
            'SCREENING_TOSS_S_3',
            'SCREENING_TOSS_S_4',
            'SCREENING_TOSS_S_5'
        ]
    }],

    screens: [{
        id: 'SCREENING_TOSS_S_1',
        className: 'SCREENING_TOSS_S_1',
        questions: [
            { id: 'SCREENING_TOSS_Q_1', mandatory: true }
        ]
    }, {
        id: 'SCREENING_TOSS_S_2',
        className: 'SCREENING_TOSS_S_2',
        accessRoles: ['admin', 'site'],
        questions: [
            { id: 'SCREENING_TOSS_Q_2', mandatory: true }
        ]
    }, {
        id: 'SCREENING_TOSS_S_3',
        className: 'SCREENING_TOSS_S_3',
        questions: [
            { id: 'SCREENING_TOSS_Q_3', mandatory: true }
        ]
    }, {
        id: 'SCREENING_TOSS_S_4',
        className: 'SCREENING_TOSS_S_4',
        questions: [
            { id: 'SCREENING_TOSS_Q_4', mandatory: true }
        ]
    }, {
        id: 'SCREENING_TOSS_S_5',
        className: 'SCREENING_TOSS_S_5',
        questions: [
            { id: 'SCREENING_TOSS_Q_5', mandatory: true }
        ]
    }],

    questions: [{
        id: 'SCREENING_TOSS_Q_1',
        IG: 'ScrToss',
        IT: 'SCRTOSS_Q1',
        text: 'QUESTION_1',
        className: 'RadioButton',
        widget: {
            id: 'SCREENING_TOSS_W_1',
            type: 'RadioButton',
            className: 'SCREENING_TOSS_W_1',
            template: {},
            answers: [
                { text: 'ABSENT', value: '0' },
                { text: 'MILD_SYMPTOMS', value: '1' },
                { text: 'MODERATE_SYMPTOMS', value: '2' },
                { text: 'SEVERE_SYMPTOMS', value: '3' }
            ]
        }
    }, {
        id: 'SCREENING_TOSS_Q_2',
        IG: 'ScrToss',
        IT: 'SCRTOSS_Q2',
        text: 'QUESTION_2',
        className: 'CustomRadioButton',
        widget: {
            id: 'SCREENING_TOSS_W_2',
            type: 'CustomRadioButton',
            template: {},
            answers: [
                { text: 'ABSENT', value: '0' },
                { text: 'MILD_SYMPTOMS', value: '1' },
                { text: 'MODERATE_SYMPTOMS', value: '2' },
                { text: 'SEVERE_SYMPTOMS', value: '3' }
            ]
        }
    }, {
        id: 'SCREENING_TOSS_Q_3',
        IG: 'ScrToss',
        IT: 'SCRTOSS_Q3',
        text: 'QUESTION_3',
        className: 'CustomRadioButton',
        widget: {
            id: 'SCREENING_TOSS_W_3',
            type: 'CustomRadioButton',
            template: {},
            answers: [
                { text: 'ABSENT', value: '0' },
                { text: 'MILD_SYMPTOMS', value: '1' },
                { text: 'MODERATE_SYMPTOMS', value: '2' },
                { text: 'SEVERE_SYMPTOMS', value: '3' }
            ]
        }
    }, {
        id: 'SCREENING_TOSS_Q_4',
        IG: 'ScrToss',
        IT: 'SCRTOSS_Q4',
        text: 'QUESTION_4',
        className: 'CustomRadioButton',
        widget: {
            id: 'SCREENING_TOSS_W_4',
            type: 'CustomRadioButton',
            template: {},
            answers: [
                { text: 'ABSENT', value: '0' },
                { text: 'MILD_SYMPTOMS', value: '1' },
                { text: 'MODERATE_SYMPTOMS', value: '2' },
                { text: 'SEVERE_SYMPTOMS', value: '3' }
            ]
        }
    }, {
        id: 'SCREENING_TOSS_Q_5',
        IG: 'ScrToss',
        IT: 'SCRTOSS_Q5',
        text: 'QUESTION_5',
        className: 'RadioButton',
        widget: {
            id: 'SCREENING_TOSS_W_5',
            type: 'RadioButton',
            className: 'SCREENING_TOSS_W_5',
            template: {},
            answers: [
                { text: 'ABSENT', value: '0' },
                { text: 'MILD_SYMPTOMS', value: '1' },
                { text: 'MODERATE_SYMPTOMS', value: '2' },
                { text: 'SEVERE_SYMPTOMS', value: '3' }
            ]
        }
    }]
};
