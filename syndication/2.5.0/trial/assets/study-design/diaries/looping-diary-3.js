export default {
    questionnaires: [
        {
            id: 'P_Loop3',
            SU: 'Loop3',
            displayName: 'DISPLAY_NAME',
            previousScreen: true,
            affidavit: 'DEFAULT',
            screens: [
                'LOOP3_S_1',
                'LOOP3_S_2',
                'LOOP3_S_3',
                'LOOP3_S_4',
                'LOOP3_S_5',
                'LOOP3_S_6',
                'LOOP3_S_7'
            ],
            branches: [
                {
                    branchFrom: 'LOOP3_S_3',
                    branchTo: 'LOOP3_S_7',
                    clearBranchedResponses: false,
                    branchFunction: 'toSympReviewScreenLoop3'
                }, {
                    branchFrom: 'LOOP3_S_6',
                    branchTo: 'LOOP3_S_4',
                    clearBranchedResponses: false,
                    branchFunction: 'toMedsReviewScreenLoop3'
                }, {
                    branchFrom: 'LOOP3_S_4',
                    branchTo: 'LOOP3_S_7',
                    clearBranchedResponses: false,
                    branchFunction: 'toSympReviewScreenLoop3'
                }
            ]
        }
    ],
    screens: [
        {
            id: 'LOOP3_S_1',
            className: 'LOOP3_S_1',
            questions: [
                {
                    id: 'LOOP3_Q1',
                    mandatory: true
                }]
        }, {
            id: 'LOOP3_S_2',
            className: 'LOOP3_S_2',
            questions: [
                {
                    id: 'LOOP3_Q2',
                    mandatory: true
                }]
        }, {
            id: 'LOOP3_S_3',
            className: 'LOOP3_S_3',
            questions: [
                {
                    id: 'LOOP3_Q3',
                    mandatory: true
                }]
        }, {
            id: 'LOOP3_S_4',
            className: 'LOOP3_S_4',
            questions: [
                {
                    id: 'LOOP3_Q4',
                    mandatory: false
                }]
        }, {
            id: 'LOOP3_S_5',
            className: 'LOOP3_S_5',
            questions: [
                {
                    id: 'LOOP3_Q5',
                    mandatory: true
                }]
        }, {
            id: 'LOOP3_S_6',
            className: 'LOOP3_S_6',
            questions: [
                {
                    id: 'LOOP3_Q6',
                    mandatory: true
                }]
        }, {
            id: 'LOOP3_S_7',
            className: 'LOOP3_S_7',
            questions: [
                {
                    id: 'LOOP3_Q7',
                    mandatory: true
                }]
        }
    ],
    questions: [
        {
            id: 'LOOP3_Q1',
            IG: 'Loop3A',
            IT: 'SelectSymp',
            text: 'QUESTION_1',
            widget: {
                id: 'LOOP3_W1',
                type: 'RadioButton',
                template: { },
                answers: [
                    {
                        text: 'COUGH',
                        value: '0'
                    }, {
                        text: 'HEADACHE',
                        value: '1'
                    }, {
                        text: 'ITCHY_EYES',
                        value: '2'
                    }, {
                        text: 'RUNNY_NOSE',
                        value: '3'
                    }, {
                        text: 'SINUS_PRESSURE',
                        value: '4'
                    }, {
                        text: 'SORE_THROAT',
                        value: '5'
                    }, {
                        text: 'STUFFY_NOSE',
                        value: '6'
                    }, {
                        text: 'WATERY_EYES',
                        value: '7'
                    }
                ]
            }
        }, {
            id: 'LOOP3_Q2',
            IG: 'Loop3A',
            IT: 'SympSev',
            text: 'QUESTION_2',
            widget: {
                id: 'LOOP3_W2',
                type: 'RadioButton',
                template: { },
                answers: [
                    {
                        text: 'ABSENT',
                        value: '0'
                    }, {
                        text: 'MILD_SYMPTOMS',
                        value: '1'
                    }, {
                        text: 'MOD_SYMPTOMS',
                        value: '2'
                    }, {
                        text: 'SEV_SYMPTOMS',
                        value: '3'
                    }
                ]
            }
        }, {
            id: 'LOOP3_Q3',
            IG: 'Loop3A',
            IT: 'SympStill',
            text: 'QUESTION_3',
            widget: {
                id: 'LOOP3_W3',
                type: 'RadioButton',
                templates: {},
                answers: [
                    {
                        text: 'YES',
                        value: '0'
                    }, {
                        text: 'NO',
                        value: '1'
                    }
                ]
            }
        }, {
            id: 'LOOP3_Q4',
            IG: 'Loop3B',
            repeating: true,
            text: 'QUESTION_4',
            widget: {
                id: 'LOOP3_W4',
                type: 'ReviewScreen',
                screenFunction: 'medsLoop3',
                templates: {
                    itemsTemplate: 'LOOP1:ScreenItems'
                },
                buttons: [
                    {
                        id: 1,
                        availability: 'always',
                        text: 'ADD_REM',
                        actionFunction: 'addNewRemLoop3'
                    }, {
                        id: 2,
                        text: 'EDIT_REM',
                        actionFunction: 'editRemLoop3'
                    }, {
                        id: 3,
                        text: 'DELETE_REM',
                        actionFunction: 'deleteRemLoop3'
                    }
                ]
            }
        }, {
            id: 'LOOP3_Q5',
            IG: 'Loop3B',
            IT: 'ColdMed',
            text: 'QUESTION_5',
            widget: {
                id: 'LOOP3_W5',
                type: 'CustomRadioButton',
                template: { },
                answers: [
                    {
                        text: 'COUGH_DROPS',
                        value: '0'
                    }, {
                        text: 'DECONGESTANT',
                        value: '1'
                    }, {
                        text: 'EXPECTORANT',
                        value: '2'
                    }, {
                        text: 'EYE_DROPS',
                        value: '3'
                    }, {
                        text: 'NOSE_SPRAY',
                        value: '4'
                    }, {
                        text: 'THROAT_SPRAY',
                        value: '5'
                    }
                ]
            }
        }, {
            id: 'LOOP3_Q6',
            IG: 'Loop3B',
            IT: 'HowMuch',
            text: 'QUESTION_6',
            widget: {
                id: 'LOOP3_W6',
                type: 'NumberSpinner',
                config: {
                    min: 1,
                    max: 999,
                    step: 1
                }
            }
        }, {
            id: 'LOOP3_Q7',
            IG: 'Loop3A',
            repeating: true,
            text: 'QUESTION_7',
            widget: {
                id: 'LOOP3_W7',
                type: 'ReviewScreen',
                screenFunction: 'sympLoop3',
                templates: {
                    itemsTemplate: 'LOOP1:ScreenItems'
                },
                validation: {
                    validationFunc: 'checkForSymptom'
                },
                buttons: [
                    {
                        id: 1,
                        availability: 'always',
                        text: 'ADD_SYMP',
                        actionFunction: 'addNewSympLoop3'
                    }, {
                        id: 2,
                        text: 'EDIT_SYMP',
                        actionFunction: 'editSympLoop3'
                    }, {
                        id: 3,
                        text: 'DELETE_SYMP',
                        actionFunction: 'deleteSympLoop3'
                    }
                ]
            }
        }
    ]
};
