export default {
    questionnaires: [
        {
            id: 'P_Site_Diary',
            SU: 'SITE',
            displayName: 'DISPLAY_NAME',
            className: 'SITE_DIARY',
            previousScreen: true,
            scheduleRoles: ['admin', 'site'],
            affidavit: undefined,
            screens: [
                'SITE_DIARY_S_1'
            ]
        }
    ],

    screens: [
        {
            id: 'SITE_DIARY_S_1',
            className: 'SITE_DIARY_S_1',
            questions: [
                {
                    id: 'SITE_DIARY_Q_1',
                    mandatory: false
                }
            ]
        }
    ],

    questions: [
        {
            id: 'SITE_DIARY_Q_1',
            IG: 'Site',
            text: [
                'MESSAGE'
            ],
            className: 'SITE_DIARY_Q_1'
        }
    ]
};
