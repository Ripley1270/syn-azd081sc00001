export default {
    schedules: [
        {
            id: 'Patient_IMAGE_Test_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_Image_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ],
    questionnaires: [
        {
            id: 'P_Image_Diary',
            SU: 'IMAGE',
            displayName: 'DISPLAY_NAME',
            className: 'ImageTestDiary',
            affidavit: undefined,
            previousScreen: true,
            screens: [
                'IMAGE_TEST_S1',
                'IMAGE_TEST_S2',
                'IMAGE_TEST_S3',
                'IMAGE_TEST_S4',
                'IMAGE_TEST_S5',
                'IMAGE_TEST_S6',
                'IMAGE_TEST_S7',
                'IMAGE_TEST_S8',
                'IMAGE_TEST_S9',
                'IMAGE_TEST_REVIEW'
            ]
        }
    ],
    screens: [
        {
            id: 'IMAGE_TEST_S1',
            className: 'IMAGE_TEST_S1',
            questions: [
                {
                    id: 'IMAGE_TEST_S1_Q1',
                    mandatory: false
                }
            ]
        },
        {
            id: 'IMAGE_TEST_S2',
            className: 'IMAGE_TEST_S2',
            questions: [
                {
                    id: 'IMAGE_TEST_S2_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'IMAGE_TEST_S3',
            className: 'IMAGE_TEST_S3',
            questions: [
                {
                    id: 'IMAGE_TEST_S3_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'IMAGE_TEST_S4',
            className: 'IMAGE_TEST_S4',
            questions: [
                {
                    id: 'IMAGE_TEST_S4_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'IMAGE_TEST_S5',
            className: 'IMAGE_TEST_S5',
            questions: [
                {
                    id: 'IMAGE_TEST_S5_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'IMAGE_TEST_S6',
            className: 'IMAGE_TEST_S6',
            questions: [
                {
                    id: 'IMAGE_TEST_S6_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'IMAGE_TEST_S7',
            className: 'IMAGE_TEST_S7',
            questions: [
                {
                    id: 'IMAGE_TEST_S7_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'IMAGE_TEST_S8',
            className: 'IMAGE_TEST_S8',
            questions: [
                {
                    id: 'IMAGE_TEST_S8_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'IMAGE_TEST_S9',
            className: 'IMAGE_TEST_S9',
            questions: [
                {
                    id: 'IMAGE_TEST_S9_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'IMAGE_TEST_REVIEW',
            className: 'IMAGE_TEST_REVIEW',
            questions: [
                {
                    id: 'IMAGE_TEST_REVIEW_Q',
                    mandatory: false
                }
            ]
        }
    ],
    questions: [
        {
            id: 'IMAGE_TEST_S1_Q1',
            IG: 'IMAGE',
            IT: 'IMAGE_TEST_S1_Q1',
            skipIT: 'IMAGE_1_SKP',
            title: 'IMAGE_Q1_TITLE',
            text: ['IMAGE_Q1_TEXT'],
            className: 'IMAGE_TEST_S1_Q1',
            widget: {
                id: 'IMAGE_TEST_W1',
                type: 'ImageWidget',
                imageName: 'skeleton.svg',
                singleSelect: true,
                imageText: {
                    legend0: 'Q1_LEGEND_0',
                    legend1: 'Q1_LEGEND_1'
                },
                nodeIDRegex: /[rl]\d+/
            }
        },
        {
            id: 'IMAGE_TEST_S2_Q1',
            IG: 'IMAGE',
            skipIT: 'IMAGE_S2_SKP',
            title: 'IMAGE_Q2_TITLE',
            text: ['IMAGE_Q2_TEXT'],
            className: 'IMAGE_TEST_S2_Q1',
            widget: {
                id: 'IMAGE_TEST_W2',
                type: 'ImageWidget',
                imageName: 'skeleton.svg',
                imageText: {
                    legend0: 'Q2_LEGEND_0',
                    legend1: 'Q2_LEGEND_1'
                },
                nodeIDRegex: /[rl]\d+/
            }
        },
        {
            id: 'IMAGE_TEST_S3_Q1',
            IG: 'IMAGE',
            skipIT: 'IMAGE_S3_SKP',
            title: 'IMAGE_Q3_TITLE',
            text: ['IMAGE_Q3_TEXT'],
            className: 'IMAGE_TEST_S3_Q1',
            widget: {
                id: 'IMAGE_TEST_W3',
                type: 'ImageWidget',
                imageName: 'skeleton.svg',
                options: [
                    {
                        value: '0',
                        css: {
                            fill: '#FFFFFF'
                        }
                    }, {
                        value: '1',
                        css: {
                            fill: '#0000FF'
                        }
                    }, {
                        value: '2',
                        css: {
                            fill: '#FF0000'
                        }
                    }, {
                        value: '3',
                        css: {
                            fill: '#00FF00'
                        }
                    }
                ],
                imageText: {
                    legend0: 'Q3_LEGEND_0',
                    legend1: 'Q3_LEGEND_1'
                },
                nodeIDRegex: /[rl]\d+/
            }
        },
        {
            id: 'IMAGE_TEST_S4_Q1',
            IG: 'IMAGE',
            skipIT: 'IMAGE_S4_SKP',
            title: 'IMAGE_Q4_TITLE',
            text: ['IMAGE_Q4_TEXT'],
            className: 'IMAGE_TEST_S4_Q1',
            widget: {
                id: 'IMAGE_TEST_W4',
                type: 'ImageWidgetModal',
                imageName: 'skeleton.svg',
                modalTitle: 'SELECT_PAIN',
                options: [
                    {
                        value: '0',
                        css: {
                            fill: '#FFFFFF'
                        }
                    }, {
                        value: '1',
                        css: {
                            fill: '#0000FF'
                        }
                    }, {
                        value: '2',
                        css: {
                            fill: '#FF0000'
                        }
                    }, {
                        value: '3',
                        css: {
                            fill: '#00FF00'
                        }
                    }
                ],
                inputOptions: {
                    itemTemplate: 'DEFAULT:ModalMultipleChoiceItem',
                    type: 'MultipleChoiceInput',
                    items: [{
                        text: 'IMAGE_OPTION_0',
                        value: '0'
                    }, {
                        text: 'IMAGE_OPTION_1',
                        value: '1'
                    }, {
                        text: 'IMAGE_OPTION_2',
                        value: '2'
                    }, {
                        text: 'IMAGE_OPTION_3',
                        value: '3'
                    }
                    ]
                },

                imageText: {
                    legend0: 'Q4_LEGEND_0',
                    legend1: 'Q4_LEGEND_1'
                },
                nodeIDRegex: /[rl]\d+/
            }
        },
        {
            id: 'IMAGE_TEST_S5_Q1',
            IG: 'IMAGE',
            skipIT: 'IMAGE_S5_SKP',
            title: 'IMAGE_Q5_TITLE',
            text: ['IMAGE_Q5_TEXT'],
            className: 'IMAGE_TEST_S5_Q1',
            widget: {
                id: 'IMAGE_TEST_W5',
                type: 'ImageWidgetModal',
                imageName: 'skeleton.svg',
                templates: {
                    modal: 'DEFAULT:NumberSpinnerModal'
                },
                modalTitle: 'SELECT_PAIN',
                labels: {
                    labelOne: 'PAIN_INTENSITY'
                },
                options: [
                    {
                        value: '0',
                        css: {
                            fill: '#FFFFFF'
                        }
                    }, {
                        value: '1',
                        css: {
                            fill: '#0000FF'
                        }
                    }, {
                        value: '2',
                        css: {
                            fill: '#FF0000'
                        }
                    }, {
                        value: '3',
                        css: {
                            fill: '#00FF00'
                        }
                    }
                ],
                inputOptions: {
                    type: 'NumberSpinnerInput',
                    itemTemplate: 'DEFAULT:NumberItemTemplate',
                    template: 'DEFAULT:NumberSpinnerControl',
                    min: 0,
                    max: 3
                },

                imageText: {
                    legend0: 'Q4_LEGEND_0',
                    legend1: 'Q4_LEGEND_1'
                },
                nodeIDRegex: /[rl]\d+/
            }
        },
        {
            id: 'IMAGE_TEST_S6_Q1',
            IG: 'IMAGE',
            skipIT: 'IMAGE_S6_SKP',
            title: 'IMAGE_Q6_TITLE',
            text: ['IMAGE_Q6_TEXT'],
            className: 'IMAGE_TEST_S6_Q1',
            widget: {
                id: 'IMAGE_TEST_W6',
                type: 'ImageWidgetModal',
                imageName: 'skeleton.svg',
                templates: {
                    modal: 'DEFAULT:NumberSpinnerModal'
                },
                modalTitle: 'SELECT_PAIN',
                labels: {
                    labelOne: 'PAIN_INTENSITY'
                },
                options: [
                    {
                        value: '0',
                        css: {
                            fill: '#FFFFFF'
                        }
                    }, {
                        value: '1',
                        css: {
                            fill: '#0000FF'
                        }
                    }, {
                        value: '2',
                        css: {
                            fill: '#FF0000'
                        }
                    }, {
                        value: '3',
                        css: {
                            fill: '#00FF00'
                        }
                    }
                ],
                inputOptions: {
                    type: 'NumberSpinnerInput',
                    itemTemplate: 'DEFAULT:NumberItemTemplate',
                    template: 'DEFAULT:NumberSpinnerControl',
                    showButtons: true,
                    buttonLayout: 'horizontal',
                    min: 0,
                    max: 3
                },

                imageText: {
                    legend0: 'Q4_LEGEND_0',
                    legend1: 'Q4_LEGEND_1'
                },
                nodeIDRegex: /[rl]\d+/
            }
        },
        {
            id: 'IMAGE_TEST_S7_Q1',
            IG: 'IMAGE',
            skipIT: 'IMAGE_S7_SKP',
            title: 'IMAGE_Q7_TITLE',
            text: ['IMAGE_Q7_TEXT'],
            className: 'IMAGE_TEST_S7_Q1',
            widget: {
                id: 'IMAGE_TEST_W7',
                type: 'ImageWidgetModal',
                imageName: 'skeleton.svg',
                templates: {
                    modal: 'DEFAULT:NumberSpinnerModal'
                },
                modalTitle: 'SELECT_PAIN',
                labels: {
                    labelOne: 'PAIN_INTENSITY'
                },
                options: [
                    {
                        value: '0',
                        css: {
                            fill: '#FFFFFF'
                        }
                    }, {
                        value: '1',
                        css: {
                            fill: '#0000FF'
                        }
                    }, {
                        value: '2',
                        css: {
                            fill: '#FF0000'
                        }
                    }, {
                        value: '3',
                        css: {
                            fill: '#00FF00'
                        }
                    }
                ],
                inputOptions: {
                    type: 'NumberSpinnerInput',
                    itemTemplate: 'DEFAULT:NumberItemTemplate',
                    template: 'DEFAULT:NumberSpinnerControl',
                    showButtons: false,
                    buttonLayout: 'horizontal',
                    min: 0,
                    max: 3
                },

                imageText: {
                    legend0: 'Q4_LEGEND_0',
                    legend1: 'Q4_LEGEND_1'
                },
                nodeIDRegex: /[rl]\d+/
            }
        },
        {
            id: 'IMAGE_TEST_S8_Q1',
            IG: 'IMAGE',
            skipIT: 'IMAGE_S8_SKP',
            title: 'IMAGE_Q8_TITLE',
            text: ['IMAGE_Q8_TEXT'],
            className: 'IMAGE_TEST_S8_Q1',
            widget: {
                id: 'IMAGE_TEST_W8',
                type: 'ImageWidgetModal',
                imageName: 'skeleton.svg',
                templates: {
                    modal: 'DEFAULT:NumberSpinnerModal'
                },
                modalTitle: 'SELECT_PAIN',
                labels: {
                    labelOne: 'PAIN_INTENSITY'
                },
                options: [
                    {
                        value: '0',
                        css: {
                            fill: '#FFFFFF'
                        }
                    }, {
                        value: '1',
                        css: {
                            fill: '#0000FF'
                        }
                    }, {
                        value: '2',
                        css: {
                            fill: '#FF0000'
                        }
                    }, {
                        value: '3',
                        css: {
                            fill: '#00FF00'
                        }
                    }
                ],
                inputOptions: {
                    type: 'NumberSpinnerInput',
                    itemTemplate: 'DEFAULT:NumberItemTemplate',
                    template: 'DEFAULT:NumberSpinnerControl',
                    showButtons: true,
                    buttonLayout: 'vertical',
                    min: 0,
                    max: 3
                },

                imageText: {
                    legend0: 'Q4_LEGEND_0',
                    legend1: 'Q4_LEGEND_1'
                },
                nodeIDRegex: /[rl]\d+/
            }
        },
        {
            id: 'IMAGE_TEST_S9_Q1',
            IG: 'IMAGE',
            skipIT: 'IMAGE_S9_SKP',
            title: 'IMAGE_Q9_TITLE',
            text: ['IMAGE_Q9_TEXT'],
            className: 'IMAGE_TEST_S9_Q1',
            widget: {
                id: 'IMAGE_TEST_W9',
                type: 'ImageWidgetModal',
                imageName: 'skeleton.svg',
                templates: {
                    modal: 'DEFAULT:NumberSpinnerModal'
                },
                modalTitle: 'SELECT_PAIN',
                labels: {
                    labelOne: 'PAIN_INTENSITY'
                },
                options: [
                    {
                        value: '0',
                        css: {
                            fill: '#FFFFFF'
                        }
                    }, {
                        value: '1',
                        css: {
                            fill: '#0000FF'
                        }
                    }, {
                        value: '2',
                        css: {
                            fill: '#FF0000'
                        }
                    }, {
                        value: '3',
                        css: {
                            fill: '#00FF00'
                        }
                    }
                ],
                inputOptions: {
                    type: 'NumberSpinnerInput',
                    itemTemplate: 'DEFAULT:NumberItemTemplate',
                    template: 'DEFAULT:NumberSpinnerControl',
                    showButtons: true,
                    min: 0,
                    max: 3
                },

                imageText: {
                    legend0: 'Q4_LEGEND_0',
                    legend1: 'Q4_LEGEND_1'
                },
                nodeIDRegex: /[rl]\d+/
            }
        },
        {
            id: 'IMAGE_TEST_REVIEW_Q',
            IG: 'IMAGE',
            IT: 'IMAGE_REVIEW',
            title: 'SUMMARY_TITLE',
            text: ['SUMMARY_QUESTION'],
            className: 'IMAGE_TEST_Review_Q'
        }

    ]
};
