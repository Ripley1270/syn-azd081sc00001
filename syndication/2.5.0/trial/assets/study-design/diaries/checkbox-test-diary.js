export default {
    questionnaires: [
        {
            id: 'P_Checkbox_Diary',
            SU: 'Checkbox_Diary',
            displayName: 'DISPLAY_NAME',
            className: 'CHECBOX_TEST',

            // Choose default affidavit per user role
            affidavit: undefined,
            previousScreen: true,
            screens: [
                'CHECKBOX_TEST_S1',
                'CHECKBOX_TEST_S2',
                'CHECKBOX_TEST_S3',
                'CHECKBOX_TEST_S4',
                'CHECKBOX_TEST_S5',
                'CHECKBOX_TEST_S6',
                'CHECKBOX_TEST_S7',
                'CHECKBOX_TEST_S8',
                'CHECKBOX_TEST_S9'
            ],
            branches: []
        }
    ],

    screens: [
        {
            id: 'CHECKBOX_TEST_S1',
            className: 'CHECKBOX_TEST_S1',
            questions: [
                {
                    id: 'CHECKBOX_TEST_S1_Q1',
                    mandatory: true
                }
            ]
        },

        // Non-sequential values
        {
            id: 'CHECKBOX_TEST_S2',
            className: 'CHECKBOX_TEST_S2',
            questions: [
                {
                    id: 'CHECKBOX_TEST_S2_Q1',
                    mandatory: true
                }
            ]
        },

        // Custom CSS
        {
            id: 'CHECKBOX_TEST_S3',
            className: 'CHECKBOX_TEST_S3',
            questions: [
                {
                    id: 'CHECKBOX_TEST_S3_Q1',
                    mandatory: true
                }
            ]
        },

        // Custom answer CSS
        {
            id: 'CHECKBOX_TEST_S4',
            className: 'CHECKBOX_TEST_S4',
            questions: [
                {
                    id: 'CHECKBOX_TEST_S4_Q1',
                    mandatory: true
                }
            ]
        },

        // Multiple heights
        {
            id: 'CHECKBOX_TEST_S5',
            className: 'CHECKBOX_TEST_S5',
            questions: [
                {
                    id: 'CHECKBOX_TEST_S5_Q1',
                    mandatory: true
                }
            ]
        },

        // Centered text
        {
            id: 'CHECKBOX_TEST_S6',
            className: 'CHECKBOX_TEST_S6',
            questions: [
                {
                    id: 'CHECKBOX_TEST_S6_Q1',
                    mandatory: true
                }
            ]
        },

        // Custom values (from answerFunc)
        {
            id: 'CHECKBOX_TEST_S7',
            className: 'CHECKBOX_TEST_S7',
            questions: [
                {
                    id: 'CHECKBOX_TEST_S7_Q1',
                    mandatory: false
                }
            ]
        },

        // Display answers
        {
            id: 'CHECKBOX_TEST_S8',
            className: 'CHECKBOX_TEST_S8',
            questions: [
                {
                    id: 'CHECKBOX_TEST_S8_Q1',
                    mandatory: false
                }
            ]
        },

        // Display answers
        {
            id: 'CHECKBOX_TEST_S9',
            className: 'CHECKBOX_TEST_S9',
            questions: [
                {
                    id: 'CHECKBOX_TEST_S9_Q1',
                    mandatory: false
                }
            ]
        }
    ],

    questions: [
        // Checkbox
        // Radio button test Screen 1
        {
            id: 'CHECKBOX_TEST_S1_Q1',
            IG: 'CheckBox_Test',
            skipIT: 'CB_TEST_S1_Q2_2_SKP',
            title: 'QUESTION_1_TITLE',
            text: [
                'QUESTION_1'
            ],
            className: 'CHECKBOX_TEST_S1_Q1',
            widget: {
                id: 'CHECKBOX_TEST_S1_Q1',
                type: 'CheckBox',
                label: 'Pick a number',
                templates: { },
                answers: [
                    {
                        text: 'ONE_TWO_LINES',
                        value: '1',
                        IT: 'CB_TEST_S1_Q1_0'
                    },
                    {
                        text: 'TWO_LONG',
                        value: '2',
                        IT: 'CB_TEST_S1_Q1_1'
                    },
                    {
                        text: 'THREE',
                        value: '3',
                        response: '1',
                        IT: 'CB_TEST_S1_Q1_2'
                    },
                    {
                        text: 'FOUR',
                        value: '4',
                        IT: 'CB_TEST_S1_Q1_3'
                    }
                ]
            }
        },

        // Radio button test Screen 2
        {
            id: 'CHECKBOX_TEST_S2_Q1',
            IG: 'CheckBox_Test',
            skipIT: 'CB_TEST_S1_Q2_2_SKP',
            title: 'QUESTION_2_TITLE',
            text: [
                'QUESTION_2'
            ],
            className: '',
            widget: {
                id: 'CHECKBOX_TEST_S2_Q1',
                type: 'CheckBox',
                label: 'Pick a number',
                templates: {},
                answers: [
                    {
                        text: 'ONE',
                        value: '2',
                        IT: 'CB_TEST_S1_Q2_0'
                    },
                    {
                        text: 'FIVE',
                        value: '5',
                        IT: 'CB_TEST_S1_Q2_1'
                    },
                    {
                        text: 'SEVEN',
                        value: '7',
                        IT: 'CB_TEST_S1_Q2_2'
                    },
                    {
                        text: 'EIGHT',
                        value: '8',
                        IT: 'CB_TEST_S1_Q2_3'
                    }
                ]
            }
        },

        // Radio button test Screen 3
        {
            id: 'CHECKBOX_TEST_S3_Q1',
            IG: 'CheckBox_Test',
            skipIT: 'CB_TEST_S1_Q3_2_SKP',
            title: 'QUESTION_3_TITLE',
            text: [
                'QUESTION_3'
            ],
            className: 'CheckBox',
            widget: {
                id: 'CHECKBOX_TEST_S3_Q1',
                type: 'CheckBox',
                label: 'Pick a number',
                templates: {},
                answers: [
                    {
                        text: 'ONE',
                        value: '1',
                        IT: 'CB_TEST_S1_Q3_0'
                    },
                    {
                        text: 'TWO',
                        value: '2',
                        IT: 'CB_TEST_S1_Q3_1'
                    },
                    {
                        text: 'THREE',
                        value: '3',
                        IT: 'CB_TEST_S1_Q3_2'
                    },
                    {
                        text: 'FOUR',
                        value: '4',
                        IT: 'CB_TEST_S1_Q3_3'
                    }
                ]
            }
        },
        {
            id: 'CHECKBOX_TEST_S4_Q1',
            IG: 'CheckBox_Test',
            skipIT: 'CB_TEST_S1_Q4_2_SKP',
            title: 'QUESTION_4_TITLE',
            text: [
                'QUESTION_4'
            ],
            className: 'CHECKBOX_TEST_S4_Q1',
            widget: {
                id: 'CHECKBOX_TEST_S4_Q1',
                type: 'CheckBox',
                label: 'Pick a number',
                templates: { },
                answers: [
                    {
                        text: 'ONE',
                        value: '1',
                        IT: 'CB_TEST_S1_Q4_0'
                    },
                    {
                        text: 'TWO',
                        value: '2',
                        IT: 'CB_TEST_S1_Q4_1'
                    },
                    {
                        text: 'THREE',
                        value: '3',
                        IT: 'CB_TEST_S1_Q4_2'
                    },
                    {
                        text: 'FOUR',
                        value: '4',
                        IT: 'CB_TEST_S1_Q4_3'
                    }
                ]
            }
        }, {
            id: 'CHECKBOX_TEST_S5_Q1',
            IG: 'CheckBox_Test',
            skipIT: 'CB_TEST_S1_Q5_2_SKP',
            title: 'QUESTION_5_TITLE',
            text: [
                'QUESTION_5'
            ],
            className: 'CHECKBOX_TEST_S5_Q1',
            widget: {
                id: 'CHECKBOX_TEST_S5_Q1',
                type: 'CheckBox',
                label: 'Pick a number',
                templates: { },
                answers: [
                    {
                        text: 'ONE',
                        value: '1',
                        IT: 'CB_TEST_S1_Q5_0'
                    },
                    {
                        text: 'TWO',
                        value: '2',
                        IT: 'CB_TEST_S1_Q5_1'
                    },
                    {
                        text: 'THREE',
                        value: '3',
                        IT: 'CB_TEST_S1_Q5_2'
                    },
                    {
                        text: 'FOUR_EXCLUDE',
                        value: '4',
                        IT: 'CB_TEST_S1_Q5_3',
                        exclude: ['All']
                    }
                ]
            }
        }, {
            id: 'CHECKBOX_TEST_S6_Q1',
            IG: 'CheckBox_Test',
            skipIT: 'CB_TEST_S1_Q6_2_SKP',
            title: 'QUESTION_6_TITLE',
            text: [
                'QUESTION_6'
            ],
            className: 'CHECKBOX_TEST_S6_Q1',
            widget: {
                id: 'CHECKBOX_TEST_S6_Q1',
                type: 'CheckBox',
                label: 'Pick a number',
                templates: { },
                answers: [
                    {
                        text: 'ONE',
                        value: '1',
                        IT: 'CB_TEST_S1_Q6_0'
                    },
                    {
                        text: 'TWO',
                        value: '2',
                        IT: 'CB_TEST_S1_Q6_1'
                    },
                    {
                        text: 'THREE',
                        value: '3',
                        IT: 'CB_TEST_S1_Q6_2'
                    },
                    {
                        text: 'FOUR',
                        value: '4',
                        IT: 'CB_TEST_S1_Q6_3'
                    }
                ]
            }
        }, {
            id: 'CHECKBOX_TEST_S7_Q1',
            IG: 'CheckBox_Test',
            skipIT: 'CB_TEST_S1_Q7_2_SKP',
            title: 'QUESTION_7_TITLE',
            text: [
                'QUESTION_7'
            ],
            className: 'CHECKBOX_TEST_S7_Q1',
            widget: {
                id: 'CHECKBOX_TEST_S7_Q1',
                type: 'CheckBox',
                label: 'Pick a number',
                templates: {},
                paramFunctions: [{
                    id: 'answerFunc',
                    evaluate: 'checkboxAnswersTest',
                    screenshots: {
                        values: [
                            [{
                                text: 'ONE',
                                value: '0',
                                IT: 'CB_TEST_S1_Q7_0'
                            }, {
                                text: 'TWO',
                                value: '1',
                                IT: 'CB_TEST_S1_Q7_1'
                            }, {
                                text: 'THREE',
                                value: '2',
                                IT: 'CB_TEST_S1_Q7_2'
                            }]
                        ]
                    }
                }]
            }
        }, {
            id: 'CHECKBOX_TEST_S8_Q1',
            IG: 'CheckBox_Test',
            skipIT: 'CB_TEST_S1_Q8_2_SKP',
            title: 'QUESTION_8_TITLE',
            text: [
                'QUESTION_8'
            ],
            className: 'CHECKBOX_TEST_S8_Q1',
            widget: {
                id: 'CHECKBOX_TEST_S8_Q1',
                type: 'CheckBox',
                label: 'Pick a number',
                templates: {},
                paramFunctions: [{
                    id: 'answerFunc',
                    evaluate: 'checkboxClearAnswers',
                    screenshots: {
                        values: [
                            [{
                                text: 'ONE',
                                value: '1',
                                IT: 'CB_TEST_S1_S8_0'
                            }, {
                                text: 'TWO',
                                value: '2',
                                IT: 'CB_TEST_S1_S8_1'
                            }]
                        ]
                    }
                }]
            }
        }, {
            id: 'CHECKBOX_TEST_S9_Q1',
            IG: 'CheckBox_Test',
            skipIT: '',
            title: 'QUESTION_9_TITLE',
            text: [
                'QUESTION_9'
            ],
            className: 'CHECKBOX_TEST_S9_Q1'
        }
    ]
};
