export default {
    questionnaires: [
        {
            id: 'P_DropDownList_Diary',
            SU: 'DDL',
            displayName: 'DISPLAY_NAME',
            className: 'DDL',
            affidavit: undefined,
            previousScreen: true,
            triggerPhase: 'TREATMENT',
            screens: [
                'DropDownList_TEST_S1',
                'DropDownList_TEST_S2',
                'DropDownList_TEST_S3',
                'DropDownList_TEST_S4',
                'DropDownList_TEST_S5',
                'DropDownList_TEST_Review'
            ]
        }
    ],
    screens: [
        {
            id: 'DropDownList_TEST_S1',
            className: 'DropDownList_TEST_S1',
            questions: [
                {
                    id: 'DropDownList_TEST_S1_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'DropDownList_TEST_S2',
            className: 'DropDownList_TEST_S2',
            questions: [
                {
                    id: 'DropDownList_TEST_S2_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'DropDownList_TEST_S3',
            className: 'DropDownList_TEST_S3',
            questions: [
                {
                    id: 'DropDownList_TEST_S3_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'DropDownList_TEST_S4',
            className: 'DropDownList_TEST_S4',
            questions: [
                {
                    id: 'DropDownList_TEST_S4_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'DropDownList_TEST_S5',
            className: 'DropDownList_TEST_S5',
            questions: [
                {
                    id: 'DropDownList_TEST_S5_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'DropDownList_TEST_Review',
            className: 'DropDownList_TEST_Review',
            questions: [
                {
                    id: 'DropDownList_TEST_Review_Q',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'DropDownList_TEST_S1_Q1',
            IG: 'DDL',
            IT: 'DDL1',
            title: 'DropDownList_Q1_TITLE',
            text: ['DropDownList_Q1_TEXT'],
            className: 'DropDownList_TEST_S1_Q1',
            widget: {
                id: 'DropDownList_TEST_W1',
                type: 'DropDownList',

                // TC17382 - list has less than max displayed rows (no scrolling)
                label: 'CLICK_TO_MAKE_SELECTION',
                items: [
                    {
                        value: 1,
                        text: 'Value_1'
                    },
                    {
                        value: 2,
                        text: 'Value_2'
                    },
                    {
                        value: 3,
                        text: 'Value_3'
                    }
                ],
                visibleRows: 4
            }
        },
        {
            id: 'DropDownList_TEST_S2_Q1',
            IG: 'DDL',
            IT: 'DDL2',
            title: 'DropDownList_Q2_TITLE',
            text: ['DropDownList_Q2_TEXT'],
            className: 'DropDownList_TEST_S2_Q1',
            widget: {
                id: 'DropDownList_TEST_W2',
                type: 'DropDownList',
                items: 'dropDownListItems'
            }
        },
        {
            id: 'DropDownList_TEST_S3_Q1',
            IG: 'DDL',
            IT: 'DDL3',
            title: 'DropDownList_Q3_TITLE',
            text: ['DropDownList_Q3_TEXT'],
            className: 'DropDownList_TEST_S3_Q1',
            widget: {
                id: 'DropDownList_TEST_W3',
                type: 'DropDownList',

                // TC14718 - list has no max rows defined, more options than default (should scroll)
                items: [
                    {
                        value: '1',
                        text: 'Item_1'
                    },
                    {
                        value: '2',
                        text: 'Item_2'
                    },
                    {
                        value: '3',
                        text: 'Item_3'
                    },
                    {
                        value: '4',
                        text: 'Item_4'
                    },
                    {
                        value: '5',
                        text: 'Item_5'
                    },
                    {
                        value: '6',
                        text: 'Item_6'
                    },
                    {
                        value: '7',
                        text: 'Item_7'
                    },
                    {
                        value: '8',
                        text: 'Item_8'
                    },
                    {
                        value: '9',
                        text: 'Item_9'
                    },
                    {
                        value: '10',
                        text: 'Item_10'
                    }
                ]
            }
        },
        {
            id: 'DropDownList_TEST_S4_Q1',
            IG: 'DDL',
            IT: 'DDL4',
            title: 'DropDownList_Q4_TITLE',
            text: ['DropDownList_Q4_TEXT'],
            className: 'DropDownList_TEST_S4_Q1',
            widget: {
                id: 'DropDownList_TEST_W4',
                type: 'DropDownList',

                // TC14717 - list has max rows set, more items than max (should scroll)
                visibleRows: 3,
                items: [
                    {
                        value: '1',
                        text: 'Item_1'
                    },
                    {
                        value: '2',
                        text: 'Item_2'
                    },
                    {
                        value: '3',
                        text: 'Item_3'
                    },
                    {
                        value: '4',
                        text: 'Item_4'
                    },
                    {
                        value: '5',
                        text: 'Item_5'
                    },
                    {
                        value: '6',
                        text: 'Item_6'
                    },
                    {
                        value: '7',
                        text: 'Item_7'
                    },
                    {
                        value: '8',
                        text: 'Item_8'
                    },
                    {
                        value: '9',
                        text: 'Item_9'
                    },
                    {
                        value: '10',
                        text: 'Item_10'
                    }
                ]
            }
        },
        {
            id: 'DropDownList_TEST_S5_Q1',
            IG: 'DDL',
            IT: 'DDL5',
            title: 'DropDownList_Q5_TITLE',
            text: ['DropDownList_Q5_TEXT'],
            className: 'DropDownList_TEST_S5_Q1',
            widget: {
                id: 'DropDownList_TEST_W5',
                type: 'DropDownList',

                visibleRows: 3,

                // TC17383 - list has item wider than list area, should wrap
                items: [
                    {
                        value: '1',
                        text: 'Item_1_Long'
                    },
                    {
                        value: '2',
                        text: 'Item_2'
                    },
                    {
                        value: '3',
                        text: 'Item_3'
                    },
                    {
                        value: '4',
                        text: 'Item_4'
                    },
                    {
                        value: '5',
                        text: 'Item_5'
                    },
                    {
                        value: '6',
                        text: 'Item_6'
                    },
                    {
                        value: '7',
                        text: 'Item_7'
                    },
                    {
                        value: '8',
                        text: 'Item_8'
                    },
                    {
                        value: '9',
                        text: 'Item_9'
                    },
                    {
                        value: '10',
                        text: 'Item_10'
                    }
                ]
            }
        },
        {
            id: 'DropDownList_TEST_Review_Q',
            IG: 'DDL',
            IT: 'DDL6',
            title: 'SUMMARY_TITLE',
            text: ['SUMMARY_QUESTION'],
            className: 'DropDownList_TEST_Review_Q'
        }
    ]
};
