export default {
    questionnaires: [
        {
            id: 'P_DateSpinner_Diary',
            SU: 'DateSpin',
            displayName: 'DISPLAY_NAME',
            className: 'DATESPINNER_TEST',

            // Choose default affidavit per user role
            affidavit: undefined,
            previousScreen: true,
            triggerPhase: 'TREATMENT',
            screens: [
                'DateSpin_S1',
                'DateSpin_S2',
                'DateSpin_S3',
                'DateSpin_S4',
                'DateSpin_S5',
                'DateSpin_S6',
                'DateSpin_S7',
                'DateSpin_S8',
                'DateSpin_S9',
                'DateSpin_S10',
                'DateSpin_S11',
                'DateSpin_Review'
            ],
            branches: []
        }
    ],
    screens: [
        {
            id: 'DateSpin_S1',
            className: 'DateSpin_S1',
            questions: [
                {
                    id: 'DateSpin_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'DateSpin_S2',
            className: 'DateSpin_S2',
            questions: [
                {
                    id: 'DateSpin_Q2',
                    mandatory: false
                }
            ]
        },
        {
            id: 'DateSpin_S3',
            className: 'DateSpin_S3',
            questions: [
                {
                    id: 'DateSpin_Q3',
                    mandatory: true
                }
            ]
        },
        {
            id: 'DateSpin_S4',
            className: 'DateSpin_S4',
            questions: [
                {
                    id: 'DateSpin_Q4',
                    mandatory: true
                }
            ]
        },
        {
            id: 'DateSpin_S5',
            className: 'DateSpin_S5',
            questions: [
                {
                    id: 'DateSpin_Q5',
                    mandatory: true
                }
            ]
        }, {
            id: 'DateSpin_S6',
            className: 'DateSpin_S6',
            questions: [
                {
                    id: 'DateSpin_Q6',
                    mandatory: true
                }
            ]
        }, {
            id: 'DateSpin_S7',
            className: 'DateSpin_S7',
            questions: [
                {
                    id: 'DateSpin_Q7',
                    mandatory: true
                }
            ]
        }, {
            id: 'DateSpin_S8',
            className: 'DateSpin_S8',
            questions: [
                {
                    id: 'DateSpin_Q8',
                    mandatory: true
                }
            ]
        }, {
            id: 'DateSpin_S9',
            className: 'DateSpin_S9',
            questions: [
                {
                    id: 'DateSpin_Q9',
                    mandatory: true
                }
            ]
        }, {
            id: 'DateSpin_S10',
            className: 'DateSpin_S10',
            questions: [
                {
                    id: 'DateSpin_Q10',
                    mandatory: true
                }
            ]
        },
        {
            id: 'DateSpin_S11',
            className: 'DateSpin_S11',
            questions: [
                {
                    id: 'DateSpin_Q11',
                    mandatory: false
                },
                {
                    id: 'DateSpin_Q12',
                    mandatory: false
                }
            ]
        },
        {
            id: 'DateSpin_Review',
            className: 'DateSpin_Review',
            questions: [
                {
                    id: 'DateSpin_ReviewQ',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'DateSpin_Q1',
            IG: 'DateSpin',
            IT: 'DateSpin_Q1',
            skipIT: 'DateSpin_Q1_SKP',
            title: 'DATESPIN_Q1_TITLE',
            text: 'DATESPIN_Q1_TEXT',
            className: 'DateSpin_Q1',
            widget: {
                id: 'DATE_SPINNER_W_1',
                type: 'DateSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                className: 'DateSpinner',
                dateFormat: 'M-D-YY',
                displayFormat: 'M-D-YY',
                minDate: '2013/11/30',
                maxDate: '2019/11/30',
                initialDate: '2016/11/30'
            }
        },
        {
            id: 'DateSpin_Q2',
            IG: 'DateSpin',
            IT: 'DateSpin_Q2',
            skipIT: 'DateSpin_Q2_SKP',
            title: 'DATESPIN_Q2_TITLE',
            text: 'DATESPIN_Q2_TEXT',
            className: 'DateSpin_Q2',
            widget: {
                id: 'DATE_SPINNER_W_2',
                type: 'DateSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                embedded: false,
                className: 'DateSpinner',
                dateFormat: 'MMM-D-YY',
                displayFormat: 'MMM-D-YY'
            }
        },
        {
            id: 'DateSpin_Q3',
            IG: 'DateSpin',
            IT: 'DateSpin_Q3',
            skipIT: 'DateSpin_Q3_SKP',
            title: 'DATESPIN_Q3_TITLE',
            text: 'DATESPIN_Q3_TEXT',
            className: 'DateSpin_Q3',
            widget: {
                id: 'DATE_SPINNER_W_3',
                type: 'DateSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                className: 'DateSpinner',
                dateFormat: 'Do-MMMM-YYYY',
                minDate: new Date('2015/05/15')
            }
        },
        {
            id: 'DateSpin_Q4',
            IG: 'DateSpin',
            IT: 'DateSpin_Q4',
            skipIT: 'DateSpin_Q4_SKP',
            title: 'DATESPIN_Q4_TITLE',
            text: 'DATESPIN_Q4_TEXT',
            className: 'DateSpin_Q4',
            widget: {
                id: 'DATE_SPINNER_W_4',
                type: 'DateSpinner',
                label: '',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                embedded: true,
                showButtons: true,
                buttonLayout: 'vertical',
                className: 'DateSpinner',
                maxDate: () => {
                    return new Date(`${(new Date().getFullYear() + 15)}/10/04`);
                }
            }
        },
        {
            id: 'DateSpin_Q5',
            IG: 'DateSpin',
            IT: 'DateSpin_Q5',
            skipIT: 'DateSpin_Q5_SKP',
            title: 'DATESPIN_Q5_TITLE',
            text: 'DATESPIN_Q5_TEXT',
            className: 'DateSpin_Q5',
            widget: {
                id: 'DATE_SPINNER_W_5',
                type: 'DateSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                className: 'DateSpinner',
                dateFormat: 'L',
                maxDate: () => {
                    return new Date(new Date().setFullYear(new Date().getFullYear() + 35));
                },
                minDate: () => {
                    return new Date(new Date().setFullYear(new Date().getFullYear() - 35));
                }
            }
        },
        {
            id: 'DateSpin_Q6',
            IG: 'DateSpin',
            IT: 'DateSpin_Q6',
            skipIT: 'DateSpin_Q6_SKP',
            title: 'DATESPIN_Q6_TITLE',
            text: 'DATESPIN_Q6_TEXT',
            className: 'DateSpin_Q6',
            widget: {
                id: 'DATE_SPINNER_W_6',
                type: 'DateSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                embedded: false,
                showButtons: true,
                buttonLayout: 'horizontal',
                className: 'DateSpinner',
                dateFormat: 'MMMM',
                storageFormat: 'MMMM',
                displayFormat: 'MMMM',
                initialDate: new Date('1970/01/01'),
                minDate: new Date('1970/01/01'),
                maxDate: new Date('1970/12/01')
            }
        },
        {
            id: 'DateSpin_Q7',
            IG: 'DateSpin',
            IT: 'DateSpin_Q7',
            skipIT: 'DateSpin_Q7_SKP',
            title: 'DATESPIN_Q7_TITLE',
            text: 'DATESPIN_Q7_TEXT',
            className: 'DateSpin_Q7',
            widget: {
                id: 'DATE_SPINNER_W_7',
                type: 'DateSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                embedded: false,
                showButtons: true,
                buttonLayout: 'vertical',
                className: 'DateSpinner',
                dateFormat: 'M-D-YY',
                displayFormat: 'M-D-YY',
                minDate: new Date('2013/11/30'),
                maxDate: new Date('2019/11/30'),
                initialDate: new Date('2016/11/30'),
                flow: 'calculate'
            }
        },
        {
            id: 'DateSpin_Q8',
            IG: 'DateSpin',
            IT: 'DateSpin_Q8',
            skipIT: 'DateSpin_Q8_SKP',
            title: 'DATESPIN_Q8_TITLE',
            text: 'DATESPIN_Q8_TEXT',
            className: 'DateSpin_Q8',
            widget: {
                id: 'DATE_SPINNER_W_8',
                type: 'DateSpinner',
                label: '',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                embedded: true,
                showButtons: true,
                buttonLayout: 'horizontal',
                className: 'DateSpinner',
                dateFormat: 'M-D-YY',
                displayFormat: 'M-D-YY',
                minDate: new Date('2013/11/30'),
                maxDate: new Date('2019/11/30'),
                initialDate: new Date('2016/11/30'),
                flow: 'natural'
            }
        },
        {
            id: 'DateSpin_Q9',
            IG: 'DateSpin',
            IT: 'DateSpin_Q9',
            skipIT: 'DateSpin_Q9_SKP',
            title: 'DATESPIN_Q9_TITLE',
            text: 'DATESPIN_Q9_TEXT',
            className: 'DateSpin_Q9',
            widget: {
                id: 'DATE_SPINNER_W_9',
                type: 'DateSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                embedded: false,
                showButtons: true,
                className: 'DateSpinner',
                dateFormat: 'M-D-YY',
                displayFormat: 'M-D-YY',
                minDate: new Date('2013/11/30'),
                maxDate: new Date('2019/11/30'),
                initialDate: new Date('2016/11/30'),
                flow: 'rtl'
            }
        },
        {
            id: 'DateSpin_Q10',
            IG: 'DateSpin',
            IT: 'DateSpin_Q10',
            skipIT: 'DateSpin_Q10_SKP',
            title: 'DATESPIN_Q10_TITLE',
            text: 'DATESPIN_Q10_TEXT',
            className: 'DateSpin_Q10',
            widget: {
                id: 'DATE_SPINNER_W_10',
                type: 'DateSpinner',
                label: '',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                embedded: true,
                showButtons: false,
                className: 'DateSpinner',
                dateFormat: 'M-D-YY',
                displayFormat: 'M-D-YY',
                minDate: new Date('2013/11/30'),
                maxDate: new Date('2019/11/30'),
                initialDate: new Date('2016/11/30'),
                flow: 'ltr'
            }
        },
        {
            id: 'DateSpin_Q11',
            IG: 'DateSpin',
            IT: 'DateSpin_Q11',
            skipIT: 'DateSpin_Q11_SKP',
            title: 'DATESPIN_Q11_TITLE',
            text: 'DATESPIN_Q11_TEXT',
            className: 'DateSpin_Q11',
            widget: {
                id: 'DATE_SPINNER_W_11',
                type: 'DateSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                className: 'DateSpinner',
                dateFormat: 'M-D-YY',
                displayFormat: 'M-D-YY',
                minDate: '2013/11/30',
                maxDate: '2019/11/30',
                initialDate: '2016/11/30'
            }
        },
        {
            id: 'DateSpin_Q12',
            IG: 'DateSpin',
            IT: 'DateSpin_Q12',
            skipIT: 'DateSpin_Q12_SKP',
            title: 'DATESPIN_Q12_TITLE',
            text: 'DATESPIN_Q12_TEXT',
            className: 'DateSpin_Q12',
            widget: {
                id: 'DATE_SPINNER_W_12',
                type: 'DateSpinner',
                label: '',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                embedded: true,
                showButtons: true,
                buttonLayout: 'horizontal',
                className: 'DateSpinner',
                dateFormat: 'MMMM',
                storageFormat: 'MMMM',
                displayFormat: 'MMMM',
                initialDate: new Date('1970/01/01'),
                minDate: new Date('1970/01/01'),
                maxDate: new Date('1970/12/01')
            }
        },
        {
            id: 'DateSpin_ReviewQ',
            IG: 'DateSpin',
            IT: 'DateSpin_ReviewQ',
            title: 'SUMMARY_TITLE',
            text: ['SUMMARY_QUESTION'],
            className: 'DateSpin_ReviewQ'
        }
    ]
};
