export default {
    questionnaires: [
        {
            id: 'Site_CheckBoxAffidavit_Diary',
            SU: 'Site_CheckBoxAffidavit_Diary',
            displayName: 'DISPLAY_NAME',
            className: 'Site_CheckBoxAffidavit_Diary',
            previousScreen: true,
            affidavit: 'CheckBoxAffidavit',
            screens: [
                'Site_CheckBoxAffidavit_DIARY_S_1'
            ]
        }
    ],
    screens: [
        {
            id: 'Site_CheckBoxAffidavit_DIARY_S_1',
            className: 'Site_CheckBoxAffidavit_DIARY_S_1',
            questions: [{
                id: 'Site_CheckBoxAffidavit_DIARY_Q1',
                mandatory: true
            }]
        }
    ],
    questions: [
        {
            id: 'Site_CheckBoxAffidavit_DIARY_Q1',
            IG: 'Site_CheckBoxAffidavit',
            IT: 'Site_CheckBoxAffidavit_Q1',
            title: 'Site_CheckBoxAffidavit_TITLE',
            text: [
                'Site_CheckBoxAffidavit_TEXT'
            ],
            widget: {
                id: 'SITE_CHECKBOXAFFIDAVIT_TEST_S1_Q1',
                type: 'CheckBox',
                label: 'Pick a number',
                templates: { },
                answers: [
                    { text: 'TEST_ANSWER', value: '0' }
                ]
            },
            className: 'Site_CheckBoxAffidavit_DIARY_Q1'
        }
    ]
};
