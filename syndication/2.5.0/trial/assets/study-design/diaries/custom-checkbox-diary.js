export default {
    questionnaires: [
        {
            id: 'P_CustomCheckBox_Test',
            SU: 'CustomCheckBox_Test',
            displayName: 'DISPLAY_NAME',
            className: 'CUSTOMCHECKBOX_TEST',
            affidavit: undefined,
            previousScreen: true,
            screens: ['CUSTOMCHECKBOX_TEST_S1', 'CUSTOMCHECKBOX_TEST_S2']
        }
    ],

    screens: [
        {
            id: 'CUSTOMCHECKBOX_TEST_S1',
            className: 'CUSTOMCHECKBOX_TEST_S1',
            questions: [
                { id: 'CUSTOMCHECKBOX_TEST_S1_Q1', mandatory: true }
            ]
        },
        {
            id: 'CUSTOMCHECKBOX_TEST_S2',
            className: 'CUSTOMCHECKBOX_TEST_S2',
            questions: [
                { id: 'CUSTOMCHECKBOX_TEST_S2_Q1', mandatory: true }
            ]
        }
    ],

    questions: [
        {
            id: 'CUSTOMCHECKBOX_TEST_S1_Q1',
            IG: 'CustomCheckBox_Test',
            IT: 'CUSTOMCHECKBOX_TEST_S1_Q1',
            skipIT: '',
            title: 'QUESTION_1_TITLE',
            text: ['QUESTION_1'],
            className: 'CUSTOMCHECKBOX_TEST_S1_Q1',
            widget: {
                id: 'CUSTOMCHECKBOX_TEST_S1_Q1',
                type: 'CustomCheckBox',
                label: 'Pick a number',
                templates: {},
                answers: [
                    { text: 'TEXT1', value: 'Try Again' },
                    { text: 'TEXT2', value: 'Skip Communication(Warning: New PEs will not be captured if you choose this option.' }
                ]
            }
        },
        {
            id: 'CUSTOMCHECKBOX_TEST_S2_Q1',
            IG: 'CustomCheckBox_Test',
            IT: 'CUSTOMCHECKBOX_TEST_S2_Q1',
            skipIT: '',
            title: 'QUESTION_1_TITLE',
            text: ['QUESTION_1'],
            className: 'CUSTOMCHECKBOX_TEST_S2_Q1',
            widget: {
                id: 'CUSTOMCHECKBOX_TEST_S2_Q1',
                type: 'CustomCheckBox',
                label: 'Pick a number',
                templates: {},
                answers: [
                    { text: 'TEXT1', value: 'Try Again' },
                    { text: 'TEXT3', value: 'Skip Communication(Warning: New PEs will not be captured if you choose this option.' }
                ]
            }
        }
    ]
};
