export default {
    questionnaires: [
        {
            id: 'P_Blank_Diary',
            SU: 'BLANK',
            displayName: 'DISPLAY_NAME',
            className: 'BLANK_DIARY',
            affidavit: undefined,
            previousScreen: true,
            screens: []
        }
    ],

    screens: [
        {
        }
    ],

    questions: [
        {
        }
    ]
};
