export default {
    questionnaires: [
        {
            id: 'P_Complex_Branching_Test',
            SU: 'Complex_Brancing_Test',
            displayName: 'DISPLAY_NAME',
            className: 'COMPLEX_BRANCHING_TEST',
            affidavit: undefined,
            screens: [
                'CBT_SCREEN_A',
                'CBT_SCREEN_B',
                'CBT_SCREEN_C',
                'CBT_SCREEN_D',
                'CBT_SCREEN_E',
                'CBT_SCREEN_F',
                'CBT_SCREEN_G_REVIEW'
            ],
            branches: [{
                branchFrom: 'CBT_SCREEN_D',
                branchTo: 'CBT_SCREEN_D',
                clearBranchedResponses: true,
                branchFunction: 'cbt_logicForScreenD'
            }]
        }
    ],
    screens: [
        {
            id: 'CBT_SCREEN_A',
            className: 'CBT_SCREEN_A',
            questions: [
                { id: 'CBT_SCREEN_A_Q1', mandatory: true }
            ]
        }, {
            id: 'CBT_SCREEN_B',
            className: 'CBT_SCREEN_B',
            questions: [
                { id: 'CBT_SCREEN_B_Q1', mandatory: true }
            ]
        }, {
            id: 'CBT_SCREEN_C',
            className: 'CBT_SCREEN_C',
            questions: [
                { id: 'CBT_SCREEN_C_Q1', mandatory: true }
            ]
        }, {
            id: 'CBT_SCREEN_D',
            className: 'CBT_SCREEN_D',
            questions: [
                { id: 'CBT_SCREEN_D_Q1', mandatory: true }
            ]
        }, {
            id: 'CBT_SCREEN_E',
            className: 'CBT_SCREEN_E',
            questions: [
                { id: 'CBT_SCREEN_E_Q1', mandatory: true }
            ]
        }, {
            id: 'CBT_SCREEN_F',
            className: 'CBT_SCREEN_F',
            questions: [
                { id: 'CBT_SCREEN_F_Q1', mandatory: true }
            ]
        }, {
            id: 'CBT_SCREEN_G_REVIEW',
            className: 'CBT_SCREEN_G_REVIEW',
            questions: [
                { id: 'CBT_SCREEN_G_REVIEW_Q1', mandatory: true }
            ]
        }
    ],
    questions: [
        {
            id: 'CBT_SCREEN_A_Q1',
            IG: 'CBT',
            IT: 'CBT_Q1',
            title: 'Q1_TITLE',
            text: [
                'HELP_TEXT',
                'Q_1'
            ],
            className: 'CBT_SCREEN_A_Q1',
            widget: {
                id: 'CBT_SCREEN_A_Q1_W1',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    { text: 'RB_1', value: '1' },
                    { text: 'RB_2', value: '2' }
                ]
            }
        }, {
            id: 'CBT_SCREEN_B_Q1',
            IG: 'CBT',
            IT: 'CBT_Q2',
            title: 'Q2_TITLE',
            text: [
                'HELP_TEXT',
                'Q_2'
            ],
            className: 'CBT_SCREEN_B_Q1',
            widget: {
                id: 'CBT_SCREEN_B_Q1_W1',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    { text: 'RB_1', value: '1' },
                    { text: 'RB_2', value: '2' }
                ]
            }
        }, {
            id: 'CBT_SCREEN_C_Q1',
            IG: 'CBT',
            IT: 'CBT_Q3',
            title: 'Q3_TITLE',
            text: [
                'HELP_TEXT',
                'Q_3'
            ],
            className: 'CBT_SCREEN_C_Q1',
            widget: {
                id: 'CBT_SCREEN_C_Q1_W1',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    { text: 'RB_1', value: '1' },
                    { text: 'RB_2', value: '2' }
                ]
            }
        }, {
            id: 'CBT_SCREEN_D_Q1',
            IG: 'CBT',
            IT: 'CBT_Q4',
            title: 'Q4_TITLE',
            text: [
                'HELP_TEXT',
                'Q_4'
            ],
            className: 'CBT_SCREEN_D_Q1',
            widget: {
                id: 'CBT_SCREEN_D_Q1_W1',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    { text: 'RB_1_A', value: 'A' },
                    { text: 'RB_2_B', value: 'B' },
                    { text: 'RB_3', value: 'F' },
                    { text: 'RB_4', value: 'none' }
                ]
            }
        }, {
            id: 'CBT_SCREEN_E_Q1',
            IG: 'CBT',
            IT: 'CBT_Q5',
            title: 'Q5_TITLE',
            text: [
                'HELP_TEXT',
                'Q_5'
            ],
            className: 'CBT_SCREEN_E_Q1',
            widget: {
                id: 'CBT_SCREEN_E_Q1_W1',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    { text: 'RB_1', value: '1' },
                    { text: 'RB_2', value: '2' }
                ]
            }
        }, {
            id: 'CBT_SCREEN_F_Q1',
            IG: 'CBT',
            IT: 'CBT_Q6',
            title: 'Q6_TITLE',
            text: [
                'HELP_TEXT',
                'Q_6'
            ],
            className: 'CBT_SCREEN_F_Q1',
            widget: {
                id: 'CBT_SCREEN_F_Q1_W1',
                type: 'RadioButton',
                className: 'RadioButton',
                templates: {},
                answers: [
                    { text: 'RB_1', value: '1' },
                    { text: 'RB_2', value: '2' }
                ]
            }
        }, {
            id: 'CBT_SCREEN_G_REVIEW_Q1',
            IG: 'CBT',
            IT: 'CBT_Q7',
            title: 'Q7_TITLE',
            text: [
                'HELP_TEXT',
                'Q_7_REVIEW'
            ],
            className: 'CBT_SCREEN_G_Q1'

        }
    ]
};
