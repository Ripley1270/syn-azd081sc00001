export default {
    questionnaires: [
        {
            id: 'P_HorizontalVAS_Diary',
            SU: 'VAS',
            displayName: 'DISPLAY_NAME',
            className: 'Custom_HVAS_Class',
            affidavit: undefined,
            previousScreen: true,
            screens: [
                'HVAS_TEST_S1',
                'HVAS_TEST_S2',
                'HVAS_TEST_S3',
                'HVAS_TEST_S4',
                'HVAS_TEST_S5',
                'HVAS_TEST_S6',
                'HVAS_TEST_S7',
                'HVAS_TEST_S8',
                'HVAS_TEST_S9',
                'HVAS_TEST_S10',
                'HVAS_TEST_S11',
                'HVAS_TEST_S12'
            ]
        }
    ],
    screens: [
        {
            id: 'HVAS_TEST_S1',
            className: 'HVAS_TEST_S1',
            questions: [
                {
                    id: 'HVAS_TEST_S1_Q1',
                    mandatory: false
                }
            ]
        },
        {
            id: 'HVAS_TEST_S2',
            className: 'HVAS_TEST_S2',
            questions: [
                {
                    id: 'HVAS_TEST_S2_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'HVAS_TEST_S3',
            className: 'HVAS_TEST_S3',
            questions: [
                {
                    id: 'HVAS_TEST_S3_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'HVAS_TEST_S4',
            className: 'HVAS_TEST_S4',
            questions: [
                {
                    id: 'HVAS_TEST_S4_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'HVAS_TEST_S5',
            className: 'HVAS_TEST_S5',
            questions: [
                {
                    id: 'HVAS_TEST_S5_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'HVAS_TEST_S6',
            className: 'HVAS_TEST_S6',
            questions: [
                {
                    id: 'HVAS_TEST_S6_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'HVAS_TEST_S7',
            className: 'HVAS_TEST_S7',
            questions: [
                {
                    id: 'HVAS_TEST_S7_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'HVAS_TEST_S8',
            className: 'HVAS_TEST_S8',
            questions: [
                {
                    id: 'HVAS_TEST_S8_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'HVAS_TEST_S9',
            className: 'HVAS_TEST_S9',
            questions: [
                {
                    id: 'HVAS_TEST_S9_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'HVAS_TEST_S10',
            className: 'HVAS_TEST_S10',
            questions: [
                {
                    id: 'HVAS_TEST_S10_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'HVAS_TEST_S11',
            className: 'HVAS_TEST_S11',
            questions: [
                {
                    id: 'HVAS_TEST_S11_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'HVAS_TEST_S12',
            className: 'HVAS_TEST_S12',
            questions: [
                {
                    id: 'HVAS_TEST_S12_Q1',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'HVAS_TEST_S1_Q1',
            IG: 'VAS',
            IT: 'VAS1',
            skipIT: 'VAS1_SKP',
            title: 'HVAS_Q1_TITLE',
            text: ['HVAS_Q1_TEXT'],
            className: 'HVAS_TEST_S1_Q1',
            widget: {
                id: 'HVAS_TEST_W1',
                type: 'VAS',
                displayAs: 'Horizontal',
                reverseOnRtl: false,

                initialCursorDisplay: false,
                pointer: {
                    isVisible: true,
                    color: '#FFFFFF'
                },
                anchors: {
                    font: {
                        name: 'Arial',
                        size: 24,
                        color: 'yellow'
                    },
                    min: {
                        text: 'NO_PAIN',
                        value: 0
                    },
                    max: {
                        text: 'EXTREME_PAIN',
                        value: 100
                    }
                },
                selectedValue: {
                    selectionBox: {
                        borderColor: 'red',
                        fillColor: '#000',
                        borderWidth: 5
                    },
                    font: {
                        name: 'Helvetica',
                        size: 20,
                        color: 'red'
                    },
                    isVisible: true,
                    location: 'both'
                }
            }
        },
        {
            id: 'HVAS_TEST_S2_Q1',
            IG: 'VAS',
            IT: 'VAS2',
            skipIT: 'VAS2_SKP',
            title: 'HVAS_Q2_TITLE',
            text: ['HVAS_Q2_TEXT'],
            className: 'HVAS_TEST_S2_Q1',
            widget: {
                id: 'HVAS_TEST_W2',
                type: 'VAS',
                displayAs: 'Horizontal',
                reverseOnRtl: true,
                initialCursorDisplay: true,
                pointer: {
                    isVisible: true
                },
                anchors: {
                    font: {
                        name: 'Arial',
                        size: 20
                    },
                    min: {
                        text: 'NO_PAIN',
                        value: 10
                    },
                    max: {
                        text: 'EXTREME_PAIN',
                        value: 20
                    }
                },
                selectedValue: {
                    font: {
                        name: 'Courier',
                        size: 28,
                        color: 'red'
                    },
                    isVisible: true,
                    location: 'dynamic'
                },
                customProperties: {
                    labels: {
                        position: 'above',
                        arrow: false,
                        justified: 'screenEdge'
                    }
                }
            }
        },
        {
            id: 'HVAS_TEST_S3_Q1',
            IG: 'VAS',
            IT: 'VAS3',
            skipIT: 'VAS3_SKP',
            title: 'HVAS_Q3_TITLE',
            text: ['HVAS_Q3_TEXT'],
            className: 'HVAS_TEST_S3_Q1',
            widget: {
                id: 'HVAS_TEST_W3',
                type: 'VAS',
                displayAs: 'Horizontal',
                initialCursorDisplay: true,
                pointer: {
                    isVisible: true,
                    location: 'above'
                },
                anchors: {
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                },
                selectedValue: {
                    isVisible: true,
                    location: 'dynamic'
                },
                customProperties: {
                    labels: {
                        position: 'above',
                        arrow: false,
                        justified: 'screenEdge'
                    }
                }
            }
        },
        {
            id: 'HVAS_TEST_S4_Q1',
            IG: 'VAS',
            IT: 'VAS4',
            skipIT: 'VAS4_SKP',
            title: 'HVAS_Q4_TITLE',
            text: ['HVAS_Q4_TEXT'],
            className: 'HVAS_TEST_S4_Q1',
            widget: {
                id: 'HVAS_TEST_W4',
                type: 'VAS',
                displayAs: 'Horizontal',
                initialCursorDisplay: true,
                pointer: {
                    isVisible: false
                },
                anchors: {
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                },
                selectedValue: {
                    isVisible: true,
                    font: {
                        name: 'Courier',
                        size: 40,
                        color: '#FCF03D'
                    },
                    selectionBox: {
                        borderWidth: 2,
                        borderColor: '#FCF03D',
                        fillColor: '#000'
                    }
                },
                customProperties: {
                    labels: {
                        position: 'above',
                        arrow: false,
                        justified: 'screenEdge'
                    }
                }
            }
        },
        {
            id: 'HVAS_TEST_S5_Q1',
            IG: 'VAS',
            IT: 'VAS5',
            skipIT: 'VAS5_SKP',
            title: 'HVAS_Q5_TITLE',
            text: ['HVAS_Q5_TEXT'],
            className: 'HVAS_TEST_S5_Q1',
            widget: {
                id: 'HVAS_TEST_W5',
                type: 'VAS',
                displayAs: 'Horizontal',
                initialCursorDisplay: true,
                anchors: {
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                },
                selectedValue: {
                    isVisible: true
                },
                customProperties: {
                    labels: {
                        position: 'above',
                        arrow: false,
                        justified: 'screenEdge'
                    }
                }
            }
        },
        {
            id: 'HVAS_TEST_S6_Q1',
            IG: 'VAS',
            IT: 'VAS6',
            skipIT: 'VAS6_SKP',
            title: 'HVAS_Q6_TITLE',
            text: ['HVAS_Q6_TEXT'],
            className: 'HVAS_TEST_S6_Q1',
            widget: {
                id: 'HVAS_TEST_W6',
                type: 'VAS',
                displayAs: 'Horizontal',
                initialCursorDisplay: true,
                selectedValue: {
                    isVisible: true
                },
                pointer: {
                    isVisible: true
                },
                anchors: {
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                },
                customProperties: {
                    labels: {
                        position: 'above',
                        arrow: false,
                        justified: 'screenEdge'
                    }
                }
            }
        },
        {
            id: 'HVAS_TEST_S7_Q1',
            IG: 'VAS',
            IT: 'VAS7',
            skipIT: 'VAS7_SKP',
            title: 'HVAS_Q7_TITLE',
            text: ['HVAS_Q7_TEXT'],
            className: 'HVAS_TEST_S7_Q1',
            widget: {
                id: 'HVAS_TEST_W7',
                type: 'VAS',
                displayAs: 'Horizontal',
                initialCursorDisplay: true,
                pointer: {
                    isVisible: true
                },
                anchors: {
                    swapMinMaxLocation: false,
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                },
                selectedValue: {
                    isVisible: true
                },
                customProperties: {
                    labels: {
                        position: 'above',
                        arrow: false,
                        justified: 'screenEdge'
                    }
                }
            }
        },
        {
            id: 'HVAS_TEST_S8_Q1',
            IG: 'VAS',
            IT: 'VAS8',
            skipIT: 'VAS8_SKP',
            title: 'HVAS_Q8_TITLE',
            text: ['HVAS_Q8_TEXT'],
            className: 'HVAS_TEST_S8_Q1',
            widget: {
                id: 'HVAS_TEST_W8',
                type: 'VAS',
                displayAs: 'Horizontal',
                selectedValue: {
                    isVisible: true,
                    location: 'both'
                },
                pointer: {
                    isVisible: true,
                    displayInitially: false
                },
                anchors: {
                    swapMinMaxLocation: true,
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                },
                customProperties: {
                    labels: {
                        position: 'below',
                        arrow: true,
                        justified: 'screenEdge'
                    }
                }
            }
        },
        {
            id: 'HVAS_TEST_S9_Q1',
            IG: 'VAS',
            IT: 'VAS9',
            skipIT: 'VAS9_SKP',
            title: 'HVAS_Q9_TITLE',
            text: ['HVAS_Q9_TEXT'],
            className: 'HVAS_TEST_S9_Q1',
            widget: {
                id: 'HVAS_TEST_W9',
                type: 'VAS',
                displayAs: 'Horizontal',
                selectedValue: {
                    isVisible: false
                },
                pointer: {
                    isVisible: true
                },
                anchors: {
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                },
                customProperties: {
                    labels: {
                        position: 'above',
                        arrow: true,
                        justified: 'center'
                    }
                }
            }
        },
        {
            id: 'HVAS_TEST_S10_Q1',
            IG: 'VAS',
            IT: 'VAS10',
            skipIT: 'VAS10_SKP',
            title: 'HVAS_Q10_TITLE',
            text: ['HVAS_Q10_TEXT'],
            className: 'HVAS_TEST_S10_Q1',
            widget: {
                id: 'HVAS_TEST_W10',
                type: 'VAS',
                displayAs: 'Horizontal',
                selectedValue: {
                    isVisible: true
                },
                pointer: {
                    isVisible: true
                },
                anchors: {
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                },
                customProperties: {
                    labels: {
                        position: 'above',
                        arrow: true,
                        justified: 'center'
                    }
                }
            }
        },
        {
            id: 'HVAS_TEST_S11_Q1',
            IG: 'VAS',
            IT: 'VAS11',
            skipIT: 'VAS11_SKP',
            title: 'HVAS_Q11_TITLE',
            text: ['HVAS_Q11_TEXT'],
            className: 'HVAS_TEST_S11_Q1',
            widget: {
                id: 'HVAS_TEST_W11',
                type: 'VAS',
                displayAs: 'Horizontal',
                selectedValue: {
                    isVisible: true
                },
                pointer: {
                    isVisible: true
                },
                anchors: {
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                },
                customProperties: {
                    labels: {
                        position: 'below',
                        arrow: true,
                        justified: 'center'
                    }
                }
            }
        },
        {
            id: 'HVAS_TEST_S12_Q1',
            IG: 'VAS',
            IT: 'VAS12',
            skipIT: 'VAS12_SKP',
            title: 'SUMMARY_TITLE',
            text: ['SUMMARY_QUESTION'],
            className: 'HVAS_TEST_S12_Q1'
        }
    ]
};
