export default {
    questionnaires: [
        {
            id: 'P_Toss_Diary',
            SU: 'Toss',
            displayName: 'DISPLAY_NAME',
            className: 'TOSS',
            previousScreen: true,
            accessRoles: ['subject'],
            affidavit: 'P_SignatureAffidavit',
            allowTranscriptionMode: true,
            screens: [
                'TOSS_S_0',
                'TOSS_S_1',
                'TOSS_S_2',
                'TOSS_S_3',
                'TOSS_S_4',
                'TOSS_S_5',
                'TOSS_S_6',
                'TOSS_S_7'
            ]
        }
    ],

    screens: [
        {
            id: 'TOSS_S_0',
            className: 'TOSS_S_0',
            questions: [
                { id: 'TOSS_Q_0', mandatory: false }
            ]
        }, {
            id: 'TOSS_S_1',
            className: 'TOSS_S_1',
            questions: [
                { id: 'TOSS_Q_1', mandatory: true }
            ]
        }, {
            id: 'TOSS_S_2',
            className: 'TOSS_S_2',
            questions: [
                { id: 'TOSS_Q_2', mandatory: true }
            ]
        }, {
            id: 'TOSS_S_3',
            className: 'TOSS_S_3',
            questions: [
                { id: 'TOSS_Q_3', mandatory: true }
            ]
        }, {
            id: 'TOSS_S_4',
            className: 'TOSS_S_4',
            questions: [
                { id: 'TOSS_Q_4', mandatory: false }
            ]
        }, {
            id: 'TOSS_S_5',
            className: 'TOSS_S_5',
            questions: [
                { id: 'TOSS_Q_5', mandatory: true }
            ]
        }, {
            id: 'TOSS_S_6',
            className: 'TOSS_S_6',
            questions: [
                { id: 'TOSS_Q_6', mandatory: true }
            ]
        }, {
            id: 'TOSS_S_7',
            className: 'TOSS_S_7',
            questions: [
                { id: 'TOSS_Q_7', mandatory: true }
            ]
        }
    ],

    questions: [
        {
            id: 'TOSS_Q_0',
            IG: 'Toss',
            text: 'QUESTION_0',
            skipIT: 'TOSS_Q_0_SKP',
            className: 'TOSS_Q_0',
            widget: {
                id: 'TOSS_W_0',
                type: 'CheckBox',
                className: 'CheckBox',
                template: {},
                answers: [
                    { text: 'ITCHY_EYES', value: '0', IT: 'TOSS_Q_0_0' },
                    { text: 'TEARING_EYES', value: '1', IT: 'TOSS_Q_0_1' },
                    { text: 'RED_EYES', value: '2', IT: 'TOSS_Q_0_2' },
                    { text: 'SNEEZING', value: '3', IT: 'TOSS_Q_0_3' },
                    { text: 'ITCHY_NOSE', value: '4', IT: 'TOSS_Q_0_4' },
                    { text: 'RUNNY_NOSE', value: '5', IT: 'TOSS_Q_0_5' },
                    { text: 'BLOCKED_NOSE', value: '6', IT: 'TOSS_Q_0_6' },
                    { text: 'NONE', value: '7', IT: 'TOSS_Q_0_7', exclude: ['All'] }
                ]
            }
        },
        {

            id: 'TOSS_Q_1',
            IG: 'Toss',
            text: 'QUESTION_1',
            className: 'TOSS_Q_1',
            widget: {
                id: 'TOSS_W_1',
                type: 'CheckBox',
                className: 'CheckBox',
                template: {},
                answers: [
                    { text: 'ABSENT', value: '0', IT: 'TOSS_Q_1_0', exclude: ['All'] },
                    { text: 'MILD_SYMPTOMS', value: '1', IT: 'TOSS_Q_1_1' },
                    { text: 'MODERATE_SYMPTOMS', value: '2', IT: 'TOSS_Q_1_2' },
                    { text: 'SEVERE_SYMPTOMS', value: '3', IT: 'TOSS_Q_1_3' }
                ]
            }
        },
        {
            id: 'TOSS_Q_2',
            IG: 'Toss',
            text: 'QUESTION_2',
            className: 'CustomCheckBox',
            widget: {
                id: 'TOSS_W_2',
                type: 'CustomCheckBox',
                template: {},
                answers: [
                    { text: 'ABSENT', value: '0', IT: 'TOSS_Q_2_0' },
                    { text: 'MILD_SYMPTOMS', value: '1', IT: 'TOSS_Q_2_1' },
                    { text: 'MODERATE_SYMPTOMS', value: '2', IT: 'TOSS_Q_2_2' },
                    { text: 'SEVERE_SYMPTOMS', value: '3', IT: 'TOSS_Q_2_3' }
                ]
            }
        },
        {
            id: 'TOSS_Q_3',
            IG: 'Toss',
            text: 'QUESTION_3',
            skipIT: 'TOSS_Q_3_SKP',
            className: 'TOSS_Q_3',
            widget: {
                id: 'TOSS_W_3',
                type: 'CustomCheckBox',
                template: {},
                paramFunctions: [{
                    id: 'answerFunc',
                    evaluate: 'customTossAnswers3',
                    screenshots: {
                        values: [
                            [
                                { text: 'TOSS_0', value: '0', IT: 'TOSS_Q_3_0' },
                                { text: 'TOSS_1', value: '1', IT: 'TOSS_Q_3_1' },
                                { text: 'TOSS_2', value: '2', IT: 'TOSS_Q_3_2' }
                            ]

                        ]
                    }
                }]
            }
        },
        {
            id: 'TOSS_Q_4',
            IG: 'Toss',
            text: 'QUESTION_4',
            skipIT: 'TOSS_Q_4_SKP',
            className: 'CustomCheckBox',
            widget: {
                id: 'TOSS_W_4',
                type: 'CustomCheckBox',
                template: {},
                answers: [
                    { text: 'ABSENT', value: '0', IT: 'TOSS_Q_4_0' },
                    { text: 'MILD_SYMPTOMS', value: '1', IT: 'TOSS_Q_4_1' },
                    { text: 'MODERATE_SYMPTOMS', value: '2', IT: 'TOSS_Q_4_2' },
                    { text: 'SEVERE_SYMPTOMS', value: '3', IT: 'TOSS_Q_4_3' }
                ]
            }
        },
        {
            id: 'TOSS_Q_5',
            IG: 'Toss',
            text: 'QUESTION_5',
            className: 'CheckBox',
            widget: {
                id: 'TOSS_W_5',
                type: 'CheckBox',
                template: {},
                answers: [
                    { text: 'ABSENT', value: '0', IT: 'TOSS_Q_5_0' },
                    { text: 'MILD_SYMPTOMS', value: '1', IT: 'TOSS_Q_5_1' },
                    { text: 'MODERATE_SYMPTOMS', value: '2', IT: 'TOSS_Q_5_2' },
                    { text: 'SEVERE_SYMPTOMS', value: '3', IT: 'TOSS_Q_5_3' }
                ]
            }
        },
        {
            id: 'TOSS_Q_6',
            IG: 'Toss',
            text: 'QUESTION_6',
            className: 'CustomCheckBox',
            widget: {
                id: 'TOSS_W_6',
                type: 'CustomCheckBox',
                template: {},
                answers: [
                    { text: 'ABSENT', value: '0', IT: 'TOSS_Q_6_0' },
                    { text: 'MILD_SYMPTOMS', value: '1', IT: 'TOSS_Q_6_1' },
                    { text: 'MODERATE_SYMPTOMS', value: '2', IT: 'TOSS_Q_6_2' },
                    { text: 'SEVERE_SYMPTOMS', value: '3', IT: 'TOSS_Q_6_3' }
                ]
            }
        },
        {
            id: 'TOSS_Q_7',
            IG: 'Toss',
            text: 'QUESTION_7',
            className: 'CustomCheckBox',
            widget: {
                id: 'TOSS_W_7',
                type: 'CustomCheckBox',
                template: {},
                answers: [
                    { text: 'ABSENT', value: '0', exclude: ['All'], IT: 'TOSS_Q_7_0' },
                    { text: 'MILD_SYMPTOMS', value: '1', IT: 'TOSS_Q_7_1' },
                    { text: 'MODERATE_SYMPTOMS', value: '2', IT: 'TOSS_Q_7_2' },
                    { text: 'SEVERE_SYMPTOMS', value: '3', IT: 'TOSS_Q_7_3' }
                ]
            }
        }
    ]
};
