export default {
    questionnaires: [
        {
            id: 'P_NoWidget',
            SU: 'P_NoWidget',
            displayName: 'DISPLAY_NAME',
            className: 'P_NoWidget',
            affidavit: false,
            previousScreen: false,
            screens: [
                'NO_WIDGET_S_1',
                'NO_WIDGET_S_2'
            ],
            branches: []
        }
    ],

    screens: [
        {
            id: 'NO_WIDGET_S_1',
            className: 'NO_WIDGET_S_1',
            questions:
            [
                {
                    id: 'NO_WIDGET_Q_1', mandatory: true
                }
            ]
        },
        {
            id: 'NO_WIDGET_S_2',
            className: 'NO_WIDGET_S_2',
            questions: [
                { id: 'NO_WIDGET_Q_2' },
                { id: 'NO_WIDGET_Q_1' }
            ]
        }
    ],

    questions: [
        {
            id: 'NO_WIDGET_Q_1',
            IG: 'No_Widget_1',
            IT: '',
            skipIT: '',
            title: 'NO_WIDGET_Q1_TITLE',
            text: ['NO_WIDGET_Q1_MESSAGE'],
            className: 'NO_WIDGET'
        },
        {
            id: 'NO_WIDGET_Q_2',
            IG: 'No_Widget_1',
            IT: '',
            skipIT: '',
            title: 'NO_WIDGET_Q2_TITLE',
            text: ['NO_WIDGET_Q2_MESSAGE'],
            className: 'NO_WIDGET'
        }
    ]
};
