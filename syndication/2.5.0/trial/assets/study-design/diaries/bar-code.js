export default {
    questionnaires: [
        {
            id: 'P_BarcodeDiary',
            SU: 'Barcode',
            displayName: 'DISPLAY_NAME',
            className: 'Barcode',
            previousScreen: false,
            affidavit: undefined,
            screens: [
                'BARCODE_S_1',
                'BARCODE_S_2',
                'BARCODE_S_3',
                'BARCODE_S_4'
            ]
        }
    ],

    screens: [
        {
            id: 'BARCODE_S_1',
            className: 'BARCODE_S_1',
            questions: [
                { id: 'BARCODE_Q_1', mandatory: true }
            ]
        },
        {
            id: 'BARCODE_S_2',
            className: 'BARCODE_S_2',
            questions: [
                { id: 'BARCODE_Q_2', mandatory: false }
            ]
        },
        {
            id: 'BARCODE_S_3',
            className: 'BARCODE_S_3',
            questions: [
                { id: 'BARCODE_Q_3', mandatory: false }
            ]
        },
        {
            id: 'BARCODE_S_4',
            className: 'BARCODE_S_4',
            questions: [
                {
                    id: 'BARCODE_Q_4',
                    mandatory: false
                }
            ]
        }
    ],

    questions: [
        {
            id: 'BARCODE_Q_1',
            IG: 'Barcode',
            IT: 'BC_Q1',
            text: 'QUESTION_1',
            skipIT: 'BC_Q1_SKP',
            className: 'BARCODE_Q_1',
            widget: {
                id: 'BARCODE_W_1',
                type: 'BarcodeScanner',
                className: 'BarcodeScanner',
                hideManualInput: false,

                // regexToMatch : /^ABC/i, <<-- Built in string match function
                validation: {
                    validationFunc: 'checkStringMatch',
                    params: {
                        matchString: 'ABC'
                    }
                },
                validationErrors: [
                    {
                        property: 'matchesRegex',
                        errorType: 'banner',
                        errString: 'Q1_WRONG_ANS'
                    }
                ]
            }
        },
        {
            id: 'BARCODE_Q_2',
            IG: 'Barcode',
            IT: 'BC_Q2',
            skipIT: 'BC_Q2_SKP',
            text: 'QUESTION_2',
            className: 'BARCODE_Q_2',
            widget: {
                id: 'BARCODE_W_2',
                type: 'BarcodeScanner',
                className: 'BarcodeScanner',
                hideManualInput: true
            }
        },
        {
            id: 'BARCODE_Q_3',
            IG: 'Barcode',
            IT: 'BC_Q3',
            skipIT: 'BC_Q3_SKP',
            text: 'QUESTION_3',
            className: 'BARCODE_Q_3',
            widget: {
                id: 'BARCODE_W_3',
                type: 'BarcodeScanner',
                className: 'BarcodeScanner'
            }
        },
        {
            id: 'BARCODE_Q_4',
            IG: 'Barcode',
            IT: 'BC_Q4',
            text: 'QUESTION_4',
            className: 'BARCODE_Q_4',
            widget: {
                id: 'BARCODE_W_4',
                type: 'ReviewScreen',
                className: 'ReviewScreen',
                screenFunction: 'barCodeList',
                templates: {
                    itemsTemplate: 'P_BarcodeDiary:ScreenItems'
                },
                buttons: []
            }
        }
    ]
};
