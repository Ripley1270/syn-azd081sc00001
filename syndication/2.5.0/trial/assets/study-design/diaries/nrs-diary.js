export default {
    questionnaires: [
        {
            id: 'P_NRS_Diary',
            SU: 'NRS',
            displayName: 'DISPLAY_NAME',
            className: 'NRS_DIARY',
            previousScreen: true,

            // Choose default affidavit per user role
            affidavit: undefined,
            screens: [
                'NRS_DIARY_S_1',
                'NRS_DIARY_S_2',
                'NRS_DIARY_S_3',
                'NRS_DIARY_S_4',
                'NRS_DIARY_S_5',
                'NRS_DIARY_S_6',
                'NRS_DIARY_S_7',
                'NRS_DIARY_S_8',
                'NRS_DIARY_S_10',
                'NRS_DIARY_S_11',
                'NRS_DIARY_S_12',
                'NRS_DIARY_S_14',
                'NRS_DIARY_S_15',
                'NRS_DIARY_S_16',
                'NRS_DIARY_S_17',
                'NRS_DIARY_S_18',
                'NRS_DIARY_S_19',
                'NRS_DIARY_S_20',
                'NRS_DIARY_S_21',
                'NRS_DIARY_S_13'
            ]
        }
    ],
    screens: [
        {
            id: 'NRS_DIARY_S_1',
            className: 'NRS_DIARY_S_1',
            questions: [{
                id: 'NRS_DIARY_Q1',
                mandatory: true
            }]
        }, {
            id: 'NRS_DIARY_S_2',
            className: 'NRS_DIARY_S_2',
            questions: [{
                id: 'NRS_DIARY_Q2',
                mandatory: true
            }]
        }, {
            id: 'NRS_DIARY_S_3',
            className: 'NRS_DIARY_S_3',
            questions: [{
                id: 'NRS_DIARY_Q3',
                mandatory: true
            }]
        }, {
            id: 'NRS_DIARY_S_4',
            className: 'NRS_DIARY_S_4',
            questions: [
                {
                    id: 'NRS_DIARY_Q4',
                    mandatory: false
                }
            ]
        }, {
            id: 'NRS_DIARY_S_5',
            className: 'NRS_DIARY_S_5',
            questions: [
                {
                    id: 'NRS_DIARY_Q5',
                    mandatory: false
                }
            ]
        }, {
            id: 'NRS_DIARY_S_6',
            className: 'NRS_DIARY_S_6',
            questions: [
                {
                    id: 'NRS_DIARY_Q6',
                    mandatory: false
                }
            ]
        }, {
            id: 'NRS_DIARY_S_7',
            className: 'NRS_DIARY_S_7',
            questions: [
                {
                    id: 'NRS_DIARY_Q7',
                    mandatory: true
                }
            ]
        }, {
            id: 'NRS_DIARY_S_8',
            className: 'NRS_DIARY_S_8',
            questions: [
                {
                    id: 'NRS_DIARY_Q8',
                    mandatory: true
                }
            ]
        }, {
            id: 'NRS_DIARY_S_10',
            className: 'NRS_DIARY_S_10',
            questions: [
                {
                    id: 'NRS_DIARY_Q10',
                    mandatory: true
                }
            ]
        }, {
            id: 'NRS_DIARY_S_11',
            className: 'NRS_DIARY_S_11',
            questions: [
                {
                    id: 'NRS_DIARY_Q11',
                    mandatory: false
                }
            ]
        }, {
            id: 'NRS_DIARY_S_12',
            className: 'NRS_DIARY_S_12',
            questions: [
                {
                    id: 'NRS_DIARY_Q12',
                    mandatory: true
                }
            ]
        }, {
            id: 'NRS_DIARY_S_13',
            className: 'NRS_DIARY_S_13',
            questions: [
                {
                    id: 'NRS_DIARY_Q13',
                    mandatory: true
                }
            ]
        },
        {
            id: 'NRS_DIARY_S_14',
            className: 'NRS_DIARY_S_14',
            questions: [
                {
                    id: 'NRS_DIARY_Q14',
                    mandatory: false
                }
            ]
        }, {
            id: 'NRS_DIARY_S_15',
            className: 'NRS_DIARY_S_15',
            questions: [
                {
                    id: 'NRS_DIARY_Q15',
                    mandatory: false
                }
            ]
        }, {
            id: 'NRS_DIARY_S_16',
            className: 'NRS_DIARY_S_16',
            questions: [
                {
                    id: 'NRS_DIARY_Q16',
                    mandatory: false
                }
            ]
        }, {
            id: 'NRS_DIARY_S_17',
            className: 'NRS_DIARY_S_17',
            questions: [
                {
                    id: 'NRS_DIARY_Q17',
                    mandatory: false
                }
            ]
        }, {
            id: 'NRS_DIARY_S_18',
            className: 'NRS_DIARY_S_18',
            questions: [
                {
                    id: 'NRS_DIARY_Q18',
                    mandatory: false
                }
            ]
        }, {
            id: 'NRS_DIARY_S_19',
            className: 'NRS_DIARY_S_19',
            questions: [
                {
                    id: 'NRS_DIARY_Q19',
                    mandatory: false
                }
            ]
        }, {
            id: 'NRS_DIARY_S_20',
            className: 'NRS_DIARY_S_20',
            questions: [
                {
                    id: 'NRS_DIARY_Q20',
                    mandatory: false
                }
            ]
        }, {
            id: 'NRS_DIARY_S_21',
            className: 'NRS_DIARY_S_21',
            questions: [
                {
                    id: 'NRS_DIARY_Q21',
                    mandatory: false
                }
            ]
        }
    ],
    questions: [
        {
            id: 'NRS_DIARY_Q1',
            IG: 'NRS',
            IT: 'NRS_Q1',
            title: 'NRS_TITLE_1',
            text: [
                'NRS_TEXT'
            ],
            className: 'NRS_DIARY_Q1',
            widget: {
                id: 'NRS_DIARY_W1',
                type: 'NumericRatingScale',
                templates: {},
                reverseOnRtl: true,
                width: '85',
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    arrow: true,
                    justification: 'center',
                    position: 'below'
                },
                answers: [
                    { text: 'NRS_0', value: '0' },
                    { text: 'NRS_1', value: '1' },
                    { text: 'NRS_2', value: '2' },
                    { text: 'NRS_3', value: '3' },
                    { text: 'NRS_4', value: '4' },
                    { text: 'NRS_5', value: '5' },
                    { text: 'NRS_6', value: '6' },
                    { text: 'NRS_7', value: '7' },
                    { text: 'NRS_8', value: '8' },
                    { text: 'NRS_9', value: '9' },
                    { text: 'NRS_10', value: '10' }
                ]
            }

        }, {
            id: 'NRS_DIARY_Q2',
            IG: 'NRS',
            IT: 'NRS_Q2',
            title: 'NRS_TITLE_2',
            text: [
                'NRS_TEXT'
            ],
            className: 'NRS_DIARY_Q2',
            widget: {
                id: 'NRS_DIARY_W2',
                type: 'NumericRatingScale',
                templates: {},
                width: '160px',
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    justification: 'outside',
                    position: 'below'
                },
                answers: [
                    { text: 'NRS_0', value: '0' },
                    { text: 'NRS_1', value: '1' },
                    { text: 'NRS_2', value: '2' },
                    { text: 'NRS_3', value: '3' },
                    { text: 'NRS_4', value: '4' }
                ]
            }

        }, {
            id: 'NRS_DIARY_Q3',
            IG: 'NRS',
            IT: 'NRS_Q3',
            title: 'NRS_TITLE_3',
            text: [
                'NRS_TEXT'
            ],
            className: 'NRS_DIARY_Q3',
            widget: {
                id: 'NRS_DIARY_W3',
                type: 'NumericRatingScale',
                templates: {},
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    justification: 'inside',
                    position: 'above',
                    arrow: true,

                    // Number of buttons the markers can extend to
                    spaceAllowed: '2'
                },
                answers: [
                    { text: 'NRS_10', value: '10' },
                    { text: 'NRS_20', value: '20' },
                    { text: 'NRS_30', value: '30' },
                    { text: 'NRS_40', value: '40' },
                    { text: 'NRS_50', value: '50' },
                    { text: 'NRS_60', value: '60' },
                    { text: 'NRS_70', value: '70' },
                    { text: 'NRS_80', value: '80' },
                    { text: 'NRS_90', value: '90' },
                    { text: 'NRS_100', value: '100' }
                ]
            }
        }, {
            id: 'NRS_DIARY_Q4',
            IG: 'NRS',
            title: 'NRS_TITLE_4',
            text: ['NRS_TEXT1', 'NRS_TEXT2'],
            className: 'NRS_DIARY_Q4',
            widget: {
                id: 'NRS_DIARY_W4',
                type: 'ReviewScreen',
                screenFunction: 'painNRS',
                templates: {
                    itemsTemplate: 'NRS:ScreenItems'
                },
                buttons: [{
                    id: 1,
                    availability: 'always',
                    text: 'ALERT',
                    actionFunction: 'nrsAlertAction'
                }]
            }
        }, {
            id: 'NRS_DIARY_Q5',
            IG: 'NRS',
            text: ['NRS_TEXT_COMPLETE_1'],
            className: 'NRS_DIARY_Q5',
            widget: {
                type: 'ReviewScreen',
                id: 'NRS_DIARY_Q5_W1',
                screenFunction: 'defaultSummary',
                buttons: [
                    {
                        text: 'ALERT_TEXT',
                        id: 'alertBtnId',
                        availability: 'always',
                        actionFunction: 'alertAction'
                    }, {
                        text: 'AVERAGE_TEXT',
                        id: 'averageBtnId',
                        actionFunction: 'averageAction',
                        availability: 'always'
                    }
                ]
            }
        }, {
            id: 'NRS_DIARY_Q6',
            IG: 'NRS',
            IT: 'NRS_Q6',
            skipIT: 'NRS_Q6_SKP',
            title: 'NRS_TITLE_6',
            text: [
                'NRS_TEXT_Q6'
            ],
            className: 'NRS_DIARY_Q6',
            widget: {
                id: 'NRS_DIARY_W6',
                type: 'NumericRatingScale',
                templates: {},
                width: '100',
                reverseOnRtl: true,
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    arrow: true,
                    justification: 'inside',
                    position: 'above',

                    // Number of buttons the markers can extend to
                    spaceAllowed: '2'
                },
                answers: [
                    { text: 'NRS_1', value: '1' },
                    { text: 'NRS_2', value: '2' },
                    { text: 'NRS_3', value: '3' },
                    { text: 'NRS_4', value: '4' },
                    { text: 'NRS_5', value: '5' },
                    { text: 'NRS_10', value: '10' },
                    { text: 'NRS_20', value: '20' },
                    { text: 'NRS_30', value: '30' },
                    { text: 'NRS_40', value: '40' },
                    { text: 'NRS_50', value: '50' },
                    { text: 'NRS_6', value: '6' },
                    { text: 'NRS_7', value: '7' },
                    { text: 'NRS_8', value: '8' },
                    { text: 'NRS_9', value: '9' },
                    { text: 'NRS_0', value: '0' },
                    { text: 'NRS_60', value: '60' },
                    { text: 'NRS_70', value: '70' },
                    { text: 'NRS_80', value: '80' },
                    { text: 'NRS_90', value: '90' },
                    { text: 'NRS_100', value: '100' }
                ]
            }
        }, {
            id: 'NRS_DIARY_Q7',
            IG: 'NRS',
            IT: 'NRS_Q7',
            title: 'NRS_TITLE_7',
            text: [
                'NRS_TEXT'
            ],
            className: 'NRS_DIARY_Q7',
            widget: {
                id: 'NRS_DIARY_W7',
                type: 'NumericRatingScale',
                templates: {},
                width: '100',
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    arrow: false,
                    justification: 'inside',
                    position: 'above',

                    // Number of buttons the markers can extend to
                    spaceAllowed: '2'
                },
                answers: [
                    { text: 'NRS_0_NEG', value: '0' },
                    { text: 'NRS_1_NEG', value: '1' },
                    { text: 'NRS_2_NEG', value: '2' },
                    { text: 'NRS_3_NEG', value: '3' },
                    { text: 'NRS_4_NEG', value: '4' },
                    { text: 'NRS_5_NEG', value: '5' },
                    { text: 'NRS_6_NEG', value: '6' },
                    { text: 'NRS_7_NEG', value: '7' },
                    { text: 'NRS_8_NEG', value: '8' },
                    { text: 'NRS_9_NEG', value: '9' }
                ]
            }
        },
        {
            id: 'NRS_DIARY_Q8',
            IG: 'NRS',
            IT: 'NRS_Q8',
            title: 'NRS_TITLE_8',
            text: [
                'NRS_TEXT'
            ],
            className: 'CustomNumericRatingScale',
            widget: {
                id: 'NRS_DIARY_W8',
                type: 'NumericRatingScale',
                templates: {},
                width: '100',
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    justification: 'inside',
                    position: 'below',

                    // Number of buttons the markers can extend to
                    spaceAllowed: '5'
                },
                answers: [
                    { text: 'NRS_10', value: '10' },
                    { text: 'NRS_20', value: '20' },
                    { text: 'NRS_30', value: '30' },
                    { text: 'NRS_40', value: '40' },
                    { text: 'NRS_50', value: '50' },
                    { text: 'NRS_60', value: '60' }
                ]
            }
        }, {
            id: 'NRS_DIARY_Q10',
            IG: 'NRS',
            IT: 'NRS_Q10',
            title: 'NRS_TITLE_10',
            text: [
                'NRS_TEXT'
            ],
            className: 'NumericRatingScale',
            widget: {
                id: 'NRS_DIARY_W10',
                type: 'NumericRatingScale',
                templates: {},
                width: '100',
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    justification: 'inside',
                    position: 'below',

                    // Number of buttons the markers can extend to
                    spaceAllowed: '5'
                },
                paramFunctions: [{
                    id: 'answerFunc',
                    evaluate: 'nrsAnswers',
                    screenshots: {
                        values: [
                            [
                                { text: 'NRS_0_CHAR', value: '0' },
                                { text: 'NRS_1_CHAR', value: '1' },
                                { text: 'NRS_2_CHAR', value: '2' }
                            ]

                        ]
                    }
                }]
            }
        }, {
            id: 'NRS_DIARY_Q11',
            IG: 'NRS',
            IT: 'NRS_Q11',
            skipIT: 'NRS_Q11_SKP',
            title: 'NRS_TITLE_11',
            text: [
                'NRS_TEXT_11'
            ],
            className: 'NumericRatingScale',
            widget: {
                id: 'NRS_DIARY_W10',
                type: 'NumericRatingScale',
                templates: {},
                width: '100',
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    justification: 'inside',
                    position: 'below'
                },
                answers: [
                    { text: 'NRS_DYN_1', value: '0' },
                    { text: 'NRS_DYN_2', value: '1' }
                ]
            }
        },
        {
            id: 'NRS_DIARY_Q12',
            IG: 'NRS',
            IT: 'NRS_Q12',
            title: 'NRS_TITLE_12',
            text: [
                'NRS_TEXT'
            ],
            className: 'CustomNumericRatingScale',
            widget: {
                id: 'NRS_DIARY_W12',
                type: 'NumericRatingScale',
                templates: {},
                width: '100',
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    justification: 'inside',
                    position: 'below',

                    // Number of buttons the markers can extend to
                    spaceAllowed: '5'
                },
                paramFunctions: [{
                    id: 'answerFunc',
                    evaluate: 'nrsAnswers2',
                    screenshots: {
                        values: [
                            [
                                { text: 'NRS_0_CHAR', value: '0' },
                                { text: 'NRS_1_CHAR', value: '1' },
                                { text: 'NRS_2_CHAR', value: '2' },
                                { text: 'NRS_3_CHAR', value: '3' }
                            ]

                        ]
                    }
                }]
            }
        },
        {
            id: 'NRS_DIARY_Q14',
            IG: 'NRS',
            IT: 'NRS_Q14',
            skipIT: 'NRS_Q14_SKP',
            title: 'NRS_TITLE_14',
            text: [
                'NRS_TEXT4'
            ],
            className: 'NRS_DIARY_Q14',
            widget: {
                id: 'NRS_DIARY_W14',
                type: 'NumericRatingScale',
                templates: {},
                reverseOnRtl: true,
                width: '75',
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    arrow: true,
                    justification: 'inside',
                    position: 'below'
                },
                answers: [
                    { text: 'NRS_0', value: '0' },
                    { text: 'NRS_1', value: '1' },
                    { text: 'NRS_2', value: '2' },
                    { text: 'NRS_3', value: '3' },
                    { text: 'NRS_4', value: '4' },
                    { text: 'NRS_5', value: '5' },
                    { text: 'NRS_6', value: '6' },
                    { text: 'NRS_7', value: '7' },
                    { text: 'NRS_8', value: '8' },
                    { text: 'NRS_9', value: '9' },
                    { text: 'NRS_10', value: '10' }
                ]
            }

        },
        {
            id: 'NRS_DIARY_Q15',
            IG: 'NRS',
            IT: 'NRS_Q15',
            skipIT: 'NRS_Q15_SKP',
            title: 'NRS_TITLE_15',
            text: [
                'NRS_TEXT4'
            ],
            className: 'NRS_DIARY_Q15',
            widget: {
                id: 'NRS_DIARY_W15',
                type: 'NumericRatingScale',
                templates: {},
                reverseOnRtl: true,
                width: '100',
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    arrow: true,
                    justification: 'inside',
                    position: 'below'
                },
                answers: [
                    { text: 'NRS_0', value: '0' },
                    { text: 'NRS_1', value: '1' },
                    { text: 'NRS_2', value: '2' },
                    { text: 'NRS_3', value: '3' },
                    { text: 'NRS_4', value: '4' },
                    { text: 'NRS_5', value: '5' },
                    { text: 'NRS_6', value: '6' },
                    { text: 'NRS_7', value: '7' },
                    { text: 'NRS_8', value: '8' },
                    { text: 'NRS_9', value: '9' },
                    { text: 'NRS_10', value: '10' }
                ]
            }

        }, {
            id: 'NRS_DIARY_Q16',
            IG: 'NRS',
            IT: 'NRS_Q16',
            skipIT: 'NRS_Q16_SKP',
            title: 'NRS_TITLE_16',
            text: [
                'NRS_TEXT4'
            ],
            className: 'NRS_DIARY_Q16',
            widget: {
                id: 'NRS_DIARY_W16',
                type: 'NumericRatingScale',
                templates: {},
                reverseOnRtl: true,
                width: '160px',
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    arrow: true,
                    justification: 'inside',
                    position: 'below'
                },
                answers: [
                    { text: 'NRS_0', value: '0' },
                    { text: 'NRS_1', value: '1' },
                    { text: 'NRS_2', value: '2' },
                    { text: 'NRS_3', value: '3' },
                    { text: 'NRS_4', value: '4' },
                    { text: 'NRS_5', value: '5' },
                    { text: 'NRS_6', value: '6' },
                    { text: 'NRS_7', value: '7' },
                    { text: 'NRS_8', value: '8' },
                    { text: 'NRS_9', value: '9' },
                    { text: 'NRS_10', value: '10' }
                ]
            }

        }, {
            id: 'NRS_DIARY_Q17',
            IG: 'NRS',
            IT: 'NRS_Q17',
            skipIT: 'NRS_Q17_SKP',
            title: 'NRS_TITLE_17',
            text: [
                'NRS_TEXT4'
            ],
            className: 'NRS_DIARY_Q17',
            widget: {
                id: 'NRS_DIARY_W17',
                type: 'NumericRatingScale',
                templates: {},
                reverseOnRtl: true,
                markers: {
                    left: 'LEFT',
                    right: 'RIGHT',
                    arrow: true,
                    justification: 'inside',
                    position: 'below'
                },
                answers: [
                    { text: 'NRS_0', value: '0' },
                    { text: 'NRS_1', value: '1' },
                    { text: 'NRS_2', value: '2' },
                    { text: 'NRS_3', value: '3' },
                    { text: 'NRS_4', value: '4' },
                    { text: 'NRS_5', value: '5' },
                    { text: 'NRS_6', value: '6' },
                    { text: 'NRS_7', value: '7' },
                    { text: 'NRS_8', value: '8' },
                    { text: 'NRS_9', value: '9' },
                    { text: 'NRS_10', value: '10' }
                ]
            }

        }, {
            id: 'NRS_DIARY_Q18',
            IG: 'NRS',
            IT: 'NRS_Q18',
            skipIT: 'NRS_Q18_SKP',
            title: 'NRS_TITLE_18',
            text: [
                'NRS_TEXT4'
            ],
            className: 'NRS_DIARY_Q18',
            widget: {
                id: 'NRS_DIARY_W18',
                type: 'NumericRatingScale',
                templates: {},
                width: '100%',
                reverseOnRtl: true,
                markers: {
                    left: 'LEFTY',
                    right: 'RIGHTY',
                    justification: 'inside',

                    // Number of buttons the markers can extend to
                    spaceAllowed: '1'
                },
                answers: [
                    { text: 'NRS_0', value: '0' },
                    { text: 'NRS_1', value: '1' },
                    { text: 'NRS_2', value: '2' },
                    { text: 'NRS_3', value: '3' },
                    { text: 'NRS_4', value: '4' },
                    { text: 'NRS_5', value: '5' }
                ]
            }
        }, {
            id: 'NRS_DIARY_Q19',
            IG: 'NRS',
            IT: 'NRS_Q19',
            skipIT: 'NRS_Q19_SKP',
            title: 'NRS_TITLE_19',
            text: [
                'NRS_TEXT4'
            ],
            className: 'NRS_DIARY_Q19',
            widget: {
                id: 'NRS_DIARY_W19',
                type: 'NumericRatingScale',
                templates: {},
                width: '100%',
                reverseOnRtl: true,
                markers: {
                    left: 'LEFTY',
                    right: 'RIGHTY',
                    justification: 'inside',

                    // Number of buttons the markers can extend to
                    spaceAllowed: '2'
                },
                answers: [
                    { text: 'NRS_0', value: '0' },
                    { text: 'NRS_1', value: '1' },
                    { text: 'NRS_2', value: '2' },
                    { text: 'NRS_3', value: '3' },
                    { text: 'NRS_4', value: '4' },
                    { text: 'NRS_5', value: '5' }
                ]
            }
        }, {
            id: 'NRS_DIARY_Q20',
            IG: 'NRS',
            IT: 'NRS_Q20',
            skipIT: 'NRS_Q20_SKP',
            title: 'NRS_TITLE_20',
            text: [
                'NRS_TEXT4'
            ],
            className: 'NRS_DIARY_Q20',
            widget: {
                id: 'NRS_DIARY_W20',
                type: 'NumericRatingScale',
                templates: {},
                width: '100%',
                reverseOnRtl: true,
                markers: {
                    left: 'LEFTY',
                    right: 'RIGHTY',
                    justification: 'inside',

                    // Number of buttons the markers can extend to
                    spaceAllowed: '3'
                },
                answers: [
                    { text: 'NRS_0', value: '0' },
                    { text: 'NRS_1', value: '1' },
                    { text: 'NRS_2', value: '2' },
                    { text: 'NRS_3', value: '3' },
                    { text: 'NRS_4', value: '4' },
                    { text: 'NRS_5', value: '5' }
                ]
            }
        }, {
            id: 'NRS_DIARY_Q21',
            IG: 'NRS',
            IT: 'NRS_Q21',
            skipIT: 'NRS_Q21_SKP',
            title: 'NRS_TITLE_21',
            text: [
                'NRS_TEXT4'
            ],
            className: 'NRS_DIARY_Q21',
            widget: {
                id: 'NRS_DIARY_W21',
                type: 'NumericRatingScale',
                templates: {},
                width: '100%',
                reverseOnRtl: true,
                markers: {
                    left: 'LEFTY',
                    right: 'RIGHTY',
                    justification: 'inside'
                },
                answers: [
                    { text: 'NRS_0', value: '0' },
                    { text: 'NRS_1', value: '1' },
                    { text: 'NRS_2', value: '2' },
                    { text: 'NRS_3', value: '3' },
                    { text: 'NRS_4', value: '4' },
                    { text: 'NRS_5', value: '5' }
                ]
            }
        },
        {
            id: 'NRS_DIARY_Q13',
            IG: 'NRS',
            IT: 'NRS_DIARY_Q13',
            title: 'SUMMARY_TITLE',
            text: ['SUMMARY_QUESTION'],
            className: 'NRS_DIARY_Q13'
        }
    ]
};
