export default {
    questionnaires: [
        {
            id: 'P_Browser_Diary',
            SU: 'Browser',
            displayName: 'DISPLAY_NAME',
            className: 'Browser',
            accessRoles: ['subject', 'site', 'admin'],
            affidavit: undefined,
            screens: [
                'Browser_S0',
                'Browser_S1',
                'Browser_S2',
                'Browser_Review'
            ]
        }
    ],
    screens: [
        {
            id: 'Browser_S0',
            className: 'Browser_S0',
            questions: [
                { id: 'Browser_Q0', mandatory: false }
            ]
        }, {
            id: 'Browser_S1',
            className: 'Browser_S1',
            questions: [
                { id: 'Browser_Q1', mandatory: true }
            ]
        }, {
            id: 'Browser_S2',
            className: 'Browser_S2',
            questions: [
                { id: 'Browser_Q2', mandatory: true }
            ]
        }, {
            id: 'Browser_Review',
            className: 'Browser_Review',
            questions: [
                { id: 'Browser_Review_Q', mandatory: true }
            ]
        }
    ],
    questions: [
        {
            id: 'Browser_Q0',
            IT: 'Browser_Q0',
            IG: 'Browser',
            text: 'QUESTION_0',
            skipIT: 'Browser_Q0_SKP',
            className: 'Browser_Q0',
            widget: {
                id: 'Browser_Q0_W1',
                type: 'Browser',
                url: 'https://thieme-compliance.de/en/informed-consent-forms-diomed/',
                linkText: 'LINK_TEXT'
            }
        }, {
            id: 'Browser_Q1',
            IT: 'Browser_Q1',
            IG: 'Browser',
            text: 'QUESTION_1',
            skipIT: 'Browser_Q1_SKP',
            className: 'Browser_Q1',
            widget: {
                id: 'Browser_Q1_W1',
                type: 'Browser',
                url: 'https://bjanaszek.github.io/ert/input.html',
                linkText: 'LINK_TEXT'
            }
        }, {
            id: 'Browser_Q2',
            IT: 'Browser_Q2',
            IG: 'Browser',
            text: 'QUESTION_2',
            skipIT: 'Browser_Q2_SKP',
            className: 'Browser_Q2',
            widget: {
                id: 'Browser_Q2_W1',
                type: 'Browser',
                url: 'https://player.vimeo.com/video/113247705',
                linkText: 'LINK_TEXT'
            }
        }, {
            id: 'Browser_Review_Q',
            IG: 'Browser',
            title: 'SUMMARY_TITLE',
            text: ['SUMMARY_QUESTION'],
            className: 'Browser_Review_Q'
        }
    ]
};
