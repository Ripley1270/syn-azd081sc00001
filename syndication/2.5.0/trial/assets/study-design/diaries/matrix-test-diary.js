export default {
    questionnaires: [
        {
            id: 'P_Matrix_Diary',
            SU: 'Radio_Matrix',
            displayName: 'DISPLAY_NAME',
            className: 'MATRIX_DIARY',
            previousScreen: true,
            affidavit: undefined,
            screens: [
                'Matrix_Diary_S1',
                'Matrix_Diary_S2',
                'Matrix_Diary_S3',
                'Matrix_Diary_S4',
                'Matrix_Diary_S5'
            ]
        }
    ],
    screens: [
        {
            id: 'Matrix_Diary_S1',
            className: 'Matrix_Diary_S1',
            questions: [
                {
                    id: 'Matrix_Diary_Q1',
                    mandatory: true
                }
            ]
        }, {
            id: 'Matrix_Diary_S2',
            className: 'Matrix_Diary_S2',
            questions: [
                {
                    id: 'Matrix_Diary_Q2',
                    mandatory: true
                }
            ]
        }, {
            id: 'Matrix_Diary_S3',
            className: 'Matrix_Diary_S3',
            questions: [
                {
                    id: 'Matrix_Diary_Q3',
                    mandatory: true
                }
            ]
        }, {
            id: 'Matrix_Diary_S4',
            className: 'Matrix_Diary_S4',
            questions: [
                {
                    id: 'Matrix_Diary_Q4',
                    mandatory: true
                }
            ]
        }, {
            id: 'Matrix_Diary_S5',
            className: 'Matrix_Diary_S5',
            questions: [
                {
                    id: 'Matrix_Diary_Q5',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'Matrix_Diary_Q1',
            IG: 'Radio_Matrix',
            text: ['MATRIX_DIARY_Q1'],
            className: 'MATRIX_DIARY_Q1',
            widget: {
                type: 'MatrixRadioButton',
                id: 'Matrix_Diary_Q1_Widget',
                className: 'MATRIX_DIARY_Q1 MatrixCustomClass',
                questions: [
                    {
                        IT: 'Radio_Matrix_1',
                        text: 'MD_RB_Q1_TITLE'
                    },
                    {
                        IT: 'Radio_Matrix_2',
                        text: 'MD_RB_Q2_TITLE'
                    },
                    {
                        IT: 'Radio_Matrix_3',
                        text: 'MD_RB_Q3_TITLE'
                    },
                    {
                        IT: 'Radio_Matrix_4',
                        text: 'MD_RB_Q4_TITLE'
                    },
                    {
                        IT: 'Radio_Matrix_5',
                        text: 'MD_RB_Q5_TITLE'
                    }
                ],
                matrixAnswers: [
                    { text: 'MATRIX_ANSWER_SETB_1', value: '1' },
                    { text: 'MATRIX_ANSWER_SETB_2', value: '2' }
                ]
            }
        }, {
            id: 'Matrix_Diary_Q2',
            IG: 'Radio_Matrix',
            text: ['MATRIX_DIARY_Q2'],
            className: 'MATRIX_DIARY_Q2',
            widget: {
                id: 'Matrix_Diary_Q2_Widget',
                type: 'MatrixRadioButton',
                className: 'MATRIX_DIARY_Q2',
                questionWidth: '65%',
                headerRowColor: '#AED6F1',
                questions: [
                    {
                        IT: 'Radio_Matrix_6',
                        text: 'MD_S2_Q1_TITLE'
                    },
                    {
                        IT: 'Radio_Matrix_7',
                        text: 'MD_S2_Q2_TITLE'
                    },
                    {
                        IT: 'Radio_Matrix_8',
                        text: 'MD_S2_Q3_TITLE'
                    }
                ],
                matrixAnswers: [
                    {
                        text: 'MATRIX_ANSWER_1',
                        value: '1'
                    }, {
                        text: 'MATRIX_ANSWER_2',
                        value: '2'
                    }, {
                        text: 'MATRIX_ANSWER_3',
                        value: '3'
                    }
                ]
            }
        }, {
            id: 'Matrix_Diary_Q3',
            IG: 'Radio_Matrix',
            text: ['MATRIX_DIARY_Q3'],
            className: 'MATRIX_DIARY_Q3',
            widget: {
                id: 'Matrix_Diary_Q3_Widget',
                type: 'MatrixRadioButton',
                className: 'MATRIX_DIARY_Q3',
                rowColor: '#FCF3CF',
                questions: [
                    {
                        IT: 'Radio_Matrix_9',
                        text: 'MD_S3_Q1_TITLE'
                    },
                    {
                        IT: 'Radio_Matrix_10',
                        text: 'MD_S3_Q2_TITLE'
                    },
                    {
                        IT: 'Radio_Matrix_11',
                        text: 'MD_S3_Q3_TITLE'
                    }
                ],
                matrixAnswers: [
                    {
                        text: 'MATRIX_ANSWER_SETB_1',
                        value: '1'
                    }, {
                        text: 'MATRIX_ANSWER_SETB_2',
                        value: '2'
                    }, {
                        text: 'MATRIX_ANSWER_SETB_3',
                        value: '3'
                    }
                ]
            }
        }, {
            id: 'Matrix_Diary_Q4',
            IG: 'Radio_Matrix',
            text: ['MATRIX_DIARY_Q4'],
            className: 'MATRIX_DIARY_Q4',
            widget: {
                id: 'Matrix_Diary_Q4_Widget',
                type: 'MatrixRadioButton',
                className: 'MATRIX_DIARY_Q4',
                rowColor: '#EBF5FB',
                alternatingRowColor: '#E8F8F5',
                questions: [
                    {
                        IT: 'Radio_Matrix_12',
                        text: 'MD_S4_Q1_TITLE'
                    },
                    {
                        IT: 'Radio_Matrix_13',
                        text: 'MD_S4_Q2_TITLE'
                    },
                    {
                        IT: 'Radio_Matrix_14',
                        text: 'MD_S4_Q3_TITLE'
                    }, {
                        IT: 'Radio_Matrix_15',
                        text: 'MD_S4_Q4_TITLE'
                    }, {
                        IT: 'Radio_Matrix_16',
                        text: 'MD_S4_Q5_TITLE'
                    }, {
                        IT: 'Radio_Matrix_17',
                        text: 'MD_S4_Q6_TITLE'
                    }, {
                        IT: 'Radio_Matrix_18',
                        text: 'MD_S4_Q7_TITLE'
                    }
                ],
                matrixAnswers: [
                    {
                        text: 'MATRIX_ANSWER_SETC_1',
                        value: '1'
                    }, {
                        text: 'MATRIX_ANSWER_SETC_2',
                        value: '2'
                    }, {
                        text: 'MATRIX_ANSWER_SETC_3',
                        value: '3'
                    }, {
                        text: 'MATRIX_ANSWER_SETC_4',
                        value: '4'
                    }
                ]
            }
        }, {
            id: 'Matrix_Diary_Q5',
            IG: 'Radio_Matrix',
            IT: 'Radio_Matrix_19',
            title: 'MATRIX_DIARY_Q5_TITLE',
            text: ['MATRIX_DIARY_Q5'],
            className: 'MATRIX_DIARY_Q5'
        }
    ]
};
