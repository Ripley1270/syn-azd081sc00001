/* // CORE FILES
import Add_User_Diary from './add-user';
import Deactivate_User from './deactivate-user';
import Activate_User from './activate-user';
import Edit_User from './edit-user';
import New_Patient_Diary from './new-patient';

import Skip_Visit_Diary from './skip-visit';
import Edit_Patient_Diary from './edit-patient-diary';
import First_Site_User from './first-site-user';
import Time_Confirmation from './time-confirmation';
 // END CORE FILES */

import Checkbox_Test_Diary from './checkbox-test-diary';
import Custom_Checkbox_Diary from './custom-checkbox-diary';
import Meds_Diary from './meds-diary';
import NRS_Diary from './nrs-diary';
import Looping_Diary from './looping-diary';
import Looping_Diary_2 from './looping-diary-2';
import Looping_Diary_3 from './looping-diary-3';
import Bar_Code_Diary from './bar-code';
import Daily_Diary from './daily-diary';
import Date_Diary from './date-diary';
import No_Widget_Diary from './no-widget';

// import Num_Spin_Diary from './num-spin';
import Radio_Button_Diary from './radio-button';
import Custom_Radio_Button_Diary from './custom-radio-button';
import Screening_Toss_Diary from './screening-toss';
import Site_Demo_Diary from './site-demo';
import Site_Diary from './site-diary';
import Toss_Diary from './toss-diary';
import Welcome_Diary from './welcome-diary';
import HVAS_Test_Diary from './hvas-diary';
import VVAS_Test_Diary from './vvas-diary';
import EQ5D_Test_Diary from './eq5d-diary';
import NumberSpinner_Test_Diary from './numberspinner-test-diary';
import NumericTextbox_Diary from './numeric-textbox-diary';
import DropDownList from './dropdownlist-test-diary';
import FreeText_Diary from './free-text-diary';
import Matrix_Test_Diary from './matrix-test-diary';
import Date_Spinner from './datespinner-test-diary';
import Complex_Branching_Diary from './complex-branching-diary';
import Browser_Diary from './browser-widget-test';
import Image_Widget from './image-widget-diary';

import Patient_CheckBoxAffidavit_Diary from './patient-checkboxaffidavit-diary';
import Site_CheckBoxAffidavit_Diary from './site-checkboxaffidavit-diary';

import { mergeObjects } from 'core/utilities/languageExtensions';

export default mergeObjects({},

    /* // CORE FILES
    Add_User_Diary, New_Patient_Diary, Skip_Visit_Diary, Edit_Patient_Diary,
    First_Site_User, Deactivate_User, Activate_User, Edit_User, Time_Confirmation,
    // END CORE FILES */
    EQ5D_Test_Diary, Radio_Button_Diary, Custom_Radio_Button_Diary, Checkbox_Test_Diary, Custom_Checkbox_Diary, Daily_Diary, Welcome_Diary,
    Meds_Diary, No_Widget_Diary, Site_Demo_Diary, Bar_Code_Diary, Toss_Diary, Screening_Toss_Diary, Date_Diary, NRS_Diary,
    HVAS_Test_Diary, VVAS_Test_Diary, Looping_Diary, NumberSpinner_Test_Diary, Site_Diary, NumericTextbox_Diary,
    DropDownList, FreeText_Diary, Matrix_Test_Diary, Date_Spinner, Looping_Diary_2, Looping_Diary_3, Complex_Branching_Diary, Patient_CheckBoxAffidavit_Diary,
    Site_CheckBoxAffidavit_Diary, Browser_Diary, Image_Widget);
