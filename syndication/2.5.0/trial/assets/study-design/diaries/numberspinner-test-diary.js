export default {
    questionnaires: [
        {
            id: 'P_NumberSpinner_Diary',
            SU: 'NumberSpinner_Diary',
            displayName: 'DISPLAY_NAME',
            className: 'NUMBERSPINNER_TEST',

            // Choose default affidavit per user role
            affidavit: undefined,
            previousScreen: true,
            triggerPhase: 'TREATMENT',
            screens: [
                'Spin_S1',
                'Spin_S2',
                'Spin_S3',
                'Spin_S4',
                'Spin_S5',
                'Spin_S6',
                'Spin_S7',
                'Spin_S8',
                'Spin_S9',

                // Diaries do not currently transfer.
                'Spin_S10',
                'Spin_S11',
                'Spin_S13',
                'Spin_S12'
            ],
            branches: []
        }
    ],
    screens: [
        {
            id: 'Spin_S1',
            className: 'Spin_S1',
            questions: [
                {
                    id: 'Spin_Q1',
                    mandatory: false
                }
            ]
        },
        {
            id: 'Spin_S2',
            className: 'Spin_S2',
            questions: [
                {
                    id: 'Spin_Q2',
                    mandatory: false
                }
            ]
        },
        {
            id: 'Spin_S3',
            className: 'Spin_S3',
            questions: [
                {
                    id: 'Spin_Q3',
                    mandatory: false
                }
            ]
        },
        {
            id: 'Spin_S4',
            className: 'Spin_S4',
            questions: [
                {
                    id: 'Spin_Q4',
                    mandatory: false
                }
            ]
        },
        {
            id: 'Spin_S5',
            className: 'Spin_S5',
            questions: [
                {
                    id: 'Spin_Q5',
                    mandatory: false
                }
            ]
        }, {
            id: 'Spin_S6',
            className: 'Spin_S6',
            questions: [
                {
                    id: 'Spin_Q6',
                    mandatory: false
                }
            ]
        }, {
            id: 'Spin_S7',
            className: 'Spin_S7',
            questions: [
                {
                    id: 'Spin_Q7',
                    mandatory: false
                }
            ]
        },
        {
            id: 'Spin_S8',
            className: 'Spin_S8',
            questions: [
                {
                    id: 'Spin_Q8',
                    mandatory: false
                }
            ]
        },
        {
            id: 'Spin_S9',
            className: 'Spin_S9',
            questions: [
                {
                    id: 'Spin_Q9',
                    mandatory: false
                }
            ]
        }, {
            id: 'Spin_S10',
            className: 'Spin_S10',
            questions: [
                {
                    id: 'Spin_Q10',
                    mandatory: false
                }
            ]
        }, {
            id: 'Spin_S11',
            className: 'Spin_S11',
            questions: [
                {
                    id: 'Spin_Q11',
                    mandatory: false
                }
            ]
        },
        {
            id: 'Spin_S13',
            className: 'Spin_S13',
            questions: [
                {
                    id: 'Spin_Q14',
                    mandatory: false
                }, {
                    id: 'Spin_Q13',
                    mandatory: false
                }
            ]
        },
        {
            id: 'Spin_S12',
            className: 'Spin_S12',
            questions: [
                {
                    id: 'Spin_S12',
                    mandatory: false
                }
            ]
        }
    ],
    questions: [
        {
            id: 'Spin_Q1',
            IG: 'NumSpin',
            IT: 'Spin_Q1',
            skipIT: 'Spin_Q1_SKP',
            title: 'SPIN_Q1_TITLE',
            text: 'SPIN_Q1_TEXT',
            className: 'Spin_Q1',
            widget: {
                id: 'NUMBER_SPINNER_W_1',
                type: 'NumberSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                labels: {
                    labelOne: 'MODAL_LABEL'
                },
                className: 'PILLS_NI',
                spinnerInputOptions: [
                    {
                        min: -20,
                        max: -10
                    }
                ]
            }
        },
        {
            id: 'Spin_Q2',
            IG: 'NumSpin',
            IT: 'Spin_Q2',
            skipIT: 'Spin_Q2_SKP',
            title: 'SPIN_Q2_TITLE',
            text: 'SPIN_Q2_TEXT',
            className: 'Spin_Q2',
            widget: {
                id: 'NUMBER_SPINNER_W_2',
                type: 'NumberSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                labels: {
                    labelOne: 'MODAL_LABEL'
                },
                embedded: false,
                showButtons: true,
                buttonLayout: 'horizontal',
                okButtonText: 'MODAL_OK_TEXT',
                className: 'PILLS_NI',
                spinnerInputOptions: [
                    {
                        min: 2000,
                        max: 2050
                    }
                ]
            }
        },
        {
            id: 'Spin_Q3',
            IG: 'NumSpin',
            IT: 'Spin_Q3',
            skipIT: 'Spin_Q3_SKP',
            title: 'SPIN_Q3_TITLE',
            text: 'SPIN_Q3_TEXT',
            className: 'Spin_Q3',
            widget: {
                id: 'NUMBER_SPINNER_W_3',
                type: 'NumberSpinner',
                label: '',
                modalTitle: 'MODAL_TITLE',
                labels: {
                    labelOne: 'MODAL_LABEL'
                },
                embedded: true,
                showButtons: false,
                buttonLayout: 'horizontal',
                okButtonText: 'MODAL_OK_TEXT',
                className: 'PILLS_NI',
                spinnerInputOptions: [
                    {
                        min: -20,
                        max: 10
                    }
                ]
            }
        },
        {
            id: 'Spin_Q4',
            IG: 'NumSpin',
            IT: 'Spin_Q4',
            skipIT: 'Spin_Q4_SKP',
            title: 'SPIN_Q4_TITLE',
            text: 'SPIN_Q4_TEXT',
            className: 'Spin_Q4',
            widget: {
                id: 'NUMBER_SPINNER_W_4',
                type: 'NumberSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                labels: {
                    labelOne: 'MODAL_LABEL'
                },

                /* embedded: false,
                showButtons: true,
                buttonLayout: 'vertical',*/
                okButtonText: 'MODAL_OK_TEXT',
                className: 'PILLS_NI',
                spinnerInputOptions: [
                    {
                        min: -8000,
                        max: 10000,
                        step: 500
                    }
                ]
            }
        },
        {
            id: 'Spin_Q5',
            IG: 'NumSpin',
            IT: 'Spin_Q5',
            skipIT: 'Spin_Q5_SKP',
            title: 'SPIN_Q5_TITLE',
            text: 'SPIN_Q5_TEXT',
            className: 'Spin_Q5',
            widget: {
                id: 'NUMBER_SPINNER_W_5',
                type: 'NumberSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                labels: {
                    labelOne: 'MODAL_LABEL'
                },
                embedded: false,
                showButtons: true,
                buttonLayout: 'vertical',
                okButtonText: 'MODAL_OK_TEXT',
                className: 'PILLS_NI',
                defaultVal: 10,
                spinnerInputOptions: [
                    {
                        min: -100,
                        max: 100
                    }
                ]
            }
        },
        {
            id: 'Spin_Q6',
            IG: 'NumSpin',
            IT: 'Spin_Q6',
            skipIT: 'Spin_Q6_SKP',
            title: 'SPIN_Q6_TITLE',
            text: 'SPIN_Q6_TEXT',
            className: 'Spin_Q6',
            widget: {
                id: 'NUMBER_SPINNER_W_6',
                type: 'NumberSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                min: 0.5,
                max: 10.49,
                modalTitle: 'MODAL_TITLE',
                templates: {
                    modal: 'DecimalSpinnerModal'
                },
                labels: {
                    labelOne: 'MODAL_LABEL',
                    labelTwo: 'MODAL_LABEL2'
                },
                okButtonText: 'MODAL_OK_TEXT',
                className: 'PILLS_NI',
                defaultVal: 3.34,
                spinnerInputOptions: [
                    {
                        min: 0,
                        max: 10
                    },
                    {
                        min: 0,
                        max: 0.99,
                        step: 0.01,
                        precision: 2
                    }
                ]
            }
        },
        {
            id: 'Spin_Q7',
            IG: 'NumSpin',
            IT: 'Spin_Q7',
            skipIT: 'Spin_Q7_SKP',
            title: 'SPIN_Q7_TITLE',
            text: 'SPIN_Q7_TEXT',
            className: 'Spin_Q7',
            widget: {
                id: 'NUMBER_SPINNER_W_7',
                type: 'NumberSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                className: 'PILLS_NI',
                defaultVal: '3.00',
                spinnerInputOptions: [
                    {
                        min: 0,
                        max: 10,
                        precision: 2
                    }
                ]
            }
        },
        {
            id: 'Spin_Q8',
            IG: 'NumSpin',
            IT: 'Spin_Q8',
            skipIT: 'Spin_Q8_SKP',
            title: 'SPIN_Q8_TITLE',
            text: 'SPIN_Q8_TEXT',
            className: 'Spin_Q8',
            widget: {
                id: 'NUMBER_SPINNER_W_8',
                type: 'NumberSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                okButtonText: 'MODAL_OK_TEXT',
                className: 'PILLS_NI',
                defaultVal: 'numberSpinnerTestDefault',
                spinnerInputOptions: 'numberSpinnerTestInputOptions'
            }
        },
        {
            id: 'Spin_Q9',
            IG: 'NumSpin',
            IT: 'Spin_Q9',
            skipIT: 'Spin_Q9_SKP',
            title: 'SPIN_Q9_TITLE',
            text: 'SPIN_Q9_TEXT',
            className: 'Spin_Q9',
            widget: {
                id: 'NUMBER_SPINNER_W_9',
                type: 'NumberSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                min: () => {
                    return Q()
                    .then(() => {
                        return 0.05 + 0.06;
                    });
                },
                max: 'Q9OverallMax',
                templates: {
                    modal: 'DecimalSpinnerModal'
                },
                okButtonText: 'MODAL_OK_TEXT',
                className: 'PILLS_NI',
                spinnerInputOptions: [
                    {
                        min: 0,
                        max: 1000
                    },
                    {
                        min: 0.00,
                        max: 0.99,
                        step: 0.05,
                        precision: 2
                    }
                ]
            }
        }, {
            id: 'Spin_Q10',
            IG: 'NumSpin',
            IT: 'Spin_Q10',
            skipIT: 'Spin_Q10_SKP',
            title: 'SPIN_Q10_TITLE',
            text: 'SPIN_Q10_TEXT',
            className: 'Spin_Q1',
            widget: {
                id: 'NUMBER_SPINNER_W_10',
                type: 'NumberSpinner',
                label: '',
                modalTitle: 'MODAL_TITLE',
                min: 0.11,
                max: 300.99,
                templates: {
                    modal: 'DecimalSpinnerModal'
                },
                embedded: true,
                showButtons: true,
                buttonLayout: 'horizontal',
                okButtonText: 'MODAL_OK_TEXT',
                className: 'PILLS_NI',
                spinnerInputOptions: [
                    {
                        min: 0,
                        max: 300
                    },
                    {
                        min: 0.20,
                        max: 0.95,
                        step: 0.01,
                        precision: 2
                    }
                ]
            }
        }, {
            id: 'Spin_Q11',
            IG: 'NumSpin',
            IT: 'Spin_Q11',
            skipIT: 'Spin_Q11_SKP',
            title: 'SPIN_Q11_TITLE',
            text: 'SPIN_Q11_TEXT',
            className: 'Spin_Q11',
            widget: {
                id: 'NUMBER_SPINNER_W_11',
                type: 'NumberSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                templates: {
                    modal: 'DecimalSpinnerModal'
                },
                okButtonText: 'MODAL_OK_TEXT',
                className: 'PILLS_NI',
                spinnerInputOptions: [
                    {
                        min: 0,
                        max: 150
                    },
                    {
                        min: 0.2,
                        max: 0.9,
                        step: 0.1,
                        precision: 1
                    }
                ]
            }
        },
        {
            id: 'Spin_Q13',
            IG: 'NumSpin',
            IT: 'Spin_Q13',
            skipIT: 'Spin_Q13_SKP',
            title: 'SPIN_Q13_TITLE',
            text: 'SPIN_Q13_TEXT',
            className: 'Spin_Q13',
            widget: {
                id: 'NUMBER_SPINNER_W_13',
                type: 'NumberSpinner',
                label: 'CLICK_TO_MAKE_SELECTION',
                modalTitle: 'MODAL_TITLE',
                labels: {
                    labelOne: 'MODAL_LABEL'
                },
                className: 'PILLS_NI',
                spinnerInputOptions: [
                    {
                        min: -20,
                        max: 10
                    }
                ]
            }
        },
        {
            id: 'Spin_Q14',
            IG: 'NumSpin',
            IT: 'Spin_Q14',
            skipIT: 'Spin_Q14_SKP',
            title: 'SPIN_Q14_TITLE',
            text: 'SPIN_Q14_TEXT',
            className: 'Spin_Q14',
            widget: {
                id: 'NUMBER_SPINNER_W_14',
                type: 'NumberSpinner',
                label: '',
                modalTitle: 'MODAL_TITLE',
                min: 0.00,
                max: 100.99,
                templates: {
                    modal: 'DecimalSpinnerModal'
                },
                embedded: true,
                showButtons: true,
                buttonLayout: 'vertical',
                defaultVal: '3.00',
                okButtonText: 'MODAL_OK_TEXT',
                className: 'PILLS_NI',
                spinnerInputOptions: [
                    {
                        min: 0,
                        max: 100
                    },
                    {
                        min: 0.00,
                        max: 0.99,
                        step: 0.01,
                        precision: 2
                    }
                ]
            }
        },
        {
            id: 'Spin_S12',
            IG: 'NumSpin',
            IT: 'Spin_Q12',
            title: 'SUMMARY_TITLE',
            text: ['SUMMARY_QUESTION'],
            className: 'NumSpin_TEST_S12_Q1'
        }
    ]
};
