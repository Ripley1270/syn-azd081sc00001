export default {
    questionnaires: [
        {
            id: 'P_VerticalVAS_Diary',
            SU: 'VVAS',
            displayName: 'DISPLAY_NAME',
            className: 'Custom_VVAS_Class',
            affidavit: undefined,
            accessRoles: ['subject'],
            screens: [
                'VVAS_TEST_S1',
                'VVAS_TEST_S2',
                'VVAS_TEST_S3',
                'VVAS_TEST_S4',
                'VVAS_TEST_S5',
                'VVAS_TEST_S6',
                'VVAS_TEST_S7',
                'VVAS_TEST_S8',
                'VVAS_TEST_S9',
                'VVAS_TEST_S10'
            ]
        }
    ],
    screens: [
        {
            id: 'VVAS_TEST_S1',
            className: 'VVAS_TEST_S1',
            questions: [
                {
                    id: 'VVAS_TEST_S1_Q1',
                    mandatory: false
                }
            ]
        },
        {
            id: 'VVAS_TEST_S2',
            className: 'VVAS_TEST_S2',
            questions: [
                {
                    id: 'VVAS_TEST_S2_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S3',
            className: 'VVAS_TEST_S3',
            questions: [
                {
                    id: 'VVAS_TEST_S3_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S4',
            className: 'VVAS_TEST_S4',
            questions: [
                {
                    id: 'VVAS_TEST_S4_Q1',
                    mandatory: false
                }
            ]
        },
        {
            id: 'VVAS_TEST_S5',
            className: 'VVAS_TEST_S5',
            questions: [
                {
                    id: 'VVAS_TEST_S5_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S6',
            className: 'VVAS_TEST_S6',
            questions: [
                {
                    id: 'VVAS_TEST_S6_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S7',
            className: 'VVAS_TEST_S7',
            questions: [
                {
                    id: 'VVAS_TEST_S7_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S8',
            className: 'VVAS_TEST_S8',
            questions: [
                {
                    id: 'VVAS_TEST_S8_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S9',
            className: 'VVAS_TEST_S9',
            questions: [
                {
                    id: 'VVAS_TEST_S9_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S10',
            className: 'VVAS_TEST_S10',
            questions: [
                {
                    id: 'VVAS_TEST_S10_Q1',
                    mandatory: false
                }
            ]
        }
    ],
    questions: [
        {
            // tests default anchor values, basic appearance
            id: 'VVAS_TEST_S1_Q1',
            IG: 'VVAS',
            IT: 'VVAS1',
            title: 'VVAS_Q1_TITLE',
            text: ['VVAS_Q1_TEXT'],
            className: 'VVAS_TEST_S1_Q1',
            widget: {
                id: 'VVAS_TEST_W1',
                type: 'VAS',
                displayAs: 'Vertical'
            }
        },
        {
            id: 'VVAS_TEST_S2_Q1',
            IG: 'VVAS',
            IT: 'VVAS2',
            title: 'VVAS_Q2_TITLE',
            text: ['VVAS_Q2_TEXT'],
            className: 'VVAS_TEST_S2_Q1',
            widget: {
                id: 'VVAS_TEST_W2',
                type: 'VAS',

                // defines which VAS to display.
                displayAs: 'Vertical',

                // should the cursor display be4 user input
                initialCursorDisplay: true,

                // configures the pointers
                pointer: {
                    isVisible: true,
                    displayInitially: true,
                    font: {
                        name: 'Times New Roman',
                        size: 30
                    }
                },

                // configures the min and max anchors on the VAS
                anchors: {
                    font: {
                        name: 'Times New Roman',
                        size: 30
                    },

                    // configures the min value anchor
                    min: {
                        text: 'NO_PAIN',
                        value: 0
                    },

                    // configures the max value anchor
                    max: {
                        text: 'EXTREME_PAIN',
                        value: 100
                    }
                },

                // configures the display of the selected value
                selectedValue: {
                    isVisible: true,
                    font: {
                        size: 38
                    }
                }
            }
        },
        {
            id: 'VVAS_TEST_S3_Q1',
            IG: 'VVAS',
            IT: 'VVAS3',
            title: 'VVAS_Q3_TITLE',
            text: ['VVAS_Q3_TEXT'],
            className: 'VVAS_TEST_S3_Q1',
            widget: {
                id: 'VVAS_TEST_W3',
                type: 'VAS',
                displayAs: 'Vertical',
                pointer: {
                    isVisible: false
                },
                selectedValue: {
                    isVisible: true,
                    location: 'dynamic'
                },
                anchors: {
                    min: {
                        text: 'NO_PAIN',
                        value: 5
                    },
                    max: {
                        text: 'EXTREME_PAIN',
                        value: 25
                    }
                }
            }
        },

        // Tests configurable anchors...everything ON
        {
            id: 'VVAS_TEST_S4_Q1',
            IG: 'VVAS',
            IT: 'VVAS4',
            skipIT: 'VVAS4_SKP',
            title: 'VVAS_Q4_TITLE',
            text: ['VVAS_Q4_TEXT'],
            className: 'VVAS_TEST_S4_Q1',
            widget: {
                id: 'VVAS_TEST_W4',
                type: 'VAS',
                displayAs: 'Vertical',
                pointer: {
                    isVisible: true
                },
                selectedValue: {
                    isVisible: true,
                    location: 'static'
                },
                anchors: {
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                }
            }
        },
        {
            id: 'VVAS_TEST_S5_Q1',
            IG: 'VVAS',
            IT: 'VVAS5',
            title: 'VVAS_Q5_TITLE',
            text: ['VVAS_Q5_TEXT'],
            className: 'VVAS_TEST_S5_Q1',
            widget: {
                id: 'VVAS_TEST_W5',
                type: 'VAS',
                displayAs: 'Vertical',
                initialCursorDisplay: true,
                pointer: {
                    isVisible: true,
                    color: '#F5E725',
                    font: {
                        name: 'Times New Roman',
                        size: 30
                    }
                },
                anchors: {
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN',
                        value: 10
                    }
                },
                selectedValue: {
                    isVisible: true,
                    location: 'both'
                }
            }
        },

        // Tests font customization.  Anchor - Arial 30, Pointer Times New Roman 46
        {
            id: 'VVAS_TEST_S6_Q1',
            IG: 'VVAS',
            IT: 'VVAS6',
            title: 'VVAS_Q6_TITLE',
            text: ['VVAS_Q6_TEXT'],
            className: 'VVAS_TEST_S6_Q1',
            widget: {
                id: 'VVAS_TEST_W6',
                type: 'VAS',
                displayAs: 'Vertical',
                initialCursorDisplay: false,
                pointer: {
                    isVisible: true
                },
                anchors: {
                    font: {
                        name: 'Arial',
                        size: 30
                    },
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                },
                selectedValue: {
                    isVisible: true,
                    location: 'dynamic',
                    font: {
                        name: 'Times New Roman',
                        size: 46
                    }
                }
            }
        },

        // Tests pointer color.  Pointer color = #B8273A
        {
            id: 'VVAS_TEST_S7_Q1',
            IG: 'VVAS',
            IT: 'VVAS7',
            title: 'VVAS_Q7_TITLE',
            text: ['VVAS_Q7_TEXT'],
            className: 'VVAS_TEST_S7_Q1',
            widget: {
                id: 'VVAS_TEST_W7',
                type: 'VAS',
                displayAs: 'Vertical',
                selectedValue: {
                    isVisible: false
                },
                initialCursorDisplay: true,
                pointer: {
                    displayInitially: true,
                    isVisible: true,
                    color: '#B8273A'
                },
                anchors: {
                    swapMinMaxLocation: false,
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                }
            }
        },
        {
            id: 'VVAS_TEST_S8_Q1',
            IG: 'VVAS',
            IT: 'VVAS8',
            title: 'VVAS_Q8_TITLE',
            text: ['VVAS_Q8_TEXT'],
            className: 'VVAS_TEST_S8_Q1',
            widget: {
                id: 'VVAS_TEST_W8',
                type: 'VAS',
                displayAs: 'Vertical',
                posting: true,
                initialCursorDisplay: false,
                pointer: {
                    isVisible: true,
                    location: 'right'
                },

                // configures the min and max anchors on the VAS
                anchors: {
                    swapMinMaxLocation: true,
                    min: {
                        text: 'NO_PAIN'
                    },
                    max: {
                        text: 'EXTREME_PAIN'
                    }
                }
            }
        },
        {
            id: 'VVAS_TEST_S9_Q1',
            IG: 'VVAS',
            IT: 'VVAS9',
            title: 'VVAS_Q9_TITLE',
            text: ['VVAS_Q9_TEXT'],
            className: 'VVAS_TEST_S9_Q1',
            widget: {
                id: 'VVAS_TEST_W9',
                type: 'VAS',

                // defines which VAS to display.
                displayAs: 'Vertical',

                // should the cursor display before user input
                initialCursorDisplay: true,

                // configures the pointers
                pointer: {
                    isVisible: true,
                    displayInitially: false,
                    location: 'right',
                    font: {
                        name: 'Times New Roman',
                        size: 45
                    }
                },

                // configures the min and max anchors on the VAS
                anchors: {
                    swapMinMaxLocation: true,

                    // configures the min value anchor
                    min: {
                        text: 'NO_PAIN',
                        value: 0
                    },

                    // configures the max value anchor
                    max: {
                        text: 'EXTREME_PAIN',
                        value: 100
                    }
                },

                // configures the display of the selected value
                selectedValue: {
                    isVisible: true,
                    location: 'dynamic'
                }
            }
        },
        {
            id: 'VVAS_TEST_S10_Q1',
            IG: 'VAS',
            IT: 'VAS10',
            title: 'SUMMARY_TITLE',
            text: ['SUMMARY_QUESTION'],
            className: 'HVAS_TEST_S12_Q1'
        }
    ]
};
