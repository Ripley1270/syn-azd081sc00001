/* // CORE FILES
import RestartManager from 'core/classes/RestartManager';
import StudyDBUpgrade from './db-upgrade';
// END CORE FILES */
import { MessageRepo } from 'core/Notify';

// Add rules, actions, and expressions here...
export default (rules) => {
    rules.remove('trackIGRs');

    /*
     * This rule adds an array to track the nested IGR for LOOP3 diary.
     */
    rules.add({
        id: 'trackIGRs',
        trigger: 'QUESTIONNAIRE:Open/P_Loop3',
        expression (filter, resume) {
            LF.Data.Questionnaire.nestedIGRs = [0];
            resume(true);
        },
        actionData: [{
            trueAction: 'defaultAction'
        }]
    });

    /**
     * Example: Check if user attempts to start visit outside configured window.
     * Ruled is used for visits defined in "isVisitOutOfWindow" action and is NOT "hard stop"
     * but only displays message to the user.
     */
    rules.add({
        id: 'beforeStartVisitExample1',
        salience: 10,
        trigger: 'VISITGATEWAY:BeforeStartVisit',
        evaluate: 'isVisitOutOfWindow',
        resolve: [
            { action: (param) => {
                let { Dialog } = MessageRepo;

                LF.DynamicText.visitName = param.visit.get('name');

                return MessageRepo.display(Dialog && Dialog.START_VISIT_OUTSIDE_WINDOW)
                .then(() => {
                    // handle YES/OK
                }, () => {
                    return { preventDefault: true };
                });
            } }
        ],
        reject: [
        ]
    });

    /**
     * Example: Check if user attempts to start visit outside configured window.
     * Ruled is used for visits defined in "shouldVisitBeHardStopped" action and IS "hard stop".
     * User is not allowed to enter this visit.
     */
    rules.add({
        id: 'beforeStartVisitExample2',
        salience: 20,
        trigger: 'VISITGATEWAY:BeforeStartVisit',
        evaluate: 'shouldVisitBeHardStopped',
        resolve: [
            { action: (param) => {
                let { Dialog } = MessageRepo;

                LF.DynamicText.visitName = param.visit.get('name');

                return MessageRepo.display(Dialog && Dialog.START_VISIT_HARD_STOP)
                .then(() => {
                    return { preventDefault: true };
                });
            } }
        ],
        reject: [
        ]
    });

    /**
     * Example: Rule used to filter Visits collection before gateway render.
     * Filtering by phase:
     * a) if subject's phase is equal 30, show visits 13 and 14, put visit 15 to not available
     * b) if subject's phase is not equal 30, hide visits 13 and 14, show visit 15
     */
    rules.add({
        id: 'customVisitsRender1',
        salience: 10,
        trigger: 'VISITGATEWAY:BeforeRenderVisits',
        resolve: [{
            action: (obj) => {
                let subject = obj.subject;
                obj.collection.each((model) => {
                    let { visit, userVisit } = model.toJSON();
                    if (subject.get('phase') === 30) {
                        if (visit.id === 'visit13' || visit.id === 'visit14') {
                            model.set('action', LF.StudyDesign.visitAction.show);
                        } else if (visit.id === 'visit15') {
                            if (userVisit == null || userVisit.get('state') !== LF.VisitStates.SKIPPED) {
                                model.set('action', LF.StudyDesign.visitAction.notAvailable);
                            }
                        }
                    } else {
                        // eslint-disable-next-line no-lonely-if
                        if (visit.id === 'visit13' || visit.id === 'visit14') {
                            model.set('action', LF.StudyDesign.visitAction.hide);
                        } else if (visit.id === 'visit15') {
                            model.set('action', LF.StudyDesign.visitAction.show);
                        }
                    }
                });
            }
        }]
    });

    /**
     * Example: Rule used to filter Visits collection before gateway render.
     * Filtering by visit:
     * a) if visit 16 is not completed, show visit 16, put visit 17 to not available
     * b) if visit 16 is completed, hide visit 16 and show visit 17
     */
    rules.add({
        id: 'customVisitsRender2',
        salience: 20,
        trigger: 'VISITGATEWAY:BeforeRenderVisits',
        resolve: [{
            action: (obj) => {
                return LF.Collection.UserVisits.fetchCollection()
                .then((uVisits) => {
                    let subject = obj.subject,
                        completed = false;

                    let completedUserVisit = uVisits.find((uVisit) => {
                        return uVisit.get('subjectKrpt') === subject.get('krpt') &&
                            uVisit.get('visitId') === 'visit16' &&
                            uVisit.get('state') === LF.VisitStates.COMPLETED;
                    });

                    completed = completedUserVisit != null;

                    obj.collection.each((model) => {
                        let { visit, userVisit } = model.toJSON();

                        if (!completed) {
                            if (visit.id === 'visit16') {
                                model.set('action', LF.StudyDesign.visitAction.show);
                            } else if (visit.id === 'visit17') {
                                if (userVisit == null || userVisit.get('state') !== LF.VisitStates.SKIPPED) {
                                    model.set('action', LF.StudyDesign.visitAction.notAvailable);
                                }
                            }
                        } else {
                            // eslint-disable-next-line no-lonely-if
                            if (visit.id === 'visit16') {
                                model.set('action', LF.StudyDesign.visitAction.hide);
                            } else if (visit.id === 'visit17') {
                                model.set('action', LF.StudyDesign.visitAction.show);
                            }
                        }
                    });
                });
            }
        }]
    });

    /**
     * Example: Rule used to filter Visits collection before gateway render.
     * Filtering by visit and phase:
     * a) if visit 18 is not completed, show visit 18, put visit 19, 20, 21 to not available
     * b) if visit 18 is completed and subject's phase is 20, show visit 19, put visit 20, 21 to not available
     * c) if visit 18 is completed and subject's phase is not 20, hide visit 18 and 19, show visit 20 and 21.
     */
    rules.add({
        id: 'customVisitsRender3',
        trigger: 'VISITGATEWAY:BeforeRenderVisits',
        resolve: [{
            action: (obj) => {
                return LF.Collection.UserVisits.fetchCollection()
                .then((uVisits) => {
                    let subject = obj.subject,
                        completed = false;

                    let completedUserVisit = uVisits.find((uVisit) => {
                        return uVisit.get('subjectKrpt') === subject.get('krpt') &&
                        uVisit.get('visitId') === 'visit18' &&
                        uVisit.get('state') === LF.VisitStates.COMPLETED;
                    });

                    completed = completedUserVisit != null;

                    obj.collection.each((model) => {
                        let { visit, userVisit } = model.toJSON();

                        if (!completed) {
                            if (visit.id === 'visit18') {
                                model.set('action', LF.StudyDesign.visitAction.show);
                            } else if (visit.id === 'visit19' || visit.id === 'visit20' || visit.id === 'visit21') {
                                if (userVisit == null || userVisit.get('state') !== LF.VisitStates.SKIPPED) {
                                    model.set('action', LF.StudyDesign.visitAction.notAvailable);
                                }
                            }
                        } else if (subject.get('phase') === 20) {
                            if (visit.id === 'visit19') {
                                model.set('action', LF.StudyDesign.visitAction.show);
                            } else if (visit.id === 'visit20' || visit.id === 'visit21') {
                                if (userVisit == null || userVisit.get('state') !== LF.VisitStates.SKIPPED) {
                                    model.set('action', LF.StudyDesign.visitAction.notAvailable);
                                }
                            }
                        } else {
                            // eslint-disable-next-line no-lonely-if
                            if (visit.id === 'visit18' || visit.id === 'visit19') {
                                model.set('action', LF.StudyDesign.visitAction.hide);
                            } else if (visit.id === 'visit20' || visit.id === 'visit21') {
                                model.set('action', LF.StudyDesign.visitAction.show);
                            }
                        }
                    });
                });
            }
        }]
    });


    /*
     * Rule used to determine subject's phase dynamically.
     */
    rules.add({
        id: 'customPhaseDetermination',
        trigger: 'QUESTIONNAIRE:New_Patient/DETERMINE_PHASE',
        resolve: [{
            action () {
                // Determine phase dynamically
                // this.phase = 40;
            }
        }]
    });


    /* // CORE CODE
    // Remove rules from core, so we can re-define
    rules.remove('CheckStudyDatabaseUpgrade');
    rules.remove('SkipQuestions');
    rules.remove('AutoDial');

    // Checks study database upgrades
    rules.add({
        id: 'CheckStudyDatabaseUpgrade',
        trigger: 'INSTALL:ModelsInstalled',
        evaluate (filter, resume) {
            resume(StudyDBUpgrade && !_.isEmpty(StudyDBUpgrade));
        },
        resolve: [{
            action: 'displayMessage',
            data: 'PLEASE_WAIT'
        }, {
            action: 'databaseUpgradeCheck',
            data: {
                config: StudyDBUpgrade,
                versionName: 'DB_Study'
            }
        }, {
            action: 'removeMessage'
        }]
    });

    //  Example: Skipped question confirmation
    //  Displays a confirmation when attempting to skip a question
    rules.add({
        id: 'SkipQuestions',
        trigger: 'QUESTIONNAIRE:SkipQuestions',
        evaluate: { expression: 'confirm', input: { key: 'SKIP_QUESTION_CONFIRM' } },
        resolve: [{
            action () {
                return this.skip()
                    .then(() => this.next());
            }
        }, {
            action: 'preventDefault'
        }],
        reject: [{
            action: 'preventDefault'
        }]
    });

    // Example: AutoDial
    // Transmits Data to server when alarm with ScheduleId of AutoDial goes off
    rules.add({
        id: 'AutoDial',
        trigger: 'ALARM:Trigger/AutoDial',
        evaluate: ['AND', 'isOnline', 'isStudyOnline'],
        resolve: [{
            action: 'displayMessage',
            data: 'PLEASE_WAIT'
        }, {
            action: () => RestartManager.setTransmissionActive(true)
        }, {
            action: 'transmitAll'
        }, {
            action: 'subjectSync'
        }, {
            action: 'syncHistoricalData',
            data: {
                collection: 'LastDiaries',
                webServiceFunction: 'syncLastDiary',
                activation: false
            }
        }, {
            action: 'siteAccessCodeSync'
        }, {
            action: 'transmitLogs'
        }, {
            action: 'checkViewForRefresh'
        }, {
            action: 'terminationCheck'
        }, {
            action: 'recoverOrphanDiaries'
        }, {
            action: 'removeMessage'
        }, {
            action: 'silenceAlarm'
        }, {
            action: () => RestartManager.setTransmissionActive(false)
        }],
        reject: [{
            action: 'silenceAlarm'
        }]
    });
    // END CORE CODE */
};
