import { MessageRepo } from 'core/Notify';
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';

export default function setupStudyMessages () {
    MessageRepo.add({
        type: 'Dialog',
        key: 'STUDY_NO_ROLE_FOUND',
        message: MessageHelpers.notifyDialogCreator({
            header: 'ERROR_TITLE',
            message: 'NO_USER_OF_ROLE',
            namespace: 'P_Loop1',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'START_VISIT_OUTSIDE_WINDOW',
        message: MessageHelpers.confirmDialogCreator(
            {
                header: 'START_VISIT',
                message: 'START_VISIT_OUTSIDE_WINDOW_MSG',
                type: 'warning',
                ok: 'YES',
                cancel: 'NO'
            }
        )
    }, {
        type: 'Dialog',
        key: 'START_VISIT_HARD_STOP',
        message: MessageHelpers.notifyDialogCreator(
            {
                header: 'START_VISIT_OUTSIDE_WINDOW_TITLE',
                message: 'START_VISIT_HARD_STOP_MSG',
                type: 'warning'
            }
        )
    });
}

setupStudyMessages();
