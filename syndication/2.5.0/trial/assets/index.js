
/* // CORE FILES
import dbUpgrade from './db-upgrade';
import StudyController from './study-controller';
import studyRoutes from './study-routes';
import studyWebService from './study-web-service';
// END CORE FILES */

import studyRules from './study-rules';
import studyDesign from './study-design';
import loggingConfig from './logging-config';
import studyTemplates from './study-templates';
import studyMessages from './study-messages';

export default {
    /* // CORE FILES
     dbUpgrade, StudyController, studyRoutes, studyWebService,
     // END CORE FILES */

    studyRules, studyDesign, loggingConfig, studyTemplates, studyMessages };
