export default [

    // Styles the question text of the WELCOME Diary.
    {
        name: 'Question',
        namespace: 'Welcome',
        template: '<div class="center {{ label }}">{{text}}</div><br />'
    },

    // Styles the question text of the WELCOME Diary for ko-KR.
    {
        name: 'Question',
        namespace: 'Welcome',
        language: 'fr',
        locale: 'FR',
        template: '<div style="margin-top: -11px; border-bottom-right-radius: 15px; border-bottom-left-radius: 15px;" class="ui-bar ui-bar-b center {{ label }}">{{text}}</div><br />'
    },

    {
        name: 'ScreenItems',
        namespace: 'Branching',
        template: '<li><div>{{ text0 }}</div></li>'
    },

    // A custom, centered horizontal fieldset for the WELCOME Diary.
    {
        name: 'HorizontalFieldset',
        namespace: 'Welcome',
        template: '<fieldset data-container data-role="controlgroup" data-type="horizontal" ></fieldset>'
    },


    // A custom, mini RadioButton fieldset for the WELCOME Diary.
    {
        name: 'MiniRadioButtons',
        namespace: 'Welcome',
        template: '<div class="vertical-buttons vertical-buttons-mini" data-toggle="buttons" data-container><fieldset data-container data-role="controlgroup" data-mini="true" data-type="vertical" ></fieldset></div>'
    }, {
        name: 'ScreenItems',
        namespace: 'NRS',
        template: '<li><div class="ui-grid-b">' +
            '<div class="ui-block-a">{{ text0 }}</div>' +
            '<div class="ui-block-b">{{ text1 }}</div>' +
            '<div class="ui-block-b">{{ text2 }}</div>' +
            '</div></li>'
    }, {
        name: 'ScreenItems',
        namespace: 'LOOP1',
        template: '<li><div class="ui-grid-a">' +
            '<div class="ui-block-a">{{ text0 }}</div>' +
            '<div class="ui-block-b">{{ text1 }}</div>' +
            '</div></li>'
    }, {
        name: 'InstructionsAndButtons',
        namespace: 'AM1Plus',
        template: '<div id="{{ id }}_Items"></div>' +
            '<ul id="{{ id }}_Buttons" data-role="listview"></ul>'
    }, {
        name: 'InstructionItems',
        namespace: 'AM1Plus',
        template: '<div class="instructions-container">' +
            '<span>{{ text0 }}</span>' +
            '<div class="{{ text1 }}"></div>' +
            '<span>{{ text2 }}</span>' +
            '<div class="{{ text3 }}"></div>' +
            '<span>{{ text4 }}</span>' +
            '</div>'
    }, {
        name: 'AM1PlusDataReview',
        namespace: 'AM1Plus',
        template: '<div class="review-area-container">' +
            '<div id="{{ id }}_Items"></div>' +
            '</div>'
    }, {
        name: 'ScreenItems',
        namespace: 'AM1Plus',
        template: '<div class="screen-item">' +
            '<div class="item-header">Record Number {{ text0 }}</div>' +
            '<div class="item-value-label">TIME</div>' +
            '<div class="item-column">:</div>' +
            '<div class="item-value">{{ text1 }}</div>' +
            '<div class="item-value-label">BATTERY</div>' +
            '<div class="item-column">:</div>' +
            '<div class="item-value">{{ text2 }}%</div>' +
            '<div class="item-value-label">PEF</div>' +
            '<div class="item-column">:</div>' +
            '<div class="item-value">{{ text3 }}</div>' +
            '<div class="item-value-label">FEV1</div>' +
            '<div class="item-column">:</div>' +
            '<div class="item-value">{{ text4 }}</div>' +
            '</div>'
    }, {
        name: 'ScreenItems',
        namespace: 'P_BarcodeDiary',
        template: '<li><div class="ui-grid-b">' +
            '<div class="ui-block-a">{{ text0 }}</div>' +
            '<div class="ui-block-b">{{ text1 }}</div>' +
            '<div class="ui-block-c">{{ text2 }}</div>' +
            '</div></li>'
    }

];
