/**
 * Demo function.
 * @returns {Q.Promise<boolean>}
 */
export function notAvailable () {
    return Q(false);
}

LF.Visits.availabilityFunctions.notAvailable = notAvailable;
