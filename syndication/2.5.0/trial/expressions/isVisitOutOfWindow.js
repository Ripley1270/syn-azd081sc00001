import * as dateTimeUtil from 'core/DateTimeUtil';

/**
 * Checks if specific visit is within specific time window.
 * @param {Object} params - Parameters
 * @returns {Q.Promise<boolean>} returns a promise resolving with true value if the selected visit is out of the defined window. User is still allowed to enter selected visit
 */
export function isVisitOutOfWindow (params) {
    let result = false,
        subject = params.subject,
        userVisit = params.userVisit,
        baseDate,
        currentDateMs = new Date().getTime(),
        isOutOfWindow = (baseDat, windowStartAfterBaseDays, windowEndAfterBaseDays) => {
            let dayInMs = 24 * 60 * 60 * 1000,
                windowStartMs = baseDat + (windowStartAfterBaseDays * dayInMs),
                windowEndMs = baseDat + (windowEndAfterBaseDays * dayInMs);

            if (!(windowStartMs < currentDateMs && windowEndMs > currentDateMs)) {
                LF.DynamicText.startDate = dateTimeUtil.getLocalizedDate(new Date(windowStartMs), { includeTime: false, useShortFormat: true });
                LF.DynamicText.endDate = dateTimeUtil.getLocalizedDate(new Date(windowEndMs), { includeTime: false, useShortFormat: true });
                return true;
            }
            return false;
        };
    if (subject && (!userVisit || (userVisit.get('state') === LF.VisitStates.AVAILABLE))) {
        if (this.selected.get('id') === 'visit22') {
            baseDate = new Date(subject.get('enrollmentDate')).getTime();
            result = isOutOfWindow(baseDate, 2, 4);
        } else if (this.selected.get('id') === 'visit25') {
            baseDate = new Date(subject.get('enrollmentDate')).getTime();
            result = isOutOfWindow(baseDate, 5, 10);
        }
    }
    return Q(result);
}

ELF.expression('isVisitOutOfWindow', isVisitOutOfWindow);
