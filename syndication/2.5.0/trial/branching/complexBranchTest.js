/**
 * Used for testing US7363
 * @param {Object} branchObj - The branching configuration
 * @returns {Q.Promise}
 */
LF.Branching.branchFunctions.cbt_logicForScreenD = () => new Q.Promise((resolve) => {
    let IT = 'CBT_Q4',
        answers,
        answer,
        retObj;

    answers = LF.Data.Questionnaire.queryAnswersByIT(IT);
    answer = answers[0];

    switch (answer.get('response')) {
        case 'A':
            retObj = {
                branchTo: 'CBT_SCREEN_A',
                clearBranchedResponses: false
            };
            break;
        case 'B':
            retObj = {
                branchTo: 'CBT_SCREEN_A',
                clearBranchedResponses: true
            };
            break;
        case 'F':
            retObj = {
                branchTo: 'CBT_SCREEN_F',
                clearBranchedResponses: false
            };
            break;
        default:
            retObj = false;
            break;
    }

    resolve(retObj);
});
