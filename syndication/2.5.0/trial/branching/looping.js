LF.Branching.branchFunctions.selectedMotrin = (branchObj, questionnaireView) => new Q.Promise((resolve) => {
    let IG = 'Loop1B',
        IGR = questionnaireView.getCurrentIGR(IG),
        answer = questionnaireView.queryAnswersByQuestionIDAndIGR('LOOP1_Q3', IGR);

    if (answer[0].get('response') === '4') {
        resolve(true);
    } else {
        resolve(false);
    }
});

LF.Branching.branchFunctions.selectedOxycontin = (branchObj, questionnaireView) => new Q.Promise((resolve) => {
    let IG = 'Loop1B',
        IGR = questionnaireView.getCurrentIGR(IG),
        answer = questionnaireView.queryAnswersByQuestionIDAndIGR('LOOP1_Q3', IGR);

    if (answer[0].get('response') === '5') {
        resolve(true);
    } else {
        resolve(false);
    }
});

/**
 * This always branch after doing necessary steps for Loop1 Diary.
 * @returns {Q.Promise<boolean>}
 */
LF.Branching.branchFunctions.toReviewScreenLoop1 = () => new Q.Promise((resolve) => {
    let currentIGR,
        answersByIGR,
        IG = 'Loop1B';

    // If the editing was happening, then this is the place where edited answers are
    // commited and saved into Answers collection
    if (LF.Data.Questionnaire.editing) {
        // Remove the original version
        LF.Data.Questionnaire.removeLoopEvent(IG, LF.Data.Questionnaire.editing);

        // change the edited version to original verison
        LF.Data.Questionnaire.changeIGR(IG, -1, LF.Data.Questionnaire.editing);

        // sets the flag to null which indicates the editing has been completed
        LF.Data.Questionnaire.editing = null;
    }


    if (LF.Data.Questionnaire.inserting) {
        // frees up the IGR we're inserting to and reorders the others to make space
        LF.Data.Questionnaire.makeIGRAvailable(IG, LF.Data.Questionnaire.targetInsertionIndex);

        // change the IGR from -1 (which it was initialized to for insertion) to the target that we are
        // inserting to
        LF.Data.Questionnaire.changeIGR(IG, -1, LF.Data.Questionnaire.targetInsertionIndex);
        LF.Data.Questionnaire.inserting = false;
    }

    // Update the IGR incase navigating to review screen from edit mode.
    LF.Data.Questionnaire.updateIGR(IG);

    // clears the stack before review screen is put on the stack.
    LF.Data.Questionnaire.clearLoopScreenFromStack(IG);

    // get the surrent IGR
    currentIGR = LF.Data.Questionnaire.getCurrentIGR(IG);

    // gets all answers for the current IGR
    answersByIGR = LF.Data.Questionnaire.queryAnswersByIGR(currentIGR);
    if (answersByIGR.length) {
        // increments if the current IGR is already completed, so next available IGR becomes current
        LF.Data.Questionnaire.incrementIGR(IG);
    }
    resolve(true);
});

/**
 * This always branch to Medication Review Screen after doing necessary steps.
 * @returns {Q.Promise<boolean>}
 */
LF.Branching.branchFunctions.toMedsReviewScreenLoop2 = () => Q.Promise((resolve) => {
    let currentIGR,
        answersByIGR,
        IG = 'Loop2B';

    // Update the IGR incase navigating to review screen from edit mode.
    LF.Data.Questionnaire.updateIGR(IG);

    // clears the stack before review screen is put on the stack.
    LF.Utilities.clearLoopScreenFromStack(IG);

    // get the surrent IGR
    currentIGR = LF.Data.Questionnaire.getCurrentIGR(IG);

    // gets all answers for the current IGR
    answersByIGR = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, currentIGR);
    if (answersByIGR.length) {
        // increments if the current IGR is already completed, so next available IGR becomes current
        LF.Data.Questionnaire.incrementIGR(IG);
    }
    resolve(true);
});

/**
 * This always branch to Food Review Screen after doing necessary steps.
 * @returns {Q.Promise<boolean>}
 */
LF.Branching.branchFunctions.toFoodReviewScreenLoop2 = () => Q.Promise((resolve) => {
    let IG = 'Loop2A';

    LF.Utilities.clearLoopScreenFromStack(IG);
    resolve(true);
});

/**
 * This always branch but after inserting an IT for Answers collection.
 * @returns {Q.Promise<Boolean>}
 */
LF.Branching.branchFunctions.insertNumMedsIT = () => Q.Promise((resolve) => {
    LF.Data.Questionnaire.addIT({
        question_id: 'LOOP2_Q6',
        questionnaire_id: 'Loop2',
        IG: 'Loop2C',
        IGR: 0,
        IT: 'NumMeds',
        response: LF.Data.Questionnaire.getIGR('Loop2B') - 1
    });
    resolve(true);
});

/**
 * This always branch after doing necessary steps for inner loop of Loop3 Diary.
 * @returns {Q.Promise<boolean>}
 */
LF.Branching.branchFunctions.toMedsReviewScreenLoop3 = () => new Q.Promise((resolve) => {
    let currentIGR,
        answersByIGR,
        IG = 'Loop3B',
        parentIG = 'Loop3A',

        // gets the current outer loop IGR
        currentParentIGR = LF.Data.Questionnaire.getCurrentIGR(parentIG);

    // Update the IGR incase navigating to review screen from edit mode.
    LF.Data.Questionnaire.updateIGR(IG);

    // clears the stack before review screen is put on the stack.
    LF.Utilities.clearLoopScreenFromStack(IG);

    // get the surrent IGR
    currentIGR = LF.Data.Questionnaire.getCurrentIGR(IG);

    // gets all answers for the current IGR
    answersByIGR = LF.Data.Questionnaire.queryAnswersByIGAndIGR(IG, currentIGR);
    if (answersByIGR.length) {
        // increments if the current IGR is already completed, so next available IGR becomes current
        // also increments the value in nestedIGRs to reflect one successful add
        LF.Data.Questionnaire.nestedIGRs[currentParentIGR - 1]++;
        LF.Data.Questionnaire.incrementIGR(IG);
    }
    resolve(true);
});

/**
 * Evaluate response from the user and determine if any previously added meds
 * as nested loop events needs to be removed. In case user chages to NO from previously
 * selected YES. In this case we remove all inner loop event.
 * @param {Object} branchObj - extended branch object from the configuration
 * @returns {Q.Promise<boolean>}
 */
LF.Branching.branchFunctions.toSympReviewScreenLoop3 = branchObj => new Q.Promise((resolve) => {
    let i,
        IG = 'Loop3A',
        innerIG = 'Loop3B',

        // gets the current loop IGR
        IGR = LF.Data.Questionnaire.getCurrentIGR(IG),

        // gets the answer for Question 3 for current IGR
        answer = LF.Data.Questionnaire.queryAnswersByQuestionIDAndIGR('LOOP3_Q3', IGR),
        startingIGR = 1;

    if (branchObj.branchFrom === 'LOOP3_S_3' && answer[0] && answer[0].get('response') === '0') {
        // If Question 3 has response yes, do not navigate to Symptoms review screen but instead
        // enter inner loop to enter meds for the symptom.
        resolve(false);
    } else if (branchObj.branchFrom === 'LOOP3_S_3' && answer[0] && answer[0].get('response') === '1') {
        // If Question 3 has response no, then navigate to Symptoms review screen. But before make sure
        // if any inner loop event are added previously which needs to be removed.

        // looks for any meds added for this IGR
        if (LF.Data.Questionnaire.nestedIGRs[IGR - 1] > 0) {
            // get the staring inner IGR for current outer IGR
            for (i = 0; i < IGR - 1; i++) {
                startingIGR += LF.Data.Questionnaire.nestedIGRs[i];
            }

            // Iterate and remove all meds added for Loop3 diary for current outer IGR
            for (i = 0; i < LF.Data.Questionnaire.nestedIGRs[IGR - 1]; i++) {
                // following the same patter of removing a loop event
                LF.Data.Questionnaire.removeLoopEvent(innerIG, startingIGR);
                LF.Data.Questionnaire.decrementIGR(innerIG);
                LF.Data.Questionnaire.orderIGR(innerIG, startingIGR);
            }

            // sets the array item to 0 to represents no meds entered
            LF.Data.Questionnaire.nestedIGRs[IGR - 1] = 0;
        }

        // Update the IGR incase navigating to review screen from edit mode.
        LF.Data.Questionnaire.updateIGR(IG);

        // clears the stack before review screen is put on the stack.
        LF.Utilities.clearLoopScreenFromStack(IG);
        resolve(true);
    } else if (branchObj.branchFrom === 'LOOP3_S_4') {
        // same branching function is used when navigating from inner loop review screen

        // Update the IGR incase navigating to review screen from edit mode.
        LF.Data.Questionnaire.updateIGR(IG);

        // clears the stack before review screen is put on the stack.
        LF.Utilities.clearLoopScreenFromStack(IG);
        resolve(true);
    } else {
        resolve(false);
    }
});
