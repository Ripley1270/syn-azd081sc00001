/* // CORE FILES
import 'trial/alarm';
import 'trial/schedule';
// END CORE FILES */

import assets from 'trial/assets';
import 'trial/branching';
import 'trial/dynamicText';
import 'trial/widgets';
import 'trial/visits';
import 'trial/expressions';

export default { assets };

