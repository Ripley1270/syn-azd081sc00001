(function () {
    var createTable = function (data, name, schema) {
        var columnName, i, value,
            container = $(document.createElement('div')).addClass('demo'),
            header = $(document.createElement('h2')).html(name),
            table = $(document.createElement('table')).css('width', '100%'),
            tableHead = $(document.createElement('thead')),
            tableBody = $(document.createElement('tbody')),
            row = $(document.createElement('tr')),
            offset = 'even';

        // Create a table header
        header.html(name);

        for (columnName in schema) {
            row.append('<th id=' + schema[columnName] + '>' + schema[columnName] + '</th>');
        }

        tableHead.append(row);

        for (i = 0; i < data.length; i += 1) {
            offset = (offset === 'odd') ? 'even' : 'odd';
            row = $(document.createElement('tr')).addClass(offset);

            for (columnName in schema) {
                row.append('<td>' + unescape(data[i][schema[columnName]]) + '</td>');
            }
            tableBody.append(row);
        }

        table.append(tableHead, tableBody);
        container.append(header, table);
        $('#container').html(container).fadeIn('slow');
        table.dataTable({
            'bJQueryUI' : true
        });
    };

    $(document).ready(function () {
        $('form').submit(function (e) {
            var table,
                textArea = $('#collection_result'),
                data = textArea.val();

            e.preventDefault();

            data = JSON.parse(data);
            schema = data.pop();
            table = data.pop();
            textArea.val('');
            createTable(data, table, schema);
        });
    });
}());