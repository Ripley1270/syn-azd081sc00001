
NLS Validation Tool:


Description:
This tool can be used for products like Logpad, Sitepad, etc. to check whether the keys
present in the reference file (usually the ‘coreStrings.json’ file for en-US) and all the other
languages files have the same keys under the ‘resources’ key.

The output of this tool is a '.json' file named as per the user’s requirement..

The resultant output file contains JSON data giving structured information when each
language is compared with the reference language (usually ‘en-US’). For each language it
specifies the keys present in the reference language file and not in the other languages and
vice-versa.



Language Platform: Python 3 (or Anaconda version of Python 3)
Anaconda Python 3.6: https://repo.continuum.io/archive/Anaconda3-4.4.0-Windows-x86_64.exe

Note: If you don’t have the Anaconda version of Python, you might need to use the PIP installer to install
all the package dependencies.



Dependency packages:
1) glob
2) os
3) codecs
4) json
5) sys



Running the tool (using command prompt):

Open the Command prompt utility and get into the directory where the tool file (nlsValidator.py’)
resides and refer the below code template to run the tool.

python nlsValidator.py Arg1 Arg2 [Arg3]

Arg1: 'nls' Directory of the product
Arg2: Output File path where you want the resultant file to be created (should be ‘.json’) .
Arg3: (Optional) Path of the reference coreStrings.json file if the reference file doesn’t reside in
the nls-logpad or nls-sitepad folder.



Example:
python nlsValidator.py C:\Syndication\app\sitepad\nls C:\Desktop\logpadOutput.json


Note : Using 'space(s)' in any directory names could give rise to runtime errors.