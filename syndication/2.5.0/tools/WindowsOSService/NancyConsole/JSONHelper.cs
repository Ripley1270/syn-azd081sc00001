﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NancyConsole
{
    public class JSONHelper<TSomeType>
    {
        private TSomeType obj;

        public JSONHelper(TSomeType objectToSerialize)
        {
            obj = objectToSerialize;
        }

        public JSONHelper()
        {

        }

        public string ToJSON()
        {
            StringBuilder jsonResult = new StringBuilder();
            Nancy.Json.JavaScriptSerializer serializer = new Nancy.Json.JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;
            return serializer.Serialize(obj);
        }

        public TSomeType FromJSON(string input)
        {
            Nancy.Json.JavaScriptSerializer serializer = new Nancy.Json.JavaScriptSerializer();
            return serializer.Deserialize<TSomeType>(input);
        }
    }
}
