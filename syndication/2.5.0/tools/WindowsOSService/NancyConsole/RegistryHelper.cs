﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace NancyConsole
{
    class RegistryHelper
    {
        public static List<string>  getSitePadAppIds() {
            List<string> result = new List<string>();

            Microsoft.Win32.RegistryKey OurKey = Microsoft.Win32.Registry.CurrentUser;
            OurKey = OurKey.OpenSubKey(@"SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Mappings", true);
    
            if (OurKey != null) { 
                foreach (string Keyname in OurKey.GetSubKeyNames())
                {
                    Microsoft.Win32.RegistryKey key = OurKey.OpenSubKey(Keyname);

                    if ((key.GetValue("DisplayName") != null) && (key.GetValue("DisplayName").ToString().StartsWith("SitePad") || key.GetValue("DisplayName").ToString().Contains("ERT")))
                    {                      
                        string[] keyParts = key.Name.ToString().Split('\\');                        
                        result.Add(keyParts[keyParts.Length - 1]);
                    }
                }
            }

            return result;
        }

        /**
         * Goes through all users registry entries and enables the loopback for all SitePad applications.
         * Service is run as local system and if run for debugging, needs to be admin mode or catch security exception.         
         */
        public static List<string> getSitePadAppIdsEx()
        {
            List<string> result = new List<string>();
            Microsoft.Win32.RegistryKey keyUsers = Microsoft.Win32.Registry.Users;
			Regex regex = new Regex(@"^com\.ert\..*");


            if (keyUsers != null)
            {
                foreach (string userKey in keyUsers.GetSubKeyNames())
                {
                    if (userKey.StartsWith("S-1-5-21"))//user key
                    {
                        Microsoft.Win32.RegistryKey OurKey = keyUsers.OpenSubKey(userKey + "\\SOFTWARE\\Classes\\Local Settings\\Software\\Microsoft\\Windows\\CurrentVersion\\AppContainer\\Mappings", true);

                        if (OurKey != null)
                        {
                            foreach (string Keyname in OurKey.GetSubKeyNames())
                            {
                                Microsoft.Win32.RegistryKey key = OurKey.OpenSubKey(Keyname);

                                if ((key.GetValue("Moniker") != null) && regex.Match(key.GetValue("Moniker").ToString()).Success)
                                {
                                    string[] keyParts = key.Name.ToString().Split('\\');
                                    result.Add(keyParts[keyParts.Length - 1]);
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }

    }
}
