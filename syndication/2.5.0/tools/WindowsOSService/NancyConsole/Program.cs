﻿using Nancy.Hosting.Self;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/***
 * Nancy self hosting web server application!
 * http://volkanpaksoy.com/archive/2015/11/11/building-a-simple-http-server-with-nancy/
 **/
namespace NancyConsole
{
    public class Program 
    {
        private string _url = "http://localhost";
        private int _port = 12345;
        private NancyHost _nancy;

        public Program()
        {
            var configuration = new HostConfiguration() { UrlReservations = new UrlReservations() { CreateAutomatically = true } };

        

            var uri = new Uri($"{_url}:{_port}/");
            // _nancy = new NancyHost(uri);
            _nancy = new NancyHost(configuration, uri);
        }

        public void Start()
        {
            _nancy.Start();
            Console.WriteLine($"Started listennig port {_port}");            
        }

        public void Stop() {
            _nancy.Stop();
        }

        static void Main(string[] args)
        {
            var p = new Program();
            p.Start();
        }
    }
}
