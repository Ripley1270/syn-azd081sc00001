﻿using System.Diagnostics;
using invivodata.Interop.WAMPC.Utilities;

namespace invivodata.Interop.WAMPC
{
    /// <summary>
    /// This class provides access to local database to store/manage WAMPC specific data.
    /// </summary>
    public class WAMPCDataAccess
    {
        #region ctor
        /// <summary>
        /// ctor -- initializes local objects.
        /// </summary>
        /// <param name="localDatabaseService">local data service</param>
        public WAMPCDataAccess()
        {
        }

        #endregion

        #region Pairing Related Data Access
        /// <summary>
        /// Gets/sets WAMPC's serial number.
        /// If none exists, it will return a null string.
        /// </summary>
        public string SerialNumber
        {
            get
            {
				//FIXME: Serial number stored as a persistent variable in the core. Removed SITEpro core dependency but should fix for Syndication.
				return string.Empty;
            }
            set
            {
				//FIXME: Serial number stored as a persistent variable in the core. Removed SITEpro core dependency but should fix for Syndication.
            }
        }

        /// <summary>
        /// Gets/sets WAMPC's pairing code.
        /// If none exists, it will return a null string.
        /// </summary>
        public string PairingCode
        {
            get
            {
                //FIXME: Pairing code stored as a persistent variable in the core. Removed SITEpro core dependency but should fix for Syndication.
				return string.Empty;
            }
            set
            {
                //FIXME: Pairing code stored as a persistent variable in the core. Removed SITEpro core dependency but should fix for Syndication.
            }
        }

        /// <summary>
        /// Gets/sets AC frequency filter.
        /// If none exists, it will return -1 as undefined/unassigned.
        /// </summary>
        public int ACFilter
        {
            get
            {
                //FIXME: The AC filter was stored as a persistent variable in the core but this should work differently in Syndication.
                return 50;
            }
            set
            {
				//FIXME: The AC filter was stored as a persistent variable in the core but this should work differently in Syndication.
            }
        }
        #endregion

        #region Acquisition Job Record Related Data Access

        /// <summary>
        /// Returns true if job id exists in the database.
        /// </summary>
        /// <param name="jobID">job id</param>
        /// <returns>true if job id record exists; otherwise it returns false.</returns>
        public bool AcqJobRecordExists(int jobID)
        {
			//FIXME: Determine if the AcqJob already exists. Commented-out code shows the 'intent' of this function.
			return false;
            /*object value = DatastoreService.GetGlobalSetting(AcqJobKey(jobID));
            if (Variables.IsNotDefined(value))
            {
                return false;
            }
            else
            {
                return true;
            }*/
        }

        /// <summary>
        /// Reads acquisition job record from the local database.
        /// If the requested job does not exist, then it will return null.
        /// </summary>
        /// <param name="jobID">job id</param>
        /// <returns>read in acq job record, or null if it doesn't exist.</returns>
        public AcquisitionJobRecord GetAcqJobRecord(int jobID)
        {
			//FIXME: Get the AcqJob if it already exists which I'm sure works differently on Syndication. Commented-out code shows the 'intent' of this function.
			return null;
            /*object value = DatastoreService.GetGlobalSetting(AcqJobKey(jobID));
            if (Variables.IsNotDefined(value))
            {
                // job id doesn't exist.
                return null;
            }
            else
            {
                return JsonFormatter.JsonDeserialize<AcquisitionJobRecord>((string)value);
            }*/
        }

        /// <summary>
        /// Writes acquisition job record into the local database.
        /// </summary>
        /// <param name="jobRecord">job record to write</param>
        public void SetAcqJobRecord(AcquisitionJobRecord jobRecord)
        {
            Debug.Assert(jobRecord != null);

            string key = AcqJobKey(jobRecord.JobID);
            string value = JsonFormatter.JsonSerializer<AcquisitionJobRecord>(jobRecord);

			//FIXME: Store the AcqJob in the database which works differently in Syndication. Stored as a key/value pair in SITEpro.
        }

        /// <summary>
        /// Removes job record from the local database.
        /// </summary>
        /// <param name="jobID">job id</param>
        public void DeleteAcqJobRecord(int jobID)
        {
            //FIXME: Delete the AcqJob from the database which works differently in Syndication.
        }

        #region Helpers
        /// <summary>
        /// Key prefix.
        /// </summary>
        private const string ACQJOBKEY_PREFIX = "WAMJ";

        /// <summary>
        /// Takes the job id, then prepends it with const "WAMJ".
        /// </summary>
        /// <param name="jobID">job id</param>
        /// <returns>key used to access acquisition job in local database.</returns>
        private string AcqJobKey(int jobID)
        {
            return string.Format("{0}{1:D10}", ACQJOBKEY_PREFIX, jobID);
        }
        #endregion
        #endregion

        #region PDF Related Data Access
        #endregion
    }
}
