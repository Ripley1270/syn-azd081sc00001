﻿namespace invivodata.Interop.WAMPC.Communicator
{
    public enum WamDeviceState
    {
        Unknown = 16,
        Off = 33,
        NoUTK = 48,
        HasData = 65,
        Empty = 81,
        Ready = 97,
        OperationInProgress = 113,
    }
}
