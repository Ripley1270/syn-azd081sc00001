﻿using System;
using invivodata.Interop.WAMPC.Communicator;
using Mortara.ELIxCtrl.API;

namespace invivodata.Interop.WAMPC
{
    public class WampcDeviceInformation
    {
        public string Model;
        public string Serial;
        public string Version;
        //public BatteryLevel BatteryLevel;
        public WamBatteryLevel BattLevel;
        //public DeviceState DeviceState;
        public WamDeviceState DevState;
        public int? PairingCode;
        public int? AcquisitionId;
        public DateTime? AcquisitionTimeStamp;
        public DateTime? ClockResetTimeStamp;
        public AcquisitionState AcquisitionState;
    }
}
