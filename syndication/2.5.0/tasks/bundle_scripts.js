'use strict';

var _ = require('underscore');

module.exports = function (grunt) {
    grunt.registerMultiTask('bundle_scripts', 'Bundles content of <script> and <link> tags into single .js and .css files respectively.', function () {
        var options = this.options({ ignore: [] }),
            fileExists = function (filepath) {
                if (!grunt.file.exists(filepath)) {
                    grunt.log.warn('Source File "' + filepath + '" not found.');
                    return false;
                }

                return true;
            },
            ignore = function (filepath) {
                return _.some(options.ignore, function (toMatch) {
                    return filepath.match(toMatch) != null;
                });
            },
            readAssetFiles = function (htmlContent, fileType) {
                var findAssetsPaths = function () {
                        var regexHash = {
                                css: /<link.+href="(.*?)"/g,
                                js: /\<script\s+src="(.*?)"/g
                            },
                            pattern = regexHash[fileType];
                        
                        return htmlContent.match(pattern)
                            .map(function (htmlElem) {
                                pattern.lastIndex = 0;

                                return pattern.exec(htmlElem)[1];
                            });
                    },
                    readFile = function (filepath) {
                        var read = function (filepath) {
                            var modify = options.modify && options.modify[fileType],
                                noop = function (expr) {
                                    return expr;
                                };

                            return (modify || noop)(grunt.file.read(filepath));
                        };

                        filepath = filepath.replace('.', options.cwd);
                        
                        return fileExists(filepath) && read(filepath);
                    };

                return findAssetsPaths()
                    .filter(function (filepath) {
                        return !ignore(filepath);
                    })
                    .map(readFile)
                    .join('\n');
            },
            writeToIndex = function (src, dest) {
                grunt.file.write(dest, src);
                grunt.log.writeln('File "' + dest + '" updated.');
            };

        this.files.forEach(function (file) {
            var src,
                processSrcFileContent = function (htmlContent) {
                    return readAssetFiles(htmlContent, file.type);
                };

            src = file.src.filter(function (srcPath) {
                    return fileExists(srcPath);
                })
                .map(function (srcPath) {
                    return grunt.file.read(srcPath);
                })
                .map(processSrcFileContent)
                .join('\n');

            writeToIndex(src, file.dest);
        });
    });
};
