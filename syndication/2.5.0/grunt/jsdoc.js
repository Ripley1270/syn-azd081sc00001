module.exports = function (grunt) {
    return {
        dist: {
            options: {
                source: 'app',
                destination: 'doc'
            }
        },
        elf: {
            options: {
                configure: './jsdoc.elf.conf.json'
            }
        },
        'widget-json': {
            options: {
                configure: './jsdoc-json.widget.conf.json'
            }
        },
        widget: {
            options: {
                configure: './jsdoc.widget.conf.json'
            }
        },
        models: {
            options: {
                configure: './jsdoc.models.conf.json'
            }
        },
        core: {
            options: {
                source: 'app/core',
                destination: 'doc/core'
            }
        }
    };
};
