module.exports = function (grunt) {
    return {
        dist : {
            options: {
                source: 'app',
                destination: 'doc'
            }
        }, widget : {
            options: {
                source: 'app/core/widgets',
                destination: 'doc/widgets',
                plugins: [
                    {
                        name: './esdoc-plugin.js'
                    }]
            }
        }, core: {
            options: {
                source: 'app/core',
                destination: 'doc/core'
            }
        }

    };
};
