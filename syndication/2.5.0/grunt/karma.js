var opts = require('optimist').argv;

module.exports = function (grunt) {

    return {
        options: {
            // Works here, does not work in shared.conf.js
            // this is pretty ugly
            client: {
                captureConsole: !(opts.quiet || /cons/.test(opts.suppress))
            }
        },
        core : {
            configFile  : 'app/test/conf/core.conf.js'
        },
        logpad : {
            configFile  : 'app/test/conf/logpad.conf.js'
        },
        sitepad : {
            configFile  : 'app/test/conf/sitepad.conf.js'
        },
        widgets : {
            configFile  : 'app/test/conf/widgets.conf.js'
        },
        web : {
            configFile  : 'app/test/conf/web.conf.js'
        },
        'logpad-jenkins' : {
            configFile  : 'app/test/conf/logpad-jenkins.conf.js'
        }
    };
};
