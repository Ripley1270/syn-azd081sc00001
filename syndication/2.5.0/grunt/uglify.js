module.exports = {
    options: {
        mangle: false
    },
    web_scripts: {
        files: [{
            src: ['<%= target %>/web/scripts.js'],
            dest: '<%= target %>/web/scripts.min.js'
        }]
    }
};
