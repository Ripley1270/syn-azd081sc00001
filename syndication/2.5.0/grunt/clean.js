module.exports = {

    karma_html: ['karma_html/*/**'],

    junit: ['junit/**'],

    coverage: ['coverage/**'],

    target: ['<%= target %>/'],

    logpad: ['<%= target %>/logpad'],

    sitepad: ['<%= target %>/sitepad'],

    web: ['<%= target %>/web'],

    web_dev: [
        '<%= target %>/web/*',
        '!<%= target %>/web/index.html',
        '!<%= target %>/web/*.min.*',
        '!<%= target %>/web/media',
        '!<%= target %>/web/node'
    ],

    server: ['server/public/*'],

    doc: ['doc'],

    webhost: ['<%= host_staging %>'],

    // These plugins cause the ios build to fail.
    // this task will remove them until the problem can be sorted out.
    ios: [
        '<%= target %>/**/plugins/com.darktalker.cordova.screenshot'
    ],

    'unlock-code': ['<%= target %>/unlock-code']

};
