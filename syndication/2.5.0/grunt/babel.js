module.exports = {
    web: {
        options: {
            sourceMap: false,
            presets: [[
                'env', {
                    targets: { node: 'current' }
                }
            ]],
            plugins: ['add-module-exports']
        },
        files: [{
            src: ['trial/assets/study-design/environments.js'],
            dest: '<%= target %>/web/node/environments.js'
        }]
    }
};
