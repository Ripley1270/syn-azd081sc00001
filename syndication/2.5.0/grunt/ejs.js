module.exports = {
    logpad: {
        options: {
            version: '<%= pkg.version %>'
        },
        files: [{
            src: ['<%= logpad %>/index.ejs'],
            dest: '<%= target %>/logpad/www/index.html'
        }]
    },

    sitepad: {
        options: {
            version: '<%= pkg.version %>'
        },
        files: [{
            src: ['<%= sitepad %>/index.ejs'],
            dest: '<%= target %>/sitepad/www/index.html'
        }]
    },

    web: {
        options: {
            version: '<%= pkg.version %>'
        },
        files: [{
            src: ['<%= web %>/index.ejs'],
            dest: '<%= target %>/web/index.html'
        }, {
            src: ['<%= web %>/core-version.ejs'],
            dest: '<%= target %>/web/core-version.js'
        }]
    }
};
