/* eslint-disable */
// Set support for desired orienations in iOS .plist - workaround for this cordova bug: https://issues.apache.org/jira/browse/CB-9371
module.exports = function (ctx) {
    var platforms = ctx.opts.platforms;

    /**
     * Configures the appropriate orientation settings in the PList
     * @param {Object} plist The PList object
     */
    var configure = function (plist) {
        var iPhoneOrientations = [
            'UIInterfaceOrientationPortrait'
        ];
        var iPadOrientations = [
            'UIInterfaceOrientationPortrait'
        ];
        plist['UISupportedInterfaceOrientations'] = iPhoneOrientations;
        plist['UISupportedInterfaceOrientations~ipad'] = iPadOrientations;
    };
    
    platforms.forEach(function (p) {
        if (p === 'ios') {
            var fs = require('fs'),
                plist = require('plist'),
                xmlParser = new require('xml2js').Parser(),
                plistPath = '',
                configPath = 'config.xml';
            // Construct plist path, since the app name will change based on the study
            if (fs.existsSync(configPath)) {
                var configContent = fs.readFileSync(configPath);
                // Callback is synchronous.
                xmlParser.parseString(configContent, function (err, result) {
                    var name = result.widget.name;
                    plistPath = 'platforms/ios/' + name + '/' + name + '-Info.plist';
                });
            }

            // Change plist and write.
            if (fs.existsSync(plistPath)) {
                var pl = plist.parseFileSync(plistPath);
                configure(pl);
                fs.writeFileSync(plistPath, plist.build(pl).toString());
            }
        }
    });   
};