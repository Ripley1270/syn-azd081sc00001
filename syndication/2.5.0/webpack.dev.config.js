module.exports = (platforms = ['logpad', 'sitepad', 'web']) => {
    const path = require('path');
    const webpack = require('webpack');
    const _ = require('lodash');

    // This is added with each entry to provide hot reloading.
    const hotMiddlewareScript = 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true';

    const logpad = _.includes(platforms, 'logpad');
    const sitepad = _.includes(platforms, 'sitepad');
    const web = _.includes(platforms, 'web');

    const entry = {};

    /*
     * keys in the entry object are the browser path, values are the entry file + middleware script.
     * Webpack dev server builds the bundle in memory so it needs the browser path to tell when to fetch it.
     * We can do multiple bundles for each modality at once. use --{platform} to specify.
     * OR omitting args will by default use all modalities.
     */
    if (logpad) {
        entry['logpad/'] = [
            path.resolve(__dirname, 'app/logpad/index.js'),
            hotMiddlewareScript
        ];
    }

    if (sitepad) {
        entry['sitepad/'] = [
            path.resolve(__dirname, 'app/sitepad/index.js'),
            hotMiddlewareScript
        ];
    }

    if (web) {
        entry['web/'] = [
            path.resolve(__dirname, 'app/web/index.js'),
            hotMiddlewareScript
        ];
    }

    const plugins = [
        new webpack.HotModuleReplacementPlugin(),

        /*
        * If we have more than one bundle, we can put common code in a common bundle to increase rebundle speeds.
        *   If there is just one modality, still include common.bundle because it avoids a not found error.
        *   However, if one modality the common.bundle is mostly empty.
        * remove in production buld.
        */
        new webpack.optimize.CommonsChunkPlugin({
            names: 'common',
            filename: './common.bundle.js'
        })
    ];

    return {
        devtool: '#cheap-module-eval-source-map',
        entry,
        resolve: {
            // give path aliases to fix '../../../'.
            alias: {
                core: path.resolve(__dirname, 'app/core'),
                sitepad: path.resolve(__dirname, 'app/sitepad'),
                logpad: path.resolve(__dirname, 'app/logpad'),
                trainer: path.resolve(__dirname, 'app/trainer'),
                web: path.resolve(__dirname, 'app/web'),
                trial: path.resolve(__dirname, 'trial')
            },
            extensions: ['.js']
        },
        output: {
            filename: '[name]runtime.bundle.js',
            publicPath: '/',
            path: path.resolve(__dirname, '<%= target %>')
        },
        plugins,
        module: {
            rules: [{
                test: /\.js$/,
                exclude: /(node_modules|bower_components|libs)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        cacheDirectory: true
                    }
                }
            }]
        }
    };
};
