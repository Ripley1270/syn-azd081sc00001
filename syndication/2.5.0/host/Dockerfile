FROM ubuntu:14.04

RUN sudo apt-get update \
    && apt-get install -y wget \
    && apt-get install -y curl \
    && apt-get install -y unzip \
    && apt-get install -y tofrodos

RUN sudo ln -s /usr/bin/fromdos /usr/bin/dos2unix

RUN wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | sudo apt-key add -

# Update the package list and install RabbitMQ
RUN sudo apt-get update \
    && apt-get install -y erlang \
    && apt-get install -y rabbitmq-server

# Install ngrok (latest official stable from https://ngrok.com/download).
ADD https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip /ngrok.zip
RUN set -x \
    && unzip -o /ngrok.zip -d /bin \
    && rm -f /ngrok.zip

# Install NodeJS v8.x
RUN curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash - \
    && sudo apt-get install -y nodejs

# Copy over the startup script, and convert all line endings to unix from dos.
ADD docker/start-services /usr/local/bin
RUN dos2unix /usr/local/bin/start-services

# Copy over a default, encrytped credentials for connecting to MongoDB
RUN mkdir /usr/data
ADD docker/data /usr/data

# Install nodemon for developer debugging, and pm2 for production monitoring
RUN npm install -g nodemon \
    && npm install -g pm2

# Create app directory
WORKDIR /usr/src/app

CMD start-services && bash