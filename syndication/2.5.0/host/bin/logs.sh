#!/bin/bash -e

# Display all logs from the running host container.  Ensure you've executed startup.sh prior to this.
docker exec -i -t host pm2 logs --lines=${1:-"100"}