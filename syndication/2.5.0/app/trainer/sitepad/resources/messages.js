import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';

export default [{
    type: 'Dialog',
    key: 'TRAINER_EXIT_NOTIFICATION',
    message: MessageHelpers.confirmDialogCreator({
        message: 'TRAINER_EXIT_NOTIFICATION',
        type: 'warning'
    })
}];
