import { MessageRepo } from 'core/Notify';
import State from 'sitepad/classes/State';
export default [{
    id: 'GatewayTransmit',
    trigger: [
        'HOME:Transmit',
        'VISITGATEWAY:Transmit',
        'FORMGATEWAY:Transmit',
        'LOGIN:Transmit',
        'USERMANAGEMENT:Transmit'
    ],
    resolve: [
        { action: 'displayMessage', data: 'PLEASE_WAIT' },
        { action: () => Q.delay(1000) },
        { action: 'removeMessage' }
    ]
}, {
    id: 'QuestionnaireTransmit',
    trigger: 'QUESTIONNAIRE:Transmit',
    salience: 101,
    resolve: [{ action: () => Q.delay(1000) }]
}, {
    id: 'ExitSitepadTrainer',
    trigger: 'SITEPADTRAINER:Exit',
    evaluate: {
        expression: (filter, resume) => {
            const { Dialog } = MessageRepo;
            return MessageRepo.display(Dialog && Dialog.TRAINER_EXIT_NOTIFICATION)
            .then(resume)
            .catch(() => {
                resume(false);
            });
        }
    },
    resolve: [
        { action: 'uninstall', data: ['UserReport', 'UserVisit'] },
        { action: 'disableTrainer' }
    ]
}, {
    id: 'TrainerEditUserTransmit',
    trigger: [
        'QUESTIONNAIRE:Transmit/Edit_User',
        'QUESTIONNAIRE:Transmit/Activate_User',
        'QUESTIONNAIRE:Transmit/Deactivate_User'
    ],
    salience: 200,
    resolve: [{
        // In normal mode this banner is displayed after a successful transmission. In trainer we don't transmit
        // anything so we are adding the banner as a trainer level action
        action: 'displayBanner',
        data: 'USER_EDITED'
    }]
}, {
    id: 'TrainerNewUserTransmit',
    trigger: 'QUESTIONNAIRE:Transmit/New_User_Sitepad',
    salience: 200,
    resolve: [{ action: 'displayBanner', data: 'NEW_USER_CREATED' }]
}];
