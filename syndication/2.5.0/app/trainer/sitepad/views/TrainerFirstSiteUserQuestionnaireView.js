import COOL from 'core/COOL';
import FirstSiteUserQuestionnaireView from 'sitepad/views/FirstSiteUserQuestionnaireView';

/**
 * @function extendFirstSiteUserQuestionnaireView
 * Extends FirstSiteUserQuestionnaireView to modify the unlock code validation behavior
 */
export function extendFirstSiteUserQuestionnaireView () {
    /**
     * @class TrainerFirstSiteUserQuestionnaireView
     * A class that extends the sitepad FirstSiteUserQuestionnaireView view.
     */
    class TrainerFirstSiteUserQuestionnaireView extends COOL.getClass('FirstSiteUserQuestionnaireView', FirstSiteUserQuestionnaireView) {
        /**
         * @method validateUnlockCode
         * Compares the entered unlockCode to be a string of a 9 digit number
         * @param {string} unlockCode The unlock code user entered.
         * @returns {boolean}
         */
        validateUnlockCode (unlockCode) {
            let validator = /^\d{9}$/;

            return validator.test(unlockCode);
        }


        /*
         * Sets the callback handler for handling the unlock code validation.
         * @returns {Q.Promise<void>}
         */
        setUnlockCodeHandler () {
            this.disableButton(this.$('#nextItem'));
            this.isUnlockCodeValid = false;

            let unlockCodeWidget = $('#ADD_USER_W_8');
            unlockCodeWidget.on('input', () => {
                let unlockCodeAnswer = unlockCodeWidget.val();

                if (!unlockCodeAnswer) {
                    this.clearInputState(unlockCodeWidget);
                    this.isUnlockCodeValid = false;
                    this.disableButton(this.$('#nextItem'));
                } else {
                    let validator = /^\d{9}$/;
                    if (validator.test(unlockCodeAnswer)) {
                        this.inputSuccess(unlockCodeWidget);
                        this.isUnlockCodeValid = true;
                        this.enableButton(this.$('#nextItem'));
                    } else {
                        this.inputError(unlockCodeWidget);
                        this.isUnlockCodeValid = false;
                        this.disableButton(this.$('#nextItem'));
                    }
                }

                return Q();
            });
        }
    }

    COOL.add('FirstSiteUserQuestionnaireView', TrainerFirstSiteUserQuestionnaireView);
}
