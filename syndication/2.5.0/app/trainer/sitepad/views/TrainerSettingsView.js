import COOL from 'core/COOL';
import SettingsView from 'sitepad/views/SettingsView';

/**
 * @function extendSettingsView
 * Extends SettingsView to add exit trainer mode button
 */
export function extendSettingsView () {
    /**
     * @class TrainerSettingsView
     * A class that extends the sitepad SettingsView view.
     */
    class TrainerSettingsView extends COOL.getClass('SettingsView', SettingsView) {
        /**
         * @method render
         * Overides the settings template and render the view to the DOM
         * @returns {Q.Promise<void>}
         */
        render () {
            let exitTrainerHtml = $('#sitepad-exit-trainer-template').html(),
                settingsTemplate = $('<div>').append($('#settings-template').html()),
                buttonContainer = settingsTemplate.find('#button-container');

            if (!buttonContainer.find('#exit-trainer').length) {
                buttonContainer.append(exitTrainerHtml).html();
                $('#settings-template').html(settingsTemplate.html());
            }

            this.templateStrings.exitTrainer = 'TRAINER_EXIT';

            this.events['click #exit-trainer'] = 'exitTrainerMode';

            return super.render();
        }

        /**
         * @method exitTrainerMode
         * Handles the eit trainer mode button click event
         */
        exitTrainerMode () {
            ELF.trigger('SITEPADTRAINER:Exit', {}, this)
            .done();
        }
    }

    COOL.add('SettingsView', TrainerSettingsView);
}
