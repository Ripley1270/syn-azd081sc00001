/**
 * Created by cstakutis on 12/12/2015.
 *
 * This file is brought in from the trial/index.js
 *
 * This overrides various Syndication methods/classes to IMPLEMENT Trainer.
 */

// **********************************************************************************
//
// How does Syndication startup occur?
//
// - index.html (which is a generated file) loads:
//      - frameworks javascripts
//      - application.js (which defines LF...)
//      - runtimebundle.js  (generated, see below)
//      - few misc js
//      - lots of templates from the template file/dirs
//
// Runtimebundle has the bulk of all the class-code. It is generated by examining
// app/logpad/index.js (browserified).  Index.js loads/performs:
//      - Major classes, in groups, called (and announced) as 'phases'
//      - Then it imports logpad/classes/BasicStartup which is a class that sequences
//        the startup activities. This can be overriden by PDE's via COOL
//      - THEN it brings-in the entire trial tree of classes which may override some core functions.
//      - Then it starts the boot process by invoking BasicStartup.startup() to kick things off
//      (which might be PDE overriden but this very file here.)
// *********************************************************************************/
import Logger from 'core/Logger';
import Startup from 'logpad/classes/Startup';
import COOL from 'core/COOL';
import SetTimeZoneActivationView from 'core/views/SetTimeZoneActivationView';
import { checkInstall } from 'core/Helpers';
import WebService from 'core/classes/WebService';
import { Utilities, setServiceBase } from 'core/utilities';
import ToolboxView from 'logpad/views/ToolboxView';
import Trainer from './Trainer';
import TrainerData from './TrainerData';
import TimeTravel from 'core/TimeTravel';
import CodeEntryView from 'logpad/views/CodeEntryView';

const trainerURL = 'http://trainer';

let logger = new Logger('Trainer Startup');
logger.trace('Trainer Startup');

let toolboxOnce;

class Trainer_ToolboxView extends COOL.getClass('ToolboxView', ToolboxView) {
    render () {
        if (!Trainer.trainerMode()) {
            return super.render();
        }

        if (!toolboxOnce) {
            toolboxOnce = true;

            /** First, adjust the toolbox view and add the exit-trainer anchor **/
            if ($('#tempScratch').length === 0) {
                let div = document.createElement('div');
                div.id = 'tempScratch';
                document.body.appendChild(div);
            }
            let html = $('#toolbox-template').html();
            $('#tempScratch').html(html);
            let anchor =
                '<a id="exit-trainer" class="list-group-item">' +
                '<span class="navigate-right">' +
                '<span class="title-block"><p class="title">{{ exitTrainer }}</p></span>' +
                '<span class="glyphicon glyphicon-menu-right"></span>' +
                '</span>' +
                '</a>';

            $('#tempScratch a:last').after(anchor);
            $('#toolbox-template').html($('#tempScratch').html());
            $('#tempScratch').html('');
        }

        /** Next, alter the translate strings **/
        this.templateStrings.exitTrainer = 'TRAINER_EXIT';

        /** Alter the events...this might NOT work... **/
        this.events['click #exit-trainer'] = 'exitTrainerMode';

        return super.render();
    }

    /**
     * Clears all data and returns to study entry screen.
     * @param {Event} e Event data
     */
    exitTrainerMode (e) {
        this.undelegateEvents();
        Trainer.trainerMode(false)
        .catch(this.delegateEvents());
    }
}
COOL.add('ToolboxView', Trainer_ToolboxView);

/**
 * Let's implement Trainer...
 *
 * Here are the major steps we need to take:
 * - Wrap CodeEntryView so we can see if the user types-in Trainer
 * - Have CodeEntry see if we were partially activated and undo-trainer
 * - Startup to auto-detect trainer mode
 * - Webservices to fake the I/O
 * - Toolbox view to add exittrainer
 *
 * - the isOnline check/hack
 * - timetravel hack
 */
function setTrainer () {
    // Create a 'environment' that specifically has timeTravel enabled
    // This is important because there is core code that gets triggered
    // and forcibly set timeTravel based on the matching environment description.
    return Q()
    .then(() => {
        LF && LF.StudyDesign && LF.StudyDesign.environments && LF.StudyDesign.environments.push({
            id: 'anonymous',
            label: 'anonymous',
            modes: ['production'],
            url: trainerURL,
            studyDbName: 'anonymous',
            timeTravel: true
        });

        // FIXME: There is no point importing TrainerData module and passing it as is as a parameter. Instead import it on the Trainer class
        Trainer.setData(TrainerData);
        TimeTravel.displayTimeTravel(true);
        return Trainer.trainerMode(true);
    })
    .catch((err) => {
        logger.error('Error setting up the trainer mode', err);
    });
}

class Trainer_CodeEntryView extends COOL.getClass('CodeEntryView', CodeEntryView) {
    resolve () {
        // Detect the bizarre case that the user started with trainer, went down the setup
        // path for a bit, but then backed out.  Need to clean and start fresh.
        // Bear in mind, 'code_entry' view is called A lot of times during activation and
        // we can't find/fix them all, so we dont want to break them either! Trust me...
        return super.resolve()
        .then(() => {
            if (Trainer.trainerMode()) {
                return checkInstall()
                .then((count) => {
                    if (count) {
                        logger.warn('Trainer mode continuing...');
                        return setTrainer();
                    }

                    logger.warn('Detected trainer setting; must reset/uninstall');
                    return LF.Actions.uninstall({}).then(() => {
                        // Force the app to restart so all data/vars are clean too!
                        // Force a fresh-start on the world
                        this.exitingTrainer = true;
                        LF.Utilities.restartApplication();
                    });
                });
            }

            return Q();
        });
    }

    render () {
        if (this.exitingTrainer) {
            return Q();
        }

        return super.render();
    }

    /**
     * Sets the subject language.
     * @param {Object} res - The response from the getSubjectActive API call.
     */
    setSubjectLanguage (res) {
        // If it is trainer mode, do nothing. The language will be set in the next screen
        if (!Trainer.trainerMode()) {
            super.setSubjectLanguage(res);
        }
    }

    /**
     * Submits the form
     * @param {Event} e Event data.
     */
    submit (e) {
        let val = this.$txtStudy.val();

        if (val.search(/trainer/i) !== -1 && this.isValidForm('#txtCode')) {
            e.preventDefault();
            this.$txtStudy.val(trainerURL);

            setTrainer()
            .then(() => {
                LF.Data.code = this.$txtCode.val().trim();
                localStorage.setItem('activationCode', LF.Data.code);
                localStorage.setItem('krpt', TrainerData.Configs.defaults.krpt);
                setServiceBase(trainerURL);

                if (val.search(/setup/i) !== -1) {
                    localStorage.setItem('trainerType', 'setup');
                    this.navigate('trainer_setup');
                } else {
                    localStorage.removeItem('trainerType');
                    localStorage.setItem('activationFlag', true);
                    this.navigate('trainer_language_selection');
                }
            });

            return Q();
        }

        return super.submit(e);
    }
}

COOL.add('CodeEntryView', Trainer_CodeEntryView);

class Trainer_SetTimeZoneActivationView extends COOL.getClass('SetTimeZoneActivationView', SetTimeZoneActivationView) {
    /**
     * Navigates back to the language selection view.
     * @event e Event data
     * @param {Object} e Event object.
     * @returns {Q.Promise<void>}
     */
    back (e) {
        if (!Trainer.trainerMode()) {
            super.back(e);
            return Q();
        }

        e.preventDefault();
        if (localStorage.getItem('trainerType') === 'setup') {
            this.navigate('trainer_setup');
        } else {
            this.navigate('trainer_language_selection');
        }

        return Q();
    }
}

COOL.add('SetTimeZoneActivationView', Trainer_SetTimeZoneActivationView);

class Trainer_WebService extends COOL.getClass('WebService', WebService) {
    transmit (ajaxConfig, data, onFake = {}) {
        if (!Trainer.trainerMode()) {
            return super.transmit(ajaxConfig, data);
        }

        let deferred = Q.defer();
        let storedProc = data && data.StoredProc ? ` with stored procedure "${data.StoredProc}"` : '',
            syncLevel = data && data.level ? ` for syncLevel "${data.level}"` : '';

        logger.operational(`Attempt to transmit ${ajaxConfig.type} ${ajaxConfig.uri}${storedProc}${syncLevel} started.`, {
            storedProc: data && data.StoredProc,
            syncLevel: data && data.level,
            method: ajaxConfig.type,
            uri: ajaxConfig.uri
        }, 'CommStart');

        deferred.resolve({
            res: this.dataToBeReturned,
            syncID: 5,
            isSubjectActive: 1,
            isDuplicate: false
        });

        this.dataToBeReturned = undefined;

        return deferred.promise;
    }

    setDataToBeReturned (methodName) {
        if (Trainer.trainerMode()) {
            this.dataToBeReturned = Trainer.trainerData(methodName);
        }
    }

    getSiteAccessCode (...args) {
        this.setDataToBeReturned('getSiteAccessCode');
        return super.getSiteAccessCode(...args);
    }

    registerDevice (...args) {
        this.setDataToBeReturned('registerDevice');
        return super.registerDevice(...args);
    }

    getSites (...args) {
        this.setDataToBeReturned('getSites');
        return super.getSites(...args);
    }

    sendSubjectAssignment (...args) {
        this.setDataToBeReturned('sendSubjectAssignment');
        return super.sendSubjectAssignment(...args);
    }

    getSubjectActive (...args) {
        this.setDataToBeReturned('getSubjectActive');
        return super.getSubjectActive(...args);
    }

    getSubjectQuestion (...args) {
        this.setDataToBeReturned('getSubjectQuestion');
        return super.getSubjectQuestion(...args);
    }

    doSubjectSync (...args) {
        this.setDataToBeReturned('doSubjectSync');
        return super.doSubjectSync(...args);
    }

    getAllSubjects (...args) {
        this.setDataToBeReturned('getAllSubjects');
        return super.getAllSubjects(...args);
    }

    querySubjectData (...args) {
        this.setDataToBeReturned('querySubjectData');
        return super.querySubjectData(...args);
    }

    updateSubjectData (...args) {
        this.setDataToBeReturned('updateSubjectData');
        return super.updateSubjectData(...args);
    }

    setSubjectData (...args) {
        this.setDataToBeReturned('setSubjectData');
        return super.setSubjectData(...args);
    }

    sendDiary (...args) {
        this.setDataToBeReturned('sendDiary');
        return super.sendDiary(...args);
    }

    sendLogs (...args) {
        this.setDataToBeReturned('sendLogs');
        return super.sendLogs(...args);
    }

    syncLastDiary (...args) {
        this.setDataToBeReturned('syncLastDiary');
        return super.syncLastDiary(...args);
    }

    addUser (...args) {
        this.setDataToBeReturned('addUser');
        return super.addUser(...args);
    }

    updateUser (...args) {
        this.setDataToBeReturned('updateUser');
        return super.updateUser(...args);
    }

    updateUserCredentials (...args) {
        this.setDataToBeReturned('updateUserCredentials');
        return super.updateUserCredentials(...args);
    }

    syncUsers (...args) {
        this.setDataToBeReturned('syncUsers');
        return super.syncUsers(...args);
    }

    sendEditPatient (...args) {
        this.setDataToBeReturned('sendEditPatient');
        return super.sendEditPatient(...args);
    }

    syncUserVisitsAndReports (...args) {
        this.setDataToBeReturned('syncUserVisitsAndReports');
        return super.syncUserVisitsAndReports(...args);
    }

    getServerTime (...args) {
        this.setDataToBeReturned('getServerTime');
        return super.getServerTime(...args);
    }

    getSingleSubject (...args) {
        this.setDataToBeReturned('getSingleSubject');
        return super.getSingleSubject(...args);
    }

    getDeviceID (subjectData) {
        if (!Trainer.trainerMode()) {
            return super.getDeviceID(subjectData);
        }

        let res = TrainerData.getDeviceID();

        return Q({ res });
    }

    getkrDom (params = {}) {
        if (!Trainer.trainerMode()) {
            return super.getkrDom(params);
        }

        let res = TrainerData.getkrDom();

        return Q({ res, syncID: 1 });
    }

    getAdminPassword (params = {}) {
        if (!Trainer.trainerMode()) {
            return super.getAdminPassword(params);
        }

        let res = TrainerData.getAdminPassword();

        return Q({ res, syncID: 1 });
    }
}
COOL.add('WebService', Trainer_WebService);

class Trainer_Startup extends COOL.getClass('Startup', Startup) {
    /*
     * Let's catch good-and-early where to detect we're in trainer mode at Startup
     */
    studyStartup () {
        logger.trace('PDE studyStartup');
        super.studyStartup();
        if (Trainer.trainerMode()) {
            logger.trace('studyStartup: Detected trainer mode');
            setTrainer();
        }
    }
}
COOL.add('Startup', Trainer_Startup);

class Trainer_Utilities extends COOL.getClass('Utilities', Utilities) {
    static isOnline (callback = $.noop) {
        if (Trainer.trainerMode()) {
            callback(true);
            return Q(true);
        }

        return super.isOnline(callback);
    }
}
COOL.add('Utilities', Trainer_Utilities);
