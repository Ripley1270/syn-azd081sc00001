import { MessageRepo } from 'core/Notify';
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';

export default function setupLogpadDialogs () {
    MessageRepo.add({
        type: 'Dialog',
        key: 'REVERSE_HANDOFF_CONFIRM',
        message: MessageHelpers.confirmDialogCreator({
            message: 'REVERSE_HANDOFF_CONFIRM'
        })
    }, {
        type: 'Dialog',
        key: 'LOGOUT_CONFIRM',
        message: MessageHelpers.confirmDialogCreator({
            message: 'LOGOUT_CONFIRM'
        })
    }, {
        type: 'Dialog',
        key: 'DIARY_TIMEOUT',
        message: MessageHelpers.notifyDialogCreator({
            message: 'DIARY_TIMEOUT'
        })
    }, {
        type: 'Dialog',
        key: 'TERMINATION_REPLACED',
        message: MessageHelpers.notifyDialogCreator({
            header: 'PLEASE_CONFIRM',
            ok: 'OK',
            message: 'ELPU_ON_REPLACED_DEVICE'
        })
    }, {
        type: 'Dialog',
        key: 'DEACTIVATED',
        message: MessageHelpers.notifyDialogCreator({
            header: 'PLEASE_CONFIRM',
            ok: 'OK',
            message: 'DEACTIVATED_LABEL'
        })
    }, {
        type: 'Dialog',
        key: 'ACTIVATED_ELSEWHERE',
        message: MessageHelpers.notifyDialogCreator({
            header: 'PLEASE_CONFIRM',
            ok: 'OK',
            message: 'ACTIVATED_ELSEWHERE'
        })
    }, {
        type: 'Dialog',
        key: 'SITE_DEACTIVATED',
        message: MessageHelpers.notifyDialogCreator({
            header: 'PLEASE_CONFIRM',
            ok: 'OK',
            message: 'SITE_DEACTIVATED_LABEL'
        })
    }, {
        type: 'Dialog',
        key: 'SITE_ACTIVATED_ELSEWHERE',
        message: MessageHelpers.notifyDialogCreator({
            header: 'PLEASE_CONFIRM',
            ok: 'OK',
            message: 'SITE_ACTIVATED_ELSEWHERE'
        })
    }, {
        type: 'Dialog',
        key: 'ELPU_FAILURE',
        message: MessageHelpers.notifyDialogCreator({
            type: 'error',
            message: 'ELPU_FAILURE'
        })
    }, {
        type: 'Dialog',
        key: 'LOCAL_STORAGE_FAILURE',
        message: MessageHelpers.notifyDialogCreator({
            type: 'error',
            message: 'PRIVATE_BROWSING_ON',
            header: 'LOCAL_STORAGE_FAILURE'
        })
    }, {
        type: 'Dialog',
        key: 'SECRET_QUESTION_CHANGE_FAILED',
        message: MessageHelpers.notifyDialogCreator({
            type: 'error',
            message: 'SECRET_QUESTION_CHANGE_FAILED',
            header: 'ERROR_TITLE'
        })
    }, {
        type: 'Dialog',
        key: 'SYNC_FAILED',
        message: MessageHelpers.confirmDialogCreator({
            header: 'ERROR_TITLE',
            message: 'SYNC_FAILED',
            ok: 'RETRY',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'INSTALL_FAILED',
        message: MessageHelpers.confirmDialogCreator({
            header: 'ERROR_TITLE',
            message: 'INSTALL_FAILED',
            ok: 'OK',
            type: 'error'
        })
    }, {
        type: 'Dialog',
        key: 'TIME_ZONE_CONFIRM',
        message: MessageHelpers.confirmDialogCreator({
            header: 'TIME_ZONE_CONFIRM_HEADER',
            message: 'TIME_ZONE_CONFIRM'
        })
    }, {
        type: 'Dialog',
        key: 'TRAINER_EXIT_NOTIFICATION',
        message: MessageHelpers.confirmDialogCreator({
            header: 'TRAINER_EXIT',
            message: 'TRAINER_EXIT_NOTIFICATION'
        })
    }, {
        type: 'Dialog',
        key: 'RESTART_NOTIFICATION',
        message: MessageHelpers.notifyDialogCreator({
            header: 'RESTART_NOTIFICATION_HEADER',
            message: 'RESTART_NOTIFICATION_MSG',
            ok: 'RESTART_NOW'
        })
    }, {
        type: 'Dialog',
        key: 'HAND_DEVICE_SITE',
        message: MessageHelpers.notifyDialogCreator({
            header: 'HAND_DEVICE_SITE_TITLE',
            message: 'HAND_DEVICE_SITE_BODY'
        })
    });
}
