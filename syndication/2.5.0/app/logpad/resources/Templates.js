
import Templates from 'core/resources/Templates';

/**
 * @fileOverview Core set of templates.
 * @author <a href="mailto:chopkins@phtcorp.com">Corey Hopkins</a>
 * @version 1.4
 */

Templates.set([
    {
        name: 'IndicatorFull',
        namespace: 'DEFAULT',
        template: '<div>' +
                        '<p>{{ label }}</p>' +
                        '<img src="{{ image }}" alt="{{ label }}"/>' +
                      '</div>'
    }, {
        name: 'IndicatorImage',
        namespace: 'DEFAULT',
        template: '<div><img src="{{ image }}"/></div>'
    }, {
        name: 'IndicatorLabel',
        namespace: 'DEFAULT',
        template: '<div><p>{{ label }}</p></div>'
    }, {
        name: 'Questionnaire',
        namespace: 'DEFAULT',
        template: '<a class="navigate-right">' +
                '<span class="title-block">' +
                    '<p class="title">{{ title }}</p>' +
                '</span>' +
                '<span class="glyphicon glyphicon-menu-right"></span>' +
            '</a>'

    }, {
        name: 'Question',
        namespace: 'DEFAULT',
        template: '<p class="{{ label }}">{{ text }}</p>'
    }, {
        name: 'QuestionHeader',
        namespace: 'DEFAULT',
        template: '<div class="question-header row">' +
            '<div class="col-xs-12">{{ left }}</div>' +
            '</div>'
    }, {
        name: 'Help',
        namespace: 'DEFAULT',
        template: '<p class="help"><strong>{{ help }}</strong></p><hr />'
    },
    {
        name: 'Input',
        namespace: 'DEFAULT',
        template: '<label for="{{ id }}"><br />{{ label }}</label><input type="{{ type }}" id="{{ id }}" name="{{ name }}" placeholder="{{ placeholder }}" size="{{ size }}" />'
    }, {
        name: 'DatePicker',
        namespace: 'DEFAULT',
        template: '<input id="{{ id }}" class="date-input" min="{{ min }}" max="{{ max }}" data-role="datebox" data-options=\'{{ configuration }}\' />'
    }, {
        name: 'DatePickerLabel',
        namespace: 'DEFAULT',
        template: '<label for="{{ id }}">{{ dateLabel }}</label>'
    }, {
        name: 'TimePicker',
        namespace: 'DEFAULT',
        template: '<input id="{{ id }}_time_input" class="time-input" min="{{ min }}" max="{{ max }}" data-role="datebox" data-options=\'{{ configuration }}\' />'
    }, {
        name: 'TimePickerLabel',
        namespace: 'DEFAULT',
        template: '<label for="{{ id }}_time_input">{{ timeLabel }}</label>'
    }, {
        name: 'AlarmPicker',
        namespace: 'DEFAULT',
        template: '<input id="{{ id }}_alarm_input" class="alarm-input" style="display:none" data-role="datebox"/>' +
                '<a id="{{ id }}" class="list-group-item">' +
                    '<span class="navigate-right">' +
                        '<span>{{ diaryName }}</span>' +
                        '<span class="direction-right ui-li-count ">' +
                            '<time>{{ alarmTime }}</time>' +
                            '<i></i>' +
                        '</span>' +
                    '</span>' +
                '</a>'
    }, {
        name: 'VasTextLabels',
        namespace: 'DEFAULT',
        template: '<div class="vas-label row">' +
                '<div class="col-xs-6 text-left"><p>{{ min }}</p></div>' +
                '<div class="col-xs-6 text-right"><p>{{ max }}</p></div>' +
            '</div>'
    },

    {
        name: 'VAS',
        namespace: 'DEFAULT',
        template: ''
    },
    {
        name: 'EQ5D',
        namespace: 'DEFAULT',
        template: `<div id='parentContainer' class='eq5d-container'> <!-- Flex cols -->
                        <div id='left' class='eq5d-left-col'>
                            <div id='logpadInstructionTextDIV' class='instructionTextDiv'>
                                <p class='eq5d-instruction-text'>{{ instructionText }}</p>
                            </div>
                            <div id='answerContainer'>
                                <div id='answerDIV'></div>
                            </div>
                            <div id='yhtDIV'>{{ answerLabelText }}</div>
                        </div> <!-- Flex rows -->
                        <div id='right' class='eq5d-right-col'>
                            <p id='topper' style=''>{{ topText }}</p>
                            <div id='middle'>
                                <canvas id='mainVasCanvas' class='eq5d-main-canvas' height='0' width='0' />
                            </div>
                            <p id='bottom' style=''>{{ botText }}</p>
                        </div>
                   </div>`
    },
    {
        name: 'EQ5D-OLD',
        namespace: 'DEFAULT',
        template: '<div id="parentContainer" style="display:flex; flex-direction: column; height:auto">' +
        '<div id="logpadInstructionTextDIV">' +
        '    <p style="margin: 0px; word-break:break-all; width:75%">{{ instructionText }}</p>' +
        '</div>' +
        '<div id="innerDIVS" style="display:flex;">' +
        '    <div id="left" style="display:flex; flex-direction:column;">' +
        '       <div id="spacer"/>' +
        '           <div id="answerDIV">' +
        '               <canvas id="answerCanvas"/>' +
        '           </div>' +
        '           <div id="yhtDIV" style="display: flex; flex:1; align-items: flex-start; justify-content: flex-start">' +
        '               <p style="word-break:break-all; text-align:center; width:100%; margin:0px;">Your health today</br>demonstrate wrapping some text</p></p>' +
        '           </div>' +
        '    </div>' +
        '    <div id="right" style="display:flex; flex-direction:column;">' +
        '        <p id="topper" style="margin: 0px; padding:0px; text-align:center">{{ topText }}</p>' +
        '        <div id="middle">' +
        '           <canvas id="mainVasCanvas" style="background-color:white"/>' +
        '        </div>' +
        '        <p id="bottom" style="margin: 0px; padding:0px; word-break:break-all; text-align:center">{{ botText }}</p>' +
        '    </div>' +
        '</div>'
    }, {
        name: 'Markers',
        namespace: 'DEFAULT',
        template: '<div class="markers clearfix"><marker class="left">{{ left }}</marker><marker class="right">{{ right }}</marker></div>'
    }, {
        name: 'NRSMarkers',
        namespace: 'DEFAULT',
        template: '<div class="markers row">' +
            '<div class="col-xs-6 text-left"><p>{{ left }}</p></div>' +
            '<div class="col-xs-6 text-right"><p>{{ right }}</p></div>' +
        '</div>'
    }, {
        name: 'ListView',
        namespace: 'DEFAULT',
        template: '<ul id="{{ id }}" data-role="listview"></ul>'
    }, {
        name: 'Screen',
        namespace: 'DEFAULT',
        template: '<section class="screen {{ className }}"></div>'
    }, {
        name: 'SubjectNumber',
        namespace: 'DEFAULT',
        template: '<div><strong>{{ subjectLabel }} {{ subjectNumber }}</strong></div>'
    }, {
        name: 'SiteCode',
        namespace: 'DEFAULT',
        template: '<div><strong>{{ siteLabel }} {{ siteCode }}</strong></div>'
    }, {
        name: 'PasswordRules',
        namespace: 'DEFAULT',
        template: '<div class="password-rules">{{ text }}</div><hr />'
    }, {
        name: 'ReviewScreen',
        namespace: 'DEFAULT',
        template: '<div id="{{ id }}" class="reviewScreen"><ul id="{{ id }}_Items" class="reviewScreen_Items" data-role="listview"></ul>' +
                      '<hr />' +
                      '<ul id="{{ id }}_Buttons" class="reviewScreen_Buttons" data-role="listview"></ul></div>'
    }, {
        name: 'ReviewScreenItems',
        namespace: 'DEFAULT',
        template: '<li><div>{{ text0 }}</div></li>'
    }, {
        name: 'ReviewScreenButtons',
        namespace: 'DEFAULT',
        template: '<li><a data-role="button">{{ text }}</a></li>'
    }, {
        name: 'SiteUser',
        namespace: 'DEFAULT',
        template: '<div id="site-user-name"><strong>{{ siteUserLabel }} {{ siteUser }}</strong></div>'
    },
    {
        name: 'TextBox',
        namespace: 'DEFAULT',
        template: '<div class="form-group"><input id="{{ id }}" placeholder="{{ placeholder }}" name="{{ name }}" class="{{ className }}" maxlength="{{ maxLength }}" type="text" autocapitalize="off"/></div>'
    },
    {
        name: 'DateTimeSpinnerModal',
        namespace: 'DEFAULT',
        template: `<div class="modal fade date-spinner-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin: auto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">
                                                {{modalTitle}}
                                            </h4>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body">
                                            <div class="modal-horizontal-container date-time-date-container date-container"></div>
                                            <div class="modal-horizontal-container date-time-time-container time-container"></div>
                                        </div>


                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                {{okButtonText}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    },
    {
        name: 'DateTimeSpinnerModal_12Hour',
        namespace: 'DEFAULT',
        template: `<div class="modal fade date-spinner-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin: auto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">
                                                {{modalTitle}}
                                            </h4>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body">
                                            <div class="modal-horizontal-container date-time-date-container date-container"></div>
                                            <div class="modal-horizontal-container date-time-time-container time-container" data-format="hh:mm A"></div>
                                        </div>


                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                {{okButtonText}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    },
    {
        name: 'DateTimeSpinnerModal_24Hour',
        namespace: 'DEFAULT',
        template: `<div class="modal fade date-spinner-modal" tabindex="-1" role="dialog" aria-hidden="true" style="margin: auto">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel">
                                                {{modalTitle}}
                                            </h4>
                                        </div>

                                        <!-- Modal Body -->
                                        <div class="modal-body">
                                            <div class="modal-horizontal-container date-time-date-container date-container"></div>
                                            <div class="modal-horizontal-container date-time-time-container time-container" data-format="HH:mm"></div>
                                        </div>


                                        <!-- Modal Footer -->
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                {{okButtonText}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>`
    }
], {
    add: true,
    merge: true,
    remove: false
});

LF.Resources.Templates = Templates;

// A shortcut to make displaying templates easier.
LF.templates = LF.Resources.Templates;

export default Templates;
