import PageView from 'core/views/PageView';
import * as Helpers from 'core/Helpers';
import ELF from 'core/ELF';
import CurrentContext from 'core/CurrentContext';
import Logger from 'core/Logger';
import CurrentSubject from 'core/classes/CurrentSubject';

const logger = new Logger('ToolboxView');

export default class ToolboxView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} events - The view's events.
         * @readonly
         */
        this.events = {
            // Click event for back button
            'click #back': 'back',

            // Displays the about content
            'click #about-btn': 'about',

            // Navigates to the change password view
            'click #change-password': 'changePassword',

            // Navigates to the privacy policy application view
            'click #privacy-policy': 'privacyPolicy',

            // Execute all transmissions and DCF requests.
            'click #transmit': 'transmit',

            // Navigates to the change secret question view
            'click #change-secret-question': 'changeSecretQuestion',

            // Navigates to the support view
            'click #support': 'support',

            // Navigates to the set alarms view
            'click #set-alarms': 'setAlarms',

            // Navigates to the take messageBox screenshots view
            'click #messagebox-screenshots': 'takeMessageBoxScreenshots',

            // Nagivates to the set time zone view
            'click #set-time-zone': 'setTimeZone',

            // Logout
            'click #logout': 'attemptLogout',

            // Logout
            'click #console-log': 'consoleLog'
        };

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch in the render function.
         */
        this.templateStrings = {
            about: 'ABOUT',
            header: 'APPLICATION_HEADER',
            title: 'SETTINGS',
            changePassword: 'CHANGE_PASSWORD',
            changeSecretQuestion: 'CHANGE_SECRET_QUESTION',
            privacyPolicy: 'PRIVACY_POLICY',
            transmit: 'TRANSMIT_REPORTS',
            support: 'SUPPORT',
            setAlarms: 'SET_ALARMS',
            setTimeZone: 'SET_TIMEZONE',
            logout: 'LOGOUT',
            consoleLog: 'CONSOLE_LOG'
        };

        /**
         * Execute all transmissions and DCF requests.
         * @returns {Q.Promise<void>}
         */
        this.transmit = _.debounce(function transmissions () {
            LF.security.pauseSessionTimeOut();

            return this.spinner.show()
            .then(() => {
                return this.triggerTransmitRule()
                .finally(() => {
                    this.spinner.hide();
                    this.render();
                    LF.security.restartSessionTimeOut();
                });
            });
        }, 300, true);
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'toolbox-page'
     */
    get id () {
        return 'toolbox-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#toolbox-template'
     */
    get template () {
        return '#toolbox-template';
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return CurrentSubject.getSubject()
        .then((subject) => {
            /**
             * @property {Subject} subject - The subject the device is registered to.
             */
            this.subject = subject;
        });
    }

    /**
     * Navigates back to the dashboard view.
     */
    back () {
        this.undelegateEvents();
        this.sound.play('click-audio');
        this.navigate('dashboard');
    }

    /**
     * Calls the transmit rule for this view.
     * @returns {Q.Promise<void>}
     */
    triggerTransmitRule () {
        return ELF.trigger('TOOLBOX:Transmit', { subject: this.subject }, this);
    }

    /**
     * Navigates the user to the about view.
     */
    about () {
        this.undelegateEvents();
        this.navigate('about');
    }

    /**
     * Navigates the user to the change password view.
     */
    changePassword () {
        this.undelegateEvents();
        this.navigate('change_password');
    }

    /**
     * Navigates the user to the change secret question view.
     */
    changeSecretQuestion () {
        this.undelegateEvents();
        this.navigate('change_secret_question');
    }

    /**
     * Navigates the user to the privacy policy application view.
     */
    privacyPolicy () {
        this.undelegateEvents();
        this.navigate('privacy_policy');
    }

    /**
     * Navigates the user to the customer support view.
     */
    support () {
        this.undelegateEvents();
        this.navigate('support');
    }

    /**
     * Navigates the user to the set alarms view.
     */
    setAlarms () {
        this.undelegateEvents();
        this.navigate('set_alarms');
    }

    /**
     * Navigates the user to the take messageBox screenshots view
     */
    takeMessageBoxScreenshots () {
        this.undelegateEvents();
        this.navigate('take_messagebox_screenshots');
    }

    /**
     * Navigates the user to the set time zone view
     */
    setTimeZone () {
        this.undelegateEvents();
        this.navigate('set_time_zone');
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        return this.buildHTML({}, true)
        .then(() => {
            window.scrollTo(0, 0);
            this.delegateEvents();

            ELF.trigger('TOOLBOX:Render', {}, this)
            .then(() => {
                this.showSettingsItems();
                this.delegateEvents();
            });
        });
    }

    /**
     * Displays set alarm item on settings page
     * @param {Object} params - Additional parameters passed into the action from ELF.  Not used in this action.
     * @param {Function} [callback=$.noop] - A callback function invoked upon the action's completion.
     */
    showSetAlarm (params, callback = $.noop) {
        let subject = this.subject,
            subjectActive = subject && subject.get('subject_active') !== 0,
            subjectAlarmsExists = false,
            schedules = Helpers.getScheduleModels();

        if (schedules && schedules.length > 0) {
            subjectAlarmsExists = _.find(schedules, (schedule) => {
                let alarmParams = schedule.get('alarmParams');
                return alarmParams && alarmParams.subjectConfig;
            });
        }

        if (subjectActive && subjectAlarmsExists) {
            this.$('#set-alarms').removeClass('hidden');
        }

        callback();
    }

    /**
     * Displays message box screenshots item.
     * @param {Object} params - Additional parameters passed into the action from ELF.  Not used in this action.
     * @param {Function} [callback=$.noop] - A callback function invoked upon the action's completion.
     */
    showMessageBoxScreenShots (params, callback = $.noop) {
        LF.Wrapper.exec({
            execWhenWrapped: () => {
                if (LF.StudyDesign.enableMessageBoxScreenshots) {
                    this.$('#messagebox-screenshots').removeClass('hidden');
                }
            }
        });

        callback();
    }

    /**
     * Displays change password item
     * @param {Object} params - Additional parameters passed into the action from ELF.  Not used in this action.
     * @param {Function} [callback=$.noop] - A callback function invoked upon the action's completion.
     */
    showChangePassword (params, callback = $.noop) {
        this.$('#change-password').removeClass('hidden');

        callback();
    }

    /**
     * Displays change security question item
     * @param {Object} params -  Additional parameters passed into the action from ELF.  Not used in this action.
     * @param {Function} [callback=$.noop] - A callback function invoked upon the action's completion.
     */
    showChangeSecurityQuestion (params, callback = $.noop) {
        if (LF.StudyDesign.askSecurityQuestion) {
            this.$('#change-secret-question').removeClass('hidden');
        }

        callback();
    }

    /**
     * Displays set time zone item
     * @param {Object} params - Additional parameters passed into the action from ELF.  Not used in this action.
     * @param {Function} [callback=$.noop] - A callback function invoked upon the action's completion.
     */
    showSetTimeZone (params, callback = $.noop) {
        LF.Wrapper.exec({
            execWhenWrapped: () => {
                // Only show the Set Time Zone button if the platform is not iOS.
                // iOS doesn't allow applications to alter time zone.
                if (LF.Wrapper.platform !== 'ios') {
                    this.$('#set-time-zone').removeClass('hidden');
                }

                callback();
            },
            execWhenNotWrapped: () => {
                this.$('#set-time-zone').removeClass('hidden');

                callback();
            }
        });
    }

    /**
     * Hides the specific list item
     * @param {string} params - The id of the list element that will be not displayed on the settings page
     * @param {Function} [callback=$.noop] - A callback function invoked upon the action's completion.
     */
    hideToolboxItem (params, callback = $.noop) {
        if (params == null) {
            throw new Error('Missing Argument: params is null or undefined.');
        }

        this.$(params).addClass('hidden');

        callback();
    }

    /**
     * Runs through and executes methods to display settings items.
     */
    showSettingsItems () {
        let itemsList = LF.StudyDesign.toolboxConfig,
            itemsListLength = itemsList && itemsList.length,
            /* eslint-disable-next-line consistent-return */
            toBeExecuted = (itemRole) => {
                if (!itemRole) {
                    return false;
                }

                if (_.isArray(itemRole)) {
                    return _.contains(itemRole, 'all') || _.contains(itemRole, CurrentContext().role);
                }

                if (typeof itemRole === 'string') {
                    return itemRole === 'all';
                }
                return false;
            };

        for (let i = 0; i < itemsListLength; i++) {
            if (this[itemsList[i].itemFunction]) {
                if (toBeExecuted(itemsList[i].toolboxRole)) {
                    this[itemsList[i].itemFunction]();
                }
            } else {
                const msg = `Invalid/Missing Argument: itemFunction for ${itemsList[i].id} ` +
                             'is null, undefined or invalid';
                logger.error(msg);
                throw new Error(msg);
            }
        }
    }

    /**
     * Attempts to log out of the application.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    attemptLogout (e) {
        e.preventDefault();
        this.undelegateEvents();

        this.sound.play('click-audio');

        ELF.trigger(`DASHBOARD:Logout/${CurrentContext().role}`, {}, this)
        .then(res => res.preventDefault || LF.security.logout(true))
        .done();
    }

    /**
     *  Shows console log button
     */
    showConsoleLog () {
        this.$('#console-log').removeClass('hidden');
    }

    /**
     * Navigate to the Error Log view.
     */
    consoleLog () {
        this.navigate('console_log');
    }
}
