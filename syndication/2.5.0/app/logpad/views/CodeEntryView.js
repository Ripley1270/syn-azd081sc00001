import ActivationBaseView from 'core/views/ActivationBaseView';
import * as lStorage from 'core/lStorage';
import Data from 'core/Data';
import * as Utilities from 'core/utilities';
import Logger from 'core/Logger';
import COOL from 'core/COOL';
import EULA from 'core/classes/EULA';
import Users from 'core/collections/Users';
import CurrentContext from 'core/CurrentContext';
import ELF from 'core/ELF';

let logger = new Logger('CodeEntryView');
const _evt = { preventDefault: $.noop };

/**
 * View for collecting QR code for device/subject activation.
 */
export default class CodeEntryView extends ActivationBaseView {
    /**
     * Constructor
     * @param {Object} options - View options
     */
    constructor (options) {
        super(options);

        logger.traceEnter('Constructor');

        /**
         * @property {Object<string,string>} selectors - A list of selectors to generate.
         */
        this.selectors = {
            txtCode: '#txtCode',
            txtStudy: '#txtStudy',
            next: '#next',
            connectionBtn: '#checkConnectionBtn',
            resetBtn: '#resetBtn'
        };

        /**
         * @property {Object<string,string> events - Code Entry view's event list.
         * @readonly
         */
        this.events = {
            // Submit form event
            'click #next': 'submit',

            // Input event for code entry
            'input #txtCode': 'onInput',

            // Input event for url entry
            'input #txtStudy': 'onInput',

            // Click event for scanner button
            'click #scanner': 'scan',

            // Click event for check connect button.
            'click #checkConnectionBtn': 'checkConnectHandler',

            'click #resetBtn': 'resetHandler'
        };

        /**
         * @property {Object<string,(Object|string)>}Strings to fetch in the render function.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: 'DEVICE_ACTIVATION',
            activationCode: 'ACTIVATION_CODE',
            submit: 'NEXT',
            studyUrl: 'STUDY_URL',
            submitIcon: {
                key: 'LOGIN',
                namespace: 'ICONS'
            },
            checkConnectBtn: 'CHECK_CONNECTION_TEXT',
            resetBtn: 'RESET_BTN_TEXT'
        };

        // Initialize...
        lStorage.setItem('activationRole', 'subject');
        EULA.decline();
        localStorage.removeItem('activationCode');
        localStorage.removeItem('activationFlag');
        logger.traceExit('Constructor');
    }

    /**
     * The id of the root element.
     * @property {string} id
     * @readonly
     * @default 'code-entry-page'
     */
    get id () {
        return 'code-entry-page';
    }

    /**
     * Id of template to render
     * @property {string} template
     * @readonly
     * @default '#codeEntry-template'
     */
    get template () {
        return '#codeEntry-template';
    }

    /**
     * Renders the view.
     * @returns {Q.Promise<void>}
     */
    render () {
        logger.traceEnter('render');
        CurrentContext().setContextLanguage(`${LF.StudyDesign.defaultLanguage}-${LF.StudyDesign.defaultLocale}`);
        return this.buildHTML({
            key: this.key
        }, true)
        .catch(err => logger.error('CodeEntryView could not render', err))
        .finally(() => {
            Utilities.removeSplashScreen();
            logger.traceExit('render async part');
        });
    }

    /**
     * Process a setupCode and will ask the server for validation.
     * @param {string} code - The setup code to process.
     * @returns {Q.Promise<void>}
     */
    processSetupCode (code) {
        // eslint-disable-next-line consistent-return
        const process = () => {
            if (this.isValidForm('#txtCode')) {
                let studyNameOrUrl = this.$txtStudy.val();
                let fullUrl = this.getStudyUrl(studyNameOrUrl);
                if (fullUrl == null) {
                    throw new Error('Error while processing setup code. Study url is not defined.');
                }
                Utilities.setServiceBase(fullUrl);
                return this.checkSubjectIsActive(code);
            }
            this.sound.play('error-audio');
            this.showInputError('#txtCode', 'INVALID_CODE');
        };

        // Closes the virtual keyboard and scrolls to top of page
        window.scrollTo(0, 0);

        // only process setup code if there is no rule, or a rule returns false
        return ELF.trigger('CODEENTRY:Submit', { value: this.$txtStudy.val(), product: 'logpad' }, this)
            .then((res) => {
                if (!res.preventDefault) {
                    process();
                }
            })
            .catch((err) => {
                logger.error('Error processing Setup Code', err);
            });
    }

    /**
     * Submits the form and validates the setup code.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     * @returns {Q.Promise<void>}
     */
    submit (e) {
        let input = this.$txtCode,
            code = input.val().trim();

        e.preventDefault();
        input.blur();
        return this.processSetupCode(code);
    }

    /**
     * Sets the subject language.
     * @param {Object} res - The response from the getSubjectActive API call.
     */
    setSubjectLanguage (res) {
        logger.trace('checkSubjectIsActive->handleResult->setSubjectLanguage');

        // Pick the language out of the server response, if set
        if (res && res.X) {
            let ary = res.X.split('-');
            if (ary.length === 2) {
                this.preferredLanguage = ary[0];
                this.preferredLocale = ary[1];
            }
        }

        if (!this.preferredLocale && !this.preferredLanguage) {
            this.preferredLanguage = LF.StudyDesign.defaultLanguage;
            this.preferredLocale = LF.StudyDesign.defaultLocale;
        }

        LF.content.setContextLanguage(`${LF.StudyDesign.defaultLanguage}-${LF.StudyDesign.defaultLocale}`);

        // To change the time zone we need to record this in localStorage
        localStorage.setItem('preferredLanguageLocale', `${this.preferredLanguage}-${this.preferredLocale}`);
    }

    /**
     * Checks the setup code against the server. Handles transmission result and error.
     * @param {string} code - The setup code to process.
     * @returns {Q.Promise<void>}
     */
    checkSubjectIsActive (code) {
        logger.traceEnter(`checkSubjectIsActive code:${code}`);

        let activationCode = code.toString(),
            handleResult = ({ res }) => {
                let lsKrpt = localStorage.getItem('krpt');
                logger.trace(`checkSubjectIsActive->handleResult res:${res}`);
                if (_.isEmpty(res)) {
                    lStorage.removeItem('serviceBase');

                    this.showInputError('#txtCode', 'INVALID_CODE');
                    this.sound.play('error-audio');
                    this.removeMessage();
                } else {
                    this.setSubjectLanguage(res);

                    Data.code = activationCode;

                    // This will enable changing to second user if first user's account is locked by providing
                    // wrong password and moving back to CodeEntryView (DE20655).
                    if (lsKrpt && lsKrpt !== res.K) {
                        let users = new Users();
                        users.fetch()
                            .then(() => {
                                let user = users.findWhere({ id: 0 });
                                if (user) {
                                    LF.security.resetFailureCount(user);
                                    user.save();
                                }
                            });
                    }

                    localStorage.setItem('krpt', res.K);

                    // The flag is true, if it is activation process; false, if it is reactivation process
                    Data.activation = !res.S;

                    // To change the time zone we need to record this in localStorage
                    localStorage.setItem('activationCode', activationCode);
                    localStorage.setItem('activationFlag', !res.S);

                    logger.operational(`Setup code is verified by server. ${!res.S ? 'Activation' : 'Reactivation'} is starting.`);

                    ELF.trigger('CODEENTRY:Complete', { }, this)
                        .then((evt) => {
                            if (!evt.preventDefault) {
                                this.navigate('set_time_zone_activation', true);
                            }

                            this.removeMessage();
                        })
                        .done();
                }
            },
            handleError = ({ errorCode, httpCode, isTimeout }) => {
                let errorMsg = 'INVALID_CODE';

                logger.trace(`checkSubjectIsActive->handleError errorCode: ${errorCode}, httpCode: ${httpCode}, isTimeout: ${isTimeout}`);

                lStorage.removeItem('serviceBase');

                this.removeMessage();

                if (isTimeout) {
                    errorMsg = 'CONNECTION_TIMEOUT';
                } else if (httpCode === 404) {
                    errorMsg = 'CODE_ENTRY_NO_SERVER';
                }

                this.showInputError('#txtCode', errorMsg);

                this.sound.play('error-audio');
            };

        return this.attemptTransmission()
            .then((online) => {
                // If offline, the attemptTransmission method will handle display of a modal.
                if (online) {
                    return this.webService.getSubjectActive(activationCode)
                        .then(handleResult)
                        .catch(handleError);
                }

                lStorage.removeItem('serviceBase');
                return Q();
            })
            .finally(() => {
                logger.traceExit('checkSubjectIsActive');
            });
    }

    /**
     * Interpret a QR Text string
     * @param {string} text - the string to interpret
     */
    processQRText (text) {
        // Called both by a received QR scan OR if the user-setupCode looks like a json string
        // which is used for debugging or pointing the system at a different URL for testing etc.
        let qrParams,
            studyUrl,
            badQRCode = () => {
                this.disableButton(this.$next);
                this.showInputError('#txtCode', 'ERROR_TITLE');
            };

        logger.traceEnter(`processQRText text:${text}`);

        if (!text) {
            return;
        }

        try {
            qrParams = $.parseJSON(text);

            if (!qrParams.study) {
                badQRCode();
                this.sound.play('error-audio');
                logger.error('Missing study param in QR text');
            }

            if (!qrParams.setupcode) {
                badQRCode();
                this.sound.play('error-audio');
                logger.error('Missing setupcode param in QR text');
            }

            studyUrl = this.getStudyUrl(qrParams.study);

            this.$txtStudy.val(studyUrl || qrParams.study);
            this.$txtCode.val(qrParams.setupcode);

            // studyUrl will be null if the study does not exist in environment.js and the custom environments are disabled
            if (studyUrl) {
                Utilities.setServiceBase(studyUrl);

                // If the language is present, let's take it (otherwise we take it from the study)
                if (qrParams.language) {
                    let ary = qrParams.language.split('-');
                    if (ary.length === 2) {
                        this.preferredLanguage = ary[0];
                        this.preferredLocale = ary[1];
                    }
                }

                this.clearInputState('#txtCode');
                this.clearInputState('#txtStudy');

                this.enableButton(this.$next);
            }
        } catch (e) {
            badQRCode();
            this.sound.play('error-audio');
            logger.error(`json qrcode didnt parse; text:${text}`);
        } finally {
            logger.traceExit('processQRText');
        }
    }

    /**
     * Activate the QR Scanner to take picture and process it
     */
    scan () {
        let config = {
            orientation: 'portrait'
        };

        return cordova.plugins.barcodeScanner.scan((result) => {
            this.processQRText(result.text);
        }, $.noop, config);
    }

    /**
     * Enables the next button when setup code and study URL input are both filled out
     */
    onInput () {
        let validCode = this.$txtCode.val().length > 0,
            validUrl = this.$txtStudy.val().length > 0;

        if (validCode && validUrl) {
            this.enableButton(this.$next);
        } else {
            this.disableButton(this.$next);
        }
    }

    /**
     * Click event handler for the checkConnection button.
     * @param {(MouseEvent|TouchEvent)} [evt=_evt] - A mouse or touch event.
     * @example
     * this.events = { 'click #checkConnectionBtn': 'checkConnectHandler' };
     */
    checkConnectHandler (evt = _evt) {
        evt.preventDefault();

        // We're using a handler to invoke CodeEntryView.checkConnect() so .done() can be called on
        // the returned promise.  This prevents any errors from being swallowed
        // and the error 'Detected 1 unhandled rejections of Q promises' from being thrown.
        this.checkConnect()
            .done();
    }

    /**
     * Performs the calls to check the device connection to its study
     * @returns {Q.Promise<void>}
     */
    checkConnect () {
        // Returned promise is for testability alone.
        return ELF.trigger('checkConnectionToStudy', {}, this);
    }

    /**
     * Handles reset button
     * @param {Event} e - Event args
     * @returns {Q.Promise<void>}
     */
    resetHandler (e) {
        e.preventDefault();

        return ELF.trigger('APPLICATION:ResetData', {}, this)
        .done();
    }

    /**
     * It shows the connection status on the button whether the device
     * is online or offline
     * @param status - boolean; true if device is online, otherwise false
     * @returns {Q.Promise<any>}
     */
    showConnectionStatus (status) {
        const stringsToFetch = {
            success: 'CONNECT_SUCCESS',
            fail: 'CONNECT_FAIL'
        };
        return this.i18n(stringsToFetch)
            .then((strings) => {
                if (status) {
                    // we flush out the existing classes instead of checking for them, then set the one we want.
                    logger.operational('Network connection status was checked. Result: Success');
                    this.$connectionBtn.html(strings.success).removeClass('btn-primary btn-success btn-danger').addClass('btn-success');
                } else {
                    logger.operational('Network connection status was checked. Result: Failed to connect.');
                    this.$connectionBtn.html(strings.fail).removeClass('btn-primary btn-success btn-danger').addClass('btn-danger');
                }
            });
    }

    /*
     * Handles the restricted custom environment entry attempts
     */
    handleDisabledCustomEnvironment () {
        logger.error('Custom environments are disabled');

        this.disableButton(this.$next);
        this.sound.play('error-audio');
        this.showInputError('#txtStudy', 'CUSTOM_ENVIRONMENT_USAGE_ERROR');
    }

    /**
     * Returns the URL for the study
     * @param {string} studyName - The text entered as the study name.
     * @returns {(string|null)} The templated URL.
     */
    getStudyUrl (studyName) {
        let environment,
            disableCustomEnvironments = LF.StudyDesign.disableCustomEnvironments !== undefined ?
                LF.StudyDesign.disableCustomEnvironments :
                LF.CoreSettings.disableCustomEnvironments;

        if (studyName === 'trainer') {
            return studyName;
        }

        // DE21035 - Ensure the entered studyName is lowercase.
        // eslint-disable-next-line no-param-reassign
        studyName = studyName.toLowerCase();

        // if it looks like a URL
        if (/^http/i.test(studyName)) {
            // try matching the url of an environment
            environment = _.find(LF.StudyDesign.environments.toJSON(), (env) => {
                return studyName === env.url.toLowerCase();
            });

            // Use the entered url if it matches an existing environment or Custom environments are not disabled
            if (environment || !disableCustomEnvironments) {
                return studyName;
            }

            // StudyName does not match the url of any environment and custom environments are disabled
            if (disableCustomEnvironments) {
                this.handleDisabledCustomEnvironment();
                return null;
            }
        }

        // try matching on the hostname part of an environment
        environment = _.find(LF.StudyDesign.environments.toJSON(), (env) => {
            // eslint-disable-next-line prefer-template
            return new RegExp(`^https?://${studyName}([/.]|$)`).test(env.url.toLowerCase());
        });

        if (environment) {
            return environment.url;
        }

        // also matching on the id of an environment
        environment = _.findWhere(LF.StudyDesign.environments.toJSON(), { id: studyName });

        if (environment) {
            return environment.url;
        }

        // StudyName does not exist in any environment and custom environemnets are disabled
        if (disableCustomEnvironments) {
            this.handleDisabledCustomEnvironment();
            return null;
        }

        // If no match in environments, construct a url from template
        return _.template(LF.StudyDesign.serviceUrlFormat, {
            interpolate: /{{([\s\S]+?)}}/g
        })({ study: studyName });
    }
}

window.LF.View.CodeEntryView = CodeEntryView;
COOL.add('CodeEntryView', CodeEntryView);
