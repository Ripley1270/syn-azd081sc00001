import PageView from 'core/views/PageView';
import Data from 'core/Data';
import CurrentContext from 'core/CurrentContext';
import ELF from 'core/ELF';
import { getScheduleModels } from 'core/Helpers';
import Questionnaires from 'core/collections/Questionnaires';
import Indicators from 'core/collections/Indicators';
import Subjects from 'core/collections/Subjects';
import Logger from 'core/Logger';
import * as DisplayItem from 'logpad/scheduling/DisplayItem';
import CurrentSubject from 'core/classes/CurrentSubject';

let logger = new Logger('DashboardView');

export default class DashboardView extends PageView {
    constructor (options = {}) {
        super(options);

        logger.traceEnter('Constructor start');

        /**
         * @property {Object<string,string>} events - A map of the view's events.
         * @readonly
         */
        this.events = _.extend(this.events, {
            // Tap event for toolbox button
            'click #application-toolbox': 'toolbox',

            // Click event for sync button
            'click #sync-button': 'transmitHandler'
        });

        /**
         * Debounced transmit.
         * @returns {Q.Promise<void>}
         */
        this.transmitHandler = _.debounce(() => {
            return this.transmit();
        }, 500, true);

        /**
         * @property {Object} options - Options passed into the constructor.
         */
        this.options = options;

        /**
         * @property {Object} listViews - A map of list views.
         */
        this.listViews = {};

        // remove questionnaire to dashboard navigation flag
        localStorage.removeItem('questionnaireToDashboard');

        Data.Questionnaire = false;

        this.resetModels();

        LF.schedule.availableSchedules.reset();

        /**
         * @property {Subjects} subjects - A collection of subjects.
         */
        this.subjects = new Subjects();

        this.$toolbox = undefined;
        this.$sync = undefined;

        logger.traceExit('Constructor exit');
    }

    /**
     * @property {string} id - The unique id of the root element.
     * @readonly
     * @default 'dashboard-page'
     */
    get id () {
        return 'dashboard-page';
    }

    /**
     * @property {string} template - Id of template to render
     * @readonly
     * @default '#dashboard-template'
     */
    get template () {
        return '#dashboard-template';
    }

    /**
     * @property {Object<string,string>} selectors - A list of selectors to populate.
     */
    get selectors () {
        return {
            toolbox: '#application-toolbox',
            inactiveMsg: '#inactiveMsg',
            questionnaires: '#questionnaires',
            noDiariesMsg: '#noDiariesMsg',
            sync: '#sync-button'
        };
    }

    /**
     * Resolve any dependecies required for the view to render.
     * @returns {Q.Promise<Object>}
     */
    resolve () {
        return CurrentSubject.getSubject()
        .then((subject) => {
            this.subject = subject;

            return ELF.trigger(`DASHBOARD:Open/${CurrentContext().role}`, {}, this);
        });
    }

    /**
     * dislable all the buttons to avoid race conditions
     */
    disableAll () {
        this.disableButton(this.$toolbox);
        this.disableButton(this.$sync);
        this.disableButton('.list-group-item');
    }

    /**
     * re-enable buttons
     */
    enableAll () {
        this.enableButton(this.$toolbox);
        this.enableButton(this.$sync);
        this.enableButton('.list-group-item');
    }

    /**
     * Execute all transmissions and DCF requests.
     * @returns {Q.Promise<void>}
     */
    transmit () {
        // If the sync button is disabled, do nothing.
        if (this.isDisabled(this.$sync)) {
            return Q();
        }

        // Disabled all buttons to prevent further actions.
        this.disableAll();

        LF.security.pauseSessionTimeOut();

        return this.spinner.show()
        .then(() => this.triggerTransmitRule())
        .finally(() => {
            LF.security.restartSessionTimeOut();
        })
        .then(() => this.spinner.hide())
        .then(() => this.render())
        .finally(() => {
            this.enableAll();
        });
    }

    /**
     * Calls the transmit rule for this view.
     * @returns {Q.Promise<void>}
     */
    triggerTransmitRule () {
        return ELF.trigger(`DASHBOARD:Transmit/${CurrentContext().role}`, { subject: this.subject }, this);
    }

    /**
     * Load available schedules into LF.schedule.availableSchedules.
     * @returns {Q.Promise<null>}
     */
    loadSchedules () {
        let schedules = LF.schedule.availableSchedules;

        // scheds is schedules
        const determineSchedules = (scheds, context) => Q.Promise((resolve) => {
            LF.schedule.determineSchedules(scheds, context, resolve);
        });

        logger.trace('loadSchedules');

        return determineSchedules(CurrentContext().get('schedules'), {
            subject: this.subject
        })
        .then((availableSchedules) => {
            logger.trace('availableSchedules  Calling callback');
            this.stopListening(LF.schedule.availableSchedules);

            schedules.set(this.filterSchedules(availableSchedules));
            return schedules;
        });
    }

    /**
     * Render content part of the dashboard
     * @param {string} contentId selector for the content to render
     * @returns {Q.Promise<void>}
     */
    renderContent (contentId) {
        let subjectActive = this.subject != null && this.subject.get('subject_active') !== 0,
            dashboardContent = this.$(contentId);

        logger.traceEnter(`renderContent id: ${contentId}`);

        if (!subjectActive ||
                this.subject.get('phase') === LF.StudyDesign.terminationPhase ||
                this.subject.get('subject_active') === 2) {
            this.showElement(dashboardContent);
            this.showElement(this.$inactiveMsg);
            this.hideElement(this.$questionnaires);

            logger.traceExit('renderContent');
            return Q();
        }
        this.clear();

        this.hideElement(this.$inactiveMsg);
        this.showElement(this.$questionnaires);

        return this.loadSchedules()
        .then((availableSchedules) => {
            let diariesAvailable = (currentSchedules) => {
                if (currentSchedules.length === 0 ||
                    LF.schedule.getDiarySchedules(currentSchedules.models).length === 0) {
                    this.showElement(this.$noDiariesMsg);
                } else {
                    this.hideElement(this.$noDiariesMsg);
                }
            };

            dashboardContent.removeClass('hidden');
            diariesAvailable(availableSchedules);

            this.listenTo(LF.schedule.availableSchedules, 'add remove', () => {
                diariesAvailable(LF.schedule.availableSchedules);
            });
        })
        .then(() => DisplayItem.addToView(this))
        .then((views) => {
            this.listViews = views || {};
            logger.trace(`addToView views:${views}`);
        })
        .then(() => ELF.trigger('DASHBOARD:Upcoming', {}, this))
        .then(() => {
            // When a new schedule is added or removed, reload available schedules
            this.listenTo(CurrentContext().get('schedules'), 'add change remove', () => {
                this.loadSchedules();
            });
            logger.traceExit('renderContent');
        });
    }

    /**
     * Renders the view
     * @returns {Q.Promise<void>}
     */
    render () {
        const text = CurrentContext().get('role').get('text');

        logger.traceEnter('render');

        this.templateStrings = {
            deactivated: (text && text.deactivated) || 'DEACTIVATED_LABEL',
            header: (text && text.header) || 'APPLICATION_HEADER',
            noDiariesTitle: (text && text.noDiariesTitle) || 'NO_DIARIES_TITLE',
            noDiariesAvailable: (text && text.noDiariesAvailable) || 'SCHED_NO_DIARIES_AVAILABLE',
            activatedElsewhere: (text && text.activatedElsewhere) || 'ACTIVATED_ELSEWHERE',
            sync: 'SYNC'
        };

        if (this.subject == null || this.subject.get('subject_active') === 0) {
            this.templateStrings.deactivated = this.templateStrings.activatedElsewhere;
        }

        return this.getPendingReportCount()
        .then(count => this.buildHTML({ count }, true))
        .then(() => ELF.trigger(`DASHBOARD:Rendered/${CurrentContext().role}`, {
            subject: this.subject,

            // Skip if we are forcing render (IE: Screenshot mode)
            skip: this.options.force
        }, this))
        .then(() => this.renderContent('#dashboard-content'))
        .finally(() => {
            logger.traceExit('render');
        });
    }

    /**
     * Navigate to the Toolbox screen.
     * @param {Event} e Event Data
     */
    toolbox (e) {
        if (this.isDisabled(this.$toolbox)) {
            return;
        }

        this.disableAll();
        e.preventDefault();
        this.sound.play('click-audio');
        this.navigate('toolbox');
    }

    /**
     * Remove a questionnaire from the dashboard.
     * @param {string} id The id of the questionnaire to remove.
     */
    removeFromDashboard (id) {
        let scheduleModel = _.find(getScheduleModels(), obj => obj.get('target').id === id);

        if (this.listViews && this.listViews.questionnairesList) {
            this.listViews.questionnairesList.removeSubview(scheduleModel);

            return ELF.trigger(`DASHBOARD:RemovedFromDashboard/${id})`, {}, this);
        }

        return Q();
    }

    /**
     * Reset the schedulable models.
     *  Parsing and stringifying into a new Collection creates a deep clone.
     */
    resetModels () {
        let questionnaires = JSON.parse(JSON.stringify(LF.StudyDesign.questionnaires)),
            indicators = JSON.parse(JSON.stringify(LF.StudyDesign.indicators));

        // reset questionnaire models before new schedule evaluation
        if (this.questionnaires) {
            this.questionnaires.reset(questionnaires);
        } else {
            this.questionnaires = new Questionnaires(questionnaires);
        }

        // reset indicator models before new schedule evaluation
        if (this.indicators) {
            this.indicators.reset(indicators);
        } else {
            this.indicators = new Indicators(indicators);
        }
    }

    // noinspection JSMethodCanBeStatic
    /**
     * Filters the schedules to those allowed on this view.
     * Currently no filtering required
     * @param {Object} availableSchedules the schedules to be filtered
     * @return {Object} the available schedules, EXACTLY as passed in
     */
    filterSchedules (availableSchedules) {
        return availableSchedules;
    }

    /**
     * Refreshes the Dashboard's listview.
     */
    refresh () {
        if (this.listViews && this.listViews.questionnairesList) {
            this.listViews.questionnairesList.refresh();
        }
    }

    /**
     * Cleanly close all listViews to make sure any listeners are destroyed.
     */
    clear () {
        this.listViews && Object.keys(this.listViews).forEach((index) => {
            this.listViews[index].close();
        });
    }

    /**
     * Clear listViews then close.
     */
    close () {
        this.clear();
        super.close();
    }
}
