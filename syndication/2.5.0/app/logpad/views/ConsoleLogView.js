import ConsoleLogView from 'core/views/ConsoleLogView';
import * as Utilities from 'core/utilities';
import COOL from 'core/COOL';

export default class LogPadConsoleLogView extends ConsoleLogView {
    /**
     * Render the view to the DOM.
     * @returns {Q.Promise<void>}
     */
    render () {
        return super.render();
    }

    /**
     * Navigates back to the settings view.
     */
    back () {
        Utilities.setLanguage(`${LF.Preferred.language}-${LF.Preferred.locale}`,
            LF.strings.getLanguageDirection());
        this.navigate('toolbox');
    }
}
COOL.add('LogpadConsoleLogView', LogPadConsoleLogView);
