import * as lStorage from 'core/lStorage';
import ELF from 'core/ELF';
import COOL from 'core/COOL';
import Users from 'core/collections/Users';
import Logger from 'core/Logger';
import CurrentContext from 'core/CurrentContext';
import UniversalLogin from 'core/classes/UniversalLogin';

let logger = new Logger('action/questionnaireScreenAuthenticationByRole');

/**
 * @memberOf ELF.actions/logpad
 * @method questionaireScreenAuthenticationByRole
 * @description
 * Execute the validation function attached to a widget if present.
 * @param {Object} param - contains the validation object as well as the answer
 * @param {Function} callback - Invoked to retain the action chain.
 * @returns {Q.Promise<(Object|undefined)>}
 * @example
 * ELF.rules.add({
 *     id: 'QuestionnaireScreenAuthenticationByRole',
 *     trigger: 'QUESTIONNAIRE:Before',
 *     resolve: [{ action: 'questionnaireScreenAuthenticationByRole' }]
 * }];
 */
export function questionnaireScreenAuthenticationByRole (param) {
    return Q.Promise((resolve) => {
        let screen = this.data.screens[this.screen],
            role = LF.security.activeUser ? LF.security.activeUser.get('role') : '',
            diaryLevelRoles = this.model.get('accessRoles') ||
                (this.questionnaireRoleOwner ? [this.questionnaireRoleOwner.get('role')] : [role]),
            screenLevelRoles = param.screenLevelRoles || screen.get('accessRoles') || diaryLevelRoles,
            universalLogin = new UniversalLogin({
                loginView: COOL.getClass('ContextSwitchingView', null),

                // DE16259 - "No role" popup not displayed.
                // Added a fallback to the questionnaire level configuration.
                noRoleFound: screen.get('noRoleFound') || this.model.get('noRoleFound'),
                subject: param.subject
            }),
            successfulLoginReRenderFunction = () => {
                let user = CurrentContext().get('user'),
                    roleOwners = typeof this.model.get('roleOwners') === 'undefined' ? true : this.model.get('roleOwners'),
                    allowOwner = roleOwners && (typeof roleOwners[user.get('role')] === 'undefined' ? true : roleOwners[user.get('role')]);

                if (allowOwner && !this.allRoleOwners[user.get('role')]) {
                    this.allRoleOwners[user.get('role')] = user;
                }

                if (this.questionnaireEntryUser.get('id') === user.get('id')) {
                    lStorage.setItem('isAuthorized', true);
                } else {
                    lStorage.removeItem('isAuthorized');
                }

                let screenToRender = _(this.screenStack).last();

                // DE16314
                // If there is no screen in the screen stack to render,
                // this is the first screen of the questionnaire, and we should reset the started attribute on the dashboard record.
                if (screenToRender == null) {
                    this.data.started = new Date();
                    this.data.dashboard.set('started', this.data.started.ISOStamp());

                    // Restart the questionnaire timeout once the context switch is complete.
                    LF.security.startQuestionnaireTimeOut();
                } else {
                    // Otherwise, we need to restart the paused questionnaire timeout.
                    LF.security.restartQuestionnaireTimeout();
                }

                return this.render(screenToRender)
                .then(() => this.delegateEvents());
            },
            filterUsers = (user) => {
                let roleOwner = this.allRoleOwners[user.get('role')];

                return Q.Promise((resolveUser) => {
                    if (screenLevelRoles.indexOf(user.get('role')) > -1) {
                        if (roleOwner) {
                            if (roleOwner.get('role') === user.get('role') && roleOwner.get('username') === user.get('username')) {
                                resolveUser(user);
                            } else {
                                resolveUser(false);
                            }
                        } else {
                            resolveUser(user);
                        }
                    } else {
                        resolveUser(false);
                    }
                });
            },
            sortUsers = (users) => {
                return Q.Promise((resolveUsers) => {
                    let sorted = (_.sortBy(users, (user) => {
                        // Its possible for a Sync'd user to be added that wont have a lastLogin time. A default date in added for sorting.
                        return user.get('lastLogin') ? user.get('lastLogin') : new Date(1986, 1, 21).toString();
                    })).reverse();

                    resolveUsers(sorted);
                });
            };

        this.allRoleOwners = this.allRoleOwners || {};

        if (!this.questionnaireRoleOwner) {
            if (diaryLevelRoles.indexOf(role) > -1) {
                Users.fetchCollection()
                .then(() => {
                    this.questionnaireRoleOwner = param.questionnaireRoleOwner || LF.security.activeUser;

                    // Affidavit was already configured before the role owner was determined. Remove that Affidavit and make sure the correct one is added.
                    // This could be changed so that QuestionnaireView.configureAffidavit() is not called until this point. Currently breaks Sitepad if done this way.
                    let screenAffidavitIndex = this.data.screens.indexOf(_.findWhere(this.data.screens, { id: 'AFFIDAVIT' }));
                    let questionAffidavit = this.data.questions.get('AFFIDAVIT');

                    if (questionAffidavit && screenAffidavitIndex > -1) {
                        this.data.screens.splice(screenAffidavitIndex, 1);
                        this.data.questions.remove(questionAffidavit, 1);

                        this.configureAffidavit();
                    } else if (!questionAffidavit && screenAffidavitIndex === -1) {
                        this.configureAffidavit();
                    } else {
                        logger.error('You have an affidavit screen or question, but not the other. Cannot configure affidavit correctly.');
                        return;
                    }

                    let affidavit = _.findWhere(this.data.screens, { id: 'AFFIDAVIT' });

                    if (affidavit) {
                        affidavit.accessRoles = this.questionnaireRoleOwner ? [this.questionnaireRoleOwner.get('role')] : undefined;
                    }
                })
                .done();
            }
        }

        let backArrow,
            forwardArrow;

        // If direction is not set, this is the displaying the first screen on entry of the diary. We should determine the user who entered the diary.
        if (!this.direction) {
            this.direction = 'forward';

            this.questionnaireEntryUser = LF.security.activeUser;
        }

        if (this.direction === 'forward') {
            backArrow = () => {
                this.previous();
            };
        } else {
            forwardArrow = () => {
                this.next();
            };
        }

        if (screenLevelRoles.indexOf(role) === -1) {
            // During a context switch, we pause the questionnaire timeout.
            LF.security.pauseQuestionnaireTimeout();

            universalLogin.newUserLogin({
                successfulLogin: successfulLoginReRenderFunction,
                filterUsers,
                sortUsers,
                backArrow,
                forwardArrow
            });
            resolve({ stopActions: true, preventDefault: true });
        } else {
            resolve();
        }
    });
}

ELF.action('questionnaireScreenAuthenticationByRole', questionnaireScreenAuthenticationByRole);

LF.Actions.questionnaireScreenAuthenticationByRole = questionnaireScreenAuthenticationByRole;
