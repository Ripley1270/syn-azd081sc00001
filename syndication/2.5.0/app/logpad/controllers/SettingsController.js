import BaseController from 'core/controllers/BaseController';
import COOL from 'core/COOL';
import AboutView from '../views/AboutView';
import ChangePasswordView from '../views/ChangePasswordView';
import ChangeTemporaryPasswordView from 'core/views/ChangeTemporaryPasswordView';
import PrivacyPolicyApplicationView from '../views/PrivacyPolicyApplicationView';
import ChangeSecretQuestionView from '../views/ChangeSecretQuestionView';
import ResetSecretQuestionView from 'core/views/ResetSecretQuestionView';
import ToolboxView from '../views/ToolboxView';
import SetAlarmsView from '../views/SetAlarmsView';
import SetTimeZoneView from 'core/views/SetTimeZoneView';
import SupportView from '../views/SupportView';
import Logger from 'core/Logger';
import CurrentSubject from 'core/classes/CurrentSubject';
import LogPadConsoleLogView from '../views/ConsoleLogView';

let logger = new Logger('SettingsController');

/**
 * Controller for LogPad App settings workflow.
 * @class RegistrationController
 * @extends BaseController
 */
export default class SettingsController extends BaseController {
    /**
     * Display the About Screen.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /settings/#about
     * @returns {Q.Promise<void>}
     */
    about (options) {
        this.clear();

        return this.authenticateThenGo('AboutView', AboutView, options)
        .catch(e => logger.error(e));
    }

    /**
     * Displays the change password view.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /settings/#change_password
     * @returns {Q.Promise<void>}
     */
    changePassword (options) {
        this.clear();

        return this.authenticateThenGo('ChangePasswordView', ChangePasswordView, options)
        .catch(e => logger.error(e));
    }

     /**
     * Displays the change password view.
     * @example /settings/#change_password
     * @returns {Q.Promise<void>}
     */
    changeTemporaryPassword (options) {
        this.clear();

        return this.authenticateThenGo('ChangeTemporaryPasswordView', ChangeTemporaryPasswordView, options)
        .catch(e => logger.error(e));
    }

    /**
     * Navigate to the console log view.
     */
    consoleLog () {
        this.authenticateThenGo('LogPadConsoleLogView', LogPadConsoleLogView)
            .catch(e => logger.error(e))
            .done();
    }

    /**
     * The route for a Privacy Policy.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /settings/#privacy_policy
     * @returns {Q.Promise<void>}
     **/
    privacyPolicy (options) {
        this.clear();

        return this.authenticateThenGo('PrivacyPolicyApplicationView', PrivacyPolicyApplicationView, options)
        .catch(e => logger.error(e));
    }

    /**
     * Display the change secret question view.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /settings/#change_secret_question
     * @returns {Q.Promise<void>}
     */
    changeSecretQuestion (options) {
        const go = opts => this.authenticateThenGo('ChangeSecretQuestionView', ChangeSecretQuestionView, opts);
        this.clear();

        return CurrentSubject.getSubject()
        .then(subject => go(_.extend({ subject }, options)))
        .catch(e => logger.error(e));
    }

    /**
     * Display the reset secret question view.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /settings/#reset_secret_question
     * @returns {Q.Promise<void>}
     */
    resetSecretQuestion (options) {
        const goToView = options.force || localStorage.getItem('Reset_Password_Secret_Question');
        const go = opts => this.go('ResetSecretQuestionView', ResetSecretQuestionView, opts);

        this.clear();

        if (goToView) {
            return CurrentSubject.getSubject()
            .then(subject => go(_.extend({ subject }, options)))
            .catch(e => logger.error(e));
        }

        return LF.security.logout(true);
    }

    /**
     * Display the ToolBox Screen.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /settings/#toolbox
     * @returns {Q.Promise<void>}
     */
    toolbox (options) {
        this.clear();

        return this.authenticateThenGo('ToolboxView', ToolboxView, options)
        .catch(e => logger.error(e));
    }

    /**
     * Display the customer support view.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /settings/#support
     * @returns {Q.Promise<void>}
     */
    support (options) {
        this.clear();

        return this.authenticateThenGo('SupportView', SupportView, options)
        .catch(e => logger.error(e));
    }

    /**
     * Display the set alarms view.
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /settings/#set_alarms
     * @returns {Q.Promise<void>}
     */
    setAlarms (options) {
        this.clear();

        return this.authenticateThenGo('SetAlarmsView', SetAlarmsView, options)
        .catch(e => logger.error(e));
    }

    /**
     * Displays the set time zone view
     * @param {Object} options - Parameters provided by Router.flash().
     * @example /settings/#set_time_zone
     * @returns {Q.Promise<void>}
     */
    setTimeZone (options) {
        this.clear();

        return this.authenticateThenGo('SetTimeZoneView', SetTimeZoneView, options)
        .catch(e => logger.error(e));
    }
}

COOL.add('SettingsController', SettingsController);
