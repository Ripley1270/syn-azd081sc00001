import ELF from 'core/ELF';

/**
 * Determines if the first site user data entered is valid.
 * @memberOf ELF.expressions/sitepad
 * @method isFirstSiteUserDataValid
 * @returns {Q.Promise<boolean>}
 * @example
 * evaluate: 'isFirstSiteUserDataValid'
 */
export function isFirstSiteUserDataValid () {
    return Q(this.isUnlockCodeValid && this.passwordsMatch() && this.validateScreens());
}

ELF.expression('isFirstSiteUserDataValid', isFirstSiteUserDataValid);
