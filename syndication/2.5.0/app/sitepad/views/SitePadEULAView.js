import EULAView from 'core/views/EULAView';
import Logger from 'core/Logger';
import COOL from 'core/COOL';
import State from 'sitepad/classes/State';
import WizardStepper from 'sitepad/views/WizardStepper';

const logger = new Logger('SitePadEULAView');

/*
 * View for EULA page, user's accepttance is required for activation.
 * @class SitePadEULAView
 * @extends EULAView
 */
export default class SitePadEULAView extends COOL.getClass('EULAView', EULAView) {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch prior to render.
         */
        this.templateStrings = _.extend(this.templateStrings, {
            help: 'HELP' });

        /**
         * @property {Object<string,string>} events - A list of DOM events.
         * @readonly
         */
        this.events = _.extend(this.events, {
            'click #help-btn': 'settings' });
    }

    /**
     * Navigate to the next view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    next (evt) {
        super.next(evt);

        logger.operational('EULA is accepted by the first site user.');
        return ELF.trigger('REGISTRATION:SkipOrDisplayFirstSiteUser', {}, this);
    }

    /**
     * Navigate back to the previous view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    back (evt) {
        super.back(evt);

        logger.operational('EULA is declined by the first site user.');

        State.set(State.states.setTimeZone);
        this.navigate('setTimeZone');
    }

    render () {
        return super.render()
        .then(() => {
            let stepper = new WizardStepper({ viewId: this.id });
            stepper.render()
            .then(() => {
                $('#content').prepend(stepper.el);
            });
        });
    }

    /**
     * Navigate to the settings screen
     */
    settings () {
        this.navigate('settingsInfo');
    }
}

COOL.add('SitePadEULAView', SitePadEULAView);
