import PageView from 'core/views/PageView';
import UnlockCode from 'core/classes/UnlockCode';
import WebService from 'core/classes/WebService';
import * as lStorage from 'core/lStorage';
import { Banner } from 'core/Notify';
import Sites from 'core/collections/Sites';
import COOL from 'core/COOL';
import State from 'sitepad/classes/State';
import * as CoreUtilities from 'core/utilities/coreUtilities';
import WizardStepper from 'sitepad/views/WizardStepper';

const _evt = { preventDefault: $.noop };

export default class UnlockCodeView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {string} template - A selector that points to the template for the view.
         * @readonly
         * @default '#unlock-code-tpl'
         */
        this.template = '#unlock-code-tpl';

        /**
         * @property {Object<string,string>} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #back': 'back',
            'click #next': 'next',
            'input #unlock-code': 'validate',
            'click #help-btn': 'settings'
        };

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            tagline: 'ENTER_UNLOCK_CODE',
            unlockCode: 'STARTUP_UNLOCK_CODE',
            enterUnlockCode: 'ENTER_UNLOCK_CODE',
            helpText: 'UNLOCK_CODE_HELP',
            site: 'SITE',
            back: 'BACK',
            next: 'NEXT',
            help: 'HELP'
        };

        /**
         * @property {WebService} service - An instance of the WebService.
         */
        this.service = COOL.new('WebService', WebService);
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'unlock-code-view'
     */
    get id () {
        return 'unlock-code-view';
    }

    /**
    * @property {Object<string,string>} selectors - A list of selectors to populate.
    */
    get selectors () {
        return {
            unlockCode: '#unlock-code',
            btnBack: 'button#back',
            btnNext: 'button#next',
            form: '#unlockCodeForm',
            content: '#content'
        };
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        let studyName = lStorage.getItem('studyDbName');

        return Sites.fetchFirstEntry()
        .then((site) => {
            /**
             * @property {Site} site The site the device is registered to.
             */
            this.site = site;

            /**
             * @property {String} unlockCode The valid unlock code the user must match.
             */
            this.unlockCode = UnlockCode.createStartupUnlockCode(this.site.get('siteCode'), studyName);
        });
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise<void>}
     */
    render () {
        return this.buildHTML({
            siteCode: this.site.get('siteCode')
        }, true)
        .then(() => this.initForm())
        .then(() => {
            let stepper = new WizardStepper({ viewId: this.id });
            stepper.render()
            .then(() => {
                this.$content.prepend(stepper.el);
            });
        })
        .then(() => CoreUtilities.removeSplashScreen());
    }

    /**
     * Prevents the form from being sumitted via traditional means.
     */
    initForm () {
        this.$form.submit((e) => {
            e.preventDefault();
            return false;
        });
    }

    /**
     * Compares the entered unlock code with the expected one
     * @param {string} unlockCode - The unlock code user entered.
     * @returns {boolean}
     */
    validateUnlockCode (unlockCode) {
        return unlockCode === this.unlockCode;
    }

    /**
     * Validate the entered Unlock Code.
     * @param {ChangeEvent} evt - A input ChangeEvent.
     */
    validate (evt = _evt) {
        let unlockCode = this.$unlockCode.val();

        evt.preventDefault();

        if (unlockCode.length) {
            if (this.validateUnlockCode(unlockCode)) {
                this.inputSuccess(this.$unlockCode);
                this.enableButton(this.$btnNext);
            } else {
                this.inputError(this.$unlockCode);
                this.disableButton(this.$btnNext);
            }
        } else {
            this.clearInputState(this.$unlockCode);
            this.disableButton(this.$btnNext);
        }
    }

    /**
     * Navigate back to the test connection view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    back (evt = _evt) {
        evt.preventDefault();
        State.set(State.states.testConnection);
        this.navigate('testConnection');
    }

    /**
     * Submit the form and navigate to the next view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    next (evt = _evt) {
        let localCode = this.$unlockCode.val();

        evt.preventDefault();

        if (this.validateUnlockCode(localCode)) {
            State.set(State.states.setTimeZone);
            this.navigate('setTimeZone');
        } else {
            // @TODO SCREENSHOT-SITEPAD
            Banner.show({
                text: 'Invalid Unlock Code',
                type: 'error'
            });
        }
    }

    /*
     * Navigate to the settings screen
     */
    settings () {
        this.navigate('settingsInfo');
    }
}

COOL.add('UnlockCodeView', UnlockCodeView);
