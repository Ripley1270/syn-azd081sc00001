import PageView from 'core/views/PageView';
import State from 'sitepad/classes/State';
import COOL from 'core/COOL';
import WizardStepper from 'sitepad/views/WizardStepper';
import Logger from 'core/Logger';

let logger = new Logger('TestConnectionView');
const _evt = { preventDefault: $.noop };

export default class TestConnectionView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {string} template - A selector that points to the template for the view.
         * @readonly
         * @default '#test-connection-tpl'
         */
        this.template = '#test-connection-tpl';

        /**
         * @property {Object<string,string>} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #back': 'back',
            'click #next': 'next',
            'click #connectionBtn': 'testConnection',
            'click #help-btn': 'settings'
        };

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate.
         */
        this.selectors = {
            btnBack: 'button#back',
            btnNext: 'button#next',
            content: '#content'
        };

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            help: 'HELP',
            tagline: 'TEST_CONNECTION',
            helpText: 'NETWORK_BODY',
            networkConnection: 'NETWORK_CONNECTION',
            next: 'NEXT',
            back: 'BACK'
        };
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'test-connection-view'
     */
    get id () {
        return 'test-connection-view';
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise<void>}
     */
    render () {
        return this.buildHTML(null, true)
        .then(() => {
            this.delegateEvents();
        })
        .then(() => {
            let stepper = new WizardStepper({ viewId: this.id });

            stepper.render()
            .then(() => {
                this.$content.prepend(stepper.el);
            });
        });
    }

    /**
     * Navigate back to the language selection view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    back (evt = _evt) {
        evt.preventDefault();
        State.set(State.states.languageSelection);
        this.navigate('languageSelection');
    }

    /**
     * Navigate to the next view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    next (evt) {
        evt && evt.preventDefault();
        State.set(State.states.locked);
        this.navigate('unlockCode');
    }

    /**
     * Performs the calls to check the device connection to its study
     */
    testConnection () {
        ELF.trigger('checkConnectionToStudy', {}, this).done();
    }

    /**
     * It shows the connection status on the button whether the device
     * is online or offline
     * @param status - boolean; true if device is online, otherwise false
     * @returns {Q.Promise<any>}
     */
    showConnectionStatus (status) {
        const stringsToFetch = {
            success: 'CONNECT_SUCCESS',
            fail: 'CONNECT_FAIL'
        };

        return this.i18n(stringsToFetch)
        .then((strings) => {
            if (status) {
                // we flush out the existing classes instead of checking for them, then set the one we want.
                logger.operational('Network connection status was checked. Result: Success');
                this.$('#connectionBtn').html(strings.success).removeClass('btn-primary btn-success btn-danger').addClass('btn-success');
            } else {
                logger.operational('Network connection status was checked. Result: Failed to connect.');
                this.$('#connectionBtn').html(strings.fail).removeClass('btn-primary btn-success btn-danger').addClass('btn-danger');
            }
        });
    }

    /**
     * Navigate to about page
     */
    settings () {
        this.navigate('settingsInfo');
    }
}

COOL.add('TestConnectionView', TestConnectionView);
