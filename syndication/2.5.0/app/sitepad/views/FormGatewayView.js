import PageView from 'core/views/PageView';
import Data from 'core/Data';
import ELF from 'core/ELF';
import Sites from 'core/collections/Sites';
import CurrentContext from 'core/CurrentContext';
import UserVisits from 'sitepad/collections/UserVisits';
import UserVisit from 'sitepad/models/UserVisit';
import Subject from 'core/models/Subject';
import FormTableView from 'sitepad/views/FormTableView';
import { MessageRepo } from 'core/Notify';
import VisitService from 'sitepad/classes/VisitService';
import * as DateTimeUtil from 'core/DateTimeUtil';
import { getCurrentProtocol } from 'core/Helpers';
import Logger from 'core/Logger';

let logger = new Logger('FormGatewayView');

/**
 * Displays available forms for a given Subject & Visit.
 * @class FormGatewayView
 * @extends PageView
 */
export default class FormGatewayView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {number} subjectId - The ID of the subject to display forms for.
         */
        this.subjectId = options.subject.get('id');

        /**
         * @property {Visit} visit - The target visit to display forms for.
         */
        this.visit = options.visit;

        LF.DynamicText.subject = options.subject;

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate.
         */
        this.selectors = {
            wrapper: '.table-wrapper',
            pendingTransmission: '#pending-transmission-count'
        };

        /**
         * @property {Object<string,string>} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #start-form': _.debounce(() => {
                this.start();
            }, 2000, true),
            'click #skip-form': _.debounce(() => {
                this.skip();
            }, 2000, true),
            'click #back': 'back',
            'click #transmit': 'transmit'
        };

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            available: 'AVAILABLE',
            notavailable: 'NOT_AVAILABLE',
            skipped: 'SKIPPED',
            completed: 'COMPLETED',
            back: 'BACK',
            assignedTo: 'ASSIGNED_TO',
            formName: 'FORM_NAME',
            header: 'APPLICATION_HEADER',
            help: 'HELP',
            menu: 'MENU',
            patient: 'PATIENT',
            settings: 'SETTINGS',
            site: 'SITE',
            siteConfirmHeader: 'HAND_DEVICE_SITE_TITLE',
            siteConfirmBody: 'HAND_DEVICE_SITE_BODY',
            skipForm: 'SKIP_FORM',
            startForm: 'START_FORM',
            status: 'STATUS',
            sync: 'SYNC',
            subjectConfirmHeader: 'HAND_DEVICE_SUBJECT_TITLE',
            subjectConfirmBody: 'HAND_DEVICE_SUBJECT_BODY',
            cancel: 'CANCEL',
            ok: 'OK',
            logout: 'LOGOUT',
            sponsor: 'SPONSOR',
            studyVersion: 'STUDY_VERSION',
            protocol: 'PROTOCOL'
        };
        this.checkVisitExpired = !options.disableCheckVisitExpired;
    }

    /**
     * The ID of the view.
     * @property {string} id
     * @readonly
     * @default 'begin-visit'
     */
    get id () {
        return 'begin-visit';
    }

    /**
     * A selector that points to the template for the view.
     * @property {string} template
     * @readonly
     * @default '#form-gateway-template'
     */
    get template () {
        return '#form-gateway-template';
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        let getSubject = () => {
            let subject = new Subject({ id: this.subjectId });

            return subject.fetch()
            .then(() => {
                if (Object.keys(subject.attributes).length > 1) {
                    return subject;
                }
                return undefined;
            });
        };

        return Q.all([Sites.fetchFirstEntry(), getSubject()])
        .spread((site, subject) => {
            this.site = site;
            this.subject = subject;
        });
    }

    /**
     * Calls the transmit rule for this view.
     * @returns {Q.Promise<void>}
     */
    triggerTransmitRule () {
        LF.security.pauseSessionTimeOut();

        return ELF.trigger('FORMGATEWAY:Transmit', { subject: this.subject }, this)
        .then((flags) => {
            if (flags && flags.preventDefault) {
                return Q();
            }

            LF.security.restartSessionTimeOut();

            return this.resolve()
            .then(() => this.render());
        });
    }

    /*
     * Handles transmission and sync operations
     * @returns {Q.Promise<void>}
     */
    transmit () {
        return this.triggerTransmitRule();
    }

    /**
     * Render the view to the DOM.
     * example this.render();
     * @returns {Q.Promise<void>}
     */
    render () {
        let user = LF.security.activeUser,
            subject = this.subject,
            role = CurrentContext().get('role');

        if (subject) {
            this.templateStrings.role = {
                namespace: 'STUDY',
                key: role.get('displayName')
            };

            return this.buildHTML({
                username: user.get('username'),
                patientInitials: subject.get('initials'),
                patientNumber: subject.get('subject_id'),
                siteCode: this.site.get('siteCode'),
                visitName: this.visit.get('name'),
                sponsorVal: LF.StudyDesign.clientName,
                protocolVal: getCurrentProtocol(subject),
                studyVersionVal: LF.StudyDesign.studyVersion,
                image: this.getImageIconUrl(user.get('role'))
            }, true)
            .then(() => {
                if (user.get('role') === 'subject') {
                    this.hideElement('#site-user-image,#site-user-info,#site-user-visits,.ellipsis-overflow');
                    this.showElement('#patient-image,#patient-info,#patient-visits');
                }
                return this.renderTable();
            })
            .then(() => this.getPendingReportCount())
            .then((count) => {
                this.$pendingTransmission.html(count);
            });
        }
        let { display, Dialog } = MessageRepo;

        return display(Dialog && (LF.security.activeUser.isSubjectUser() ? Dialog.SUBJECT_DELETED_PATIENT : Dialog.SUBJECT_DELETED))
        .finally(() => {
            LF.security.activeUser.isSubjectUser() ? this.navigate('login') : this.navigate('home');
        });
    }

    getImageIconUrl (role) {
        return role === 'subject' ? './media/images/patient.png' : './media/images/doctor.png';
    }

    /**
     * Render the form table to the view.
     * @param {Object} translations - Translated strings to pass into the table for display.
     * @returns {Q.Promise<void>}
     */
    renderTable (translations) {
        this.table = new FormTableView({
            rows: [{
                header: 'FORM_NAME',
                property: 'displayName'
            }, {
                header: 'ASSIGNED_TO',
                property: 'assignedTo'
            }, {
                header: 'STATUS',
                property: 'status'
            }],
            translations,
            subject: this.subject,
            visit: this.visit
        });

        this.listenTo(this.table, 'select', this.select);
        this.listenTo(this.table, 'deselect', this.deselect);
        this.listenTo(this.table, 'noFormsAvailable', this.noFormsAvailable);
        this.listenTo(this.table, 'renderRows', this.checkVisitStatus);

        return this.table.render()
        .then(() => {
            this.$wrapper.append(this.table.$el);

            // While we care about the table rendering to the DOM in the returned promise,
            // the table's rows should populate individually.
            this.table.fetch()
            .done();
        });
    }

    /**
     * Checks Visit status and displays a proper message to the active user if visit is Completed
     * or Inactive after sync process (Skipped or Incompleted on server side).
     * Also, navigation is handled.
     * @returns {Q.Promise<void>}
     */
    checkVisitStatus () {
        return UserVisits.fetchCollection()
        .then((userVisits) => {
            return userVisits.findWhere({
                visitId: this.visit.id,
                subjectKrpt: this.subject.get('krpt')
            });
        })
        .then((userVisit) => {
            let visitState = userVisit ? userVisit.get('state') : undefined,
                expired = userVisit ? userVisit.get('expired') : 'false';
            if (visitState && (visitState === LF.VisitStates.COMPLETED ||
                visitState === LF.VisitStates.INCOMPLETE ||
                visitState === LF.VisitStates.SKIPPED)) {
                let msgKey = visitState === LF.VisitStates.INCOMPLETE || visitState === LF.VisitStates.SKIPPED ? 'VISIT_INACTIVE' : 'VISIT_COMPLETED';

                if (LF.security.activeUser.isSubjectUser()) {
                    msgKey = visitState === LF.VisitStates.COMPLETED ? 'VISIT_COMPLETED_SUBJECT' : 'VISIT_INACTIVE_SUBJECT';
                }

                if (expired === 'true') {
                    msgKey = LF.security.activeUser.isSubjectUser() ? 'VISIT_EXPIRED_SUBJECT' : 'VISIT_EXPIRED';
                }

                return this.displayMessageAndNavigate(msgKey, expired);
            }
            if (!this.checkVisitExpired || !this.visit.get('expiration')) {
                this.checkVisitExpired = true;
                return Q(false);
            }
            return VisitService.isVisitExpired(this.visit, userVisit, this.subject)
            .then((visitExpired) => {
                if (!visitExpired) {
                    return Q(false);
                }
                let currentDate = new Date();
                if (userVisit == null) {
                    // eslint-disable-next-line no-param-reassign
                    userVisit = new UserVisit();
                    userVisit.set({
                        // eslint-disable-next-line new-cap
                        dateStarted: currentDate.ISOLocalTZStamp()
                    });
                }
                userVisit.set({
                    visitId: this.visit.get('id'),
                    studyEventId: this.visit.get('studyEventId'),
                    subjectKrpt: this.subject.get('krpt'),
                    state: LF.VisitStates.INCOMPLETE,
                    dateModified: DateTimeUtil.timeStamp(currentDate),
                    expired: 'true'
                });

                return userVisit.save()
                .then(() => {
                    let visitStateCode = 2;

                    return VisitService.createEndVisitReport({
                        visitStateCode,
                        visit: this.visit,
                        skippedReason: '0',
                        visitStartDate: userVisit.get('dateStarted'),
                        subject: this.subject,
                        ink: null,
                        visitExpired: true
                    });
                })
                .then(res => userVisit.save({
                    visitEndSigId: res.sig_id,
                    dateEnded: res.eventEndDate
                }))
                .then(() => {
                    let dialogKey = LF.security.activeUser.isSubjectUser() ? 'VISIT_EXPIRED_SUBJECT' : 'VISIT_EXPIRED';
                    return this.displayMessageAndNavigate(dialogKey);
                });
            });
        });
    }

    /**
     * Displays message and navigates away.
     * @param {string} messageKey - key that identifies MessageRepo.Dialog
     * @param {string} visitExpired - true if current visit expired
     * @returns {Q.Promise<void>}
     */
    displayMessageAndNavigate (messageKey, visitExpired = 'false') {
        if (visitExpired === 'true') {
            logger.operational(`Visit with id = "${this.visit.id}" expired for subject ${this.subject.get('subject_id')}. Message displayed to the user: ${LF.security.activeUser.get('username')}.`);
        }

        return MessageRepo.display(MessageRepo.Dialog && MessageRepo.Dialog[messageKey])

        // We don't care if the notification is resolved or rejected, we will still navigate away.
        .finally(() => {
            if (LF.security.activeUser.isSubjectUser()) {
                this.navigateToDashboard();
            } else {
                this.navigate(`visits/${this.subject.get('id')}`, true, { visit: this.visit });
            }
        });
    }

    /**
     * Triggered when a form row is selected.
     * @param {FormRow} formRow - The selected FormRow.
     */
    select (formRow) {
        this.selected = formRow;

        // If the selected questionnaire is available, enable the start and skip buttons.
        if (formRow.get('status') === 'available') {
            this.enableButton('#start-form, #skip-form');
        } else {
            // Otherwise, disable them.
            this.deselect();
        }
    }

    /**
     * Start a form.
     * @returns {Q.Promise<void>}
     * @example let request = this.start();
     */
    start () {
        let id = this.selected.get('id');

        // Trigger an OpenQuestionnaire event via ELF.
        return ELF.trigger(`DASHBOARD:OpenQuestionnaire/${id}`, { questionnaire: this.selected }, this)
        .then((res) => {
            // If not told to prevent default behavior, open the questionnaire.
            res.preventDefault || this.openQuestionnaire(id);
        });
    }

    /**
     * Skip a form.
     * @example this.skip();
     */
    skip () {
        // TODO: skip a form
    }

    /**
     * Open a form.
     * @param {string} questionnaireId - ID of the questionnaire.
     * @returns {Q.Promise<void>}
     */
    openQuestionnaire (questionnaireId) {
        let id = questionnaireId || this.selected.get('id');

        Data.Questionnaire = {};

        // DE12483: Delay opening questionnaire to prevent the issue of questionnaire screen display
        return Q.delay(150)
        .then(() => this.navigate(`questionnaire/${id}`, true, {
            subject: this.subject,
            visit: this.visit
        }));
    }

    /**
     * Triggered when the form is deselected.
     * @example this.deselect();
     */
    deselect () {
        delete this.selected;

        this.disableButton('#start-form, #skip-form');
    }

    /**
     * Navigates back.
     * @example this.back();
     */
    back () {
        if (LF.security.activeUser.isSubjectUser()) {
            this.navigateToDashboard();
        } else {
            this.navigate(`visits/${this.subject.get('id')}`, true, { visit: this.visit });
        }
    }

    /**
     * Checks if all reports are completed.
     * @returns {Q.Promise<void>}
     * @example this.noFormsAvailable();
     */
    noFormsAvailable () {
        let { display, Dialog } = MessageRepo;

        return display(Dialog && Dialog.VISIT_COMPLETED_SUBJECT)
        .then(() => this.navigateToDashboard());
    }

    /**
     * This function is navigating to the dashboard, but saying you want the roles associated with the permission of
     * diaryBackoutRoles, to be the next set of Roles you login in with. We should fix this when we do permissions.
     */
    navigateToDashboard () {
        this.navigate('dashboard/diaryBackoutRoles', true, {
            subject: this.subject,
            visit: this.visit
        });
    }
}
