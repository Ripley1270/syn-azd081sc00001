import EULA from 'core/classes/EULA';
import Users from 'core/collections/Users';

/*
 * @class WizardStepper
 * @description View for displaying wizard stepper during sitepad registration
 * @extends Backbone.View
 */
export default class WizardStepper extends Backbone.View {
    constructor (options) {
        super(options);
        this.stepperOptions = options;
        this.templateId = 'DEFAULT:SitepadRegistrationWizardStepper';

        this.templateStrings = {
            language: 'LANGUAGE_LABEL',
            network: 'NETWORK',
            unlockCode: 'STARTUP_UNLOCK_CODE',
            timeZone: 'TIME_ZONE_LABEL',
            eula: 'END_USER_LICENSE_LABEL',
            addUser: 'ADD_USER',
            summary: 'SUMMARY'
        };
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'wizard-stepper-view'
     */
    get id () {
        return 'wizard-stepper-view';
    }

    /**
     * Renders stepper view from template.
     * @returns {Q.Promise<void>}
     */
    render () {
        return LF.strings.display(this.templateStrings)
        .then((translated) => {
            let stepperTemplateHtml = LF.templates.display(this.templateId, translated);
            this.$el.append(stepperTemplateHtml);
            if (this.stepperOptions && this.stepperOptions.viewId) {
                this.$el.find(this.getStepperElementFromViewId(this.stepperOptions.viewId)).addClass('-es-active');
                if (this.stepperOptions.viewId === 'summary-screen-view') {
                    this.$el.find('li:not(:has(>div#summary-step))').addClass('-es-disabled');
                }
            }

            if (EULA.isAccepted()) {
                this.$el.find('#eula-step').parent().addClass('-es-disabled');
            }
        });
    }

    /**
     * Maps view with stepper div element.
     * @param {String} viewId - view's ID.
     * @returns {String} stepper div element id.
     */
    getStepperElementFromViewId (viewId) {
        switch (viewId) {
            case 'language-selection-view':
                return '#language-step';
            case 'test-connection-view':
                return '#network-step';
            case 'unlock-code-view':
                return '#unlock-step';
            case 'set-tz-activation-view':
                return '#time-zone-step';
            case 'end-user-license-view':
                return '#eula-step';
            case 'First_Site_User':
                return '#add-user-step';
            case 'summary-screen-view':
                return '#summary-step';
            default:
                return '';
        }
    }

    /**
     * Checks if admin user is existing already.
     * @returns {Q.Promise<void>}
     */
    hasAdminUser () {
        let adminFilter = (user) => {
                return user.get('role') === 'admin' && user.get('active') === 1;
            },
            users = new Users();

        return users.fetch()
        .then(() => {
            let filterFunction = LF.StudyDesign.sitePad.adminUserFilter || adminFilter,
                adminUsers = users.filter(filterFunction);

            return adminUsers.length > 0;
        });
    }
}
