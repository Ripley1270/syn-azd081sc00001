import PageView from 'core/views/PageView';
import * as lStorage from 'core/lStorage';
import State from 'sitepad/classes/State';
import COOL from 'core/COOL';
import CurrentContext from 'core/CurrentContext';
import * as Utilities from 'core/utilities';
import WizardStepper from 'sitepad/views/WizardStepper';

export default class LanguageSelectionView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {string} template - A selector that points to the template for the view.
         * @readonly
         * @default '#language-selection-tpl'
         */
        this.template = '#language-selection-tpl';

        /**
         * @property {Object<string,string>} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #next': 'next',
            'change #language': 'changeLanguage',
            'click #help-btn': 'settings'
        };

        /**
         * @property {Object<string,string>} selectors - A list of selectors to populate.
         */
        this.selectors = {
            language: '#language',
            content: '#content'
        };

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            help: 'HELP',
            language: 'LANGUAGE',
            tagline: 'SELECT_LANGUAGE',
            next: 'NEXT'
        };
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'language-selection-view'
     */
    get id () {
        return 'language-selection-view';
    }

    /**
     * @property {string} id of the template for the language select options
     * @readonly
     * @default '#language-login-option'
     */
    get languageSelectionTemplate () {
        return '#language-login-option';
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise.<void>}
     */
    render () {
        return this.buildHTML(null, true)
        .then(() => {
            this.delegateEvents();
            this.$language.select2({
                escapeMarkup: markup => markup,
                data: this.createLanguageSelectionData(),
                templateSelection: item => this.getTemplate(this.languageSelectionTemplate)({ item }),
                templateResult: item => this.getTemplate(this.languageSelectionTemplate)({ item }),
                minimumResultsForSearch: Infinity,

                // Defer width of select2 in favor of the CSS preferences for this element.
                width: ''
            });
        })
        .then(() => {
            let stepper = new WizardStepper({ viewId: this.id });
            stepper.render()
            .then(() => {
                this.$content.prepend(stepper.el);
            });
        })
        .then(() => Utilities.removeSplashScreen());
    }

    /**
     * Creates and returns language data
     * @return {Array} data to populate the language dropdown list
     */
    createLanguageSelectionData () {
        let localized = LF.strings.match({ namespace: 'CORE', localized: {} }),
            langs = [];

        // get the localized names from the resource string files in order of 'language' and 'locale' keys
        localized.sort((firstComparatorObject, secondComparatorObject) => {
            let lang1 = firstComparatorObject.get('language'),
                lang2 = secondComparatorObject.get('language'),
                locale1 = firstComparatorObject.get('locale'),
                locale2 = secondComparatorObject.get('locale');
            if (lang1 > lang2) {
                return 1;
            } else if (lang1 < lang2) {
                return -1;
            }
            return locale1 > locale2 ? 1 : -1;
        }).forEach((language) => {
            let langCode = `${language.get('language')}-${language.get('locale')}`,
                lang = {
                    id: langCode,
                    localized: language.get('localized'),
                    fontFamily: Utilities.getFontFamily(langCode),
                    dir: language.get('direction'),
                    cssClass: language.get('direction') === 'rtl' ? 'right-direction text-right-absolute' : 'left-direction text-left-absolute',
                    selected: language.get('language') === LF.Preferred.language && language.get('locale') === LF.Preferred.locale
                };

            langs.push(lang);
        });

        return langs;
    }

    /**
     * Handle a language change event.
     * @param {ChangeEvent} evt - An input change event.
     * @returns {Q.Promise<void>}
     */
    changeLanguage (evt) {
        let value = this.$language.val();

        evt && evt.preventDefault();

        return CurrentContext().setContextLanguage(value)
        .then(() => {
            this.render();
        });
    }

    /**
     * Navigate to the next view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    next (evt) {
        let mode = lStorage.getItem('mode'),
            value = this.$language.val(),
            split = value.split('-'),
            lang = split[0],
            locale = split[1],
            state,
            screenName;

        evt && evt.preventDefault();

        lStorage.setItem('language', lang);
        lStorage.setItem('locale', locale);

        switch (mode) {
            case 'provision':
            case 'trainer':
                state = State.states.testConnection;
                screenName = 'testConnection';
                break;
            default:
                state = State.states.siteSelection;
                screenName = 'siteSelection';
        }

        State.set(state);
        this.navigate(screenName);
    }

    /*
     * Navigate to the settings screen
     */
    settings () {
        this.navigate('settingsInfo');
    }
}

COOL.add('LanguageSelectionView', LanguageSelectionView);
