import PageView from 'core/views/PageView';
import Users from 'core/collections/Users';
import * as lStorage from 'core/lStorage';
import State from 'sitepad/classes/State';
import Logger from 'core/Logger';
import COOL from 'core/COOL';
import { isAdminUser } from '../actions/skipOrDisplayFirstSiteUser';
import WizardStepper from 'sitepad/views/WizardStepper';
import { getSiteSupportNumberWithBreaks } from 'core/dynamicText/getSiteSupportNumber';
import DeviceSetupSyncStatus from 'sitepad/classes/DeviceSetupSyncStatus';

let logger = new Logger('SummaryScreenView');

const _evt = { preventDefault: $.noop };

/**
 * The class used to display Summary Screen during sitepad registration
 * @class SummaryScreenView
 * @extends PageView
 */
export default class SummaryScreenView extends PageView {
    /**
     * Constructs a Summary Screen View
     * @param {Backbone.ViewOptions} options The options used to construct the class.
     */
    constructor (options) {
        super(options);

        /**
         * @property {Object} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #next': 'nextHandler',
            'click #help-btn': 'settings'
        };

        /**
         * @property {Object.<string, string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: 'FINALIZE_DEVICE_SETUP',
            networkConnection: 'NETWORK_CONNECTION',
            dataTransmission: 'DATA_TRANSMISSION',
            language: 'LANGUAGE_LABEL',
            timeZone: 'TIME_ZONE_LABEL',
            siteAdministrator: 'SITE_ADMINISTRATOR',
            finalize: 'FINALIZE',
            callServiceProvider: 'CALL_SERVICE_PROVIDER',
            callSupport: 'CALL_SUPPORT',
            help: 'HELP'
        };
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return Users.fetchCollection()
        .then((users) => {
            this.users = users;
            if (DeviceSetupSyncStatus.internetConnected === undefined || DeviceSetupSyncStatus.dataTransmitted === undefined) {
                return DeviceSetupSyncStatus.syncStatusHandler();
            }
            return Q();
        })
        .then(() => {
            let faviconOnline = '<div class="col-sm-12"><span class="fa fa-check-circle"></span></div>',
                faviconOffline = '<span class="fa fa-times-circle"></span>',
                faviconNoCheck = '<div class="col-sm-12"><span class="fa fa-minus-square"></span></div>';
            if (DeviceSetupSyncStatus.internetConnected) {
                logger.info('Network Connection is Online');
                this.networkConectionString = faviconOnline;
                if (DeviceSetupSyncStatus.dataTransmitted) {
                    logger.info('Data Trasmission is successful');
                    this.dataTransmissionString = faviconOnline;
                    return Q();
                }
                logger.info('Data Trasmission was not successful');
                return getSiteSupportNumberWithBreaks.evaluate()
                .then((numberString) => {
                    return this.i18n(this.templateStrings.callSupport)
                    .then((translated) => {
                        this.dataTransmissionString = `<div class='col-sm-5'>${faviconOffline}${translated}</div>
                        <div class='col-sm-7'>${numberString}</div>`;
                    });
                });
            }
            logger.info('Network Connection is Offline');
            return this.i18n(this.templateStrings.callServiceProvider)
            .then((translated) => {
                this.networkConectionString = `<div class='col-sm-12'>${faviconOffline} ${translated}</div>`;
                this.dataTransmissionString = faviconNoCheck;
            });
        })
        .then(() => super.resolve());
    }

    /**
     * The ID of the view.
     * @property {string} id
     * @readonly
     * @default 'summary-screen-view'
     */
    get id () {
        return 'summary-screen-view';
    }

    /**
     * A selector that points to the template for the view.
     * @property {string} template
     * @readonly
     * @default '#summary-screen-tpl'
     */
    get template () {
        return '#summary-screen-tpl';
    }

    /**
     * Render the view.
     * @returns {Q.Promise<void>}
     * @example
     * this.render()
     * .then({ ... });
     */
    render () {
        let language = LF.strings.findWhere({
                language: lStorage.getItem('language'),
                locale: lStorage.getItem('locale')
            }),
            filterFunction = LF.StudyDesign.sitePad.adminUserFilter || isAdminUser,
            adminUsers = this.users.filter(filterFunction),
            adminUsernames = adminUsers.map((user, index) => {
                if (index === 0) {
                    return user.get('username');
                }
                return ` ${user.get('username')}`;
            }).toString();

        return this.buildHTML({
            networkConnectionVal: this.networkConectionString,
            dataTransmissionVal: this.dataTransmissionString,
            languageVal: language.get('localized'),
            timeZoneVal: lStorage.getItem('selectedTZName'),
            siteAdministratorVal: adminUsernames
        }, true)
        .then(() => {
            let stepper = new WizardStepper({ viewId: this.id });
            stepper.render()
            .then(() => {
                this.$('#content').prepend(stepper.el);
            });
        });
    }

    /**
     * Click event handler for the Next button.
     * @param {(MouseEvent|TouchEvent)} [evt=_evt] - A mouse or touch event.
     * @returns {Q.Promise<void>}
     * @example
     * this.events = { 'click #next': 'nextHandler' };
     */
    nextHandler (evt = _evt) {
        evt.preventDefault();

        return Q()
        .then(() => {
            logger.info('Navigating to Login');
            State.set(State.states.activated);
            lStorage.removeItem('selectedTZName');
            this.navigate('login');
        });
    }

    /**
     * Navigate to the settings screen
     */
    settings () {
        this.navigate('settingsInfo');
    }
}

COOL.add('SummaryScreenView', SummaryScreenView);
