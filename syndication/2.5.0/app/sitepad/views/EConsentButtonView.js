import ELF from 'core/ELF';
import * as utils from 'core/utilities';

/**
 * Button used to launch the eConsent window.
 * @class EConsentButtonView
 * @extends Backbone.View
 */
export default class EConsentButtonView extends Backbone.View {
    constructor (options) {
        super(options);

        /**
         * @property {Object} config - The eConsent configuration.
         */
        this.config = utils.getNested('StudyDesign.eConsent', LF);
    }

    /**
     * @property {string} events - A id attribute of the view.
     */
    get id () {
        return 'econsent-btn';
    }

    /**
     * @property {string} tagName - The HTML tag used to construct the view.
     */
    get tagName () {
        return 'button';
    }

    /**
     * @property {string} className - The class of the attribute of the view.
     */
    get className () {
        return 'list-group-item no-action';
    }

    /**
     * @property {Object<string,string>} events - A list of events to bind to the view.
     */
    get events () {
        return { click: 'clickHandler' };
    }

    /**
     * An alias for LF.strings.display. See {@link core/collections/Languages Languages} for details.
     * @param {object|string} strings The translations keys to translate.
     * @returns {Q.Promise}
     */
    i18n (strings) {
        return LF.strings.display(strings);
    }

    /**
     * Render the button.
     * @return {Q.Promise<void>}
     */
    render () {
        return this.i18n({ text: 'ECONSENT' })
        .then(({ text }) => {
            let template = `<span>
                ${this.config.buttonIcon || '<i class="fa fa-check-square-o"></i>'}
                ${text}
            </span>`;

            this.$el.html(template);
        });
    }

    /**
     * Click event handler for the eConsent button
     * @param {(MouseEvent|TouchEvent)} [evt] - A click event object.
     * @example
     * this.clickHandler(evt);
     */
    clickHandler (evt) {
        evt.preventDefault();

        this.click()
        .done();
    }

    /**
     * Opens the external eConsent window.
     * @returns {Q.Promise<void>}
     */
    open () {
        return Q.Promise((resolve) => {
            const execWhenWrapped = () => {
                cordova.InAppBrowser.open(this.config.url, '_blank', 'location=no,EnableViewPortScale=yes');

                resolve();
            };

            const execWhenNotWrapped = () => {
                window.open(this.config.url, 'ECONSENT', 'location=true,resizable=true,scrollbars=true,status=true');

                resolve();
            };

            LF.Wrapper.exec({ execWhenWrapped, execWhenNotWrapped });
        });
    }

    /**
     * Trigger an event to open eConsent.
     * @returns {Q.Promise<void>}
     */
    click () {
        return ELF.trigger('ECONSENT:Open', {}, this);
    }
}
