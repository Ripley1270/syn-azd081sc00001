import SupportBaseView from 'core/views/SupportBaseView';

export default class CustomerSupportView extends SupportBaseView {
    constructor (options) {
        super(options);

        /**
         * @property {string} id - The ID of template to render.
         * @readonly
         * @default '#customer-support-template'
         */
        this.template = '#customer-support-template';
    }

    /**
    * @property {string} id - The ID of the view.
    * @readonly
    * @default 'customer-support'
    */
    get id () {
        return 'customer-support';
    }

    /**
     * Navigates back to the settings view.
     */
    back () {
        this.navigate('settings');
    }
}
