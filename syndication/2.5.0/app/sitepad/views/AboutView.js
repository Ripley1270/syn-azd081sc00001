import PageView from 'core/views/PageView';
import InfoView from 'sitepad/views/InfoView';

/**
 * Backing class for the about view
 */
export default class AboutView extends PageView {
    /**
     * Constructor
     * @param {Object} options - The view options
     */
    constructor (options) {
        super(options);

        /**
         * @property {string} template - A selector that points to the template for the view.
         * @readonly
         * @default '#about-tpl'
         */
        this.template = '#about-tpl';

        /**
         * @property {Object} selectors - A list of DOM selectors
         */
        this.selectors = {
            btnVisit: '#btn-visit',
            btnDcr: '#btn-dcr',
            network: '#network-parent'
        };

        /**
         * @property {Object} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #back': 'back',
            'click #btn-add-patient': 'addPatient',
            'click #btn-add-user': 'addUser',
            'click #btn-device-language': 'changeDeviceLanguage',
            'click #btn-patient-language': 'goHome',
            'click #btn-timezone': 'setTimezone',
            'click #btn-dcr': 'goToGSSO',
            'click #btn-visit': 'goToGSSO'
        };

        /**
         * @property {Object} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            title: 'HELP',
            back: 'BACK',
            workflowHelp: 'WORKFLOW_HELP',
            subjectHeader: 'SUBJECT_HEADER',
            subjectBody: 'SUBJECT_BODY',
            addPatient: 'ADD_PATIENT_BTN',
            userHeader: 'USER_HEADER',
            userBody: 'USER_BODY',
            addUser: 'ADD_USER_BTN',
            deviceHelp: 'DEVICE_HELP',
            deviceLangHeader: 'DEVICE_LANG_HEADER',
            deviceLangBody: 'DEVICE_LANG_BODY',
            editDeviceLang: 'EDIT_DEVICE_LANG_BTN',
            patientLangHeader: 'PATIENT_LANG_HEADER',
            patientLangBody: 'PATIENT_LANG_BODY',
            editPatientLang: 'EDIT_PATIENT_LANG',
            timezoneHeader: 'TIMEZONE_HEADER',
            timezoneBody: 'TIMEZONE_BODY',
            timezoneBtn: 'TIMEZONE_BTN',
            networkHeader: 'NETWORK_HEADER',
            networkBody: 'NETWORK_BODY',
            visitHeader: 'VISIT_HEADER',
            visitBody: 'VISIT_BODY',
            visitBtn: 'VISIT_BTN',
            dcrHeader: 'DCR_HEADER',
            dcrBody: 'DCR_BODY',
            dcrBtn: 'DCR_BTN'
        };
    }

    /**
     * Returns the ID of the view
     * @returns {string}
     */
    get id () {
        return 'about';
    }

    /**
     * Create instance of InfoView (or inherited class) to use with this control.
     * Default is to use normal InfoView.
     * @returns {InfoView} instance of InfoFiew
     */
    constructInfoView () {
        return new InfoView();
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise<void>}
     */
    render () {
        let infoView = this.constructInfoView();

        return this.buildHTML({}, true)
        .then(() => {
            return infoView.render();
        })
        .then(() => {
            this.$('.info-container').append(infoView.$el);
        })
        .then(() => {
            this.$btnVisit.addClass('hidden');
            this.$btnDcr.addClass('hidden');
        });
    }

    /**
     * Navigate back to previous page.
     */
    back () {
        Backbone.history.history.back();
    }

    /**
     * Navigate to the home view (manage patients)
     */
    addPatient () {
        this.navigate('home');
    }

    /**
     * Navigate to the home view to manage site users
     */
    addUser () {
        this.navigate('home');
    }

    /**
     * Navigate to the change language view.
     */
    changeDeviceLanguage () {
        this.navigate('site-users');
    }

    /**
     * Navigate to the home view to facilitate
     */
    goHome () {
        this.navigate('home');
    }

    /**
     * Navigates to the set time zone view.
     */
    setTimezone () {
        this.navigate('set-time-zone');
    }

    /**
     * This should be overriden in the web modality.
     */
    goToGSSO () {
        return null;
    }
}
