import PageView from 'core/views/PageView';
import Sites from 'core/collections/Sites';
import Transmissions from 'core/collections/Transmissions';
import Logger from 'core/Logger';
import ELF from 'core/ELF';
import UserTableView from 'sitepad/views/UserTableView';
import { getCurrentProtocol } from 'core/Helpers';

// eslint-disable-next-line no-unused-vars
let logger = new Logger('sitepad/views/SiteUsersView');

/**
 * Manage SitePad users.
 * @class SiteUsersView
 * @extends PageView
 */
export default class SiteUsersView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {Object} events A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #transmit': 'transmit',
            'click #back': 'goBack',
            'click #edit-user-btn': 'editUser',
            'click #add-site-user-btn': 'addNewUser',
            'click #deactivate-user-btn': 'deactivate',
            'click #activate-user-btn': 'activate'
        };

        /**
         * @property {Object} templateStrings A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: 'MANAGE_USERS',
            back: 'BACK',
            site: 'SITE',
            usernameHeader: 'USERNAME',
            name: 'NAME',
            editUser: 'EDIT_USER',
            active: 'ACTIVE',
            activateUser: 'ACTIVATE_USER',
            role: 'ROLES',
            lastLogin: 'LAST_LOGIN',
            deactivated: 'DEACTIVATED',
            deactivateUser: 'DEACTIVATE_USER',
            addUser: 'ADD_USER',
            status: 'STATUS',
            sponsor: 'SPONSOR',
            studyVersion: 'STUDY_VERSION',
            protocol: 'PROTOCOL'
        };
    }

    /**
     * The ID of the view.
     * @property {string} id
     * @readonly
     * @default 'site-users-view'
     */
    get id () {
        return 'site-users-view';
    }

    /**
     * A selector that points to the template for the view.
     * @property {string} template
     * @readonly
     * @default '#site-users-tpl'
     */
    get template () {
        return '#site-users-tpl';
    }

    /**
     * A list of selectors to populate.
     * @property {Object} A list of selectors to populate.
     */
    get selectors () {
        return {
            activate: '#activate-user-btn',
            add: '#add-site-user-btn',
            deactivate: '#deactivate-user-btn',
            edit: '#edit-user-btn',
            sync: '#transmit',
            wrapper: '.table-wrapper',
            pendingTransmission: '#pending-transmission-count'
        };
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return Q.all([Sites.fetchFirstEntry(), Transmissions.fetchCollection()])
        .spread((site, transmissions) => {
            this.site = site;
            this.transmissions = transmissions;
        });
    }

    /**
     * Render the view to the DOM.
     * @example this.render();
     * @returns {Q.Promise<void>}
     */
    render () {
        let user = LF.security.activeUser,
            role = LF.security.getUserRole();

        this.templateStrings.role = role.get('displayName');

        return this.buildHTML({
            username: user.get('username'),
            siteCode: this.site.get('siteCode'),
            sponsorVal: LF.StudyDesign.clientName,
            protocolVal: getCurrentProtocol(),
            studyVersionVal: LF.StudyDesign.studyVersion
        }, true)
        .then((translations) => {
            this.table = new UserTableView({
                rows: [{
                    header: 'USERNAME',
                    property: 'username',
                    sortable: true,
                    comparator: (model) => {
                        return model.get('username').toLowerCase();
                    }
                }, {
                    header: 'ROLES',
                    property: 'role',
                    sortable: true,

                    // roleText is the translated displayText of the User's role model.
                    // This value is set on the UserRowView after translation is complete.
                    comparator: 'roleText'
                }, {
                    header: 'LAST_LOGIN',
                    property: 'lastLogin',
                    sortable: true,
                    comparator: (model) => {
                        if (model.get('lastLogin')) {
                            return new Date(model.get('lastLogin')).getTime().toString();
                        }
                        return '0';
                    }
                }, {
                    header: 'STATUS',
                    property: 'active',
                    sortable: true,
                    comparator: 'statusText'
                }],
                filter: (collection) => {
                    return collection.filter((model) => {
                        return !model.isSubjectUser() && !!LF.StudyDesign.roles.find({ id: model.get('role') });
                    });
                },
                translations
            });

            this.listenTo(this.table, 'select', this.select);
            this.listenTo(this.table, 'deselect', this.deselect);

            this.table.render()
            .then(() => {
                this.$wrapper && this.$wrapper.append(this.table.$el);
                this.table.fetch();
            })
            .then(() => this.getPendingReportCount())
            .then((count) => {
                this.$pendingTransmission.html(count);
                if (count > 0) {
                    this.enableButton(this.$sync);
                }
            });
        });
    }

    /**
     * Select a user.
     * @param {User} user The user to select.
     * @example this.select(user);
     */
    select (user) {
        this.selected = user;

        // If the user is active...
        if (user.get('active') === 1) {
            // If the selected user is not the same as the logged in user,
            // enable the deactivate button.
            if (user.get('id') !== LF.security.activeUser.get('id')) {
                this.enableButton(this.$edit);
                if (!user.get('isDuplicate')) {
                    this.enableButton(this.$deactivate);
                } else {
                    this.disableButton(this.$deactivate);
                }
            } else {
                this.disableButton(this.$edit);
                this.disableButton(this.$deactivate);
            }

            // Disable the Activate User button.
            this.disableButton(this.$activate);

        // If the user is deactivated...
        } else {
            // Disable the Edit User and Deactivate User buttons.
            this.disableButton(this.$edit);
            this.disableButton(this.$deactivate);

            // Enable the Activate User button.
            this.enableButton(this.$activate);
        }
    }

    /**
     * Calls the transmit rule for this view.
     * @returns {Q.Promise<void>}
     */
    triggerTransmitRule () {
        return ELF.trigger('USERMANAGEMENT:Transmit', {}, this)
        .finally(() => {
            this.reload();
        });
    }

    /**
     * Sync all users that belong to this site.
     * @example this.transmit();
     * @returns {Q.Promise<void>}
     */
    transmit () {
        this.disableButton(this.$sync);

        return this.triggerTransmitRule();
    }

    /**
     * Deactivate the currently selected user.
     * @returns {Q.Promise<void>}
     * @example this.deactivate();
     */
    deactivate () {
        let id = this.selected.get('id');

        return ELF.trigger('USERMANAGEMENT:DeactivateUser', {}, this)
        .then((evt) => {
            // If not told to prevent default action, navigate to the add new user view
            !evt.preventDefault && this.navigate(`deactivate-user/${id}`);
        });
    }

    /**
     * Activate the currently selected user.
     * @returns {Q.Promise<void>}
     * @example this.activate();
     */
    activate () {
        let id = this.selected.get('id');

        return ELF.trigger('USERMANAGEMENT:ActivateUser', {}, this)
        .then((evt) => {
            // If not told to prevent default action, navigate to the add new user view
            !evt.preventDefault && this.navigate(`activate-user/${id}`);
        });
    }

    /**
     * Deselect the currently selected user.
     * @example this.deselect(user);
     */
    deselect () {
        delete this.selected;

        this.disableButton(this.$activate);
        this.disableButton(this.$edit);
        this.disableButton(this.$deactivate);
    }

    /**
     * Go back to the home view.
     * @example this.goBack();
     */
    goBack () {
        this.navigate('home');
    }

    /**
     * Navigate to the edit user view.
     * @returns {Q.Promise<void>}
     * @example this.editUser();
     */
    editUser () {
        return ELF.trigger('USERMANAGEMENT:EditUser', {}, this)
        .then((evt) => {
            // If not told to prevent default action, navigate to the add new user view
            !evt.preventDefault && this.navigate(`edit-user/${this.selected.get('id')}`);
        });
    }

    /**
     * Navigate to the add user view.
     * @returns {Q.Promise<void>}
     * @example this.addNewUser();
     */
    addNewUser () {
        return ELF.trigger('USERMANAGEMENT:NewUser', {}, this)
        .then((evt) => {
            // If not told to prevent default action, navigate to the add new user view
            !evt.preventDefault && this.navigate('add-site-user');
        });
    }
}
