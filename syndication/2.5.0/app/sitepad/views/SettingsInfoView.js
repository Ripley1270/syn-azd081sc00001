import PageView from 'core/views/PageView';
import InfoView from 'sitepad/views/InfoView';

export default class SettingsInfoView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {string} template - A selector that points to the template for the view.
         * @readonly
         * @default '#about-tpl'
         */
        this.template = '#settings-info-tpl';

        /**
         * @property {Object} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #back': 'back'
        };

        /**
         * @property {Object} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            title: 'SETTINGS_INFO_TITLE',
            back: 'BACK'
        };
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'about'
     */
    get id () {
        return 'settings-info';
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise<void>}
     */
    render () {
        let infoView = new InfoView();
        return this.buildHTML({}, true)
        .then(() => {
            return infoView.render();
        })
        .then(() => {
            this.$('.info-container').append(infoView.$el);
        });
    }

    /**
     * Navigate back to previous page.
     */
    back () {
        Backbone.history.history.back();
    }
}
