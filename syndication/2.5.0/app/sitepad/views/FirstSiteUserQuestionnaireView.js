import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Logger from 'core/Logger';
import WizardStepper from 'sitepad/views/WizardStepper';
import UnlockCode from 'core/classes/UnlockCode';
import * as lStorage from 'core/lStorage';
import Sites from 'core/collections/Sites';

let logger = new Logger('core/view/BaseQuestionnaireView');

export default class FirstSiteUserQuestionnaireView extends BaseQuestionnaireView {
    /**
     *
     * @param {Object} options
     * Required properties
     * id {string} - the ID of the questionnaire
     * Optional properties
     * showCancel {boolean} - Indicates whether to display the cancel button
     */
    constructor (options = {}) {
        super(options);

        /**
         * Id of template to render
         * @type String
         * @readonly
         * @default '#questionnaire-template'
         */
        this.template = '#first-site-user-questionnaire-template';

        /**
         * @property {Object<string,string>} events - A list of DOM events.
         * @readonly
         */
        this.events = _.extend(this.events, {
            'click #help-btn': 'settings' });

        this.isUnlockCodeValid = false;
    }

    /**
     * Renders the view.
     * @param {string=} id - screen id to render (TODO: if omitted does what?)
     * @returns {Q.Promise<void>}
     */
    render (id) {
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            tagline: {
                namespace: this.model.get('id'),
                key: this.model.get('displayName')
            },
            back: 'BACK',
            next: 'NEXT',
            help: 'HELP'
        };

        if (this.showCancel) {
            this.templateStrings.cancel = 'CANCEL';
        } else {
            this.templateStrings.cancel = '';
        }

        return this.buildHTML({
            className: this.model.get('className')
        })
        .then(() => {
            this.page();

            this.disableButton(this.$('#nextItem'));
            this.disableButton(this.$('#prevItem'));

            // Hide the cancel button, if appropriate:
            if (!this.showCancel) {
                this.hideCancelButton();
            }
        })
        .then(() => {
            let stepper = new WizardStepper({ viewId: this.id });
            stepper.render()
            .then(() => {
                $('#content').prepend(stepper.el);
            });
        })
        .then(() => ELF.trigger(`QUESTIONNAIRE:Rendered/${this.id}`, { questionnaire: this.id }, this))
        .then(() => {
            return this.displayScreen(id || this.data.screens[0].get('id'));
        })
        .then(() => {
            if (!id) {
                this.screenStack.push(this.data.screens[0].get('id'));
            }
            this.delegateEvents();
        })
        .then(() => {
            return this.setUnlockCodeHandler();
        })
        .catch((e) => {
            if (e.doNotLog) {
                return Q();
            }

            logger.error('From render.QUESTIONNAIRE:Rendered: ', e);
            return Q.reject(e);
        });
    }

    get defaultRouteOnExit () {
        return 'summaryScreen';
    }

    get defaultFlashParamsOnExit () {
        return {};
    }

    /**
     * Checks if password input fields match.
     */
    passwordsMatch () {
        return ($('#FIRST_USER_W_5').val().length > 0) && ($('#FIRST_USER_W_5').val() === $('#FIRST_USER_W_6').val());
    }

    /*
     * Navigate to the settings screen
     */
    settings () {
        this.navigate('settingsInfo');
    }

    /*
     * Sets the callback handler for handling the unlock code validation.
     * @returns {Q.Promise<void>}
     */
    setUnlockCodeHandler () {
        this.disableButton(this.$('#nextItem'));

        let unlockCodeWidget = $('#ADD_USER_W_8');
        unlockCodeWidget.on('input', () => {
            let unlockCodeAnswer = unlockCodeWidget.val();

            if (!unlockCodeAnswer) {
                this.clearInputState(unlockCodeWidget);
                this.isUnlockCodeValid = false;
                this.disableButton(this.$('#nextItem'));
            } else {
                let studyName = lStorage.getItem('studyDbName');
                return Sites.fetchFirstEntry()
                .then((site) => {
                    let unlockCode = UnlockCode.createStartupUnlockCode(site.get('siteCode'), studyName);
                    if (unlockCodeAnswer === unlockCode) {
                        this.inputSuccess(unlockCodeWidget);
                        this.isUnlockCodeValid = true;
                        this.enableButton(this.$('#nextItem'));
                    } else {
                        this.inputError(unlockCodeWidget);
                        this.isUnlockCodeValid = false;
                        this.disableButton(this.$('#nextItem'));
                    }
                });
            }

            return Q();
        });
    }

    /**
     * Performs the standard screen, Question and affidavit prep work.
     * @returns {Q.Promise<void>} resolved when complete
     */
    resolve () {
        return Q.Promise((resolve, reject) => {
            this.prepScreens();
            this.prepQuestions();
            return this.prepAnswerData(this.id, this.ordinal)
            .then(() => this.open())
            .then(() => resolve())
            .catch(e => reject(e));
        });
    }
}
