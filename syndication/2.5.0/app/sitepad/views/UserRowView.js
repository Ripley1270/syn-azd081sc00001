import RowView from './RowView';

// Just importing this file to ensure the Data.prototype (which is bad!) methods exist.
import * as dateTime from 'core/DateTimeUtil';

/**
 * Displays a user record as part of a table.
 * @class UserRowView
 * @extends RowView
 */
export default class UserRowView extends RowView {
    constructor (options) {
        super(options);

        this.template = _.template(`
            <td>{{ username }}</td>
            <td>{{ role }}</td>
            <td>{{ lastLogin }}</td>
            <td>{{ status }}</td>
        `);
        this.strings = options.strings;
    }

    /*
     * Render the view
     * @returns {Q.Promise<void>}
     */
    render () {
        let username = this.model.get('username'),
            role = this.model.getRole(),
            lastLogin = this.model.get('lastLogin'),
            status = this.strings[this.model.get('active') ? 'active' : 'deactivated'];

        if (lastLogin != null) {
            let date = new Date(lastLogin);

            // Format the date by locale.
            lastLogin = dateTime.getLocalizedDate(date, { includeTime: false, useShortFormat: false });
        }

        return this.i18n(role.get('displayName'))
        .then((roleText) => {
            this.model.set('roleText', roleText);
            this.model.set('statusText', status);

            this.$el.html(this.template({ username, role: roleText, lastLogin, status }));

            return super.render();
        });
    }
}
