import ComponentView from 'core/views/ComponentView';
import * as lStorage from 'core/lStorage';
import * as dateTimeUtil from 'core/DateTimeUtil';
import Sites from 'core/collections/Sites';
import { getCurrentProtocol } from 'core/Helpers';

export default class InfoView extends ComponentView {
    constructor (options) {
        super(options);

        /**
         * @property {string} template - A selector that points to the template for the view.
         * @readonly
         * @default '#info-tpl'
         */
        this.template = options && options.template ? options.template : '#info-tpl';

        /**
         * @property {Object} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            labelDeviceInfo: 'DEVICE_INFO',
            labelStudyInfo: 'STUDY_INFO',
            labelStudyVersion: 'STUDY_VERSION',
            labelStudyURL: 'SERVICE_URL',
            labelDeviceId: 'DEVICE_ID',
            labelDeviceSerialNumber: 'DEVICE_SERIAL_NUMBER',
            labelUsername: 'LOGGED_IN_USER',
            labelSite: 'SITE',
            labelProtocol: 'PROTOCOL',
            labelSponsor: 'SPONSOR',
            labelStoredReports: 'STORED_REPORTS',
            labelProductVersion: 'CORE_PRODUCT_VERSION_LABEL',
            labelBatteryLevel: 'BATTERY_LEVEL',
            labelLastRefreshTime: 'LAST_REFRESH_TIME'
        };
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'about'
     */
    get id () {
        return 'info';
    }

    /**
     * Creates the parameters for the template
     * @returns {Q.Promise<Object>}
     */
    getTemplateParameters () {
        let na = 'N/A',
            lastRefreshTime = lStorage.getItem('lastRefreshTime'),
            getBatteryLevel = () => {
                return Q.Promise((resolve) => {
                    LF.Wrapper.Utils.getBatteryLevel(resolve);
                });
            },
            username = LF.security.activeUser ? LF.security.activeUser.get('username') : na;

        return Q.all([
            Sites.fetchCollection(),
            this.getPendingReportCount(),
            getBatteryLevel()
        ])
            .spread((sites, storedReports, batteryLevel) => ({
                studyVersion: LF.StudyDesign.studyVersion,
                studyURL: lStorage.getItem('serviceBase') || LF.apiBase,
                deviceId: lStorage.getItem('deviceId') || na,
                deviceSerialNumber: lStorage.getItem('IMEI') || na,
                site: sites.at(0).get('siteCode'),
                protocol: getCurrentProtocol(),
                sponsor: LF.StudyDesign.clientName,
                storedReports,
                productVersion: LF.coreVersion,
                batteryLevel: batteryLevel || na,
                lastRefreshTime: lastRefreshTime ? dateTimeUtil.getLocalizedDate(new Date(`${lastRefreshTime}Z`), { includeTime: true, useShortFormat: false }) : na,
                username
            }));
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise<void>}
     */
    render () {
        return this.getTemplateParameters()
            .then(parameters => this.buildHTML(parameters, false));
    }
}
