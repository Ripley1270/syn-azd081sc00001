import BaseController from 'core/controllers/BaseController';
import Logger from 'core/Logger';
import { checkTimeZoneSet } from 'core/Helpers';
import ELF from 'core/ELF';
import EULA from 'core/classes/EULA';
import PageView from 'core/views/PageView';
import FirstSiteUserQuestionnaireView from 'sitepad/views/FirstSiteUserQuestionnaireView';
import QuestionnaireCompletionView from 'core/views/QuestionnaireCompletionView';
import State from 'sitepad/classes/State';
import ModeSelectionView from 'sitepad/views/ModeSelectionView';
import SiteSelectionView from 'sitepad/views/SiteSelectionView';
import SummaryScreenView from 'sitepad/views/SummaryScreenView';
import UnlockCodeView from 'sitepad/views/UnlockCodeView';
import SetTimeZoneActivationView from 'sitepad/views/SetTimeZoneActivationView';
import LanguageSelectionView from 'sitepad/views/LanguageSelectionView';
import SitePadEULAView from 'sitepad/views/SitePadEULAView';
import TestConnectionView from 'sitepad/views/TestConnectionView';

let logger = new Logger('RegistrationController');

export default class RegistrationController extends BaseController {
    constructor (options) {
        super(options);

        switch (State.get()) {
            case State.states.locked:
            case State.states.setTimeZone:
            case State.states.endUserLicenseAgreements:
            case State.states.setupUser:
                State.set(State.states.languageSelection);

            // No default
        }

        // Initialize
        EULA.decline();
    }

    /**
     * Route based on state of application.
     */
    route () {
        switch (State.get()) {
            case State.states.new:
                this.modeSelection();
                break;
            case State.states.siteSelection:
                this.siteSelection();
                break;
            case State.states.locked:
                this.unlockCode();
                break;
            case State.states.setTimeZone:
                this.setTimeZone();
                break;
            case State.states.languageSelection:
                this.languageSelection();
                break;
            case State.states.testConnection:
                this.testConnection();
                break;
            case State.states.endUserLicenseAgreements:
                this.endUserLicenseAgreements();
                break;
            case State.states.setupUser:
                this.setupUser();
                break;
            case State.states.activated:
            default:
                this.navigate('login');
                break;
        }
    }

    /**
     * Display the ModeSelectionView.
     */
    modeSelection () {
        this.go('ModeSelectionView', ModeSelectionView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the first user creation questionnaire.
     */
    setupUser () {
        if (State.isCurrentState(State.states.setupUser)) {
            this.go('FirstSiteUserQuestionnaireView', FirstSiteUserQuestionnaireView, {
                id: 'First_Site_User'
            })
            .catch(e => logger.error(e))
            .done();
        } else {
            this.navigate('language-selection');
        }
    }

    /**
     * Display the LanguageSelectionView.
     */
    languageSelection () {
        checkTimeZoneSet()
        .then((result) => {
            if (result) {
                return ELF.trigger('REGISTRATION:SkipOrDisplayFirstSiteUser', {}, new PageView());
            }

            return this.go('LanguageSelectionView', LanguageSelectionView)
            .catch(e => logger.error(e))
            .done();
        })
        .done();
    }

    /**
     * Display the TestConnectionView.
     */
    testConnection () {
        this.go('TestConnectionView', TestConnectionView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the SiteSelectionView.
     */
    siteSelection () {
        this.go('SiteSelectionView', SiteSelectionView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the UnlockCodeView.
     */
    unlockCode () {
        if (State.isCurrentState(State.states.locked)) {
            this.go('UnlockCodeView', UnlockCodeView)
            .catch(e => logger.error(e))
            .done();
        } else {
            this.navigate('language-selection');
        }
    }

    /**
     * Display the SetTimeZoneActivationView.
     */
    setTimeZone () {
        this.go('SetTimeZoneActivationView', SetTimeZoneActivationView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the End User License Agreement (EULA).
     */
    endUserLicenseAgreements () {
        // this check is needed to fix DE23481.
        checkTimeZoneSet()
        .then(() => {
            if (EULA.isAccepted()) {
                ELF.trigger('REGISTRATION:SkipOrDisplayFirstSiteUser', {}, this);
            } else {
                this.go('SitePadEULAView', SitePadEULAView)
                .catch(e => logger.error(e))
                .done();
            }
        })
        .done();
    }

    /**
     * Display the QuestionnaireCompletionView.
     */
    questionnaireCompletion () {
        this.go('QuestionnaireCompletionView', QuestionnaireCompletionView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the SummaryScreenView.
     */
    summaryScreen () {
        this.go('SummaryScreenView', SummaryScreenView)
        .catch(e => logger.error(e))
        .done();
    }
}
