import ELF from 'core/ELF';
import Answer from 'core/models/Answer';
import Logger from 'core/Logger';
import Transmission from 'core/models/Transmission';
import Transmissions from 'core/collections/Transmissions';
import * as lStorage from 'core/lStorage';

let logger = new Logger('editPatientSave');

/**
 * Saves a new Edit Patient Diary
 * @memberOf ELF.actions/sitepad
 * @method editPatientSave
 * @param {Object} params - Not used in this action.
 * @param {function} done - A callback function invoked upon completion of the action.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'editPatientSave' }]
 */
export function editPatientSave (params, done) {
    let deferred = Q.defer(),
        diaryAnswers = this.data.answers,
        subject = this.subject,
        user = this.user,
        dashboard = this.data.dashboard,
        reason = diaryAnswers.match({ question_id: 'EDIT_PATIENT_REASON' })[0].get('response'),
        answersToRemove = [],
        addStatus = false,
        now,
        parameters,

        // DE23361 if user is trying to edit the id of the patient which transmission item is not yet sent, app will edit the original
        // transmission item instead of creating a new one
        patientNotTransmitted = false,
        checkAndAddAnswers = () => {
            let answer,
                subjectIdAnswer,
                subjectInitialsAnswer,
                languageAnswer;

            answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_ID' });
            subjectIdAnswer = answer[0].get('response');

            if (subjectIdAnswer === subject.get('subject_id')) {
                answersToRemove.push(answer[0]);
                if (subject.get('isDuplicate')) {
                    addStatus = true;
                }
            } else {
                let comment = new Answer({
                        subject_id: subject.get('subject_id'),
                        response: reason,
                        SW_Alias: 'PT.Patientid.Comment',
                        questionnaire_id: this.id,
                        question_id: 'EDIT_PATIENT_ID_COMMENT'
                    }),
                    subjectNumber = subjectIdAnswer.substring(LF.StudyDesign.participantSettings.participantNumberPortion[0],
                        LF.StudyDesign.participantSettings.participantNumberPortion[1]);

                diaryAnswers.add(comment);

                // setting the subject_id to subject model
                subject.set('subject_id', subjectIdAnswer);

                // setting subject number to subject model
                subject.set('subject_number', subjectNumber);

                // setting the subject_id to username of the user model (DE16517)
                user.set('username', subjectIdAnswer);

                // hacking SWAlias
                answer[0].set('SW_Alias', answer[0].get('SW_Alias').replace('.0.', '.'));
            }

            answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_INITIALS' });
            subjectInitialsAnswer = answer[0] ? answer[0].get('response') : '---';

            if (subjectInitialsAnswer === subject.get('initials')) {
                answersToRemove.push(answer[0]);
            } else {
                let comment = new Answer({
                    subject_id: subject.get('subject_id'),
                    response: reason,
                    SW_Alias: 'PT.Initials.Comment',
                    questionnaire_id: this.id,
                    question_id: 'EDIT_PATIENT_INITIALS_COMMENT'
                });

                diaryAnswers.add(comment);

                // setting the initials to subject model
                subject.set('initials', subjectInitialsAnswer);

                // hacking SWAlias
                answer[0].set('SW_Alias', answer[0].get('SW_Alias').replace('.0.', '.'));
            }

            answer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_LANGUAGE' });
            languageAnswer = JSON.parse(answer[0].get('response')).language;


            if (languageAnswer === user.get('language')) {
                answersToRemove.push(answer[0]);
            } else {
                let comment = new Answer({
                    subject_id: subject.get('subject_id'),
                    response: reason,
                    SW_Alias: 'Assignment.0.Language.Comment',
                    questionnaire_id: this.id,
                    question_id: 'EDIT_PATIENT_LANGUAGE_COMMENT'
                });
                diaryAnswers.add(comment);

                // setting the language to subject model
                subject.set('language', languageAnswer);

                // setting the language to user model
                user.set('language', languageAnswer);
                answer[0].set('response', languageAnswer.replace('-', '_'));
            }
        },
        removeAnswers = () => {
            // answers to delete should be deleted here
            let reasonAnswer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_REASON' }),
                usr = LF.security.activeUser,
                diaryData;


            if (reasonAnswer) {
                answersToRemove.push(reasonAnswer[0]);
            }

            diaryAnswers.remove(answersToRemove);

            now = new Date();
            diaryData = _.where(diaryAnswers.toJSON(), { questionnaire_id: this.id });

            // prepare the parameters for transmission after answer model changes are done
            parameters = JSON.stringify({
                krpt: subject.get('krpt'),
                ResponsibleParty: usr.get('username'),
                dateStarted: dashboard.get('started'),
                dateCompleted: now.ISOStamp(),
                reportDate: dashboard.get('report_date'),
                phase: subject.get('phase'),
                phaseStartDate: subject.get('phaseStartDateTZOffset'),
                sigID: `SA.${this.data.started.getTime().toString(16)}${lStorage.getItem('IMEI')}`,
                answers: diaryData,
                ink: this.data.ink,
                batteryLevel: dashboard.get('battery_level')
            });
        },
        createTransmission = () => {
            if (patientNotTransmitted) {
                return false;
            }
            let model = new Transmission(),
                newTransmission = {
                    method: 'transmitEditPatient',
                    params: parameters,
                    created: now.getTime()
                };

            if (addStatus) {
                newTransmission.status = 'failed';
            }

            return model.save(newTransmission);
        },
        resolveExistingTransmission = () => {
            return Q.all([Transmissions.fetchCollection()])
            .spread((transmissions) => {
                transmissions.forEach((transmission) => {
                    let transmissionMethod = transmission.get('method'),
                        parsedParams = JSON.parse(transmission.get('params')),
                        editPatientIdAnswer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_ID' }),
                        editPatientId = editPatientIdAnswer && editPatientIdAnswer[0].get('response');

                    if (parsedParams.krpt === subject.get('krpt')) {
                        switch (transmissionMethod) {
                            case 'transmitSubjectAssignment': {
                                let editPatientInitialsAnswer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_INITIALS' }),
                                    editPatientInitials = editPatientInitialsAnswer && editPatientInitialsAnswer[0].get('response'),
                                    editPatientLanguageAnswer = diaryAnswers.match({ question_id: 'EDIT_PATIENT_LANGUAGE' }),
                                    editPatientLanguage = editPatientLanguageAnswer && editPatientLanguageAnswer[0].get('response');

                                patientNotTransmitted = true;
                                parsedParams.patientId = editPatientId || parsedParams.patientId;
                                parsedParams.initials = editPatientInitials || parsedParams.initials;
                                parsedParams.language = editPatientLanguage || parsedParams.language;
                                parsedParams = JSON.stringify(parsedParams);
                                transmission.unset('status');
                                transmission.save({ params: parsedParams });
                                break;
                            }

                            case 'transmitEditPatient':
                                patientNotTransmitted = true;
                                transmission.save({ params: parameters });

                                if (transmission.get('status') === 'failed' && !addStatus) {
                                    transmission.destroy();
                                }
                                break;

                            // no default
                        }
                    }
                });
            });
        };

    Q()
    .then(checkAndAddAnswers)
    .then(removeAnswers)
    .then(resolveExistingTransmission)
    .then(createTransmission)
    .then(() => {
        return subject.save();
    })
    .then(() => {
        return user.save();
    })
    .then(() => {
        deferred.resolve(true);
    })
    .catch((e) => {
        logger.error(`EditPatientSave failed:  ${e}`);
        LF.spinner.hide();
        deferred.reject(false);
    })
    .done(() => {
        done();
    });

    return deferred.promise;
}

ELF.action('editPatientSave', editPatientSave);
