import Transmissions from 'core/collections/Transmissions';
import Users from 'core/collections/Users';
import * as sync from 'core/actions/syncUsers';

/**
 * @memberOf ELF.actions
 * @method handleFailedUserTransmission
 * @description
 * Synchronize the site user list and check if are no more duplicate users, if so then enable the failed deuplicate user transmission(s) and attempt to transmit.
 * @returns {Q.Promise<ELF~ActionResult>}
 * @example
 * resolve: [{
 *     action: 'handleFailedUserTransmission'
 * }]
 */

export default function handleFailedUserTransmission () {
    return sync.syncUsers()
    .then(() => Q.all([Users.fetchCollection(), Transmissions.fetchCollection()]))
    .spread((users, transmissions) => {
        transmissions.forEach((transmission) => {
            let transmissionMethod = transmission.get('method');

            if (transmission.get('status') === 'failed' && (transmissionMethod === 'transmitUserData' || transmissionMethod === 'transmitUserEdit')) {
                let params = JSON.parse(transmission.get('params')),
                    user = users.findWhere({ id: params.userId }) || users.findWhere({ id: params.id }),
                    username = user.get('username'),
                    isMarkedAsDuplicate = username.indexOf('*') !== -1,

                    // find if there's a user with same username (without '*') and role
                    isDuplicateFound = users.findWhere({ username: username.slice(0, -1), role: user.get('role') });

                if (isMarkedAsDuplicate && !isDuplicateFound) {
                    user.unset('isDuplicate');
                    user.save({ username: username.slice(0, -1) });
                    transmission.unset('status');
                    transmission.save();
                }
            }
        });
    });
}

ELF.action('handleFailedUserTransmission', handleFailedUserTransmission);
