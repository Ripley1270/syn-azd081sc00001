import ELF from 'core/ELF';
import Logger from 'core/Logger';
import { getNested } from 'core/utilities';
import User from 'core/models/User';
import Users from 'core/collections/Users';
import Transmission from 'core/models/Transmission';
import Transmissions from 'core/collections/Transmissions';

const logger = new Logger('editUserSave');

/**
 * @memberOf ELF.actions/sitepad
 * @method  editUserSave
 * @description
 * Saves changes made to a user via the Edit_User questionnaire.
 * @param {Object[]} config A configuration defining the edit user form.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{
 *      action: 'editUserSave',
 *      data: [{
 *          SW_Alias: 'Edit_User.0.EDIT_USER_1',
 *          question_id: 'EDIT_USER_USERNAME',
 *          questionnaire_id: 'Edit_User',
 *          // The property on this response is determine by the 'field' property
 *          // on the question's widget configuration. e.g. { username: 'jsmith' }
 *          // Indicate that the response is JSON.
 *          isJSON: true,
 *          // Determine the field name of the JSON response. e.g. { username: ... }
 *          field: 'username',
 *          // Use the user model scoped to the UserStatusQuestionnaireView (this.user).
 *          model: 'user',
 *          // Use the user model's username property to populate the response.
 *          // e.g. this.user.get('username');
 *          property: 'username'
 *      }, ...]
 *  }]
 */
export default function editUserSave (config) {
    const editUserMethod = 'transmitUserEdit';
    const newUserMethod = 'transmitUserData';

    // Even though we have the user in scope, we are still going to fetch a fresh copy
    // from the local database.  Since the fetch is by ID, it's fast and has little to no hit on performance.
    let user = new User({ id: this.user.get('id') });

    // We need to maintain previous values when resolving any duplicate usernames.
    let previousUsername;
    let previousRole;

    let transmissions = new Transmissions();

    // Find an existing edit or new user transmission for a given user.
    // returns {Transmission}
    let findExistingTransmission = (usr) => {
        return transmissions.find((transmission) => {
            // We only care about edit user and new user transmission methods.
            if (transmission.get('method') === editUserMethod) {
                // Parse out the user's ID from the transmission params.
                let { id } = JSON.parse(transmission.get('params'));

                return usr.get('id') === id;

            // In the case of a new user method existing, it may be a failed transmission due to duplication of username.
            } else if (transmission.get('method') === newUserMethod) {
                // Parse out the user's ID from the transmission params.
                // The new user transmission record stores the user's ID as userId instead of id.
                let { userId } = JSON.parse(transmission.get('params'));

                return usr.get('id') === userId;
            }

            return false;
        });
    };

    // Removes the original transmission, and requeues it.
    // This is required when modifying a duplicate username.  We need to
    // ensure the modification to the original user is transmitted prior to modification
    // made to resolve the duplicate username.  Normally, we shouldn't modify transmissions
    // already in the queue, as they are usually dependent on a specified order.
    // As this is a rare case, and users aren't native StudyWorks entities, we can make an exception here.
    // If a form is transmitted with the user's signature, prior to creation of said user, there shouldn't be a
    // problem.
    let requeueTransmission = (transmission) => {
        let { id, method, params } = transmission.toJSON();

        logger.trace('Attempting to requeue transmission.', transmission);

        // Destroy the original transmission record.
        return transmissions.destroy(id)
        .then(() => {
            let newTransmission = new Transmission({
                method,
                params,
                created: new Date().getTime()
            });

            return newTransmission.save();
        });
    };

    // Resolves a transmission record for the user edit.
    // returns {Q.Promise<void>}
    let resolveTransmission = (usr, requeue = false) => {
        // Determines if a transmitUserEdit transmission record exists for the given user.
        let existingTransmission = findExistingTransmission(usr);

        // If a transmission already exists, no need to create another.
        if (existingTransmission) {
            logger.trace(`An existing transmission record exists for user ${usr.get('username')}.`, existingTransmission.toJSON());

            // If the existing transmission previously failed, we have to null out the status so a new attempt will be made.
            // If the transmission hasn't yet failed, we don't need to do anything. The transmission will kick off at the next transmitAll.
            if (existingTransmission.get('status') === 'failed') {
                existingTransmission.set('status', null);

                if (requeue) {
                    return requeueTransmission(existingTransmission);
                }
                return existingTransmission.save();
            }

            // Returns a promise for consistent-return.
            return requeue ? requeueTransmission(existingTransmission) : Q();
        }
        logger.trace(`A new ${editUserMethod} transmission will be created for user ${usr.get('username')}.`);
        let transmission = new Transmission({
            method: editUserMethod,
            params: JSON.stringify({ id: usr.get('id') }),
            created: new Date().getTime()
        });

        return transmission.save();
    };

    // Search for and resolve any duplication of username.
    // returns {Q.Promise<void>}
    let resolveDuplicates = () => {
        // Users are unique per composite username/role.
        let usernameChanged = user.get('username') !== previousUsername;
        let roleChanged = user.get('role') !== previousRole;

        logger.trace(`Resolving duplicate users with username ${previousUsername} and role ${previousRole}.`);

        // If the modified user's previous username didn't contain an asterisk,
        // then we need to check all other users if they're a duplicate.
        // Depending on the number of stored users, this is an expensive operation for a minor quality of life feature.
        if (previousUsername.indexOf('*') === -1 && (usernameChanged || roleChanged)) {
            return Users.fetchCollection()
            .then((users) => {
                // Find the duplicate username with an asterisk.
                // I doubt there will ever be more than one duplicate, even if there were,
                // we can only resolve the first instance of the duplication.
                let duplicate = users.find((usr) => {
                    return usr.get('username').toLowerCase() === `${previousUsername.toLowerCase()}*` && usr.get('role') === previousRole;
                });

                // If there is a duplicate user, we need to resolve the name.
                if (duplicate) {
                    logger.trace(`Duplicate user ${duplicate.get('username')} found.  Resolving username to ${duplicate.get('username').slice(0, -1)}`);
                    duplicate.set('username', duplicate.get('username').slice(0, -1));

                    if (LF.security.activeUser.get('username') === `${previousUsername}*`) {
                        LF.security.activeUser = duplicate;
                    }

                    duplicate.set('isDuplicate', false);
                    return duplicate.save()

                    // Now that the duplicate user is modified, we need to resolve the user's transmision record.
                    .then(() => resolveTransmission(duplicate, true));
                }

                logger.trace('No duplicate users to be resolved.');

                // Need a return value for consistent-return.
                return Q();
            });
        }

        logger.trace('User is not a duplicate anymore.');

        user.set('isDuplicate', false);
        return user.save();
    };

    logger.trace('Executing ELF action editUserSave.');

    // Fetch the updated user record and all existing transmissions.
    return Q.all([user.fetch(), transmissions.fetch()])
    .then(() => {
        logger.trace(`Edits are being applied to user: ${user.get('username')}.`);

        previousUsername = user.get('username');
        previousRole = user.get('role');

        // Loop through each configured form field.
        config.forEach((field) => {
            // Find the answer model the matches the configuration item's.
            let answer = this.answers.findWhere({ SW_Alias: field.SW_Alias });

            // Use getNested to parse out the configured model from the context.
            // This allows for configuring a model deep within the context.
            // e.g. model: 'parent.child.model'
            let source = getNested(this, field.model);

            let response;

            // If the response should be JSON, format it as such.
            // e.g { response : "{ username: 'jsmith'}" }
            if (field.isJSON) {
                // Fetch the response from the answer model and parse it as JSON.
                let parsed = JSON.parse(answer.get('response'));

                response = parsed[field.field || field.property];
            } else {
                response = answer.get('response');
            }

            // If the field has changed from it's original value.
            if (source.get(field.property) !== response) {
                // If the response is a string, trim excess space.
                response = typeof response === 'string' ? response.trim() : response;

                // Set the changed property on the user model.
                // We don't really care about validation, or duplicate values, as the form should handle that.
                user.set(field.property, response);
                logger.trace(`User property ${field.property} has changed to ${response}.`);
            }
        });
        return user.save();
    })

    // Now that the user modifications are saved, we need to resolve a transmission record to sync the changes.
    .then(() => resolveTransmission(user))

    // After the transmission is successfully completed, resolve any duplicate usernames.
    // This is a secondary concern compared to creating the transmission record.
    .then(() => resolveDuplicates())
    .finally(() => {
        logger.trace('ELF action editUserSave has completed.');
    });
}

ELF.action('editUserSave', editUserSave);
