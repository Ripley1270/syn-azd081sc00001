import ELF from 'core/ELF';
import State from 'sitepad/classes/State';

/**
 * Set the state of the application.
 * @memberOf ELF.actions/sitepad
 * @method setState
 * @param {string} toState - The state to set. See sitepad/classes/State for a list of avialable states.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'setState', data: 'SET_TIME_ZONE' }]
 */
export default function setState (toState) {
    State.set(toState);
    return Q();
}

ELF.action('setState', setState);
