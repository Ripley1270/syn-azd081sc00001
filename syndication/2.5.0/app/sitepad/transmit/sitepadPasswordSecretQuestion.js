import COOL from 'core/COOL';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';
import CurrentSubject from 'core/classes/CurrentSubject';
import Subjects from 'core/collections/Subjects';
import { MessageRepo } from 'core/Notify';
import getSetupCode from './getSetupCode';
import getDeviceId from './getDeviceId';
import Spinner from 'core/Spinner';
import transmitResetCredentials from 'sitepad/transmit/transmitResetCredentials';
import transmitResetPassword from 'sitepad/transmit/transmitResetPassword';

const logger = new Logger('Transmit.sitepadPasswordSecretQuestion');

/**
 * Handles the sitepadPasswordSecretQuestion transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise}
 */
export default function sitepadPasswordSecretQuestion (transmissionItem) {
    let subject,
        params = JSON.parse(transmissionItem.get('params'));

    let onTransmissionError = ({ errorCode, httpCode, isSubjectActive }) => {
        const { Dialog } = MessageRepo;

        if (!isSubjectActive) {
            return Spinner.hide()
            .then(() => LF.Actions.notify({
                dialog: Dialog && Dialog.PASSWORD_CHANGE_FAILED
            }))
            .then(() => Spinner.show())
            .then(() => this.destroy(transmissionItem.get('id')))
            .then(() => Q.reject({ errorCode, httpCode, isSubjectActive }));
        }

        if (httpCode !== LF.ServiceErr.HTTP_NOT_FOUND) {
            return Spinner.hide()
            .then(() => {
                // eslint-disable-next-line no-param-reassign
                errorCode = errorCode || '';

                return LF.Actions.notify({
                    dialog: Dialog && Dialog.TRANSMISSION_ERROR_HTTP_AND_ERROR,
                    options: { httpCode, errorCode }
                });
            })
            .then(() => Spinner.show())
            .then(() => {
                if (httpCode === LF.ServiceErr.HTTP_FORBIDDEN && errorCode === LF.ServiceErr.SUBJECT_NOT_FOUND) {
                    // If the subject is deleted on backend then destroy the transmission item
                    return this.destroy(transmissionItem.get('id'))
                    .then(() => Q.reject({ errorCode, httpCode, isSubjectActive }));
                }

                this.remove(transmissionItem);
                return Q.reject({ errorCode, httpCode, isSubjectActive });
            });
        }

        this.remove(transmissionItem);
        return Q.reject({ errorCode, httpCode, isSubjectActive });
    };

    let isPermanentPasswordCreated = (subj) => {
        let service = COOL.new('WebService', WebService);

        return service.getSingleSubject(subj.get('krpt'), null)
        .catch(onTransmissionError)
        .then(({ res }) => !!res.password);
    };

    let replaceTransmissionMethod = () => {
        if (params.secret_question && params.secret_answer) {
            return transmissionItem.save({
                method: 'transmitResetCredentials',
                params: JSON.stringify({
                    password: params.password,
                    krpt: params.krpt,
                    secretQuestion: params.secret_question,
                    secretAnswer: params.secret_answer
                })
            })
            .then(() => transmitResetCredentials.call(this, transmissionItem));
        }

        return transmissionItem.save({ method: 'transmitResetPassword' })
        .then(() => transmitResetPassword.call(this, transmissionItem));
    };

    let sendTransmission = (subj) => {
        let onSuccess,
            mappedJSON,
            auth = [subj.get('setupCode'), subj.get('service_password'), subj.get('device_id')].join(':');

        if (params.secret_question === null) {
            params.secret_question = subj.get('secret_question');
        }

        if (params.secret_answer === null) {
            params.secret_answer = subj.get('secret_answer');
        }

        mappedJSON = {
            Q: params.secret_question,
            A: params.secret_answer
        };

        if (params.password) {
            mappedJSON.W = params.password;
        }

        onSuccess = ({ res, isSubjectActive }) => {
            let attributes = {
                secret_question: params.secret_question,
                secret_answer: params.secret_answer,
                subject_active: isSubjectActive
            };

            if (params.password) {
                attributes.subject_password = params.password;
            }

            if (res && res.W) {
                attributes.service_password = res.W;
            }

            if (res && res.D) {
                attributes.device_id = res.D;
            }

            return subj.save(attributes)
            .then(() => {
                CurrentSubject.clearSubject();
                return this.destroy(transmissionItem.get('id'));
            });
        };

        let service = COOL.new('WebService', WebService);

        if (!subj.get('service_password')) {
            mappedJSON.U = subj.get('setupCode');

            return service.setSubjectData(mappedJSON)
            .catch(onTransmissionError)
            .then(onSuccess);
        }

        return service.updateSubjectData(subj.get('device_id'), mappedJSON, auth)
        .catch(onTransmissionError)
        .then(onSuccess);
    };

    return Subjects.getSubjectBy({ krpt: params.krpt })
    .tap((subjectByKrpt) => {
        subject = subjectByKrpt;
    })
    .tap(getSetupCode)
    .tap(getDeviceId)
    .catch((err) => {
        // Let's handle errors received from above.
        // The .then() block below will throw its own errors which we don't want to handle here
        logger.error('Error in sitepadPasswordSecretQuestion', err);

        this.remove(transmissionItem);
        return Q.reject(err);
    })
    .then(() => isPermanentPasswordCreated(subject))
    .then((permanentPasswordCreated) => {
        // If permanent password is already set on another device then we should be calling a different API
        // The existing transmissionItem should be updated and then redirected to the right transmit method
        if (permanentPasswordCreated) {
            return replaceTransmissionMethod();
        }

        return sendTransmission(subject);
    });
}
