import COOL from 'core/COOL';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';
import SitePadPayload from 'sitepad/classes/SitePadPayload';

const logger = new Logger('Transmit.transmitSPStartDate');

/**
 * Handles the SPStartDate update transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise}
 */
export default function transmitSPStartDate (transmissionItem) {
    let params = JSON.parse(transmissionItem.get('params')),
        dataJSON = COOL.getClass('Payload', SitePadPayload).sPStartDate(params),
        sendSPStartDateSuccess = ({ res }) => {
            logger.operational(`SPStartDate transmitted: res: ${res}`);
            return this.destroy(transmissionItem.get('id'));
        },
        sendSPStartDateError = ({ errorCode, httpCode, isSubjectActive }) => {
            logger.trace(`transmitSPStartDate: SPStartDate error: removing transmissionItem errorCode: ${errorCode} httpCode: ${httpCode}`);
            this.remove(transmissionItem);
            return Q.reject({ errorCode, httpCode, isSubjectActive });
        };

    logger.traceEnter('transmitSPStartDate');

    let service = COOL.new('WebService', WebService);

    return service.sendEditPatient(dataJSON, '')
    .then(sendSPStartDateSuccess)
    .catch(sendSPStartDateError)
    .finally(() => {
        logger.traceExit('transmitSPStartDate');
    });
}
