import COOL from 'core/COOL';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';
import CurrentSubject from 'core/classes/CurrentSubject';
import Subjects from 'core/collections/Subjects';
import * as transmitResetCredentials from 'sitepad/transmit/transmitResetCredentials';
import getSetupCode from 'sitepad/transmit/getSetupCode';

const logger = new Logger('Transmit.transmitResetPassword');

/**
 * Handles the transmitResetPassword transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise}
 */
export default function transmitResetPassword (transmissionItem) {
    let params = JSON.parse(transmissionItem.get('params'));

    return Subjects.getSubjectBy({ krpt: params.krpt })
    .tap(getSetupCode)
    .catch((err) => {
        logger.error('Error in transmitResetPassword', err);
        this.remove(transmissionItem);
    })
    .then((subject) => {
        let service = COOL.new('WebService', WebService),
            payload = { clientPassword: params.password };

        if (LF.StudyDesign.askSecurityQuestion) {
            payload.challengeQuestions = [{
                question: subject.get('secret_question'),
                answer: subject.get('secret_answer')
            }];
        }

        let onSuccess = ({ res, isSubjectActive }) => {
            return subject.save({
                subject_password: params.password,
                service_password: res.password,
                subject_active: isSubjectActive
            })
            .catch((err) => {
                logger.error('Error saving subject in transmitResetCredentials', err);
                this.remove(transmissionItem);
            })
            .then(() => {
                CurrentSubject.clearSubject();
                return this.destroy(transmissionItem.get('id'));
            });
        };

        return service.resetSubjectPassword(payload, params.krpt)
        .catch((err) => {
            return transmitResetCredentials.onError.call(this, err, transmissionItem);
        })
        .then(onSuccess);
    });
}
