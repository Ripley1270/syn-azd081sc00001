import COOL from 'core/COOL';
import * as lStorage from 'core/lStorage';
import * as DateTimeUtil from 'core/DateTimeUtil';
import * as utils from 'core/utilities';
import Dashboard from 'core/models/Dashboard';
import Answer from 'core/models/Answer';
import Transmission from 'core/models/Transmission';

const getBatteryLevel = () => {
    return Q.Promise((resolve) => {
        LF.Wrapper.Utils.getBatteryLevel((batteryLevel) => {
            resolve(batteryLevel);
        });
    });
};

export default class VisitService {
    /**
     * Create and return a Signature ID (sig_id) value.
     * @param {Date} [currentDate] - The current date.
     * @param {number} [offset=0] - Offset to append to the generated timestamp.
     * @returns {string}
     */
    static createSigId (currentDate = new Date(), offset = 0) {
        return `${'SA'}.${(currentDate.getTime() + offset).toString(16)}${lStorage.getItem('IMEI')}`;
    }

    /**
     * Creates event at entry variables.
     * @param {Object} options - options
     * @param {number} options.eventId - Event id
     * @param {Date} options.startDate - Start date
     * @param {number} options.offset - Timezone offset in millis
     * @param {string} options.questionnaireId - Questionnaire ID
     * @param {number} options.dashboardId - id of the dashboard record in the local db
     * @param {Subject} options.subject - Subject in question
     * @returns {Q.Promise<Number>}
     */
    static createEventAtEntryVariables ({
        studyEventId,
        startDate,
        offset,
        questionnaireId,
        dashboardId,
        subject
    }) {
        const createAnswer = (questionId, response) => {
            return new Answer({
                subject_id: subject.get('subject_id'),
                question_id: questionId,
                questionnaire_id: questionnaireId,
                response,
                SW_Alias: `Phase.0.${questionId}`,
                dashboardId
            })
            .save();
        };

        return Q.all([
            createAnswer('EventAtEntry', studyEventId),
            createAnswer('EventStartDate', startDate),
            createAnswer('EventTZOffset', offset.toString())
        ]);
    }

    /**
     * Calculates visit end date for expired visit: visit start date + expiration, otherwise undefined.
     * @param {boolean} visitExpired - indicates if current visit is expired
     * @param {string} visitStartDate - date when visit is started
     * @param {number} visitExpiration - param indicates when visit should expire (in minutes) from visit start.
     * @returns {Date}
     */
    static calculateVisitEndDate (visitExpired = false, visitStartDate, visitExpiration) {
        let visitEndDate;

        if (visitExpired && visitStartDate != null && visitExpiration != null) {
            // eslint-disable-next-line new-cap
            visitEndDate = new Date(DateTimeUtil.parseDateTimeIsoNoOffset(visitStartDate).getTime() + (visitExpiration * 60000));
        }

        return visitEndDate;
    }

    /**
     * Creates visit start report for current visit.
     * @param {number} studyEventId - The ID of the study event.
     * @param {boolean} isSkipVisit - true if skipping visit.
     * @param {Date} visitStartDate - The start date of the visit.
     * @returns {Q.Promise<string>} The sig_id of the newly created dashboard record.
     */
    static createStartVisitReport ({
        studyEventId,
        subject,
        startDate,
        isSkipVisit,
        visitStartDate
    }) {
        // 1. Create a dashboard for visit start
        // 2. Add report variables
        // 3. Create transmission item

        // 1. Create a dashboard for visit start
        // 2. Add report variables
        let questionnaireId = 'P_VisitStart',
            user = LF.security.activeUser,
            currentDate = new Date(),
            started = isSkipVisit ? currentDate.ISOStamp() : startDate.ISOStamp(),
            reportDate = isSkipVisit ? DateTimeUtil.convertToDate(currentDate) : DateTimeUtil.convertToDate(startDate),
            eventStartDate = isSkipVisit ? utils.buildStudyWorksFormat(DateTimeUtil.dateLocalTZFromISO8601(visitStartDate)) : utils.buildStudyWorksFormat(startDate),
            dashboard;

        let generateAnswers = () => {
            let createAnswer = (questionId, response, alias) => {
                return new Answer({
                    subject_id: subject.get('subject_id'),
                    question_id: questionId,
                    questionnaire_id: questionnaireId,
                    response,
                    SW_Alias: alias || `VisitStartDate.0.${questionId}`,
                    dashboardId: dashboard.get('id')
                })
                .save();
            };

            return Q([
                createAnswer('EventStartDate', eventStartDate),
                createAnswer('EventID', studyEventId),
                createAnswer('AFFIDAVIT', '1', 'CustomAffidavit'),
                createAnswer('Protocol', this.getProtocol(subject), 'Protocol.0.Protocol')
            ]);
        };

        let createTransmission = () => {
            let transmission = new Transmission({
                method: 'transmitQuestionnaire',
                params: JSON.stringify({
                    dashboardId: dashboard.get('id'),
                    sigId: dashboard.get('sig_id')
                }),
                created: currentDate.getTime(),
                hideCount: true
            });

            if (subject.get('isDuplicate')) {
                transmission.set('status', 'failed');
            }

            return transmission.save();
        };

        let saveDashboard = (batteryLevel) => {
            dashboard = new Dashboard({
                user_id: user ? user.get('id') : '',
                subject_id: subject.get('subject_id'),
                device_id: lStorage.getItem('deviceId') || lStorage.getItem('IMEI') || subject.device_id || subject.get('device_id'),
                questionnaire_id: questionnaireId,
                SU: 'VisitStart',
                started,
                report_date: reportDate,
                phase: subject.get('phase'),
                phaseStartDateTZOffset: subject.get('phaseStartDateTZOffset'),
                study_version: LF.StudyDesign.studyVersion,
                ink: '',
                sig_id: this.createSigId(currentDate),
                krpt: subject.get('krpt'),
                responsibleParty: isSkipVisit ? user.get('username') : lStorage.getItem('studyDbName'),
                completed: currentDate.ISOStamp(),
                completed_tz_offset: currentDate.getOffset(),
                diary_id: parseInt(currentDate.getTime().toString() + currentDate.getMilliseconds().toString(), 10),
                battery_level: batteryLevel
            });

            return dashboard.save();
        };

        return getBatteryLevel()
        .then(batteryLevel => saveDashboard(batteryLevel))
        .tap(() => generateAnswers())
        .tap(() => {
            return this.createEventAtEntryVariables({
                studyEventId,
                startDate: eventStartDate,
                offset: DateTimeUtil.tzOffsetInMillis(isSkipVisit ? currentDate : startDate),
                questionnaireId,
                dashboardId: dashboard.get('id'),
                subject
            });
        })
        .then(() => createTransmission())
        .then(() => dashboard.get('sig_id'));
    }

    /**
     * Get the correct protocol from a subject.
     * @param {Subject} subject - The target subject.
     * @returns {string} Stringified protocol.
     */
    static getProtocol (subject) {
        let dflt = utils.getNested('StudyDesign.studyProtocol.defaultProtocol', LF);

        // DE23136 - NetPro's subject sync sends empty string values for custom1-10.
        // I've changed the condition to send the default protocol for any falsy values.
        // StudyWorks also changed to reject transmissions with unknown protocols, e.g. "" or "0",
        // so I modified the StudyDesign to use -9999, which is <UnspecifiedProtocol> in SW.
        return subject.get('custom10') ? subject.get('custom10') : dflt.toString();
    }

    /**
     * Creates end visit report.
     * @param {Object} options - options.
     * @param {number} options.visitStateCode - (1 Skipped, 2 Incomplete, 3 Complete).
     * @param {number} options.visit - current visit.
     * @param {number} options.skippedReason - reason code for skipping visits.
     * @param {Date} options.visitStartDate - The start date of the visit.
     * @param {Object} options.subject - Subject
     * @param {string} options.ink - Ink
     * @param (boolean) options.visitExpired - indicates if visit is expired
     * @returns {Q.Promise<Object>}
     */
    static createEndVisitReport ({
        visitStateCode,
        visit,
        skippedReason,
        visitStartDate,
        subject,
        ink,
        visitExpired = false
    }) {
        let user = LF.security.activeUser,
            currentDate = new Date(),
            reportDate = DateTimeUtil.convertToDate(currentDate),
            visitExpiration = visit.get('expiration'),
            expirationParam = visitExpiration && visitExpiration.functionParams && visitExpiration.functionParams.timeoutMinutes,
            eventEndDate = this.calculateVisitEndDate(visitExpired, visitStartDate, expirationParam) || currentDate,
            studyEventId = visit.get('studyEventId'),
            dashboard = new Dashboard({
                user_id: user ? user.get('id') : '',
                subject_id: subject.get('subject_id'),
                device_id: lStorage.getItem('deviceId') || lStorage.getItem('IMEI') || subject.device_id || subject.get('device_id'),
                questionnaire_id: 'P_VisitEnd',
                SU: 'VisitEnd',
                started: currentDate.ISOStamp(),
                report_date: reportDate,
                phase: subject.get('phase'),
                phaseStartDateTZOffset: subject.get('phaseStartDateTZOffset'),
                study_version: LF.StudyDesign.studyVersion,
                ink: ink ? JSON.stringify(ink) : '',

                // add extra millisecond to current date when creating sig_id. After time travel Date is updated every 1000ms
                // and same value is returned within same second. This value should be different than sig_id of VisitStart (see DE20558).
                sig_id: this.createSigId(currentDate, 1),
                krpt: subject.get('krpt'),
                responsibleParty: skippedReason ? user.get('username') : lStorage.getItem('studyDbName')
            });

        let saveDashboard = (batteryLevel) => {
            return dashboard.save({
                completed: currentDate.ISOStamp(),
                completed_tz_offset: currentDate.getOffset(),
                diary_id: parseInt(currentDate.getTime().toString() + currentDate.getMilliseconds().toString(), 10),
                battery_level: batteryLevel
            });
        };

        let createTransmission = () => {
            let transmission = new Transmission({
                method: 'transmitQuestionnaire',
                params: JSON.stringify({
                    dashboardId: dashboard.get('id'),
                    sigId: dashboard.get('sig_id')
                }),
                created: currentDate.getTime(),
                hideCount: true
            });

            if (subject.get('isDuplicate')) {
                transmission.set('status', 'failed');
            }

            return transmission.save();
        };

        let generateAnswers = () => {
            let createAnswer = (questionId, response, alias) => {
                return new Answer({
                    subject_id: subject.get('subject_id'),
                    question_id: questionId,
                    questionnaire_id: 'P_VisitEnd',
                    response,
                    SW_Alias: alias || `VisitEndDate.0.${questionId}`,
                    dashboardId: dashboard.get('id')
                })
                .save();
            };

            return Q.all([
                createAnswer('EventEndDate', utils.buildStudyWorksFormat(eventEndDate)),
                createAnswer('EventID', studyEventId),
                createAnswer('ReasonforSkippingVisit', skippedReason || ''),
                createAnswer('VisitComplete', (visitStateCode === 3) ? '1' : '0'),
                createAnswer('VisitStatus', visitStateCode.toString()),
                createAnswer('AFFIDAVIT', '1', 'CustomAffidavit'),
                createAnswer('Protocol', this.getProtocol(subject), 'Protocol.0.Protocol')
            ]);
        };

        let createEntryVariables = () => {
            let visitStartTimeStamp = DateTimeUtil.dateLocalTZFromISO8601(visitStartDate);

            return this.createEventAtEntryVariables({
                studyEventId,
                startDate: utils.buildStudyWorksFormat(visitStartTimeStamp),
                offset: DateTimeUtil.tzOffsetInMillis(visitStartTimeStamp),
                questionnaireId: 'P_VisitEnd',
                dashboardId: dashboard.get('id'),
                subject
            });
        };

        return getBatteryLevel()
        .then(batteryLevel => saveDashboard(batteryLevel))
        .then(() => generateAnswers())
        .then(() => createEntryVariables())
        .then(() => createTransmission())
        .then(() => {
            // TODO: Rename ISOLocalTZStamp() to meet code guidelines.
            // eslint-disable-next-line new-cap
            return { sig_id: dashboard.get('sig_id'), eventEndDate: eventEndDate.ISOLocalTZStamp() };
        });
    }

    static isVisitExpired (visit, userVisit, subject) {
        let expiration = visit.get('expiration'),
            availabilityFunction;

        if (!expiration) {
            return Q(false);
        }

        availabilityFunction = LF.Visits.availabilityFunctions[expiration.function];

        if (!availabilityFunction) {
            return Q(false);
        }

        return availabilityFunction({ expiration, visit, userVisit, subject });
    }

    checkVisitAvailability (visit, userVisit, subject) {
        let visitAvailability = visit.get('availability'),
            functionList = [];

        if (!visitAvailability) {
            return Q(true);
        }

        for (let i = 0; i < visitAvailability.length; i++) {
            let availabilityFunction = LF.Visits.availabilityFunctions[visitAvailability[i].function];

            if (availabilityFunction) {
                functionList.push(availabilityFunction({ avaliability: visitAvailability[i], visit, userVisit, subject }));
            }
        }

        return Q.all(functionList)
        .then((results) => {
            for (let i = 0; i < results.length; i++) {
                if (results[i] === false) {
                    // stop evaluating and return action
                    return visitAvailability[i].actionNotAvailable;
                }
            }

            return LF.StudyDesign.visitAction.show;
        });
    }
}

COOL.add('VisitService', VisitService);
