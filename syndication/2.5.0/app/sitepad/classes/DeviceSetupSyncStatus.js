import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';
import Logger from 'core/Logger';

let logger = new Logger('Sitepad:DeviceSetupSyncStatus');
let internetConnected,
    dataTransmitted;

/**
 * The class to store the sync status for tablet setup
 * @class DeviceSetupSyncStatus
 */
export default class DeviceSetupSyncStatus {
    /**
     * Sets the current internet connection status
     * @param {boolean} value - The result of internet connection check
     * @static
     */
    static set internetConnected (value) {
        internetConnected = value;
    }

    /**
     * Returns the current internet connection status
     * @returns {boolean} The result of internet connection check
     * @static
     */
    static get internetConnected () {
        return internetConnected;
    }

    /**
     * Sets the value for first site user transmission status
     * @param {boolean} value - The result of first site user transmission
     * @static
     */
    static set dataTransmitted (value) {
        dataTransmitted = value;
    }

    /**
     * Returns the current first site user transmission
     * @returns {boolean} The result of first site user transmission
     * @static
     */
    static get dataTransmitted () {
        return dataTransmitted;
    }

    /**
     * Gets the internet connection status and set the flags according to the results.
     * @returns {Q.Promise<void>}
     */
    static syncStatusHandler () {
        let service = COOL.new('WebService', WebService);

        return service.getInternetConnectionStatus()
        .then(() => {
            DeviceSetupSyncStatus.internetConnected = true;
            DeviceSetupSyncStatus.dataTransmitted = false;
            logger.info('internet connection check succeeded');
        })
        .catch(() => {
            DeviceSetupSyncStatus.internetConnected = false;
            DeviceSetupSyncStatus.dataTransmitted = false;
            logger.error('internet connection check failed');
        });
    }
}

