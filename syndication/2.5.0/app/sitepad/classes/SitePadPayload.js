import COOL from 'core/COOL';
import Payload from 'core/classes/Payload';

/*
 * @typedef {Payload~Questionnaire} SitePadPayload~Questionnaire
 * @property {string} K - The KRPT of the target patient.
 * @property {string} M - The party reponsible for the edits to the patient record. e.g. 'SysAdmin'
 */

/*
 * @typedef {Payload~QuestionnaireInput} SitePadPayload~QuestionnaireInput
 * @property {string} krpt - The KRPT of the target patient.
 * @property {string} responsibleParty - The party reponsible for the edits to the patient record. e.g. 'SysAdmin'
 */

/**
 * Utility for generating Tablet-based HTTP request payloads.
 * @extends Payload
 */
export default class SitePadPayload extends COOL.getClass('Payload', Payload) {
    /**
     * Generate a SitePad questionnaire payload.
     * @param {SitePadPayload~QuestionnaireInput} form - SitePad questionnaire form data.
     * @returns {SitePadPayload~Questionnaire}
     */
    static questionnaire (form) {
        let res = super.questionnaire(form);

        res.payload.K = form.krpt;
        res.payload.M = form.responsibleParty;

        return res;
    }

    /**
     * Generates a SPStartDate payload.  This is a Edit Patient form, but only changes the SPStartDate property.
     * @param {Payload~EditPatientInput}
     * @returns {Payload~EditPatient}
     */
    static sPStartDate (form) {
        let payload = {
            K: form.krpt,
            M: form.ResponsibleParty,
            U: 'Assignment',
            S: form.dateStarted,
            C: form.dateCompleted,
            R: form.reportDate,
            P: form.phase,
            E: LF.coreVersion,
            V: LF.StudyDesign.studyVersion,
            T: form.phaseStartDate,
            L: form.batteryLevel,
            J: form.sigID,
            A: this.answers([{
                response: '1',
                SW_Alias: 'SPStartDateAffidavit',
                question_id: 'AFFIDAVIT'
            }, {
                response: form.SPStartDate,
                SW_Alias: 'PT.SPStartDate'
            }])
        };

        payload.A = this.pack(payload.A);

        return payload;
    }

    static deviceId (subject) {
        return {
            U: subject.setupCode,
            W: subject.subject_password
        };
    }
}

COOL.add('Payload', SitePadPayload);
