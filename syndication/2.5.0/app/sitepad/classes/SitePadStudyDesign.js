import StudyDesign from 'core/classes/StudyDesign';
import COOL from 'core/COOL';

/**
 * Extended Study Design functionality for eCOA Tablet.
 * @class SitePadStudyDesign
 * @extends StudyDesign
 */
export default class SitePadStudyDesign extends COOL.getClass('StudyDesign', StudyDesign) {
    constructor (design) {
        super(design);

        // Only these configured roles can transcribe forms on behalf of a subject/patient.
        const transcriptionRoles = this.sitePad.transcriptionRoles;

        this.questionnaires.forEach((questionnaire) => {
            let { accessRoles, allowTranscriptionMode } = questionnaire.toJSON();

            // We only want to modify access roles for questionnaires that are available for transcription.
            if (allowTranscriptionMode) {
                // If the questionnaire is accessible to only subjects, we need to open availablity to admins and site users.
                if (!accessRoles || (accessRoles && accessRoles.indexOf('subject') !== -1)) {
                    // Union of two arrays with unique values only.
                    accessRoles = _.union(accessRoles || ['subject'], transcriptionRoles);

                    // The requireLogin flag is used to force a context switch on questionnaire start.
                    questionnaire.set({ accessRoles, requireLogin: true });
                }
            }
        });
    }
}

COOL.add('StudyDesign', SitePadStudyDesign);
