import BaseStartup from 'core/classes/BaseStartup';
import trial from 'core/index';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import COOL from 'core/COOL';

// The following components are included to be bootstrapped into COOL.
// Order is important!
// Disabled import due to DE23609
// import 'sitepad/classes/SitePadStudyDesign';
import 'sitepad/transmit';
import 'sitepad/classes/SitePadPayload';
import SitepadDatabaseVersionManager from 'sitepad/classes/SitepadDatabaseVersionManager';
import WebService from 'sitepad/classes/WebService';
import Router from 'sitepad/Router';
import RegistrationController from 'sitepad/controllers/RegistrationController';
import ApplicationController from 'sitepad/controllers/ApplicationController';
import SettingsController from 'sitepad/controllers/SettingsController';
import setupSitepadMessages from 'sitepad/resources/Messages';

let logger = new Logger('Startup');

/**
 * The class that manages the application startup process for Tablet modality
 */
export default class Startup extends BaseStartup {
    /**
     * Create the router.
     * @param {Object} [options={}] options passed into the Router's constructor.
     * @returns {Q.Promise<Router>}
     */
    createRouter (options = {}) {
        logger.traceEnter('routerStartup');

        return Q()
        .then(() => {
            const registration = new RegistrationController();
            const application = new ApplicationController();
            const settings = new SettingsController();
            const study = new trial.assets.StudyController();

            const routerOpts = _.extend({}, options.routerOpts, {
                controllers: _.extend({}, options.controllers, {
                    application, registration, settings, study
                })
            });

            return new Router(routerOpts);
        });
    }

    /**
     * Setup the application's router.
     * @param {Object} [options={}] - Options passed into the Router's constructor.
     * @returns {Q.Promise<void>}
     */
    backboneStartup (options = {}) {
        logger.traceEnter('backboneStartup');

        return Q()
        .then(() => this.createRouter(options))
        .then((router) => {
            LF.router = router;

            // Add the web service to the LF namespace.
            // As much as I don't like doing this, we still have a lot of code
            // using the LF.webService namespace.
            LF.webService = COOL.new('WebService', WebService);
        })
        .catch((err) => {
            logger.error('Error on Backbone startup.', err);
        })
        .finally(() => {
            logger.traceExit('backboneStartup');
        });
    }

    /**
     * Start Backbone.history
     */
    startUI () {
        logger.traceEnter('startUI');

        Backbone.history.start({ pushState: false, silent: true });

        // hack so backbone things route it changing.  Unfortunately it sets fragment to ''
        // if silent: true, even though it never actually navigated anywhere.
        Backbone.history.fragment = undefined;
        logger.traceExit('startUI');
    }

    /**
     * Startup the application.
     * @returns {Q.Promise<void>}
     */
    startup () {
        setupSitepadMessages();

        return this.preSystemStartup()
        .then(() => this.jQueryStartup())
        .then(() => this.cordovaStartup())
        .then(() => this.studyStartup())
        .then(() => SitepadDatabaseVersionManager.createDatabaseVersions())
        .then(() => this.backboneStartup())
        .then(() => this.setupUsersAndContext())
        .then(() => this.startUI())
        .then(() => ELF.trigger('APPLICATION:Loaded'))
        .then((res) => {
            // TODO remove the usage of this flag.
            localStorage.removeItem('questionnaireCompleted');

            if (!res.preventDefault) {
                LF.router.navigate('', true);
            }
        })
        .catch(err => err && logger.error('Error Loading Application', err));
    }
}
