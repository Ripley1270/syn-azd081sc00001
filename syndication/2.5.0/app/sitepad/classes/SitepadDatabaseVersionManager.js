import CoreDatabaseVersionManager from 'core/classes/DatabaseVersionManager';

/**
 * A class to manage database version related opreations in Tablet modality
 */
export default class SitepadDatabaseVersionManager extends CoreDatabaseVersionManager {
    /**
     * The attributes for the database version records
     * @property {Array} versionAttributes
     * @readonly
     * @static
     */
    static get versionAttributes () {
        let coreDbVersion = LF.coreDbVersion;

        // DatabaseVersion records do not exist in Tablet modality prior to 2.5 release
        // This is a workaround to avoid upgrade scripts for 2.5 not being run
        if (LF.coreDbVersion === 2.5) {
            coreDbVersion = 1;
        }

        return [{
            versionName: 'DB_Core',
            version: coreDbVersion
        }, {
            versionName: 'DB_Study',
            version: LF.StudyDesign.studyDbVersion
        }];
    }
}
