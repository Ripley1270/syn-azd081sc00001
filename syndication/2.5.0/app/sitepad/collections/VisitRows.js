import VisitRow from 'sitepad/models/VisitRow';

/**
 * A collection of VisitRow models.
 * @class {VisitRows<T>}
 * @extends {Backbone.Collection<T>}
 */
export default class VisitRows extends Backbone.Collection {
    /**
     * @property {VisitRow} model - The model class the collection contains.
     */
    get model () {
        return VisitRow;
    }
}
