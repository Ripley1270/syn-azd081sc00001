import { MessageRepo } from 'core/Notify';
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';
import ScreenshotSpinner from './ScreenshotSpinner';

/**
 * Include report-level messages for the purpose of taking screen shots.
 */
export default function setupSitepadScreenshotDialogs () {
    MessageRepo.add(
        MessageHelpers.createErrorBanner('USER_ALREADY_EXISTS'),
        {
            type: 'Dialog',
            key: 'EDIT_PATIENT_QUESTION_1_SCREENSHOT_SP',
            message: MessageHelpers.notifyDialogCreator({
                header: 'EDIT_PATIENT_QUESTION_1',
                message: 'EDIT_PATIENT_QUESTION_1_EMPTY',
                ok: 'OK'
            })
        }, {
            type: 'Dialog',
            key: 'EDIT_USER_CANCEL_CONFIRM_SCREENSHOT_SP',
            message: MessageHelpers.confirmDialogCreator({
                header: 'EDIT_USER_CANCEL_CONFIRM',
                message: 'EDIT_USER_CANCEL_BODY',
                ok: 'YES',
                cancel: 'NO',
                type: 'error'
            })
        }, {
            type: 'Dialog',
            key: 'TRAINER_EXIT_NOTIFICATION_SCREENSHOT_SP',
            message: MessageHelpers.confirmDialogCreator({
                message: 'TRAINER_EXIT_NOTIFICATION',
                ok: 'YES',
                cancel: 'NO',
                type: 'error'
            })
        }, {
            type: 'Dialog',
            key: 'EDIT_PATIENT_QUESTION_1_RANGE_HEADER_SCREENSHOT_SP',
            message: MessageHelpers.notifyDialogCreator({
                header: 'EDIT_PATIENT_QUESTION_1_RANGE_HEADER',
                message: 'EDIT_PATIENT_QUESTION_1_RANGE',
                ok: 'OK'
            })
        }, {
            type: 'Dialog',
            key: 'EDIT_PATIENT_QUESTION_1_UNIQUE_HEADER_SCREENSHOT_SP',
            message: MessageHelpers.notifyDialogCreator({
                header: 'EDIT_PATIENT_QUESTION_1_UNIQUE_HEADER',
                message: 'EDIT_PATIENT_QUESTION_1_UNIQUE',
                ok: 'OK'
            })
        }, {
            type: 'Dialog',
            key: 'EDIT_PATIENT_QUESTION_2_EMPTY_HEADER_SCREENSHOT_SP',
            message: MessageHelpers.notifyDialogCreator({
                header: 'EDIT_PATIENT_QUESTION_2_EMPTY_HEADER',
                message: 'EDIT_PATIENT_QUESTION_2_EMPTY',
                ok: 'OK'
            })
        }, {
            type: 'Dialog',
            key: 'SPINNER_SCREENSHOT_SP',
            message: () => {
                let customSpinner = new ScreenshotSpinner();
                return customSpinner.show()
                .delay(1000)
                .then(() => customSpinner.hide());
            }
        });
}
