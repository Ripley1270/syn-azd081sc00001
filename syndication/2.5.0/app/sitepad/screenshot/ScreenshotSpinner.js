import SpinnerView from 'core/views/SpinnerView';

/**
 * @classdesc Spinner without animation. (Screen shots do not work with animated images.)
 */
export default class ScreenshotSpinner extends SpinnerView {
    /** @inheritdoc */
    render (strings) {
        super.render(strings);
        this.$el.find('i').removeClass('fa-pulse');
    }
}
