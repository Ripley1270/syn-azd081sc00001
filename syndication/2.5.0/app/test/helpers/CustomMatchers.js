jasmine.addMatcher = function (name, messageTemplates, checkFn, formatValueFn = jasmine.pp) {
    jasmine.addMatchers({
        [name]: function () {
            messageTemplates = [].concat(messageTemplates);
            let normalMessage = messageTemplates[0]
                .replace(' (not)', '')
                .replace(' (NOT)', '');
            let negatedMessage = messageTemplates[1] ||
                messageTemplates[0]
                    .replace('(not)', 'not')
                    .replace('(NOT)', 'NOT');

            return {
                compare: function (value) {
                    let pass = checkFn(value);
                    let message = (pass ? negatedMessage : normalMessage)
                        .replace('{}', formatValueFn(value));
                    return { pass, message };
                }
            };
        }
    });
};

beforeEach(() => {
    jasmine.addMatcher('toBePromise', 'Expected (NOT) to be a promise.', (it) => Q.isPromise(it));
    jasmine.addMatcher('toHaveBeenFulfilled', 'Expected promise (NOT) to be a resolved.', (it) => it.isFulfilled());
    jasmine.addMatcher('toHaveBeenRejected', 'Expected promise (NOT) to be rejected.', (it) => it.isRejected());
    jasmine.addMatcher('toBeArray', 'Expected (NOT) to be an Array.', (it) => Array.isArray(it));

    // jasmine.addMatchers({
    //     toBePromise: () => {
    //         return {
    //             compare: promise => {
    //                 let result = { pass: Q.isPromise(promise) };

    //                 if (result.pass) {
    //                     result.message = 'Expected NOT to be a promise.';
    //                 } else {
    //                     result.message = 'Expected to be a promise.';
    //                 }

    //                 return result;
    //             }
    //         };
    //     },
    //     toHaveBeenFulfilled : () => {
    //         return {
    //             compare: promise => {
    //                 let result = { pass: promise.isFulfilled() };

    //                 if (result.pass) {
    //                     result.message = 'Expected promise NOT to be resolved';
    //                 } else {
    //                     result.message = 'Expected promise to be resolved.';
    //                 }

    //                 return result;
    //             }
    //         };
    //     },
    //     toHaveBeenRejected: () => {
    //         return {
    //             compare: promise => {
    //                 let result = { pass: promise.isRejected() };

    //                 if (result.pass) {
    //                     result.message = 'Expected promise NOT to be rejected';
    //                 } else {
    //                     result.message = 'Expected promise to be rejected.';
    //                 }

    //                 return result;
    //             }
    //         };
    //     },
    //     toBeArray: () => {
    //         return {
    //             compare: value => {
    //                 let result = { pass: Array.isArray(value) };

    //                 if (result.pass) {
    //                     result.message = 'Expected NOT to be an Array.';
    //                 } else {
    //                     result.message = 'Expected to be an Array.';
    //                 }

    //                 return result;
    //             }
    //         };
    //     }
    // });
});
