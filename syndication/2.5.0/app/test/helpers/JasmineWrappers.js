import ELF from 'core/ELF';
import { eCoaDB, LogDB } from 'core/dataAccess';

let isPhantom = navigator.userAgent.match('PhantomJS');

// Long stack support gives more useful stack traces from Q.
// It has a downside: all Q defer's will cause a dummy throw Error()
// which is a hassle.  Tip: in debugger, 'blackbox' q.js
if (!isPhantom) {
    Q.longStackSupport = true;
}

// The point of all the following is to know at any given time,
// which test case is running; this can be useful when logging,
// or creating test data.
//
// The rest is fluff: it optionally displays test case name in browser
// as tests are being run.
//
// Tried changing this to class, and specDone was no longer called,
// though specStarted still was.
// More info at https://goo.gl/RGGv3A
function CaptureCurrentSpec (options = {}) {
    // @todo investigate about options, and why timer is required
    options = _.extend({
        timer: new jasmine.Timer()
    }, options);
    jasmine.JsApiReporter.call(this, options);
    this.currentSpec = null;
    this.html = options.html;

    this.specStarted = (spec) => {
        //console.log('>>>    ', spec.fullName);
        this.currentSpec = spec;

        // just playing now...
        if (this.html) {
            try {
                let label = document.createElement('div');
                label.id = spec.id;
                label.innerHTML = `<b><i>${spec.fullName}...</i></b>`;
                document.body.appendChild(label);
            } catch (e) {
                // eslint-disable-next-line no-console
                console.error(e);
            }
        }
    };

    this.specDone = (spec) => {
        //console.log('<<<    ', spec.fullName);
        this.currentSpec = null;

        // just playing now...
        if (this.html) {
            try {
                let label = document.getElementById(spec.id);
                switch (spec.status) {
                    case 'disabled':
                    case 'pending':
                        document.body.removeChild(label); // Don't care
                        break;
                    case 'failed':
                        // todo: cleaner dom manipulation
                        label.innerHTML = `<font color="red">${spec.fullName}: ${spec.status.toUpperCase()}</font>`;
                        break;
                    default:
                        label.innerHTML = `${spec.fullName}: ${spec.status.toUpperCase()}`;
                        break;
                }
            } catch (e) {
                // eslint-disable-next-line no-console
                console.error(e);
            }
        }
    };

    this.suiteStarted = (suite) => {
        this.currentSuite = suite;
        //console.log('>>>', suite.fullName);
    };

    this.suiteDone = (suite) => {
        this.currentSuite = null;
        //console.log('<<<', suite.fullName);
    };
}

export let currentSpecCapturer = new CaptureCurrentSpec({html: false});
jasmine.getEnv().addReporter(currentSpecCapturer);

export let clearDbs = () => {
    return _.reduce([eCoaDB, LogDB], (chain, db) => {
        return chain.then(() => {
            let clear = [];

            for (let store in db) {
                if (db[store].clear) {
                    // Introduced a slight delay to prevent version change errors with IndexedDB.
                    let request = Q.delay(100)
                    .then(() => db[store].clear());

                    clear.push(request);
                }
            }

            return Q.all(clear);
        });
    }, Q());
};

let wasted = 0; // tally up time spent in full resets
function fullReset () {
    let start = Date.now();

    ELF.rules.clear(); // TODO: should also clear actions and expressions

    restoreLF();

    localStorage.clear();
    localStorage.setItem('krpt', '*');

    return clearDbs()

    // eslint-disable-next-line no-console
    .catch(e => console.error(e))
    .finally(() => {
        let elapsed = Date.now() - start;
        wasted += elapsed;
        //console.log(`------------------------------ FULL RESET ${elapsed}ms / ${wasted / 1000}s ---------------------`);
    });
}

let _LF;
export function restoreLF () {
    window['LF'.trim()] = $.extend(true, {}, _LF);
}

// Add a function to be called once at the beginning of each spec file.
// This is different from doing beforeAll here; that would only run once
// total, not once per spec file
// DO NOT EXPORT this method, and do not call it from any spec file, only use here
let beforeEachSpecFile = (func, timeout) => {
    // querying for 'all spec files' can only happen after all
    // files have been processed, e.g. in a global beforeAll()
    beforeAll(() => {
        jasmine.getEnv().topSuite().children.forEach(spec => {
            spec.beforeAllFns.unshift({ fn: func, timeout: timeout});
        });
    });
};

// DO NOT EXPORT this method, and do not call it from any spec file, only use here
let afterEachSpecFile = (func, timeout) => {
    // querying for 'all spec files' can only happen after all
    // files have been processed, e.g. in a global beforeAll()
    beforeAll(() => {
        jasmine.getEnv().topSuite().children.forEach(spec => {
            spec.afterAllFns.push({ fn: func, timeout: timeout});
        });
    });
};

// This is done in beforeAll, rather than top-level, so as to
// be sure to capture state after everything loaded, just before
// tests start running.  I.e. topSuite().children is populated by now.
beforeAll(() => {
    //console.log('---------------------------------- BEFORE ALL -----------------------------------');
    // make sure not to leave any lingering timeouts from other tests
    if (LF && LF.security)  {
        LF.security.pauseSessionTimeOut && LF.security.pauseSessionTimeOut();
        LF.security.stopQuestionnaireTimeOut && LF.security.stopQuestionnaireTimeOut();
    }

    if (!_LF) {
        // Capture initial state of LF; able to restore it later
        _LF = $.extend(true, {}, LF);
    }
});

afterAll(() => {
    // make sure not to leave any lingering timeouts
    if (LF && LF.security && LF.security.pauseSessionTimeOut) {
        LF.security.pauseSessionTimeOut && LF.security.pauseSessionTimeOut();
        LF.security.stopQuestionnaireTimeOut && LF.security.stopQuestionnaireTimeOut();
    }
});

beforeEachSpecFile((done) => {
    // The following installs a beforeAll on each top-level spec (file, basically)
    // The cost of fullReset is small, we could afford to do it beforeEach even, but
    // no doubt some tests would break because they intentionally pass state from
    // it() to it().  That's tolerable (if only barely). But passing state from spec
    // to spec (files) is beyond reasonable.
    fullReset()

        // eslint-disable-next-line no-console
        .catch(e => console.error(e))
        .done(done);
});
