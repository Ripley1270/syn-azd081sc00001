import Session from 'web/classes/Session';
import CoreSession from 'core/classes/Session';
import * as lStorage from 'core/lStorage';
import WebService from 'web/classes/WebService';
import User from 'core/models/User';

class SessionSuite {
    constructor () {
        beforeEach(() => this.beforeEach());

        afterEach(() => this.afterEach());
    }

    beforeEach () {
        LF.router = jasmine.createSpyObj('router', ['navigate']);

        this.session = new Session();
    }

    afterEach () {
        delete this.session;
    }

    testLogin () {
        TRACE_MATRIX('DE20424')
        .TRACE_MATRIX('DE20551')
        .describe('method:login', () => {
            Async.it('should log out of the application, but not navigate.', () => {
                spyOn(lStorage, 'removeItem').and.stub();

                let request = this.session.logout();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(lStorage.removeItem).toHaveBeenCalledWith('User_Login');
                    expect(lStorage.removeItem).toHaveBeenCalledWith('isAuthorized');

                    expect(LF.router.navigate).not.toHaveBeenCalled();
                });
            });

            Async.it('should log out of the application and navigate to the login view.', () => {
                spyOn(lStorage, 'removeItem').and.stub();

                let request = this.session.logout(true);

                expect(request).toBePromise();

                return request.then(() => {
                    expect(lStorage.removeItem).toHaveBeenCalledWith('User_Login');
                    expect(lStorage.removeItem).toHaveBeenCalledWith('isAuthorized');

                    expect(LF.router.navigate).toHaveBeenCalledWith('login', true);
                });
            });
        });
    }

    testUserLockout () {
        TRACE_MATRIX('US7734')
        .describe('method:checkLockout', () => {
            beforeEach(() => {
                LF.security = new Session();
                LF.StudyDesign = {
                    maxLoginAttempts: 3
                };
            });

            Async.it('should return true for a locked out user', () => {
                let result = {
                        res: {
                            locked: true,
                            user: 0
                        }
                    },
                    user = new User({
                        id: 1,
                        username: 'aa',
                        role: 'admin',
                        isLocked: true
                    });

                spyOn(LF.security, 'getLoginFailure').and.returnValue(2);
                spyOn(lStorage, 'getItem').and.returnValue('0001');
                spyOn(WebService.prototype, 'checkUserLockout').and.resolve(result);
                spyOn(WebService.prototype, 'setUserLockout').and.resolve();
                spyOn(User.prototype, 'save').and.resolve();

                let request = this.session.checkUserLockout(user);
                expect(request).toBePromise();

                return request.then((res) => {
                    expect(res).toBe(true);
                });
            });

            Async.it('should return false for a user not locked out', () => {
                let result = {
                        res: {
                            locked: false,
                            user: 0
                        }
                    },
                    user = new User({
                        id: 1,
                        username: 'aa',
                        role: 'admin',
                        isLocked: false
                    });

                spyOn(LF.security, 'getLoginFailure').and.returnValue(1);
                spyOn(lStorage, 'getItem').and.returnValue('0001');
                spyOn(WebService.prototype, 'checkUserLockout').and.resolve(result);
                spyOn(WebService.prototype, 'setUserLockout').and.resolve();
                spyOn(User.prototype, 'save').and.resolve();

                let request = this.session.checkUserLockout(user);
                expect(request).toBePromise();

                return request.then((res) => {
                    expect(res).toBe(false);
                });
            });
        });
    }
}

describe('Session', () => {
    let suite = new SessionSuite();

    suite.testLogin();
    suite.testUserLockout();
});
