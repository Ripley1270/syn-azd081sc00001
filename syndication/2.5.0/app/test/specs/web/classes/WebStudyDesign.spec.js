import WebStudyDesign from 'web/classes/WebStudyDesign';

import { rawStudyDesign } from 'test/helpers/StudyDesign';

class WebStudyDesignSuite {
    constructor () {
        beforeAll(() => this.beforeAll());
    }

    // Using beforeAll instead of beforeEach to prevent a constant stream of validation errors in the logs
    beforeAll () {
        const design = _.extend({}, rawStudyDesign);

        design.affidavits = [{
            id: 'SignatureAffidavit',
            widget: {
                type: 'SignatureBox'
            }
        }, {
            id: 'P_SignatureAffidavit',
            widget: {
                type: 'SignatureBox'
            }
        }, {
            id: 'PasswordAffidavit',
            widget: {
                type: 'PasswordWidget',
                text: ['QUESTION_TEXT']
            }
        }, {
            id: 'P_PasswordAffidavit',
            widget: {
                type: 'PasswordWidget'
            }
        }, {
            id: 'CustomSignatureAffidavit',
            widget: {
                type: 'SignatureBox'
            }
        }];

        design.roles = [{
            id: 'subject',
            defaultAffidavit: 'P_SignatureAffidavit'
        }, {
            id: 'site',
            defaultAffidavit: 'SignatureAffidavit'
        }];

        design.sitePad = (design.sitePad || {});
        design.sitePad.transcriptionRoles = ['site'];

        design.questionnaires = [{
            id: 'Daily_Diary',
            SU: 'DAILY_DIARY',
            displayName: 'DISPLAY_NAME',
            className: 'DAILY_DIARY',
            affidavit: 'SignatureAffidavit',
            previousScreen: true,
            accessRoles: ['subject'],
            allowTranscriptionMode: true,
            screens: ['DAILY_DIARY_S_1', 'DAILY_DIARY_S_2', 'DAILY_DIARY_S_3']
        }, {
            id: 'Daily_Diary_2',
            SU: 'DAILY_DIARY',
            displayName: 'DISPLAY_NAME',
            className: 'DAILY_DIARY',
            affidavit: 'SignatureAffidavit',
            previousScreen: true,
            accessRoles: ['site', 'subject'],
            allowTranscriptionMode: true,
            screens: ['DAILY_DIARY_S_1', 'DAILY_DIARY_S_2', 'DAILY_DIARY_S_3']
        }, {
            id: 'Meds',
            SU: 'MEDS',
            displayName: 'DISPLAY_NAME',
            className: 'Meds',
            previousScreen: true,
            accessRoles: ['subject'],
            affidavit: 'P_SignatureAffidavit',
            screens: ['MEDS_S_1']
        }, {
            id: 'Meds_2',
            SU: 'MEDS',
            displayName: 'DISPLAY_NAME',
            className: 'Meds',
            previousScreen: true,
            accessRoles: ['subject'],
            affidavit: 'CustomSignatureAffidavit',
            screens: ['MEDS_S_1']
        }];

        this.design = new WebStudyDesign(design);
    }

    testAffidavits () {
        TRACE_MATRIX('US8120')
        .TRACE_MATRIX('DE21852')
        .TRACE_MATRIX('DE21853')
        .TRACE_MATRIX('DE21854')
        .TRACE_MATRIX('DE21863')
        describe('Display Alternative Affidavit', () => {
            it('should replace the Daily Diary affidavit with the PasswordAffidavit affidavit.', () => {
                let dailyDiary = this.design.questionnaires.findWhere({ id: 'Daily_Diary' });

                expect(dailyDiary.get('affidavit')).toBe('PasswordAffidavit');
            });

            it('should replace the Meds affidavit with the P_PasswordAffidavit affidavit.', () => {
                let medsDiary = this.design.questionnaires.findWhere({ id: 'Meds' });

                expect(medsDiary.get('affidavit')).toBe('P_PasswordAffidavit');
            });

            it('should replace the site role\'s default affidavit with the PasswordAffidavit affidavit.', () => {
                let site = this.design.roles.findWhere({ id: 'site' });

                expect(site.get('defaultAffidavit')).toBe('PasswordAffidavit');
            });

            it('should replace the subject role\'s default affidavit with the P_PasswordAffidavit affidavit.', () => {
                let subject = this.design.roles.findWhere({ id: 'subject' });

                expect(subject.get('defaultAffidavit')).toBe('P_PasswordAffidavit');
            });

            it('should replace the Meds_2 affidavit with a generated Password Affidavit.', () => {
                let dailyDiary = this.design.questionnaires.findWhere({ id: 'Meds_2' }),
                    id = 'PasswordAffidavit_CustomSignatureAffidavit';

                expect(dailyDiary.get('affidavit')).toBe(id);

                let aff = this.design.affidavits.findWhere({ id });

                expect(aff.get('widget').text).toEqual(['QUESTION_TEXT']);
            });

        });
    }
}

describe('WebStudyDesign', () => {
    let suite = new WebStudyDesignSuite();

    suite.testAffidavits();
});
