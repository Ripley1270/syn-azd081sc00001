import TabManager from 'web/classes/TabManager';
import * as lStorage from 'core/lStorage';
import ELF from 'core/ELF';

TRACE_MATRIX('US7956')
.describe('TabManager', () => {
    let tab,
        $input;

    beforeEach(() => {
        $input = $('<input id="active-tab" type="text" value="0" />');

        $('body').append($input);

        tab = new TabManager();
    });

    afterEach(() => {
        $input.remove();
        $input = null;
    });

    it('should have a predefined session tab ID.', () => {
        expect(tab.sessionTab).toBeDefined();
        expect(tab.sessionTab).toEqual(jasmine.any(String));
    });

    it('should have a predefined active tab ID.', () => {
        expect(tab.activeTab).toBeDefined();
        expect(tab.activeTab).toEqual(jasmine.any(String));
    });

    describe('method:isActiveTab', () => {
        it('should return true (tab is active).', () => {
            expect(tab.isActiveTab()).toBe(true);
        });

        it('should return false (tab is inactive).', () => {
            tab.activeTab = '12345';

            expect(tab.isActiveTab()).toBe(false);
        });
    });

    describe('method:activateTab', () => {
        it('should activate the tab.', () => {
            tab.activeTab = '12345';

            expect(tab.isActiveTab()).toBe(false);

            tab.activateTab();

            expect(tab.isActiveTab()).toBe(true);
        });
    });

    describe('method:handleUnload', () => {
        beforeEach(() => spyOn(lStorage, 'removeItem').and.stub());

        it('should do nothing.', () => {
            // Ensure the active tab is different than the session tab.
            tab.activeTab = '54321';

            tab.handleUnload();

            expect(lStorage.removeItem).not.toHaveBeenCalled();
        });

        it('should remove the active tab ID from localStorage.', () => {
            // Activate the current tab.
            tab.activateTab();

            tab.handleUnload();

            expect(lStorage.removeItem).toHaveBeenCalledWith('ACTIVE_TAB_ID');
        });
    });

    TRACE_MATRIX('DE22423')
    .TRACE_MATRIX('DE22461')
    .describe('property:isDuplicate', () => {
        it('should be false. Tab is not a duplicate.', () => {
            $input.val('0');

            expect(tab.isDuplicate).toBe(false);
        });

        it('should be true. Tab is duplicate.', () => {
            $input.val('1');

            expect(tab.isDuplicate).toBe(true);
        });

        it('should set the tab as a duplicate.', () => {
            tab.isDuplicate = true;

            expect($input.val()).toBe('1');
        });
    });

    TRACE_MATRIX('DE22423')
    .describe('method:checkDuplicates', () => {
        it('should set the tab to active.', () => {
            expect($input.val()).toBe('0');

            tab.checkDuplicates();

            expect($input.val()).toBe('1');
        });

        it('should replace the Session ID of the duplicate tab.', () => {
            let oldSession = tab.sessionTab;

            $input.val('1');
            tab.checkDuplicates();

            expect(tab.sessionTab).not.toEqual(oldSession);
        });
    });

    TRACE_MATRIX('DE22527')
    .describe('method:clear', () => {
        it('should clear the tab state.', () => {
            spyOn(TabManager, 'stopMonitoring').and.stub();

            tab.clear();

            expect(TabManager.stopMonitoring).toHaveBeenCalled();
            expect(tab.activeTab).toBe(null);
        });
    });

    describe('method:checkStatus', () => {
        beforeEach(() => spyOn(ELF, 'trigger').and.resolve());

        it('should do nothing.', () => {
            // Ensure the active tab is different than the session tab.
            tab.activeTab = '54321';

            tab.checkStatus();

            expect(ELF.trigger).toHaveBeenCalledWith('APPLICATION:InactiveTab', jasmine.any(Object), tab);
        });

        it('should trigger an ELF event.', () => {
            // Activate the current tab.
            tab.activateTab();

            tab.checkStatus();

            expect(ELF.trigger).not.toHaveBeenCalled();
        });
    });
});
