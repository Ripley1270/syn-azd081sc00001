import State from 'web/classes/State';
import { setState } from 'web/actions/setState';

TRACE_MATRIX('DE21308').
describe('setState', () => {
    Async.it('should set the application state.', () => {
        spyOn(State, 'set').and.stub();

        let request = setState('locked');

        expect(request).toBePromise();

        return request.then(() => {
            expect(State.set).toHaveBeenCalledWith(State.states.locked);
        });
    });

    Async.it('should set the application state if the state value is provided instead of the key.', () => {
        spyOn(State, 'set').and.stub();

        let request = setState('LOCKED');

        expect(request).toBePromise();

        return request.then(() => {
            expect(State.set).toHaveBeenCalledWith(State.states.locked);
        });
    });
});