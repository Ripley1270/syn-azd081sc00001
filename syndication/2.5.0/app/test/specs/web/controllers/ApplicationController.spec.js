import ApplicationController from 'web/controllers/ApplicationController';
import PaperQuestionnaireView from 'web/views/PaperQuestionnaireView';
import SetSiteTimeZoneView from 'web/views/SetSiteTimeZoneView';
import Subject from 'core/models/Subject';
import Visit from 'core/models/Visit';

describe('ApplicationController', () => {
    let ctrl,
        visit,
        subject;

    beforeEach(() => {
        ctrl = new ApplicationController();
        subject = new Subject({ id: 1 });
        visit = new Visit({ id: 1 });
    });

    describe('method:transcribe', () => {
        Async.it('should navigate to the PaperQuestionnaireView.', () => {
            spyOn(ctrl, 'authenticateThenGo').and.resolve();

            ctrl.transcribe('Daily', { subject, visit });

            return Q().then(() => {
                expect(ctrl.authenticateThenGo).toHaveBeenCalledWith('PaperQuestionnaireView', PaperQuestionnaireView, {
                    id: 'Daily',
                    subject,
                    visit
                });
            });
        });
    });

    TRACE_MATRIX('DE21314')
    .describe('method:setSiteTimeZone', () => {
        Async.it('should authenticate the user prior to navigating to the SetSiteTimezoneView.', () => {
            spyOn(ctrl, 'authenticateThenGo').and.resolve();

            ctrl.setSiteTimeZone()

            return Q().then(() => {
                expect(ctrl.authenticateThenGo).toHaveBeenCalledWith('SetSiteTimeZoneView', SetSiteTimeZoneView);
            });
        });
    });
});