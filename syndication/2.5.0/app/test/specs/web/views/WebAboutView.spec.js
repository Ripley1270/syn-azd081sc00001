import * as lStorage from 'core/lStorage';
import * as DateTimeUtil from 'core/DateTimeUtil';
import Sites from 'core/collections/Sites';
import User from 'core/models/User';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';
import WebAboutView from 'web/views/WebAboutView';
import WebInfoView from 'web/views/WebInfoView';

TRACE_MATRIX('US8508')
.describe('WebAboutView Redesign', () => {
    let view,
        aboutTemplate,
        infoTemplate,
        userData = { id: 1, username: 'batman', language: 'en-US' };

    beforeEach(() => {
        resetStudyDesign();
        aboutTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/about.ejs');

        infoTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/info.ejs');

        LF.security = LF.security || {};
        LF.security.activeUser = new User(userData);

        spyOn(lStorage, 'getItem').and.callFake((key) => {
            switch (key) {
                case 'serviceBase':
                    return 'https://pht-syn-study01.phtnetpro.com';
                case 'deviceId':
                    return '675';
                case 'IMEI':
                    return '990000862471854';
                case 'lastRefreshTime':
                    return '2016-12-02T13:09:01.050';
                case 'mode':
                    return 'web';
                default:
                    return null;
            }
        });

        spyOn(Sites, 'fetchCollection').and.resolve(new Sites({ siteCode: '0008' }));

        spyOn(DateTimeUtil, 'getLocalizedDate').and.callFake(dt => 'Friday, December 02, 2016 08:09');

        view = new WebAboutView();
    });

    afterEach(() => {
        aboutTemplate();
        infoTemplate();
    });

    TRACE_MATRIX('DE23714')
    .describe('method:constructInfoView', () => {
        it('returns instance of WebInfoView', () => {
            let i = view.constructInfoView();
            expect(i instanceof WebInfoView).toBe(true);
        });
    });

    describe('render', () => {
        Async.beforeEach(() => {
            return view.render();
        });

        it('should render the DCR button on the web modality', () => {
            const btn = view.$('#btn-dcr')[0];
            expect(btn.classList).not.toContain('hidden');
        });

        it('should render the Visit button on the web modality', () => {
            const btn = view.$('#btn-visit')[0];
            expect(btn.classList).not.toContain('hidden');
        });

        it('should hide the network settings FAQ on the web modality', () => {
            const network = view.$('#network-parent')[0];
            expect(network.classList).toContain('hidden');
        });

        it('should hide the timezone panel on the web modality', () => {
            const tz = view.$('#timezone-parent')[0];
            expect(tz.classList).toContain('hidden');
        });
    });

    describe('events', () => {
        Async.beforeEach(() => {
            return view.render();
        });

        TRACE_MATRIX('DE23769')
        .describe('event:goToGSSO', () => {
            it('should trigger goToGSSO() when clicking the Visit button', () => {
                spyOn(view, 'goToGSSO').and.callFake($.noop);
                view.delegateEvents();

                view.$('#btn-visit').trigger('click');

                expect(view.goToGSSO).toHaveBeenCalled();
            });

            it('should trigger goToGSSO when clicking the DCR button', () => {
                spyOn(view, 'goToGSSO').and.callFake($.noop);
                view.delegateEvents();

                view.$('#btn-dcr').trigger('click');

                expect(view.goToGSSO).toHaveBeenCalled();
            });

            it('should blur the target element and call window.open', () => {
                let e = {
                    target: {
                        blur: $.noop
                    }
                };
                spyOn(e.target, 'blur');
                spyOn(window, 'open');

                view.goToGSSO(e);

                expect(e.target.blur).toHaveBeenCalled();
                expect(window.open).toHaveBeenCalled();
            });
        });

        describe('event:back', () => {
            it('should navigate to "about" if back button is clicked', () => {
                spyOn(view, 'navigate').and.callFake($.noop);
                view.delegateEvents();

                view.$('#back').trigger('click');

                expect(view.navigate).toHaveBeenCalledWith('settings');
            });
        });
    });
});
