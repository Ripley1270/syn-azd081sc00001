import PaperQuestionnaireView from 'web/views/PaperQuestionnaireView';
import SitePadQuestionnaireView from 'sitepad/views/SitePadQuestionnaireView';
import Screen from 'core/models/Screen';
import Dashboard from 'core/models/Dashboard';
import Answers from 'core/collections/Answers';

import { resetStudyDesign } from 'test/helpers/StudyDesign';

class PaperQuestionnaireViewSuite {
    constructor () {
        Async.beforeEach(() => this.beforeEach());
    }

    beforeEach () {
        resetStudyDesign();

        //LF.security = jasmine.createSpyObj('security', []);

        LF.StudyDesign.screens.add({
            id: 'TRANSCRIPTION_REPORT_DATE_S_1'
        });

        LF.StudyDesign.questionnaires.add({
            id: 'EQ5D',
            SU: 'EQ5D',
            displayName: 'DISPLAY_NAME',
            className: 'EQ5D',
            screens: ['EQ5D_S_1', 'EQ5D_S_2', 'EQ5D_S_3']
        })

        this.view = new PaperQuestionnaireView({ id: 'EQ5D' });

        return Q();
    }

    testTranscriptionViaPaper () {
        describe('property:transcriptionViaPaper', () => {
            it('should have transcriptionViaPaper set to true.', () => {
                expect(this.view.transcriptionViaPaper).toBe(true);
            });
        });
    }

    testPrepSingleScreen () {
        describe('method:prepSingleScreen', () => {
            it('should create a single screen with all questions.', () => {
                this.view.model.set('screens', ['SCREEN_1', 'SCREEN_2']);
                this.view.data.screens = [];

                LF.StudyDesign.screens.add([{
                    id: 'SCREEN_1',
                    questions: [{
                        id: 'QUESTION_1'
                    }]
                }, {
                    id: 'SCREEN_2',
                    questions: [{
                        id: 'QUESTION_2'
                    }]
                }]);

                LF.StudyDesign.questions.add([{
                    id: 'QUESTION_1'
                }, {
                    id: 'QUESTION_2'
                }]);

                this.view.prepSingleScreen();

                expect(this.view.data.screens.length).toBe(1);
                expect(this.view.data.screens[0].get('questions').length).toBe(2);
            });
        });
    }

    testSetReportDate () {
        describe('method:setReportDate', () => {
            beforeEach(() => {
                this.view.data.dashboard = new Dashboard();
                this.view.answers = new Answers([{
                    SW_Alias: 'TRANSCRIPTION_REPORT_DATE.0.DATE_TIME',
                    response: new Date()
                }]);

                spyOn(Dashboard.prototype, 'set').and.stub();

                this.view.screen = 0;
            });

            Async.it('shouldn\'t set the report date.', () => {
                let screen = new Screen({ id: 'SCREEN_S_1' });

                this.view.data.screens = [screen];

                let request = this.view.setReportDate();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(Dashboard.prototype.set).not.toHaveBeenCalled();
                });
            });

            Async.it('should set the report date.', () => {
                let screen = new Screen({ id: 'TRANSCRIPTION_REPORT_DATE_S_1' });

                this.view.data.screens = [screen];

                let request = this.view.setReportDate();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(Dashboard.prototype.set).toHaveBeenCalledWith('started', jasmine.any(String));
                });
            });
        });
    }

    testPrepScreens () {
        describe('method:prepScreens', () => {
            beforeEach(() => {
                spyOn(this.view, 'prepSingleScreen').and.stub();

                this.view.data.screens = [];
            });

            it('should add the report screen to the screen stack.', () => {
                this.view.prepScreens();

                expect(this.view.prepSingleScreen).toHaveBeenCalled();
                expect(this.view.screens.length).toBe(1);
                expect(this.view.screens[0].get('id')).toBe('TRANSCRIPTION_REPORT_DATE_S_1');
            });
        });
    }

    testValidateScreens () {
        describe('method:validateScreens', () => {
            beforeEach(() => {
                spyOn(SitePadQuestionnaireView.prototype, 'validateScreens').and.returnValue(false);
            
                this.view.screen = 0;
            });

            it('should validate the screen.', () => {
                let screen = new Screen({ id: `${this.view.model.get('id')}_S_1` });

                this.view.data.screens = [screen];

                expect(this.view.validateScreens()).toBe(true);
                expect(SitePadQuestionnaireView.prototype.validateScreens).not.toHaveBeenCalled();
            });

            it('should return false.', () => {
                let screen = new Screen({ id: 'TRANSCRIPTION_REPORT_DATE_S_1' });

                this.view.data.screens = [screen];

                expect(this.view.validateScreens()).toBe(false);
                expect(SitePadQuestionnaireView.prototype.validateScreens).toHaveBeenCalled();
            });
        });
    }
}

describe('PaperQuestionnaireView', () => {
    let suite = new PaperQuestionnaireViewSuite();

    suite.testTranscriptionViaPaper();
    suite.testPrepSingleScreen();
    suite.testSetReportDate();
    suite.testPrepScreens();
    suite.testValidateScreens();
});