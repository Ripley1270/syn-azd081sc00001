import Site from 'core/models/Site';
import Sites from 'core/collections/Sites';
import { getSiteSupportNumber, getSiteSupportNumberWithBreaks } from 'core/dynamicText/getSiteSupportNumber';
import { resetStudyDesign } from '../../../helpers/StudyDesign';
import { fakeGetStrings } from '../../../helpers/SpecHelpers';

TRACE_MATRIX('US8475')
.describe('getSiteSupportNumber', () => {
    let mockSite;
    beforeAll(() => {
        resetStudyDesign();
    });

    beforeEach(() => {
        fakeGetStrings();
        mockSite = new Site({ siteCode: '0001', countryCode: 'BE' });
    });

    it('has a screenshots property with 1 string in it', () => {
        expect(getSiteSupportNumber.screenshots.values.length).toBe(1);
        expect(typeof getSiteSupportNumber.screenshots.values[0]).toBe('string');
    });

    describe('getSiteSupportNumber #evaluate', () => {
        beforeEach(() => {
            spyOn(Sites, 'fetchFirstEntry').and.resolve(mockSite);
        });

        Async.it('calls fetchFirstEntry for site, returns strings for Belgium', () => {
            return getSiteSupportNumber.evaluate()
            .then((ret) => {
                expect(Sites.fetchFirstEntry).toHaveBeenCalled();
                expect(ret).toBe('SUPPORT_NUMBER_BELGIUM_1, SUPPORT_NUMBER_BELGIUM_2, SUPPORT_NUMBER_BELGIUM_3');
                expect(LF.strings.display).toHaveBeenCalled();
            });
        });

        Async.it('Returns not found string when country is not found.', () => {
            mockSite.set('countryCode', 'NOT A COUNTRY CODE');
            return getSiteSupportNumber.evaluate()
            .then((ret) => {
                expect(Sites.fetchFirstEntry).toHaveBeenCalled();
                expect(ret).toBe('SUPPORT_NUMBER_NOT_FOUND');
                expect(LF.strings.display).toHaveBeenCalled();
            });
        });
    });

    describe('getSiteSupportNumberWithBreaks #evaluate', () => {
        beforeEach(() => {
            spyOn(Sites, 'fetchFirstEntry').and.resolve(mockSite);
        });

        Async.it('calls fetchFirstEntry for site, returns strings for Belgium', () => {
            return getSiteSupportNumberWithBreaks.evaluate()
            .then((ret) => {
                expect(Sites.fetchFirstEntry).toHaveBeenCalled();
                expect(ret).toBe('SUPPORT_NUMBER_BELGIUM_1<br />SUPPORT_NUMBER_BELGIUM_2<br />SUPPORT_NUMBER_BELGIUM_3');
                expect(LF.strings.display).toHaveBeenCalled();
            });
        });

        Async.it('Returns not found string when country is not found.', () => {
            mockSite.set('countryCode', 'NOT A COUNTRY CODE');
            return getSiteSupportNumberWithBreaks.evaluate()
            .then((ret) => {
                expect(Sites.fetchFirstEntry).toHaveBeenCalled();
                expect(ret).toBe('SUPPORT_NUMBER_NOT_FOUND');
                expect(LF.strings.display).toHaveBeenCalled();
            });
        });
    });
});
