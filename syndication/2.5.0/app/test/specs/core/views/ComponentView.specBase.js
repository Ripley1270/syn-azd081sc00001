import { Banner, MessageRepo } from 'core/Notify';
import RouterBase from 'core/classes/RouterBase';
import * as specHelpers from 'test/helpers/SpecHelpers';

export default class ComponentViewTests {
    constructor () {
        Async.beforeAll(() => this.beforeAll());
        Async.beforeEach(() => this.beforeEach());

        Async.afterEach(() => this.afterEach());
        Async.afterAll(() => this.afterAll());
    }

    /**
     * Invoked before all unit tests are executed.
     * @returns {Q.Promise<void>}
     */
    beforeAll () {
        return Q();
    }

    /**
     * Invoked after all unit tests are executed.
     * @returns {Q.Promise<void>}
     */
    afterAll () {
        return Q();
    }

    /**
     * Default values to pass into each test method.
     * @type Object
     */
    get defaults () {
        return {
            id: 'component',
            tagName: 'div',
            template: '#page-tpl',
            dynamicStrings: { count: 10 },
            translationKeys: 'TRANSMISSIONS',
            button: '#submit',
            input: 'input'
        };
    }

    /**
     * Annotate a method's arguments from the provided context.
     * @param {Function} fn The target function to annotate.
     * @param {Object} context The context to pull argument values from.
     * @returns {Array} An array of arguments to pass into the target method.
     */
    annotate (fn, context) {
        const FN_ARGS = /^function\s*[^\(]*\(\s*([^\)]*)\)/m;
        const FN_ARG = /^\s*(_?)(.+?)\1\s*$/;
        const FN_ARG_SPLIT = /,/;
        const STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;

        let fnText = fn.toString().replace(STRIP_COMMENTS, '');
        let argDecl = fnText.match(FN_ARGS);
        let inject = [];

        _.forEach(argDecl[1].split(FN_ARG_SPLIT), (arg) => {
            arg.replace(FN_ARG, (all, underscore, name) => {
                inject.push(name);
            });
        });

        // Start of a chain with the suite's default values.
        let args = _.chain(context)

            // Mixin the passed in context.
            .defaults(this.defaults)

            // Pick only the matching argument names.
            .pick(inject)
            .values()
            .value();

        return () => fn.apply(this, args);
    }

    /**
     * Annotate all test methods.
     * @param {String[]} methods The list of method names.
     * @param {Object} context The context to populate the values with.
     */
    annotateAll (methods, context) {
        _.forEach(methods, (name, index) => {
            let isTestCase = name.match('^test.{1,}$');
            let isNotExcluded = !~context.exclude.indexOf(name);

            if (isTestCase && isNotExcluded) {
                this.annotate(this[name], context)();
            }
        });
    }

    /**
     * Execute all unit tests.
     * @param {Object} [context={}] The context of which to populate test method arguments from.
     */
    executeAll (context = {}) {
        let methods = Object.getOwnPropertyNames(ComponentViewTests.prototype);

        // Populate an exlcusion property if non exists.
        context.exclude = context.exclude || [];

        this.annotateAll(methods, context);
    }

    /**
     * Invoked before each unit test.
     * @returns {Q.Promise<void>}
     */
    beforeEach () {
        this.removeSpinnerTemplate = specHelpers.renderTemplateToDOM('core/templates/spinner.ejs');
        let template = '<div id="application"></div>';

        $('body').append(template);

        return Q();
    }

    /**
     * Invoked after each unit test.
     * @returns {Q.Promise<void>}
     */
    afterEach () {
        this.removeSpinnerTemplate();

        $('#application').remove();
        this.view.clearTemplateCache();

        // This ensures all event listeners are removed from the view, and all its children.
        this.view.remove();

        return Q();
    }

    /**
     * Execute before each DOM related unit test.
     * @param {Object} dynamicStrings A map of strings required for the view to render.
     * @returns {Q.Promise<Object>}
     */
    DOMBeforeEach (dynamicStrings = {}) {
        Async.beforeEach(() => {
            if (this.view.template == null) {
                let template = `<script id="page-tpl" type="text/template">
                        <div class="form-group">
                            <input id="username" type="text" />
                        </div>
                        <button id="submit" type="button" disabled="disabled">Submit</button>
                    </script>`;

                $('body').append(template);

                this.view.template = '#page-tpl';
            }

            return this.view.buildHTML(dynamicStrings);
        });
    }

    /**
     * Invoked after each DOM related unit test is executed.
     */
    DOMAfterEach () {
        afterEach(() => {
            if (this.view.template == null) {
                $('#page-tpl').remove();
            }
        });
    }

    /**
     * Tests the ID property of the view.
     * @param {String} [id=page] The id to test for.
     * @example
     * suite.testId('dashboard-view');
     */
    testId (id) {
        it(`should have an id of ${id}.`, () => {
            expect(this.view.id).toBe(id);
            expect(this.view.$el.attr('id')).toBe(id);
        });
    }

    /**
     * Tests the tagName property of the view.
     * @param {String} [tagName=div] The tag name.
     * @example
     * suite.testTagName('span');
     */
    testTagName (tagName) {
        it(`should have a tagName of ${tagName}.`, () => {
            expect(this.view.tagName).toBe(tagName);
        });
    }

    /**
     * Tests a property on the view.
     * @param {String} name The name of the property to test.
     * @param {Any} value The value to test for.
     * @example
     * suite.testProperty('template', '#dashboard-template');
     */
    testProperty (name, value) {
        it(`should have a property ${name} of ${value}.`, () => {
            expect(this.view[name]).toEqual(value);
        });
    }

    /**
     * Tests the className of the view.
     * @param {String} className The class to test for.
     * @example
     * suite.testClass('view');
     */
    testClass (className) {
        it(`should have a class of ${className}.`, () => {
            expect(this.view.className).toBe(className);
            expect(this.view.$el.attr('class')).toBe(className);
        });
    }

    /**
     * Tests for an attribute (view.attributes[attribute]).
     * @param {String} attribute The name of the attribute.
     * @param {Any} value The value of the attribute.
     * @example
     * suite.testAttribute('data-role', 'model');
     */
    testAttribute (attribute, value) {
        it(`should have an attribute ${attribute} with a value of ${value}.`, () => {
            expect(this.view.attributes[attribute]).toBe(value);
            expect(this.view.$el.attr(attribute)).toBe(value);
        });
    }

    /**
     * Test the view's getTemplate method.
     * @param {String} [selector=#page-tpl] The selector to use.
     */
    testGetTemplate (template) {
        describe('method:getTemplate', () => {
            beforeEach(() => {
                if (this.view.template == null) {
                    $('body').append(`<script id="page-tpl" type="text/template">
                        <h1>{{ greeting }}</h1>
                    </script>`);
                }
            });

            afterEach(() => $('#page-tpl').remove());

            it('should get the template (string).', () => {
                let tpl = this.view.getTemplate(template);

                expect(tpl).toBeDefined();
                expect(_.isFunction(tpl)).toBe(true);
            });

            it('should get the template ($).', () => {
                let tpl = this.view.getTemplate($(template));

                expect(tpl).toBeDefined();
                expect(_.isFunction(tpl)).toBe(true);
            });
        });
    }

    /**
     * Tests the view's buildHTML method.
     * @param {Object} [dynamicStrings={count:10}] Strings required to render the view.
     * @example
     * suite.testBuildHTML({ username: 'admin' });
     */
    testBuildHTML (dynamicStrings) {
        describe('method:buildHTML', () => {
            let template = `<script id="page-tpl" type="text/template">
                    <span>{{ count }}</span>
                </script>`;

            beforeEach(() => {
                if (this.view.template == null) {
                    $('body').append(template);
                    this.view.template = '#page-tpl';
                }
            });

            afterEach(() => $('#page-tpl').remove());

            Async.it('should fail to compile the template.', () => {
                spyOn(this.view, 'i18n').and.reject('DOMError');

                let request = this.view.buildHTML(dynamicStrings);

                expect(request).toBePromise();

                return request.then(() => fail('Expected buildHTML to fail'))
                    .catch((e) => {
                        expect(e).toBe('DOMError');
                    });
            });

            Async.it('should compile the template.', () => {
                let request = this.view.buildHTML(dynamicStrings);

                expect(request).toBePromise();

                return request.tap((strings) => {
                    expect(this.view.$el.attr('id')).toBe(this.view.id);
                });
            });
        });
    }

    /**
     * Tests the view's i18n method.
     * @param {Object|Array|String} [translationKeys=TRANSMISSIONS] Translation keys to pass to the i18n method.
     * @example
     * suite.testI18({ COUNT: 'Count' });
     */
    testI18n (translationKeys) {
        describe('method:i18n', () => {
            Async.it('should attempt to translate.', () => {
                // We really only care that it's being called.
                spyOn(LF.strings, 'display').and.resolve();
                return this.view.i18n(translationKeys, $.noop, { }).tap(() => {
                    expect(LF.strings.display)
                        .toHaveBeenCalledWith(translationKeys, jasmine.any(Function), jasmine.any(Object));
                });
            });
        });
    }

    /**
     * Tests the view's buildSelectors method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @example
     * suite.testBuildSelectors({ count: 10 });
     */
    testBuildSelectors (dynamicStrings) {
        describe('method:buildSelectors', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should build the selectors.', () => {
                if (this.view.selectors == null) {
                    this.view.selectors = {
                        submit: '#submit',
                        username: '#username'
                    };
                }

                this.view.buildSelectors();

                _.forEach(this.view.selectors, (value, key) => {
                    expect(this.view[`$${key}`]).toBeDefined();
                });
            });
        });
    }

    /**
     * Test the view's clearSelectors method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @example
     * suite.testClearSelectors({ count: 10 });
     */
    testClearSelectors (dynamicStrings) {
        describe('method:clearSelectors', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should clear the selectors.', () => {
                if (this.view.selectors == null) {
                    this.view.selectors = {
                        submit: '#submit',
                        username: '#username'
                    };
                }

                this.view.buildSelectors();
                _.forEach(this.view.selectors, (value, key) => {
                    expect(this.view[`$${key}`]).toBeDefined();
                });

                this.view.clearSelectors();
                _.forEach(this.view.selectors, (value, key) => {
                    expect(this.view[`$${key}`]).toBeFalsy();
                });
            });
        });
    }

    /**
     * Test the view's enableElement method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [button=#submit] The button of the element to enable.
     * @example
     * suite.testEnableElement({ count: 10 }, '#sync');
     */
    testEnableElement (dynamicStrings, button) {
        describe('method:enableElement', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            beforeEach(() => {
                this.view.$(button).attr('disabled', 'disabled');
            });

            it('should enable the element ($).', () => {
                let $ele = this.view.$(button);

                expect($ele.attr('disabled')).toBe('disabled');
                this.view.enableElement($ele);
                expect($ele.attr('disabled')).toBeFalsy();

                $ele = null;
            });

            it('should enable the element (string).', () => {
                let $ele = this.view.$(button);

                expect($ele.attr('disabled')).toBe('disabled');
                this.view.enableElement(button);
                expect($ele.attr('disabled')).toBeFalsy();

                $ele = null;
            });
        });
    }

    /**
     * Test the view's disableElement method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [button=#submit] The selector of the element to disable.
     * @example
     * suite.testDisableElement({ count: 10 }, '#sync');
     */
    testDisableElement (dynamicStrings, button) {
        describe('method:disableElement', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            beforeEach(() => this.view.$(button).removeAttr('disabled'));

            it('should disabled the element ($).', () => {
                let $btn = this.view.$(button);

                expect($btn.attr('disabled')).toBe(undefined);
                this.view.disableElement($btn);
                expect($btn.attr('disabled')).toBe('disabled');

                $btn = null;
            });

            it('should disabled the element (string).', () => {
                let $btn = this.view.$(button);

                expect($btn.attr('disabled')).toBe(undefined);
                this.view.disableElement(button);
                expect($btn.attr('disabled')).toBe('disabled');

                $btn = null;
            });
        });
    }

    /**
     * Test the view's enableButton method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [button=#submit] The selector of the button to enable.
     * @example
     * suite.testEnableButton({ count: 10 }, '#sync');
     */
    testEnableButton (dynamicStrings, button) {
        describe('method:enableButton', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should enable the button.', () => {
                // enableButton is just an alias for enableElement.  No need to re-test.
                spyOn(this.view, 'enableElement');

                this.view.enableButton(button);
                expect(this.view.enableElement).toHaveBeenCalledWith(button);
            });
        });
    }

    /**
     * Test the view's disableButton method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [button=#submit] The selector of the button to disable.
     * @example
     * suite.testDisableButton({ count: 10 }, '#sync');
     */
    testDisableButton (dynamicStrings, button) {
        describe('method:disableButton', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            it('should disable the button.', () => {
                // disableButton is just an alias for disableElement.  No need to re-test.
                spyOn(this.view, 'disableElement');

                this.view.disableButton(button);
                expect(this.view.disableElement).toHaveBeenCalledWith(button);
            });
        });
    }

    /**
     * Test the view's showElement method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [button=#submit] The selector of the element to show.
     * @example
     * suite.testShowElement({ count: 10 }, '#sync');
     */
    testShowElement (dynamicStrings, button) {
        describe('method:showElement', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();

            beforeEach(() => this.view.$(button).addClass('hidden'));

            it('should show the element ($).', () => {
                let $btn = this.view.$(button);

                expect($btn.hasClass('hidden')).toBe(true);
                this.view.showElement($btn);
                expect($btn.hasClass('hidden')).toBe(false);

                $btn = null;
            });

            it('should show the element (string).', () => {
                let $btn = this.view.$(button);

                expect($btn.hasClass('hidden')).toBe(true);
                this.view.showElement(button);
                expect($btn.hasClass('hidden')).toBe(false);

                $btn = null;
            });
        });
    }

    /**
     * Test the view's hideElement method.
     * @param {Object} [dynamicStrings={}] A map of strings required for the view to render.
     * @param {String} [button=#submit] The selector of the element to hide.
     * @example
     * suite.testHideElement({ count: 10 }, '#sync');
     */
    testHideElement (dynamicStrings, button) {
        describe('method:hideElement', () => {
            this.DOMBeforeEach(dynamicStrings);
            this.DOMAfterEach();
            beforeEach(() => this.view.$(button).removeClass('hidden'));

            it('should hide the element ($).', () => {
                let $btn = this.view.$(button);

                expect($btn.hasClass('hidden')).toBe(false);
                this.view.hideElement($btn);
                expect($btn.hasClass('hidden')).toBe(true);

                $btn = null;
            });

            it('should hide the element (string).', () => {
                let $btn = this.view.$(button);

                expect($btn.hasClass('hidden')).toBe(false);
                this.view.hideElement(button);
                expect($btn.hasClass('hidden')).toBe(true);

                $btn = null;
            });
        });
    }
}
