import ComponentView from 'core/views/ComponentView';
import Dashboards from 'core/collections/Dashboards';
import Languages from 'core/collections/Languages';
import * as dateTimeUtil from 'core/DateTimeUtil';

import ComponentViewTests from './ComponentView.specBase';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

class ComponentTests extends ComponentViewTests {
    beforeAll () {
        // Set the correct interpolation settings for templates.
        _.templateSettings = {
            evaluate: /\{\[([\s\S]+?)\]\}/g,
            interpolate: /\{\{(.+?)\}\}/g
        };

        return super.beforeAll();
    }

    beforeEach () {
        resetStudyDesign();

        LF.StudyDesign.defaultLanguage = 'en';
        LF.StudyDesign.defaultLocale = 'US';
        LF.security = { restartSessionTimeOut: $.noop };

        spyOn(LF.security, 'restartSessionTimeOut');

        this.view = new ComponentView();

        let template = '<div id="application"></div>';

        $('body').append(template);
        $('#application').append(this.view.$el);

        this.removeSpinnerTemplate = specHelpers.renderTemplateToDOM('core/templates/spinner.ejs');

        return super.beforeEach();
    }

    afterEach () {
        $('#application').remove();

        this.removeSpinnerTemplate();

        return super.afterEach();
    }
}

describe('ComponentView', () => {
    let suite = new ComponentTests();

    suite.executeAll({
        id: 'component',
        tagName: 'div',
        template: '#page-tpl',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',

            // PageView has no class name.
            'testClass'
        ]
    });
});
