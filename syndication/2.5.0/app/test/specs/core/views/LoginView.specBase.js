import User from 'core/models/User';
import PageViewSuite from './PageView.specBase';

export default class CoreLoginViewSuite extends PageViewSuite {
    afterEach () {
        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(SecretQuestionBaseViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testLoginSync () {
        TRACE_MATRIX('US7853').
        describe('method:login', () => {
            Async.it('should trigger LOGIN:Sync', () => {
                let user = new User({
                    id: 1,
                    username: 'admin',
                    role: 'admin'
                });

                this.view.$password.val('password');
                spyOn(LF.security, 'login').and.callFake(() => {
                    return Q(user);
                });
                spyOn(ELF, 'trigger').and.stub();
                this.view.login({
                    preventDefault: $.noop
                });
                return Q().then(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('LOGIN:Sync', { subject: this.view.subject }, this.view);
                });
            });
        });
    }

}
