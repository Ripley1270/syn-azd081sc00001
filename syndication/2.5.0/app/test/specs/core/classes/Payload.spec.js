import Payload from 'core/classes/Payload';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

class PayloadSuite {
    constructor () {
        beforeEach(() => {
            resetStudyDesign();
        });
    }

    testResetPassword () {
        describe('method:resetPassword', () => {
            it('should format the resetPassword request.', () => {
                let res = Payload.resetPassword({
                    password: '12345',
                    secret_question: '1',
                    secret_answer: 'ERT'
                });

                expect(res.W).toBe('12345');
                expect(res.Q).toBe('1');
                expect(res.A).toBe('ERT');
            });
        });
    }

    testResetSecretQuestion () {
        describe('method:resetSecretQuestion', () => {
            it('should format the resetSecretQuestion payload.', () => {
                let res = Payload.resetSecretQuestion({
                    secret_question: '1',
                    secret_answer: 'ERT'
                });

                expect(res.Q).toBe('1');
                expect(res.A).toBe('ERT');
            });
        });
    }

    testAnswer () {
        describe('method:answer', () => {
            it('should format the answer payload', () => {
                let res = Payload.answer({
                    response: '1',
                    SW_Alias: 'Daily.0.DailyQ1',
                    question_id: 'DailyQ1'
                });

                expect(res.G).toBe('1');
                expect(res.F).toBe('Daily.0.DailyQ1');
                expect(res.Q).toBe('DailyQ1');
            });
        });
    }

    testInk () {
        TRACE_MATRIX('DE21158').
        describe('method:ink', () => {
            it('should format the ink payload.', () => {
                let res = Payload.ink({
                    signatureData: [],
                    xSize: 10,
                    ySize: 10,
                    author: 'SysAdmn'
                });

                expect(res.D).toEqual([]);
                expect(res.X).toBe(10);
                expect(res.Y).toBe(10);
                expect(res.H).toBe('SysAdmn');
            });

            it('should round the xSize and ySize down.', () => {
                let res = Payload.ink({
                    xSize: 10.1,
                    ySize: 10.1,
                });

                expect(res.X).toBe(10);
                expect(res.Y).toBe(10);
            });
        });
    }

    testPack () {
        describe('method:pack', () => {
            let data;

            beforeEach(() => data = { hello: 'world' });

            it('should not pack the JSON', () => {
                LF.StudyDesign.jsonh = false;
                spyOn(JSONH, 'pack').and.stub();

                Payload.pack(data);

                expect(JSONH.pack).not.toHaveBeenCalled();
            });

            it('should pack the JSON', () => {
                LF.StudyDesign.jsonh = true;
                spyOn(JSONH, 'pack').and.stub();

                Payload.pack(data);

                expect(JSONH.pack).toHaveBeenCalledWith(data);
            });
        });
    }

    testSubjectAssignment () {
        describe('method:subjectAssignment', () => {
            let form;

            beforeEach(() => {
                form = {
                    krpt: '321312412ed31fg12y1',
                    responsibleParty: 'SysAdmin',
                    dateStarted: '2012-07-25T17:39:43Z',
                    dateCompleted: '2012-07-25T17:42:43Z',
                    reportDate: '2012-07-25',
                    subjectAssignmentPhase: '10',
                    phaseStartDate: -4,
                    batteryLevel: '89',
                    sigID: 'SA.1232131231231232131',
                    initials: 'ABC',
                    patientId: '0100',
                    enrollDate: '2015-12-08T13:05:36.000-05:00',
                    TZValue: '21',
                    language: 'en-US',
                    affidavit: 'SignatureAffidavit',
                    SPStartDate: '2012-07-25'
                }
            });

            it('should format the subject assignment form.', () => {
                let payload = Payload.subjectAssignment(form);

                expect(payload.K).toBe(form.krpt);
                expect(payload.M).toBe(form.responsibleParty);
                expect(payload.U).toBe('Assignment');
                expect(payload.S).toBe(form.dateStarted);
                expect(payload.C).toBe(form.dateCompleted);
                expect(payload.R).toBe(form.reportDate);
                expect(payload.P).toBe(form.subjectAssignmentPhase);
                
                // These two values are pulled from the study design.
                expect(payload.E).toBeDefined();
                expect(payload.V).toBeDefined();

                expect(payload.T).toBe(form.phaseStartDate);
                expect(payload.L).toBe(form.batteryLevel);
                expect(payload.J).toBe(form.sigID);

                expect(payload.A.length).toBe(7);
                expect(payload.A[0]).toEqual({
                    G: form.initials,
                    F: 'PT.Initials',
                    Q: ''
                });
                expect(payload.A[1]).toEqual({
                    G: form.patientId,
                    F: 'PT.Patientid',
                    Q: ''
                });
                expect(payload.A[2]).toEqual({
                    G: form.enrollDate,
                    F: 'PT.EnrollDate',
                    Q: ''
                });
                expect(payload.A[3]).toEqual({
                    G: form.TZValue,
                    F: 'SU.TZValue',
                    Q: ''
                });
                expect(payload.A[4]).toEqual({
                    G: form.language,
                    F: 'Assignment.0.Language',
                    Q: ''
                });
                expect(payload.A[5]).toEqual({
                    G: '',
                    F: form.affidavit,
                    Q: 'AFFIDAVIT'
                });
                expect(payload.A[6]).toEqual({
                    G: form.SPStartDate,
                    F: 'PT.SPStartDate',
                    Q: ''
                });
            });
        });
    }

    testEditPatient () {
        describe('method:editPatient', () => {
            let form;

            beforeEach(() => {
                form = {
                    krpt: '321312412ed31fg12y1',
                    ResponsibleParty: 'SysAdmin',
                    dateStarted: '2012-07-25T17:39:43Z',
                    dateCompleted: '2012-07-25T17:42:43Z',
                    reportDate: '2012-07-25',
                    phaseStartDate: -4,
                    batteryLevel: '89',
                    sigID: 'SA.1232131231231232131',
                    answers: [{
                        response: 'en-US',
                        SW_Alias: 'Assignment.0.Language'
                    }]
                }
            });

            it('should format the edit patient payload.', () => {
                let payload = Payload.editPatient(form);

                expect(payload.K).toBe(form.krpt);
                expect(payload.M).toBe(form.ResponsibleParty);
                expect(payload.U).toBe('Assignment');
                expect(payload.S).toBe(form.dateStarted);
                expect(payload.C).toBe(form.dateCompleted);
                expect(payload.R).toBe(form.reportDate);
                expect(payload.P).toBe(form.subjectAssignmentPhase);
                
                // These two values are pulled from the study design.
                expect(payload.E).toBeDefined();
                expect(payload.V).toBeDefined();

                expect(payload.T).toBe(form.phaseStartDate);
                expect(payload.L).toBe(form.batteryLevel);
                expect(payload.J).toBe(form.sigID);

                expect(payload.A.length).toBe(1);
                expect(payload.A[0]).toEqual({
                    G: 'en-US',
                    F: 'Assignment.0.Language',
                    Q: ''
                });
            });
        });
    }

    testQuestionnaire () {
        describe('method:questionnaire', () => {
            let form;

            beforeEach(() => {
                form = {
                    diary_id: 'Daily',
                    SU: 'Dialy',
                    started: '2012-07-25T17:39:43Z',
                    completed: '2012-07-25T17:42:43Z',
                    report_date: '2012-07-25',
                    completed_tz_offset: -4,
                    phase: '10',
                    phaseStartDateTZOffset: -4,
                    change_phase: 'false',
                    device_id: '987654321',
                    core_version: '1.0.0',
                    study_version: '0.2.1',
                    battery_level: '98',
                    sig_id: 'SA.1232131231231232131',
                    Answers: [{
                        response: '1',
                        SW_Alias: 'Daily.0.DailyQ1',
                        question_id: 'DailyQ1'
                    }]
                };
            });

            it('should format the questionnaire payload.', () => {
                let { payload } = Payload.questionnaire(form);

                expect(payload.I).toBe(form.diary_id);
                expect(payload.U).toBe(form.SU);
                expect(payload.S).toBe(form.started);
                expect(payload.C).toBe(form.completed);
                expect(payload.R).toBe(form.report_date);
                expect(payload.O).toBe(form.completed_tz_offset);
                expect(payload.P).toBe(form.phase);
                expect(payload.T).toBe(form.phaseStartDateTZOffset);
                expect(payload.B).toBe(form.change_phase);
                expect(payload.D).toBe(form.device_id);
                expect(payload.E).toBe(form.core_version);
                expect(payload.V).toBe(form.study_version);
                expect(payload.L).toBe(form.battery_level);
                expect(payload.J).toBe(form.sig_id);

                expect(payload.A.length).toBe(1);
                expect(payload.A[0]).toEqual({
                    G: '1',
                    F: 'Daily.0.DailyQ1',
                    Q: 'DailyQ1'
                });
            });
        });
    }
}

describe('Payload', () => {
    let suite = new PayloadSuite();

    suite.testResetPassword();
    suite.testResetSecretQuestion();
    suite.testAnswer();
    suite.testInk();
    suite.testQuestionnaire();
    suite.testPack();
    suite.testSubjectAssignment();
    suite.testEditPatient();
});