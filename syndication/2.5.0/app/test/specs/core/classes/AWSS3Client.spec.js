import AWSClient from 'core/classes/AWSS3Client';

describe('AWSS3Client', () => {
    let awsClient = null,
        awsKey = 'AKIAIFXBXB7IO5I5U6CA',
        awsSecretKey = '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',
        awsRegion = 'us-east-2',
        awsBucket = 'syndication-app-rater-training';

    beforeEach(() => {
        awsClient = new AWSClient(
            awsKey,
            awsSecretKey,
            awsRegion,
            awsBucket
        );
    });

    afterEach(() => {
        awsClient = null;
    });

    describe('#constructor', () => {
        it('should create the AWS.S3 client', () => {
            expect(awsClient._s3).not.toBeNull();
        });

        it('should create the AWS bucket property', () => {
            expect(awsClient._bucket).not.toBeNull();
        });
    });

    describe('#functions', () => {
        describe('#getFileListByPrefix', () => {
            it('should call listObjectsV2 in the AWS client', () => {
                spyOn(awsClient._s3, 'listObjectsV2').and.callFake(() => {});

                awsClient.getFileListByPrefix();

                expect(awsClient._s3.listObjectsV2).toHaveBeenCalled();
            });
        });
        describe('#getFileContents', () => {
            it('should call getObject in the AWS client', () => {
                spyOn(awsClient._s3, 'getObject').and.callFake(() => {});

                awsClient.getFileContents('fake');

                expect(awsClient._s3.getObject).toHaveBeenCalled();
            });
        });
        
        describe('#getDownloadURL', () => {
            it('should call getSignedUrl in the AWS client for getObject', () => {
                spyOn(awsClient._s3, 'getSignedUrl').and.callFake(() => {});

                awsClient.getDownloadURL('fake');

                expect(awsClient._s3.getSignedUrl).toHaveBeenCalled();
            });
        });
    });
});