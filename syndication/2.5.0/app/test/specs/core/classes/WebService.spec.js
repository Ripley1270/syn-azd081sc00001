import * as lStorage from 'core/lStorage';
import WebService from 'core/classes/WebService';

describe('WebService', () => {
    let webService,
        originalStudyDesign,
        serviceBase = 'https://backend-api-url',
        source = 'LogPadApp',
        jsonh = true;

    beforeAll(() => {
        originalStudyDesign = LF.StudyDesign;
        LF.StudyDesign = LF.StudyDesign || {};
        LF.StudyDesign.jsonh = jsonh;
    });

    afterAll(() => {
        LF.StudyDesign = originalStudyDesign;
    });

    beforeEach(() => {
        lStorage.setItem('serviceBase', serviceBase);
        webService = new WebService();
        webService.getSource = () => source;

        spyOn(webService, 'transmit').and.resolve();

        spyOn(lStorage, 'getItem').and.callFake((key) => {
            switch (key) {
                case 'PHT_Authorization':
                    return '123:abcdef456:789abc-def';
                case 'serviceBase':
                    return serviceBase;
                default:
                    return null;
            }
        });
    });

    TRACE_MATRIX('US7392')
    .TRACE_MATRIX('US8388')
    .describe('sendDiary', () => {
        let ajaxConfig,
            diary = {
                diary_id: '1',
                SU: 'Meds',
                Answers: [{}, {}]
            },
            auth = '';

        beforeEach(() => {
            ajaxConfig = {
                type: 'POST',
                uri: webService.buildUri('/v1/diaries/'),
                auth
            };
        });

        Async.it('should set the correct jsonh value for the ajaxConfig in sendDiary', () => {
            return webService.sendDiary(diary, auth, true)
            .then(() => {
                ajaxConfig.jsonh = true;
                expect(webService.transmit).toHaveBeenCalledWith(ajaxConfig, diary);
            })
            .then(() => webService.sendDiary(diary, auth, false))
            .then(() => {
                ajaxConfig.jsonh = false;
                expect(webService.transmit).toHaveBeenCalledWith(ajaxConfig, diary);
            });
        });

        Async.it('should send the correct source value', () => {
            return webService.sendDiary(diary, auth, true)
            .then(() => {
                let payloadWithSource = _.extend({}, diary, { source }),
                    ajaxConfigWithJsonH = _.extend({}, ajaxConfig, { jsonh: true });

                expect(webService.transmit).toHaveBeenCalledWith(ajaxConfigWithJsonH, payloadWithSource);
            });
        });
    });

    TRACE_MATRIX('US8388')
    .describe('sendSubjectAssignment', () => {
        let payload = {},
            auth = '';

        Async.it('should call the correct backend API', () => {
            let ajaxConfig = {
                type: 'POST',
                uri: webService.buildUri('/v1/subjects/'),
                auth,
                jsonh
            };

            return webService.sendSubjectAssignment(payload, auth)
            .then(() => {
                expect(webService.transmit).toHaveBeenCalledWith(ajaxConfig, jasmine.any(Object));
            });
        });

        Async.it('should send the correct source value', () => {
            return webService.sendSubjectAssignment(payload, auth)
            .then(() => {
                let payloadWithSource = _.extend({}, payload, { source });

                expect(webService.transmit).toHaveBeenCalledWith(jasmine.any(Object), payloadWithSource);
            });
        });
    });

    TRACE_MATRIX('US8388')
    .describe('sendEditPatient', () => {
        let krpt = 'SA.c18f9a50508341c2a816225316e69248',
            payload = { K: krpt },
            auth = '';

        Async.it('should call the correct backend API', () => {
            let ajaxConfig = {
                type: 'PUT',
                uri: webService.buildUri(`/v1/subjects/${krpt}`),
                auth,
                jsonh
            };

            return webService.sendEditPatient(payload, auth)
            .then(() => {
                expect(webService.transmit).toHaveBeenCalledWith(ajaxConfig, jasmine.any(Object));
            });
        });

        Async.it('should send the correct source value', () => {
            return webService.sendEditPatient(payload, auth)
            .then(() => {
                let payloadWithSource = _.extend({}, payload, { source });

                expect(webService.transmit).toHaveBeenCalledWith(jasmine.any(Object), payloadWithSource);
            });
        });
    });

    TRACE_MATRIX('US8388')
    .describe('resetSubjectCredentials', () => {
        let payload = {},
            unlockCode = '123456',
            krpt = 'SA.c18f9a50508341c2a816225316e69248';

        Async.it('should call the correct backend API', () => {
            let ajaxConfig = {
                type: 'POST',
                unlockCode,
                uri: webService.buildUri(`/v1/subjects/${krpt}/credentials`)
            };

            return webService.resetSubjectCredentials(payload, krpt, unlockCode)
            .then(() => {
                expect(webService.transmit).toHaveBeenCalledWith(ajaxConfig, jasmine.any(Object));
            });
        });

        Async.it('should send the correct source value', () => {
            return webService.resetSubjectCredentials(payload, krpt, unlockCode)
            .then(() => {
                let payloadWithSource = _.extend({}, payload, { source });

                expect(webService.transmit).toHaveBeenCalledWith(jasmine.any(Object), payloadWithSource);
            });
        });
    });

    TRACE_MATRIX('US8388')
    .describe('resetSubjectPassword', () => {
        let payload = {},
            krpt = 'SA.c18f9a50508341c2a816225316e69248';

        Async.it('should call the correct backend API', () => {
            let ajaxConfig = {
                type: 'PUT',
                uri: webService.buildUri(`/v1/subjects/${krpt}/credentials`)
            };

            return webService.resetSubjectPassword(payload, krpt)
            .then(() => {
                expect(webService.transmit).toHaveBeenCalledWith(ajaxConfig, jasmine.any(Object));
            });
        });

        Async.it('should send the correct source value', () => {
            return webService.resetSubjectPassword(payload, krpt)
            .then(() => {
                let payloadWithSource = _.extend({}, payload, { source });

                expect(webService.transmit).toHaveBeenCalledWith(jasmine.any(Object), payloadWithSource);
            });
        });
    });

    TRACE_MATRIX('US8388')
    .describe('setSubjectData', () => {
        let payload = {};

        Async.it('should call the correct backend API', () => {
            let ajaxConfig = {
                type: 'POST',
                uri: webService.buildUri('/v1/devices/')
            };

            return webService.setSubjectData(payload)
            .then(() => {
                expect(webService.transmit).toHaveBeenCalledWith(ajaxConfig, jasmine.any(Object));
            });
        });

        Async.it('should send the correct source value', () => {
            return webService.setSubjectData(payload)
            .then(() => {
                let payloadWithSource = _.extend({}, payload, { O: source });

                expect(webService.transmit).toHaveBeenCalledWith(jasmine.any(Object), payloadWithSource);
            });
        });
    });

    TRACE_MATRIX('US8388')
    .describe('getDeviceID', () => {
        let unlockCode = '123456';

        Async.it('should call the correct backend API with unlockCode', () => {
            let payload = { code: unlockCode },
                ajaxConfig = {
                    type: 'POST',
                    uri: webService.buildUri('/v1/devices'),
                    unlockCode
                };

            return webService.getDeviceID(payload)
            .then(() => {
                expect(webService.transmit).toHaveBeenCalledWith(ajaxConfig, jasmine.any(Object));
            });
        });

        Async.it('should call the correct backend API without unlockCode', () => {
            let payload = {},
                ajaxConfig = {
                    type: 'POST',
                    uri: webService.buildUri('/v1/devices'),
                    unlockCode: false
                };

            return webService.getDeviceID(payload)
            .then(() => {
                expect(webService.transmit).toHaveBeenCalledWith(ajaxConfig, jasmine.any(Object));
            });
        });

        Async.it('should send the correct source value', () => {
            let payload = {};

            return webService.getDeviceID(payload)
            .then(() => {
                let payloadWithSource = _.extend({}, payload, { O: source });

                expect(webService.transmit).toHaveBeenCalledWith(jasmine.any(Object), payloadWithSource);
            });
        });
    });
});
