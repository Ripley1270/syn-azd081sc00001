'use strict';

import { evaluateBranching } from 'core/branching/evaluateBranching';

describe('evaluateBranching', () => {
    TRACE_MATRIX('US7363').
        describe('if branchingFunction returns a branch obj, that branch Obj is returned', () => {
            let savedLF;
    
            beforeEach(() => {
                savedLF = window.LF;
                window.LF = {
                    Branching: {
                        branchFunctions: {}
                    }
                };
            });
        
            afterEach(() => {
                window.LF = savedLF;
            });

            Async.it('returns object', () => {
                const curScreen = 'bar',
                    newBranch = {
                        branchTo: 'branchTo',
                        clearBranchedResponses: true
                    },
                    branchingFunction = 'US7363_testBranchingfunction',
                    branchArray = [{
                        branchTo: 'foo',
                        branchFrom: curScreen,
                        branchFunction: branchingFunction,
                        clearBranchedResponses: true
                    }];

                LF.Branching.branchFunctions[branchingFunction] = () => {
                    return Q(newBranch);
                };

                spyOn(LF.Branching.branchFunctions, branchingFunction).and.callThrough();

                return evaluateBranching(branchArray, curScreen, [], {})
                    .then((result) => {
                        expect(LF.Branching.branchFunctions[branchingFunction].branchFunction).toHaveBeenCalled;
                        expect(result).toBe(newBranch);
                    });
            });
        });
});