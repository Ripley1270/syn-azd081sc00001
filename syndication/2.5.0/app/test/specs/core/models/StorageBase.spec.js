import StorageBase from 'core/models/StorageBase';
import ELF from 'core/ELF';

class ListItem extends StorageBase {
    get name () {
        return 'ListItem';
    }

    static get schema () {
        return {
            task: {
                type: String,
                encrypt: true
            }
        }
    }
}

describe('StorageBase', () => {
    beforeEach(() => {
        spyOn(ListItem.prototype, 'save');
        spyOn(ELF, 'trigger').and.resolve();
    });

    TRACE_MATRIX('DE22546')
    .describe('method:decrypt', () => {
        it('should fail to decrypt the data.', () => {
            let model = new ListItem({ task: '12345677' });

            model.decrypt();

            expect(ELF.trigger).toHaveBeenCalledWith('APPLICATION:DecryptionError', jasmine.any(Object), jasmine.any(ListItem));
        });
    });

    TRACE_MATRIX('DE22546')
    .describe('method:encrypt', () => {
        it('should fail to encrypt the data.', () => {
            let model = new ListItem({ task: '12345677' });

            spyOn(GibberishAES, 'enc').and.throwError();

            model.encrypt();

            expect(ELF.trigger).toHaveBeenCalledWith('APPLICATION:EncryptionError', jasmine.any(Object), jasmine.any(ListItem));
        });
    });
});