import SelectWidgetBaseTests from './SelectWidgetBase.spec';
import Widget from 'core/models/Widget';

const DEFAULT_ITEM_TEMPLATE = 'DEFAULT:SelectItemTemplateTrans';
const DEFAULT_INPUT_TEMPLATE = 'DEFAULT:SelectWidget';
const DEFAULT_CLASS_NAME = 'DropDownList';
const DEFAULT_RESULTS_CLASS_NAME = 'DropDownList-Results';
const DEFAULT_UNIFORM_OPTION_HEIGHT = true;
const DEFAULT_VISIBLE_ROWS = null;

const VERY_LONG_ANSWER = `here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.
here is an extremely long response.  here is an extremely long response.  here is an extremely long response.`;

/* global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
// jscs:disable requireArrowFunctions
export default class DropDownListTests extends SelectWidgetBaseTests {
    constructor (model = DropDownListTests.model) {
        super(model);
    }

    getMinDummyQuestion (studyDesign) {
        let ret = super.getMinDummyQuestion(studyDesign);
        $('#application').append($(`#${ret.getQuestionnaire().id}`));
        return ret;
    }

    static get model () {
        return new Widget({
            id: 'DROPDOWN_DIARY_W_1',
            type: 'DropDownList',
            className: 'DropDownList',
            items: [
                {
                    value: 'i1',
                    text: 'item 1'
                },
                {
                    value: 2,
                    text: 'item 2'
                },
                {
                    value: 3,
                    text: 'item 3'
                },
                {
                    value: 4,
                    text: 'item 4'
                }
            ]
        });
    }

    execTests () {
        afterAll(() => {
            $('#application').remove();
        });

        super.execTests();

        describe('DropDownList instance tests', () => {
            let templates,
                dummyParent,
                security,
                selectWidget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                $('body').append('<div id="application" style="width:400px;position:absolute;top:0;left:0;"></div>');
                dummyParent = this.getMinDummyQuestion();
            });

            afterEach(() => {
                $('#application').remove();
                selectWidget = null;
            });

            describe('getters', () => {
                beforeEach(() => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                });
                describe('#defaultItemTemplate', () => {
                    it('returns correct default', () => {
                        expect(selectWidget.defaultItemTemplate).toBe(DEFAULT_ITEM_TEMPLATE);
                    });
                });

                describe('#input', () => {
                    it('returns correct default', () => {
                        expect(selectWidget.input).toBe(DEFAULT_INPUT_TEMPLATE);
                    });
                });

                describe('#defaultClassName', () => {
                    it('returns correct default', () => {
                        expect(selectWidget.defaultClassName).toBe(DEFAULT_CLASS_NAME);
                    });
                });

                describe('#defaultResultsClassName', () => {
                    it('returns correct default', () => {
                        expect(selectWidget.defaultResultsClassName).toBe(DEFAULT_RESULTS_CLASS_NAME);
                    });
                });
            });

            describe('#constructor', () => {
                it('sets properties to defaults', () => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                    expect(selectWidget.completed).toBe(false);
                    expect(selectWidget.answer).toBe(null);
                    expect(selectWidget.needToRespond.value).toBe('');
                    expect(selectWidget.$select2).toBe(null);
                });

                it('sets defaults if the model attributes are undefined/invalid', () => {
                    let customModel = _.extend({}, this.model);
                    customModel.attributes = _.extend(customModel.attributes, {
                        itemTemplate: undefined,
                        className: undefined,
                        resultsClassName: undefined,
                        uniformOptionHeight: 'string type... not a boolean',
                        visibleRows: 'string type... not a number'
                    });
                    selectWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    expect(selectWidget.model.get('itemTemplate')).toBe(DEFAULT_ITEM_TEMPLATE);
                    expect(selectWidget.model.get('className')).toBe(DEFAULT_CLASS_NAME);
                    expect(selectWidget.model.get('resultsClassName')).toBe(DEFAULT_RESULTS_CLASS_NAME);
                    expect(selectWidget.model.get('uniformOptionHeight')).toBe(DEFAULT_UNIFORM_OPTION_HEIGHT);
                    expect(selectWidget.model.get('visibleRows')).toBe(DEFAULT_VISIBLE_ROWS);
                });

                it('uses properties passed into the model, if defined', () => {
                    let customModel = _.extend({}, this.model);
                    customModel.attributes = _.extend(customModel.attributes, {
                        itemTemplate: 'CustomItemTemplate',
                        className: 'CustomClassName',
                        resultsClassName: 'CustomResultsClassName',
                        uniformOptionHeight: false,
                        visibleRows: null
                    });
                    selectWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });

                    expect(selectWidget.model.get('itemTemplate')).toBe('CustomItemTemplate');
                    expect(selectWidget.model.get('className')).toBe('CustomClassName');
                    expect(selectWidget.model.get('resultsClassName')).toBe('CustomResultsClassName');
                    expect(selectWidget.model.get('uniformOptionHeight')).toBe(false);
                    expect(selectWidget.model.get('visibleRows')).toBe(null);
                });

                it('allows numeric value for visible rows.  Changes uniformOptionHeight to true.', () => {
                    let customModel = _.extend({}, this.model);
                    customModel.attributes = _.extend(customModel.attributes, {
                        itemTemplate: 'CustomItemTemplate',
                        className: 'CustomClassName',
                        resultsClassName: 'CustomResultsClassName',
                        uniformOptionHeight: false,
                        visibleRows: 14
                    });
                    selectWidget = new LF.Widget[customModel.get('type')]({
                        model: customModel,
                        mandatory: false,
                        parent: dummyParent
                    });
                    expect(selectWidget.model.get('uniformOptionHeight')).toBe(true);
                    expect(selectWidget.model.get('visibleRows')).toBe(14);
                });

                TRACE_MATRIX('DE22235').describe('#dropDownListNoResultsFound', () => {
                    Async.it('checks returned query results string of an empty list.', () => {
                        let customModel = _.extend({}, this.model);
                        customModel.attributes = _.extend(customModel.attributes, {
                            items: []
                        });
                        selectWidget = new LF.Widget[customModel.get('type')]({
                            model: customModel,
                            mandatory: false,
                            parent: dummyParent
                        });

                        // render (construct) the actual widget to initialize all values
                        return selectWidget.render().then(() => {
                            return LF.getStrings('NO_RESULTS_FOUND');
                        }).tap((string) => {
                            return new Q.Promise((resolve) => {
                                let doNext;
                                let id = `${selectWidget.model.get('id')}`;

                                // handle events ourselves to drive our promise.
                                selectWidget.undelegateEvents();
                                doNext = () => {
                                    selectWidget.$(id).off('select2:open', doNext);

                                    // there just needs to be the slightest delay for the UI component.
                                    Q().delay(0).then(() => {
                                        expect($(`#select2-${id}-results`)[0].innerText.trim()).toEqual(string);
                                        resolve();
                                    });
                                };
                                selectWidget.$(`#${id}`).on('select2:open', doNext);
                                selectWidget.$select2.select2('open');
                            });
                        });
                    });
                });
            });

            describe('#delegateEvents()', () => {
                it('calls addCustomEvents', () => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                    spyOn(selectWidget, 'addCustomEvents');
                    selectWidget.delegateEvents();
                    expect(selectWidget.addCustomEvents.calls.count()).toBe(1);
                });
            });

            describe('#undelegateEvents()', () => {
                it('calls removeCustomEvents', () => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                    spyOn(selectWidget, 'removeCustomEvents');
                    selectWidget.undelegateEvents();
                    expect(selectWidget.removeCustomEvents.calls.count()).toBe(1);
                });
            });

            describe('#addCustomEvents()', () => {
                Async.it('adds events for select2', () => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return selectWidget.render()
                        .then(() => {
                            spyOn($.fn, 'on');
                            selectWidget.addCustomEvents();
                            expect($.fn.on.calls.count()).toBe(1);
                            expect($.fn.on).toHaveBeenCalledWith('select2:open', selectWidget.setHeights);
                        });
                });
            });

            describe('#removeCustomEvents()', () => {
                Async.it('removes events for select2', () => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return selectWidget.render()
                        .then(() => {
                            spyOn($.fn, 'off');
                            selectWidget.removeCustomEvents();
                            expect($.fn.off.calls.count()).toBe(1);
                            expect($.fn.off).toHaveBeenCalledWith('select2:open', selectWidget.setHeights);
                        });
                });
            });

            describe('#renderOptions', () => {
                let id = `#${this.model.get('id')}`;
                beforeEach((done) => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });
                    selectWidget.setupModel()
                        .then(
                            () => selectWidget.render()
                        )
                        .then(() => {
                            done();
                        }).done();
                });

                Async.xit('calls setupCustomParams()', () => {
                    spyOn(selectWidget, 'setupCustomParams').and.callThrough();
                    return selectWidget.renderOptions(id)
                        .tap(() => {
                            expect(selectWidget.setupCustomParams.calls.count()).toBe(1);
                        });
                });

                Async.it('empties the target div', () => {
                    spyOn($.fn, 'empty');
                    return selectWidget.renderOptions(id)
                        .tap(() => {
                            expect($.fn.empty).toHaveBeenCalled();
                        });
                });

                Async.it('can add translated options', () => {
                    selectWidget.model.set('itemTemplate', 'DEFAULT:SelectItemTemplateTrans');
                    spyOn(LF.strings, 'display').and.callFake((string) => {
                        return Q.resolve(`translated: ${string}`);
                    });
                    return selectWidget.renderOptions(id)
                        .tap(() => {
                            let $options = selectWidget.$('option'),
                                items = this.model.get('items');
                            expect($options.length).toBe(4);
                            for (let i = 0; i < $options.length; ++i) {
                                expect(selectWidget.$($options[i]).attr('value')).toBe(items[i].value.toString());
                                expect(selectWidget.$($options[i]).html()).toBe(`translated: ${items[i].text}`);
                            }
                        });
                });
            });

            describe('#initializeSelect2', () => {
                beforeEach((done) => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    selectWidget.render()
                        .then(() => {
                            done();
                        }).done();
                });

                Async.it('populates select2 jQuery reference', () => {
                    return selectWidget.initializeSelect2()
                        .tap(() => {
                            expect(selectWidget.$select2.length).toBe(1);
                        });
                });

                Async.it('sets answer if it exists', () => {
                    selectWidget.answer = {
                        get: (param) => {
                            if (param === 'response') {
                                return 'i1';
                            }

                            return '';
                        }
                    };
                    return selectWidget.initializeSelect2()
                        .tap(() => {
                            expect(selectWidget.$select2.val()).toBe('i1');
                        });
                });

                Async.it('sets defaultVal if it exists (and no answer)', () => {
                    selectWidget.answer = null;
                    selectWidget.model.set('defaultVal', 2);
                    return selectWidget.initializeSelect2()
                        .tap(() => {
                            expect(selectWidget.$select2.val()).toBe('2');
                        });
                });

                Async.it('sets needToRespond if no default val and no answer', () => {
                    selectWidget.answer = null;
                    selectWidget.model.set('defaultVal', null);
                    selectWidget.needToRespond.value = 4;
                    return selectWidget.initializeSelect2()
                        .tap(() => {
                            expect(selectWidget.$select2.val()).toBe('4');
                        });
                });
            });

            describe('#setHeights()', () => {
                let id;
                beforeEach(() => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    id = `#${selectWidget.model.get('id')}`;
                });

                Async.it('allows different heights if uniformOptionHeight is false', () => {
                    selectWidget.model.set('uniformOptionHeight', false);
                    selectWidget.model.set('visibleRows', null);
                    selectWidget.model.set('items', [
                        {
                            value: 1,
                            text: 'one'
                        },
                        {
                            value: 2,
                            text: VERY_LONG_ANSWER
                        },
                        {
                            value: 3,
                            text: 'three'
                        }
                    ]);

                    return selectWidget.render()

                        .then(() => {
                            return new Q.Promise((resolve) => {
                                let doNext;

                                // handle events ourselves to drive our promise.
                                selectWidget.undelegateEvents();
                                doNext = () => {
                                    selectWidget.$(id).off('select2:open', doNext);
                                    resolve();
                                };
                                selectWidget.$(id).on('select2:open', doNext);
                                selectWidget.$select2.select2('open');
                            });
                        })
                        .then(() => {
                            return selectWidget.setHeights();
                        })
                        .tap(() => {
                            return new Q.Promise((resolve) => {
                                let $optionContainer = $(`#select2-${selectWidget.model.get('id')}-results`),
                                    $options = $optionContainer.find('.select2-results__option');
                                $options.ready(() => {
                                    expect(
                                        $($options[0]).height() < $($options[1]).height()
                                    ).toBe(true);
                                    resolve();
                                });
                            });
                        });
                });

                Async.it('makes heights match if uniformOptionHeight is true', () => {
                    selectWidget.model.set('uniformOptionHeight', true);
                    selectWidget.model.set('visibleRows', null);
                    selectWidget.model.set('items', [
                        {
                            value: 1,
                            text: 'one'
                        },
                        {
                            value: 2,
                            text: VERY_LONG_ANSWER
                        },
                        {
                            value: 3,
                            text: 'three'
                        }
                    ]);

                    $(dummyParent.$el).css('width', '100%');
                    $(dummyParent.$el).css('position', 'block');

                    return selectWidget.render()
                        .then(() => {
                            return new Q.Promise((resolve) => {
                                let doNext;

                                // handle events ourselves to drive our promise.
                                selectWidget.undelegateEvents();
                                doNext = () => {
                                    selectWidget.$(id).off('select2:open', doNext);
                                    resolve();
                                };
                                selectWidget.$(id).on('select2:open', doNext);
                                selectWidget.$select2.select2('open');
                            });
                        })
                        .then(() => {
                            return selectWidget.setHeights();
                        })
                        .tap(() => {
                            return new Q.Promise((resolve) => {
                                let $optionContainer = $(`#select2-${selectWidget.model.get('id')}-results`),
                                    $options = $optionContainer.find('.select2-results__option');
                                $options.ready(() => {
                                    expect($($options[0]).height()).toBe($($options[1]).height());
                                    expect($($options[1]).height()).toBe($($options[2]).height());
                                    resolve();
                                });
                            });
                        });
                });

                Async.it('makes container fit a number of items if visible rows is defined', () => {
                    selectWidget.model.set('uniformOptionHeight', true);
                    selectWidget.model.set('visibleRows', 3);
                    selectWidget.model.set('items', [
                        {
                            value: 1,
                            text: 'one'
                        },
                        {
                            value: 2,
                            text: VERY_LONG_ANSWER
                        },
                        {
                            value: 3,
                            text: 'three'
                        },
                        {
                            value: 4,
                            text: 'four'
                        }
                    ]);

                    $(dummyParent.$el).css('width', '100%');
                    $(dummyParent.$el).css('position', 'block');

                    return selectWidget.render()
                        .then(() => {
                            return new Q.Promise((resolve) => {
                                let doNext;

                                // handle events ourselves to drive our promise.
                                selectWidget.undelegateEvents();
                                doNext = () => {
                                    selectWidget.$(id).off('select2:open', doNext);
                                    resolve();
                                };
                                selectWidget.$(id).on('select2:open', doNext);
                                selectWidget.$select2.select2('open');
                            });
                        })
                        .then(() => {
                            return selectWidget.setHeights();
                        })
                        .tap(() => {
                            return new Q.Promise((resolve) => {
                                let $optionContainer = $(`#select2-${selectWidget.model.get('id')}-results`),
                                    $options = $optionContainer.find('.select2-results__option');
                                $options.ready(() => {
                                    expect($optionContainer.height()).toBe($($options[0]).outerHeight() * 3);
                                    resolve();
                                });
                            });
                        });
                });
            });
            describe('#respond()', () => {
                Async.beforeEach(() => {
                    selectWidget = new LF.Widget[this.model.get('type')]({
                        model: this.model,
                        mandatory: false,
                        parent: dummyParent
                    });

                    return selectWidget.render();
                });

                Async.it('clears answers and sets completed to false if null', () => {
                    selectWidget.undelegateEvents();
                    selectWidget.completed = true;
                    selectWidget.addAnswer();
                    selectWidget.$select2.val(null);
                    let e = $.Event('change', { target: selectWidget.$select2 });
                    return selectWidget.respond(e)
                        .tap(() => {
                            expect(selectWidget.answers.size()).toBe(0);
                            expect(selectWidget.completed).toBe(false);
                        });
                });

                Async.it('adds answer and calls respondHelper, making completed true, if answer is not null', () => {
                    selectWidget.undelegateEvents();
                    selectWidget.completed = false;
                    selectWidget.removeAllAnswers();
                    selectWidget.$select2.val(2);
                    spyOn(selectWidget, 'respondHelper').and.callThrough();
                    let e = $.Event('change', { target: selectWidget.$select2 });
                    return selectWidget.respond(e)
                        .tap(() => {
                            expect(selectWidget.answers.size()).toBe(1);
                            expect(selectWidget.completed).toBe(true);

                            // args 0 is answer object, which doesn't match after respond helper is called.
                            // just check args 2 and 3 instead.
                            expect(selectWidget.respondHelper.calls.mostRecent().args[1]).toBe('2');
                            expect(selectWidget.respondHelper.calls.mostRecent().args[2]).toBe(true);
                            expect(selectWidget.respondHelper.calls.count()).toBe(1);
                        });
                });
            });
        });
    }
}
// jscs:enable requireArrowFunctions

let tester = new DropDownListTests();
tester.execTests();
