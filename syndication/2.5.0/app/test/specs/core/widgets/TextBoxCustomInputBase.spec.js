import 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';
import 'core/resources/Templates';

import WidgetBaseTests from './WidgetBase.specBase.js';
import Widget from 'core/models/Widget';
import TextBoxCustomInputBase from 'core/widgets/TextBoxCustomInputBase';

const defaultWrapperTemplate = 'DEFAULT:FormGroup',
    defaultClassName = 'Modal',
    defaultLabelTemplate = 'DEFAULT:Label',
    defaultInputTemplate = 'DEFAULT:TextBoxWithEmbeddedControl',
    defaultLabels = {},
    { Model } = Backbone;

/* global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
export default class TextBoxCustomInputBaseTests extends WidgetBaseTests {
    constructor (model = TextBoxCustomInputBaseTests.model) {
        super(model);
    }

    static testMixin (superclass) {
        return class extends superclass {
            storageValueToDisplayFormat (val) {
                return val;
            }

            displayValueToStorageFormat (val) {
                return val;
            }

            getModalValuesString () {
                return 'test';
            }

            getModalTranslationsObject () {
                return {};
            }

            renderModal () {
                return Q();
            }
        };
    }

    static get model () {
        return new Widget({
            id: 'TextBoxCustomInputBase_W_1',
            type: 'TextBoxCustomInputBase',
            className: 'TextBoxCustomInputBase_W_1',
            modalMixin: TextBoxCustomInputBaseTests.testMixin
        });
    }

    htmlRenderedCheck () {
        const myModel = TextBoxCustomInputBaseTests.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${this.dummyParent.id}`).find('input').length).toEqual(1);
            });
    }

    execTests () {
        super.execTests();

        // eslint-disable-next-line new-cap
        TRACE_MATRIX('US7326')
        .describe('TextBoxCustomInputBase', () => {
            let templates,
                dummyParent,
                security,
                widget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
                widget = new LF.Widget[TextBoxCustomInputBaseTests.model.get('type')]({
                    model: TextBoxCustomInputBaseTests.model,
                    mandatory: false,
                    parent: dummyParent
                });
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
                widget = null;
            });

            describe('getters', () => {
                beforeEach(() => {
                    widget = new LF.Widget[TextBoxCustomInputBaseTests.model.get('type')]({
                        model: TextBoxCustomInputBaseTests.model,
                        mandatory: false,
                        parent: this.dummyParent
                    });
                });

                describe('#defaultClassName', () => {
                    it('returns expected value', () => {
                        expect(widget.defaultClassName).toBe(defaultClassName);
                    });
                });

                describe('#defaultLabels', () => {
                    it('returns expected value', () => {
                        expect(widget.defaultLabels).toEqual(defaultLabels);
                    });
                });

                describe('#defaultWrapperTemplate', () => {
                    it('returns expected value', () => {
                        expect(widget.defaultWrapperTemplate).toBe(defaultWrapperTemplate);
                    });
                });

                describe('#defaultLabelTemplate', () => {
                    it('returns expected value', () => {
                        expect(widget.defaultLabelTemplate).toBe(defaultLabelTemplate);
                    });
                });

                describe('#input', () => {
                    it('returns expected value', () => {
                        expect(widget.input).toBe(defaultInputTemplate);
                    });
                });

                describe('#value', () => {
                    it('getter returns value of displayValueToStorageFormat', () => {
                        spyOn(widget, 'displayValueToStorageFormat').and.returnValue(15);
                        expect(widget.value).toBe(15);
                        expect(widget.displayValueToStorageFormat).toHaveBeenCalled();
                    });

                    it('setter calls storageValueToDisplayFormat', () => {
                        spyOn(widget, 'storageValueToDisplayFormat').and.callThrough();
                        widget.value = 15;
                        expect(widget.storageValueToDisplayFormat).toHaveBeenCalledWith(15);
                    });
                });

                describe('#displayText', () => {
                    it('returns null if no answer', () => {
                        widget.answer = null;
                        expect(widget.displayText).toBe(null);
                    });

                    it('returns response from answer', () => {
                        widget.answer = new Model({
                            response: 15
                        });
                        expect(widget.displayText).toBe(15);
                    });
                });
            });

            describe('#constructor', () => {
                it('throws an error if mixin not defined', () => {
                    let customModel = TextBoxCustomInputBaseTests.model;
                    customModel.unset('modalMixin');
                    expect(() => {
                        widget = new LF.Widget[customModel.get('type')]({
                            model: customModel,
                            mandatory: false,
                            parent: this.dummyParent
                        });
                    }).toThrow(
                        new Error('Configuration Error:  TextBoxCustomInputBase requires a modalMixin property.')
                    );
                });
            });

            describe('#getValueForModal', () => {
                it('returns value getter', () => {
                    spyOn(widget, 'displayValueToStorageFormat').and.returnValue(15);
                    expect(widget.getValueForModal()).toBe(15);
                });
            });

            describe('#render', () => {
                Async.it('calls renderModal with translations object, delegates events', () => {
                    const i18nResult = { string1: 'val1' };
                    spyOn(widget, 'i18n').and.resolve(i18nResult);
                    spyOn(widget, 'getModalTranslationsObject').and.returnValue({ testProp: 'testval' });
                    spyOn(widget, 'renderModal').and.resolve();
                    spyOn(widget, 'delegateEvents');

                    return widget.render()
                    .then(() => {
                        expect(widget.i18n).toHaveBeenCalledWith({ testProp: 'testval' }, jasmine.any(Function), { namespace: 'dummy1-questionnaire' });
                        expect(widget.renderModal).toHaveBeenCalledWith(i18nResult);
                        expect(widget.delegateEvents).toHaveBeenCalled();
                    });
                });
            });

            describe('post-rendered methods', () => {
                Async.beforeEach(() => {
                    return widget.render();
                });

                describe('#propagateModalValue', () => {
                    Async.it('sets value to answer from getModalValuesString', () => {
                        spyOn(widget, 'getModalValuesString').and.resolve('15');
                        spyOn(widget, 'respond').and.resolve();
                        spyOn(widget.$input, 'trigger');
                        return widget.propagateModalValue()
                        .then(() => {
                            // false is default for stopModal
                            expect(widget.getModalValuesString).toHaveBeenCalledWith(false);
                            expect(widget.value).toBe('15');
                            expect(widget.$input.trigger).toHaveBeenCalledWith('input');
                            expect(widget.respond).toHaveBeenCalled();
                        });
                    });

                    Async.it('calls with "true" for stopModal if true passed in', () => {
                        spyOn(widget, 'getModalValuesString').and.resolve('15');
                        spyOn(widget, 'respond').and.resolve();
                        spyOn(widget.$input, 'trigger');
                        return widget.propagateModalValue(true)
                        .then(() => {
                            expect(widget.getModalValuesString).toHaveBeenCalledWith(true);
                        });
                    });
                });
            });
        });
    }
}

let tester = new TextBoxCustomInputBaseTests();
tester.execTests();
