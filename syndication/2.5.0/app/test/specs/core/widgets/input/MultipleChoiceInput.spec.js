import Templates from 'core/resources/Templates';
import * as helpers from 'test/helpers/SpecHelpers';
import MultipleChoiceInput from 'core/widgets/input/MultipleChoiceInput';

const { Model } = Backbone;

class MultipleChoiceInputTests {
    get model () {
        return new Model();
    }

    get inputOptions () {
        return {
            model: this.model,
            itemTemplate: 'DEFAULT:ModalMultipleChoiceItem',
            template: 'DEFAULT:MultipleChoiceInputControl',
            items: [{
                text: 'IMAGE_OPTION_0',
                value: '0'
            }, {
                text: 'IMAGE_OPTION_1',
                value: '1'
            }, {
                text: 'IMAGE_OPTION_2',
                value: '2'
            }, {
                text: 'IMAGE_OPTION_3',
                value: '3'
            }],
            strings: {
                IMAGE_OPTION_0: 'test item 0',
                IMAGE_OPTION_1: 'test item 1',
                IMAGE_OPTION_2: 'test item 2',
                IMAGE_OPTION_3: 'test item 3'
            },
            callerId: 'test-caller',
            parent: $('<div></div>').get(0)
        };
    }

    execTests () {
        TRACE_MATRIX('US7038')
        .describe('MultipleChoiceInput', () => {
            let input;
            beforeEach(() => {
                input = new MultipleChoiceInput(this.inputOptions);
            });

            describe('#constructor', () => {
                it('constructs the MultipleChoiceInput class', () => {
                    expect(input.model.get('template')).toEqual(this.inputOptions.template);
                    expect(input.model.get('itemTemplate')).toEqual(this.inputOptions.itemTemplate);
                    expect(input.model.get('items')).toEqual(this.inputOptions.items);
                });
            });

            describe('#show()', () => {
                Async.it('calls render() if this.isRendered is false', () => {
                    spyOn(input, 'render').and.callFake(() => {
                        return Q(input.$el);
                    });
                    input.isRendered = false;
                    return input.show().then(() => {
                        expect(input.render).toHaveBeenCalled();
                    });
                });

                Async.it('does not call render() if this.isRendered is true', () => {
                    spyOn(input, 'render').and.callFake(() => {
                        return Q(input.$el);
                    });

                    // simulate situation where it has been rendered already.
                    input.isRendered = true;
                    input._renderPromise = Q();

                    return input.show().then(() => {
                        expect(input.render.calls.count()).toBe(0);
                    });
                });
            });

            describe('#render()', () => {
                Async.it('appends $el, displays items, and delegates events', () => {
                    spyOn(input, 'displayItems');
                    spyOn(input, 'delegateEvents');
                    input.$el.attr('id', 'test-div');
                    return input.render().then(() => {
                        expect(input.displayItems).toHaveBeenCalled();
                        expect(input.delegateEvents).toHaveBeenCalled();
                        expect(input.$el.select('.item-container').length).toBe(1);
                    });
                });
            });

            describe('#onHidden()', () => {
                it('calls clearValues', () => {
                    spyOn(input, 'clearValues');
                    input.onHidden();
                    expect(input.clearValues).toHaveBeenCalled();
                });
            });

            describe('#clearValues()', () => {
                it('removes active class and checked attribute', () => {
                    spyOn($.fn, 'removeClass');
                    spyOn($.fn, 'removeAttr');
                    input.clearValues();
                    expect($.fn.removeClass).toHaveBeenCalled();
                    expect($.fn.removeAttr).toHaveBeenCalled();
                });
            });

            describe('#itemDisplayValueFunction()', () => {
                it('returns value passed in... no processing required', () => {
                    expect(input.itemDisplayValueFunction('test value')).toBe('test value');
                });
            });

            describe('#setValue()', () => {
                Async.it('sets value and calls "pullValue" to update UI.', () => {
                    spyOn(input, 'pullValue').and.resolve();
                    return input.setValue('testVal').then(() => {
                        expect(input.value).toBe('testVal');
                        expect(input.pullValue).toHaveBeenCalled();
                    });
                });
            });

            describe('post-render functions', () => {
                Async.beforeEach(() => {
                    return input.render();
                });

                describe('#displayItems()', () => {
                    it('displays items from items array', () => {
                        input.$('.item-container').empty();

                        input.displayItems();
                        let $itemContainer = input.$('.item-container');
                        expect($itemContainer.find('.btn').length).toBe(4);
                        expect($itemContainer.find('input').length).toBe(4);

                        // Check input IDs
                        expect($itemContainer.find('input#modal-radio-test-caller-0-0').length).toBe(1);
                        expect($itemContainer.find('input#modal-radio-test-caller-0-1').length).toBe(1);
                        expect($itemContainer.find('input#modal-radio-test-caller-0-2').length).toBe(1);
                        expect($itemContainer.find('input#modal-radio-test-caller-0-3').length).toBe(1);

                        // Check input values
                        expect($itemContainer.find('input[value=0]').length).toBe(1);
                        expect($itemContainer.find('input[value=1]').length).toBe(1);
                        expect($itemContainer.find('input[value=2]').length).toBe(1);
                        expect($itemContainer.find('input[value=3]').length).toBe(1);
                    });
                });

                describe('#pullValue()', () => {
                    it('updates UI with model value', () => {
                        input.value = '2';
                        spyOn(input, 'clearValues').and.callThrough();
                        input.pullValue();
                        expect(input.clearValues).toHaveBeenCalled();
                        let $selected = input.$('input[checked]');
                        expect($selected.length).toBe(1);
                        expect($selected.attr('id')).toBe('modal-radio-test-caller-0-2');
                        expect($selected.closest('.btn.active').length).toBe(1);
                    });
                });

                describe('#pushValue()', () => {
                    it('pushes ui value to model', () => {
                        input.value = '2';
                        input.pullValue();
                        input.value = '3';
                        input.pushValue();
                        expect(input.value).toBe('2');
                    });
                });
            });
        });
    }
}

new MultipleChoiceInputTests().execTests();
