const { Model, View } = Backbone;
import BaseInput from 'core/widgets/input/BaseInput';
import Templates from 'core/resources/Templates';

/* global describe, beforeEach, afterEach, beforeAll, afterAll, it, spyOn */
TRACE_MATRIX('US7038')
.TRACE_MATRIX('US7326')
.describe('BaseInput', () => {
    let options,
        $parent,
        input,
        oldTemplateSettings;

    beforeAll(() => {
        // Set the correct interpolation settings for templates.
        oldTemplateSettings = _.templateSettings;
        _.templateSettings = {
            evaluate: /\{\[([\s\S]+?)]}/g,
            interpolate: /\{\{(.+?)}}/g
        };
    });

    afterAll(() => {
        _.templateSettings = oldTemplateSettings;
    });

    beforeEach(() => {
        $parent = $('<div class="spinner-container">');

        document.documentElement.appendChild(_.first($parent));

        options = {
            parent: _.first($parent),
            template: 'DEFAULT:MultipleChoiceInputControl',
            deceleration: 0.003,
            numItems: 5,
            model: new Model()
        };
    });

    afterEach(() => {
        if (input) {
            input.destroy();
            input.remove();
            input = null;
        }
        if ($parent) {
            $parent.remove();
            $parent = null;
        }
    });

    describe('Tests before rendered', () => {
        beforeEach(() => {
            input = new BaseInput(options);
        });
        describe('getters and setters', () => {
            describe('#value', () => {
                it('gets and sets the value in the model', () => {
                    input.value = 'test';
                    expect(input.model.get('value')).toBe('test');
                    expect(input.value).toBe('test');
                });
            });
        });

        describe('#constructor', () => {
            it('throws an error if no template', () => {
                options.template = null;
                expect(() => {
                    input = new BaseInput(options);
                }).toThrow(new Error('Invalid configuration for Input class.  "template" is undefined.'));
            });

            it('sets up model and variables', () => {
                options.strings = { test: 'test', test2: 'test2' };
                options.callerId = 'testcaller';
                input = new BaseInput(options);
                expect(input.model.get('strings')).toEqual(options.strings);
                expect(input.model.get('callerId')).toEqual(options.callerId);
                expect(input.model.get('template')).toEqual(options.template);
                expect(typeof input._renderPromise.then).toBe('function');

                // Check to see if isRendered is a promise
                expect(input.isRendered).toBe(false);
            });
        });

        describe('#show', () => {
            Async.it('calls render if not already rendered', () => {
                spyOn(input, 'render').and.callFake(() => Q());
                return input.show()
                    .tap(() => {
                        let expected = 1;
                        expect(input.render.calls.count()).toBe(expected);
                    });
            });
            Async.it('skips if already rendered', () => {
                spyOn(input, 'render').and.callFake(() => Q());
                input.isRendered = true;
                return input.show()
                    .tap(() => {
                        let expected = 0;
                        expect(input.render.calls.count()).toBe(expected);
                    });
            });
        });
    });
    describe('Tests after rendering', () => {
        beforeEach((done) => {
            input = new BaseInput(options);

            input.render().then(() => {
                done();
            }).catch((e) => {
                fail(e);
            });
        });

        describe('#delegateEvents', () => {
            it('calls super', () => {
                spyOn(View.prototype, 'delegateEvents');
                spyOn(input, 'addCustomEvents');

                input.delegateEvents();
                let expected = 1;
                expect(View.prototype.delegateEvents.calls.count()).toBe(expected);
            });
            it('calls addCustomEvents()', () => {
                spyOn(View.prototype, 'delegateEvents');
                spyOn(input, 'addCustomEvents');

                input.delegateEvents();
                let expected = 1;
                expect(input.addCustomEvents.calls.count()).toBe(expected);
            });
        });

        describe('#undelegateEvents', () => {
            it('calls super', () => {
                spyOn(View.prototype, 'undelegateEvents');
                spyOn(input, 'removeCustomEvents');

                input.undelegateEvents();
                let expected = 1;
                expect(View.prototype.undelegateEvents.calls.count()).toBe(expected);
            });
            it('calls removeEvents()', () => {
                spyOn(View.prototype, 'delegateEvents');
                spyOn(input, 'removeCustomEvents');

                input.undelegateEvents();
                let expected = 1;
                expect(input.removeCustomEvents.calls.count()).toBe(expected);
            });
        });

        describe('#pullValue', () => {
            it('throws an error if unimplemented', () => {
                expect(() => {
                    input.pullValue();
                }).toThrow(new Error('Unimplemented pullValue function'));
            });
        });

        describe('#pushValue', () => {
            it('throws an error if unimplemented', () => {
                expect(() => {
                    input.pushValue();
                }).toThrow(new Error('Unimplemented pushValue function'));
            });
        });

        describe('#destroy', () => {
            it('calls undelegateEvents', () => {
                spyOn(input, 'undelegateEvents');
                input.destroy();
                let expected = 1;
                expect(input.undelegateEvents).toHaveBeenCalledTimes(expected);
            });
        });
    });
});
