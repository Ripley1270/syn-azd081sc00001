import 'test/helpers/SpecHelpers';

describe('Select2 Button Decorator', () => {
    let data = [{ id: 1, text: 'one' }, { id: 2, text: 'two' }, { id: 3, text: 'three' }],
        buttonSelector = '.select2-results > span > a.btn.btn-default.btn-block',
        template = `<div id="user-template-1">
                        <label for="user1">USER:</label>
                        <select id="user1" name="user1"></select>
                    </div>
                    <div id="user-template-2">
                        <label for="user2">USER:</label>
                        <select id="user2" name="user2"></select>
                    </div>`;

    beforeAll(() => {
        $('body').append(template);
    });

    describe('when button decorator is not used', () => {
        Async.beforeAll(() => {
            return Q.Promise((resolve) => {
                $('#user1').select2({
                    language: {noResults: () => 'No Results'},
                    escapeMarkup: (markup) => markup,
                    data: data
                });

                $('#user1').on('select2:open', () => resolve());

                $('#user1').select2('open');
            });
        });

        it('should not render a button', () => {
            expect($(buttonSelector).length).toEqual(0, `${buttonSelector} should NOT be found`);
        });

        afterAll(() => {
            $('#user1').select2('destroy');
            $('#user-template-1').remove();
        });
    });

    //DE15892
    xdescribe('when button decorator is used', () => {
        let buttonText = 'This is the button text',
            isClickHandled = false,
            onClickHandler = () => {
                isClickHandled = true;
            };

        Async.beforeAll(() => {
            return Q.Promise((resolve) => {
                $('#user2').on('select2:open', resolve);

                $.fn.select2.amd.require([
                    'select2/utils',
                    'select2/dropdown',
                    'select2/dropdown/attachBody',
                    'select2/dropdown/button'
                ], (utils, dropdownAdapter, attachBody, button) => {
                    let decorated = utils.Decorate(dropdownAdapter, attachBody);
                    decorated = utils.Decorate(decorated, button);

                    $('#user2').select2({
                        language: { noResults: () => 'No Results' },
                        escapeMarkup: (markup) => markup,
                        data: data,
                        dropdownAdapter: decorated,
                        buttonText: buttonText,
                        onButtonClick: onClickHandler
                    });

                    $('#user2').select2('open');
                });
            });
        });

        it('should render the button', () => {
            expect($(buttonSelector).length).toEqual(1, `${buttonSelector} should be found`);
        });

        it('should customize the button text', () => {
            expect($($(buttonSelector)[0]).text()).toEqual(buttonText);
        });

        it('should customize the button click handler', () => {
            $($(buttonSelector)[0]).click();
            expect(isClickHandled).toEqual(true, 'should have called click handler');
        });

        afterAll(() => {
            $('#user2').select2('destroy');
            $('#user-template-2').remove();
        });
    });
});
