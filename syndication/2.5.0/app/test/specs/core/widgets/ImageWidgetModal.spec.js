/**
 * TODO:  Before running, replace the following patterns
 * ImageWidgetModal - Your widget name (case sensitive)
 */

import 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';
import 'core/resources/Templates';

import WidgetBaseTests from './WidgetBase.specBase.js';
import Widget from 'core/models/Widget';
import ImageWidgetModal from 'core/widgets/ImageWidgetModal';

/* global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
export default class ImageWidgetModalTests extends WidgetBaseTests {
    constructor (model = ImageWidgetModalTests.model) {
        super(model);
    }

    static testMixin (superclass) {
        return class extends superclass {
            storageValueToDisplayFormat (val) {
                return val;
            }

            displayValueToStorageFormat (val) {
                return val;
            }

            getModalValuesString () {
                return 'test';
            }

            getModalTranslationsObject () {
                return {};
            }

            renderModal () {
                return Q();
            }

            openDialog () {
                return Q();
            }
        };
    }

    /**
     * {Widget} get the widget model.
     */
    static get model () {
        return new Widget({
            id: 'ImageWidgetModal_W_1',
            type: 'ImageWidgetModal',
            className: 'ImageWidgetModal_W_1',
            modalMixin: ImageWidgetModalTests.testMixin,
            imageName: 'skeleton.svg',
            modalTitle: 'SELECT_PAIN',
            options: [
                {
                    value: '0',
                    css: {
                        fill: '#FFFFFF'
                    }
                }, {
                    value: '1',
                    css: {
                        fill: '#0000FF'
                    }
                }, {
                    value: '2',
                    css: {
                        fill: '#FF0000'
                    }
                }, {
                    value: '3',
                    css: {
                        fill: '#00FF00'
                    }
                }
            ],
            imageText: {
                legend0: 'Q4_LEGEND_0',
                legend1: 'Q4_LEGEND_1'
            },
            nodeIDRegex: /[rl]\d+/
        });
    }

    htmlRenderedCheck () {
        const myModel = ImageWidgetModalTests.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${this.dummyParent.id}`).find('object').length).toEqual(1);
            });
    }

    checkNeedMultipleWidgetSupport () {
        const myModel = ImageWidgetModalTests.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        expect(widget.needMultipleWidgetSupport).toBeTruthy();
    }

    execTests () {
        super.execTests();

        // eslint-disable-next-line new-cap
        TRACE_MATRIX('US7326')
        .describe('ImageWidgetModal', () => {
            let templates,
                dummyParent,
                security,
                widget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
                widget = new LF.Widget[ImageWidgetModalTests.model.get('type')]({
                    model: ImageWidgetModalTests.model,
                    mandatory: false,
                    parent: dummyParent
                });
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
                widget = null;
            });

            describe('#getValueForModal', () => {
                it('returns data(state) value of selected node', () => {
                    widget.$selectedNode = {
                        data: (param) => {
                            return param === 'state' ? '3' : null;
                        }
                    };

                    expect(widget.getValueForModal()).toBe('3');
                });

                it('returns 0 if data(state) of selected node is falsy', () => {
                    widget.$selectedNode = {
                        data: (param) => {
                            return param === 'state' ? false : 'unfalse';
                        }
                    };

                    expect(widget.getValueForModal()).toBe('0');
                });

                it('returns 0 if no selected node', () => {
                    widget.$selectedNode = null;
                    expect(widget.getValueForModal()).toBe('0');
                });
            });

            describe('#render', () => {
                Async.it('calls renderModal with translations object, delegates events', () => {
                    spyOn(widget, 'getModalTranslationsObject').and.returnValue({ testProp: 'testval' });
                    spyOn(widget, 'renderModal').and.resolve();
                    spyOn(widget, 'delegateEvents');

                    return widget.render()
                    .then(() => {
                        expect(widget.i18n).toHaveBeenCalledWith({ testProp: 'testval' }, jasmine.any(Function), { namespace: 'dummy1-questionnaire' });
                        expect(widget.renderModal).toHaveBeenCalled();
                        expect(widget.delegateEvents).toHaveBeenCalled();
                    });
                });
            });

            describe('post-rendered methods', () => {
                Async.beforeEach(() => {
                    return widget.render();
                });

                describe('#propagateModalValue', () => {
                    Async.it('updates selectedNode value and style, calls handleMultiSelect', () => {
                        spyOn(widget, 'getModalValuesString').and.resolve('3');
                        widget.$selectedNode = {
                            data: $.noop,
                            css: $.noop
                        };
                        spyOn(widget.$selectedNode, 'data');
                        spyOn(widget.$selectedNode, 'css');
                        spyOn(widget, 'handleMultiSelect').and.resolve();

                        let $cachedSelectedNode = widget.$selectedNode;

                        return widget.propagateModalValue()
                        .then(() => {
                            // false is default for stopModal
                            expect(widget.getModalValuesString).toHaveBeenCalledWith(false);
                            expect($cachedSelectedNode.data).toHaveBeenCalledWith('state', '3');
                            expect($cachedSelectedNode.css).toHaveBeenCalledWith(widget.model.get('options')[3].css);
                            expect(widget.handleMultiSelect).toHaveBeenCalled();
                            expect(widget.$selectedNode).toBeNull();
                        });
                    });

                    Async.it('calls getModalValuesString with true if true passed in', () => {
                        spyOn(widget, 'getModalValuesString').and.resolve('3');
                        widget.$selectedNode = {
                            data: $.noop,
                            css: $.noop
                        };
                        spyOn(widget.$selectedNode, 'data');
                        spyOn(widget.$selectedNode, 'css');
                        spyOn(widget, 'handleMultiSelect').and.resolve();

                        return widget.propagateModalValue(true)
                        .then(() => {
                            // false is default for stopModal
                            expect(widget.getModalValuesString).toHaveBeenCalledWith(true);
                        });
                    });
                });

                describe('#respond', () => {
                    Async.it('updates selectedNode, calls openDialog', () => {
                        let $elem = $('<div id="myTestElement"></div>');
                        spyOn(widget, 'openDialog').and.resolve();
                        return widget.respond({ target: $elem })
                        .then(() => {
                            expect(widget.$selectedNode.attr('id')).toBe('myTestElement');
                            expect(widget.openDialog).toHaveBeenCalled();
                        });
                    });
                });
            });
        });
    }
}

let tester = new ImageWidgetModalTests();
tester.execTests();
