import 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';
import 'core/resources/Templates';

import DateSpinnerMixin from 'core/widgets/mixin/DateSpinnerMixin';
import TextBoxCustomInputBaseTests from './TextBoxCustomInputBase.spec.js';
import Widget from 'core/models/Widget';
import DateSpinner from 'core/widgets/DateSpinner';

/* global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
export default class DateSpinnerTests extends TextBoxCustomInputBaseTests {
    constructor (model = DateSpinnerTests.model) {
        super(model);
    }

    /**
     * {Widget} get the widget model.
     */
    static get model () {
        return new Widget({
            id: 'DateSpinner_W_1',
            type: 'DateSpinner',
            className: 'DateSpinner_W_1'
        });
    }

    execTests () {
        super.execTests();

        // eslint-disable-next-line new-cap

        TRACE_MATRIX('US6106')
        .TRACE_MATRIX('US7038')
        .describe('DateSpinner', () => {
            let templates,
                dummyParent,
                security,
                widget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
                widget = new LF.Widget[DateSpinnerTests.model.get('type')]({
                    model: DateSpinnerTests.model,
                    mandatory: false,
                    parent: dummyParent
                });
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
                widget = null;
            });

            describe('#constructor', () => {
                it('initializes TextBoxCustomInputBase with DateSpinnerMixin', () => {
                    let model = DateSpinnerTests.model;
                    spyOn(model, 'set').and.callThrough();
                    widget = new LF.Widget[DateSpinnerTests.model.get('type')]({
                        model,
                        mandatory: false,
                        parent: this.dummyParent
                    });

                    expect(model.set).toHaveBeenCalledWith('modalMixin', DateSpinnerMixin);
                });
            });
        });
    }
}

let tester = new DateSpinnerTests();
tester.execTests();
