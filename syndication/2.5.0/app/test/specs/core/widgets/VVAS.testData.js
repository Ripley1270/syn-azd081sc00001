export default {
    schedules: [
        {
            id: 'Patient_VVAS_Test_DiarySchedule_01',
            target: {
                objectType: 'questionnaire',
                id: 'P_VerticalVAS_Diary'
            },
            scheduleFunction: 'checkAlwaysAvailability',
            scheduleRoles: ['subject', 'site', 'admin']
        }
    ],
    questionnaires: [
        {
            id: 'P_VerticalVAS_Diary',
            SU: 'VVAS',
            displayName: 'DISPLAY_NAME',
            className: 'VVAS',
            affidavit: undefined,
            previousScreen: true,
            triggerPhase: 'TREATMENT',
            screens: [
                'VVAS_TEST_S1',
                'VVAS_TEST_S2',
                'VVAS_TEST_S3',
                'VVAS_TEST_S4',
                'VVAS_TEST_S5',
                'VVAS_TEST_S6',
                'VVAS_TEST_S7',
                'VVAS_TEST_S8',
                'VVAS_TEST_S9',
                'VVAS_TEST_S10'
            ]
        }
    ],
    screens: [
        {
            id: 'VVAS_TEST_S1',
            className: 'VVAS_TEST_S1',
            questions: [
                {
                    id: 'VVAS_TEST_S1_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S2',
            className: 'VVAS_TEST_S2',
            questions: [
                {
                    id: 'VVAS_TEST_S2_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S3',
            className: 'VVAS_TEST_S3',
            questions: [
                {
                    id: 'VVAS_TEST_S3_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S4',
            className: 'VVAS_TEST_S4',
            questions: [
                {
                    id: 'VVAS_TEST_S4_Q1',
                    mandatory: false
                }
            ]
        },
        {
            id: 'VVAS_TEST_S5',
            className: 'VVAS_TEST_S5',
            questions: [
                {
                    id: 'VVAS_TEST_S5_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S6',
            className: 'VVAS_TEST_S6',
            questions: [
                {
                    id: 'VVAS_TEST_S6_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S7',
            className: 'VVAS_TEST_S7',
            questions: [
                {
                    id: 'VVAS_TEST_S7_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S8',
            className: 'VVAS_TEST_S8',
            questions: [
                {
                    id: 'VVAS_TEST_S8_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S9',
            className: 'VVAS_TEST_S9',
            questions: [
                {
                    id: 'VVAS_TEST_S9_Q1',
                    mandatory: true
                }
            ]
        },
        {
            id: 'VVAS_TEST_S10',
            className: 'VVAS_TEST_S10',
            questions: [
                {
                    id: 'VVAS_TEST_S10_Q1',
                    mandatory: true
                }
            ]
        }
    ],
    questions: [
        {
            id: 'VVAS_TEST_S1_Q1', // tests default anchor values, basic appearance
            IG: 'VVAS',
            IT: 'VVAS1',
            title: 'VVAS_Q1_TITLE',
            text: ['VVAS_Q1_TEXT'],
            className: 'VVAS_TEST_S1_Q1',
            widget: {
                id: 'VVAS_TEST_W1',
                type: 'VerticalVAS'  // no posting specified = off
            }
        },
        {
            id: 'VVAS_TEST_S2_Q1',
            IG: 'VVAS',
            IT: 'VVAS2',
            title: 'VVAS_Q2_TITLE',
            text: ['VVAS_Q2_TEXT'],
            className: 'VVAS_TEST_S2_Q1',
            widget: {
                id: 'VVAS_TEST_W2',
                type: 'VerticalVAS',

                // Tests cursor explicitly OFF
                initialCursorDisplay: true,

                // Tests posting explicitly ON
                posting: true,
                pointer: {
                    // Tests pointer explicitly OFF
                    isVisible: true
                },
                topAnchor: {
                    text: 'NO_PAIN'
                },
                bottomAnchor: {
                    text: 'EXTREME_PAIN'
                }
            }
        },
        {
            id: 'VVAS_TEST_S3_Q1',
            IG: 'VVAS',
            IT: 'VVAS3',
            title: 'VVAS_Q3_TITLE',
            text: ['VVAS_Q3_TEXT'],
            className: 'VVAS_TEST_S3_Q1',
            widget: {
                id: 'VVAS_TEST_W3',
                type: 'VerticalVAS',
                initialCursorDisplay: true,
                posting: true,
                displayValueLocation: 'dynamic',
                pointer: {
                    isVisible: true
                },
                topAnchor: {
                    text: 'NO_PAIN'
                },
                bottomAnchor: {
                    text: 'EXTREME_PAIN'
                },
                minValue: 5,
                maxValue: 25
            }
        },
        {
            // Tests configurable anchors...everything ON
            id: 'VVAS_TEST_S4_Q1',
            IG: 'VVAS',
            IT: 'VVAS4',
            skipIT: 'VVAS4_SKP',
            title: 'VVAS_Q4_TITLE',
            text: ['VVAS_Q4_TEXT'],
            className: 'VVAS_TEST_S4_Q1',
            widget: {
                id: 'VVAS_TEST_W4',
                type: 'VerticalVAS',
                posting: true,
                initialCursorDisplay: true,
                displayValueLocation: 'static',
                pointer: {
                    isVisible: true
                },
                topAnchor: {
                    text: 'NO_PAIN'
                },
                bottomAnchor: {
                    text: 'EXTREME_PAIN'
                }
            }
        },
        {
            // Tests font customization.  Top anchor - Arial 30, Bottom anchor Courier 12,  Pointer Times New Roman 46, answerFont Garamond 80
            id: 'VVAS_TEST_S5_Q1',
            IG: 'VVAS',
            IT: 'VVAS5',
            title: 'VVAS_Q5_TITLE',
            text: ['VVAS_Q5_TEXT'],
            className: 'VVAS_TEST_S5_Q1',
            widget: {
                id: 'VVAS_TEST_W5',
                type: 'VerticalVAS',
                posting: true,
                initialCursorDisplay: true,
                displayValueLocation: 'both',
                pointer: {
                    isVisible: true,
                    color: '#F5E725'
                },
                topAnchor: {
                    text: 'NO_PAIN'
                },
                bottomAnchor: {
                    text: 'EXTREME_PAIN'
                },
                minValue: 0,
                maxValue: 10
            }
        },
        {
            // Tests font customization.  Top anchor - Arial 30, Bottom anchor Courier 12,  Pointer Times New Roman 46, answerFont Garamond 80
            id: 'VVAS_TEST_S6_Q1',
            IG: 'VVAS',
            IT: 'VVAS6',
            title: 'VVAS_Q6_TITLE',
            text: ['VVAS_Q6_TEXT'],
            className: 'VVAS_TEST_S6_Q1',
            widget: {
                id: 'VVAS_TEST_W6',
                type: 'VerticalVAS',
                posting: false,
                initialCursorDisplay: false,
                pointer: {
                    isVisible: false
                },
                topAnchor: {
                    text: 'NO_PAIN'
                },
                bottomAnchor: {
                    text: 'EXTREME_PAIN'
                }
            }
        },
        {
            // Tests font customization.  Top anchor - Arial 30, Bottom anchor Courier 12,  Pointer Times New Roman 46, answerFont Garamond 80
            id: 'VVAS_TEST_S7_Q1',
            IG: 'VVAS',
            IT: 'VVAS7',
            title: 'VVAS_Q7_TITLE',
            text: ['VVAS_Q7_TEXT'],
            className: 'VVAS_TEST_S7_Q1',
            widget: {
                id: 'VVAS_TEST_W7',
                type: 'VerticalVAS',
                posting: false,
                initialCursorDisplay: true,
                pointer: {
                    isVisible: true
                },
                topAnchor: {
                    text: 'NO_PAIN'
                },
                bottomAnchor: {
                    text: 'EXTREME_PAIN'
                }
            }
        },
        {
            // Tests font customization.  Top anchor - Arial 30, Bottom anchor Courier 12,  Pointer Times New Roman 46, answerFont Garamond 80
            id: 'VVAS_TEST_S8_Q1',
            IG: 'VVAS',
            IT: 'VVAS8',
            title: 'VVAS_Q8_TITLE',
            text: ['VVAS_Q8_TEXT'],
            className: 'VVAS_TEST_S8_Q1',
            widget: {
                id: 'VVAS_TEST_W8',
                type: 'VerticalVAS',
                posting: true,
                initialCursorDisplay: false,
                pointer: {
                    isVisible: true,
                    location: 'right'
                },
                topAnchor: {
                    text: 'NO_PAIN'
                },
                bottomAnchor: {
                    text: 'EXTREME_PAIN'
                }
            }
        },
        {
            // Tests font customization. Top anchor - Arial 30, Bottom anchor Courier 12,  Pointer Times New Roman 46, answerFont Garamond 80
            id: 'VVAS_TEST_S9_Q1',
            IG: 'VVAS',
            IT: 'VVAS9',
            title: 'VVAS_Q9_TITLE',
            text: ['VVAS_Q9_TEXT'],
            className: 'VVAS_TEST_S9_Q1',
            widget: {
                id: 'VVAS_TEST_W9',
                type: 'VerticalVAS',
                posting: true,
                displayValueLocation: 'both',
                initialCursorDisplay: true,
                answerFont: {
                    name: 'Times New Roman',
                    size: 60
                },
                pointer: {
                    isVisible: true,
                    font: {
                        name: 'Helvetica',
                        size: 18
                    }
                },
                topAnchor: {
                    text: 'NO_PAIN',
                    font: {
                        name: 'Arial',
                        size: 30
                    }
                },
                bottomAnchor: {
                    text: 'EXTREME_PAIN',
                    font: {
                        name: 'Garamond',
                        size: 10
                    }
                }
            }
        },
        {
            // Tests font customization.  Top anchor - Arial 30, Bottom anchor Courier 12,  Pointer Times New Roman 46, answerFont Garamond 80
            id: 'VVAS_TEST_S10_Q1',
            IG: 'VVAS',
            IT: 'VVAS10',
            title: 'VVAS_Q10_TITLE',
            text: ['VVAS_Q10_TEXT'],
            className: 'VVAS_TEST_S10_Q1'
        }
    ]
};
