import Widget from 'core/models/Widget';
import Language from 'core/models/Language';

import Answers from 'core/collections/Answers';

import Templates from 'core/collections/Templates';

import HorizontalVAS from '../../../../core/widgets/VAS/HorizontalVAS';

import Data from 'core/Data';
import * as helpers from 'core/Helpers';

import * as specHelpers from 'test/helpers/SpecHelpers';

import WidgetBaseTests from './WidgetBase.specBase';

class HorizontalVASCoreTests extends WidgetBaseTests{
    constructor (renderedSelector, options) {
        super(options);
        this.renderedSelector = renderedSelector;
    }

    htmlRenderedCheck () {
        const myModel = this.model,
            widget = new LF.Widget[myModel.get('type')]({
                model: myModel,
                mandatory: false,
                parent: this.dummyParent
            });

        return widget.render()
            .then(() => {
                expect($(`#${widget.question.id}`).find(this.renderedSelector).length).toEqual(1);
            });
    }
}

//jscs:disable requireArrowFunctions
/*global describe, it, beforeEach, assert, afterEach, expect, xdescribe, beforeAll, afterAll */
describe('HorizontalVAS', () => {

    let dummyParent,
        security;

    const model = new Widget({
        type: 'HorizontalVAS',
        id: 'testWidgetId',
        initialCursorDisplay: false,
        pointer: {
            isVisible: true
        },
        anchors: {
            min: {
                text:'NO_PAIN',
                value: 0
            },
            max: {
                text:'EXTREME_PAIN',
                value: 100
            }
        },
        customProperties: {
            labels: {
                position:'above',
                arrow:false,
                justification:'screenEdge'
            }
        }
    });

    const baseTests = new HorizontalVASCoreTests('.vas-label', model);
    baseTests.execTests();

    beforeAll(function () {
        specHelpers.installDatabase();
        security = LF.security;
        LF.security = WidgetBaseTests.getDummySecurity();
    });

    afterAll(function () {
        specHelpers.uninstallDatabase();
        LF.security = security;
    });

    beforeEach(() => {
        spyOn(helpers, 'checkInstall').and.callFake(callback => callback(true));

        Data.Questionnaire = {};
        LF.Resources.Templates = new Templates([{
            name        : 'VasTextLabels',
            namespace   : 'DEFAULT',
            template    : `<div class="vas-label row">
                    <div class="col-xs-6 text-left"><p>{{ min }}</p></div>
                    <div class="col-xs-6 text-right"><p>{{ max }}</p></div>
                </div>`
        }]);
        LF.templates = LF.Resources.Templates;

        dummyParent = baseTests.getMinDummyQuestion();
    });

    afterEach(function () {
        Data = {};
        LF.router = undefined;
        LF.schedule = undefined;

        $(`#${dummyParent.getQuestionnaire().id}`).remove();
    });

    describe('with center justified labels below vas line and canvas width 506px', () => {
        let modelWidget,
            canvasWidth,
            widgetVas;

        beforeEach(() => {
            canvasWidth = 506;
            modelWidget = new Widget({
                id                      : 'VAS_DIARY_W_6',
                type                    : 'VAS',
                posting                 : true,
                initialCursorDisplay    : true,
                pointer                 : {
                    isVisible: false
                },
                anchors: {
                    min: {
                        text: 'NO_PAIN',
                        value: '0'
                    },
                    max: {
                        text: 'EXTREME_PAIN_LONG',
                        value: '100'
                    }
                },
                customProperties: {
                    labels: {
                        position:'below',
                        arrow:false,
                        justification:'center'
                    }
                }
            });

            widgetVas = new LF.Widget[modelWidget.get('type')]({
                model: modelWidget,
                answer : new Answers(),
                parent: dummyParent
            });
            spyOn(widgetVas, 'getCanvasWidth').and.returnValue(canvasWidth);
        });

        afterEach(() => {
            widgetVas = null;
        });

        it('should have label position \'below\'', function () {

            expect(widgetVas.customProperties.labels.position).toEqual('below');

        });

        it('should have arrow value \'false\'', function () {

            expect(widgetVas.customProperties.labels.arrow).toEqual(false);

        });

        it('should have center justification', function () {

            expect(widgetVas.customProperties.labels.justification).toEqual('center');

        });

        it('should have default posting value \'false\'', function () {

            expect(widgetVas.selectedValue.isVisible).toEqual(false);

        });

        it('should have default pointer value \'false\'', function () {

            expect(widgetVas.pointer.isVisible).toEqual(false);

        });

        it('should have initial cursor display value \'true\'', function () {

            expect(widgetVas.initialCursorDisplay).toEqual(true);

        });

        it('should create canvas', function () {

            widgetVas.createCanvas();

            expect(widgetVas.numLabelCanvas.height).toBe(10);
            expect(widgetVas.vasCanvas.height).toBe(10);
            expect(widgetVas.arrowsCanvas.height).toBe(10);
            expect(widgetVas.unitCursorCanvas.height).toBe(10);

            expect(widgetVas.ctxNumLabels).toBeDefined();
            expect(widgetVas.ctxVas).toBeDefined();
            expect(widgetVas.ctxArrows).toBeDefined();
            expect(widgetVas.ctxUnitCursor).toBeDefined();

            expect($(widgetVas.unitCursorCanvas).hasClass('vas-pointer')).toBeTruthy();

        });

        Async.it('should build html layout', function () {
            widgetVas.createCanvas();
            return widgetVas.render().then(() => {
                expect(widgetVas.$el.children().last().hasClass('vas-label')).toBeTruthy();
            });
        });

        it('should set canvas pixels', function () {
            widgetVas.createCanvas(canvasWidth);
            widgetVas.createVAS(canvasWidth);
            widgetVas.setCanvasPixels(canvasWidth);

            expect($(widgetVas.numLabelCanvas).height()).toBe(widgetVas.numLabelCanvas.height);
            expect($(widgetVas.arrowsCanvas).height()).toBe(widgetVas.pointer.height * 1.5);
            expect($(widgetVas.vasCanvas).height()).toBe(widgetVas.vasCanvas.height);
            expect($(widgetVas.numLabelCanvas).width()).toBe(canvasWidth);
            expect($(widgetVas.vasCanvas).width()).toBe(canvasWidth);
            expect($(widgetVas.arrowsCanvas).width()).toBe(canvasWidth);
        });

        describe('--tests after render--', () => {
            Async.beforeEach(() => {

                // Put spies for render here.
                spyOn(widgetVas, 'moveUnitCursor').and.callThrough();
                spyOn(widgetVas, 'renderArrows').and.callThrough();
                spyOn(widgetVas, 'drawArrows').and.callThrough();

                return widgetVas.drawTheVas();
            });


            it('should set the position of text labels', function () {
                expect(widgetVas.$('.vas-label').hasClass('vas-label-screenEdge')).toBeTruthy();
            });

            it('should not draw arrows', function () {
                expect(widgetVas.renderArrows).not.toHaveBeenCalled();
                expect(widgetVas.drawArrows).toHaveBeenCalled();
            });

            it('should have unit pixel size of 4 (5 minus padding for arrow/anchors)', function () {
                expect(widgetVas.vasLine.getUnitPixelSize()).toEqual(4);
            });

            it('should have first unit pixel size of 2', function () {
                expect(widgetVas.vasLine.getFirstUnitPixelSize()).toEqual(2);
            });

            it('should have vas line width of 400', function () {
                expect(widgetVas.vasLine.width).toEqual(400);
            });

            it('should have vas line height of 3', function () {
                expect(widgetVas.vasLine.height).toEqual(3);
            });

            it('should have anchor height of 37', function () {
                expect(widgetVas.anchors.height).toEqual(37);
            });

            it('should have anchor width of 3', function () {
                expect(widgetVas.anchors.width).toEqual(3);
            });

            it('should have anchor center pixel of 2', function () {

                expect(widgetVas.anchors.getCenterPixel()).toEqual(2);

            });

            it('should have anchor extension of 17', function () {

                expect(widgetVas.anchors.getExtension()).toEqual(17);

            });

            it('should have unit cursor height of 33', function () {

                expect(widgetVas.strikeMark.height).toEqual(33);

            });

            it('should have unit cursor width of 5', function () {

                expect(widgetVas.strikeMark.width).toEqual(5);

            });

            it('should have unit cursor center pixel of 3', function () {

                expect(widgetVas.strikeMark.getCenterPixel()).toEqual(3);

            });

            it('should have unit cursor extension of 15', function () {

                expect(widgetVas.strikeMark.getExtension()).toEqual(15);

            });

            it('should have  margin of 51', function () {

                expect(widgetVas.vasPosition.getMargin()).toEqual(51);

            });

            it('should have vas line starting at pixel x axis of 53', function () {

                expect(widgetVas.vasPosition.getLineStartX()).toEqual(53);

            });

            it('should have vas line starting at pixel y axis of 17', function () {

                expect(widgetVas.vasPosition.getLineStartY()).toEqual(17);

            });

            it('should have vas line ending at pixel x axis of 452', function () {

                expect(widgetVas.vasPosition.getLineEndX()).toEqual(452);

            });

            it('should have unit cursor starting at pixel y axis of 2', function () {

                expect(widgetVas.vasPosition.getCursorStartY()).toEqual(2);

            });

            it('should have left anchor starting at pixel x axis of 52', function () {

                expect(widgetVas.vasPosition.getLeftAnchorX()).toEqual(52);

            });

            it('should have left anchor starting at pixel y axis of 0', function () {

                expect(widgetVas.vasPosition.getLeftAnchorY()).toEqual(0);

            });

            it('should have right anchor starting at pixel x axis of 451', function () {

                expect(widgetVas.vasPosition.getRightAnchorX()).toEqual(451);

            });

            it('should have right anchor starting at pixel y axis of 0', function () {

                expect(widgetVas.vasPosition.getRightAnchorY()).toEqual(0);

            });

            it('should have pointer height of 40', function () {

                expect(widgetVas.pointer.height).toEqual(40);

            });

            it('should have pointer width of 20', function () {

                expect(widgetVas.pointer.width).toEqual(20);

            });

            it('should have pointer color of #00ACE6 (teal)', function () {

                expect(widgetVas.pointer.color).toEqual('#00ACE6');

            });

            it('should have numeric value of 0 for the first pixel of vas line', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineStartX())).toEqual(0);

            });

            it('should have numeric value of 100 for the last pixel of vas line', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineEndX())).toEqual(100);

            });

            it('should have numeric value of 50 for the middle pixel of vas line', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineStartX() + (widgetVas.vasLine.width / 2))).toEqual(50);
            });

            it('should have numeric value 0 for the point that has numeric value 0', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(0))).toEqual(0);

            });

            it('should have numeric value 1 for the point that has numeric value 1', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(1))).toEqual(1);

            });

            it('should have numeric value 29 for the point that has numeric value 29', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(29))).toEqual(29);

            });

            it('should have numeric value 82 for the point that has numeric value 82', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(82))).toEqual(82);

            });

            it('should have numeric value 99 for the point that has numeric value 99', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(99))).toEqual(99);

            });

            it('should have numeric value 100 for the point that has numeric value 100', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(100))).toEqual(100);

            });

            it('should draw initial unit cursor in the middle of the vas line', function () {
                expect(widgetVas.moveUnitCursor).toHaveBeenCalledWith(widgetVas.getCursorPoint(50), false, false);

            });

            it('should move unit cursor', function () {
                let x = 5 + widgetVas.vasPosition.getMargin(),
                    y = 33;

                spyOn(widgetVas, 'clearCanvas');
                spyOn(widgetVas, 'renderUnitCursor');
                spyOn(widgetVas, 'renderPointer');
                spyOn(widgetVas, 'renderPostingValue');

                widgetVas.checkUnitCursorMove(x, y);
                widgetVas.moveUnitCursor(x);

                expect(widgetVas.clearCanvas).toHaveBeenCalled();
                expect(widgetVas.renderUnitCursor).toHaveBeenCalledWith(x);
                expect(widgetVas.renderPointer).toHaveBeenCalledWith(x);
                expect(widgetVas.renderPostingValue).toHaveBeenCalledWith(widgetVas.getVASNumericValue(x), x);

            });

            it('should clear canvas', function () {

                widgetVas.canvasWidth = 100;

                widgetVas.clearCanvas();

                expect(widgetVas.unitCursorCanvas.width).toEqual(widgetVas.canvasWidth);

            });

            it('should render scale containing left and right anchors', function () {

                spyOn(widgetVas.ctxVas, 'fillRect');

                widgetVas.renderScale();

                expect(widgetVas.ctxVas.fillRect.calls.count()).toEqual(3);

            });

            it('should render unit cursor', function () {

                spyOn(widgetVas.ctxUnitCursor, 'fillRect');

                widgetVas.renderUnitCursor(100);

                expect(widgetVas.ctxUnitCursor.fillRect.calls.count()).toEqual(2);

            });

            it('should render numeric labels', function () {

                widgetVas.renderNumericLabels();

                expect(widgetVas.ctxNumLabels.textAlign).toBe('center');

            });

            it('should not render pointer', function () {

                spyOn(widgetVas.ctxVas, 'fill');

                widgetVas.renderPointer();

                expect(widgetVas.ctxVas.fill).not.toHaveBeenCalled();

            });

            it('should render posting value', function () {

                widgetVas.renderPostingValue(50);

                expect(widgetVas.ctxNumLabels.textBaseline).toBe('bottom');
                expect(widgetVas.ctxNumLabels.textAlign).toBe('center');

            });
        });
    });

    describe('width canvas width 266px', function () {
        let modelWidget,
            canvasWidth,
            widgetVas;

        beforeEach(() => {
            canvasWidth = 266;
            modelWidget = new Widget({
                id                      : 'VAS_DIARY_W_6',
                type                    : 'VAS',
                posting                 : true,
                initialCursorDisplay    : true,
                pointer                 : {
                    isVisible: false
                },
                anchors: {
                    min: {
                        text: 'NO_PAIN',
                        value: '0'
                    },
                    max: {
                        text: 'EXTREME_PAIN_LONG',
                        value: '100'
                    }
                },
                customProperties: {
                    labels: {
                        position:'below',
                        arrow:false,
                        justification:'center'
                    }
                }
            });

            widgetVas = new LF.Widget[modelWidget.get('type')]({
                model: modelWidget,
                answer : new Answers(),
                parent: dummyParent
            });
            spyOn(widgetVas, 'getCanvasWidth').and.returnValue(canvasWidth);
        });

        afterEach(() => {
            widgetVas = null;
            modelWidget = null;
        });

        describe('--tests after render--', () => {
            Async.beforeEach(() => {

                // Put spies for render here.
                spyOn(widgetVas, 'moveUnitCursor').and.callThrough();

                return widgetVas.drawTheVas();
            });

            it('should have unit pixel size of 2', function () {
                expect(widgetVas.vasLine.getUnitPixelSize()).toEqual(2);
            });

            it('should have first unit pixel size of 1', function () {

                expect(widgetVas.vasLine.getFirstUnitPixelSize()).toEqual(1);

            });

            it('should have vas line width of 200', function () {

                expect(widgetVas.vasLine.width).toEqual(200);

            });

            it('should have vas line height of 3', function () {

                expect(widgetVas.vasLine.height).toEqual(3);

            });

            it('should have anchor height of 37', function () {

                expect(widgetVas.anchors.height).toEqual(37);

            });

            it('should have anchor width of 3', function () {

                expect(widgetVas.anchors.width).toEqual(3);

            });

            it('should have anchor center pixel of 2', function () {

                expect(widgetVas.anchors.getCenterPixel()).toEqual(2);

            });

            it('should have anchor extension of 17', function () {

                expect(widgetVas.anchors.getExtension()).toEqual(17);

            });

            it('should have unit cursor height of 33', function () {

                expect(widgetVas.strikeMark.height).toEqual(33);

            });

            it('should have unit cursor width of 5', function () {

                expect(widgetVas.strikeMark.width).toEqual(5);

            });

            it('should have unit cursor center pixel of 3', function () {

                expect(widgetVas.strikeMark.getCenterPixel()).toEqual(3);

            });

            it('should have unit cursor extension of 15', function () {

                expect(widgetVas.strikeMark.getExtension()).toEqual(15);

            });

            it('should have  margin of 31', function () {

                expect(widgetVas.vasPosition.getMargin()).toEqual(31);

            });

            it('should have vas line starting at pixel x axis of 33', function () {

                expect(widgetVas.vasPosition.getLineStartX()).toEqual(33);

            });

            it('should have vas line starting at pixel y axis of 17', function () {

                expect(widgetVas.vasPosition.getLineStartY()).toEqual(17);

            });

            it('should have vas line ending at pixel x axis of 232', function () {

                expect(widgetVas.vasPosition.getLineEndX()).toEqual(232);

            });

            it('should have unit cursor starting at pixel y axis of 2', function () {

                expect(widgetVas.vasPosition.getCursorStartY()).toEqual(2);

            });

            it('should have left anchor starting at pixel x axis of 32', function () {

                expect(widgetVas.vasPosition.getLeftAnchorX()).toEqual(32);

            });

            it('should have left anchor starting at pixel y axis of 0', function () {

                expect(widgetVas.vasPosition.getLeftAnchorY()).toEqual(0);

            });

            it('should have right anchor starting at pixel x axis of 231', function () {

                expect(widgetVas.vasPosition.getRightAnchorX()).toEqual(231);

            });

            it('should have right anchor starting at pixel y axis of 0', function () {

                expect(widgetVas.vasPosition.getRightAnchorY()).toEqual(0);

            });

            it('should have pointer height of 40', function () {

                expect(widgetVas.pointer.height).toEqual(40);

            });

            it('should have pointer width of 20', function () {

                expect(widgetVas.pointer.width).toEqual(20);

            });

            it('should have numeric value of 0 for the first pixel of vas line', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineStartX())).toEqual(0);

            });

            it('should have numeric value of 100 for the last pixel of vas line', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineEndX())).toEqual(100);

            });

            it('should have numeric value of 50 for the middle pixel of vas line', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineStartX() + (widgetVas.vasLine.width / 2))).toEqual(50);

            });

            it('should have numeric value 0 for the point that has numeric value 0', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(0))).toEqual(0);

            });

            it('should have numeric value 1 for the point that has numeric value 1', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(1))).toEqual(1);

            });

            it('should have numeric value 50 for the point that has numeric value 50', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(50))).toEqual(50);

            });

            it('should have numeric value 61 for the point that has numeric value 61', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(61))).toEqual(61);

            });

            it('should have numeric value 99 for the point that has numeric value 99', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(99))).toEqual(99);

            });

            it('should have numeric value 100 for the point that has numeric value 100', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(100))).toEqual(100);

            });
        });
    });

    describe('with default study configurations and canvas width 655px', function () {
        let modelDefault,
            widgetVas,
            canvasWidth;

        beforeEach(() => {
            canvasWidth = 655;
            modelDefault = new Widget({
                        id      : 'VAS_DIARY_W_6',
                        type    : 'HorizontalVAS',
                        anchors: {
                            min: {
                                text: 'NO_PAIN',
                                value: '0'
                            },
                            max: {
                                text: 'EXTREME_PAIN_LONG',
                                value: '100'
                            }
                        }
                    });

            widgetVas = new HorizontalVAS({
                model   : modelDefault,
                answers : new Answers(),
                parent  : dummyParent
            });
            spyOn(widgetVas, 'getCanvasWidth').and.returnValue(canvasWidth);
        });

        afterEach(() => {
            widgetVas = null;
            modelDefault = null;
        });

        describe('--tests after render--', () => {
            Async.beforeEach(() => {
                return widgetVas.drawTheVas();
            });

            it('should have default label position \'below\'', function () {

                expect(widgetVas.vasConfig.labelPosition).toEqual('below');

            });

            it('should have default arrow value \'true\'', function () {

                expect(widgetVas.vasConfig.labelArrow).toEqual(true);

            });

            it('should have screenEdge justification as default', function () {

                expect(widgetVas.vasConfig.labelJustified).toEqual('screenEdge');

            });

            it('should have default posting value \'false\'', function () {

                expect(widgetVas.vasConfig.postValue).toEqual(false);

            });

            it('should have default pointer value \'false\'', function () {

                expect(widgetVas.vasConfig.pointer).toEqual(false);

            });

            it('should have default initial cursor display value \'undefined\'', function () {

                expect(widgetVas.model.get('initialCursorDisplay')).toEqual(undefined);

            });

            it('should create canvas', function () {
                expect(widgetVas.vasCanvas.height).toBe(100);
                expect(widgetVas.arrowsCanvas.height).toBe(60);
                expect(widgetVas.unitCursorCanvas.height).toBe(100);

                expect(widgetVas.ctxNumLabels).toBeDefined();
                expect(widgetVas.ctxVas).toBeDefined();
                expect(widgetVas.ctxArrows).toBeDefined();
                expect(widgetVas.ctxUnitCursor).toBeDefined();

                expect($(widgetVas.unitCursorCanvas).hasClass('vas-pointer')).toBeTruthy();

            });

            it('should build html layout', function () {
                expect(widgetVas.$el.children().first().get(0).id).toBe('numLabelCanvas');
                expect(widgetVas.$el.children().last().hasClass('vas-label')).toBeTruthy();
            });

            it('should set canvas pixels', function () {

                let canvasWidth = 655;

                widgetVas.setCanvasPixels(canvasWidth);

                expect($(widgetVas.numLabelCanvas).height()).toBe(widgetVas.numLabelCanvas.height);
                expect($(widgetVas.arrowsCanvas).height()).toBe(widgetVas.pointer.height * 1.5);
                expect($(widgetVas.vasCanvas).height()).toBe(widgetVas.vasCanvas.height);
                expect($(widgetVas.numLabelCanvas).width()).toBe(canvasWidth);
                expect($(widgetVas.vasCanvas).width()).toBe(canvasWidth);
                expect($(widgetVas.arrowsCanvas).width()).toBe(canvasWidth);

            });

            it('should set the position of text labels', () => {
                expect(widgetVas.$('.vas-label').hasClass('vas-label-screenEdge')).toBeTruthy();
            });

            it('should draw arrows', () => {

                spyOn(widgetVas, 'renderArrows');

                widgetVas.drawArrows();

                expect(widgetVas.renderArrows).toHaveBeenCalled();
                expect(widgetVas.renderArrows.calls.count()).toEqual(1);

            });

            it('should render numeric labels', function () {

                widgetVas.renderNumericLabels();

                expect(widgetVas.ctxNumLabels.textAlign).toBe('center');

            });

            it('should have unit pixel size of 6', function () {

                expect(widgetVas.vasLine.getUnitPixelSize()).toEqual(6);

            });

            it('should have first unit pixel size of 3', function () {

                expect(widgetVas.vasLine.getFirstUnitPixelSize()).toEqual(3);

            });

            it('should have vas line width of 600', function () {

                expect(widgetVas.vasLine.width).toEqual(600);

            });

            it('should have vas line height of 5', function () {

                expect(widgetVas.vasLine.height).toEqual(5);

            });

            it('should have anchor height of 37', function () {

                expect(widgetVas.anchors.height).toEqual(37);

            });

            it('should have anchor width of 5', function () {

                expect(widgetVas.anchors.width).toEqual(5);

            });

            it('should have anchor center pixel of 3', function () {

                expect(widgetVas.anchors.getCenterPixel()).toEqual(3);

            });

            it('should have anchor extension of 16', function () {

                expect(widgetVas.anchors.getExtension()).toEqual(16);

            });

            it('should have unit cursor height of 33', function () {

                expect(widgetVas.strikeMark.height).toEqual(33);

            });

            it('should have unit cursor width of 7', function () {

                expect(widgetVas.strikeMark.width).toEqual(7);

            });

            it('should have unit cursor center pixel of 4', function () {

                expect(widgetVas.strikeMark.getCenterPixel()).toEqual(4);

            });

            it('should have unit cursor extension of 14', function () {

                expect(widgetVas.strikeMark.getExtension()).toEqual(14);

            });

            it('should have  margin of 24', function () {

                expect(widgetVas.vasPosition.getMargin()).toEqual(24);

            });

            it('should have vas line starting at pixel x axis of 27', function () {

                expect(widgetVas.vasPosition.getLineStartX()).toEqual(27);

            });

            it('should have vas line starting at pixel y axis of 16', function () {

                expect(widgetVas.vasPosition.getLineStartY()).toEqual(16);

            });

            it('should have vas line ending at pixel x axis of 626', function () {

                expect(widgetVas.vasPosition.getLineEndX()).toEqual(626);

            });

            it('should have unit cursor starting at pixel y axis of 2', function () {

                expect(widgetVas.vasPosition.getCursorStartY()).toEqual(2);

            });

            it('should have left anchor starting at pixel x axis of 25', function () {

                expect(widgetVas.vasPosition.getLeftAnchorX()).toEqual(25);

            });

            it('should have left anchor starting at pixel y axis of 0', function () {

                expect(widgetVas.vasPosition.getLeftAnchorY()).toEqual(0);

            });

            it('should have right anchor starting at pixel x axis of 624', function () {

                expect(widgetVas.vasPosition.getRightAnchorX()).toEqual(624);

            });

            it('should have right anchor starting at pixel y axis of 0', function () {

                expect(widgetVas.vasPosition.getRightAnchorY()).toEqual(0);

            });

            it('should have pointer height of 40', function () {

                expect(widgetVas.pointer.height).toEqual(40);

            });

            it('should have pointer width of 28', function () {

                expect(widgetVas.pointer.width).toEqual(28);

            });

            it('should have numeric value of 0 for the first pixel of vas line', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineStartX())).toEqual(0);

            });

            it('should have numeric value of 100 for the last pixel of vas line', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineEndX())).toEqual(100);

            });

            it('should have numeric value of 50 for the middle pixel of vas line', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineStartX() + (widgetVas.vasLine.width / 2))).toEqual(50);

            });

            it('should have numeric value 0 for the point that has numeric value 0', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(0))).toEqual(0);

            });

            it('should have numeric value 1 for the point that has numeric value 1', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(1))).toEqual(1);

            });

            it('should have numeric value 48 for the point that has numeric value 48', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(48))).toEqual(48);

            });

            it('should have numeric value 17 for the point that has numeric value 17', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(17))).toEqual(17);

            });

            it('should have numeric value 99 for the point that has numeric value 99', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(99))).toEqual(99);

            });

            it('should have numeric value 100 for the point that has numeric value 100', function () {

                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(100))).toEqual(100);

            });

            it('should set cursor point', function () {

                expect(widgetVas.vasPosition.setCursorStartX(100)).toEqual(97);

            });
        });
    });

    describe('with center justified labels below was line and canvas width 106px', function () {
        let modelWidget,
            widgetVas;

        beforeEach(() => {
            let canvasWidth = 106;
            modelWidget = new Widget({
                        id                      : 'VAS_DIARY_W_6',
                        type                    : 'HorizontalVAS',
                        posting                 : false,
                        pointer: {
                            isVisible: true
                        },
                        initialCursorDisplay    : false,
                        anchors: {
                            min: {
                                text: 'NO_PAIN',
                                value: '0'
                            },
                            max: {
                                text: 'EXTREME_PAIN_LONG',
                                value: '100'
                            }
                        },
                        label: {
                            position    : 'below',
                            arrow       : true,
                            justified   : 'center'
                        }
                    });

            widgetVas = new HorizontalVAS({
                model   : modelWidget,
                answers : new Answers(),
                parent  : dummyParent
            });

            spyOn(widgetVas, 'getCanvasWidth').and.returnValue(canvasWidth);
        });

        afterEach(() => {
            widgetVas = null;
            modelWidget = null;
        });

        describe('--after render tests--', () => {
            Async.beforeEach(() => {
                return widgetVas.drawTheVas();
            });

            it('should render widget', function () {
                expect(widgetVas.vasLine).toBeDefined();
                expect(widgetVas.anchors).toBeDefined();
                expect(widgetVas.strikeMark).toBeDefined();
                expect(widgetVas.vasPosition).toBeDefined();
                expect(widgetVas.pointer).toBeDefined();
            });

            it('should have anchor ending at pixel y axis of 37', function () {

                expect(widgetVas.vasPosition.getAnchorEndY()).toEqual(37);

            });

            it('should have cursor ending at pixel y axis of 35', function () {

                expect(widgetVas.vasPosition.getCursorEndY()).toEqual(35);

            });

            it('should have values 0..100 on the vas line', function () {

                let value = 0,
                    point = widgetVas.vasPosition.getLineStartX();

                while (point <= widgetVas.vasPosition.getLineEndX()) {
                    expect(widgetVas.getVASNumericValue(point)).toEqual(value);
                    point += 1;
                    value += 1;
                }
            });

            it('should draw arrows', function () {

                spyOn(widgetVas, 'renderArrows');

                widgetVas.drawArrows();

                expect(widgetVas.renderArrows).toHaveBeenCalled();
                expect(widgetVas.renderArrows.calls.count()).toEqual(1);

            });

            it('should render pointer', function () {

                spyOn(widgetVas.ctxUnitCursor, 'fill');

                widgetVas.renderPointer();

                expect(widgetVas.ctxUnitCursor.fill.calls.count()).toEqual(1);

            });

            it('should move unit cursor using vmouseHandler', function () {

                widgetVas.vmouseHandler({
                    preventDefault: $.noop,
                    type            : 'mousedown',
                    pageX           : 1 + widgetVas.vasCanvas.offsetLeft,
                    pageY           : 5 + widgetVas.vasCanvas.offsetTop

                });

                expect(widgetVas.answer.get('response')).toEqual('0');

                widgetVas.vmouseHandler({
                    preventDefault: $.noop,
                    type            : 'mouseup'
                });

                expect(widgetVas.unitCursorMove).toBeFalsy();
                expect(widgetVas.answer.get('response')).toEqual('0');

                widgetVas.vmouseHandler({
                    preventDefault: $.noop,
                    type            : 'mousemove',
                    pageX           : 20 + widgetVas.vasCanvas.offsetLeft
                });

                expect(widgetVas.unitCursorMove).toBeFalsy();
                expect(widgetVas.answer.get('response')).toEqual('0');

                widgetVas.vmouseHandler({
                    preventDefault: $.noop,
                    type            : 'mousedown',
                    pageX           : 164 + widgetVas.vasCanvas.offsetLeft,
                    pageY           : 5 + widgetVas.vasCanvas.offsetTop
                });

                widgetVas.vmouseHandler({
                    preventDefault: $.noop,
                    type            : 'mousemove',
                    pageX           : 106 + widgetVas.vasCanvas.offsetLeft,
                    pageY           : 5 + widgetVas.vasCanvas.offsetTop
                });

                expect(widgetVas.unitCursorMove).toBeTruthy();
                expect(widgetVas.answer.get('response')).toEqual('100');

                widgetVas.vmouseHandler({
                    preventDefault: $.noop,
                    type            : 'mousedown'
                });

                expect(widgetVas.unitCursorMove).toBeFalsy();
            });

            it('should draw the initial unit cursor based on the last vas response', function () {

                widgetVas.vmouseHandler({
                    preventDefault: $.noop,
                    type            : 'mousedown',
                    pageX           : 164 + widgetVas.vasCanvas.offsetLeft,
                    pageY           : 5 + widgetVas.vasCanvas.offsetTop
                });

                widgetVas.vmouseHandler({
                    preventDefault: $.noop,
                    type            : 'mousemove',
                    pageX           : 106 + widgetVas.vasCanvas.offsetLeft,
                    pageY           : 5 + widgetVas.vasCanvas.offsetTop
                });

                spyOn(widgetVas, 'moveUnitCursor');

                widgetVas.drawInitialUnitCursor();

                expect(widgetVas.moveUnitCursor).toHaveBeenCalledWith(widgetVas.getCursorPoint(100), false, true);

            });
        });
    });

    describe('with default study configurations in a RTL language', function () {

        let modelDefault,
            language,
            pref;

        beforeEach(() => {
            modelDefault = new Widget({
                id      : 'VAS_DIARY_W_6',
                type    : 'HorizontalVAS',
                anchors: {
                    min: {
                        text: 'NO_PAIN',
                        value: '0'
                    },
                    max: {
                        text: 'EXTREME_PAIN_LONG',
                        value: '100'
                    }
                }
            });

            language = new Language({
                namespace   : 'CORE',
                language    : 'ar',
                locale      : 'EG',
                direction   : 'rtl',
                resources   : {}
            });

            pref = {
                lang: LF.Preferred.language,
                loc: LF.Preferred.locale
            };
        });

        describe('with reverseOnRtl set to true', function () {
            let widgetVas;

            Async.beforeEach(() => {
                let canvasWidth = 655;

                modelDefault.set('reverseOnRtl', true);

                LF.strings.add(language);
                LF.Preferred = {
                    language: 'ar',
                    locale: 'EG'
                };

                widgetVas = new HorizontalVAS({
                    model: modelDefault,
                    answers: new Answers(),
                    parent: dummyParent
                });

                spyOn(widgetVas, 'getCanvasWidth').and.returnValue(canvasWidth);

                // Render VAS
                return widgetVas.drawTheVas();
            });

            afterEach(() => {
                widgetVas = null;
                modelDefault = null;
            });

            it('should have numeric value of 0 for the first pixel of vas line', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineStartX())).toEqual(0);
            });

            it('should have numeric value of 100 for the last pixel of vas line', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineEndX())).toEqual(100);
            });

            it('should have numeric value of 50 for the middle pixel of vas line', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineStartX() +
                    (widgetVas.vasLine.width / 2))).toEqual(50);
            });

            it('should have numeric value 0 for the point that has numeric value 0', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(0))).toEqual(0);
            });

            it('should have numeric value 1 for the point that has numeric value 1', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(1))).toEqual(1);
            });

            it('should have numeric value 48 for the point that has numeric value 48', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(48))).toEqual(48);
            });

            it('should have numeric value 17 for the point that has numeric value 17', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(17))).toEqual(17);
            });

            it('should have numeric value 99 for the point that has numeric value 99', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(99))).toEqual(99);
            });

            it('should have numeric value 100 for the point that has numeric value 100', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(100))).toEqual(100);
            });

            it('should remove RTL language and reset LF.Preferred', function () {
                LF.strings.remove(language);

                LF.Preferred = {
                    language: pref.lang,
                    locale: pref.loc
                };
            });
        });

        describe('with reverseOnRtl not set (default: true)', function () {
            let widgetVas;

            Async.beforeEach(() => {
                let canvasWidth = 655;

                LF.strings.add(language);
                LF.Preferred = {
                    language: 'ar',
                    locale: 'EG'
                };
                widgetVas = new HorizontalVAS({
                    model: modelDefault,
                    answers: new Answers(),
                    parent: dummyParent
                });

                spyOn(widgetVas, 'getCanvasWidth').and.returnValue(canvasWidth);

                // Render VAS
                return widgetVas.drawTheVas();
            });

            afterEach(() => {
                widgetVas = null;
                modelDefault = null;
            });

            it('should have numeric value of 100 for the first pixel of vas line', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineStartX())).toEqual(100);
            });

            it('should have numeric value of 0 for the last pixel of vas line', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineEndX())).toEqual(0);
            });

            it('should have numeric value of 50 for the middle pixel of vas line', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.vasPosition.getLineStartX() +
                    (widgetVas.vasLine.width / 2))).toEqual(50);
            });

            it('should have numeric value 0 for the point that has numeric value 0', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(0))).toEqual(0);
            });

            it('should have numeric value 52 for the point that has numeric value 52', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(52))).toEqual(52);
            });

            it('should have numeric value 100 for the point that has numeric value 100', function () {
                expect(widgetVas.getVASNumericValue(widgetVas.getCursorPoint(100))).toEqual(100);
            });
        });

    });

});
