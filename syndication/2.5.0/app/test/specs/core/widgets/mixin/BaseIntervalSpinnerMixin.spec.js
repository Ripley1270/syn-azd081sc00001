/* eslint-disable max-lines */
import Templates from 'core/resources/Templates';
import { mix } from 'core/utilities/languageExtensions';
import ValueSpinnerInput from 'core/widgets/input/ValueSpinnerInput';

import BaseIntervalSpinnerMixin, { FlowOptions } from 'core/widgets/mixin/BaseIntervalSpinnerMixin';

const { Model, View } = Backbone,
    defaultInputTemplate = 'DEFAULT:NumberSpinnerControl',
    defaultSpinnerItemTemplate = 'DEFAULT:NumberItemTemplate',
    defaultClassName = 'DateSpinner',
    defaultStorageLocale = 'en-US',
    defaultNumberOfSpinnerItems = 2.5,
    defaultDateFormat = 'LL',
    defaultTimeFormat = 'LT',
    defaultDateTimeFormat = 'LLL',
    defaultDateYearRange = 30,

    // Date where each date part has a unique value.
    //  This is used to generate a test date that can be displayed, reordered for RTL,
    //  and then re-populated with the date pattern parts (e.g. DD, MMM, etc.)
    dateWithUniqueParts = '08 Nov 1970 23:24',

    // for minImplementation of abstract class, just use DateSpinner settings
    defaultModalTemplate = 'DEFAULT:DateSpinnerModal',
    defaultDisplayFormat = 'LL',
    defaultStorageFormat = 'DD MMM YYYY';

/* global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
class BaseIntervalSpinnerMixinTests {
    /**
     * {Widget} get the widget model.
     */
    get model () {
        return new Model({
            id: 'SPINNER_DIARY_W_1',
            type: 'DateSpinner',
            className: 'SPINNER_DIARY_W_1',
            label: 'PICK_A_NUMBER'
        });
    }

    /**
     * Min implementation of the mixin class (i.e. DateSpinner).
     * @param {class} superclass superclass with which to mix
     * @returns {class} mixin extending superclass
     */
    minImplementationMixin (superclass) {
        return class extends superclass {
            getValueForModal () {
                return '';
            }

            get defaultDisplayFormat () {
                return defaultDisplayFormat;
            }

            get defaultStorageFormat () {
                return defaultStorageFormat;
            }

            get defaultModalTemplate () {
                return defaultModalTemplate;
            }

            validateUI () {
                return true;
            }
        };
    }

    execTests () {
        TRACE_MATRIX('US7038')
        .describe('BaseIntervalSpinnerMixin', () => {
            let MinMixinClass = mix(View).with(BaseIntervalSpinnerMixin),
                FullMixinClass = mix(View).with(BaseIntervalSpinnerMixin, this.minImplementationMixin),
                mixin,
                temp;

            beforeEach(() => {
                temp = null;
            });

            afterEach(() => {
                if (mixin) {
                    mixin.destroy();
                    mixin.$el.remove();
                    mixin = null;
                }
            });

            describe('#getters and abstract functions', () => {
                let testValue,
                    baseSpinner;

                beforeEach(() => {
                    let customModel = _.extend({}, this.model),
                        displayFormat = 'LL',
                        displayLocale = 'hi-IN',
                        storageFormat = 'DD MMM YYYY',
                        storageLocale = 'en-US';

                    testValue = new Date('12/19/2016');
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        displayFormat,
                        storageFormat,
                        storageLocale
                    });

                    mixin = new MinMixinClass({
                        model: customModel
                    });
                    temp = null;
                });

                describe('#defaultModalTemplate', () => {
                    it('throws an error when called from the base class', () => {
                        expect(() => {
                            temp = mixin.defaultModalTemplate;
                        }).toThrow(new Error('Invalid Interval Spinner Implementation.  A Modal Template is required'));
                    });
                });
                describe('#defaultDisplayFormat', () => {
                    it('throws an error when called from the base class', () => {
                        expect(() => {
                            temp = mixin.defaultDisplayFormat;
                        }).toThrow(new Error('Invalid Interval Spinner Implementation.  A Display Format is required'));
                    });
                });
                describe('#defaultStorageFormat', () => {
                    it('throws an error when called from the base class', () => {
                        expect(() => {
                            temp = mixin.defaultStorageFormat;
                        }).toThrow(new Error('Invalid Interval Spinner Implementation.  A Storage Format is required'));
                    });
                });
                describe('#defaultDateformat', () => {
                    it('defaults to correct value', () => {
                        expect(mixin.defaultDateFormat).toBe(defaultDateFormat);
                    });
                });
                describe('#defaultTimeFormat', () => {
                    it('defaults to correct value', () => {
                        expect(mixin.defaultTimeFormat).toBe(defaultTimeFormat);
                    });
                });
                describe('#defaultDateTimeFormat', () => {
                    it('defaults to correct value', () => {
                        expect(mixin.defaultDateTimeFormat).toBe(defaultDateTimeFormat);
                    });
                });
                describe('#defaultStorageLocale', () => {
                    it('defaults to correct value', () => {
                        expect(mixin.defaultStorageLocale).toBe(defaultStorageLocale);
                    });
                });
                describe('#defaultInputTemplate', () => {
                    it('defaults to correct value', () => {
                        expect(mixin.defaultInputTemplate).toBe(defaultInputTemplate);
                    });
                });
                describe('#defaultClassName', () => {
                    it('defaults to correct value', () => {
                        expect(mixin.defaultClassName).toBe(defaultClassName);
                    });
                });
                describe('#defaultSpinnerItemTemplate', () => {
                    it('defaults to correct value', () => {
                        expect(mixin.defaultSpinnerItemTemplate).toBe(defaultSpinnerItemTemplate);
                    });
                });
                describe('#defaultYear', () => {
                    it('defaults to correct value', () => {
                        expect(mixin.defaultYear)
                            .toBe(mixin.model.get('initialDate').getFullYear());
                    });
                });
                describe('#defaultMonth', () => {
                    it('defaults to correct value', () => {
                        expect(mixin.defaultMonth)
                            .toBe(mixin.model.get('initialDate').getMonth());
                    });
                });
                describe('#defaultDay', () => {
                    it('defaults to correct value', () => {
                        expect(mixin.defaultDay)
                            .toBe(mixin.model.get('initialDate').getDate());
                    });
                });
                describe('#defaultHour', () => {
                    it('defaults to correct value', () => {
                        expect(mixin.defaultHour)
                            .toBe(mixin.model.get('initialDate').getHours());
                    });
                });
                describe('#defaultMinute', () => {
                    it('defaults to correct value', () => {
                        expect(mixin.defaultMinute)
                            .toBe(mixin.model.get('initialDate').getMinutes());
                    });
                });
                describe('#defaultInitialDate', () => {
                    it('defaults to correct value', () => {
                        let compTime = (new Date()).getTime(),
                            widgetTime = mixin.defaultInitialDate.getTime();

                        // should never be a whole second off from current date
                        expect(widgetTime).toBeGreaterThan(compTime - 1000);
                        expect(widgetTime).toBeLessThan(compTime + 1000);
                    });
                });
                describe('#defaultMinDate', () => {
                    it('defaults to correct value', () => {
                        let compTime = (new Date()).setFullYear((new Date()).getFullYear() - defaultDateYearRange),
                            widgetTime = mixin.defaultMinDate.getTime();

                        // should never be a whole second off from current date
                        expect(widgetTime).toBeGreaterThan(compTime - 1000);
                        expect(widgetTime).toBeLessThan(compTime + 1000);
                    });
                });
                describe('#defaultMaxDate', () => {
                    it('defaults to correct value', () => {
                        let compTime = (new Date()).setFullYear((new Date()).getFullYear() + defaultDateYearRange),
                            widgetTime = mixin.defaultMaxDate.getTime();

                        // should never be a whole second off from current date
                        expect(widgetTime).toBeGreaterThan(compTime - 1000);
                        expect(widgetTime).toBeLessThan(compTime + 1000);
                    });
                });
                describe('#year', () => {
                    it('defaults to defaultYear', () => {
                        expect(mixin.year).toBe(mixin.defaultYear);
                    });
                    it('overrides with the value of the spinner at the correct index', () => {
                        // mock up the spinners array and yearIndex index.
                        mixin.inputs = [{ value: 1950 }];
                        mixin.yearIndex = 0;

                        expect(mixin.year).toBe(1950);
                    });
                });
                describe('#month', () => {
                    it('defaults to defaultMonth', () => {
                        expect(mixin.month).toBe(mixin.defaultMonth);
                    });
                    it('overrides with the value of the spinner at the correct index', () => {
                        // mock up the spinners array and yearIndex index.
                        mixin.inputs = [{ value: 10 }];
                        mixin.monthIndex = 0;

                        // 9, because months are 0-indexed in the actual date object.
                        expect(mixin.month).toBe(9);
                    });
                });
                describe('#day', () => {
                    it('defaults to defaultDay', () => {
                        expect(mixin.day).toBe(mixin.defaultDay);
                    });
                    it('overrides with the value of the spinner at the correct index', () => {
                        // mock up the spinners array and yearIndex index.
                        mixin.inputs = [{ value: 4 }];
                        mixin.dayIndex = 0;

                        expect(mixin.day).toBe(4);
                    });
                });
                describe('#hour', () => {
                    it('defaults to defaultHour', () => {
                        expect(mixin.hour).toBe(mixin.defaultHour);
                    });
                    it('overrides with the value of the spinner at the correct index', () => {
                        // mock up the spinners array and yearIndex index.
                        mixin.inputs = [{ value: 8 }];
                        mixin.hourIndex = 0;

                        expect(mixin.hour).toBe(8);
                    });
                });
                describe('#minute', () => {
                    it('defaults to defaultMinute', () => {
                        expect(mixin.minute).toBe(mixin.defaultMinute);
                    });
                    it('overrides with the value of the spinner at the correct index', () => {
                        // mock up the spinners array and yearIndex index.
                        mixin.inputs = [{ value: 30 }];
                        mixin.minuteIndex = 0;

                        expect(mixin.minute).toBe(30);
                    });
                });

                describe('#inputEvaluateOrder', () => {
                    it('places indexes into an array, ordered from year to minute', () => {
                        mixin.yearIndex = 5;
                        mixin.monthIndex = 4;
                        mixin.dayIndex = 3;
                        mixin.meridianIndicatorIndex = 1;
                        mixin.hourIndex = 0;
                        mixin.minuteIndex = 2;

                        expect(mixin.inputEvaluateOrder).toEqual([5, 4, 3, 1, 0, 2]);
                    });

                    it('skips null indexes (returning empty array if all are null)', () => {
                        mixin.yearIndex = null;
                        mixin.monthIndex = null;
                        mixin.dayIndex = null;
                        mixin.meridianIndicatorIndex = null;
                        mixin.hourIndex = null;
                        mixin.minuteIndex = null;

                        expect(mixin.inputEvaluateOrder).toEqual([]);
                    });
                });

                describe('#getDatePartByIndex', () => {
                    beforeEach(() => {
                        mixin.yearIndex = 5;
                        mixin.monthIndex = 4;
                        mixin.dayIndex = 3;
                        mixin.meridianIndicatorIndex = 1;
                        mixin.hourIndex = 0;
                        mixin.minuteIndex = 2;
                    });

                    it('gets all datepart strings for corresponding indexes', () => {
                        expect(mixin.getDatePartByIndex(5)).toBe('year');
                        expect(mixin.getDatePartByIndex(4)).toBe('month');
                        expect(mixin.getDatePartByIndex(3)).toBe('day');
                        expect(mixin.getDatePartByIndex(1)).toBe('meridianIndicator');
                        expect(mixin.getDatePartByIndex(0)).toBe('hour');
                        expect(mixin.getDatePartByIndex(2)).toBe('minute');
                    });

                    it('returns null for non-existent index', () => {
                        expect(mixin.getDatePartByIndex(6)).toBeNull();
                    });
                });

                describe('#constructedDate', () => {
                    it('gets a new date object with the value of _constructedDate', () => {
                        mixin._constructedDate = new Date('01 Jan 2018 17:00');
                        expect(mixin.constructedDate.getTime()).toEqual(mixin._constructedDate.getTime());
                    });

                    it('changes will not modify value of _constructedDate', () => {
                        mixin._constructedDate = new Date('01 Jan 2018 17:00');
                        let testDate = mixin.constructedDate,
                            originalTime = mixin._constructedDate.getTime();
                        testDate.setHours(15);
                        expect(mixin._constructedDate.getTime()).toBe(originalTime);
                    });
                });

                describe('#validateUI', () => {
                    it('throws an error when called from the base class', () => {
                        expect(() => {
                            temp = mixin.validateUI();
                        }).toThrow(new Error('IntervalSpinnerBase.validateUI() must be overridden by subclass'));
                    });
                });

                describe('#modalValue', () => {
                    it('returns the date in the storage format and locale', () => {
                        spyOn(mixin, 'getDate').and.returnValue(new Date('19 Mar 2018 10:38'));
                        expect(mixin.modalValue).toBe('19 Mar 2018');
                    });
                });
            });

            describe('#constructor', () => {
                it('creates object of type IntervalSpinnerBase, with everything initialized', () => {
                    mixin = new FullMixinClass({
                        model: this.model
                    });

                    expect(mixin.yearIndex).toBe(null);
                    expect(mixin.monthIndex).toBe(null);
                    expect(mixin.dayIndex).toBe(null);
                    expect(mixin.hourIndex).toBe(null);
                    expect(mixin.minuteIndex).toBe(null);
                    expect(mixin.meridianIndicatorIndex).toBe(null);

                    expect(mixin.model.get('initialDate')).toEqual(mixin.defaultInitialDate);
                    expect(mixin.model.get('minDate')).toEqual(mixin.defaultMinDate);
                    expect(mixin.model.get('maxDate')).toEqual(mixin.defaultMaxDate);

                    expect(mixin.model.get('displayFormat')).toBe(mixin.defaultDisplayFormat);
                    expect(mixin.model.get('storageFormat')).toBe(mixin.defaultStorageFormat);
                    expect(mixin.model.get('storageLocale')).toBe(mixin.defaultStorageLocale);

                    expect(mixin.model.get('dateFormat')).toBe(mixin.defaultDateFormat);
                    expect(mixin.model.get('timeFormat')).toBe(mixin.defaultTimeFormat);
                    expect(mixin.model.get('dateTimeFormat')).toBe(mixin.defaultDateTimeFormat);

                    expect(mixin.model.get('flow')).toBe(FlowOptions.calculate);

                    expect(mixin.moment instanceof moment).toBe(true);
                    expect(mixin.moment.locale()).toBe(moment().locale('en-US').locale());
                });

                it('accepts model parameters.', () => {
                    let customModel = _.extend({}, this.model),
                        options = {
                            initialDate: new Date('10/04/16'),
                            minDate: new Date('10/04/06'),
                            maxDate: new Date('10/04/25'),
                            displayFormat: 'DD/MMM/YYYY',
                            storageFormat: 'YYYY~DD~MM',
                            storageLocale: 'fr-CA',
                            dateFormat: 'MMMM DD YYYY',
                            timeFormat: 'HH:mm a',
                            dateTimeFormat: 'MMMM DD YYYY HH:MM a',
                            flow: 'ltr'
                        };
                    customModel.attributes = _.extend({}, this.model.attributes, options);
                    mixin = new FullMixinClass({
                        model: customModel
                    });

                    expect(mixin.model.get('initialDate')).toEqual(options.initialDate);
                    expect(mixin.model.get('minDate')).toEqual(options.minDate);
                    expect(mixin.model.get('maxDate')).toEqual(options.maxDate);

                    expect(mixin.model.get('displayFormat')).toBe(options.displayFormat);
                    expect(mixin.model.get('storageFormat')).toBe(options.storageFormat);
                    expect(mixin.model.get('storageLocale')).toBe(options.storageLocale);

                    expect(mixin.model.get('dateFormat')).toBe(options.dateFormat);
                    expect(mixin.model.get('timeFormat')).toBe(options.timeFormat);
                    expect(mixin.model.get('dateTimeFormat')).toBe(options.dateTimeFormat);

                    expect(mixin.model.get('flow')).toBe(FlowOptions[options.flow.toLowerCase()]);
                });
            });

            describe('post-rendered tests', () => {
                Async.beforeEach(() => {
                    mixin = new FullMixinClass({
                        model: this.model
                    });

                    return mixin.renderModal({});
                });

                describe('#displayValueToStorageFormat', () => {
                    it('returns value in storage format', () => {
                        mixin.model.set('displayFormat', 'LL');
                        mixin.moment.locale('fr-FR');
                        mixin.model.set('storageFormat', 'YYYY/MM/DD');
                        mixin.model.set('storageLocale', 'en-US');

                        expect(mixin.displayValueToStorageFormat('19 mars 2018')).toBe('2018/03/19');
                    });

                    it('forwards blank value', () => {
                        expect(mixin.displayValueToStorageFormat('')).toBe('');
                    });
                });

                describe('#storageValueToDisplayFormat', () => {
                    it('returns value in displayFormat format', () => {
                        mixin.model.set('displayFormat', 'LL');
                        mixin.moment.locale('fr-FR');
                        mixin.model.set('storageFormat', 'YYYY/MM/DD');
                        mixin.model.set('storageLocale', 'en-US');

                        expect(mixin.storageValueToDisplayFormat('2018/03/19')).toBe('19 mars 2018');
                    });

                    it('returns blank for blank, null, or undefined', () => {
                        expect(mixin.storageValueToDisplayFormat('')).toBe('');
                        expect(mixin.storageValueToDisplayFormat(null)).toBe('');
                        expect(mixin.storageValueToDisplayFormat(undefined)).toBe('');
                    });
                });

                describe('#getInputValuesArray', () => {
                    it('returns array with indexes populated with correct values', () => {
                        let valArray = mixin.getInputValuesArray();

                        // verify we actually have values, and the loop will be entered.
                        expect(valArray.length > 0).toBe(true);

                        for (let i = 0; i < valArray.length; ++i) {
                            let compVal,
                                defaultVal = mixin.model.get('initialDate') || '',
                                val;

                            val = mixin.getValueForModal();
                            if (val === '' || val === undefined) {
                                val = moment(defaultVal).locale(mixin.moment.locale()).format(mixin.model.get('displayFormat'));
                            }
                            val = moment(val, mixin.model.get('displayFormat'), mixin.moment.locale()).toDate();
                            switch (i) {
                                case mixin.yearIndex:
                                    compVal = val.getFullYear();
                                    break;
                                case mixin.monthIndex:
                                    // + 1 because months are zero indexed
                                    compVal = val.getMonth() + 1;
                                    break;
                                case mixin.dayIndex:
                                    compVal = val.getDate();
                                    break;
                                case mixin.hourIndex:
                                    compVal = val.getHours();
                                    if (typeof mixin.meridianIndicatorIndex === 'number') {
                                        compVal = compVal % 12;
                                    }
                                    break;
                                case mixin.meridianIndicatorIndex:
                                    if (val.getHours() >= 12) {
                                        compVal = 1;
                                    } else {
                                        valArray[this.meridianIndicatorIndex] = 0;
                                    }
                                    break;
                                case mixin.minuteIndex:
                                    compVal = val.getMinutes();
                                    break;
                                default:
                                    compVal = null;
                            }

                            if (compVal !== null) {
                                expect(compVal).toBe(valArray[i]);
                            }
                        }
                    });
                });

                describe('#setMeridianDisplayValues()', () => {
                    Async.beforeEach(() => {
                        let customModel = this.model;
                        customModel.set('dateFormat', 'LLL');
                        mixin.destroy();
                        mixin.$el.remove();
                        mixin = new FullMixinClass({
                            model: customModel
                        });
                        return mixin.renderModal({})
                        .then(() => {
                            return mixin.openDialog();
                        }).then(() => {
                            // duplicating some work that is done by render, but show() and setting values
                            //  are thread safe so this is a good way to listen to the promise
                            return mixin.refreshInputs();
                        });
                    });

                    it('sets meridian indicators to correspond with the current time.', () => {
                        // verify initial state of meridian indicators
                        let $meridianContainer = $(mixin.inputs[mixin.meridianIndicatorIndex].parent);

                        expect($meridianContainer.find('.item:eq(1)').html()).toEqual('AM');
                        expect($meridianContainer.find('.item:eq(2)').html()).toEqual('PM');

                        // Test Chinese meridian indicators reacting to time
                        mixin.moment.locale('zh-CN');

                        spyOn(mixin, 'getDate').and.returnValue(moment('01 Jan 1970 5:00'));
                        mixin.setMeridianDisplayValues();
                        expect($meridianContainer.find('.item:eq(1)').html()).toEqual('凌晨');
                        expect($meridianContainer.find('.item:eq(2)').html()).toEqual('下午');

                        mixin.getDate.and.returnValue(moment('01 Jan 1970 8:00'));
                        mixin.setMeridianDisplayValues();
                        expect($meridianContainer.find('.item:eq(1)').html()).toEqual('早上');
                        expect($meridianContainer.find('.item:eq(2)').html()).toEqual('晚上');

                        mixin.getDate.and.returnValue(moment('01 Jan 1970 23:30'));
                        mixin.setMeridianDisplayValues();
                        expect($meridianContainer.find('.item:eq(1)').html()).toEqual('中午');
                        expect($meridianContainer.find('.item:eq(2)').html()).toEqual('晚上');
                    });
                });

                describe('#testAllValuesForIndex', () => {
                    it('returns true for year index, false for others', () => {
                        mixin.yearIndex = 2;
                        expect(mixin.testAllValuesForIndex(2)).toBe(true);
                        expect(mixin.testAllValuesForIndex(3)).toBe(false);
                        expect(mixin.testAllValuesForIndex(1)).toBe(false);
                    });
                });

                describe('#testValue', () => {
                    it('calls testDate', () => {
                        spyOn(mixin, 'getDatePartByIndex').and.returnValue('year');
                        spyOn(mixin, 'testDate');

                        mixin.testValue(2, 2014);

                        expect(mixin.testDate).toHaveBeenCalledWith('year', 2014);
                    });
                });

                describe('#testDate', () => {
                    Async.it('returns true or false on the proper side of minDate', () => {
                        mixin.model.set('minDate', new Date('10/25/16 8:30 PM'));
                        mixin.model.set('maxDate', new Date('10/30/17 8:30 PM'));
                        spyOn(mixin, 'getValueForModal').and.returnValue('25 Oct 2016 22:00');

                        return mixin.openDialog().then(() => {
                            if (typeof mixin.yearIndex === 'number') {
                                expect(mixin.testDate('year', 2016)).toBe(false, 'year before min');
                                expect(mixin.testDate('year', 2017)).toBe(true, 'year after min');
                            }
                        }).then(() => {
                            if (typeof mixin.monthIndex === 'number') {
                                expect(mixin.testDate('month', 9)).toBe(false, 'month before min');
                                expect(mixin.testDate('month', 10)).toBe(true, 'month after min');
                            }
                        }).then(() => {
                            if (typeof mixin.dayIndex === 'number') {
                                expect(mixin.testDate('day', 24)).toBe(false, 'day before min');
                                expect(mixin.testDate('day', 25)).toBe(true, 'day after min');
                            }
                        }).then(() => {
                            if (mixin.hourIndex) {
                                expect(mixin.testDate('hour', 21)).toBe(false, 'hour before min');
                                expect(mixin.testDate('hour', 22)).toBe(true, 'hour after min');
                            }
                        }).then(() => {
                            if (mixin.minuteIndex) {
                                expect(mixin.testDate('minute', 29)).toBe(false, 'minute before min');
                                expect(mixin.testDate('minute', 30)).toBe(true, 'minute after min');
                            }
                        });
                    });
                    Async.it('returns true or false on the proper side of maxDate', () => {
                        mixin.model.set('minDate', new Date('10/25/16 8:30 PM'));
                        mixin.model.set('maxDate', new Date('10/30/17 8:30 PM'));
                        spyOn(mixin, 'getValueForModal').and.returnValue('30 Oct 2017 22:00');

                        return mixin.openDialog().then(() => {
                            if (typeof mixin.yearIndex === 'number') {
                                expect(mixin.testDate('year', 2017)).toBe(true, 'year before max');
                                expect(mixin.testDate('year', 2018)).toBe(false, 'year after max');
                            }
                        }).then(() => {
                            if (typeof mixin.monthIndex === 'number') {
                                expect(mixin.testDate('month', 10)).toBe(true, 'month before max');
                                expect(mixin.testDate('month', 11)).toBe(false, 'month after max');
                            }
                        }).then(() => {
                            if (typeof mixin.dayIndex === 'number') {
                                expect(mixin.testDate('day', 30)).toBe(true, 'day before max');
                                expect(mixin.testDate('day', 31)).toBe(false, 'day after max');
                            }
                        }).then(() => {
                            if (mixin.hourIndex) {
                                expect(mixin.testDate('hour', 22)).toBe(true, 'hour before max');
                                expect(mixin.testDate('hour', 23)).toBe(false, 'hour after max');
                            }
                        }).then(() => {
                            if (mixin.minuteIndex) {
                                expect(mixin.testDate('minute', 30)).toBe(true, 'minute before max');
                                expect(mixin.testDate('minute', 31)).toBe(false, 'minute after max');
                            }
                        });
                    });
                });

                describe('#getDate()', () => {
                    Async.it('returns a date with the custom property altered', () => {
                        mixin.model.set('minDate', new Date('10/25/16 8:30 PM'));
                        mixin.model.set('maxDate', new Date('10/30/17 8:30 PM'));
                        spyOn(mixin, 'getValueForModal').and.returnValue('15 May 2017 22:10');

                        return mixin.openDialog().then(() => {
                            if (typeof mixin.yearIndex === 'number') {
                                expect(mixin.getDate().getFullYear()).toBe(2017, 'year, default');
                                expect(mixin.getDate('year', 2018).getFullYear()).toBe(2018, 'year, custom');
                            }

                            if (typeof mixin.monthIndex === 'number') {
                                expect(mixin.getDate().getMonth() + 1).toBe(5, 'month, default');
                                expect(mixin.getDate('month', 9).getMonth() + 1).toBe(9, 'month, custom');
                            }

                            if (typeof mixin.dayIndex === 'number') {
                                expect(mixin.getDate().getDate()).toBe(15, 'day, default');
                                expect(mixin.getDate('day', 20).getDate()).toBe(20, 'day, custom');
                            }

                            if (typeof mixin.hourIndex === 'number') {
                                expect(mixin.getDate().getHours()).toBe(22, 'hour, default');
                                expect(mixin.getDate('hour', 18).getHours()).toBe(18, 'hour, custom');
                            }

                            if (typeof mixin.hourIndex === 'number') {
                                expect(mixin.getDate().getMinutes()).toBe(10, 'minute, default');
                                expect(mixin.getDate('minute', 40).getMinutes()).toBe(40, 'minute, custom');
                            }
                        });
                    });
                });

                describe('#getYearItemsArray', () => {
                    it('loops on years in a locale and pushes them into an array', () => {
                        let locale = 'hi-IN',
                            format = 'YYYY';
                        mixin.model.set('minDate', new Date('10/25/16 8:30 PM'));
                        mixin.model.set('maxDate', new Date('10/30/25 8:30 PM'));
                        mixin.moment.locale('hi-IN');

                        let values = mixin.getYearItemsArray(format),
                            curVal = new Date(mixin.model.get('minDate')).getFullYear();
                        expect(values.length).toBe(10);

                        for (let i = 0; i < values.length; ++i) {
                            let expectedVal = moment(`01 Jan ${curVal}`, 'DD MMM YYYY', 'en-US');
                            expectedVal.locale(locale);
                            expect(values[i].value).toBe(curVal);
                            expect(values[i].text).toBe(expectedVal.format(format));
                            ++curVal;
                        }
                    });
                });

                describe('#getMonthItemsArray', () => {
                    it('loops on months in a locale and pushes them into an array', () => {
                        let locale = 'hi-IN',
                            format = 'MMM';
                        mixin.moment.locale('hi-IN');

                        let values = mixin.getMonthItemsArray(format),
                            curVal = 1,
                            expectedVal = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
                        expect(values.length).toBe(12);

                        expectedVal.locale(locale);
                        for (let i = 0; i < values.length; ++i) {
                            expect(values[i].value).toBe(curVal);
                            expect(values[i].text).toBe(expectedVal.format(format));
                            ++curVal;
                            expectedVal.add(1, 'M');
                        }
                    });
                });

                describe('#getDayItemsArray', () => {
                    it('loops on days in a locale and pushes them into an array', () => {
                        let locale = 'hi-IN',
                            format = 'MMM';
                        mixin.moment.locale('hi-IN');

                        let values = mixin.getDayItemsArray(format),
                            curVal = 1,
                            expectedVal = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
                        expect(values.length).toBe(31);

                        expectedVal.locale(locale);
                        for (let i = 0; i < values.length; ++i) {
                            expect(values[i].value).toBe(curVal);
                            expect(values[i].text).toBe(expectedVal.format(format));
                            ++curVal;
                            expectedVal.add(1, 'd');
                        }
                    });
                });

                describe('#getHourItemsArray', () => {
                    it('loops on days in a locale and pushes them into an array', () => {
                        let locale = 'hi-IN',
                            format = 'HH';
                        mixin.moment.locale('hi-IN');

                        let values = mixin.getHourItemsArray(format),
                            curVal = 0,
                            expectedVal = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
                        expect(values.length).toBe(24);

                        expectedVal.locale(locale);
                        for (let i = 0; i < values.length; ++i) {
                            expect(values[i].value).toBe(curVal);
                            expect(values[i].text).toBe(expectedVal.format(format));
                            ++curVal;
                            expectedVal.add(1, 'h');
                        }
                    });
                });

                describe('#getMinuteItemsArray', () => {
                    it('loops on days in a locale and pushes them into an array', () => {
                        let locale = 'hi-IN',
                            format = 'HH';
                        mixin.moment.locale('hi-IN');

                        let values = mixin.getMinuteItemsArray(format),
                            curVal = 0,
                            expectedVal = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
                        expect(values.length).toBe(60);

                        expectedVal.locale(locale);
                        for (let i = 0; i < values.length; ++i) {
                            expect(values[i].value).toBe(curVal);
                            expect(values[i].text).toBe(expectedVal.format(format));
                            ++curVal;
                            expectedVal.add(1, 'm');
                        }
                    });
                });

                describe('#getMeridiemItemsArray', () => {
                    it('returns an array of length 2, with blank text values (placeholders)', () => {
                        let values = mixin.getMeridiemItemsArray();
                        expect(values.length).toBe(2);
                        expect(values[0].value).toBe(0);
                        expect(values[0].text).toBe('');
                        expect(values[1].value).toBe(1);
                        expect(values[1].text).toBe('');
                    });
                });

                describe('setInputLimits()', () => {
                    Async.it('calls setMeridianDisplayValues', () => {
                        spyOn(mixin, 'setMeridianDisplayValues');
                        return mixin.openDialog().then(() => {
                            return mixin.setInputLimits();
                        }).then(() => {
                            expect(mixin.setMeridianDisplayValues).toHaveBeenCalled();
                        });
                    });
                });

                describe('#parseDateParts', () => {
                    let parseTestCases = [];
                    parseTestCases.push({
                        locale: 'en-US',
                        format: 'DMY',
                        value: ['D', 'M', 'Y']
                    });
                    parseTestCases.push({
                        locale: 'en-US',
                        format: 'Do MMMM YYYYY',
                        value: ['Do', ' ', 'MMMM', ' ', 'YYYYY']
                    });
                    parseTestCases.push({
                        locale: 'zh-CN',
                        format: 'LT',
                        value: ['A', 'h', '点', 'mm', '分']
                    });
                    parseTestCases.push({
                        locale: 'hi',
                        format: 'LLL',
                        value: [
                            'D', ' ', 'MMMM', ' ', 'YYYY', ', ',
                            'A', ' ', 'h', ':', 'mm', ' बजे']
                    });
                    _.each(parseTestCases, (testCase) => {
                        let locale = testCase.locale || 'en-US';
                        it(`correctly parses "${testCase.format}" value in locale ${locale}`, () => {
                            mixin.moment.locale(locale);
                            expect(mixin.parseDateParts(testCase.format)).toEqual(testCase.value);
                        });
                    });
                });

                TRACE_MATRIX('DE22389')
                .describe('#datePartsWithDocumentFlow', () => {
                    it('returns original dateparts if LTR', () => {
                        let testDateParts = mixin.parseDateParts('LL');
                        spyOn(LF.strings, 'getLanguageDirection').and.returnValue('ltr');
                        mixin.model.set('flow', FlowOptions.calculate);

                        let dateParts = mixin.datePartsWithDocumentFlow(testDateParts);

                        expect(dateParts).toEqual(testDateParts);
                    });

                    it('returns original dateparts if flowOptions is not calculate', () => {
                        let testDateParts = mixin.parseDateParts('LL');
                        spyOn(LF.strings, 'getLanguageDirection').and.returnValue('rtl');
                        mixin.model.set('flow', FlowOptions.ltr);

                        let dateParts = mixin.datePartsWithDocumentFlow(testDateParts);

                        expect(dateParts).toEqual(testDateParts);
                    });

                    it('returns reverse of all date parts in reverse order (expected display value of toStrictRTL in en-US)', () => {
                        spyOn(LF.strings, 'getLanguageDirection').and.returnValue('rtl');
                        mixin.model.set('flow', FlowOptions.calculate);

                        let testDateParts = mixin.parseDateParts('LL'),
                            dateParts = mixin.datePartsWithDocumentFlow(testDateParts),
                            expectedDateParts = [];
                        for (let i = testDateParts.length - 1; i >= 0; --i) {
                            let part = testDateParts[i],
                                testFormat = mixin.moment.format(part);

                            if (testFormat === part) {
                                // not a format for date parsing... so reverse the string
                                part = part.split('').reverse().join('');
                            }
                            expectedDateParts.push(part);
                        }

                        expect(dateParts).toEqual(expectedDateParts);
                    });

                    it('Correctly returns reversed dateparts for D/M/YYYY format (formerly replaced part of the year, due to DE22389', () => {
                        spyOn(LF.strings, 'getLanguageDirection').and.returnValue('rtl');
                        mixin.model.set('flow', FlowOptions.calculate);

                        let testDateParts = mixin.parseDateParts('D/M/YYYY'),
                            dateParts = mixin.datePartsWithDocumentFlow(testDateParts),
                            expectedDateParts = [];
                        for (let i = testDateParts.length - 1; i >= 0; --i) {
                            let part = testDateParts[i],
                                testFormat = mixin.moment.format(part);

                            if (testFormat === part) {
                                // not a format for date parsing... so reverse the string
                                part = part.split('').reverse().join('');
                            }
                            expectedDateParts.push(part);
                        }

                        expect(dateParts).toEqual(expectedDateParts);
                    });
                });

                describe('#buildDynamicTemplate', () => {
                    it('creates containers for parsed date parts and separators', () => {
                        spyOn(mixin, 'parseDateParts').and.returnValue([
                            'MMM', 'DD', 'YY', ' ', 'H', ':', 'mm', ' ~~ ', 'A'
                        ]);
                        let $dateContainer = mixin.$modal.find('.date-container, .time-container, .date-time-container').first();
                        $dateContainer.empty();
                        mixin.buildDynamicTemplate();
                        let $dateParts = $dateContainer.find('div');

                        let i = 0;
                        expect($($dateParts.get(i)).attr('class')).toBe('month-spinner-container');
                        expect($($dateParts.get(i)).data('format')).toBe('MMM');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('day-spinner-container');
                        expect($($dateParts.get(i)).data('format')).toBe('DD');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('year-spinner-container');
                        expect($($dateParts.get(i)).data('format')).toBe('YY');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('separator');
                        expect($($dateParts.get(i)).html()).toBe('&nbsp;');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('hour-spinner-container');
                        expect($($dateParts.get(i)).data('format')).toBe('H');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('separator');
                        expect($($dateParts.get(i)).html()).toBe(':');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('minute-spinner-container');
                        expect($($dateParts.get(i)).data('format')).toBe('mm');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('separator');
                        expect($($dateParts.get(i)).html()).toBe('&nbsp;~~&nbsp;');

                        i += 1;
                        expect($($dateParts.get(i)).attr('class')).toBe('meridian-indicator-spinner-container');
                        expect($($dateParts.get(i)).data('format')).toBe('A');
                    });
                });

                describe('#setModalFlow', () => {
                    beforeEach(() => {
                        // Remove any styles that were already on the modal.
                        mixin.$modal[0].classList.remove('flow-calculate');
                        mixin.$modal[0].classList.remove('flow-natural');
                        mixin.$modal[0].classList.remove('flow-ltr');
                        mixin.$modal[0].classList.remove('flow-rtl');
                    });
                    it('sets flow-calculate class', () => {
                        let expectedClass = 'flow-calculate';
                        mixin.model.set('flow', FlowOptions.calculate);
                        mixin.setModalFlow();
                        expect(mixin.$modal.find('.modal-horizontal-container').hasClass(expectedClass)).toBe(true,
                        `Expect modal to have ${expectedClass} class set`);
                    });
                    it('sets flow-natural class', () => {
                        let expectedClass = 'flow-natural';
                        mixin.model.set('flow', FlowOptions.natural);
                        mixin.setModalFlow();
                        expect(mixin.$modal.find('.modal-horizontal-container').hasClass(expectedClass)).toBe(true,
                        `Expect modal to have ${expectedClass} class set`);
                    });
                    it('sets flow-ltr class', () => {
                        let expectedClass = 'flow-ltr';
                        mixin.model.set('flow', FlowOptions.ltr);
                        mixin.setModalFlow();
                        expect(mixin.$modal.find('.modal-horizontal-container').hasClass(expectedClass)).toBe(true,
                        `Expect modal to have ${expectedClass} class set`);
                    });
                    it('sets flow-rtl class', () => {
                        let expectedClass = 'flow-rtl';
                        mixin.model.set('flow', FlowOptions.rtl);
                        mixin.setModalFlow();
                        expect(mixin.$modal.find('.modal-horizontal-container').hasClass(expectedClass)).toBe(true,
                        `Expect modal to have ${expectedClass} class set`);
                    });
                });

                describe('#injectModalInputs', () => {
                    it('creates a ValueSpinnerInput for each spinner and injects them into the modal dialog', () => {
                        spyOn(mixin, 'setModalFlow').and.callThrough();
                        mixin.injectModalInputs({});
                        let valueCnt = 0,
                            $datePart;

                        $datePart = mixin.$modal.find('.year-spinner-container');
                        if ($datePart.length > 0) {
                            valueCnt += 1;
                        }

                        $datePart = mixin.$modal.find('.month-spinner-container');
                        if ($datePart.length > 0) {
                            valueCnt += 1;
                        }

                        $datePart = mixin.$modal.find('.day-spinner-container');
                        if ($datePart.length > 0) {
                            valueCnt += 1;
                        }

                        $datePart = mixin.$modal.find('.hour-spinner-container');
                        if ($datePart.length > 0) {
                            valueCnt += 1;
                        }

                        $datePart = mixin.$modal.find('.minute-spinner-container');
                        if ($datePart.length > 0) {
                            valueCnt += 1;
                        }

                        $datePart = mixin.$modal.find('.meridian-indicator-spinner-container');
                        if ($datePart.length > 0) {
                            valueCnt += 1;
                        }

                        expect(mixin.inputs.length).toBe(valueCnt);

                        for (let i = 0; i < mixin.inputs.length; ++i) {
                            expect(mixin.inputs[i] instanceof ValueSpinnerInput).toBe(true);
                        }

                        expect(mixin.setModalFlow).toHaveBeenCalled();
                    });
                });
            });
        });
    }
}

export default BaseIntervalSpinnerMixinTests;
new BaseIntervalSpinnerMixinTests().execTests();
