let { Model, View } = Backbone;
import Templates from 'core/resources/Templates';
import { mix } from 'core/utilities/languageExtensions';
import NumberSpinnerMixin from 'core/widgets/mixin/NumberSpinnerMixin';

import NumberSpinnerInput from 'core/widgets/input/NumberSpinnerInput';

const defaultModalTemplate = 'DEFAULT:NumberSpinnerModal',
    defaultInputTemplate = 'DEFAULT:NumberSpinnerControl',
    defaultSpinnerItemTemplate = 'DEFAULT:NumberItemTemplate',
    defaultClassName = 'NumberSpinner';

export default class NumberSpinnerMixinTests {
    /**
     * {Model} get the model for this mixin.
     */
    get model () {
        return new Model({
            id: 'SPINNER_DIARY_W_1',
            type: 'NumberSpinner',
            className: 'SPINNER_DIARY_W_1',
            label: 'PICK_A_NUMBER',
            inputOptions: [
                {
                    min: 0,
                    max: 500
                }
            ]
        });
    }

    get decimalModel () {
        return new Model({
            id: 'SPINNER_DIARY_W_2',
            type: 'NumberSpinner',
            className: 'SPINNER_DIARY_W_2',
            label: 'PICK_A_NUMBER',
            min: 0.11,
            max: 100,
            templates: {
                modal: 'DecimalSpinnerModal'
            },
            okButtonText: 'MODAL_OK_TEXT',
            defaultVal: '3.00',
            inputOptions: [
                {
                    min: 0,
                    max: 101
                },
                {
                    min: 0,
                    max: 0.99,
                    step: 0.05,
                    precision: 2
                }
            ]
        });
    }

    minImplementationMixin (superclass) {
        return class extends superclass {
            getValueForModal () {
                return this.model.get('defaultVal');
            }
        };
    }

    execTests () {
        TRACE_MATRIX('US6106')
        .TRACE_MATRIX('US7326')
        .TRACE_MATRIX('US7038')
        .describe('NumberSpinnerMixin', () => {
            let MinMixinClass = mix(View).with(NumberSpinnerMixin),
                FullMixinClass = mix(View).with(NumberSpinnerMixin, this.minImplementationMixin),
                mixin,
                temp;

            beforeEach(() => {
                temp = null;
            });

            afterEach(() => {
                if (mixin) {
                    mixin.destroy();
                    mixin.$el.remove();
                    mixin = null;
                }
            });

            describe('Getters', () => {
                beforeEach(() => {
                    mixin = new MinMixinClass({
                        model: this.model
                    });
                });

                it('#defaultModalTemplate', () => {
                    expect(mixin.defaultModalTemplate).toEqual(defaultModalTemplate);
                });

                it('#defaultInputTemplate', () => {
                    expect(mixin.defaultInputTemplate).toEqual(defaultInputTemplate);
                });

                it('#defaultSpinnerItemTemplate', () => {
                    expect(mixin.defaultSpinnerItemTemplate).toEqual(defaultSpinnerItemTemplate);
                });

                it('#defaultClassName', () => {
                    expect(mixin.defaultClassName).toEqual(defaultClassName);
                });

                it('#modalValue', () => {
                    spyOn(mixin, 'getNumericValue').and.returnValue(4);
                    expect(mixin.modalValue).toBe('4');
                });
            });

            TRACE_MATRIX('DE22219')
            .describe('Value functions', () => {
                Async.it('#getInputValuesArray() with value', () => {
                    let customModel = _.extend({}, this.model);
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        defaultVal: '4'
                    });

                    mixin = new FullMixinClass({
                        model: customModel
                    });

                    return mixin.renderModal({})
                        .tap(() => {
                            expect(mixin.getInputValuesArray()[0]).toEqual('4');
                        });
                });

                Async.it('#getNumericValue() with value', () => {
                    let customModel = _.extend({}, this.model);
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        defaultVal: '4'
                    });

                    mixin = new FullMixinClass({
                        model: customModel
                    });

                    return mixin.renderModal({})
                        .then(() => {
                            return mixin.openDialog();
                        })
                        .then(() => {
                            // duplicating some work that is done by render, but show() and setting values
                            //  are thread safe so this is a good way to listen to the promise
                            return mixin.refreshInputs();
                        })
                        .then(() => {
                            _.each(mixin.inputs, (spinner) => {
                                spinner.stop();
                                spinner.pushValue();
                            });
                            return mixin.getNumericValue();
                        }).tap((value) => {
                            expect(value).toBe(4);
                        });
                });

                Async.it('#getNumericValue() overrides value with customValues array', () => {
                    let customModel = _.extend({}, this.model);
                    customModel.attributes = _.extend({}, this.model.attributes, {
                        defaultVal: '4'
                    });

                    mixin = new FullMixinClass({
                        model: customModel
                    });

                    return mixin.renderModal({})
                        .then(() => {
                            return mixin.openDialog();
                        })
                        .then(() => {
                            // duplicating some work that is done by render, but show() and setting values
                            //  are thread safe so this is a good way to listen to the promise
                            return mixin.refreshInputs();
                        })
                        .then(() => {
                            _.each(mixin.inputs, (spinner) => {
                                spinner.stop();
                                spinner.pushValue();
                            });
                            return mixin.getNumericValue([{ index: 0, value: 7 }]);
                        }).tap((value) => {
                            expect(value).toBe(7);
                        });
                });

                Async.it('#injectModalInputs() places spinners inside modal template', () => {
                    mixin = new FullMixinClass({
                        model: this.model
                    });

                    return mixin.renderModal({})
                        .then(() => {
                            return mixin.openDialog();
                        })
                        .then(() => {
                            // duplicating some work that is done by render, but show() and setting values
                            //  are thread safe so this is a good way to listen to the promise
                            return mixin.refreshInputs();
                        })
                        .tap(() => {
                            let $containers = mixin.$modal.find('.number-spinner-container');
                            expect($containers.length).toBe(1);

                            $containers.each((index, element) => {
                                expect(mixin.inputs[index] instanceof NumberSpinnerInput).toBe(true);
                                expect($(element).children().length).toBe(1);
                            });
                        });
                });
            });

            TRACE_MATRIX('DE22219')
            .describe('min/max validation', () => {
                Async.beforeEach(() => {
                    mixin = new FullMixinClass({
                        model: this.decimalModel
                    });

                    return mixin.renderModal({})
                        .then(() => {
                            // disable automatic change listening, since we are testing the change handlers themselves.
                            mixin.stopListening(mixin.inputs[0].model);
                            mixin.stopListening(mixin.inputs[1].model);
                            return mixin.openDialog();
                        })
                        .then(() => {
                            // duplicating some work that is done by render, but show() and setting values
                            //  are thread safe so this is a good way to listen to the promise
                            return mixin.refreshInputs();
                        });
                });

                describe('#testValue', () => {
                    it('returns true for whole number containing part of range (max)', () => {
                        expect(mixin.testValue(0, 100)).toBe(true);
                    });
                    it('returns false for whole number containing no part of range (max)', () => {
                        expect(mixin.testValue(0, 101)).toBe(false);
                    });
                    it('returns true for whole number containing part of range (min)', () => {
                        expect(mixin.testValue(0, 0)).toBe(true);
                    });
                    it('returns true if whole number contains entirety of range', () => {
                        mixin.model.set('min', 75.25);
                        mixin.model.set('max', 75.75);
                        expect(mixin.testValue(0, 75)).toBe(true);
                    });
                    it('returns true for decimal above min of range', () => {
                        mixin.inputs[0].setValue(0);
                        expect(mixin.testValue(1, 0.15)).toBe(true);
                    });
                    it('returns false for decimal below min of range', () => {
                        mixin.inputs[0].setValue(0);
                        expect(mixin.testValue(1, 0.10)).toBe(false);
                    });
                    it('returns true for decimal below max of range', () => {
                        mixin.inputs[0].setValue(100);
                        expect(mixin.testValue(1, 0)).toBe(true);
                    });
                    it('returns false for decimal above max of range', () => {
                        mixin.inputs[0].setValue(100);
                        expect(mixin.testValue(1, 0.5)).toBe(false);
                    });
                });

                TRACE_MATRIX('DE22348')
                .describe('#testAllValuesForIndex', () => {
                    it('returns true if min and max both pass testValue', () => {
                        spyOn(mixin, 'testValue').and.callFake((index, value) => {
                            expect(index).toBe(0);
                            switch (value) {
                                case 0:
                                    return true;
                                case 101:
                                    return true;
                                default:
                                    fail(`unexpected value ${value}`);
                                    return false;
                            }
                        });

                        expect(mixin.testAllValuesForIndex(0)).toBe(true);
                    });
                    it('returns false if min returns false for testValue', () => {
                        spyOn(mixin, 'testValue').and.callFake((index, value) => {
                            expect(index).toBe(0);
                            switch (value) {
                                case 0:
                                    return false;
                                case 101:
                                    return true;
                                default:
                                    fail(`unexpected value ${value}`);
                                    return false;
                            }
                        });

                        expect(mixin.testAllValuesForIndex(0)).toBe(false);
                    });
                    it('returns false if max returns false for testValue', () => {
                        spyOn(mixin, 'testValue').and.callFake((index, value) => {
                            expect(index).toBe(0);
                            switch (value) {
                                case 0:
                                    return true;
                                case 101:
                                    return false;
                                default:
                                    fail(`unexpected value ${value}`);
                                    return false;
                            }
                        });

                        expect(mixin.testAllValuesForIndex(0)).toBe(false);
                    });
                });
            });
        });
    }
}

new NumberSpinnerMixinTests().execTests();
