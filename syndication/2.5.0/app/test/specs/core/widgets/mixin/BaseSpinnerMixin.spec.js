let { Model, View } = Backbone;
import Templates from 'core/resources/Templates';
import { mix } from 'core/utilities/languageExtensions';
import BaseSpinnerMixin from 'core/widgets/mixin/BaseSpinnerMixin';
import NumberSpinnerMixin from 'core/widgets/mixin/NumberSpinnerMixin';

import NumberSpinnerInput from 'core/widgets/input/NumberSpinnerInput';

const defaultClassName = 'Spinner',
    defaultOkButtonText = 'MODAL_OK_TEXT',
    defaultInputTemplate = 'DEFAULT:NumberSpinnerControl',
    defaultModalTemplate = 'DEFAULT:NumberSpinnerModal',
    defaultSpinnerItemTemplate = 'DEFAULT:NumberItemTemplate',
    defaultLabels = { labelOne: '', labelTwo: '' },
    embeddedValueStopDelay = 350;

/* global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
export default class BaseSpinnerMixinTests {
    get model () {
        return new Model({
            id: 'testID',
            inputOptions: {
                template: defaultInputTemplate,
                itemTemplate: defaultSpinnerItemTemplate,
                items: [{
                    text: 'IMAGE_OPTION_0',
                    value: '0'
                }, {
                    text: 'IMAGE_OPTION_1',
                    value: '1'
                }, {
                    text: 'IMAGE_OPTION_2',
                    value: '2'
                }, {
                    text: 'IMAGE_OPTION_3',
                    value: '3'
                }]
            },
            templates: {
                modal: defaultModalTemplate
            }
        });
    }

    get decimalModel () {
        return new Model({
            id: 'SPINNER_DIARY_W_2',
            type: 'NumberSpinner',
            className: 'SPINNER_DIARY_W_2',
            label: 'PICK_A_NUMBER',
            min: 0.11,
            max: 100,
            templates: {
                modal: 'DecimalSpinnerModal'
            },
            okButtonText: 'MODAL_OK_TEXT',
            defaultVal: '3.00',
            inputOptions: [
                {
                    min: 0,
                    max: 101
                },
                {
                    min: 0,
                    max: 0.99,
                    step: 0.05,
                    precision: 2
                }
            ]
        });
    }
    minImplementationMixin (superclass) {
        let n = 0;
        return class extends superclass {
            /**
             * Place inputs into place.  For the NumberSpinner implementation, they go into elements
             * with the CSS class 'number-spinner-container'.
             * @param {Object} strings key value pair of strings to use within control
             */
            injectModalInputs (strings) {
                this.$modal.find('.number-spinner-container').each((i, element) => {
                    let spinnerTemplate = this.getInputTemplate(i),
                        spinnerItemTemplate = this.getSpinnerItemTemplate(i);

                    this.inputs[i] = new NumberSpinnerInput(
                        _.extend({
                            model: new Model(),
                            parent: element,
                            template: spinnerTemplate,
                            itemTemplate: spinnerItemTemplate,
                            showButtons: this.model.get('showButtons') || false,
                            buttonLayout: this.model.get('buttonLayout') || 'vertical',
                            strings: this.getStringsForInputIndex(strings, i)
                        })
                    );
                });
            }

            get defaultSpinnerItemTemplate () {
                return defaultSpinnerItemTemplate;
            }

            get defaultInputTemplate () {
                return defaultInputTemplate;
            }

            getInputValuesArray () {
                return [this.getValueForModal()];
            }

            getValueForModal () {
                return 41;
            }

            get modalValue () {
                if (this.model.get('test__dynamicValue')) {
                    return ++n;
                }
                return 41;
            }

            propagateModalValue () {
                return Q();
            }
        };
    }

    minNumberSpinnerImplementationMixin (superclass) {
        return class extends superclass {
            getValueForModal () {
                return undefined;
            }
        };
    }

    execTests () {
        TRACE_MATRIX('US6106')
        .TRACE_MATRIX('US7038')
        .TRACE_MATRIX('US7326')
        .describe('BaseSpinnerMixin', () => {
            let MinMixinClass = mix(View).with(BaseSpinnerMixin),
                FullMixinClass = mix(View).with(BaseSpinnerMixin, this.minImplementationMixin),
                NumberSpinnerMixinClass = mix(View).with(NumberSpinnerMixin, this.minNumberSpinnerImplementationMixin),
                mixin,
                temp;

            beforeEach(() => {
                temp = null;
            });

            afterEach(() => {
                if (mixin) {
                    mixin.destroy();
                    mixin.$el.remove();
                    mixin = null;
                }
            });

            describe('#getters', () => {
                beforeEach(() => {
                    mixin = new MinMixinClass({
                        model: this.model
                    });
                });

                describe('#defaultSpinnerItemTemplate', () => {
                    it('Throws an error if unimplemented by instance', () => {
                        expect(() => {
                            temp = mixin.defaultSpinnerItemTemplate;
                        }).toThrow(new Error('Unimplemented defaultSpinnerItemTemplate getter in a BaseSpinner implementation.'));
                    });
                });

                describe('#inputEvaluateOrder', () => {
                    it('Returns input array indexes in order (by default)', () => {
                        // quick temp array with 4 keys
                        mixin.inputs = [1, 2, 3, 4];
                        expect(mixin.inputEvaluateOrder).toEqual([0, 1, 2, 3]);
                    });
                });

                describe('#defaultClassName', () => {
                    it('returns correct default value', () => {
                        expect(mixin.defaultClassName).toBe(defaultClassName);
                    });
                });

                describe('#defaultLabels', () => {
                    it('returns correct default value', () => {
                        expect(mixin.defaultLabels).toEqual(defaultLabels);
                    });
                });
            });

            describe('post-rendered methods', () => {
                Async.beforeEach(() => {
                    mixin = new FullMixinClass({
                        model: this.model
                    });

                    return mixin.renderModal({});
                });

                describe('#modalValueChanged', () => {
                    Async.it('calls setInputLimits before continuing with handler', () => {
                        spyOn(mixin, 'setInputLimits').and.resolve();
                        return mixin.modalValueChanged()
                        .then(() => {
                            expect(mixin.setInputLimits).toHaveBeenCalled();
                        });
                    });
                });

                describe('#getModalValuesString', () => {
                    Async.it('stops spinners (default)', () => {
                        spyOn(mixin, 'stopSpinners');
                        return mixin.getModalValuesString()
                        .then(() => {
                            expect(mixin.stopSpinners).toHaveBeenCalled();
                        });
                    });

                    Async.it('does not stop spinners if called with false', () => {
                        spyOn(mixin, 'stopSpinners');
                        return mixin.getModalValuesString(false)
                        .then(() => {
                            expect(mixin.stopSpinners).not.toHaveBeenCalled();
                        });
                    });

                    Async.it('calls setInputLimits and returns modalValue result', () => {
                        spyOn(mixin, 'setInputLimits').and.resolve();
                        return mixin.getModalValuesString(false)
                        .then((val) => {
                            expect(mixin.setInputLimits).toHaveBeenCalled();
                            expect(val).toBe(41);
                        });
                    });
                });

                describe('#stopSpinners', () => {
                    it('stops each input and pushes the value', () => {
                        expect(mixin.inputs.length > 0).toBe(true);
                        _.each(mixin.inputs, (input) => {
                            spyOn(input, 'stop');
                            spyOn(input, 'pushValue');
                        });
                        mixin.stopSpinners();
                        _.each(mixin.inputs, (input) => {
                            expect(input.stop).toHaveBeenCalled();
                            expect(input.pushValue).toHaveBeenCalled();
                        });
                    });
                });

                TRACE_MATRIX('DE23771')
                .describe('#processValue', () => {
                    Async.it('does no delay if embedded is false', () => {
                        spyOn(Q, 'delay').and.resolve();

                        // modal value that changes each call.
                        mixin.model.set('test__dynamicValue', true);
                        mixin.model.set('embedded', false);

                        return mixin.processValue()
                        .then(() => {
                            expect(Q.delay).not.toHaveBeenCalled();
                        });
                    });

                    Async.it('does no delay if initialValue is same as value after processing', () => {
                        spyOn(Q, 'delay').and.resolve();
                        mixin.model.set('embedded', true);

                        return mixin.processValue()
                        .then(() => {
                            expect(Q.delay).not.toHaveBeenCalled();
                        });
                    });

                    Async.it('delays with constant value if embedded is true and value changed', () => {
                        spyOn(Q, 'delay').and.resolve();

                        // modal value that changes each call.
                        mixin.model.set('test__dynamicValue', true);
                        mixin.model.set('embedded', true);

                        return mixin.processValue()
                        .then(() => {
                            expect(Q.delay).toHaveBeenCalledWith(embeddedValueStopDelay);
                        });
                    });
                });

                describe('#refreshInputs', () => {
                    Async.it('calls setInputLimits', () => {
                        spyOn(mixin, 'setInputLimits').and.resolve();

                        return mixin.refreshInputs()
                        .then(() => {
                            expect(mixin.setInputLimits).toHaveBeenCalled();
                        });
                    });
                });

                describe('#getSpinnerItemTemplate', () => {
                    it('returns spinnerItemTemplates if they exist in model, default if index is out of range', () => {
                        let customModel = _.extend({}, this.model);
                        customModel.attributes = _.extend({}, this.model.attributes, {
                            templates: {
                                spinnerItemTemplates: [
                                    'test1',
                                    'test2'
                                ]
                            }
                        });
                        mixin.model = customModel;
                        expect(mixin.getSpinnerItemTemplate(0)).toBe('test1');
                        expect(mixin.getSpinnerItemTemplate(1)).toBe('test2');
                        expect(mixin.getSpinnerItemTemplate(2)).toBe(mixin.defaultSpinnerItemTemplate);
                    });

                    it('returns default spinner template if no templates defined', () => {
                        let customModel = _.extend({}, this.model);
                        customModel.attributes = _.extend({}, this.model.attributes, {
                            templates: null
                        });
                        mixin.model = customModel;
                        expect(mixin.getSpinnerItemTemplate(0)).toBe(mixin.defaultSpinnerItemTemplate);
                    });
                });

                TRACE_MATRIX('DE22260')
                .TRACE_MATRIX('DE22302')
                .describe('#setInputLimits', () => {
                    Async.beforeEach(() => {
                        return mixin.openDialog()
                        .then(() => {
                            spyOn(mixin, 'adjustInput').and.callThrough();
                        });
                    });

                    Async.it('calls adjustInput for each of our input items', () => {
                        return mixin.setInputLimits()
                        .then(() => {
                            expect(mixin.adjustInput.calls.count()).toBe(1);
                            expect(mixin.adjustInput).toHaveBeenCalledWith(0);
                        });
                    });

                    Async.it('skips if inputLimitsPromise is already a running promise', () => {
                        mixin.inputLimitsPromise = Q();
                        return mixin.setInputLimits()
                        .then(() => {
                            expect(mixin.adjustInput.calls.count()).toBe(0);
                        });
                    });

                    Async.it('skips if inputs length is 0', () => {
                        mixin.inputs = [];
                        return mixin.setInputLimits()
                        .then(() => {
                            expect(mixin.adjustInput.calls.count()).toBe(0);
                        });
                    });

                    Async.it('skips if a spinner is not yet rendered', () => {
                        mixin.inputs[0].isRendered = false;
                        return mixin.setInputLimits()
                        .then(() => {
                            expect(mixin.adjustInput.calls.count()).toBe(0);
                        });
                    });
                });

                describe('#testAllValuesForIndex', () => {
                    it('returns false by default', () => {
                        expect(mixin.testAllValuesForIndex()).toBe(false);
                    });
                });

                describe('#testValue', () => {
                    it('returns true by default', () => {
                        expect(mixin.testValue()).toBe(true);
                    });
                });

                TRACE_MATRIX('DE19538')
                .describe('#adjustInput', () => {
                    Async.beforeEach(() => {
                        // While this method is generic, it is much easier and more thorough to
                        // test with a fully configured number spinner (subclass of this mixin)
                        // with multiple inputs
                        mixin.destroy();
                        mixin.$el.remove();
                        mixin = new NumberSpinnerMixinClass({
                            model: this.decimalModel
                        });

                        return mixin.renderModal({})
                        .then(() => {
                            return mixin.openDialog();
                        });
                    });

                    Async.it('hides the correct number from the whole number portion', () => {
                        return mixin.adjustInput(0)
                        .then(() => {
                            // 0-101, minus the 101st value (out of range), is 101 items.  Verify that is our count.
                            let visibleItemsCount = 0;
                            mixin.inputs[0].$items.each((iNdx, item) => {
                                if ($(item).css('display') !== 'none') {
                                    ++visibleItemsCount;
                                }
                            });
                            expect(visibleItemsCount).toBe(101);
                        });
                    });

                    Async.it('hides the correct number from the decimal number portion (min)', () => {
                        mixin.inputs[0].setValue(0);
                        return mixin.adjustInput(1)
                        .then(() => {
                            // 0.00-0.95 (20 items), minus 0.00, 0.05, and 0.10 (out of range), is 17 items.  Verify that is our count.
                            let visibleItemsCount = 0;
                            mixin.inputs[1].$items.each((iNdx, item) => {
                                if ($(item).css('display') !== 'none') {
                                    ++visibleItemsCount;
                                }
                            });
                            expect(visibleItemsCount).toBe(17);
                        });
                    });

                    Async.it('hides the correct number from the decimal number portion (max)', () => {
                        mixin.inputs[0].setValue(100);
                        return mixin.adjustInput(1)
                        .then(() => {
                            // 0.00 is the only item allowed.  Verify our count is 1.
                            let visibleItemsCount = 0;
                            mixin.inputs[1].$items.each((iNdx, item) => {
                                if ($(item).css('display') !== 'none') {
                                    ++visibleItemsCount;
                                }
                            });
                            expect(visibleItemsCount).toBe(1);
                        });
                    });

                    Async.it('hides the nothing from the decimal number portion (mid-range)', () => {
                        mixin.inputs[0].setValue(50);
                        return mixin.adjustInput(1)
                        .then(() => {
                            // Verify our count is 20.
                            let visibleItemsCount = 0;
                            mixin.inputs[1].$items.each((iNdx, item) => {
                                if ($(item).css('display') !== 'none') {
                                    ++visibleItemsCount;
                                }
                            });
                            expect(visibleItemsCount).toBe(20);
                        });
                    });

                    Async.it('Does not allow decimal value to be blank if whole number is selected', () => {
                        // Clear stuff out so we can redefine our mixin
                        mixin.destroy();
                        mixin.$el.remove();
                        mixin = null;

                        let customModel = this.decimalModel;

                        customModel.unset('defaultVal');

                        mixin = new NumberSpinnerMixinClass({
                            model: customModel
                        });

                        return mixin.renderModal({})
                            .then(() => {
                                // disable automatic change listening, since we are testing the change handlers themselves.
                                mixin.stopListening(mixin.inputs[0].model);
                                mixin.stopListening(mixin.inputs[1].model);
                                return mixin.openDialog();
                            })
                            .then(() => {
                                // duplicating some work that is done by render, but show() and setting values
                                //  are thread safe so this is a good way to listen to the promise
                                return mixin.refreshInputs();
                            }).then(() => {
                                expect(mixin.inputs[1].value).toBe('');
                            }).then(() => {
                                return mixin.inputs[0].setValue(50);
                            }).then(() => {
                                return mixin.adjustInput(1);
                            }).then(() => {
                                expect(mixin.inputs[1].value).toBe(0);
                            });
                    });
                });

                describe('#destroy', () => {
                    Async.it('calls inputLimitsPromise and returns a promise', () => {
                        let promiseDone = false;

                        // very slight delay to ensure this promise isn't complete by a race condition
                        mixin.inputLimitsPromise = Q().delay(10).then(() => {
                            promiseDone = true;
                        });
                        return mixin.destroy(true)
                        .then(() => {
                            expect(promiseDone).toBe(true);
                        });
                    });
                });
            });
        });
    }
}
new BaseSpinnerMixinTests().execTests();
