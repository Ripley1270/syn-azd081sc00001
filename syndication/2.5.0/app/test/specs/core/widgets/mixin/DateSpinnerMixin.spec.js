import Templates from 'core/resources/Templates';
import { mix } from 'core/utilities/languageExtensions';
import DateSpinnerMixin from 'core/widgets/mixin/DateSpinnerMixin';
import ValueSpinnerInput from 'core/widgets/input/ValueSpinnerInput';

const { Model, View } = Backbone;

export default class DateSpinnerMixinTests {
    get testValue1 () {
        return '15 Oct 1995';
    }

    get displayValue1 () {
        return moment(this.testValue1).format('LL');
    }

    get testValue2 () {
        return '29 Nov 2016';
    }

    get displayValue2 () {
        return moment(this.testValue2).format('LL');
    }

    /**
     * {Widget} get the widget model.
     */
    get model () {
        return new Model({
            id: 'SPINNER_DIARY_W_1',
            type: 'DateSpinner',
            className: 'SPINNER_DIARY_W_1',
            label: 'PICK_A_NUMBER'
        });
    }

    execTests () {
        TRACE_MATRIX('US6106')
        .TRACE_MATRIX('US7038')
        .describe('DateSpinnerMixin', () => {
            let MixinClass = mix(View).with(DateSpinnerMixin),
                mixin,
                temp;

            beforeEach(() => {
                temp = null;
            });

            afterEach(() => {
                if (mixin) {
                    mixin.destroy();
                    mixin.$el.remove();
                    mixin = null;
                }
            });

            describe('Default properties', () => {
                beforeEach(() => {
                    mixin = new MixinClass({
                        model: this.model
                    });
                });

                describe('#defaultModalTemplate', () => {
                    it('is defaulted to DEFAULT:DateSpinnerModal', () => {
                        expect(mixin.defaultModalTemplate).toBe('DEFAULT:DateSpinnerModal');
                    });
                });

                describe('#defaultDisplayFormat', () => {
                    it('is defaulted to DEFAULT:DateSpinnerModal', () => {
                        expect(mixin.defaultModalTemplate).toBe('DEFAULT:DateSpinnerModal');
                    });
                });

                describe('#defaultStorageFormat', () => {
                    it('is defaulted to correct format', () => {
                        expect(mixin.defaultStorageFormat).toBe('DD MMM YYYY');
                    });
                });

                describe('#defaultInitialDate', () => {
                    it('is defaulted to floor of current date', () => {
                        let expectedDt = new Date();
                        expectedDt.setHours(0, 0, 0, 0);
                        expect(mixin.defaultInitialDate).toEqual(expectedDt);
                    });
                });
            });

            describe('Instance methods', () => {
                beforeEach(() => {
                    mixin = new MixinClass({
                        model: this.model
                    });
                });

                describe('#validateUI', () => {
                    Async.it('throws an error if no year, month, or day are specified in format', () => {
                        mixin.model.set('dateFormat', 'HH:mm');
                        spyOn(mixin, 'validateUI').and.stub();
                        return mixin.renderModal({}).then(() => {
                            mixin.validateUI.and.callThrough();
                            expect(() => {
                                mixin.validateUI();
                            }).toThrow(new Error('Invalid template for DateSpinner widget. Expected day + month + year containers'));
                        });
                    });

                    Async.it('throws an error if hour or minute are specified in format', () => {
                        mixin.model.set('dateFormat', 'DD MMM YYYY HH:mm');
                        spyOn(mixin, 'validateUI').and.stub();
                        return mixin.renderModal({}).then(() => {
                            mixin.validateUI.and.callThrough();
                            expect(() => {
                                mixin.validateUI();
                            }).toThrow(new Error('Invalid template for DateSpinner widget. Hour and minute containers must not exist. ' +
                                'Use TimeSpinner or DateTimeSpinner if you wish to include these.'));
                        });
                    });

                    Async.it('does not throw an error if only date parts are used (function is void, so returns undefined)', () => {
                        mixin.model.set('dateFormat', 'DD MMM YYYY');
                        spyOn(mixin, 'validateUI').and.stub();
                        return mixin.renderModal({}).then(() => {
                            mixin.validateUI.and.callThrough();
                            expect(mixin.validateUI()).toBeUndefined();
                        });
                    });
                });
            });
        });
    }
}

let tester = new DateSpinnerMixinTests();
tester.execTests();
