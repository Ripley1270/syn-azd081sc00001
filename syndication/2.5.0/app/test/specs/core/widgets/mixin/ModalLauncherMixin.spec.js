let { Model, View } = Backbone;
import Templates from 'core/resources/Templates';
import { mix } from 'core/utilities/languageExtensions';
import ModalLauncherMixin from 'core/widgets/mixin/ModalLauncherMixin';
import MultipleChoiceInput from 'core/widgets/input/MultipleChoiceInput';
const defaultOkButtonText = 'MODAL_OK_TEXT';

const testModalTemplate = 'DEFAULT:MultipleChoiceModal',
    defaultModalTemplate = 'DEFAULT:MultipleChoiceModal',
    defaultInputTemplate = 'DEFAULT:MultipleChoiceInputControl',
    mouseDownEvents = ['MSPointerDown', 'mousedown', 'pointerdown', 'touchstart'],
    mouseUpEvents = ['MSPointerUp', 'mouseup', 'pointerup', 'touchend'];

export default class ModalLauncherMixinTests {
    get model () {
        return new Model({
            id: 'testID',
            inputOptions: {
                template: 'DEFAULT:MultipleChoiceInputControl',
                itemTemplate: 'DEFAULT:ModalMultipleChoiceItem',
                type: 'MultipleChoiceInput',
                items: [{
                    text: 'IMAGE_OPTION_0',
                    value: '0'
                }, {
                    text: 'IMAGE_OPTION_1',
                    value: '1'
                }, {
                    text: 'IMAGE_OPTION_2',
                    value: '2'
                }, {
                    text: 'IMAGE_OPTION_3',
                    value: '3'
                }]
            },
            templates: {
                modal: testModalTemplate
            }
        });
    }

    minImplementationMixin (superclass) {
        return class extends superclass {
            get defaultModalTemplate () {
                return defaultModalTemplate;
            }

            get defaultInputTemplate () {
                return defaultInputTemplate;
            }

            getInputValuesArray () {
                return [this.getValueForModal()];
            }

            getValueForModal () {
                return 41;
            }

            getModalValuesString () {
                return '41';
            }

            propagateModalValue () {
                return Q();
            }

            /**
             * Method to inject modal inputs into the modal dialog.
             * @param {Object} strings key/value pairs of translated strings.
             */
            injectModalInputs (strings) {
                // Ensure inputOptions is an array
                let inputOptions = _.compact(_.flatten([this.model.get('inputOptions')]));

                this.$modal.find('.modal-input-container').each((i, elem) => {
                    let template = this.getInputTemplate(i);
                    this.inputs[i] = new MultipleChoiceInput(
                        _.extend({
                            model: new Model(),
                            parent: elem,
                            callerId: this.model.get('id'),
                            inputIndex: i,
                            strings: this.getStringsForInputIndex(strings, i),
                            template
                        }, inputOptions[i] || {})
                    );
                });
            }
        };
    }

    execTests () {
        TRACE_MATRIX('US7326')
        .TRACE_MATRIX('US7038')
        .describe('ModalLauncherMixin', () => {
            // Implicitly tests languageExtensions/mix as well.
            let MinMixinClass = mix(View).with(ModalLauncherMixin),
                FullMixinClass = mix(View).with(ModalLauncherMixin, this.minImplementationMixin),
                mixin,
                temp;
            beforeEach(() => {
                temp = null;
            });

            afterEach(() => {
                if (mixin) {
                    mixin.destroy();
                    mixin.$el.remove();
                    mixin = null;
                }
            });

            describe('getters and abstract methods', () => {
                beforeEach(() => {
                    mixin = new MinMixinClass({
                        model: this.model
                    });
                });

                describe('#defaultModalTemplate', () => {
                    it('throws error if not defined by subclass', () => {
                        expect(() => {
                            temp = mixin.defaultModalTemplate;
                        }).toThrow(new Error('Unimplemented defaultModalTemplate getter in a ModalLauncher implementation.'));
                    });
                });

                describe('#defaultInputTemplate', () => {
                    it('throws error if not defined by subclass', () => {
                        expect(() => {
                            temp = mixin.defaultInputTemplate;
                        }).toThrow(new Error('Unimplemented defaultInputTemplate getter in a ModalLauncher implementation.'));
                    });
                });

                describe('#defaultOkButtonText', () => {
                    it('returns expected default', () => {
                        expect(mixin.defaultOkButtonText).toBe(defaultOkButtonText);
                    });
                });

                describe('#$modal', () => {
                    it('getters/setters interface with private _$modal', () => {
                        let $modal = $('<div></div>');
                        mixin.$modal = $modal;
                        expect(mixin.$modal.get(0)).toEqual($modal.get(0));
                        expect(mixin.$modal).toBe(mixin._$modal);
                    });
                });

                describe('#modalValue', () => {
                    it('throws an error if not defined in subclass', () => {
                        expect(() => {
                            temp = mixin.modalValue;
                        }).toThrow(new Error('Must define a modalValue getter for retrieving current modal value'));
                    });
                });

                describe('#displayValueToStorageFormat', () => {
                    it('returns value, unaltered, by default', () => {
                        expect(mixin.displayValueToStorageFormat('test')).toBe('test');
                    });
                });

                describe('#storageValueToDisplayFormat', () => {
                    it('returns value, unaltered, by default', () => {
                        expect(mixin.storageValueToDisplayFormat('test')).toBe('test');
                    });
                });

                describe('#getInputValuesArray()', () => {
                    it('throws error when undefined in subclass', () => {
                        expect(() => {
                            temp = mixin.getInputValuesArray();
                        }).toThrow(new Error('Must define getInputValuesArray() in ModalLauncherMixin implementation'));
                    });
                });

                describe('#getValueForModal()', () => {
                    it('throws an error when undefined in mixin class (mixin is responsible for giving this value to modal)', () => {
                        expect(() => {
                            temp = mixin.getValueForModal();
                        }).toThrow(new Error('Must define getValueForModal() in ModalLauncherMixin implementation'));
                    });
                });

                describe('#getModalValuesString()', () => {
                    it('throws error when undefined in subclass', () => {
                        expect(() => {
                            temp = mixin.getModalValuesString();
                        }).toThrow(new Error('Unimplemented getModalValuesString() method in a ModalLauncher implementation'));
                    });
                });

                describe('#injectModalInputs', () => {
                    it('throws error when undefined in subclass', () => {
                        expect(() => {
                            temp = mixin.injectModalInputs();
                        }).toThrow(new Error('injectModalInputs method is required for ModalLauncherMixin implemenation'));
                    });
                });

                describe('#propagateModalValue', () => {
                    it('throws error when undefined in subclass', () => {
                        expect(() => {
                            temp = mixin.propagateModalValue();
                        }).toThrow(new Error('propagateModalValue must be implemented by mixer of ModalLauncherMixin class'));
                    });
                });
            });

            describe('methods to test pre-render', () => {
                beforeEach(() => {
                    mixin = new FullMixinClass({
                        model: this.model
                    });
                });

                describe('#getInputTemplate', () => {
                    it('returns templates from model if they exist, default if index is out of range', () => {
                        let customModel = _.extend({}, this.model);
                        customModel.attributes = _.extend({}, this.model.attributes, {
                            templates: {
                                inputTemplates: [
                                    'test1',
                                    'test2'
                                ]
                            }
                        });
                        mixin.model = customModel;
                        expect(mixin.getInputTemplate(0)).toBe('test1');
                        expect(mixin.getInputTemplate(1)).toBe('test2');
                        expect(mixin.getInputTemplate(2)).toBe(mixin.defaultInputTemplate);
                    });

                    it('returns default input template if no templates defined', () => {
                        let customModel = _.extend({}, this.model);
                        customModel.attributes = _.extend({}, this.model.attributes, {
                            templates: null
                        });
                        mixin.model = customModel;
                        expect(mixin.getInputTemplate(0)).toBe(mixin.defaultInputTemplate);
                    });
                });

                describe('#delegateEvents()', () => {
                    it('calls addCustomEvents()', () => {
                        spyOn(mixin, 'addCustomEvents');
                        mixin.delegateEvents();
                        expect(mixin.addCustomEvents).toHaveBeenCalled();
                    });
                });

                describe('#undelegateEvents()', () => {
                    it('calls removeCustomEvents()', () => {
                        spyOn(mixin, 'removeCustomEvents');
                        mixin.undelegateEvents();
                        expect(mixin.removeCustomEvents).toHaveBeenCalled();
                    });
                });

                describe('#addCustomEvents()', () => {
                    it('adds click and hidden events to modal and window', () => {
                        // mock up $modal
                        mixin.$modal = $('<div><div class="modal-dialog"></div></div>');
                        mixin.$dialog = $('<div></div>');
                        mixin.$win = $('<div></div>');
                        spyOn(mixin.$modal, 'on');
                        spyOn(mixin.$dialog, 'on');
                        spyOn(mixin.$win, 'on');

                        mixin.addCustomEvents();
                        expect(mixin.$modal.on).toHaveBeenCalledWith('click', '[data-dismiss]', mixin.dialogClosing);
                        expect(mixin.$modal.on).toHaveBeenCalledWith('hidden.bs.modal', mixin.dialogHidden);

                        _.each(mouseDownEvents, (eventName) => {
                            expect(mixin.$dialog.on).toHaveBeenCalledWith(eventName, mixin.modalMouseDown);
                        });

                        _.each(mouseUpEvents, (eventName) => {
                            expect(mixin.$win.on).toHaveBeenCalledWith(eventName, mixin.handleMouseUp);
                        });
                    });
                });

                describe('#removeCustomEvents()', () => {
                    it('removes click and hidden events from modal', () => {
                        // mock up $modal
                        mixin.$modal = $('<div></div>');
                        mixin.$dialog = $('<div></div>');
                        mixin.$win = $('<div></div>');
                        spyOn(mixin.$modal, 'off');
                        spyOn(mixin.$dialog, 'off');
                        spyOn(mixin.$win, 'off');

                        mixin.removeCustomEvents();
                        expect(mixin.$modal.off).toHaveBeenCalledWith('click', '[data-dismiss]', mixin.dialogClosing);
                        expect(mixin.$modal.off).toHaveBeenCalledWith('hidden.bs.modal', mixin.dialogHidden);

                        _.each(mouseDownEvents, (eventName) => {
                            expect(mixin.$dialog.off).toHaveBeenCalledWith(eventName, mixin.modalMouseDown);
                        });

                        _.each(mouseUpEvents, (eventName) => {
                            expect(mixin.$win.off).toHaveBeenCalledWith(eventName, mixin.handleMouseUp);
                        });
                    });
                });
            });
            describe('renderModal', () => {
                let testStrings = {
                        test1: 'test 1',
                        test2: 'test 2',
                        labelOne: 'test label'
                    },
                    expectedStrings = {
                        test1: 'test 1',
                        test2: 'test 2',
                        labelOne: 'test label',
                        modalTitle: '&nbsp;',
                        okButtonText: '&nbsp;',
                        labelTwo: '&nbsp;',
                        shouldBeBlank1: '&nbsp;',
                        shouldBeBlank2: '&nbsp;'
                    },
                    testLabels = { shouldBeBlank1: '', shouldBeBlank2: '', shouldBePopulated: 'POPULATED' };

                beforeEach(() => {
                    mixin = new FullMixinClass({
                        model: this.model
                    });

                    mixin.model.set('labels', testLabels);
                });

                Async.it('calls #renderTemplate with proper strings', () => {
                    spyOn(mixin, 'renderTemplate').and.returnValue('<div></div>');
                    return mixin.renderModal(testStrings).tap(() => {
                        expect(mixin.renderTemplate).toHaveBeenCalledWith(testModalTemplate, expectedStrings);
                    });
                });

                Async.it('calls #injectModalInputs with proper strings', () => {
                    spyOn(mixin, 'injectModalInputs').and.resolve();
                    return mixin.renderModal(testStrings).tap(() => {
                        expect(mixin.injectModalInputs).toHaveBeenCalledWith(expectedStrings);
                    });
                });

                Async.it('calls #embedModal if embedded is true', () => {
                    mixin.model.set('embedded', true);
                    spyOn(mixin, 'embedModal').and.resolve();
                    return mixin.renderModal(testStrings).tap(() => {
                        expect(mixin.embedModal).toHaveBeenCalled();
                    });
                });

                Async.it('does not call #embedModal if embedded is false', () => {
                    mixin.model.set('embedded', false);
                    spyOn(mixin, 'embedModal').and.resolve();
                    return mixin.renderModal(testStrings).tap(() => {
                        expect(mixin.embedModal).not.toHaveBeenCalled();
                    });
                });
            });

            describe('#getModalContainer', () => {
                beforeEach(() => {
                    mixin = new FullMixinClass({
                        model: this.model
                    });
                });

                it('returns the modalContainer jquery collection', () => {
                    // just return the selector so we can use it to test
                    spyOn(window, '$').and.callFake((paramIn) => {
                        if (paramIn === '#modalContainer') {
                            return $('<div id="modalTest"></div>');
                        } else if (paramIn === 'body') {
                            return $('<div id="bodyTest"></div>');
                        }
                        return jQuery(paramIn);
                    });
                    expect(mixin.getModalContainer().attr('id')).toBe('modalTest');
                });

                it('returns the body jquery collection if no modalContainer id', () => {
                    // just return the selector so we can use it to test
                    spyOn(window, '$').and.callFake((paramIn) => {
                        if (paramIn === '#modalContainer') {
                            return $();
                        } else if (paramIn === 'body') {
                            return $('<div id="bodyTest"></div>');
                        }
                        return jQuery(paramIn);
                    });
                    expect(mixin.getModalContainer().attr('id')).toBe('bodyTest');
                });
            });

            describe('#getStringsForInputIndex', () => {
                let strings;
                beforeEach(() => {
                    strings = {
                        input_1_testKey1: 'test 1',
                        input_3_testKey3: 'test 3',
                        testKey1: 'original test 1',
                        testPlainKey: 'testPlainKey'
                    };
                    mixin = new FullMixinClass({
                        model: this.model
                    });
                });

                it('leaves original strings collection unaltered', () => {
                    mixin.getStringsForInputIndex(strings, 1);

                    expect(strings.input_1_testKey1).toBe('test 1');
                    expect(strings.input_3_testKey3).toBe('test 3');
                    expect(strings.testPlainKey).toBe('testPlainKey');
                });

                it('removes strings formatted for inputs', () => {
                    let testStrings = mixin.getStringsForInputIndex(strings, 1);
                    expect(testStrings.input_1_testKey1).toBeUndefined();
                    expect(testStrings.input_3_testKey3).toBeUndefined();
                });

                it('simplifies key for strings intended for this index, but not other indexes', () => {
                    let testStrings = mixin.getStringsForInputIndex(strings, 1);
                    expect(testStrings.testKey1).toBe('test 1');
                    expect(testStrings.testKey3).toBeUndefined();
                });
            });

            describe('post-render functions', () => {
                Async.beforeEach(() => {
                    mixin = new FullMixinClass({
                        model: this.model
                    });
                    return mixin.renderModal({});
                });

                describe('#refreshInputs()', () => {
                    it('calls show, setValue, and pushValue on input', () => {
                        let testVal = 2;
                        spyOn(mixin, 'getInputValuesArray').and.returnValue([testVal]);
                        spyOn(mixin.inputs[0], 'show').and.callThrough();
                        spyOn(mixin.inputs[0], 'setValue').and.callThrough();
                        spyOn(mixin.inputs[0], 'pushValue').and.callThrough();
                        mixin.refreshInputs().then(() => {
                            expect(mixin.inputs[0].show).toHaveBeenCalled();
                            expect(mixin.inputs[0].setValue).toHaveBeenCalledWith(testVal);
                            expect(mixin.inputs[0].pushValue).toHaveBeenCalled();
                        });
                    });
                });

                describe('#revealModalContent', () => {
                    it('adds reveal class to dialog', () => {
                        spyOn(mixin.$dialog, 'addClass');
                        mixin.revealModalContent();
                        expect(mixin.$dialog.addClass).toHaveBeenCalledWith('reveal');
                    });
                });

                describe('#dialogOpened', () => {
                    it('returns undefined if called from a jQuery event (finishes promise).', () => {
                        let e = new $.Event();
                        let ret = mixin.openDialog(e);
                        expect(ret).toBe(undefined);
                    });

                    Async.it('calls refreshSpinners and revealModalContent', () => {
                        spyOn(mixin, 'refreshInputs');
                        spyOn(mixin, 'revealModalContent');
                        return mixin.dialogOpened().then(() => {
                            expect(mixin.refreshInputs.calls.count()).toBe(1);
                            expect(mixin.revealModalContent.calls.count()).toBe(1);
                        });
                    });
                });

                TRACE_MATRIX('DE23812')
                .describe('#dialogHidden', () => {
                    Async.it('calls refreshInputs and then onHidden in input', () => {
                        let isCalled = false,
                            _oldOnHidden = mixin.inputs[0].onHidden,
                            context;

                        // input may not have an onHidden method.  Put one here.
                        mixin.inputs[0].onHidden = function tempFn () {
                            isCalled = true;
                            context = this;
                        };
                        spyOn(mixin, 'refreshInputs').and.resolve();

                        return mixin.dialogHidden()
                        .then(() => {
                            expect(isCalled).toBe(true);
                            expect(context).toBe(mixin.inputs[0]);
                            expect(mixin.refreshInputs).toHaveBeenCalled();

                            // return input to its previous state (no or old onHidden)
                            mixin.inputs[0].onHidden = _oldOnHidden;
                        });
                    });
                });

                TRACE_MATRIX('DE22313');
                describe('modalMouseDown', () => {
                    it('adds nocancel class to $modal and prevents bubbling (target is modal-dialog)', () => {
                        let target = $('<div class="modal-dialog"></div>')[0];
                        let fakeEvent = { preventDefault: $.noop, stopPropagation: $.noop, target };
                        spyOn(fakeEvent, 'preventDefault');
                        spyOn(fakeEvent, 'stopPropagation');
                        spyOn(mixin.$modal, 'addClass');
                        expect(mixin.modalMouseDown(fakeEvent)).toBe(false);
                        expect(fakeEvent.preventDefault).toHaveBeenCalled();
                        expect(fakeEvent.stopPropagation).toHaveBeenCalled();
                        expect(mixin.$modal.addClass).toHaveBeenCalledWith('nocancel');
                    });

                    it('adds nocancel, does not prevent bubbling, returns true (target is not modal-dialog)', () => {
                        let target = $('<div class="not-modal-dialog"></div>')[0];
                        let fakeEvent = { preventDefault: $.noop, stopPropagation: $.noop, target };
                        spyOn(fakeEvent, 'preventDefault');
                        spyOn(fakeEvent, 'stopPropagation');
                        spyOn(mixin.$modal, 'addClass');
                        expect(mixin.modalMouseDown(fakeEvent)).toBe(true);
                        expect(fakeEvent.preventDefault).not.toHaveBeenCalled();
                        expect(fakeEvent.stopPropagation).not.toHaveBeenCalled();
                        expect(mixin.$modal.addClass).toHaveBeenCalledWith('nocancel');
                    });

                    it('does nothing, returns true (embedded)', () => {
                        let target = $('<div class="not-modal-dialog"></div>')[0];
                        let fakeEvent = { preventDefault: $.noop, stopPropagation: $.noop, target };
                        mixin.model.set('embedded', true);
                        spyOn(fakeEvent, 'preventDefault');
                        spyOn(fakeEvent, 'stopPropagation');
                        spyOn(mixin.$modal, 'addClass');
                        expect(mixin.modalMouseDown(fakeEvent)).toBe(true);
                        expect(fakeEvent.preventDefault).not.toHaveBeenCalled();
                        expect(fakeEvent.stopPropagation).not.toHaveBeenCalled();
                        expect(mixin.$modal.addClass).not.toHaveBeenCalledWith('nocancel');
                    });
                });

                TRACE_MATRIX('DE22313');
                describe('handleMouseUp', () => {
                    it('removes nocancel class from modal backdrop (will allow clicking backdrop to cancel modal)', () => {
                        spyOn(mixin.$modal, 'removeClass');
                        mixin.handleMouseUp();
                        expect(mixin.$modal.removeClass).toHaveBeenCalledWith('nocancel');
                    });
                });

                describe('#getModalTranslationsObject()', () => {
                    it('creates a toTranslate object with okButtonText, modalTitle, inputOption items, and labels', () => {
                        mixin.model.set('labels', { testBlank: '', testPopulated: 'populated' });
                        mixin.model.set('okButtonText', 'ok');
                        mixin.model.set('modalTitle', 'title');

                        expect(mixin.getModalTranslationsObject()).toEqual({
                            testPopulated: 'populated',
                            okButtonText: 'ok',
                            modalTitle: 'title',
                            IMAGE_OPTION_0: 'IMAGE_OPTION_0',
                            IMAGE_OPTION_1: 'IMAGE_OPTION_1',
                            IMAGE_OPTION_2: 'IMAGE_OPTION_2',
                            IMAGE_OPTION_3: 'IMAGE_OPTION_3'
                        });
                    });
                });

                describe('#openDialog', () => {
                    beforeEach(() => {
                        // mock abstract function.
                        mixin.getInputValuesArray = () => [1];
                    });
                    it('returns undefined if called from a jQuery event (finishes promise).', () => {
                        let e = new $.Event();
                        let ret = mixin.openDialog(e);
                        expect(ret).toBe(undefined);
                    });

                    Async.it('opens the dialog by calling "show".  Adds and cleans up event handlers for dialog, calls dialogOpened handler.', () => {
                        spyOn(mixin.$modal, 'on').and.callThrough();
                        spyOn(mixin.$modal, 'modal').and.callThrough();
                        spyOn(mixin.$modal, 'off').and.callThrough();
                        spyOn(mixin, 'dialogOpened').and.callThrough();
                        return mixin.openDialog().tap(() => {
                            expect(mixin.$modal.on.calls.count()).toBe(1);
                            expect(mixin.$modal.off.calls.count()).toBe(1);
                            expect(mixin.$modal.modal.calls.count()).toBe(1);
                            expect(mixin.dialogOpened.calls.count()).toBe(1);
                            expect(mixin.$modal.modal).toHaveBeenCalledWith('show');
                        });
                    });
                });

                describe('#dialogClosing', () => {
                    it('returns undefined if called from a jQuery event (finishes promise).', () => {
                        let e = new $.Event();
                        let ret = mixin.dialogClosing(e);
                        expect(ret).toBe(undefined);
                    });
                });

                describe('#removeInputs()', () => {
                    it('destroys and de-allocates inputs', () => {
                        let isCalled = false;
                        mixin.inputs[0].destroy = () => {
                            isCalled = true;
                        };
                        mixin.removeInputs();
                        expect(isCalled).toBe(true);
                        expect(mixin.inputs.length).toBe(0);
                    });
                });

                describe('#removeModal()', () => {
                    it('removes modal dialog', () => {
                        spyOn(mixin.$modal, 'modal');
                        spyOn(mixin.$modal, 'remove');

                        // cache modal selector, since we are de-referencing it.
                        let $oldModal = mixin.$modal;

                        mixin.removeModal();

                        expect($oldModal.modal).toHaveBeenCalledWith('hide');
                        expect($oldModal.remove).toHaveBeenCalled();
                        expect(mixin.$modal).toBeNull();
                        expect(mixin.$dialog).toBeNull();
                    });
                });

                describe('#destroy()', () => {
                    it('undelegates events and removes modal', () => {
                        spyOn(mixin, 'undelegateEvents');
                        spyOn(mixin, 'removeInputs');
                        spyOn(mixin, 'removeModal');

                        mixin.destroy();

                        expect(mixin.undelegateEvents).toHaveBeenCalled();
                        expect(mixin.removeInputs).toHaveBeenCalled();
                        expect(mixin.removeModal).toHaveBeenCalled();
                        expect(mixin.$win).toBeNull();
                    });
                });

                describe('#renderTemplate()', () => {
                    it('calls LF.templates.display', () => {
                        spyOn(LF.templates, 'display');

                        mixin.renderTemplate('testName', { option: 'val' });

                        expect(LF.templates.display).toHaveBeenCalledWith('testName', { option: 'val' });
                    });
                });

                describe('#embedModal', () => {
                    beforeEach(() => {
                        // setup view to have proper divs
                        mixin.$el.html(`<div>
                            <input type='text' id='inputText' />
                            <div class='embedded-control-container'></div>
                        </div>`);
                    });
                    Async.it('converts input elements to hidden and injects modal content into .embedded-modal-container div', () => {
                        return mixin.embedModal()
                        .then(() => {
                            let $input = mixin.$('#inputText'),
                                $controlContainer = mixin.$('.embedded-control-container');
                            expect($input.attr('type')).toBe('hidden');
                            expect($controlContainer.children('.modal-body').length).toBe(1);
                        });
                    });
                    Async.it('calls refreshInputs and modalValueChanged', () => {
                        spyOn(mixin, 'refreshInputs').and.resolve();
                        spyOn(mixin, 'modalValueChanged').and.resolve();

                        return mixin.embedModal()
                        .then(() => {
                            expect(mixin.refreshInputs).toHaveBeenCalled();
                            expect(mixin.modalValueChanged).toHaveBeenCalled();
                        });
                    });
                });

                describe('#modalValueChanged', () => {
                    Async.it('does nothing if not embedded', () => {
                        spyOn(mixin, 'propagateModalValue').and.resolve();
                        return mixin.modalValueChanged()
                        .then(() => {
                            expect(mixin.propagateModalValue).not.toHaveBeenCalled();
                        });
                    });

                    Async.it('calls propagate modal value if embedded', () => {
                        mixin.model.set('embedded', true);
                        spyOn(mixin, 'propagateModalValue').and.resolve();
                        return mixin.modalValueChanged()
                        .then(() => {
                            // called with false to specify that inputs should not be stopped to gather this value
                            expect(mixin.propagateModalValue).toHaveBeenCalledWith(false);
                        });
                    });
                });

                TRACE_MATRIX('DE23771')
                .describe('#processValue', () => {
                    Async.it('does nothing if not embedded', () => {
                        spyOn(mixin, 'propagateModalValue').and.resolve();
                        return mixin.processValue()
                        .then(() => {
                            expect(mixin.propagateModalValue).not.toHaveBeenCalled();
                        });
                    });

                    Async.it('calls propagate modal value if embedded', () => {
                        mixin.model.set('embedded', true);
                        spyOn(mixin, 'propagateModalValue').and.resolve();
                        return mixin.processValue()
                        .then(() => {
                            // called with false to specify that inputs should not be stopped to gather this value
                            expect(mixin.propagateModalValue).toHaveBeenCalledWith(true);
                        });
                    });
                });
            });
        });
    }
}

new ModalLauncherMixinTests().execTests();
