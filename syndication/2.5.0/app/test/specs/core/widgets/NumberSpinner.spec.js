import 'test/helpers/SpecHelpers';
import 'test/helpers/StudyDesign';
import 'core/resources/Templates';

import NumberSpinnerMixin from 'core/widgets/mixin/NumberSpinnerMixin';
import TextBoxCustomInputBaseTests from './TextBoxCustomInputBase.spec.js';
import Widget from 'core/models/Widget';
import NumberSpinner from 'core/widgets/NumberSpinner';

/* global describe, it, beforeEach, afterEach, beforeAll, afterAll, spyOn */
export default class NumberSpinnerTests extends TextBoxCustomInputBaseTests {
    constructor (model = NumberSpinnerTests.model) {
        super(model);
    }

    /**
     * {Widget} get the widget model.
     */
    static get model () {
        return new Widget({
            id: 'NumberSpinner_W_1',
            type: 'NumberSpinner',
            className: 'NumberSpinner_W_1'
        });
    }

    execTests () {
        super.execTests();

        // eslint-disable-next-line new-cap

        TRACE_MATRIX('US6106')
        .TRACE_MATRIX('US7038')
        .describe('NumberSpinner', () => {
            let templates,
                dummyParent,
                security,
                widget = null;

            beforeAll(() => {
                security = LF.security;
                LF.security = this.getDummySecurity();
                templates = LF.templates;
            });

            afterAll(() => {
                LF.security = security;
                LF.templates = templates;
            });

            beforeEach(() => {
                dummyParent = this.getMinDummyQuestion();
                widget = new LF.Widget[NumberSpinnerTests.model.get('type')]({
                    model: NumberSpinnerTests.model,
                    mandatory: false,
                    parent: dummyParent
                });
            });

            afterEach(() => {
                $(`#${dummyParent.getQuestionnaire().id}`).remove();
                widget = null;
            });

            describe('#constructor', () => {
                it('initializes TextBoxCustomInputBase with NumberSpinnerMixin', () => {
                    let model = NumberSpinnerTests.model;
                    spyOn(model, 'set').and.callThrough();
                    widget = new LF.Widget[NumberSpinnerTests.model.get('type')]({
                        model,
                        mandatory: false,
                        parent: this.dummyParent
                    });

                    expect(model.set).toHaveBeenCalledWith('modalMixin', NumberSpinnerMixin);
                });
            });
        });
    }
}

let tester = new NumberSpinnerTests();
tester.execTests();
