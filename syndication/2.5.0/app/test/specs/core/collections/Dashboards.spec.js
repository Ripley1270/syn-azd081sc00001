import * as helpers from 'test/helpers/SpecHelpers';
import Dashboards from 'core/collections/Dashboards';
import Dashboard from 'core/models/Dashboard';
import * as lStorage from 'core/lStorage';

describe('Dashboards', () => {

    let collection,
        now = new Date();

    beforeAll(() => {

        helpers.installDatabase();

        lStorage.setItem('krpt', 'krpt.39ed120a0981231');

    });

    afterAll(() => {

        lStorage.removeItem('krpt');

    });

    beforeEach(() => {

        collection = new Dashboards([{
            id: 1,
            subject_id: '9876',
            SU: 'DAILY_DIARY',
            questionnaire_id: 'DAILY_DIARY',
            started: now.ISOStamp(),
            completed: now.ISOStamp(),
            diary_id: now.getTime(),
            completed_tz_offset: now.getOffset(),
            phase: 10,
            device_id: 'foo',
            study_version: '9'
        }, {
            id: 2,
            subject_id: '9876',
            SU: 'DAILY_DIARY',
            questionnaire_id: 'DAILY_DIARY',
            started: now.ISOStamp(),
            completed: now.ISOStamp(),
            diary_id: now.getTime(),
            completed_tz_offset: now.getOffset(),
            phase: 10,
            device_id: 'foo',
            study_version: '9'
        }, {
            id: 3,
            subject_id: '9876',
            SU: 'MEDS',
            questionnaire_id: 'MEDS',
            started: now.ISOStamp(),
            completed: now.ISOStamp(),
            diary_id: now.getTime(),
            completed_tz_offset: now.getOffset(),
            phase: 10,
            device_id: 'foo',
            study_version: '9'
        }]);

    });

    it('should have a model', () => {

        expect(collection.model).toBeDefined();

    });

    it('should have a storage property', () => {

        expect(collection.storage).toEqual('Dashboard');

    });

    Async.it('should save the collection to the database', () => {
        return collection.save()
            .then((res) => {
                expect(res).toEqual([1, 2, 3]);
            });

    });

    Async.it('should fetch all records.', () => {

        const localDashboards = new Dashboards();

        return localDashboards.clear()
            .then(() => localDashboards.fetch())
            .tap(() => {
                expect(localDashboards.size()).toEqual(0);
            })
            .then(() => collection.save())
            .then(() => localDashboards.fetch())
            .tap(() => {
                expect(localDashboards.size()).toEqual(3);
            });

    });

    Async.it('should fetch the \'DAILY_DIARY\' records by query.', () => {

        collection.reset([]);

        return collection.fetch({
                search: {
                    where: {
                        questionnaire_id: 'DAILY_DIARY'
                    }
                }
            })
            .tap(() => {
                expect(collection.size()).toEqual(2);
                expect(collection.at(0).get('id')).toEqual(1);
                expect(collection.at(1).get('id')).toEqual(2);
            });

    });

    Async.it('should fetch the \'DAILY_DIARY\' records in descending order.', () => {

        collection.reset([]);

        return collection.fetch({
            search: {
                where: {
                    questionnaire_id: 'DAILY_DIARY'
                },
                order: {
                    field: 'id',
                    modifier: 'DESC'
                }
            }
        }).tap(() => {
            expect(collection.size()).toEqual(2);
            expect(collection.at(0).get('id')).toEqual(2);
            expect(collection.at(1).get('id')).toEqual(1);
        });

    });

    Async.it('should fetch only the last \'DAILY_DIARY\' record.', () => {

        collection.reset([]);

        return collection.fetch({
            search: {
                where: {
                    questionnaire_id: 'DAILY_DIARY'
                },
                order: {
                    field: 'id',
                    modifier: 'DESC'
                },
                limit: 1
            }
        }).then(() => {
            expect(collection.size()).toEqual(1);
            expect(collection.at(0).get('id')).toEqual(2);

        });

    });

    it('should return 0 models', () => {

        let models = collection.match({
            id: 4
        });

        expect(models.length).toBeFalsy();

    });

    it('should return a missing arguments error (match)', () => {

        expect(() => {

            let models = collection.match();

        }).toThrowError('Invalid number of arguments.');

    });

    Async.it('should remove all records from the database.', () => {

        return collection.destroy()
            .then(() => {
                collection.reset();
                collection.fetch();
            }).tap(() => {
                expect(collection.size()).toEqual(0);
            });
    });

});
