import ScreenshotController from 'core/screenshot/ScreenshotController';
import ScreenshotConfigModel from 'core/screenshot/ScreenshotConfigModel';
import ScreenshotView from 'core/screenshot/pages/ScreenshotView';
import ScreenshotConfigDiary from 'core/screenshot/pages/ScreenshotConfigDiary';
import ScreenshotConfigCoreScreen from 'core/screenshot/pages/ScreenshotConfigCoreScreen';
import ScreenshotConfigMessagebox from 'core/screenshot/pages/ScreenshotConfigMessagebox';
import ScreenshotTool from 'core/screenshot/ScreenshotTool';

import RouterBase from 'core/classes/RouterBase';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

TRACE_MATRIX('US6500').
describe('ScreenshotController', () => {
    let controller, nav;

    const setupController = (product = 'logpad') => {
        controller = new ScreenshotController({ product });
        spyOn(controller, 'navigate').and.callFake(arg => nav = arg);
    };

    Async.beforeEach(() => {
        resetStudyDesign();
        nav = false;

        return specHelpers.initializeContent();
    });

    describe('navigating to routes', () => {
        describe('basic routes', () => {
            const testNavigation = (route, view) => () => {
                controller[route]();
                expect(controller.view).toEqual(jasmine.any(view));
            };

            beforeEach(setupController);

            // @TODO: These need templates to render...
            // it('should navigate to the screenshot view',
            //     testNavigation('screenshot', ScreenshotView));

            // it('should navigate to the screenshot diary config view',
            //     testNavigation('screenshotDiary', ScreenshotConfigDiary));

            //@TODO: once these two screens are done.
            // it('should navigate to the screenshot core screen config view',
            //     testNavigation('screenshotCoreScreen', ScreenshotConfigCoreScreen));

            // it('should navigate to the screenshot messagebox config view',
            //     testNavigation('screenshotMessagebox', ScreenshotConfigMessageBox));
        });

        describe('nextMode route', () => {
            const allModes = [
                'Diaries',
                'Pages'
            ];

            const testViewForMode = mode => {
                const expectViewNav = View => {
                    expect(nav).toBe(jasmine.any(View));
                };

                switch (mode) {
                    case 'Diaries':
                        expect(nav).toBe('screenshot_diary');
                        break;
                    case 'Pages':
                        expect(nav).toBe('screenshot_page');
                        break;
                    case 'done':
                        expect(nav).toBe('screenshot_take_screenshots');
                        break;
                    default:
                        fail('Mode not allowed');
                }
            };

            const loopModes = modes => () => {
                spyOn(controller, 'takeScreenshots').and.callFake($.noop);

                controller.model.get('selection').set('modes', modes);

                _.each([...modes, 'done'], mode => {
                    controller.nextMode();
                    controller.model.nextMode();
                    testViewForMode(mode);
                });

            };

            beforeEach(() => {
                nav = false;
                setupController();
            });

            it('should navigate through all modes if all modes are selected', loopModes(allModes));
            // it('should navigate through some modes if some modes are selected', loopModes([
            //     allModes[0],
            //     allModes[2]
            // ]));

            it('should navigate back to mode select', () => {
                controller.model.get('selection').set('modes', allModes);
                controller.nextMode();
                expect(nav).toBe('screenshot_diary');

                controller.model.prevMode();
                controller.nextMode();
                expect(nav).toBe('screenshot_mode');
            });
        });
    });

    describe('takeScreenshots', () => {
        Async.it('should call takeModeScreenshot for each selected mode', () => {
            const modesToTest = ['Diaries', 'Messagebox'];
            let takeModeScreenshotCalled = [];

            spyOn(ScreenshotTool.prototype, 'takeModeScreenshot').and.callFake(modes => {
                takeModeScreenshotCalled = [...takeModeScreenshotCalled, ...modes];
                return Q();
            });

            setupController();
            controller.model.get('selection').set('modes', modesToTest);

            return controller.takeScreenshots()
                .then(() => {
                    expect(takeModeScreenshotCalled).toEqual(modesToTest);
                    expect(nav).toBe('screenshot');
                });
        });
    });
});
