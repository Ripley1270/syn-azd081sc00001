import editQuestionnaire from 'core/actions/editQuestionnaire';
import Data from 'core/Data';

describe('editQuestionnaire', () => {
    beforeAll(() => {
        LF.router = jasmine.createSpyObj('router', ['navigate']);
    });

    Async.it('LF.router.navigate should have been called', () => {
        let input = {
            questionnaire: 'testStr1',
            dashboardId: 1
        };

        return Q.promise((resolve) => {
            editQuestionnaire(input, () => {
                expect(Data.Questionnaire).toEqual({});
                expect(LF.router.navigate).toHaveBeenCalledWith('questionnaire/testStr1/edit/1', true);
                resolve();
            });
        });
    });
});
