import { displayMessage } from 'core/actions/displayMessage';
import { Banner } from 'core/Notify';
import Spinner from 'core/Spinner';

describe('displayMessage', () => {
    beforeAll(() => {
        spyOn(Banner, 'closeAll');
        spyOn(Spinner, 'show');
    });

    Async.it('should display the spinner', () => {
        return Q.Promise(resolve => {
            displayMessage('', () => {
                expect(Banner.closeAll).toHaveBeenCalled();
                expect(Spinner.show).toHaveBeenCalled();
                resolve();
            });
        });
    });
});
