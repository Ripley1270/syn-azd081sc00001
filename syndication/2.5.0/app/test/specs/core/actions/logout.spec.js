import { logout } from 'core/actions/logout';

describe('logout', () => {
    beforeEach(() => {
        LF.security = jasmine.createSpyObj('security', ['logout']);
    });
    Async.it('should logout of the application.', () => {
        return Q.promise(resolve => {
            logout({}, () => {
                resolve();
            });
        }).then(() => {
            expect(LF.security.logout).toHaveBeenCalledWith(true);
        });

    });
});
