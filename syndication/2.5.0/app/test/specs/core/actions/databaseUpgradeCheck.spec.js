import databaseUpgradeCheck from 'core/actions/databaseUpgradeCheck';
import * as helpers from 'core/Helpers';
import Logger from 'core/Logger';
import DatabaseVersion from 'core/models/DatabaseVersion';
import DatabaseVersions from 'core/collections/DatabaseVersions';

// TODO: This test suite passes but does not test/cover the module functionality/behavior well enough. It needs to be re-visited.
describe('databaseUpgradeCheck', () => {
    let params = {
        config : [
            {
                version: 3,
                upgradeFunction : function (callback) {
                    callback(true);
                }
            },{
                version: 9,
                upgradeFunction : function (callback) {
                    callback(true);
                }
            },{
                version : 1,
                sql : [
                    'CREATE TABLE IF NOT EXISTS DatabaseVersions (id INTEGER PRIMARY KEY AUTOINCREMENT, versionName VARCHAR, version INTEGER);',
                    'INSERT INTO DatabaseVersions (versionName, version) VALUES (\'DB_Core\', 0);',
                    'INSERT INTO DatabaseVersions (versionName, version) VALUES (\'DB_Study\', 0);'
                ]
            },{
                version : 2,
                sql : [
                    'CREATE TABLE IF NOT EXISTS SiteUsers (id INTEGER PRIMARY KEY AUTOINCREMENT, site_user_name VARCHAR, last_used VARCHAR);',
                    'CREATE TABLE IF NOT EXISTS Sites (id INTEGER PRIMARY KEY AUTOINCREMENT, site_code VARCHAR, study_name VARCHAR, hash VARCHAR);',
                    'CREATE TABLE IF NOT EXISTS Schedules (id INTEGER PRIMARY KEY AUTOINCREMENT, scheduleId VARCHAR, target VARCHAR, scheduleFunction VARCHAR, ' +
                    'scheduleParams VARCHAR, alarmFunction VARCHAR, alarmParams VARCHAR, phase VARCHAR);'
                ]
            }],
        versionName : 'DB_Core'
    };

    let model;

    beforeEach(function () {
        model = new LF.Model.DatabaseVersion({
            versionName : 'DB_Core',
            version     : 0
        });
        spyOn(helpers, 'getDatabaseVersion');
        spyOn(DatabaseVersions, 'fetchCollection').and.callThrough();
        spyOn(Logger.prototype, 'info').and.callThrough();
        spyOn(Logger.prototype, 'error').and.callThrough();
        spyOn(Logger.prototype, 'warn').and.callThrough();
    });

    Async.it('should not upgrade to lower version', () => {
        helpers.getDatabaseVersion.and.callThrough();

        return databaseUpgradeCheck(params).then(() => {
            expect(helpers.getDatabaseVersion).toHaveBeenCalledWith('DB_Core');
        });
    });

    Async.it('should not upgrade when there is no available versions', () => {
        helpers.getDatabaseVersion.and.callThrough();

        return databaseUpgradeCheck({}).then(() => {
            expect(helpers.getDatabaseVersion).toHaveBeenCalled();
            expect(Logger.prototype.error).toHaveBeenCalledWith(
                'Missing version type: version type is null or undefined');
        });
    });

    Async.it('should catch an error that Database upgrade failed', () => {
        helpers.getDatabaseVersion.and.reject('err for test');

        return databaseUpgradeCheck(params)
        .catch((err) => {
            expect(helpers.getDatabaseVersion).toHaveBeenCalledWith(params.versionName);
            expect(Logger.prototype.error).toHaveBeenCalled();
        });
    });

    Async.it('should have upgraded the database ', () => {
        helpers.getDatabaseVersion.and.resolve();
        spyOn(DatabaseVersion.prototype, 'save').and.callThrough();
        spyOn(Array.prototype, 'reduce').and.callThrough();

        return databaseUpgradeCheck(params).then(function () {
            expect(helpers.getDatabaseVersion).toHaveBeenCalledWith('DB_Core');

            expect(Logger.prototype.info.calls.count()).toEqual(4);
            expect(Logger.prototype.info.calls.argsFor(0)).toEqual(
                [`Data Upgrade is detected for version 1 of DB_Core`]);
            expect(Logger.prototype.info.calls.argsFor(1)).toEqual(
                [`Data Upgrade is detected for version 2 of DB_Core`]);
            expect(Logger.prototype.info.calls.argsFor(2)).toEqual(
                [`Data Upgrade is detected for version 3 of DB_Core`]);
            expect(Logger.prototype.info.calls.argsFor(3)).toEqual(
                [`Data Upgrade is detected for version 9 of DB_Core`]);

            expect(Logger.prototype.warn.calls.count()).toEqual(2);
            expect(Logger.prototype.warn.calls.argsFor(0)).toEqual(
                [`SQL upgrades are no longer needed. Upgrade version: 1`]);
            expect(Logger.prototype.warn.calls.argsFor(1)).toEqual(
                [`SQL upgrades are no longer needed. Upgrade version: 2`]);

            expect(DatabaseVersion.prototype.save.calls.count()).toEqual(2);
            expect(DatabaseVersion.prototype.save.calls.argsFor(0)).toEqual(
                [{versionName: 'DB_Core', version: 3}]);
            expect(DatabaseVersion.prototype.save.calls.argsFor(1)).toEqual(
                [{versionName: 'DB_Core', version: 9}]);

            expect(Array.prototype.reduce).toHaveBeenCalledWith(
                jasmine.any(Function), jasmine.any(Object));
        });
    });

    Async.it('should have reject with error: No upgrade function configured', () => {
        let params1 = {
            config : [{version: 3}],
            versionName : 'DB_Core'
        };
        helpers.getDatabaseVersion.and.resolve();
        spyOn(Array.prototype, 'reduce').and.callThrough();

        return databaseUpgradeCheck(params1).then(function () {
            expect(helpers.getDatabaseVersion).toHaveBeenCalledWith('DB_Core');
            expect(Array.prototype.reduce).toHaveBeenCalled();
            expect(Logger.prototype.error).toHaveBeenCalledWith('Database upgrade failed:',
                new Error(`No upgrade function configured for version 3 of DB_Core`));
        });
    });

    Async.it('should have reject with error: No upgrade function configured with version and versionName', () => {
        let params2 = {
            config : [
                {
                    version: 3,
                    upgradeFunction : function (callback) {
                        callback(false);
                    }
                }],
            versionName : 'DB_Core'
        };
        helpers.getDatabaseVersion.and.resolve();

        return databaseUpgradeCheck(params2).then(function () {
            expect(helpers.getDatabaseVersion).toHaveBeenCalledWith('DB_Core');
            expect(Logger.prototype.error).toHaveBeenCalledWith('Database upgrade failed:',
                new Error(`Version 3 of DB_Core`));
        });
    });
});
