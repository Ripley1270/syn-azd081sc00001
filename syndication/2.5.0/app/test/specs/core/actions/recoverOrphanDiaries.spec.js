import { recoverOrphanDiaries } from 'core/actions/recoverOrphanDiaries';
import Transmission from 'core/models/Transmission';
import Transmissions from 'core/collections/Transmissions';
import Dashboards from 'core/collections/Dashboards';
import Logger from 'core/Logger';


describe('recoverOrphanDiaries', () => {

    it('Should call the dashboards.fetch, transmissions.fetch and transmission.save method', () => {
        spyOn(Dashboards.prototype, 'fetch').and.callFake(function(options) {
            this.add([
                {
                    id: 'SomeID1',
                    subject_id: 'SID1'
                }
            ]);
            options.onSuccess();
        });
        spyOn(Transmissions.prototype, 'fetch').and.callFake((options) => {
            options.onSuccess();
        });
        spyOn(Transmission.prototype, 'save');
        recoverOrphanDiaries(null, () => {});
        expect(Dashboards.prototype.fetch).toHaveBeenCalledWith(jasmine.any(Object));
        expect(Transmissions.prototype.fetch).toHaveBeenCalledWith(jasmine.any(Object));
        expect(Transmission.prototype.save).toHaveBeenCalledWith(jasmine.any(Object), jasmine.any(Object));
    });

    it('Should not call the transmission.save method', () => {
        spyOn(Dashboards.prototype, 'fetch').and.callFake(function(options) {
            options.onSuccess();
        });
        spyOn(Transmissions.prototype, 'fetch').and.callFake((options) => {
            options.onSuccess();
        });
        spyOn(Transmission.prototype, 'save');
        recoverOrphanDiaries(null, (res) => {
            expect(res).toBe(true);
        });
        expect(Dashboards.prototype.fetch).toHaveBeenCalledWith(jasmine.any(Object));
        expect(Transmissions.prototype.fetch).toHaveBeenCalledWith(jasmine.any(Object));
        expect(Transmission.prototype.save).not.toHaveBeenCalled();
    });

    it('Should perform onError logging when dashboards.fetch fails', () => {
        spyOn(Dashboards.prototype, 'fetch').and.callFake(function(options) {
            options.onError('This is an expected error for testing');
        });
        spyOn(Transmissions.prototype, 'fetch').and.callFake((options) => {
            options.onSuccess();
        });
        spyOn(Transmission.prototype, 'save');
        spyOn(Logger.prototype, 'error');
        recoverOrphanDiaries(null, (res) => {
            expect(res).toBe(true);
        });
        expect(Dashboards.prototype.fetch).toHaveBeenCalledWith(jasmine.any(Object));
        expect(Transmissions.prototype.fetch).not.toHaveBeenCalled();
        expect(Transmission.prototype.save).not.toHaveBeenCalled();
        expect(Logger.prototype.error).toHaveBeenCalled();
    });

    it('Should perform onError logging when transmissions.fetch fails', () => {
        spyOn(Dashboards.prototype, 'fetch').and.callFake(function(options) {
            options.onSuccess();
        });
        spyOn(Transmissions.prototype, 'fetch').and.callFake((options) => {
            options.onError('Error generated for testing purposes');
        });
        spyOn(Logger.prototype, 'error');
        recoverOrphanDiaries(null, (res) => {
            expect(res).toBe(true);
        });
        expect(Dashboards.prototype.fetch).toHaveBeenCalledWith(jasmine.any(Object));
        expect(Transmissions.prototype.fetch).toHaveBeenCalled();
        expect(Logger.prototype.error).toHaveBeenCalled();
    });
});
