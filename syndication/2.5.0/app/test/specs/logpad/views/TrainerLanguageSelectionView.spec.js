import LanguageSelectionView from 'trainer/logpad/TrainerLanguageSelectionView';
import ELF from 'core/ELF';
import * as specHelpers from 'test/helpers/SpecHelpers';
import Languages from 'core/collections/Languages';

TRACE_MATRIX('US5955')
.describe('TrainerLanguageSelectionView', () => {
    let view,
        removeTemplate,
        removeLangTemplate;

        LF.Preferred = { language: 'es', locale: 'ES' };
        LF.strings = new Languages();

        LF.strings.add([{
            namespace: 'CORE',
            language: 'en',
            locale: 'US',
            localized: 'English (US)',
            direction: 'ltr',
            resources: {
                APPLICATION_HEADER: 'ERT',
                LANGUAGE: 'Language',
                SELECT_LANGUAGE: 'Please select a preferred language for the subject.',
                NEXT: 'Next',
                BACK: 'Back'
            }
        }, {
            namespace: 'CORE',
            language: 'es',
            locale: 'ES',
            localized: 'Español (España)',
            direction: 'ltr',
            resources: {
                APPLICATION_HEADER: 'ERT',
                LANGUAGE: 'Idioma:',
                SELECT_LANGUAGE: 'Please select a preferred language for the subject. (es-ES)',
                NEXT: 'Siguiente',
                BACK: 'Atrás'
            }
        }, {
            namespace: 'CORE',
            language: 'fr',
            locale: 'FR',
            localized: 'Français (France)',
            direction: 'ltr',
            resources: {
                APPLICATION_HEADER: 'ERT',
                LANGUAGE: 'Langue :',
                SELECT_LANGUAGE: 'Please select a preferred language for the subject. (fr-FR)',
                NEXT: 'Suivant',
                BACK: 'Précédent'
            }
        }]);

    Async.beforeEach(() => {
        return Q()
        .then(() => {
            removeTemplate = specHelpers.renderTemplateToDOM('trainer/logpad/templates/templates.ejs');
            removeLangTemplate = specHelpers.renderTemplateToDOM('trainer/logpad/templates/trainer-language-option.ejs');

            view = new LanguageSelectionView();

            spyOn($.fn, 'select2').and.callThrough();
        })
        .then(() => view.resolve())
        .then(() => view.render());
    });

    afterEach(() => {
        removeTemplate();
        removeLangTemplate();
    });

    it('should have the correct id.', () => {
        expect(view.id).toBe('trainer-setup-page');
    });

    it('should have the correct template value.', () => {
        expect(view.template).toBe('#trainer-language-selection-template');
    });

    describe('method:render', () => {
        Async.it('should fail to render.', () => {
            spyOn(view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));

            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => fail('Method render should have been rejected.'))
            .catch(e => expect(e).toBe('DOMError'));
        });

        Async.it('should call the correct functions and render with the default language selected.', () => {
            let request = view.render();

            expect(request).toBePromise();

            return request.then(() => {
                expect($.fn.select2).toHaveBeenCalled();
                expect(view.$language.val()).toEqual('es-ES');
            });
        });

        Async.it('should render all languages.', () => {
            let request = view.render();

            expect(request).toBePromise();

            let options = view.$language.find('option');

            return request.then(() => {
                expect(options.length).toBe(LF.strings.getLanguages().length);
            });
        });
    });


    describe('method:next', () => {
        beforeEach(() => {
            spyOn(view, 'navigate').and.stub();
            spyOn(ELF, 'trigger').and.resolve({ });
        });

        Async.it('should navigate to the "Set Time Zone Activation" screen with default language', () => {
            spyOn(localStorage, 'setItem').and.stub();

            let request = view.next();

            return request.then(() => {
                expect(localStorage.setItem).toHaveBeenCalledWith('preferredLanguageLocale', 'es-ES');
                expect(view.navigate).toHaveBeenCalledWith('set_time_zone_activation', true);
            });
        });

        Async.it('should navigate to the "Set Time Zone Activation" screen with selected language', () => {
            spyOn(localStorage, 'setItem').and.stub();

            view.$language.val('fr-FR');

            let request = view.next();

            return request.then(() => {
                expect(localStorage.setItem).toHaveBeenCalledWith('preferredLanguageLocale', 'fr-FR');
                expect(view.navigate).toHaveBeenCalledWith('set_time_zone_activation', true);
            });
        });
    });
});
