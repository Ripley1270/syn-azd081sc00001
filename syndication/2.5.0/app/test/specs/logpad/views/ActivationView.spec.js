import * as lStorage from 'core/lStorage';
import { Banner } from 'core/Notify';
import ActivationView from 'logpad/views/ActivationView';
import Data from 'core/Data';
import 'core/wrapperjs/Wrapper';

import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

import ActivationBaseViewSuite from 'test/specs/core/views/ActivationBaseView.specBase';

class ActivationViewSuite extends ActivationBaseViewSuite {
    beforeEach () {
        resetStudyDesign();

        this.removeTemplate = specHelpers.renderTemplateToDOM('logpad/templates/activation.ejs');
        this.removeSpinnerTemplate = specHelpers.renderTemplateToDOM('core/templates/spinner.ejs');

        // Add default password format
        LF.StudyDesign.defaultPasswordFormat = {
            max: 20,
            min: 4,
            custom: [],
            allowRepeating: true,
            allowConsecutive: true
        };

        this.view = new ActivationView();

        return this.view.resolve()
        .then(() => this.view.render())
        .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();
        this.removeSpinnerTemplate()

        return super.afterEach();
    }

    testPrivacyPolicy () {
        describe('method:privacyPolicy', () => {
            it('should navigate to the privacy policy.', () => {
                spyOn(this.view, 'navigate').and.stub();

                this.view.privacyPolicy();

                expect(this.view.navigate).toHaveBeenCalledWith('privacy_policy_activation');
            });
        });
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should fail to render the view.', () => {
                spyOn(this.view, 'buildHTML').and.reject('DOMError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request
                    .then(() => fail('Method render should have failed.'))
                    .catch(e => {
                        expect(e).toBe('DOMError');
                    });
            });
        });
    }

    testSubmit () {
        describe('method:submit', () => {
            it('should play an error sound.', () => {
                spyOn(this.view.sound, 'play');

                this.view.submit({ preventDefault: $.noop });

                expect(this.view.sound.play).toHaveBeenCalledWith('error-audio');
            });

            it('should activate with the secret question feature turned off.', () => {
                LF.StudyDesign.askSecurityQuestion = false;

                this.view.$newPassword.val('1234');
                this.view.$confirmPassword.val('1234');

                spyOn(this.view, 'activate');

                this.view.submit({preventDefault: $.noop});

                expect(this.view.activate).toHaveBeenCalled();
            });

            it('should not activate with the secret question feature turned off', () => {
                LF.StudyDesign.askSecurityQuestion = false;

                this.view.$newPassword.val('12345');
                this.view.$confirmPassword.val('54321');

                spyOn(this.view, 'activate');

                this.view.submit({ preventDefault: $.noop });

                expect(this.view.activate).not.toHaveBeenCalled();
            });
        });
    }
}

describe('ActivationView', () => {
    let suite = new ActivationViewSuite();

    suite.executeAll({
        id: 'activation-page',
        template: '#activation-template',
        button: '#submit',
        input: '#txtNewPassword',
        dynamicStrings: {
            code: '12345',
            key: '54321',
            passwordRules: '',
            inputType: 'password'
        },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });

    // ActivationView
    suite.testPrivacyPolicy();
    suite.testRender();
    suite.testSubmit();
});
