import Answers from 'core/collections/Answers';
import Answer from 'core/models/Answer';
import { eCoaDB } from 'core/dataAccess';

describe('Encryption', () => {
    let collection = new Answers(),
        model,
        data,
        encryptedData,
        now,
        past;

    beforeEach(() => {
        model = new Answer();
        now = new Date();

        data = {
            id                  : 1,
            subject_id          : '12345',
            question_id         : 'DAILY_DIARY_Q_1',
            questionnaire_id    : 'Daily_Diary',
            response            : '1',
            SW_Alias            : 'Daily_Diary.0.DAILY_DIARY_Q_1',
            dashboardId: 1
        };
    });

    Async.afterAll(() => {
        return Answers.clearStorage();
    });

    it ('should encrypt the model.', () => {
        model.set(data).encrypt();

        encryptedData = model.attributes;

        expect(model.get('id')).toEqual(data.id);
        expect(model.get('subject_id')).not.toEqual(data.subject_id);
        expect(model.get('question_id')).toEqual(data.question_id);
        expect(model.get('questionnaire_id')).toEqual(data.questionnaire_id);
        expect(model.get('response')).not.toEqual(data.response);
        expect(model.get('SW_Alias')).toEqual(data.SW_Alias);
        expect(model.get('dashboardId')).toEqual(data.dashboardId);
    });

    it('should decrypt the model.', () => {
        model.set(encryptedData).decrypt();

        expect(model.get('id')).toEqual(data.id);
        expect(model.get('subject_id')).toEqual(data.subject_id);
        expect(model.get('question_id')).toEqual(data.question_id);
        expect(model.get('questionnaire_id')).toEqual(data.questionnaire_id);
        expect(model.get('response')).toEqual(data.response);
        expect(model.get('SW_Alias')).toEqual(data.SW_Alias);
        expect(model.get('dashboardId')).toEqual(data.dashboardId);
    });

    Async.it('should save the model to the database.', () => {
        return model.save(data).tap((res) => {
            expect(res).toBeDefined();
        });
    });

    Async.it('should pull the encrypted data from the database.', () => {
        return eCoaDB.Answer.all().tap(res => {
            let model = new Answer(res.at(0));

            expect(model.get('id')).toEqual(data.id);
            expect(model.get('subject_id')).not.toEqual(data.subject_id);
            expect(model.get('question_id')).toEqual(data.question_id);
            expect(model.get('questionnaire_id')).toEqual(data.questionnaire_id);
            expect(model.get('response')).not.toEqual(data.response);
            expect(model.get('SW_Alias')).toEqual(data.SW_Alias);
            expect(model.get('dashboardId')).toEqual(data.dashboardId);
        });
    });

    Async.it('should pull the decrypted data from the database.', () => {
        return collection.fetch({
            search : {
                where : {
                    response : '1'
                }
            }
        }).tap(() => {
            let model = collection.at(0);

            expect(model.get('id')).toEqual(data.id);
            expect(model.get('subject_id')).toEqual(data.subject_id);
            expect(model.get('question_id')).toEqual(data.question_id);
            expect(model.get('questionnaire_id')).toEqual(data.questionnaire_id);
            expect(model.get('response')).toEqual(data.response);
            expect(model.get('SW_Alias')).toEqual(data.SW_Alias);
            expect(model.get('dashboardId')).toEqual(data.dashboardId);
        });
    });

});
