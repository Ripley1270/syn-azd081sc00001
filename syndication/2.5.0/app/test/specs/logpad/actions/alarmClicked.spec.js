import { alarmClicked } from 'logpad/actions/alarmClicked';

describe('alarmClicked', () => {
    const alarmTrue = {
        alarmConfig: {
            wasClicked: true
        }
    };

    const alarmFalse = {
        alarmConfig: {
            wasClicked: false
        }
    };

    const alarmNoVal = {
        alarmConfig: {}
    };

    Async.it('should return true if alarmConfig.wasClicked is true', () => {
        return alarmClicked(alarmTrue).then((wasClicked) => {
            expect(wasClicked).toBeTruthy();
        });
    });

    Async.it('should return false if alarmConfig.wasClicked is false', () => {
        return alarmClicked(alarmFalse).then((wasClicked) => {
            expect(wasClicked).toBeFalsy();
        });
    });

    Async.it('should return false if alarmConfig.wasClicked is not defined', () => {
        return alarmClicked(alarmNoVal).then((wasClicked) => {
            expect(wasClicked).toBeFalsy();
        });
    });
});