// The test is disabled so let's disable the eslint rules as wellas
/* eslint-disable */ 
xdescribe('TransmitAll', function () {

    beforeEach(function () {
        LF.webService = new LF.Class.WebService();
        LF.SpecHelpers.createSubject();
    });

    afterEach(function () {
        LF.webService = undefined;
        LF.Data = {};
        localStorage.clear();
    });

    it('should install the database.', function () {

        LF.SpecHelpers.installDatabase();
    });

    it('should save the transmission to the database.', function () {

        let transmission = new LF.Model.Transmission(),
            response;

        transmission.save({
            method  : 'resetSecretQuestion',
            params  : JSON.stringify({
                secret_question : 0,
                secret_answer : 'abc'
            }),
            created : new Date().getTime()
        }, {
            onSuccess : function (res) {
                response = res;
            }
        });

        waitsFor(function () {
            return response;
        }, 'the model to save.', 1000);

        runs(function () {
            expect(response).toEqual(1);
        });

    });

    it('should have one transmission in the database', function () {

        let response,
            collection = new LF.Collection.Transmissions();

        collection.count({
            onSuccess : function (res) {
                response = res;
            }
        });

        waitsFor(function () {
            return response;
        }, 'the action to complete.', 2000);

        runs(function () {
            expect(response).toEqual(1);
        });

    });

    it('should run the transmitAll action.', function () {

        let num,
            response,
            setupCode = LF.Data.Subjects.at(0).get('subject_id');

        fakeAjax({registrations: [{
                url         : '../../api/v1/devices/',
                type        : 'PUT',
                success     : {
                    data    : {
                        W : 'apple'
                    },
                    status  : 'success',
                    xhr     : {
                        getResponseHeader: (id) => {
                            switch (id) {
                                case 'X-Device-Status':
                                    return '1';
                                case 'X-Sync-ID':
                                    return '13';
                                default:
                                    return '';
                            }
                        },
                        status : 200
                    }
                }
            }]
        });

        LF.Actions.transmitAll(null, function () {

            let collection = new LF.Collection.Transmissions();

            collection.count({
                onSuccess : function (res) {
                    response = !res;
                    num = res;
                }
            });

        });

        waitsFor(function () {
            return response;
        }, 'the action to complete.', 2000);

        runs(function () {
            expect(response).toBeTruthy();
            expect(num).toEqual(0);
        });

    });

    it('should uninstall the database.', function () {

        LF.SpecHelpers.uninstallDatabase();
    });

});
