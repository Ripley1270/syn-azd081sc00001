import CodeEntryView from 'logpad/views/CodeEntryView';
import * as helpers from 'core/Helpers';
import Logger from 'core/Logger';
import COOL from 'core/COOL';

// Get all the symbols
import 'trainer/logpad';
import Trainer from 'trainer/logpad/Trainer';
import ELF from 'core/ELF';
import Uninstall from 'core/actions/uninstall';
import navigateTo from 'core/actions/navigateTo';
import TimeTravel from 'core/TimeTravel';
import Rules from 'logpad/resources/Rules.js';
import ConfirmView from 'core/views/ConfirmView';
import { currentSpecCapturer } from 'test/helpers/JasmineWrappers';
import setupCoreMessages from 'core/resources/Messages';
import { MessageRepo } from 'core/Notify';
import TrainerLanguageSelectionView from 'trainer/logpad/TrainerLanguageSelectionView';
import CurrentContext from 'core/CurrentContext';

import '../../helpers/StudyDesign';

describe('Trainer', () => {
    let view,
        template = `
             <div id="codeEntry-template">
                 <div class="input-textbox">
                     <input type="text" id="txtStudy" name="txtStudy_{{ key }}" />
                     <input type="text" id="txtCode" name="txtCode_{{ key }}" />
                 </div>
                 <button disabled type="submit" id="submitNext" />
             </div>
        `;

    $('body').append(template);

    localStorage.clear();

    if (!document.styleSheets.length) {
        // Force at least one style sheet so things that touch document.styleSheets aren't null,
        // like the trainer stuff
        let style = document.createElement('style');

        // WebKit hack :(
        style.appendChild(document.createTextNode(''));

        // Add the <style> element to the page
        document.head.appendChild(style);
    }

    beforeAll(() => {
        setupCoreMessages();

        // JasmineWrappers clears rules, create a dummy one that gets manipulated by Trainer
        ELF.rules.add({
            id: 'Termination',
            expresion: true,
            resolve: [{ action: 'defaultAction' }]
        });

        CurrentContext.init();
        CurrentContext().setContextLanguage('en-US');

        view = COOL.new('CodeEntryView', CodeEntryView);
    });

    afterAll(() => MessageRepo.clear());

    Async.beforeEach(() => {
        LF.security = { pauseSessionTimeOut: $.noop };
        LF.TimeTravel = TimeTravel;

        return view.render()
        .then(() => {
            view.$txtCode = view.$('#txtCode');
            view.$txtStudy = view.$('#txtStudy');
            view.$txtStudy.val('');
            view.$txtCode.val('');
        });
    });

    describe('non trainer activation', () => {
        Async.it('should not enter trainer mode if "trainer" is not given as the study name/url.', () => {
            spyOn(Trainer, 'trainerMode').and.callThrough();
            spyOn(view, 'checkSubjectIsActive').and.callFake(() => Q(true));

            view.$txtCode.val('12345678');
            view.$txtStudy.val('http://someserver/somestudy');
            return view.submit({ preventDefault: $.noop })
            .then(() => {
                expect(Trainer.trainerMode).not.toHaveBeenCalledWith(true);

                // Make sure this NOT got set
                expect(localStorage.getItem('trainer')).toBeFalsy();

                // Make sure this NOT GOT defined
                let ttRule = ELF.rules.find('TrainerTermination');
                expect(ttRule).toBeFalsy('should not find TrainerTerminationRule');

                // Make sure this NOT got removed
                let tRule = ELF.rules.find('Termination');
                expect(tRule).toBeDefined('should find regular TerminationRule');
            });
        });
    });

    describe('activation', () => {
        Async.it('should try to enter trainer mode if "trainer" is given as the study name/url.', () => {
            spyOn(Trainer, 'trainerMode').and.callThrough();
            spyOn(TrainerLanguageSelectionView.prototype, 'initialize').and.stub();

            spyOn(LF.TimeTravel, 'displayTimeTravel').and.callFake(() => true);
            spyOn(view, 'checkSubjectIsActive').and.callFake(() => Q());
            spyOn(view, 'navigate').and.callFake($.noop);

            view.$txtStudy.val('trainer');
            view.$txtCode.val('12345678');

            return view.submit({ preventDefault: $.noop })
            .tap(() => {
                expect(Trainer.trainerMode).toHaveBeenCalledWith(true);

                // Make sure this got set
                expect(localStorage.getItem('trainer')).toBeTruthy('trainer should have been set');
                expect(LF.TimeTravel.displayTimeTravel).toHaveBeenCalledWith(true);

                // Make sure this got defined
                let ttRule = ELF.rules.find('TrainerTermination');
                expect(ttRule).toBeTruthy('should find trainer termination rule');

                // Make sure this got removed
                let tRule = ELF.rules.find('Termination');
                expect(tRule).toBeFalsy('shuld not find regular Termination rule');
            });
        });
    });

    // At this point, 'trainer' is defined, but not finalized because we're not going through the rest of the
    // setup screens. But there is a real chance a user will back out and we need to know the whole system
    // starts over.
    describe('deactivation', () => {
        Async.it('should try to re-start the system in a clean state', () => {
            // this would kill Karma
            spyOn(Trainer, 'restartApplication').and.callFake(() => true);
            spyOn(MessageRepo, 'display').and.resolve(true);
            MessageRepo.add({
                key: 'TRAINER_EXIT_NOTIFICATION',
                message: $.noop,
                type: 'Dialog'
            });

            return Trainer.trainerMode(false)
            .tap(() => {
                expect(Trainer.restartApplication).toHaveBeenCalled();

                // Make sure this got cleared
                expect(localStorage.getItem('trainer')).toBeFalsy();
            });
        });
    });
});
