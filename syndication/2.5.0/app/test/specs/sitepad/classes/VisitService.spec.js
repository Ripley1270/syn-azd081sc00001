import VisitService from 'sitepad/classes/VisitService';
import Subject from 'core/models/Subject';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

describe('VisitService', () => {
    beforeEach(() => {
        resetStudyDesign();

        LF.StudyDesign.studyProtocol = {
            defaultProtocol: 0
        };
    });

    TRACE_MATRIX('DE23136')
    .describe('method:getProtocol', () => {
        it('should return the default phase', () => {
            let subject = new Subject({ custom10: '' });
            let protocol = VisitService.getProtocol(subject);

            expect(protocol).toBe('0');
        });

        it('should return the custom10 value.', () => {
            let subject = new Subject({ custom10: '2' });
            let protocol = VisitService.getProtocol(subject);

            expect(protocol).toBe('2');
        });
    });
});
