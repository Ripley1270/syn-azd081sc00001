import Payload from 'core/classes/Payload';
import SitePadPayload from 'sitepad/classes/SitePadPayload';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

class SitePadPayloadSuite {
    constructor () {
        beforeEach(() => {
            resetStudyDesign();
        });
    }

    testQuestionnaire () {
        describe('method:questionnaire', () => {
            it('should format the questionnaire payload.', () => {
                spyOn(Payload, 'questionnaire').and.returnValue({ payload: {} });
                let form = {
                    krpt: '1a2b3c4d5e',
                    responsibleParty: 'SysAdmin'
                };

                let { payload } = SitePadPayload.questionnaire(form);

                expect(payload.K).toBe(form.krpt);
                expect(payload.M).toBe(form.responsibleParty);
            });
        });
    }

    testSPStartDate () {
        describe('method:sPStartDate', () => {
            let form;

            beforeEach(() => {
                form = {
                    krpt: '321312412ed31fg12y1',
                    ResponsibleParty: 'SysAdmin',
                    dateStarted: '2012-07-25T17:39:43Z',
                    dateCompleted: '2012-07-25T17:42:43Z',
                    reportDate: '2012-07-25',
                    phaseStartDate: -4,
                    batteryLevel: '89',
                    sigID: 'SA.1232131231231232131',
                    SPStartDate: '2012-07-25'
                }
            });

            it('should format the SPStartDate payload.', () => {
                let payload = SitePadPayload.sPStartDate(form);

                expect(payload.K).toBe(form.krpt);
                expect(payload.M).toBe(form.ResponsibleParty);
                expect(payload.U).toBe('Assignment');
                expect(payload.S).toBe(form.dateStarted);
                expect(payload.C).toBe(form.dateCompleted);
                expect(payload.R).toBe(form.reportDate);
                expect(payload.P).toBe(form.subjectAssignmentPhase);
                
                // These two values are pulled from the study design.
                expect(payload.E).toBeDefined();
                expect(payload.V).toBeDefined();

                expect(payload.T).toBe(form.phaseStartDate);
                expect(payload.L).toBe(form.batteryLevel);
                expect(payload.J).toBe(form.sigID);

                expect(payload.A.length).toBe(2);
                expect(payload.A[0]).toEqual({
                    G: '1',
                    F: 'SPStartDateAffidavit',
                    Q: 'AFFIDAVIT'
                });
                expect(payload.A[1]).toEqual({
                    G: form.SPStartDate,
                    F: 'PT.SPStartDate',
                    Q: ''
                });
            });
        });
    }
}

describe('SitePadPayload', () => {
    let suite = new SitePadPayloadSuite();

    suite.testQuestionnaire();
    suite.testSPStartDate();
});