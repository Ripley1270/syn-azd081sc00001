import COOL from 'core/COOL';
import * as Utilities from 'core/utilities';
import WebService from 'core/classes/WebService';
import Subject from 'core/models/Subject';
import Logger from 'core/Logger';
import DeviceSetupSyncStatus from 'sitepad/classes/DeviceSetupSyncStatus';

TRACE_MATRIX('US8476')
.describe('DeviceSetupSyncStatus', () => {
    Async.it('should connect to internet and set the flag to true).', () => {
        spyOn(WebService.prototype, 'getInternetConnectionStatus').and.resolve();

        let request = DeviceSetupSyncStatus.syncStatusHandler();

        expect(request).toBePromise();

        return request
        .then(() => {
            expect(WebService.prototype.getInternetConnectionStatus).toHaveBeenCalled();
            expect(DeviceSetupSyncStatus.internetConnected === true);
            expect(DeviceSetupSyncStatus.dataTransmitted === false);
        });
    });

    Async.it('should fail to connect to internet.', () => {
        spyOn(WebService.prototype, 'getInternetConnectionStatus').and.reject('404');
        spyOn(Logger.prototype, 'error').and.stub();

        let request = DeviceSetupSyncStatus.syncStatusHandler();

        expect(request).toBePromise();

        return request
        .then(() => {
            expect(Logger.prototype.error).toHaveBeenCalledWith('internet connection check failed');

            expect(WebService.prototype.getInternetConnectionStatus).toHaveBeenCalled();
            expect(DeviceSetupSyncStatus.internetConnected === false);
            expect(DeviceSetupSyncStatus.dataTransmitted === false);
        });
    });
});
