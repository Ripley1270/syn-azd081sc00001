import Session from 'sitepad/classes/Session';
//import CoreState from 'core/classes/State';
import State from 'sitepad/classes/State';
import { getNested } from 'core/utilities';

// globals LF: true
describe('Sitepad Session', () => {
    // eslint-disable-next-line no-global-assign, no-native-reassign
    LF = LF || {};
    let testSession, originalLF, mockDate;

    let setFakeTimeouts = (session, fakeNow, ...timeouts) => {
        let fakeNowTs = fakeNow.getTime();
        let sessionTimeout = session.sessionTimeout || 17;
        let questionnaireTimeout = getNested('LF.StudyDesign.questionnaireTimeout') || 1000000000;

        let lastCheck = fakeNowTs - 100;
        let lastActivity = fakeNowTs - 200;
        let startQuestionnaire = fakeNowTs - 300;

        if (timeouts.indexOf('activity') !== -1) {
            lastActivity = lastActivity - sessionTimeout;
        }
        session.set('Last_Active', lastActivity);

        if (timeouts.indexOf('check') !== -1) {
            lastCheck = lastCheck - sessionTimeout;
        }
        session.set('Timeout_LastCheck', lastCheck);

        if (timeouts.indexOf('questionnaire') !== -1) {
            // the questionnaire timeout is recorded in MINUTES
            startQuestionnaire = startQuestionnaire - (questionnaireTimeout * 60 * 1000);
        }
        session.questionnaireStartTime = startQuestionnaire;
    };

    beforeEach(() => {
        originalLF = {};
        Object.keys(LF).forEach((key) => {
            originalLF[key] = LF[key];
        });

        mockDate = new Date();

        // fake the context
        LF.router = jasmine.createSpyObj(['view', 'navigate']);
        LF.router.view.and.returnValue('theView');
        LF.router.controller = {};
        LF.router.controller.login = jasmine.createSpy('LF.router.controller.login');
        LF.StudyDesign = LF.StudyDesign || {};
        LF.StudyDesign.questionnaireTimeout = 17;
        LF.appName = 'LogPad App';
    });

    afterEach(() => {
        Object.keys(originalLF).forEach((key) => {
            LF[key] = originalLF[key];
        });
    });

    describe('checkTimeOut (session timeout)', () => {
        beforeEach(() => {
            spyOn(Backbone.history, 'loadUrl').and.stub();
            testSession = new Session();
            testSession.startSessionTimeOut();
        });

        describe('timing out sitepad site selection', () => {
            beforeEach(() => {
                LF.appName = 'SitePad App';
                spyOn(State, 'set');
                spyOn(Session.prototype, 'remove');
                testSession.startSessionTimeOut();
            });

            it('removes localstorage variables in state SITE_SELECTION', () => {
                spyOn(State, 'get').and.returnValue(State.states.siteSelection);
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(Session.prototype.remove.calls.count()).toBe(4);
                expect(Session.prototype.remove.calls.allArgs()).toEqual([['environment'], ['mode'], ['studyDbName'], ['serviceBase']]);
                expect(State.set).toHaveBeenCalledWith(State.states.new);
                expect(LF.router.navigate).toHaveBeenCalledWith('modeSelection', true);
            });
        });

        describe('timing out sitepad user setup', () => {
            beforeEach(() => {
                LF.appName = 'SitePad App';
                spyOn(State, 'set');
                testSession.startSessionTimeOut();
            });

            it('returns the user to the language selection screen from state LOCKED', () => {
                spyOn(State, 'get').and.returnValue(State.states.locked);
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(State.set).toHaveBeenCalledWith(State.states.languageSelection);
                expect(LF.router.navigate).toHaveBeenCalledWith('languageSelection', true);
            });

            it('returns the user to the language selection screen from state SETUP_USER', () => {
                spyOn(State, 'get').and.returnValue(State.states.setupUser);
                spyOn(ELF, 'trigger').and.resolve();
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(ELF.trigger).toHaveBeenCalledWith('QUESTIONNAIRE:SessionTimeout/First_Site_User', jasmine.any(Object), jasmine.any(String));
            });

            it('returns the user to the language selection screen from state LANGUAGE_SELECTION', () => {
                spyOn(State, 'get').and.returnValue(State.states.languageSelection);
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(State.set).toHaveBeenCalledWith(State.states.languageSelection);
                expect(LF.router.navigate).toHaveBeenCalledWith('languageSelection', true);
            });

            it('returns the user to the language selection screen from state SET_TIME_ZONE', () => {
                spyOn(State, 'get').and.returnValue(State.states.setTimeZone);
                setFakeTimeouts(testSession, mockDate, 'activity', 'check');
                testSession.checkTimeOut();
                expect(State.set).toHaveBeenCalledWith(State.states.languageSelection);
                expect(LF.router.navigate).toHaveBeenCalledWith('languageSelection', true);
            });
        });
    });
});
