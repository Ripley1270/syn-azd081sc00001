import PageView from 'core/views/PageView';
import Spinner from 'core/Spinner';
import State from 'sitepad/classes/State';
import Users from 'core/collections/Users';
import skipOrDisplayFirstSiteUser from 'sitepad/actions/skipOrDisplayFirstSiteUser';
import { isAdminUser } from 'sitepad/actions/skipOrDisplayFirstSiteUser';
import COOL from 'core/COOL';
import 'test/helpers/StudyDesign';
import * as sync from 'core/actions/syncUsers';
import * as specHelpers from 'test/helpers/SpecHelpers';
import * as lStorage from 'core/lStorage';
import EULA from 'core/classes/EULA';

TRACE_MATRIX('US6600').
describe('skipOrDisplayFirstSiteUser', () => {
    let toBeFetched,
        page = new PageView(),
        users = new Users(),
        adminUser = {
            userType: 'SiteUser',
            username: 'Thor',
            language: 'en-US',
            password: '36e368ca5df356e82efa7887d9176848',
            salt: '1e321604-d830-480e-9f34-298178f27c50',
            syncValue: 'DOM.287206.246',
            userId: 1,
            secretQuestion: '0',
            secretAnswer: 'hammer',
            active: 1,
            role: 'admin'
        },
        siteUser = {
            userType: 'SiteUser',
            username: 'Hulk',
            language: 'en-US',
            password: '36e368ca5df356e82efa7887d9176848',
            salt: '1e321604-d830-480e-9f34-298178f27c50',
            syncValue: 'DOM.287206.246',
            userId: 2,
            secretQuestion: '0',
            secretAnswer: 'green',
            active: 1,
            role: 'site'
        };

    beforeAll(() => {
        spyOn(Spinner, 'show').and.resolve();
        spyOn(Spinner, 'hide').and.resolve();
        spyOn(COOL.getClass('Utilities'), 'isOnline').and.resolve(true);
        spyOn(Users.prototype, 'filter').and.callThrough();
        spyOn(sync, 'syncUsers').and.callFake(() => {
            return Q.Promise(resolve => {
                resolve();
            });
        });
        spyOn(Users.prototype, 'fetch').and.callFake(function () {
            return Q.Promise(resolve => {
                this.add(toBeFetched);
                resolve();
            });
        });
    });

    beforeEach(() => {
        users.reset();
        toBeFetched = [];
    });

    afterEach(() => localStorage.clear());

    Async.it('should show spinner', () => {
        return skipOrDisplayFirstSiteUser.call(page)
        .then(() => {
            expect(Spinner.show).toHaveBeenCalled();
        });
    });

    Async.it('should sync User Data', () => {
        return skipOrDisplayFirstSiteUser.call(page)
        .then(() => {
            expect(sync.syncUsers).toHaveBeenCalled();
        });
    });

    Async.it('should fetch synced User Data', () => {
        return skipOrDisplayFirstSiteUser.call(page)
        .then(() => {
            expect(Users.prototype.fetch).toHaveBeenCalled();
        });
    });

    Async.it('should filter fetched users with the default filter function', () => {
        return skipOrDisplayFirstSiteUser.call(page)
        .then(() => {
            expect(Users.prototype.filter).toHaveBeenCalledWith(isAdminUser);
        });
    });

    Async.it('should hide spinner', () => {
        return skipOrDisplayFirstSiteUser.call(page)
        .then(() => {
            expect(Spinner.hide).toHaveBeenCalled();
        });
    });

    Async.it('should show First Site User Creation page when there are no users', () => {
        spyOn(State, 'set');
        spyOn(PageView.prototype, 'navigate');

        return skipOrDisplayFirstSiteUser.call(page)
        .then(() => {
            expect(State.set).toHaveBeenCalledWith(State.states.endUserLicenseAgreements);
            expect(PageView.prototype.navigate).toHaveBeenCalledWith('endUserLicenseAgreements');
        });
    });

    Async.it('should show EULA if there are no users with admin role', () => {
        spyOn(State, 'set');
        spyOn(PageView.prototype, 'navigate');

        toBeFetched.push(siteUser);
        return skipOrDisplayFirstSiteUser.call(page)
        .then(() => {
            expect(State.set).toHaveBeenCalledWith(State.states.endUserLicenseAgreements);
            expect(PageView.prototype.navigate).toHaveBeenCalledWith('endUserLicenseAgreements');
        });
    });

    Async.it('should show EULA page if there are no active users with admin role', () => {
        let data =  _.extend(_.clone(adminUser), {
            active: 0
        });

        toBeFetched.push(data);

        spyOn(State, 'set');
        spyOn(PageView.prototype, 'navigate');

        return skipOrDisplayFirstSiteUser.call(page)
        .then(() => {
            expect(State.set).toHaveBeenCalledWith(State.states.endUserLicenseAgreements);
            expect(PageView.prototype.navigate).toHaveBeenCalledWith('endUserLicenseAgreements');
        });
    });

    Async.it('should show login page if there exists an active user with admin role', () => {
        EULA.accept();
        spyOn(State, 'set');
        spyOn(PageView.prototype, 'navigate');

        toBeFetched.push(adminUser);

        return skipOrDisplayFirstSiteUser.call(page)
        .then(() => {
            expect(State.set).toHaveBeenCalledWith(State.states.activated);
            expect(PageView.prototype.navigate).toHaveBeenCalledWith('summaryScreen');
        });
    });

    Async.it('should show login page if adminUserFilter configuration states that admin user exists', () => {
        let adminUserFilter = LF.StudyDesign.sitePad.adminUserFilter;

        LF.StudyDesign.sitePad.adminUserFilter = (user) => user.get('username') === 'Hulk';

        EULA.accept();
        spyOn(State, 'set');
        spyOn(PageView.prototype, 'navigate');

        toBeFetched.push(siteUser);

        return skipOrDisplayFirstSiteUser.call(page)
        .then(() => {
            expect(State.set).toHaveBeenCalledWith(State.states.activated);
            expect(PageView.prototype.navigate).toHaveBeenCalledWith('summaryScreen');
        })
        .finally(() => {
            LF.StudyDesign.sitePad.adminUserFilter = adminUserFilter;
        });
    });

    Async.it('should show EULA page if adminUserFilter configuration states that admin user does not exists', () => {
        let adminUserFilter = LF.StudyDesign.sitePad.adminUserFilter;

        LF.StudyDesign.sitePad.adminUserFilter = (user) => user.get('username') === 'He-Man';

        spyOn(State, 'set');
        spyOn(PageView.prototype, 'navigate');

        toBeFetched.push(siteUser);

        return skipOrDisplayFirstSiteUser.call(page)
        .then(() => {
            expect(State.set).toHaveBeenCalledWith(State.states.endUserLicenseAgreements);
            expect(PageView.prototype.navigate).toHaveBeenCalledWith('endUserLicenseAgreements');
        })
        .finally(() => {
            LF.StudyDesign.sitePad.adminUserFilter = adminUserFilter;
        });
    });

    Async.it('should show First Site User Creation page if admin user does not exists and EULA has been accepted', () => {
        EULA.accept();
        spyOn(State, 'set');
        spyOn(PageView.prototype, 'navigate');

        toBeFetched.push(siteUser);
        return skipOrDisplayFirstSiteUser.call(page)
        .then(() => {
            expect(State.set).toHaveBeenCalledWith(State.states.setupUser);
            expect(PageView.prototype.navigate).toHaveBeenCalledWith('setupUser');
        });
    });
});
