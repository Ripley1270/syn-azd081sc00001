import transmitUserEdit from 'sitepad/transmit/transmitUserEdit';
import Transmission from 'core/models/Transmission';
import User from 'core/models/User';
import UserSync from 'core/classes/UserSync';
import Logger from 'core/Logger';
import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';
import ELF from 'core/ELF';

import { resetStudyDesign } from 'test/helpers/StudyDesign';

describe('transmitUserEdit', () => {
    let context,
        model,
        newService;

    beforeEach(() => {
        resetStudyDesign();

        context = jasmine.createSpyObj('context', ['remove', 'destroy']);
        context.destroy.and.callFake(() => Q());

        model = new Transmission({
            id: 1,
            params: JSON.stringify({ id: 2 }),
            created: new Date().getTime()
        });

        spyOn(User.prototype, 'fetch').and.callFake(function () {
            this.set({
                id: 2,
                username: 'System Administrator',
                role: 'site',
                language: 'en-US'
            });

            return Q();
        });

        spyOn(Logger.prototype, 'error').and.stub();
        spyOn(Logger.prototype, 'operational').and.stub();
        spyOn(Logger.prototype, 'info').and.stub();

        LF.StudyDesign.roles.reset([{
            id: 'site',
            lastDiaryRoleCode: 1,
            displayName: 'SITE_ADMINISTRATOR',
            permissions: ['ALL'],
            defaultAffidavit: 'DEFAULT',
            syncLevel: 'site',
            product: ['logpad', 'sitepad']
        }]);

        spyOn(WebService.prototype, 'updateUser');
    });

    Async.it('should fail to fetch the user.', () => {
        let expectedError = 'DatabaseError';
        User.prototype.fetch.and.reject('DatabaseError');

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.catch((err) => {
            expect(context.remove).toHaveBeenCalledWith(model);
            expect(err).toEqual(expectedError);
        });
    });

    Async.it('should not update the user (no syncLevel).', () => {
        LF.StudyDesign.roles.reset([{
            id: 'site',
            lastDiaryRoleCode: 1,
            displayName: 'SITE_ADMINISTRATOR',
            permissions: ['ALL'],
            defaultAffidavit: 'DEFAULT',
            product: ['logpad', 'sitepad']
        }]);

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.catch((err) => {
            let expectedError = 'User: System Administrator with role: site does not have a syncLevel.  Edits to the user were not transmitted to SW.';
            expect(err).toBe(expectedError);
            expect(Logger.prototype.operational).toHaveBeenCalledWith(expectedError);
            expect(context.destroy).toHaveBeenCalledWith(model.get('id'));
        });
    });

    Async.it('should fail to resolve the syncValue.', () => {
        let expectedError = 'UnknownError';

        spyOn(UserSync, 'getValue').and.reject(expectedError);

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.catch((error) => {
            expect(UserSync.getValue).toHaveBeenCalled();
            expect(error).toBe(expectedError);
            expect(Logger.prototype.error).toHaveBeenCalledWith('There was an error updating the user System Administrator with role site.', expectedError);
            expect(context.remove).toHaveBeenCalledWith(model);
        });
    });

    Async.it('should reject w/o a syncValue.', () => {
        spyOn(UserSync, 'getValue').and.resolve(null);

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.catch((error) => {
            let expectedError = 'User: System Administrator with role: site does not have a syncValue.  Edits to the user were not transmitted to SW.';
            expect(UserSync.getValue).toHaveBeenCalled();
            expect(error).toBe(expectedError);
            expect(Logger.prototype.operational).toHaveBeenCalledWith(expectedError);
            expect(context.destroy).toHaveBeenCalledWith(model.get('id'));
        });
    });

    Async.it('should fail to transmit the user update.', () => {
        spyOn(UserSync, 'getValue').and.resolve('DOM.12345');
        WebService.prototype.updateUser.and.reject('HTTPError');

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.catch((error) => {
            let expectedError = 'HTTPError';
            expect(WebService.prototype.updateUser).toHaveBeenCalled();
            expect(error).toBe(expectedError);
            expect(Logger.prototype.error).toHaveBeenCalledWith('There was an error updating the user System Administrator with role site.', expectedError);
            expect(context.remove).toHaveBeenCalledWith(model);
        });
    });

    Async.it('should resolve the update with "S".', () => {
        spyOn(UserSync, 'getValue').and.resolve('DOM.12345');
        WebService.prototype.updateUser.and.resolve({ res: 'S' });

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.then((returnValue) => {
            expect(WebService.prototype.updateUser).toHaveBeenCalled();
            expect(returnValue).toBe(true);
            expect(Logger.prototype.operational).toHaveBeenCalledWith('System Administrator was updated.');
            expect(context.destroy).toHaveBeenCalledWith(model.get('id'));
        });
    });

    Async.it('should resolve the update with "N".', () => {
        spyOn(UserSync, 'getValue').and.resolve('DOM.12345');
        spyOn(ELF, 'trigger').and.resolve();
        WebService.prototype.updateUser.and.resolve({ res: 'N' });

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.catch((error) => {
            let expectedError = 'System Administrator does not exist.';
            expect(WebService.prototype.updateUser).toHaveBeenCalled();
            expect(error).toBe(expectedError);
            expect(Logger.prototype.error).toHaveBeenCalledWith(expectedError);
            expect(context.destroy).toHaveBeenCalledWith(model.get('id'));
            expect(ELF.trigger).toHaveBeenCalledWith('TRANSMIT:Nonexistent/User', jasmine.any(Object), context);
        });
    });

    Async.it('should resolve the update with "E".', () => {
        spyOn(UserSync, 'getValue').and.resolve('DOM.12345');
        WebService.prototype.updateUser.and.resolve({ res: 'E' });

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.catch((error) => {
            let expectedError = 'There was an error updating the user System Administrator with role site.';
            expect(WebService.prototype.updateUser).toHaveBeenCalled();
            expect(error).toEqual(expectedError);
            expect(Logger.prototype.error).toHaveBeenCalledWith(expectedError);
            expect(context.remove).toHaveBeenCalledWith(model);
        });
    });

    Async.it('should resolve the update with "D".', () => {
        spyOn(UserSync, 'getValue').and.resolve('DOM.12345');
        WebService.prototype.updateUser.and.resolve({ res: 'D' });
        spyOn(ELF, 'trigger').and.resolve({ preventDefault: true });

        let request = transmitUserEdit.call(context, model);

        expect(request).toBePromise();

        return request.then((returnValue) => {
            expect(WebService.prototype.updateUser).toHaveBeenCalled();
            expect(returnValue).toBe(false);
            expect(ELF.trigger).toHaveBeenCalledWith('TRANSMIT:Duplicate/User', jasmine.any(Object), context);
            expect(Logger.prototype.info).toHaveBeenCalledWith('Transmit update user failed. User System Administrator with role site already exists.');
        });
    });
});
