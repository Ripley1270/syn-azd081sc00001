import TestConnectionView from 'sitepad/views/TestConnectionView';
import CurrentContext from 'core/CurrentContext';
import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

describe('TestConnectionView', () => {
    let view,
        removeTemplate,
        preventDefault = $.noop;

    Async.beforeEach(() => {
        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/test-connection.ejs');

        resetStudyDesign();

        CurrentContext.init();

        view = new TestConnectionView();

        return view.render();
    });

    afterEach(() => {
        removeTemplate();
    });

    it('should have an id.', () => {
        expect(view.id).toBe('test-connection-view');
    });

    describe('method:render', () => {
        Async.it('should fail to render.', () => {
            spyOn(view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));

            let request = view.render();
            expect(request).toBePromise();
            return request.then(() => fail('Method render should have been rejected.'))
                .catch(e => expect(e).toBe('DOMError'));
        });

        Async.it('should render the view.', () => {
            return view.render()
            .then(() => expect(view.$el.html()).toBeDefined());
        });
    });

    describe('method:showConnectionStatus', () => {
        const stringsToFetch = {
            success: 'CONNECT_SUCCESS',
            fail: 'CONNECT_FAIL'
        };

        beforeEach(() => {
            spyOn(view, 'i18n').and.callFake(strings => Q(strings));
        });

        Async.it('should display a success icon', () => {
            return view.showConnectionStatus(true)
            .then(() => {
                expect(view.i18n).toHaveBeenCalledWith(stringsToFetch);
                expect(view.$('#connectionBtn').html()).toEqual(stringsToFetch.success);
                expect(view.$('#connectionBtn').hasClass('btn-success')).toBeTruthy();
            });
        });

        Async.it('should display a failure icon', () => {
            return view.showConnectionStatus(false)
            .then(() => {
                expect(view.i18n).toHaveBeenCalledWith(stringsToFetch);
                expect(view.$('#connectionBtn').html()).toEqual(stringsToFetch.fail);
                expect(view.$('#connectionBtn').hasClass('btn-danger')).toBeTruthy();
            });
        });
    });

    describe('method:next', () => {
        beforeEach(() => {
            spyOn(view, 'navigate').and.stub();
        });

        Async.it('should navigate to the unlock code screen', () => {
            return view.render()
            .then(() => {
                view.next();
                expect(view.navigate).toHaveBeenCalledWith('unlockCode');
            });
        });
    });

    describe('method:back', () => {
        beforeEach(() => {
            spyOn(view, 'navigate').and.stub();
        });

        Async.it('should navigate to the language selection screen', () => {
            return view.render()
            .then(() => {
                view.back();
                expect(view.navigate).toHaveBeenCalledWith('languageSelection');
            });
        });
    });
});
