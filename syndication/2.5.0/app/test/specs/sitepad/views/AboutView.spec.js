import AboutView from 'sitepad/views/AboutView';
import * as lStorage from 'core/lStorage';
import * as DateTimeUtil from 'core/DateTimeUtil';
import Sites from 'core/collections/Sites';
import User from 'core/models/User';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';
import InfoView from 'sitepad/views/InfoView';

TRACE_MATRIX('US8508')
.describe('About View, Tablet/Web Redesign', () => {
    let view,
        removeTemplate,
        infoTemplate,
        userData = { id: 1, username: 'batman', language: 'en-US' };

    beforeEach(() => {
        resetStudyDesign();
        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/about.ejs');
        infoTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/info.ejs');

        LF.security = LF.security || {};
        LF.security.activeUser = new User(userData);

        spyOn(lStorage, 'getItem').and.callFake((key) => {
            switch (key) {
                case 'serviceBase':
                    return 'https://pht-syn-study01.phtnetpro.com';
                case 'deviceId':
                    return '675';
                case 'IMEI':
                    return '990000862471854';
                case 'lastRefreshTime':
                    return '2016-12-02T13:09:01.050';
                default:
                    return null;
            }
        });

        spyOn(Sites, 'fetchCollection').and.resolve(new Sites({ siteCode: '0008' }));

        spyOn(DateTimeUtil, 'getLocalizedDate').and.callFake(dt => 'Friday, December 02, 2016 08:09');

        view = new AboutView();
    });

    afterEach(() => {
        removeTemplate();
        infoTemplate();
    });

    TRACE_MATRIX('DE23682')
    .describe('DE23682 - Cursor Style for FAQ Headers', () => {
        Async.it('should set the FAQ headers to have a cursor:pointer style', () => {
            return view.render()
            .then(() => {
                const elems = view.$('#faq-container a[data-toggle="collapse"]');
                elems.each((i, elem) => {
                    expect(elem.classList).toContain('faq-header');
                });
            });
        });
    });

    TRACE_MATRIX('DE23714')
    .describe('method:constructInfoView', () => {
        it('returns instance of InfoView', () => {
            let i = view.constructInfoView();
            expect(i instanceof InfoView).toBe(true);
        });
    });

    describe('method:render', () => {
        it('should return a promise', () => {
            let p = view.render();
            expect(p).toBePromise();
        });

        Async.it('should render the InfoView', () => {
            return view.render()
            .then(() => {
                expect(view.$('.info-container').length).toEqual(1);
            });
        });
    });

    describe('events', () => {
        Async.beforeEach(() => {
            spyOn(view, 'navigate').and.callFake($.noop);
            return view.render();
        });

        describe('event:back', () => {
            it('should trigger back() when click the back arrow', () => {
                spyOn(view, 'back').and.callFake($.noop);
                view.delegateEvents();

                view.$('#back').trigger('click');

                expect(view.back).toHaveBeenCalled();
            });
        });

        TRACE_MATRIX('DE23718')
        .describe('event:addPatient', () => {
            it('should trigger addPatient when the Add Patient button is clicked', () => {
                spyOn(view, 'addPatient').and.callThrough();
                view.delegateEvents();

                view.$('#btn-add-patient').trigger('click');

                expect(view.addPatient).toHaveBeenCalled();
                expect(view.navigate).toHaveBeenCalledWith('home');
            });
        });

        TRACE_MATRIX('DE23718');
        describe('event:addUser', () => {
            it('should trigger addUser when the Add User button is clicked', () => {
                spyOn(view, 'addUser').and.callThrough();
                view.delegateEvents();

                view.$('#btn-add-user').trigger('click');

                expect(view.addUser).toHaveBeenCalled();
                expect(view.navigate).toHaveBeenCalledWith('home');
            });
        });

        describe('event:changeDeviceLanguage', () => {
            it('should trigger changeDeviceLanguage when clicking the "Change Device Language" button', () => {
                spyOn(view, 'changeDeviceLanguage').and.callThrough();
                view.delegateEvents();

                view.$('#btn-device-language').trigger('click');

                expect(view.changeDeviceLanguage).toHaveBeenCalled();
                expect(view.navigate).toHaveBeenCalledWith('site-users');
            });
        });

        describe('event:goHome', () => {
            it('should trigger goHome when clicking the Patient Language button', () => {
                spyOn(view, 'goHome').and.callThrough();
                view.delegateEvents();

                view.$('#btn-patient-language').trigger('click');

                expect(view.goHome).toHaveBeenCalled();
                expect(view.navigate).toHaveBeenCalledWith('home');
            });
        });

        describe('event:setTimeZone', () => {
            it('should trigger setTimezone when clicking the Set Time Zone button', () => {
                spyOn(view, 'setTimezone').and.callThrough();
                view.delegateEvents();

                view.$('#btn-timezone').trigger('click');

                expect(view.setTimezone).toHaveBeenCalled();
                expect(view.navigate).toHaveBeenCalledWith('set-time-zone');
            });
        });
    });
});

// Note: These tests have been moved to InfoView.spec.js
TRACE_MATRIX('US5955')
.xdescribe('AboutView', () => {
    let view,
        removeTemplate,
        na = 'N/A';

    beforeEach(() => {
        resetStudyDesign();
        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/about.ejs');

        spyOn(lStorage, 'getItem').and.callFake((key) => {
            switch (key) {
                case 'serviceBase':
                    return 'https://pht-syn-study01.phtnetpro.com';
                case 'deviceId':
                    return '675';
                case 'IMEI':
                    return '990000862471854';
                case 'lastRefreshTime':
                    return '2016-12-02T13:09:01.050';
                default:
                    return null;
            }
        });

        spyOn(Sites, 'fetchCollection').and.resolve(new Sites({ siteCode: '0008' }));

        spyOn(DateTimeUtil, 'getLocalizedDate').and.callFake(dt => 'Friday, December 02, 2016 08:09');

        view = new AboutView();

        spyOn(view, 'getPendingReportCount').and.resolve(2);
    });

    afterEach(() => removeTemplate());

    it('should have an id.', () => {
        expect(view.id).toBe('about');
    });

    it('should navigate to settings view.', () => {
        spyOn(view, 'navigate').and.stub();
        view.back();

        expect(view.navigate).toHaveBeenCalledWith('settings');
    });

    describe('method:render', () => {
        it('should return a promise.', () => {
            let request = view.render();

            expect(request).toBePromise();
        });

        Async.it('should fail to render the view when getTemplateParameters fails.', () => {
            spyOn(view, 'getTemplateParameters').and.reject('Unexpected error');

            return view.render()
            .then(() => fail('Method render should have been rejected.'))
            .catch(e => expect(e).toBe('Unexpected error'));
        });

        Async.it('should fail to render the view when buildHTML fails.', () => {
            spyOn(view, 'buildHTML').and.reject('DOMError');

            return view.render()
            .then(() => fail('Method render should have been rejected.'))
            .catch(e => expect(e).toBe('DOMError'));
        });

        Async.it('should render the view.', () => {
            return view.render()
            .then(() => expect(view.$el.html()).toBeDefined());
        });

        Async.it('should call buildHTML with the same parameters from getTemplateParameters', () => {
            spyOn(view, 'buildHTML').and.stub();

            return view.getTemplateParameters()
            .tap(() => view.render())
            .then(parameters => expect(view.buildHTML).toHaveBeenCalledWith(parameters, true));
        });
    });

    describe('method:getTemplateParameters', () => {
        it('should return a promise.', () => {
            let request = view.getTemplateParameters();

            expect(request).toBePromise();
        });

        Async.it('should return rejected promise when getBatteryLevel fails.', () => {
            spyOn(LF.Wrapper.Utils, 'getBatteryLevel').and.callFake(() => {
                throw new Error('getBatteryLevel Error');
            });

            return view.getTemplateParameters()
            .then(() => fail('Method getTemplateParameters should have been rejected.'))
            .catch(e => expect(e).toEqual(new Error('getBatteryLevel Error')));
        });

        Async.it('should return rejected promise when fetching Sites fails.', () => {
            Sites.fetchCollection.and.reject('DB Error');

            return view.getTemplateParameters()
            .then(() => fail('Method getTemplateParameters should have been rejected.'))
            .catch(e => expect(e).toBe('DB Error'));
        });

        Async.it('should return rejected promise when fetching the pending report count fails.', () => {
            view.getPendingReportCount.and.reject('DB Error');

            return view.getTemplateParameters()
            .then(() => fail('Method getTemplateParameters should have been rejected.'))
            .catch(e => expect(e).toBe('DB Error'));
        });

        Async.it('should return an object literal with all parameters required for the template.', () => {
            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.studyVersion).toBeDefined();
                expect(parameters.studyURL).toBeDefined();
                expect(parameters.deviceId).toBeDefined();
                expect(parameters.deviceSerialNumber).toBeDefined();
                expect(parameters.site).toBeDefined();
                expect(parameters.protocol).toBeDefined();
                expect(parameters.sponsor).toBeDefined();
                expect(parameters.storedReports).toBeDefined();
                expect(parameters.productVersion).toBeDefined();
                expect(parameters.batteryLevel).toBeDefined();
                expect(parameters.lastRefreshTime).toBeDefined();
            });
        });

        Async.it('should return the correct studyVersion.', () => {
            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.studyVersion).toEqual(LF.StudyDesign.studyVersion);
            });
        });

        Async.it('should return the correct studyURL.', () => {
            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.studyURL).toEqual('https://pht-syn-study01.phtnetpro.com');
            });
        });

        Async.it('should return the correct studyURL if there is no value set in localStorage.', () => {
            lStorage.getItem.and.callFake(() => null);

            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.studyURL).toEqual(LF.apiBase);
            });
        });

        Async.it('should return the correct deviceId.', () => {
            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.deviceId).toEqual('675');
            });
        });

        Async.it('should return the correct deviceId if there is no value set in localStorage.', () => {
            lStorage.getItem.and.callFake(() => null);

            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.deviceId).toEqual(na);
            });
        });

        Async.it('should return the correct deviceSerialNumber.', () => {
            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.deviceSerialNumber).toEqual('990000862471854');
            });
        });

        Async.it('should return the correct deviceSerialNumber if there is no value set in localStorage.', () => {
            lStorage.getItem.and.callFake(() => null);

            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.deviceSerialNumber).toEqual(na);
            });
        });

        Async.it('should return the correct site.', () => {
            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.site).toEqual('0008');
            });
        });

        Async.it('should return the correct protocol.', () => {
            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.protocol).toEqual('TestStudy');
            });
        });

        Async.it('should return the correct sponsor.', () => {
            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.sponsor).toEqual(LF.StudyDesign.clientName);
            });
        });

        Async.it('should return the correct storedReports.', () => {
            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.storedReports).toEqual(2);
            });
        });

        Async.it('should return the correct productVersion.', () => {
            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.productVersion).toEqual(LF.coreVersion);
            });
        });

        Async.it('should return the correct batteryLevel.', () => {
            spyOn(LF.Wrapper.Utils, 'getBatteryLevel').and.callFake(callback => callback(80));

            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.batteryLevel).toEqual(80);
            });
        });

        Async.it('should return the correct batteryLevel when batteryLevel is not available.', () => {
            spyOn(LF.Wrapper.Utils, 'getBatteryLevel').and.callFake(callback => callback(null));

            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.batteryLevel).toEqual(na);
            });
        });

        Async.it('should return lastRefreshTime as localized date.', () => {
            return view.getTemplateParameters()
            .then((parameters) => {
                expect(DateTimeUtil.getLocalizedDate).toHaveBeenCalledWith(new Date('2016-12-02T13:09:01.050Z'), { includeTime: true, useShortFormat: false });
                expect(parameters.lastRefreshTime).toEqual('Friday, December 02, 2016 08:09');
            });
        });

        Async.it('should return the correct lastRefreshTime when there is no value set in localStorage.', () => {
            lStorage.getItem.and.callFake(() => null);

            return view.getTemplateParameters()
            .then((parameters) => {
                expect(parameters.lastRefreshTime).toEqual(na);
            });
        });
    });
});
