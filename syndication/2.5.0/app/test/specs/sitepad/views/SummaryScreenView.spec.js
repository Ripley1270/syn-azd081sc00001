import SummaryScreenView from 'sitepad/views/SummaryScreenView';
import * as lStorage from 'core/lStorage';
import Session from 'core/classes/Session';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';
import PageViewSuite from 'test/specs/core/views/PageView.specBase';
import DeviceSetupSyncStatus from 'sitepad/classes/DeviceSetupSyncStatus';
import Templates from 'core/collections/Templates';
import State from 'sitepad/classes/State';

class SummaryScreenViewSuite extends PageViewSuite {
    beforeAll () {
        return super.afterAll();
    }

    beforeEach () {
        resetStudyDesign();

        LF.security = new Session();

        this.removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/summary-screen.ejs');

        this.view = new SummaryScreenView();

        LF.templates = new Templates([
            {
                name: 'SitepadRegistrationWizardStepper',
                namespace: 'DEFAULT',
                template: `<div id="wizard-stepper" class="es-steps">
                        <ol>
                            <li>
                                <div id="language-step" class="es-step">1</div>
                                    <label>{{ language }}</label>
                            </li>
                            <li>
                                <div id="network-step" class="es-step">2</div>
                                    <label>{{ network }}</label>
                            </li>
                            <li>
                                <div id="unlock-step" class="es-step">3</div>
                                    <label>{{ unlockCode }}</label>
                            </li>
                            <li>
                                <div id="time-zone-step" class="es-step">4</div>
                                    <label>{{ timeZone }}</label>
                            </li>
                            <li>
                                <div id="eula-step" class="es-step">5</div>
                                    <label>{{ eula }}</label>
                            </li>
                            <li>
                                <div id="add-user-step" class="es-step">6</div>
                                    <label>{{ addUser }}</label>
                            </li>
                            <li>
                                <div id="summary-step" class="es-step">7</div>
                                    <label>{{ summary }}</label>
                            </li>
                        </ol>
                    </div>`
            }
        ]);

        lStorage.setItem('language', 'en');
        lStorage.setItem('locale', 'US');
        lStorage.setItem('selectedTZName', 'America/New York');

        LF.strings.add({
            namespace: 'CORE',
            language: 'en',
            locale: 'US',
            localized: 'English (US)',
            direction: 'ltr',
            resources: {
            }
        });

        spyOn(DeviceSetupSyncStatus, 'syncStatusHandler').and.callFake(() => {
            DeviceSetupSyncStatus.internetConnected = true;
            DeviceSetupSyncStatus.dataTransmitted = true;
        });

        return this.view.resolve()
        .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();
        lStorage.removeItem('language');
        lStorage.removeItem('locale');
        lStorage.removeItem('selectedTZName');
        LF.templates = undefined;
        DeviceSetupSyncStatus.internetConnected = undefined;
        DeviceSetupSyncStatus.dataTransmitted = undefined;
        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(SummaryScreenViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should render the view.', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html()).toBeDefined();
                });
            });

            Async.it('should fail to render the view.', () => {
                spyOn(this.view, 'buildHTML').and.reject('DOMError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request
                .then(() => fail('Method render should have been rejected.'))
                .catch((e) => {
                    expect(e).toBe('DOMError');
                });
            });

            Async.it('should render with networkConnection and dataTransmission set to green checks.', () => {
                spyOn(this.view, 'buildHTML').and.callThrough();

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html()).toBeDefined();
                    expect(this.view.buildHTML).toHaveBeenCalledWith({ networkConnectionVal: '<div class="col-sm-12"><span class="fa fa-check-circle"></span></div>', dataTransmissionVal: '<div class="col-sm-12"><span class="fa fa-check-circle"></span></div>', languageVal: 'English (US)', timeZoneVal: 'America/New York', siteAdministratorVal: '' }, true);
                });
            });
        });
    }

    testNextHandler () {
        describe('method:nextHandler', () => {
            Async.it('should navigate to login view and set state to activated.', () => {
                spyOn(this.view, 'navigate');
                let request = this.view.nextHandler({ preventDefault: $.noop });

                expect(request).toBePromise();

                return request.then(() => {
                    expect(State.get()).toEqual(State.states.activated);
                    expect(this.view.navigate).toHaveBeenCalledWith('login');
                });
            });
        });
    }

    testHelp () {
        describe('method:settings', () => {
            it('should navigate to settings.', () => {
                spyOn(this.view, 'navigate');
                this.view.settings();
                expect(this.view.navigate).toHaveBeenCalledWith('settingsInfo');
            });
        });
    }
}

TRACE_MATRIX('US8477').describe('SummaryScreenView', () => {
    let suite = new SummaryScreenViewSuite();

    suite.executeAll({
        id: 'summary-screen-view',
        template: '#summary-screen-tpl',
        button: '#next',
        dynamicStrings: {
            networkConnectionVal: '',
            dataTransmissionVal: '',
            languageVal: 'English (US)',
            timeZoneVal: 'America/New York',
            siteAdministratorVal: ''
        },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',

            // SummaryScreenView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testInputSuccess',
            'testInputError',
            'testShowInputError',
            'testGetValidParent',

            'testHideKeyboardOnEnter'
        ]
    });
});
