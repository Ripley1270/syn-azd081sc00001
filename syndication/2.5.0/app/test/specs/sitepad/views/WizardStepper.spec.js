import WizardStepper from 'sitepad/views/WizardStepper';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import Templates from 'core/collections/Templates';
import * as lStorage from 'core/lStorage';

TRACE_MATRIX('US8473').
describe('WizardStepper', () => {
    let view;

    LF.templates = new Templates([
        {
            name: 'SitepadRegistrationWizardStepper',
            namespace: 'DEFAULT',
            template: `<div id="wizard-stepper" class="es-steps">
                        <ol>
                            <li>
                                <div id="language-step" class="es-step">1</div>
                                    <label>{{ language }}</label>
                            </li>
                            <li>
                                <div id="network-step" class="es-step">2</div>
                                    <label>{{ network }}</label>
                            </li>
                            <li>
                                <div id="unlock-step" class="es-step">3</div>
                                    <label>{{ unlockCode }}</label>
                            </li>
                            <li>
                                <div id="time-zone-step" class="es-step">4</div>
                                    <label>{{ timeZone }}</label>
                            </li>
                            <li>
                                <div id="eula-step" class="es-step">5</div>
                                    <label>{{ eula }}</label>
                            </li>
                            <li>
                                <div id="add-user-step" class="es-step">6</div>
                                    <label>{{ addUser }}</label>
                            </li>
                            <li>
                                <div id="summary-step" class="es-step">7</div>
                                    <label>{{ summary }}</label>
                            </li>
                        </ol>
                    </div>`
        }
    ]);

    Async.afterEach(() => {
        $('#wizard-stepper').remove();
        return Q();
    });

    it('should have an id.', () => {
        view = new WizardStepper();
        expect(view.id).toBe('wizard-stepper-view');
    });

    Async.it('should map view to div element when viewID is provided', () => {
        view = new WizardStepper({ viewId: 'unlock-code-view' });
        let request = view.render();

        spyOn(view, 'getStepperElementFromViewId').and.callThrough();

        expect(request).toBePromise();

        return request.then(() => {
            expect(view.getStepperElementFromViewId).toHaveBeenCalledWith('unlock-code-view');
        });
    });

    Async.it('should gray out EULA step when EULA is accepted', () => {
        lStorage.setItem('EULAAccepted', true);
        view = new WizardStepper({ viewId: 'language-selection-view' });
        let request = view.render();

        spyOn(view.$el, 'find').and.callThrough();

        expect(request).toBePromise();

        return request.then(() => {
            lStorage.setItem('EULAAccepted', false);
            expect(view.$el.find).toHaveBeenCalledWith('#eula-step');
        });
    });
});
