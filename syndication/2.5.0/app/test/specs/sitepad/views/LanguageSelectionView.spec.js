import LanguageSelectionView from 'sitepad/views/LanguageSelectionView';
import * as lStorage from 'core/lStorage';
import CurrentContext from 'core/CurrentContext';
import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

describe('LanguageSelectionView', () => {
    let view,
        strings,
        removeTemplate,
        removeTemplateSel2,
        preventDefault = $.noop,
        language = 'hr',
        locale = 'HR';

    beforeEach(() => {
        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/language-selection.ejs');
        removeTemplateSel2 = specHelpers.renderTemplateToDOM('sitepad/templates/language-login-option.ejs');

        resetStudyDesign();
        LF.Preferred.language = language;
        LF.Preferred.locale = locale;

        CurrentContext.init();

        strings = LF.strings;

        LF.strings.add([{
            namespace: 'CORE',
            language: 'en',
            locale: 'GB',
            localized: 'English (UK)',
            direction: 'ltr',
            resources: {
                LANGUAGE: 'Language (en-GB)',
                NO_RESULTS_FOUND: 'No results found. (en-GB)',
                SELECT_SITE_NUMBER: 'Please select site number (en-GB)',
                USERNAME: 'Username (en-GB)',
                UNLOCK_CODE: 'Unlock Code (en-GB)',
                SELECT_LANGUAGE: 'Please select a preferred language for the site. (en-GB)',
                NEXT: 'Next (en-GB)'
            }
        }, {
            namespace: 'CORE',
            language: 'hr',
            locale: 'HR',
            localized: 'Hrvatski (Hrvatska)',
            direction: 'ltr',
            resources: {
                LANGUAGE: 'Hrvatski (Hrvatska)',
                NO_RESULTS_FOUND: 'No results found. (hr-HR)',
                SELECT_SITE_NUMBER: 'Please select site number (hr-HR)',
                USERNAME: 'Username (hr-HR)',
                UNLOCK_CODE: 'Unlock Code (hr-HR)',
                SELECT_LANGUAGE: 'Please select a preferred language for the site. (hr-HR)',
                NEXT: 'Sljede?e',
                BACK: 'Natrag'
            }
        }, {
            namespace: 'CORE',
            language: 'ja',
            locale: 'JP',
            localized: '??? (??)',
            direction: 'ltr',
            resources: {
                LANGUAGE: '??? (??)',
                NO_RESULTS_FOUND: 'No results found. (ja-JP)',
                SELECT_SITE_NUMBER: 'Please select site number (ja-JP)',
                USERNAME: 'Username (ja-JP)',
                UNLOCK_CODE: 'Unlock Code (ja-JP)',
                SELECT_LANGUAGE: 'Please select a preferred language for the site. (ja-JP)',
                NEXT: '??',
                BACK: '??'
            }
        }]);

        view = new LanguageSelectionView();
    });

    afterEach(() => {
        removeTemplate();
        removeTemplateSel2();
    });

    it('should have an id.', () => {
        expect(view.id).toBe('language-selection-view');
    });

    describe('method:render', () => {
        Async.it('should fail to render.', () => {
            spyOn(view, 'buildHTML').and.callFake(() => Q.reject('DOMError'));
            let request = view.render();
            expect(request).toBePromise();
            return request.then(() => fail('Method render should have been rejected.'))
                .catch(e => expect(e).toBe('DOMError'));
        });

        Async.it('should render the view.', () => {
            return view.render()
            .then(() => expect(view.$el.html()).toBeDefined());
        });
    });


    describe('method:changeLanguage', () => {
        Async.it('should set the language w/evt.', () => {
            return view.render()
            .then(() => {
                view.$language.val('en-GB');
                spyOn(view, 'render');
            })
            .then(() => view.changeLanguage({ preventDefault }))
            .then(() => {
                expect(LF.Preferred.language).toBe('en');
                expect(LF.Preferred.locale).toBe('GB');
                expect(view.render).toHaveBeenCalled();
            });
        });

        Async.it('should set the language (ja-JP w/o evt).', () => {
            return view.render()
                .then(() => {
                    view.$language.val('ja-JP');
                    spyOn(view, 'render');
                })
                .then(() => view.changeLanguage({ preventDefault }))
                .then(() => {
                    expect(LF.Preferred.language).toBe('ja');
                    expect(LF.Preferred.locale).toBe('JP');
                    expect(view.render).toHaveBeenCalled();
                });
        });
    });

    describe('method:next', () => {
        beforeEach(() => {
            spyOn(view, 'navigate').and.stub();
        });

        Async.it('should navigate to the test connection screen', () => {
            return view.render()
            .then(() => {
                spyOn(lStorage, 'getItem').and.returnValue('provision');
                spyOn(lStorage, 'setItem').and.stub();
                view.next();
                expect(lStorage.setItem).toHaveBeenCalledWith('language', 'hr');
                expect(lStorage.setItem).toHaveBeenCalledWith('locale', 'HR');
                expect(view.navigate).toHaveBeenCalledWith('testConnection');
            });
        });

        Async.it('should navigate to the site selection screen.', () => {
            return view.render()
            .then(() => {
                spyOn(lStorage, 'getItem').and.returnValue('depot');
                spyOn(lStorage, 'setItem').and.stub();
                view.next();
                expect(lStorage.setItem).toHaveBeenCalledWith('language', 'hr');
                expect(lStorage.setItem).toHaveBeenCalledWith('locale', 'HR');
                expect(view.navigate).toHaveBeenCalledWith('siteSelection');
            });
        });
    });

    describe('method:settings', () => {
        it('should navigate to the settings screen.', () => {
            spyOn(view, 'navigate').and.stub();

            view.settings();

            expect(view.navigate).toHaveBeenCalledWith('settingsInfo');
        });
    });
});
