function ActivationBaseMixin (superclass) {
    return class extends superclass {
        /**
         * Tests the settings method.
         */
        testSettings () {
            describe('method:settings', () => {
                it('should navigate to the settings screen.', () => {
                    spyOn(this.view, 'navigate').and.stub();

                    this.view.settings();

                    expect(this.view.navigate).toHaveBeenCalledWith('settingsInfo');
                });
            });
        }
    };
}

export default ActivationBaseMixin;
