import SiteSelectionView from 'sitepad/views/SiteSelectionView.js';
import Site from 'core/models/Site';
import WebService from 'core/classes/WebService';
import * as lStorage from 'core/lStorage';
import * as specHelpers from 'test/helpers/SpecHelpers';
import Logger from 'core/Logger';
import ELF from 'core/ELF';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import { MessageRepo } from 'core/Notify';
import setupSitepadMessages from 'sitepad/resources/Messages';
import * as FileUtil from 'core/FileUtil';
import COOL from 'core/COOL';

import PageViewSuite from 'test/specs/core/views/PageView.specBase';

let preventDefault = $.noop;

class SiteSelectionViewSuite extends PageViewSuite {
    beforeAll () {
        setupSitepadMessages();
        localStorage.clear();

        return super.beforeAll();
    }

    afterAll () {
        MessageRepo.clear();
        localStorage.clear();

        return super.afterAll();
    }

    beforeEach () {
        resetStudyDesign();
        this.removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/site-selection.ejs');

        lStorage.setItem('mode', 'provision');

        this.view = new SiteSelectionView();

        spyOn(this.view.spinner, 'show').and.resolve();
        spyOn(this.view.spinner, 'hide').and.resolve();
        spyOn(WebService.prototype, 'getSites').and.resolve({
            res: [{
                krdom: 1,
                siteCode: '001',
                studyName: 'DemoStudy',
                hash: '0x12345'
            }]
        });

        return this.view.resolve()
        .then(() => this.view.render())
        .then(() => super.beforeEach());
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(SiteSelectionViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    testRenderSites () {
        describe('method:renderSites', () => {
            it('should add each site to the UI.', () => {
                spyOn(this.view, 'addSite').and.stub();

                this.view.sites.reset();
                this.view.renderSites([
                    { siteCode: '001' },
                    { siteCode: '002' }
                ]);

                expect(this.view.addSite.calls.count()).toBe(2);
                expect(this.view.addSite).toHaveBeenCalledWith(jasmine.any(Site));
            });
        });
    }

    testAddSite () {
        describe('method:addSite', () => {
            it('should add the site to the site list.', () => {
                let model = new Site({ siteCode: '9001' });

                this.view.addSite(model);

                // IT'S OVER 9000!
                expect(this.view.$site.find(':last-child').val()).toBe('9001');
            });
        });
    }

    testBackHandler () {
        describe('method:backHandler', () => {
            it('should invoke the back method w/ evt.', () => {
                spyOn(this.view, 'back').and.resolve();

                this.view.backHandler({ preventDefault });

                expect(this.view.back).toHaveBeenCalled();
            });

            it('should invoke the back method w/o evt.', () => {
                spyOn(this.view, 'back').and.resolve();

                this.view.backHandler();

                expect(this.view.back).toHaveBeenCalled();
            });
        });
    }

    testBack () {
        describe('method:back', () => {
            beforeEach(() => {
                spyOn(ELF, 'trigger').and.resolve();
                spyOn(this.view, 'navigate').and.stub();
            });

            Async.it('should navigate back to the mode selection view (provision).', () => {
                this.view.mode = 'provision';

                let request = this.view.back();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('modeSelection');
                });
            });

            Async.it('should navigate back to the mode selection view (trainer).', () => {
                this.view.mode = 'trainer';

                let request = this.view.back();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('modeSelection');
                });
            });

            Async.it('should navigate back to the language selection view.', () => {
                this.view.mode = 'depot';

                let request = this.view.back();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.navigate).toHaveBeenCalledWith('languageSelection');
                });
            });
        });
    }

    testSiteChanged () {
        describe('method:siteChanged', () => {
            beforeEach(() => {
                this.view.$unlockCode.val('2375659298');
                spyOn(this.view, 'validate').and.stub();
            });

            it('should clear the unlock code value and disable the input.', () => {
                spyOn(this.view, 'disableElement');

                this.view.$site.val('');
                this.view.siteChanged();

                expect(this.view.$unlockCode.val()).toEqual('');
                expect(this.view.disableElement).toHaveBeenCalled();
            });

            it('should clear the unlock code and enable the input.', () => {
                spyOn(this.view, 'enableElement');

                this.view.$site.val('001');
                this.view.siteChanged();

                expect(this.view.$unlockCode.val()).toEqual('');
                expect(this.view.enableElement).toHaveBeenCalled();
            });
        });
    }

    testValidate () {
        describe('method:validate', () => {
            beforeEach(() => {
                this.view.studyDbName = 'engcorestudy05';
            });

            it('should verify that next button gets enabled when site and unlock code are entered', () => {
                let spy = spyOn(this.view, 'enableButton');

                this.view.$site.val('001');
                this.view.$unlockCode.val('4238629325');

                this.view.validate({ preventDefault });

                expect(spy).toHaveBeenCalled();
            });

            it('should verify that next button is disabled when there is no selection made', () => {
                let spy = spyOn(this.view, 'disableButton');

                this.view.validate({ preventDefault });

                expect(spy).toHaveBeenCalled();
            });

            it('should verify that next button is disabled when only site is selected', () => {
                let spy = spyOn(this.view, 'disableButton');

                this.view.$site.val('001');

                this.view.validate({ preventDefault });

                expect(spy).toHaveBeenCalled();
            });
        });
    }

    testNextHandler () {
        describe('method:nextHandler', () => {
            it('should invoke the next method w/ evt.', () => {
                spyOn(this.view, 'next').and.resolve();

                this.view.nextHandler({ preventDefault });

                expect(this.view.next).toHaveBeenCalled();
            });

            it('should invoke the next method w/o evt.', () => {
                spyOn(this.view, 'next').and.resolve();

                this.view.nextHandler();

                expect(this.view.next).toHaveBeenCalled();
            });
        });
    }

    testNext () {
        TRACE_MATRIX('DE17445')
        .describe('method:next', () => {
            let Utilities;
            beforeEach(() => {
                Utilities = COOL.getClass('Utilities');

                spyOn(Utilities, 'isOnline').and.resolve(true);
                spyOn(Logger.prototype, 'error').and.stub();
                spyOn(Logger.prototype, 'warn').and.stub();
                spyOn(MessageRepo, 'display').and.resolve();

                LF.Wrapper.platform = undefined;
                LF.Wrapper.isWrapped = false;
                localStorage.removeItem('isWrapped');
            });

            Async.it('should fail to display the spinner.', () => {
                this.view.spinner.show.and.reject('DOMError');
                let request = this.view.next();

                expect(request).toBePromise();

                return request
                .then(() => {
                    expect(MessageRepo.display).toHaveBeenCalled();
                });
            });

            Async.it('should display a connectivity notification (offline).', () => {
                spyOn(this.view, 'i18n').and.callFake(strings => Q(strings));
                Utilities.isOnline.and.resolve(false);

                let request = this.view.next();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(MessageRepo.display).toHaveBeenCalled();
                    expect(Logger.prototype.warn).toHaveBeenCalledWith('An internet connection is required to register the device.');
                });
            });

            Async.it('should display a connectivity notification (rejected).', () => {
                spyOn(this.view, 'i18n').and.callFake(strings => Q(strings));
                Utilities.isOnline.and.reject('DeviceError');

                let request = this.view.next();

                expect(request).toBePromise();

                return request
                .then(() => {
                    expect(Logger.prototype.error).toHaveBeenCalledWith('Device registration failed.', 'DeviceError');
                });
            });

            Async.it('should fail to register the device.', () => {
                this.view.studyDbName = 'engcorestudy05';
                this.view.$site.val('001');
                this.view.$unlockCode.val('4238629325');

                spyOn(WebService.prototype, 'registerDevice').and.reject('ServiceError');
                spyOn(lStorage, 'setItem').and.stub();

                return this.view.next()
                .then(() => {
                    expect(lStorage.setItem).toHaveBeenCalled();
                    expect(WebService.prototype.registerDevice).toHaveBeenCalledWith({
                        studyName: 'engcorestudy05',
                        krdom: 1,
                        startupCode: '4238629325',
                        deviceUuid: jasmine.any(String),
                        imei: 'NULL'
                    });
                    expect(MessageRepo.display).toHaveBeenCalledWith(MessageRepo.Banner.DEVICE_REGISTRATION_FAILURE);
                });
            });

            Async.it('should register the device with serial number if wrapped by android.', () => {
                this.view.studyDbName = 'engcorestudy05';
                this.view.$site.val('001');
                this.view.$unlockCode.val('4238629325');

                spyOn(WebService.prototype, 'registerDevice').and.resolve();
                spyOn(lStorage, 'setItem').and.stub();

                window.plugins = {};
                window.plugins.IMEIPlugin = jasmine.createSpyObj('IMEIPlugin', ['get']);
                window.plugins.IMEIPlugin.get = (callback, errorHandler) => {
                    callback('0123456789098765');
                };

                LF.Wrapper.savePlatform('android');
                return this.view.next()
                .then(() => {
                    expect(lStorage.setItem).toHaveBeenCalled();
                    expect(WebService.prototype.registerDevice).toHaveBeenCalledWith({
                        studyName: 'engcorestudy05',
                        krdom: 1,
                        startupCode: '4238629325',
                        deviceUuid: jasmine.any(String),
                        imei: '0123456789098765'
                    });
                });
            });

            Async.it('should register the device with \'NULL\' if not wrapped.', () => {
                this.view.studyDbName = 'engcorestudy05';
                this.view.$site.val('001');
                this.view.$unlockCode.val('4238629325');

                spyOn(WebService.prototype, 'registerDevice').and.resolve();
                spyOn(lStorage, 'setItem').and.stub();

                return this.view.next()
                .then(() => {
                    expect(lStorage.setItem).toHaveBeenCalled();
                    expect(WebService.prototype.registerDevice).toHaveBeenCalledWith({
                        studyName: 'engcorestudy05',
                        krdom: 1,
                        startupCode: '4238629325',
                        deviceUuid: jasmine.any(String),
                        imei: 'NULL'
                    });
                });
            });

            Async.it('should fail to create the site.', () => {
                this.view.studyDbName = 'engcorestudy05';
                this.view.$site.val('001');
                this.view.$unlockCode.val('4238629325');
                this.view.mode = 'provision';

                spyOn(Site.prototype, 'save').and.reject('DatabaseError');
                spyOn(WebService.prototype, 'registerDevice').and.resolve({ res: { apiToken: '12345' } });

                let request = this.view.next();

                expect(request).toBePromise();

                return request
                .then(() => {
                    expect(MessageRepo.display).toHaveBeenCalledWith(MessageRepo.Banner.DEVICE_REGISTRATION_FAILURE);
                    expect(Logger.prototype.error).toHaveBeenCalledWith('Device registration failed.', 'DatabaseError');
                });
            });

            Async.it('should navigate to the language selection view', () => {
                this.view.studyDbName = 'engcorestudy05';
                this.view.$site.val('001');
                this.view.$unlockCode.val('4238629325');
                this.view.mode = 'provision';

                spyOn(this.view, 'navigate').and.stub();
                spyOn(Site.prototype, 'save').and.resolve();
                spyOn(WebService.prototype, 'registerDevice').and.resolve({ res: { apiToken: '12345', regDeviceId: '987654321' } });
                spyOn(lStorage, 'setItem').and.stub();

                let request = this.view.next();

                expect(request).toBePromise();

                return request.then((e) => {
                    expect(this.view.navigate).toHaveBeenCalledWith('languageSelection');
                    expect(lStorage.setItem).toHaveBeenCalledWith('apiToken', '12345');
                    expect(lStorage.setItem).toHaveBeenCalledWith('deviceId', '987654321');
                    expect(WebService.prototype.registerDevice).toHaveBeenCalledWith({
                        studyName: 'engcorestudy05',
                        krdom: 1,
                        startupCode: '4238629325',
                        deviceUuid: jasmine.any(String),
                        imei: 'NULL'
                    });
                });
            });

            Async.it('should trigger REGISTRATION:SkipOrDisplayFirstSiteUser.', () => {
                this.view.studyDbName = 'engcorestudy05';
                this.view.$site.val('001');
                this.view.$unlockCode.val('4238629325');
                this.view.mode = 'depot';

                spyOn(ELF, 'trigger').and.stub();
                spyOn(Site.prototype, 'save').and.resolve();
                spyOn(WebService.prototype, 'registerDevice').and.resolve({ res: { apiToken: '12345', regDeviceId: '987654321' } });
                spyOn(lStorage, 'setItem').and.stub();

                let request = this.view.next();

                return request.then((e) => {
                    expect(ELF.trigger).toHaveBeenCalledWith('REGISTRATION:SkipOrDisplayFirstSiteUser', {}, this.view);
                    expect(lStorage.setItem).toHaveBeenCalledWith('apiToken', '12345');
                    expect(lStorage.setItem).toHaveBeenCalledWith('deviceId', '987654321');
                    expect(WebService.prototype.registerDevice).toHaveBeenCalledWith({
                        studyName: 'engcorestudy05',
                        krdom: 1,
                        startupCode: '4238629325',
                        deviceUuid: jasmine.any(String),
                        imei: 'NULL'
                    });
                });
            });
        });
    }

    // @todo This shouldn't be tested here.
    testBattery () {
        describe('check for serial number and battery level', () => {
            Async.it('should check serial number', () => {
                return FileUtil.readFileFromAppFolder('serial.txt').then((serial) => {
                    expect(serial).toBeNull();
                });
            });

            Async.it('should check battery level.', () => {
                let getBattery = Q.Promise((resolve) => {
                    LF.Wrapper.Utils.getBatteryLevel((batteryLevel) => {
                        resolve(batteryLevel);
                    });
                });

                return getBattery.then((level) => {
                    if (LF.Wrapper.platform === 'windows' || LF.Wrapper.platform === 'android') {
                        expect(level).not.toBeNull();
                    } else {
                        expect(level).toBeNull();
                    }
                });
            });
        });
    }
}

TRACE_MATRIX('US5986')
.describe('SiteSelectionView', () => {
    let suite = new SiteSelectionViewSuite();

    suite.executeAll({
        id: 'site-selection-view',
        template: '#site-selection-tpl',
        button: '#next',
        input: '#unlock-code',
        dynamicStrings: { study: 'Demo Study', key: 123456 },
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',

            // SiteSelectionView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testAddHelpText',
            'testRemoveHelpText',
            'testHideKeyboardOnEnter'
        ]
    });
});
