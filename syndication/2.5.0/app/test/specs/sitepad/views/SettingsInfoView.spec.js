import SettingsInfoView from 'sitepad/views/SettingsInfoView';
import InfoView from 'sitepad/views/InfoView';

import { resetStudyDesign } from 'test/helpers/StudyDesign';
import * as specHelpers from 'test/helpers/SpecHelpers';

TRACE_MATRIX('US8536');
describe('SettingsInfoView', () => {
    let view,
        removeTemplate,
        na = 'N/A';

    beforeEach(() => {
        resetStudyDesign();
        removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/settings-info.ejs');

        view = new SettingsInfoView();
    });

    afterEach(() => removeTemplate());

    it('should have an id.', () => {
        expect(view.id).toBe('settings-info');
    });

    it('should navigate to settings view.', () => {
        spyOn(Backbone.history.history, 'back').and.stub();
        view.back();

        expect(Backbone.history.history.back).toHaveBeenCalled();
    });

    describe('method:render', () => {
        beforeEach(() => {
            spyOn(InfoView.prototype, 'render').and.resolve();
        });
        it('should return a promise.', () => {
            let request = view.render();

            expect(request).toBePromise();
        });

        Async.it('should render the view.', () => {
            return view.render()
            .then(() => expect(view.$el.html()).toBeDefined());
        });

        Async.it('should append infoView to view', () => {
            spyOn(view, 'buildHTML').and.resolve();
            spyOn($.fn, 'append').and.stub();

            return view.render()
            .then(() => {
                expect(InfoView.prototype.render).toHaveBeenCalled();
                expect($.fn.append).toHaveBeenCalled();
            });
        });
    });
});
