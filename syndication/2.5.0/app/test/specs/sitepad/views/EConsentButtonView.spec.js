import EConsentButtonView from 'sitepad/views/EConsentButtonView';
import ELF from 'core/ELF';
import { resetStudyDesign } from 'test/helpers/StudyDesign';

class EConsentButtonViewSuite {
    constructor () {
        Async.beforeEach(() => this.beforeEach());
    }

    beforeEach () {
        resetStudyDesign();

        LF.StudyDesign.eConsent = { url: 'https://ert.com' };

        this.view = new EConsentButtonView();
        spyOn(this.view, 'i18n').and.resolve({ text: 'Open eConsent' });

        return this.view.render();
    }

    executeAll () {
        this.testId();
        this.testTagName();
        this.testClassName();
        this.testRender();
        this.testOpen();
        this.testClickHandler();
        this.testClick();
    }

    testId () {
        describe('property:id', () => {
            it('should have an id of econsent-btn', () => {
                expect(this.view.id).toBe('econsent-btn');
                expect(this.view.$el.attr('id')).toBe('econsent-btn');
            });
        });
    }

    testTagName () {
        describe('property:tagName', () => {
            it('should have a tagName of button', () => {
                expect(this.view.tagName).toBe('button');
                expect(this.view.$el[0].nodeName.toLowerCase()).toBe('button');
            });
        });
    }

    testClassName () {
        describe('property:className', () => {
            it('should have a className of "list-group-item no-action"', () => {
                expect(this.view.className).toBe('list-group-item no-action');
                expect(this.view.$el.attr('class')).toBe('list-group-item no-action');
            });
        });
    }

    testRender () {
        describe('method:render', () => {
            Async.it('should fail to render', () => {
                this.view.i18n.and.reject('TranslationError');

                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => fail('method:render should have been rejected'))
                .catch((err) => {
                    expect(err).toBe('TranslationError');
                });
            });

            Async.it('should render the button to it\'s virtual DOM', () => {
                let request = this.view.render();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(this.view.$el.html().indexOf('Open eConsent')).not.toBe(-1);
                });
            });
        });
    }

    testOpen () {
        TRACE_MATRIX('DE23701')
        .describe('method:open', () => {
            Async.it('should open a new browser window', () => {
                spyOn(LF.Wrapper, 'exec').and.callFake((input) => input.execWhenNotWrapped());
                spyOn(window, 'open').and.stub();

                let request = this.view.open();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(window.open).toHaveBeenCalledWith('https://ert.com', 'ECONSENT', 'location=true,resizable=true,scrollbars=true,status=true');
                });
            });

            Async.it('should open a native browser window.', () => {
                spyOn(LF.Wrapper, 'exec').and.callFake((input) => input.execWhenWrapped());
                window.cordova = { InAppBrowser: jasmine.createSpyObj('InAppBrowser', ['open']) };

                let request = this.view.open();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(window.cordova.InAppBrowser.open).toHaveBeenCalledWith('https://ert.com', '_blank', 'location=no,EnableViewPortScale=yes');
                });
            });
        });
    }

    testClickHandler () {
        describe('method:clickHandler', () => {
            it('should invoke the click method', () => {
                spyOn(this.view, 'click').and.resolve();

                this.view.clickHandler({ preventDefault: $.noop });

                expect(this.view.click).toHaveBeenCalled();
            });
        });
    }

    testClick () {
        describe('method:click', () => {
            Async.it('should trigger an ELF event', () => {
                spyOn(ELF, 'trigger').and.resolve();

                let request = this.view.click();

                expect(request).toBePromise();

                return request.then(() => {
                    expect(ELF.trigger).toHaveBeenCalledWith('ECONSENT:Open', jasmine.any(Object), this.view);
                });
            });
        });
    }
}

TRACE_MATRIX('US8524')
.describe('EConsentButtonView', () => {
    let suite = new EConsentButtonViewSuite();

    suite.executeAll();
});