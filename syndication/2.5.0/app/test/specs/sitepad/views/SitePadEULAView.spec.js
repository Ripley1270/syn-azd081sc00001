import SitePadEULAView from 'sitepad/views/SitePadEULAView';
import EULAView from 'core/views/EULAView';
import State from 'sitepad/classes/State';

import * as specHelpers from 'test/helpers/SpecHelpers';
import { resetStudyDesign } from 'test/helpers/StudyDesign';
import EULAViewTests from 'test/specs/core/views/EULAView.specBase';
import { mix } from 'core/utilities/languageExtensions';
import ActivationBaseMixin from './ActivationBaseMixin';

class SitePadEULAViewSuite extends mix(EULAViewTests).with(ActivationBaseMixin)  {
    beforeEach () {
        this.view = new SitePadEULAView();
        this.removeTemplate = specHelpers.renderTemplateToDOM('sitepad/templates/end-user-license.ejs');

        spyOn(State, 'set').and.stub();

        return super.beforeEach();
    }

    afterEach () {
        this.removeTemplate();

        return super.afterEach();
    }

    executeAll (context) {
        super.executeAll(context);

        let methods = Object.getOwnPropertyNames(SitePadEULAViewSuite.prototype);

        this.annotateAll(methods, context);
    }

    /**
     * Tests the settings method.
     */
    testSettings () {
        super.testSettings();
    }

    testNextChild () {
        describe('method:next (child)', () => {
            it('should trigger a rule.', () => {
                spyOn(EULAView.prototype, 'next').and.callThrough();
                spyOn(this.view, 'navigate').and.stub();
                spyOn(ELF, 'trigger').and.stub();

                this.view.next();

                expect(EULAView.prototype.next).toHaveBeenCalled();
                expect(ELF.trigger).toHaveBeenCalledWith('REGISTRATION:SkipOrDisplayFirstSiteUser', {}, this.view);
            });
        });
    }

    testBackChild () {
        describe('method:back (child)', () => {
            it('should navigate to the set timezone view.', () => {
                spyOn(EULAView.prototype, 'back').and.stub();
                spyOn(this.view, 'navigate').and.stub();

                this.view.back();

                expect(EULAView.prototype.back).toHaveBeenCalled();
                expect(State.set).toHaveBeenCalledWith(State.states.setTimeZone);
                expect(this.view.navigate).toHaveBeenCalledWith('setTimeZone');
            });
        });
    }
}

describe('SitePadEULAView', () => {
    let suite = new SitePadEULAViewSuite();

    suite.executeAll({
        id: 'end-user-license-view',
        template: '#end-user-license-template',
        button: '#back',
        exclude: [
            // Should be tested individually
            'testProperty',
            'testAttribute',
            // PageView has no class name.
            'testClass',

            // DOM structure of view does not allow for these tests to pass.
            'testClearInputState',
            'testValidateInput',
            'testAddHelpText',
            'testRemoveHelpText',
            'testGetValidParent',
            'testInputSuccess',
            'testInputError',
            'testHideKeyboardOnEnter'
        ]
    });
});
