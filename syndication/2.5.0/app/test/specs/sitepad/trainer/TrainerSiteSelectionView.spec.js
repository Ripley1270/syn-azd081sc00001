import COOL from 'core/COOL';
import * as trainerSiteSelectionView from 'trainer/sitepad/views/TrainerSiteSelectionView';

TRACE_MATRIX('US6732').
describe('TrainerSiteSelectionView', () => {
    let originalSiteSelectionView = COOL.getClass('SiteSelectionView');

    afterEach(() => {
        COOL.add('SiteSelectionView', originalSiteSelectionView);
    });

    it('Should override the view in COOL library.', () => {
        let SiteSelectionView = COOL.getClass('SiteSelectionView');
        expect(SiteSelectionView.name).toEqual('SiteSelectionView');

        trainerSiteSelectionView.extendSiteSelectionView();

        SiteSelectionView = COOL.getClass('SiteSelectionView');
        expect(SiteSelectionView.name).toEqual('TrainerSiteSelectionView');
    });

    it('Shoud validate a 9 digit number.', () => {
        trainerSiteSelectionView.extendSiteSelectionView();

        let SiteSelectionView = COOL.getClass('SiteSelectionView'),
            view = new SiteSelectionView();

        expect(view.validateUnlockCode('123456789')).toEqual(true);
        expect(view.validateUnlockCode('187236290')).toEqual(true);
        expect(view.validateUnlockCode('111111111')).toEqual(true);
        expect(view.validateUnlockCode('000000000')).toEqual(true);
    });

    it('Shoud not validate if not a 9 digit number.', () => {
        trainerSiteSelectionView.extendSiteSelectionView();

        let SiteSelectionView = COOL.getClass('SiteSelectionView'),
            view = new SiteSelectionView();

        expect(view.validateUnlockCode('')).toEqual(false);
        expect(view.validateUnlockCode('12345678')).toEqual(false);
        expect(view.validateUnlockCode('1234567890')).toEqual(false);
        expect(view.validateUnlockCode('12345678$')).toEqual(false);
        expect(view.validateUnlockCode('12345678A')).toEqual(false);
        expect(view.validateUnlockCode('ABCDEFGH')).toEqual(false);
        expect(view.validateUnlockCode('ABCDEFGHI')).toEqual(false);
        expect(view.validateUnlockCode('ABCDEFGHIJ')).toEqual(false);
    });
});
