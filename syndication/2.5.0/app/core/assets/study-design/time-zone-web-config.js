import TimeZone from './time-zone-config';

export default {
    web: {
        timeZoneOptions: TimeZone.core.timeZoneOptions
    }
};
