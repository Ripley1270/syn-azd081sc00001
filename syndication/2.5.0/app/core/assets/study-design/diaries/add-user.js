import getRoleInputType from 'core/widgets/param-functions/getRoleInputType';
import { MessageRepo } from 'core/Notify';
import COOL from 'core/COOL';
import Users from 'core/collections/Users';

export default {
    questionnaires: [{
        id: 'New_User',
        SU: 'New_User',
        displayName: 'NEW_USER',
        className: 'ADD_USER',
        affidavit: 'NewUserAffidavit',
        product: ['logpad'],
        screens: [
            'ADD_USER_S_1',
            'ADD_USER_S_2'
        ],
        branches: []
    }, {
        id: 'New_User_SitePad',
        SU: 'New_User',
        displayName: 'NEW_USER',
        className: 'ADD_USER',
        affidavit: 'NewUserAffidavit',
        product: ['sitepad'],
        screens: [
            'ADD_USER_S_1',
            'ADD_USER_S_2'
        ],
        branches: [],
        accessRoles: ['site', 'admin']
    }],

    screens: [{
        id: 'ADD_USER_S_1',
        className: 'ADD_USER_S_1',
        questions: [
            { id: 'ADD_USERNAME', mandatory: true },
            { id: 'ADD_USER_ROLE', mandatory: true },
            { id: 'ADD_USER_LANG', mandatory: true },
            { id: 'ADD_USER_PASSWORD', mandatory: true },
            { id: 'ADD_USER_PASSWORD_CONFIRM', mandatory: true },
            { id: 'ADD_USER_SALT', mandatory: true }
        ]
    }, {
        id: 'ADD_USER_S_2',
        className: 'ADD_USER_S_2',
        questions: [
            { id: 'ADD_USER_PASSWORD_MESSAGE', mandatory: false }
        ]
    }],

    questions: [{
        id: 'ADD_USERNAME',
        IG: 'Add_User',
        IT: 'ADD_USER_1',
        skipIT: '',
        title: '',
        text: [],
        className: 'ADD_USER',
        widget: {
            id: 'ADD_USER_W_1',
            type: 'AddUserTextBox',
            label: 'FULL_NAME',
            templates: {},
            answers: [],
            field: 'username',
            maxLength: 20,
            allowedKeyRegex: /[\w\W ]/,
            validateRegex: /[\w\W ]{3,}/,
            validation: {
                validationFunc: 'checkUserNameField',
                params: {
                    maxLength: 20,
                    minLength: 3
                }
            }
        }
    }, {
        id: 'ADD_USER_ROLE',
        IG: 'Add_User',
        IT: 'ADD_USER_2',
        skipIT: '',
        title: '',
        text: [],
        className: 'ROLE',
        widget: {
            id: 'ADD_USER_W_2',
            type: 'RoleSelectWidget',
            label: 'ROLES',
            field: 'role',
            templates: {},
            answers: [],
            params: {
                language: 'LANGUAGE',
                password: 'PASSWORD'
            },
            validation: {
                validationFunc: 'checkRoleFieldWidget',
                params: {
                    errorString: 'CONNECTION_REQUIRED'
                }
            }
        }
    }, {
        id: 'ADD_USER_LANG',
        IG: 'Add_User',
        IT: 'ADD_USER_3',
        skipIT: '',
        title: '',
        text: [],
        className: 'LANGUAGE',
        widget: {
            id: 'ADD_USER_W_3',
            type: 'LanguageSelectWidget',
            label: 'LANGUAGE',
            field: 'language',
            templates: {},
            answers: [],
            validation: {
                validationFunc: 'checkLanguageFieldWidget',
                params: {}
            }

        }
    }, {
        id: 'ADD_USER_PASSWORD',
        IG: 'Add_User',
        IT: 'ADD_USER_4',
        skipIT: '',
        title: '',
        text: [],
        className: 'PASSWORD',
        widget: {
            id: 'ADD_USER_W_4',
            type: 'TempPasswordTextBox',
            label: 'TEMP_PASSWORD',
            templates: {},
            answers: [],
            disabled: true,
            field: 'password',
            validationErrors: [{
                property: 'isValid',
                errorType: 'banner',
                errString: 'INVALID_ENTRY'
            }],
            validation: {
                validationFunc: 'checkPasswordFieldWidget',
                params: {}
            },
            getRoleInputType: () => {
                return getRoleInputType(LF.Data.Questionnaire.data.newUserRole);
            },
            configuration: {
                obfuscate: false
            }
        }
    }, {
        id: 'ADD_USER_PASSWORD_CONFIRM',
        IG: 'Add_User',
        IT: 'ADD_USER_5',
        skipIT: '',
        title: '',
        text: [],
        className: 'PASSWORD',
        widget: {
            id: 'ADD_USER_W_5',
            type: 'ConfirmationTextBox',
            label: 'CONFIRM_TEMP_PASS',
            templates: {},
            answers: [],
            disabled: true,
            field: 'confirm',
            fieldToConfirm: 'ADD_USER_W_4',
            validationErrors: [{
                property: 'isValid',
                errorType: 'banner',
                errString: 'INVALID_ENTRY'
            }],
            validation: {
                validationFunc: 'confirmFieldValue',
                params: {
                    errorString: 'PASSWORD_MISMATCH'
                }
            },
            getRoleInputType: () => {
                return getRoleInputType(LF.Data.Questionnaire.data.newUserRole);
            },
            configuration: {
                obfuscate: false
            }
        }
    }, {
        id: 'ADD_USER_PASSWORD_MESSAGE',
        IG: 'Add_User',
        IT: 'ADD_USER_6',
        skipIT: '',
        title: '',
        text: [],
        className: 'ADD_USER',
        widget: {
            id: 'ADD_USER_W_6',
            type: 'PasswordMessageWidget',
            templates: {},
            answers: [],
            text: {
                tempPasswordMessage: 'TEMPORARY_PASSWORD_MESSAGE',
                tempPassword: 'TEMP_PASSWORD',
                role: 'ROLES',
                language: 'LANGUAGE',
                userName: 'USER_NAME'
            }
        }
    }, {
        id: 'ADD_USER_SALT',
        IG: 'Add_User',
        IT: 'ADD_USER_SALT',
        className: 'ADD_USER_SALT',
        title: '',
        text: [],
        widget: {
            id: 'ADD_USER_W_7',
            type: 'HiddenField',
            value: 'temp',
            field: 'salt'
        }
    }],
    rules: [{
        id: 'New_User_RoleChange',

        // This rule is for BOTH Logpad and Sitepad
        trigger: [
            'QUESTIONNAIRE:Answered/New_User/ADD_USER_ROLE',
            'QUESTIONNAIRE:Displayed/New_User/ADD_USER_S_1',
            'QUESTIONNAIRE:Answered/New_User_SitePad/ADD_USER_ROLE',
            'QUESTIONNAIRE:Displayed/New_User_SitePad/ADD_USER_S_1'
        ],
        resolve: [{
            action () {
                return this.questionViews.filter((question) => {
                    return question.widgetModel.get('type') === 'ConfirmationTextBox' ||
                        question.widgetModel.get('type') === 'TempPasswordTextBox';
                }).forEach((widget) => {
                    return widget.render()
                    .then(() => {
                        let roleAnswer = this.answers.findWhere({ question_id: 'ADD_USER_ROLE' });

                        if (roleAnswer && JSON.parse(roleAnswer.get('response')).role !== '') {
                            this.$(`#${widget.id} input`).removeAttr('disabled readonly');
                        }
                    });
                });
            }
        }]
    }, {
        id: 'NewUserSave',

        // This rule is for BOTH Logpad and Sitepad
        trigger: [
            'QUESTIONNAIRE:Completed/New_User_SitePad',
            'QUESTIONNAIRE:Completed/New_User'
        ],
        salience: 100,
        resolve: [
            { action: 'newUserSave' },
            { action: 'updateCurrentContext' },

            // Prevent the global QUESTIONNAIRE:Completed rule from running
            // Prevent the default behavior that saves the diary as a dashboard record into database
            { action: 'preventAll' }
        ]
    }, {
        // Perform a user sync when AddNewUser Diary is opened.
        id: 'OpenNewUserSync',

        // This rule is ONLY for Logpad
        trigger: 'QUESTIONNAIRE:Open/New_User',
        resolve: [{
            action: (params, resume) => {
                LF.spinner.show()
                .then(() => {
                    const users = new Users();

                    // consider launching syncUsers().done() to do the sync asynchronously to the UI
                    // so the user could be filling it out while the sync was in place.   Sync is there to
                    // fetch users to do dup checks.
                    return users.syncUsers();
                })
                .finally(() => {
                    LF.spinner.hide();
                    resume();
                })
                .done();
            }
        }]
    }, {
        // If the subject navigates away from the Affidavit and is not online display a message.
        id: 'AddNewUserOnlineCheck',

        // This rule is ONLY for Logpad
        trigger: 'QUESTIONNAIRE:Navigate/New_User/AFFIDAVIT',
        salience: 100,
        evaluate (filter, resume) {
            let newUserRole = _(LF.StudyDesign.roles.models).findWhere({ id: this.data.newUserRole });

            COOL.getClass('Utilities').isOnline((isOnline) => {
                resume(!isOnline && !newUserRole.canAddOffline() && filter.direction !== 'previous');
            });
        },
        resolve: [{
            action: 'removeMessage'
        }, {
            action: (errorMessage) => {
                MessageRepo.display(MessageRepo.Banner[errorMessage]);
            },
            data: 'CONNECTION_REQUIRED'
        }],
        reject: [{ action: 'defaultAction' }]
    }, {
        id: 'NewUserDiaryTransmit',

        // This rule is ONLY for Sitepad
        trigger: 'QUESTIONNAIRE:Transmit/New_User_SitePad',
        salience: 100,
        resolve: [
            // If the device is online, transmit all queued transmissions and sync users.
            { action: () => ELF.trigger('NewUserSave:Transmit', {}, this) },
            { action: 'updateCurrentContext' },
            { action: 'launchGetRaterTraining' },

            // Prevent the global QUESTIONNAIRE:Transmit rule from running
            { action: () => ({ stopRules: true }) }
        ]
    }]
};
