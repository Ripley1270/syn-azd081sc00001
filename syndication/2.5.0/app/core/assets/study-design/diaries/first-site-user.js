import Data from 'core/Data';
import * as lStorage from 'core/lStorage';
import { MessageRepo } from 'core/Notify';
import notify from 'core/actions/notify';
import State from 'sitepad/classes/State';
import EULA from 'core/classes/EULA';
import getRoleInputType from 'core/widgets/param-functions/getRoleInputType';
import DeviceSetupSyncStatus from 'sitepad/classes/DeviceSetupSyncStatus';

export default {
    questionnaires: [{
        id: 'First_Site_User',
        SU: 'First_Site_User',
        displayName: 'FIRST_SITE_USER',
        className: 'FIRST_SITE_USER',
        affidavit: 'FirstUserSignatureAffidavit',
        product: ['sitepad', 'web'],
        screens: [
            'FIRST_SITE_USER_S_1'
        ],
        accessRoles: undefined,
        branches: []
    }],

    screens: [{
        id: 'FIRST_SITE_USER_S_1',
        className: 'FIRST_SITE_USER_S_1',
        questions: [
            { id: 'FIRST_USER_USERNAME', mandatory: true },
            { id: 'FIRST_USER_LANG', mandatory: true },
            { id: 'FIRST_USER_SECRET_QUESTION', mandatory: true },
            { id: 'FIRST_USER_SECRET_ANSWER', mandatory: true },
            { id: 'FIRST_USER_PASSWORD', mandatory: true },
            { id: 'FIRST_USER_PASSWORD_CONFIRM', mandatory: true },
            { id: 'FIRST_USER_ROLE', mandatory: true },
            { id: 'FIRST_USER_UNLOCK_CODE', mandatory: true }
        ]
    }],

    questions: [{
        id: 'FIRST_USER_USERNAME',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_USERNAME',
        skipIT: '',
        title: '',
        text: [],
        className: 'FIRST_SITE_USER',
        widget: {
            id: 'ADD_USER_W_1',
            type: 'AddUserTextBox',
            label: 'FULL_NAME',
            templates: {},
            answers: [],
            field: 'username',
            maxLength: 25,
            allowedKeyRegex: /[\w\W ]/,
            validateRegex: /[\w\W ]{3,}/,
            validation: {
                validationFunc: 'checkUserNameField',
                params: {
                    maxLength: 25,
                    minLength: 3
                }
            }
        }
    }, {
        id: 'FIRST_USER_LANG',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_LANG',
        skipIT: '',
        title: '',
        text: [],
        className: 'LANGUAGE',
        widget: {
            id: 'FIRST_USER_W_2',
            type: 'FirstSiteUserLanguageSelect',
            label: 'LANGUAGE',
            field: 'language',
            templates: {},
            answers: []
        }
    }, {
        id: 'FIRST_USER_SECRET_QUESTION',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_SECRET_QUESTION',
        skipIT: '',
        title: '',
        text: [],
        className: 'FIRST_USER',
        widget: {
            id: 'ADD_USER_W_3',
            type: 'SecretQuestionList',
            label: 'SECRET_QUESTION',
            field: 'secretQuestion',
            clearOnChange: ['ADD_USER_W_4'],
            templates: {},
            answers: []
        }
    }, {
        id: 'FIRST_USER_SECRET_ANSWER',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_SECRET_ANSWER',
        skipIT: '',
        title: '',
        text: [],
        className: 'FIRST_SITE_USER',
        widget: {
            id: 'ADD_USER_W_4',
            type: 'SecretAnswerTextBox',
            label: 'SECRET_ANSWER',
            templates: {},
            field: 'secretAnswer'
        }
    }, {
        id: 'FIRST_USER_PASSWORD',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_PASSWORD',
        skipIT: '',
        title: '',
        text: [],
        className: 'PASSWORD',
        widget: {
            id: 'FIRST_USER_W_5',
            type: 'TempPasswordTextBox',
            label: 'PASSWORD',
            templates: {
                input: 'PasswordTextBox'
            },
            answers: [],
            field: 'password',
            validationErrors: [{
                property: 'isValid',
                errorType: 'banner',
                errString: 'INVALID_ENTRY'
            }],
            validation: {
                validationFunc: 'checkPasswordFieldWidget',
                params: {
                }
            },
            getRoleInputType: () => {
                return getRoleInputType(LF.Data.Questionnaire.data.newUserRole);
            },
            configuration: {
                obfuscate: true
            }
        }
    }, {
        id: 'FIRST_USER_PASSWORD_CONFIRM',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_PASSWORD_CONFIRM',
        skipIT: '',
        title: '',
        text: [],
        className: 'PASSWORD',
        widget: {
            id: 'FIRST_USER_W_6',
            type: 'ConfirmationTextBox',
            label: 'CONFIRM_PASSWORD',
            templates: {
                input: 'PasswordTextBox'
            },
            answers: [],
            field: 'confirm',
            fieldToConfirm: 'FIRST_USER_W_5',
            validationErrors: [{
                property: 'isValid',
                errorType: 'banner',
                errString: 'INVALID_ENTRY'
            }],
            validation: {
                validationFunc: 'confirmFieldValue',
                params: {
                    errorString: 'PASSWORD_MISMATCH'
                }
            },
            getRoleInputType: () => {
                return getRoleInputType(LF.Data.Questionnaire.data.newUserRole);
            },
            configuration: {
                obfuscate: true
            }
        }
    }, {
        id: 'FIRST_USER_ROLE',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_ROLE',
        className: 'FIRST_SITE_USER',
        title: '',
        text: [],
        widget: {
            id: 'ADD_USER_W_7',
            type: 'HiddenField',
            value: 'admin',
            field: 'role'
        }
    }, {
        id: 'FIRST_USER_UNLOCK_CODE',
        IG: 'First_Site_User',
        IT: 'FIRST_USER_UNLOCK_CODE',
        className: 'FIRST_USER_UNLOCK_CODE',
        title: '',
        text: [],
        widget: {
            id: 'ADD_USER_W_8',
            label: 'STARTUP_UNLOCK_CODE_LBL',
            type: 'NumericTextBox',
            min: 1,
            max: 999999999999,
            step: 1
        }
    }],
    rules: [
        // FirstSiteUserSave
        {
            id: 'FirstSiteUserSave',
            trigger: 'QUESTIONNAIRE:Completed/First_Site_User',
            salience: 100,
            resolve: [
                {
                    action: () => {
                        $('#nextItem').attr('disabled', 'disabled');
                        lStorage.removeItem('isActivation');
                    }
                },
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'newUserSave' },
                { action: 'launchGetRaterTraining' },
                { action: 'setState', data: State.states.activated },
                { action: 'removeMessage' },
                { action: 'preventAll' }
            ]
        },

        // FirstSiteUserTransmit
        {
            id: 'FirstSiteUserTransmit',
            trigger: [
                'QUESTIONNAIRE:Transmit/First_Site_User',
                'FirstSiteUserQuestionnaireTimeout:Transmit'
            ],
            salience: 100,
            evaluate: ['AND', 'isOnline', 'isStudyOnline'],
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                { action: 'transmitAll' },
                {
                    action: () => {
                        DeviceSetupSyncStatus.internetConnected = true;
                        DeviceSetupSyncStatus.dataTransmitted = true;

                        // @TODO: This is a workaroud for the delay between transmission of a diary and the diary
                        // being saved in StudyWorks. A tech debt defect is filed for a better solution: DE16040
                        return Q.delay(1000);
                    }
                },
                { action: 'refreshAllData' },
                { action: 'transmitLogs' },
                { action: 'terminationCheck' },
                { action: 'updateCurrentContext' },
                { action: 'navigateTo', data: 'summaryScreen' },
                { action: 'preventAll' }
            ],
            reject: [
                {
                    action (input) {
                        if (!input.isOnline) {
                            // If the device is offline no need to display dialog or confirmation for retry.
                            // We support offline usage and this is not an unexpected situation.

                            DeviceSetupSyncStatus.internetConnected = false;
                            DeviceSetupSyncStatus.dataTransmitted = false;
                            return Q();
                        }

                        let { Dialog } = MessageRepo;
                        return notify({
                            dialog: Dialog && Dialog.TRANSMIT_RETRY_FAILED,
                            options: { httpRespCode: input.httpRespCode || '' }
                        })
                        .then(() => {
                            return DeviceSetupSyncStatus.syncStatusHandler();
                        });
                    }
                },
                { action: 'navigateTo', data: 'summaryScreen' },
                { action: 'preventAll' }
            ]
        },

        // FirstSiteUserBackout
        {
            id: 'FirstSiteUserBackout',
            trigger: 'QUESTIONNAIRE:BackOut/First_Site_User',
            salience: 2,
            evaluate: {
                expression: 'confirm',
                input: { key: 'BACK_OUT_CONFIRM' }
            },
            resolve: [
                {
                    action: (input, done) => {
                        if (EULA.isAccepted()) {
                            State.set(State.states.setTimeZone);
                            LF.router.navigate('setTimeZone', true);
                        } else {
                            State.set(State.states.endUserLicenseAgreements);
                            LF.router.navigate('endUserLicenseAgreements', true);
                        }
                        done();
                    }
                },
                { action: 'preventAll' }
            ],
            reject: [{ action: 'preventAll' }]
        },

        /*
         * FirstSiteUserQuestionnaireTimeout
         * If the questionnaire times out during First Site User creation and the affidavit has been answered,
         * save the questionnaire and go to login; Otherwise, navigate to language selection.
         */
        {
            id: 'FirstSiteUserQuestionnaireTimeout',
            trigger: [
                'QUESTIONNAIRE:QuestionnaireTimeout/First_Site_User',
                'QUESTIONNAIRE:SessionTimeout/First_Site_User'
            ],
            salience: 4,
            evaluate: 'isFirstSiteUserDataValid',
            resolve: [
                { action: 'displayMessage', data: 'PLEASE_WAIT' },
                {
                    action: () => {
                        Data.Questionnaire.questionnaireSessionTimeout = true;
                    }
                },
                { action: 'newUserSave' },
                {
                    action () {
                        return ELF.trigger('FirstSiteUserQuestionnaireTimeout:Transmit', {}, this);
                    }
                },
                { action: 'removeMessage' },
                { action: 'setState', data: State.states.activated },
                { action: 'navigateTo', data: 'summaryScreen' },
                { action: 'preventAll' }
            ],
            reject: [
                {
                    action (input) {
                        if (input.sessionTimeout) {
                            return Q();
                        }

                        let { Dialog } = MessageRepo;
                        return notify({
                            dialog: Dialog && Dialog.DIARY_TIMEOUT
                        });
                    }
                },
                { action: 'setState', data: State.states.languageSelection },
                { action: 'navigateTo', data: 'languageSelection' },
                { action: 'preventAll' }
            ]
        }
    ]
};
