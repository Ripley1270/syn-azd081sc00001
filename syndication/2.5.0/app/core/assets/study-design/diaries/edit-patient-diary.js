// This is a core questionnaire required for the Edit Patient workflow on the SitePad App.
// Please do not remove it, but configure it as needed.
// All resource strings referenced in this questionnaire are core (namespace:CORE) resources,
// but can be overwritten at either the questionnaire (namespace:Edit_Patient)
// or study (namespace:STUDY) levels.
export default {
    questionnaires: [{
        id: 'Edit_Patient',
        SU: 'Edit_Patient',
        displayName: 'EDIT_PATIENT',
        className: 'EDIT_PATIENT',

        // Use a custom affidavit designed for this questionnaire.
        // The affidavit configuration can be found below.
        affidavit: 'EditPatientSignatureAffidavit',

        // This questionnaire is only available on the SitePad App.
        product: ['sitepad'],
        screens: [
            'EDIT_PATIENT_S_1',
            'EDIT_PATIENT_S_2',
            'EDIT_PATIENT_S_3'
        ],
        branches: [],

        // Only Site Users and System Adminstrators may access this questionnaire.
        accessRoles: ['site', 'admin']
    }],

    screens: [{
        id: 'EDIT_PATIENT_S_1',
        className: 'EDIT_PATIENT_S_1',

        // DE16954 - Disable the back button on this screen only.
        disableBack: true,
        questions: [
            { id: 'EDIT_PATIENT_S1_INFO' },
            { id: 'EDIT_PATIENT_ID', mandatory: true },
            { id: 'EDIT_PATIENT_INITIALS', mandatory: true }
        ]
    }, {
        id: 'EDIT_PATIENT_S_2',
        className: 'EDIT_PATIENT_S_2',
        questions: [
            { id: 'EDIT_PATIENT_LANGUAGE', mandatory: true }
        ]
    }, {
        id: 'EDIT_PATIENT_S_3',
        className: 'EDIT_PATIENT_S_3',
        questions: [
            { id: 'EDIT_PATIENT_REASON_INFO' },
            { id: 'EDIT_PATIENT_REASON', mandatory: true }
        ]
    }],

    questions: [{
        id: 'EDIT_PATIENT_S1_INFO',
        IG: 'Add_Patient',
        IT: 'EDIT_PATIENT_S1_INFO',
        skipIT: '',
        title: '',
        text: ['EDIT_PATIENT_QUESTION_1_MAIN_TITLE'],
        className: 'EDIT_PATIENT_S1_INFO'
    }, {
        id: 'EDIT_PATIENT_ID',
        IG: 'PT',
        IT: 'Patientid',
        skipIT: '',
        title: '',
        text: ['EDIT_PATIENT_QUESTION_1'],
        className: 'EDIT_PATIENT_ID',
        widget: {
            id: 'EDIT_PATIENT_W_1',
            type: 'PatientIDTextBox',
            templates: {},
            answers: [],

            // numeric from 1 to 4 characters
            maxLength: 4,
            allowedKeyRegex: /[\d]/,
            validateRegex: /[\d]+/,
            _isUnique: true,
            _isInRange: true,
            validationErrors: [{
                property: 'completed',
                errorType: 'popup',
                errString: 'EDIT_PATIENT_QUESTION_1_EMPTY',
                header: 'EDIT_PATIENT_QUESTION_1_EMPTY_HEADER',
                ok: 'OK'
            }, {
                property: 'isInRange',
                errorType: 'popup',
                errString: 'EDIT_PATIENT_QUESTION_1_RANGE',
                header: 'EDIT_PATIENT_QUESTION_1_RANGE_HEADER',
                ok: 'OK'
            }],
            validation: {
                validationFunc: 'checkEditPatientID',
                params: {
                    errorStrings: {
                        errString: 'EDIT_PATIENT_QUESTION_1_UNIQUE',
                        header: 'EDIT_PATIENT_QUESTION_1_UNIQUE_HEADER',
                        ok: 'OK',
                        isInRange: {
                            errString: 'EDIT_PATIENT_QUESTION_1_RANGE',
                            header: 'EDIT_PATIENT_QUESTION_1_RANGE_HEADER',
                            ok: 'OK'
                        },

                        isUnique: {
                            errString: 'EDIT_PATIENT_QUESTION_1_UNIQUE',
                            header: 'EDIT_PATIENT_QUESTION_1_UNIQUE_HEADER',
                            ok: 'OK'
                        }
                    }
                }
            }
        }
    }, {
        id: 'EDIT_PATIENT_INITIALS',
        IG: 'PT',
        IT: 'Initials',
        skipIT: '',
        title: '',
        text: ['EDIT_PATIENT_QUESTION_2'],
        className: 'EDIT_PATIENT_INITIALS',
        widget: {
            id: 'EDIT_PATIENT_W_2',
            type: 'TextBox',
            templates: {},
            answers: [{ text: 'REASON_0', value: '0' }],

            // Allow English keyboard characters only
            // jscs:disable maximumLineLength
            allowedKeyRegex: /^[a-zA-Z]*$/,
            // jscs:enable

            maxLength: 6,
            validateRegex: /.{2,}/,
            validationErrors: [{
                property: 'completed',
                errorType: 'popup',
                errString: 'EDIT_PATIENT_QUESTION_2_EMPTY',
                header: 'EDIT_PATIENT_QUESTION_2_EMPTY_HEADER',
                ok: 'OK'
            }]
        }
    }, {
        id: 'EDIT_PATIENT_LANGUAGE',
        IG: 'Assignment',
        IT: 'Language',
        skipIT: '',
        title: 'EDIT_PATIENT_QUESTION_3_TITLE',
        text: ['EDIT_PATIENT_QUESTION_3'],
        className: '',
        widget: {
            id: 'EDIT_PATIENT_W_3',
            type: 'LanguageSelectWidget',
            label: 'LANGUAGE',
            field: 'language',
            templates: {},
            answers: [],
            validation: {
                validationFunc: 'checkLanguageFieldWidget',
                params: {}
            }
        }
    }, {
        id: 'EDIT_PATIENT_REASON_INFO',
        IG: 'Edit_Patient',
        IT: 'EDIT_PATIENT_REASON_INFO',
        title: '',
        text: 'EDIT_PATIENT_QUESTION_4_TITLE',
        className: 'EDIT_PATIENT_REASON_INFO'
    }, {
        id: 'EDIT_PATIENT_REASON',
        IG: 'Edit_Patient',
        IT: 'EDIT_PATIENT_REASON',
        title: '',
        text: 'EDIT_PATIENT_QUESTION_4',
        className: 'EDIT_PATIENT_REASON',
        widget: {
            id: 'EDIT_PATIENT_REASON',
            type: 'EditReason',
            className: 'EDIT_PATIENT_REASON',
            answers: [
                { text: 'EDIT_PATIENT_REASON_0', value: 'Correcting error by clinician' },
                { text: 'EDIT_PATIENT_REASON_1', value: 'Attribute data correction' },
                { text: 'EDIT_PATIENT_REASON_2', value: 'ID change' },
                { text: 'EDIT_PATIENT_REASON_3', value: 'Incorrect language selected' }
            ]
        }
    }],

    affidavits: [{
        id: 'EditPatientSignatureAffidavit',
        text: [
            'EDIT_PATIENT_AFFIDAVIT_HEADER',
            'EDIT_PATIENT_AFF'
        ],
        templates: { question: 'AffidavitText' },
        krSig: 'AddSubject',
        widget: {
            id: 'EDIT_PATIENT_SIGNATURE_AFFIDAVIT_WIDGET',
            className: 'AFFIDAVIT',
            type: 'SignatureBox',
            configuration: {
                sizeRatio: 3
            }
        }
    }],

    rules: [
        // EditPatientQuestionnaire
        // WARNING: DO NOT REMOVE! This rule is required for SPA's Edit Patient workflow.
        // When the Edit_Patient questionnaire is rendered, the configured fields will be populated
        // based on the selected patient.  The EditPatientQuestionnaireView resolves the selected patient (this.subject)
        // and it's associated user (this.user) model.,
        // See the Edit_Patient questionnaire configuration for which fields to populate.
        {
            id: 'EditPatientQuestionnaire',
            trigger: 'QUESTIONNAIRE:Rendered/Edit_Patient',
            resolve: [{
                // This action populates answer records based on a configured model.
                action: 'populateFieldsByModel',
                data: [{
                    SW_Alias: 'PT.0.Patientid',
                    question_id: 'EDIT_PATIENT_ID',
                    questionnaire_id: 'Edit_Patient',

                    // Use the subject model scoped to the EditPatientQuestionnaireView (this.subject).
                    model: 'subject',

                    // Use the subject model's subject_id property to populate the response.
                    // e.g. this.subject.get('subject_id');
                    property: 'subject_id'
                }, {
                    SW_Alias: 'PT.0.Initials',
                    question_id: 'EDIT_PATIENT_INITIALS',
                    questionnaire_id: 'Edit_Patient',

                    // Use the subject model scoped to the EditPatientQuestionnaireView (this.subject).
                    model: 'subject',

                    // Use the subject model's initials property to populate the response.
                    // e.g. this.subject.get('initials');
                    property: 'initials'
                }, {
                    SW_Alias: 'Assignment.0.Language',
                    question_id: 'EDIT_PATIENT_LANGUAGE',
                    questionnaire_id: 'Edit_Patient',

                    // The property on this response is determine by the 'field' property
                    // on the question's widget configuration. e.g. { language: 'en-US' }
                    // Indicate that the response is JSON.
                    isJSON: true,

                    // Determine the field name of the JSON response. e.g. { language: ... }
                    field: 'language',

                    // Use the user model scoped to the EditPatientQuestionnaireView (this.user).
                    model: 'user',

                    // Use the user model's language property to populate the response.
                    // e.g. this.user.get('language');
                    property: 'language'
                }]
            }]
        },

        // WARNING: DO NOT REMOVE! This rule is required for SPA's Edit Patient workflow.
        {
            id: 'EditPatientDiaryComplete',
            trigger: 'QUESTIONNAIRE:Completed/Edit_Patient',

            // High importance so this rule runs before the global rules.
            salience: 100,
            resolve: [
                { action: 'editPatientSave' },
                { action: 'updateCurrentContext' },

                // Prevent the global QUESTIONNAIRE:Completed rule from running
                // Prevent the default behavior that saves the diary
                { action: 'preventAll' }
            ]
        },

        // WARNING: DO NOT REMOVE! This rule is required for SPA's Edit Patient workflow.
        {
            id: 'EditPatientDiarySync',
            trigger: 'QUESTIONNAIRE:Transmit/Edit_Patient',
            salience: 100,
            resolve: [
                // If the device is online, transmit all queued transmissions and sync subjects.
                { action: () => ELF.trigger('EditPatientDiarySync:Transmit', {}, this) },
                { action: 'updateCurrentContext' },

                // Prevent the global QUESTIONNAIRE:Transmit rule from running
                { action: () => ({ stopRules: true }) }
            ]
        },

        // WARNING: DO NOT REMOVE! This rule is required for SPA's Edit Patient workflow.
        {
            id: 'EditPatientValidateAnswers',
            trigger: 'QUESTIONNAIRE:Navigate/Edit_Patient/EDIT_PATIENT_S_2',
            evaluate: 'isDirectionForward',
            salience: 100,
            resolve: [
                { action: 'editPatientValidateAnswers' }
            ]
        }
    ]
};
