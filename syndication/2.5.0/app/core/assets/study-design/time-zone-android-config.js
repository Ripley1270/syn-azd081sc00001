import TimeZone from './time-zone-config';

export default {
    android: {
        timeZoneOptions: TimeZone.core.timeZoneOptions
    }
};
