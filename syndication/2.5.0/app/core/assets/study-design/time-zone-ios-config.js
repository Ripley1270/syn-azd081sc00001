import TimeZone from './time-zone-config';

export default {
    ios: {
        timeZoneOptions: TimeZone.core.timeZoneOptions
    }
};
