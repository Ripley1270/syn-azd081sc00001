/*
 * This object is used to configure routes unique to the trial.
 * You may override routes, or create new ones. Each route should call out to the StudyController.
 */
export default {

    // SitePad App routes:
    sitepad: { },

    // LogPad App routes:
    logpad: { }

};
