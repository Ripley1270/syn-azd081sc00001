/**
 * @file The Setting Variables for Core.
 * @author <a href="mailto:dyildiz@phtcorp.com">Dogus Yildiz</a>
 */

LF.CoreSettings = {
    // The maximum number of consecutive unsuccessful unlock attempts in web modality.
    maxUnlockAttempts: 3,

    // Number of minutes after which the lockout for the unlock code expires
    unlockCodeLockoutTime: 5,

    // The default language value, if none is defined in StudyDesign
    defaultLanguage: 'en',

    // The default locale value, if none is defined in StudyDesign
    defaultLocale: 'US',

    // The timeout value in seconds for the transmission Ajax calls.
    transmissionTimeout: 300,

    // The amount of time in minutes that a user can be inactive before being logged out.
    sessionTimeout: 30,

    // The amount of time in minutes before an alert appears telling the user that the session will timeout soon.
    sessionTimeoutAlert: 25,

    // The volume of the session timeout, relative to device volume.
    sessionTimeoutAlertSoundVolume: 1,

    eSense: {
        // Whether or not Bluetooth should be disabled automatically after an operation is complete
        disableBluetooth: false,

        // Default timeout value in milliseconds for eSense all API calls (except findDevices and connect).
        apiCallTimeout: 10 * 1000,

        // Default timeout value in milliseconds for eSense findDevices API calls (in milliseconds).
        findDevicesTimeout: 60 * 1000,

        // Default timeout value in milliseconds for eSense get records API calls (in milliseconds).
        getRecordsTimeout: 30 * 1000,

        // The threshold value of the difference between LogPad App time and eSense Device time, to update eSense time (in milliseconds).
        thresholdForTimeUpdate: 5 * 60 * 1000,

        // Default configurations for AM1+ Device
        AM1Plus: {
            // Default Timeout Window configuration for AM1+
            timeWindow: {
                windowNumber: 1,
                startHour: 0,
                startMinute: 0,
                endHour: 23,
                endMinute: 59,
                maxNumber: 15
            }
        }
    },

    // The countdown value in milliseconds for the application restart when the Time Zone changes.
    timeZoneChangeRestartCountdown: 15 * 1000,

    // Custom environments are disabled by default. This should only be enabled on developer workspaces.
    // When enabled, it will break a unit test and fail the build to ensure it won't be updated in DEV, INT or QA Streams
    disableCustomEnvironments: true
};
