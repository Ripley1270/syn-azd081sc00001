import COOL from 'core/COOL';
import Logger from 'core/Logger';
import Subjects from 'core/collections/Subjects';
import Dashboards from 'core/collections/Dashboards';
import Answers from 'core/collections/Answers';
import WebService from 'core/classes/WebService';
import Payload from 'core/classes/Payload';

const logger = new Logger('Transmit.transmitQuestionnaire');

let dataCache = null;

/**
 * Fetches the diary related sata and populates the dataCache
 * @returns {Q.Promise}
 */
function fetchData () {
    // Optimize transmissions by avoiding having to decrypt the database for every transmission item.
    if (dataCache) {
        return Q();
    }

    dataCache = {};
    dataCache.dashboards = new Dashboards();
    dataCache.answers = new Answers();

    return dataCache.dashboards.fetch()
    .then(() => dataCache.answers.fetch());
}

/**
 * Returns the data for the specified diary record
 * @param {number} dashboardId - the id of the dashboard record for the diary data being requested
 * @param {string} sigId - the sigId of the dashboard record for the diary data being requested
 * @param {number} transmissionItemId - the id of the transmission record for the diary
 * @returns {Q.Promise}
 */
function getDiaryData (dashboardId, sigId, transmissionItemId) {
    // Find the correct dashboard record to transmit.
    let dashboardRecord = _.where(dataCache.dashboards.toJSON(), {
        id: dashboardId
    })[0];

    logger.trace(`transmitQuestionnaire: fetchData: dashboardRecord: ${dashboardRecord}`);

    if (!dashboardRecord) {
        logger.error(`Dashboard record does not exist. Dashboard ID: ${dashboardId}, sigID: ${sigId}`);
        logger.trace('transmitQuestionnaire: no dashboard record, destroying transmission item');
        return this.destroy(transmissionItemId)
        .then(() => Q.reject());
    }

    // Find the answer records that belong to the dashboard item.
    let diaryAnswers = _.where(dataCache.answers.toJSON(), {
        questionnaire_id: dashboardRecord.questionnaire_id,
        dashboardId
    });

    if (!diaryAnswers.length) {
        logger.operational('transmitQuestionnaire: no answer records were found by dashboardId. Now, for backwards compatibility, trying to fetch answers by instance_ordinal');

        diaryAnswers = _.where(dataCache.answers.toJSON(), {
            questionnaire_id: dashboardRecord.questionnaire_id,
            instance_ordinal: dashboardRecord.instance_ordinal
        });
    }

    // If there are no answers...
    if (!diaryAnswers.length) {
        // StudyWorks will fail if "A" is []
        logger.error('transmitQuestionnaire failed to find answers');
    }

    // Copy the dashboard record data and the diary answers to the diaryJSON object
    let diaryJSON = _.extend({}, dashboardRecord);
    diaryJSON.Answers = diaryAnswers;

    return Q(diaryJSON);
}

/**
 * Bundles a questionnaire to be transmitted to the Web-Service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise}
 */
export default function transmitQuestionnaire (transmissionItem) {
    let diaryJSON,
        params = JSON.parse(transmissionItem.get('params')),
        sendDiarySuccess = ({ isSubjectActive, isDuplicate }, diaryJSONObj) => {
            let subjectKrpt = diaryJSONObj.krpt || localStorage.getItem('krpt');

            logger.operational(`Diary transmitted: ${diaryJSONObj.SU} with Dashboard ID: ${params.dashboardId}, sigID: ${diaryJSONObj.sig_id}`);

            if (isDuplicate) {
                logger.error(`Duplicate transmission: ${diaryJSONObj.SU} with Dashboard ID: ${params.dashboardId}, sigID: ${diaryJSONObj.sig_id}`);
            }

            // Destroy the corresponding dashboard and answers records
            logger.trace(`transmitQuestionnaire: sendDiarySuccess: about delete item from database ${dataCache.dashboards.get(diaryJSONObj.id)}`);
            Backbone.sync('delete', dataCache.dashboards.get(diaryJSONObj.id));

            _.each(diaryJSONObj.Answers, (answer) => {
                logger.trace(`transmitQuestionnaire: sendDiarySuccess: _each: delete answer ${dataCache.answers.get(answer.id)}`);
                Backbone.sync('delete', dataCache.answers.get(answer.id));
            });

            this.destroy(transmissionItem.get('id'), $.noop);

            if (isSubjectActive) {
                return Q();
            }

            logger.trace('transmitQuestionnaire: sendDiarySuccess: !isSubjectActive saving subject not active');

            return Subjects.getSubjectBy({ krpt: subjectKrpt })
            .then(subject => subject.save({ subject_active: 0 }));
        },
        sendDiaryError = (resultSet) => {
            logger.trace('transmitQuestionnaire: sendDiaryError: removing transmissionItem');
            this.remove(transmissionItem);
            return Q.reject(resultSet);
        },
        handleCacheClear = () => {
            let isLastItem = !this.filter(item => item.get('method') === 'transmitQuestionnaire').length;

            if (isLastItem) {
                dataCache = null;
            }
        };

    return fetchData()
    .then(() => getDiaryData(params.dashboardId, params.sigId, transmissionItem.get('id')))
    .then((diaryData) => {
        let { payload, useJSONH } = COOL.getClass('Payload', Payload).questionnaire(diaryData),
            service = COOL.new('WebService', WebService);

        diaryJSON = diaryData;
        return service.sendDiary(payload, transmissionItem.get('token'), useJSONH);
    })
    .then(resultSet => sendDiarySuccess(resultSet, diaryJSON))
    .catch(sendDiaryError)
    .finally(() => {
        handleCacheClear();
        logger.traceExit('transmitQuestionnaire');
    });
}
