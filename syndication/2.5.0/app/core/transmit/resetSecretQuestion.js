import COOL from 'core/COOL';
import ELF from 'core/ELF';
import Spinner from 'core/Spinner';
import Subjects from 'core/collections/Subjects';
import CurrentSubject from 'core/classes/CurrentSubject';
import WebService from 'core/classes/WebService';
import { MessageRepo } from 'core/Notify';
import Payload from 'core/classes/Payload';

/**
 * Handles the resetSecretQuestion transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise}
 */
export default function resetSecretQuestion (transmissionItem) {
    let resetSecretQuestionSuccess,
        resetSecretQuestionError,
        params = JSON.parse(transmissionItem.get('params')),
        mappedJSON = COOL.getClass('Payload', Payload).resetSecretQuestion(params);

    return Subjects.getSubjectBy({ krpt: params.krpt })
    .then((subject) => {
        resetSecretQuestionSuccess = ({ isSubjectActive }) => {
            return subject.save({ subject_active: isSubjectActive })
            .then(() => {
                CurrentSubject.clearSubject();
                return this.destroy(transmissionItem.get('id'));
            });
        };

        resetSecretQuestionError = ({ errorCode, httpCode, isSubjectActive }) => {
            const { Dialog } = MessageRepo;

            if (httpCode === LF.ServiceErr.HTTP_FORBIDDEN && errorCode === LF.ServiceErr.SUBJECT_NOT_FOUND) {
                // Delete clinical data
                return ELF.trigger('Termination', {
                    endParticipation: true,
                    subjectActive: !!isSubjectActive,
                    deviceID: subject.get('device_id')
                }, this)
                .finally(() => this.destroy(transmissionItem.get('id')));
            }

            if (!isSubjectActive) {
                return Spinner.hide()
                .then(() => LF.Actions.notify({
                    dialog: Dialog && Dialog.SECRET_QUESTION_CHANGE_FAILED
                }))
                .then(() => Spinner.show())
                .then(() => this.destroy(transmissionItem.get('id')));
            }

            if (httpCode !== LF.ServiceErr.HTTP_NOT_FOUND) {
                return Spinner.hide()
                .then(() => {
                    let formattedErrorCode = errorCode ? `-${errorCode}` : '';

                    return LF.Actions.notify({
                        dialog: Dialog && Dialog.TRANSMISSION_ERROR_HTTP_AND_ERROR,
                        options: { httpCode, errorCode: formattedErrorCode }
                    });
                })
                .then(() => Spinner.show())
                .then(() => {
                    if (subject.get('isDuplicate')) {
                        return transmissionItem.save({ status: 'failed' });
                    }

                    this.remove(transmissionItem);
                    return Q();
                });
            }

            this.remove(transmissionItem);

            // Error cases are handled within the transmit method.
            // No need to reject the promise chain. At least for now.
            return Q();
        };

        let service = COOL.new('WebService', WebService);
        return service.updateSubjectData(subject.get('device_id'), mappedJSON, null)
        .then(resetSecretQuestionSuccess)
        .catch(resetSecretQuestionError);
    });
}
