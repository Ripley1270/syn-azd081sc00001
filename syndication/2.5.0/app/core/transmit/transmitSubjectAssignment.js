import COOL from 'core/COOL';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';
import { MessageRepo } from 'core/Notify';
import Spinner from 'core/Spinner';
import Payload from 'core/classes/Payload';

const logger = new Logger('Transmit.transmitSubjectAssignment');

/**
 * Handles the transmitSubjectAssignment transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise}
 */
export default function transmitSubjectAssignment (transmissionItem) {
    let params = JSON.parse(transmissionItem.get('params')),
        assignment = COOL.getClass('Payload', Payload).subjectAssignment(params),
        sendAssignmentSuccess = () => {
            logger.operational(`SubjectAssignment transmitted: PatientID: ${params.patientId}, sigID: ${params.sigID}`);

            return this.destroy(transmissionItem.get('id'));
        },
        sendAssignmentError = ({ errorCode, httpCode, isSubjectActive }) => {
            let subjectId = JSON.parse(transmissionItem.attributes.params).patientId,
                responseKrpt = JSON.parse(transmissionItem.attributes.params).krpt;

            logger.error(`transmitSubjectAssignment: sendAssignmentError. removing transmissionItem. errorCode: ${errorCode}, httpCode: ${httpCode}, isSubjectActive: ${isSubjectActive}`);

            if (errorCode === '19') {
                LF.DynamicText.duplicateSubjectId = subjectId;

                logger.info(`Transmit Subject failed. Subject ${subjectId} already exists`);

                return ELF.trigger('TRANSMIT:Duplicate/Subject', { subjectId, krpt: responseKrpt }, this)
                .then(() => Spinner.hide())
                .then(() => {
                    const { Dialog } = MessageRepo;

                    return MessageRepo.display(Dialog && Dialog.SUBJECT_ALREADY_EXISTS_ERROR);
                })
                .then(() => Spinner.show())
                .then(() => transmissionItem.save({ status: 'failed' }))
                .then(() => {
                    this.remove(transmissionItem);
                });
            }

            if (errorCode === '20') {
                logger.error(`Transmit Subject failed. Subject ${subjectId} was already successfully transmitted, deleting transmissionItem.`);

                return this.destroy(transmissionItem.get('id'));
            }

            this.remove(transmissionItem);
            return Q.reject({ errorCode, httpCode, isSubjectActive });
        };

    logger.traceEnter('transmitSubjectAssignment');

    let service = COOL.new('WebService', WebService);

    return service.sendSubjectAssignment(assignment, transmissionItem.get('token'))
    .then(sendAssignmentSuccess)
    .catch(sendAssignmentError)
    .finally(() => {
        logger.traceExit('transmitSubjectAssignment');
    });
}
