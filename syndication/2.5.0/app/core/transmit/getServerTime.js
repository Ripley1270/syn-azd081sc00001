import COOL from 'core/COOL';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';

const logger = new Logger('Transmit.getServerTime');

/**
 * Gets the time in UTC from the server.
 * @returns {Q.Promise<Date>}
 */
export default function getServerTime () {
    let service = COOL.new('WebService', WebService);

    return service.getServerTime()
    .then((response = {}) => {
        let dateTime = response.res || {};

        if (!dateTime.utc) {
            logger.error('Unable to establish server date and time');
            return false;
        }

        return new Date(dateTime.utc);
    });
}
