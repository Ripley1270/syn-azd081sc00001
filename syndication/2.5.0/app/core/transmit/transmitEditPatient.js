import COOL from 'core/COOL';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import WebService from 'core/classes/WebService';
import { MessageRepo } from 'core/Notify';
import Spinner from 'core/Spinner';
import Payload from 'core/classes/Payload';

const logger = new Logger('Transmit.transmitEditPatient');

/**
 * Handles the transmitEditPatient transmission to the web-service.
 * @param {Object} transmissionItem - The item in the transmission queue to send.
 * @returns {Q.Promise}
 */
export default function transmitEditPatient (transmissionItem) {
    let params = JSON.parse(transmissionItem.get('params')),
        editPatientJSON = COOL.getClass('Payload', Payload).editPatient(params),
        sendAssignmentSuccess = ({ res }) => {
            logger.operational(`EditPatient transmitted: res: ${res}`);
            return this.destroy(transmissionItem.get('id'));
        },
        sendAssignmentError = ({ errorCode, httpCode }) => {
            logger.trace(`transmitEditPatient: EditPatientError: removing transmissionItem errorCode: ${errorCode} httpCode: ${httpCode}`);

            if (errorCode === '19') {
                let responseKrpt = JSON.parse(transmissionItem.attributes.params).krpt;

                return ELF.trigger('TRANSMIT:Duplicate/Subject', {
                    krpt: responseKrpt
                }, this)
                .then(() => Spinner.hide())
                .then(() => {
                    const { Dialog } = MessageRepo;

                    logger.info(`Transmisson for Edit Subject failed. Subject ${LF.DynamicText.duplicateSubjectId} already exists`);

                    return MessageRepo.display(Dialog && Dialog.SUBJECT_ALREADY_EXISTS_ERROR);
                })
                .then(() => Spinner.show())
                .then(() => transmissionItem.save({ status: 'failed' }))
                .then(() => {
                    this.remove(transmissionItem);
                });
            }

            this.remove(transmissionItem);

            // Error cases are handled within the transmit method.
            // No need to reject the promise chain. At least for now.
            return Q();
        };

    logger.traceEnter('transmitEditPatient');

    let service = COOL.new('WebService', WebService);

    return service.sendEditPatient(editPatientJSON, '')
    .then(sendAssignmentSuccess)
    .catch(sendAssignmentError)
    .finally(() => {
        logger.traceExit('transmitEditPatient');
    });
}
