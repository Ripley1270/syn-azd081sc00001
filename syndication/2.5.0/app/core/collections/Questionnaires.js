import BaseCollection from './BaseCollection';
import Questionnaire from '../models/Questionnaire';

/**
 * A class that creates a collection of Questionnaires.
 * @class Questionnaires
 * @extends BaseCollection
 * @example let collection = new Questionnaires();
 */
export default class Questionnaires extends BaseCollection {
    /**
     * @property {Questionnaire} model - The Model that belongs to the collection
     * @readonly
     * @default '{@link Questionnaire}'
     */
    get model () {
        return Questionnaire;
    }
}

window.LF.Collection.Questionnaires = Questionnaires;
