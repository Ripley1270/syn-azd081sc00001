import * as lStorage from 'core/lStorage';
import Users from 'core/collections/Users';
import Logger from 'core/Logger';
import CurrentContext from 'core/CurrentContext';
import ELF from 'core/ELF';
import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import { MessageRepo } from 'core/Notify';
import * as utilities from 'core/utilities';

const logger = new Logger('Session');

export default class Session {
    constructor () {
        // Used to check for session timeout.
        this.interval = 5000;

        /**
         * The current status of the abandon alert.
         * @enum {number}
         */
        this.abandonAlertStatus = {
            /**
             * The alert has not yet been displayed
             * @type {number}
            */
            pending: 0,

            /**
             * The alert is currently being displayed
             * @type {number}
            */
            open: 1,

            /**
             * The alert has been displayed and closed
             * @type {number}
            */
            closed: 2
        };

        /**
         * Holds whether the alert has been displayed and closed (2),
         * if it is currently open (1)
         * or if it has not been displayed (0)
        */
        this.currentAlertStatus = this.abandonAlertStatus.pending;
    }

    // Set an item in localStorage.
    set (name, value) {
        lStorage.setItem(name, value);
    }

    // Get an item from localStorage.
    get (name) {
        return lStorage.getItem(name);
    }

    // Remove an item from localStorage.
    remove (name) {
        return lStorage.removeItem(name);
    }

    // Listen for user activity and then update the last active timestamp.
    listenForActivity () {
        // If off() is not called, the listeners will be added multiple times.
        $('body')
            .off('mouseup keydown')
            .on('mouseup keydown', () => this.setLastActive());
    }

    // Sets a localStorage['PHT_Last_Active'] to the current time in milliseconds.
    setLastActive () {
        let now = (new Date()).getTime();

        this.set('Last_Active', now);

        return now;
    }

    // Return the time the user was last active.
    getLastActive () {
        let lastActive = this.get('Last_Active');

        if (lastActive) {
            return new Date().setTime(lastActive);
        }
        return new Date();
    }

    // Sets a localStorage['PHT_Timeout_LastCheck'] to the current time in milliseconds.
    setTimeoutLastCheck () {
        let now = (new Date()).getTime();

        this.set('Timeout_LastCheck', now);

        return now;
    }

    /**
     * Return the time that the timeout was last checked
     * @return {Object|Boolean}
     */
    getTimeoutLastCheck () {
        let lastCheck = this.get('Timeout_LastCheck');

        return lastCheck ? (new Date()).setTime(lastCheck) : new Date();
    }

    /* Start the Timer for activation and application session timeout. */
    startSessionTimeOut () {
        const sessionTimeoutAlert = (() => {
            if (LF.StudyDesign.sessionTimeoutAlert === undefined) {
                return LF.CoreSettings.sessionTimeoutAlert;
            }

            return LF.StudyDesign.sessionTimeoutAlert;
        })();

        if (_(LF.StudyDesign.sessionTimeout).isUndefined() || _(LF.StudyDesign.sessionTimeout).isNull()) {
            // TODO: Move this default value to app/core/coreSettings.js
            // Default to 30 minutes
            LF.StudyDesign.sessionTimeout = 30;
        }

        this.sessionTimeout = (LF.StudyDesign.sessionTimeout * 60) * 1000;
        this.sessionTimeoutAlert = (sessionTimeoutAlert * 60) * 1000;

        this.listenForActivity();
        this.timerOn = true;
        clearTimeout(this.timer);

        if (!this.get('Last_Active')) {
            this.setLastActive();
        }

        if (!this.get('Timeout_LastCheck')) {
            this.setTimeoutLastCheck();
        }

        this.checkTimeOut();
    }

    /**
     * Default handler for the session timeout
     * @param {Backbone.View} currentView - The view that is being displayed when timeout happens
     */
    defaultSessionTimeoutHandler (currentView) {
        if (currentView instanceof BaseQuestionnaireView) {
            // if session timeout occurs before questionnaire timeout
            if (this.questionnaireTimerOn) {
                this.stopQuestionnaireTimeOut();
            }

            // Saves the report if Affidavit is checked.
            ELF.trigger(`QUESTIONNAIRE:SessionTimeout/${currentView.id}`, {
                questionnaire: currentView.id
            }, currentView)
            .done();
        } else {
            this.logout(true);
        }
    }

    /**
     * Get the sig_id from the currentView for use in logs.
     * @param {Backbone.View} currentView - The current view.
     * @memberof Session
     * @returns {Object | null} An object containing the sig_id for use in logs.
     * Or null if the view has no sig_id.
     */
    getLogData (currentView = LF.router.view()) {
        try {
            return {
                sigId: currentView.data.dashboard.get('sig_id')
            };
        } catch (err) {
            // if no sigId, return null.
            return null;
        }
    }

    /**
     * Default handler for the session timeout
     * @param {Backbone.View} [currentView] - The view that is being displayed when timeout happens,
     * defaults to LF.router.view()
     * @returns {Q.Promise<>} Promise that either resolves immediatly
     * or after displaying an alert message if we are in a questionnaire.
     */
    sessionTimeoutAlertHandler (currentView = LF.router.view()) {
        // TODO: This is a bad-practice. We had to implement this because at the end of the release because
        // we noticed that this feature should be disabled for handheld. Instead of validating the feature
        // we disable it in order to reduce the testing effort for SQE.
        if (utilities.isLogPad()) {
            return Q();
        }

        const isAlertPending = this.currentAlertStatus === this.abandonAlertStatus.pending;

        if (currentView instanceof BaseQuestionnaireView || currentView.id === 'context-switching-view') {
            if (isAlertPending) {
                const logData = this.getLogData(currentView);

                logger.operational('Displaying session timeout alert', logData);
                this.closePopups();
                this.currentAlertStatus = this.abandonAlertStatus.open;

                if (LF.StudyDesign.sessionTimeoutAlertSound) {
                    const sound = new Audio(LF.StudyDesign.sessionTimeoutAlertSound);
                    const volume = LF.StudyDesign.sessionTimeoutAlertSoundVolume;

                    sound.volume = volume >= 0 ? volume : LF.CoreSettings.sessionTimeoutAlertSoundVolume;

                    sound.play();
                }

                return MessageRepo.display(MessageRepo.Dialog.ABANDON_ALERT)
                .then(() => {
                    this.currentAlertStatus = this.abandonAlertStatus.pending;
                    return logger.operational('Session timeout alert closed by user', logData);
                });
            }
        }

        return Q();
    }

    /**
     * Close any current Popups.
     */
    closePopups () {
        let $dateBox = $('input.date-input');
        $dateBox.length && $dateBox.datebox('close');

        let $timeBox = $('input.time-input');
        $timeBox.length && $timeBox.datebox('close');

        let $select2 = $('.select2-hidden-accessible');
        $select2.length && $select2.select2('close');

        $dateBox = null;
        $timeBox = null;
        $select2 = null;

        // Close all modal backgrounds
        $('.modal-backdrop').remove();
        $('.mbsc-jqm').remove();

        // Close modal.
        $('.modal').modal('hide');
    }

    /**
     * Checks for timeout every 5 seconds and logs out if there is a timeout.
     * @example this.checkTimeOut();
     */
    checkTimeOut () {
        let now = new Date(),
            lastActive = new Date(),
            lastCheck = new Date(),

            // SM: Generally unit tests don't set LF.router.view,
            //     then, on timeout, an error occurs here.
            //     Karma exits if error is thrown during timeout (why is a mystery);
            //     consequently, test runs terminate randomly based on timing.
            //     Hence all these seemingly paranoid checks:
            currentView = LF.router && LF.router.view ? LF.router.view() : undefined;

        if (currentView) {
            lastActive.setTime(this.getLastActive());
            lastCheck.setTime(this.getTimeoutLastCheck());

            /**
             * Check current time against given timeout.
             * @param {number} [timeout] - The timeout to check against
             * @returns {boolean} Whether the timeout has been reached.
            */
            const check = (timeout = this.sessionTimeout) => {
                const time = now.getTime();
                return ((time - lastActive.getTime()) > timeout) || ((time - lastCheck.getTime()) > timeout);
            };

            const logData = this.getLogData(currentView);

            // If we are past the sessionTimeout limit, logout.
            if (this.timerOn && check(this.sessionTimeout)) {
                this.setLastActive();

                this.closePopups();

                if (currentView.id === 'login-view') {
                    localStorage.removeItem('PHT_isAuthorized');

                    LF.router.controller.login();
                } else {
                    this.defaultSessionTimeoutHandler(currentView);
                }

                if (this.currentAlertStatus === this.abandonAlertStatus.open) {
                    logger.operational('Session has timed out with alert still displayed', logData);
                    this.currentAlertStatus = this.abandonAlertStatus.pending;
                }

                logger.operational('Session has timed out.');

            // If we are in a diary, warn before the session times out.
            } else if (
                this.sessionTimeoutAlert > 0 &&
                check(this.sessionTimeoutAlert) &&
                this.currentAlertStatus === this.abandonAlertStatus.pending
            ) {
                this.sessionTimeoutAlertHandler(currentView)
                .done();
            } else if (this.questionnaireTimerOn && (now.getTime() - this.questionnaireStartTime) > this.questionnaireTimeout) {
                let previousAlertStatus = this.currentAlertStatus;
                this.stopQuestionnaireTimeOut();

                // When questionnaire times out before abandon alert, turn off the alert.
                this.currentAlertStatus = this.abandonAlertStatus.closed;

                if (currentView instanceof BaseQuestionnaireView) {
                    this.closePopups();

                    logger.operational(`Questionnaire ${currentView.id} has timed out.`);

                    if (previousAlertStatus === this.abandonAlertStatus.open) {
                        logger.operational('Questionnaire has timed out with alert still displayed', logData);
                    }

                    // Saves the report if Affidavit is checked.
                    ELF.trigger(`QUESTIONNAIRE:QuestionnaireTimeout/${currentView.id}`, {
                        questionnaire: currentView.id
                    }, currentView)
                    .done();
                }
            }
        }

        clearTimeout(this.timer);

        if (this.timerOn) {
            // this.frequency => this.interval
            this.timer = setTimeout(_.bind(this.checkTimeOut, this), this.interval);
            this.setTimeoutLastCheck();
        }
    }

    /**
     * Pause the Timer for application session timeout.
     * @example LF.security.pauseSessionTimeOut();
     */
    pauseSessionTimeOut () {
        logger.trace('Session timeout has been paused.');

        this.timerOn = false;
    }

    /**
     * Restart the Timer for application session timeout.
     * @example LF.security.restartSessionTimeOut();
     */
    restartSessionTimeOut () {
        logger.trace('Session timeout has been restarted.');

        if (!this.timerOn) {
            this.timerOn = true;
            this.checkTimeOut();
        }
    }

    /**
     * Start the Timer for questionnaire timeout.
     * @example LF.security.startQuestionnaireTimeOut();
     */
    startQuestionnaireTimeOut () {
        this.currentAlertStatus = this.abandonAlertStatus.pending;
        if (LF.StudyDesign.questionnaireTimeout && LF.StudyDesign.questionnaireTimeout !== 0) {
            this.questionnaireTimerOn = true;
            this.questionnaireStartTime = new Date().getTime();
            this.questionnaireTimeout = (LF.StudyDesign.questionnaireTimeout * 60) * 1000;

            logger.trace('Questionnaire timeout has been started.');
        }
    }

    /**
     * Stops the Timer for questionnaire timeout.
     * @example LF.security.stopQuestionnaireTimeOut();
     */
    stopQuestionnaireTimeOut () {
        if (this.questionnaireTimerOn) {
            this.questionnaireTimerOn = false;
            this.questionnaireStartTime = undefined;

            logger.trace('Questionnaire timeout has been stopped.');
        }
    }

    /**
     * Pause the questionnaire timeout.
     */
    pauseQuestionnaireTimeout () {
        this.questionnaireTimerOn = false;

        logger.trace('Questionnaire timeout has been paused.');
    }

    /**
     * Restart the questionaire timeout.
     */
    restartQuestionnaireTimeout () {
        if (!this.questionnaireTimerOn) {
            this.questionnaireTimerOn = true;

            logger.trace('Questionnaire timeout has been restarted.');
        }
    }

    /*
     * Checks if max login attempts is exceeded
     * @param {Object} user The current user
     * @return (Boolean) true - max login attempts is exceeded, false - max login attempts is not exceeded
     */
    checkLock (user) {
        let maxAttemptsExceeded = LF.StudyDesign.maxLoginAttempts && (LF.security.getLoginFailure(user) >= LF.StudyDesign.maxLoginAttempts),
            nowUtcTimestamp = new Date().getTime(),
            userEnableTimestamp = user.get('enableDeviceUtc'),
            lockoutExpired = userEnableTimestamp && (nowUtcTimestamp >= userEnableTimestamp),
            userLocked = maxAttemptsExceeded ? !lockoutExpired : false;

        if (lockoutExpired) {
            logger.operational(`User lockout expired for user ${user.get('username')}`);

            this.resetFailureCount(user);
        }

        return userLocked;
    }

    /*
     * Sets a localStorage['PHT_Login_Failure'] to the current number of consecutive unsuccessful logon attempts.
     * @param {Number} number current number of consecutive unsuccessful logon attempts to be saved
     * @param {Object} user The current user
     */
    setLoginFailure (number, user) {
        user.set('failedLoginAttempts', number).save();
    }

    /**
     * Return the current number of consecutive unsuccessful logon attempts stored in localStorage, if not return 0
     * @param {Object} user The current user
     * @return {number}
     */
    getLoginFailure (user) {
        let attempts = user.get('failedLoginAttempts');

        return attempts != null ? parseInt(attempts, 10) : 0;
    }

    /*
     * Resets a localStorage['PHT_Login_Failure'], localStorage['PHT_Security_Question_Failure'] and ['PHT_Enable_Device_Utc'] to 0
     * @param {Object} user The current user
     */
    resetFailureCount (user) {
        user.set('failedLoginAttempts', 0);
        user.set('failedQuestionAttempts', 0);
        user.set('enableDeviceUtc', 0);
        user.save();
    }

    /**
     * Sets lastLogin to now for a user.
     * @param {Object} user The current user
     */
    setLastLogin (user) {
        user.set('lastLogin', new Date().toString()).save();
    }

    /**
     * Sets a localStorage['PHT_Security_Question_Failure'] to the current number of consecutive unsuccessful secret question attempts.
     * @param {Number} number current number of consecutive unsuccessful logon attempts to be saved
     * @param {Object} user The current user d
     */
    setSecurityQuestionFailure (number, user) {
        user.set('failedQuestionAttempts', number).save();
    }

    logout (navigate) {
        delete this.activeUser;

        if (navigate) {
            return LF.router.navigate('login', true);
        }

        return Q();
    }

    /**
     * Checks if a user is logged in.
     * @returns {Q.Promise<Boolean>} True if logged in, false if not.
    */
    checkLogin () {
        return Q()
        .then(() => {
            if (lStorage.getItem('isActivation') || localStorage.getItem('screenshot')) {
                return true;
            } else if (lStorage.getItem('isAuthorized') && lStorage.getItem('User_Login')) {
                // DE15020 - After a refresh, the activeUser property is undefined.
                if (this.activeLogin != null) {
                    return true;
                } else if (!this.activeUser) {
                    let userId = parseInt(lStorage.getItem('User_Login'), 10);

                    return this.getUser(userId)
                    .then((user) => {
                        this.activeUser = user;
                        return true;
                    });
                }

                return true;
            }

            return false;
        });
    }

    /**
     * Return the current number of consecutive unsuccessful security question attempts stored in localStorage, if not return 0
     * @param {Object} user The current user
     * @return {Number} number of security question failures
     */
    getSecurityQuestionFailure (user) {
        let questionFailure = user.get('failedQuestionAttempts');

        return questionFailure != null ? parseInt(questionFailure, 10) : 0;
    }

    /*
     * Redirects the user to Security Question Screen
     * Starts the checkLockTimer function
     * @example this.accountDisable();
     * @param {Object} user The current user
     * @return {Boolean} Returns true
     */
    accountDisable (user) {
        let nowUtcTimestamp,
            enableDeviceUtcTimeStamp;

        if (_(LF.StudyDesign.lockoutTime).isUndefined() || _(LF.StudyDesign.lockoutTime).isNull()) {
            // Default to 5 minutes
            LF.StudyDesign.lockoutTime = 5;
        }

        if (LF.StudyDesign.lockoutTime === 0) {
            // User will be able to re-enable disabled account with "Forgot Password" process described in US1610
            return true;
        }
        nowUtcTimestamp = new Date().getTime();
        enableDeviceUtcTimeStamp = nowUtcTimestamp + (LF.StudyDesign.lockoutTime * 60000);
        user.set('enableDeviceUtc', enableDeviceUtcTimeStamp).save();

        return true;
    }

    // Fetch all users from the database.
    fetchUsers () {
        let collection = new Users();

        return collection.fetch()
        .then(() => {
            this.users = collection;

            return collection;
        });
    }

    getUser (id, fetchCollection = false) {
        if (fetchCollection) {
            return this.fetchUsers()
            .then((users) => {
                return users.findWhere({
                    id
                });
            });
        }
        return Q(CurrentContext().get('users').findWhere({
            id
        }));
    }

    /*
     * @method validate
     * Validate a user login.
     */
    login (id, password) {
        return this.getUser(id, true)
        // eslint-disable-next-line consistent-return
        .then((user) => {
            if (user) {
                let userPassword = user.get('password'),
                    isAdminUser = user.get('userType') === 'Admin',
                    hash,
                    changePassword;

                if (isAdminUser) {
                    hash = (userPassword.length > 32) ? hex_sha512(user.get('salt') + password) : hex_md5(user.get('salt') + password);
                } else {
                    hash = (userPassword.length > 32) ? hex_sha512(password + user.get('salt')) : hex_md5(password + user.get('salt'));
                }

                if (hash === userPassword) {
                    logger.operational('User {{ username }} with role {{ role }} successfully logged in.', { username: user.get('username'), role: user.get('role') });

                    user.get('salt') === 'temp' ? lStorage.setItem('changePassword', true) : lStorage.removeItem('changePassword');

                    this.activeUser = user;

                    lStorage.setItem('isAuthorized', true);

                    if (this.timerOn) {
                        this.setLastActive();
                        this.setTimeoutLastCheck();
                    }

                    // Record the time the user logged in
                    if (userPassword.length <= 32 && !isAdminUser) {
                        changePassword = hex_sha512(password + user.get('salt'));

                        user.changeCredentials({ password: changePassword, salt: user.get('salt') });
                    }
                    this.setLastLogin(user);

                    this.resetFailureCount(user);

                    // Resolve the promise, passing in the current user.
                    return user;
                }
            }
        });
    }

    getUserRole () {
        if (this.activeUser == null) {
            return false;
        }

        return LF.StudyDesign.roles.findWhere({ id: this.activeUser.get('role') });
    }
}
