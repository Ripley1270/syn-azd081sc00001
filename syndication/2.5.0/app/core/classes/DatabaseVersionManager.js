import DatabaseVersion from 'core/models/DatabaseVersion';
import DatabaseVersions from 'core/collections/DatabaseVersions';
import Logger from 'core/Logger';

const logger = new Logger('DatabaseVersionManager');

/**
 * A class to manage database version related opreations
 */
export default class DatabaseVersionManager {
    /**
     * The attributes for the database version records
     * @property {Array} versionAttributes
     * @readonly
     * @static
     */
    static get versionAttributes () {
        return [{
            versionName: 'DB_Core',
            version: LF.coreDbVersion
        }, {
            versionName: 'DB_Study',
            version: LF.StudyDesign.studyDbVersion
        }];
    }

    /**
     * Create database version records in the local database
     * @returns {Q.Promise<void>}
     */
    static createDatabaseVersions () {
        return DatabaseVersions.fetchCollection()
        .then((versions) => {
            // If there already exists version records skip record creation
            // The upgrade scripts will take care of updating version records after an update
            if (versions.length) {
                return Q();
            }

            return this.versionAttributes.reduce((chain, attributes) => {
                return chain
                .then(() => new DatabaseVersion(attributes).save())
                .catch((err) => {
                    err && logger.error('Error saving model for databaseVersion', err);
                });
            }, Q());
        })
        .then(() => ELF.trigger('DATABASE:VersionsReady'));
    }
}
