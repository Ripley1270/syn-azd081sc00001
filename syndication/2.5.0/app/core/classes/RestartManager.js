import { MessageRepo } from 'core/Notify';
import Logger from 'core/Logger';
import { rescheduleAlarms } from 'core/Helpers';

let diaryActive = false,
    transmissionActive = false,
    restartPending = false,
    restartTimeoutId = null,
    logger = new Logger('RestartManager');

export default class RestartManager {
    /**
     * Sets the value for the diaryActive flag.
     * @param {boolean} state - The value to be set for the diaryActive flag.
     * @returns {Q.Promise<Object>}
     * @example setDiaryActive(true);
     */
    static setDiaryActive (state) {
        logger.info(`setDiaryActive is called with ${state}`);

        diaryActive = (state === true);

        return RestartManager.restartIfIdle();
    }

    /**
     * Sets the value for the transmissionActive flag.
     * @param {boolean} state - The value to be set for the transmissionActive flag.
     * @returns {Q.Promise<Object>}
     * @example setTransmissionActive(true);
     */
    static setTransmissionActive (state) {
        logger.info(`setTransmissionActive is called with ${state}`);

        // Stop the restart countdown, if an unexpected transmission (e.g. autodial) is started while waiting for a restart
        RestartManager.stopRestartCountdown();

        transmissionActive = (state === true);

        return RestartManager.restartIfIdle();
    }

    /**
     * Sets the value for the restartPending flag.
     * @param {boolean} state - The value to be set for the restartPending flag.
     * @returns {Q.Promise<Object>}
     * @example setRestartPending(true);
     */
    static setRestartPending (state) {
        logger.info(`setRestartPending is called with ${state}`);

        restartPending = (state === true);

        return RestartManager.restartIfIdle();
    }

    /**
     * Closes all possible popups.
     * This function is a bad practice, it knows too much about modules & libraries
     * Any modificication within those modules or libraries could break functionality
     * These hiding logic should be moved into the modules & libraries
     */
    static closePopup () {
        // Close modal.
        $('#modal.in').modal('hide');

        // Close any modals, including number spinner modals
        $('.modal').modal('hide');

        // Close all mobiscroll widgets
        $('.mbsc-jqm').remove();

        // Close all modal backgrounds
        $('.modal-backdrop').remove();

        // Close Time/Date Picker
        $('input[data-role=datebox]').trigger('datebox', {
            method: 'close'
        });

        let $select2 = $('.select2-hidden-accessible');
        $select2.length && $select2.select2('close');

        $select2 = null;
    }

    /**
     * Checks if Android device needs to restart and restarts when out of diary or transmission. Only for Android < 5.
     * @return {Q.Promise<Object>}
     * @example restartIfIdle();
     */
    static restartIfIdle () {
        return Q.Promise((resolve, reject) => {
            let isDeviceIdle = restartPending && !transmissionActive && !diaryActive,
                countdown = LF.StudyDesign.timeZoneChangeRestartCountdown || LF.CoreSettings.timeZoneChangeRestartCountdown,
                restart = () => {
                    LF.Wrapper.exec({
                        execWhenWrapped: () => {
                            if (window.plugin && window.plugin.TimeZoneUtil) {
                                restartPending = false;

                                logger.info('Restarting now.');

                                window.plugin.TimeZoneUtil.restartApplication(resolve, (err) => {
                                    // It seems like even if the restart fails, we should still move forward.
                                    // So, we log an error and resolve.
                                    logger.error('Failed to restart application on Time zone change.', err)

                                    // In the case of a reload, we have to wait for the error log to be saved before resolving.
                                    .finally(resolve);
                                });
                            } else {
                                let err = new Error('TimeZoneUtil plugin is not available.');

                                reject(err);
                            }
                        },
                        execWhenNotWrapped: () => {
                            restartPending = false;
                            resolve();
                        }
                    });
                };

            if (!isDeviceIdle) {
                resolve();
                return;
            }

            logger.info('Restart will happen after the countdown.');

            RestartManager.closePopup();

            restartTimeoutId = window.setTimeout(restart, countdown);

            LF.Actions.notify({
                dialog: MessageRepo.Dialog && MessageRepo.Dialog.RESTART_NOTIFICATION
            })
            .then(() => {
                RestartManager.stopRestartCountdown();
                restart();
            })
            .done();
        });
    }

    /**
     * Stops the restart countdown
     */
    static stopRestartCountdown () {
        if (!restartTimeoutId) {
            return;
        }

        logger.info('Restart countdown has been stopped.');
        RestartManager.closePopup();
        window.clearTimeout(restartTimeoutId);
        restartTimeoutId = null;
    }

    /**
     * Reschedules the alarms
     * @returns {Q.Promise<Object>}
     * @example refreshActiveAlarms();
     */
    static refreshActiveAlarms () {
        return rescheduleAlarms();
    }
}

LF.Class.RestartManager = RestartManager;
