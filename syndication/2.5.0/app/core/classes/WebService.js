// jscs:disable disallowKeywords, requireTemplateStrings, requireArrowFunctions
import { timeStamp, parseTime } from 'core/DateTimeUtil';
import * as lStorage from 'core/lStorage';
import Logger from 'core/Logger';
import UserSync from 'core/classes/UserSync';
import Sites from 'core/collections/Sites';
import Subjects from 'core/collections/Subjects';
import { convertXmlToJson, filterCastModelData, zeroPad } from 'core/utilities';
import COOL from 'core/COOL';

let logger = new Logger('Core:WebService');

/**
 * A class that creates the WebService object.
 * @class WebService
 * @example LF.webService = new LF.Class.WebService();
 */
export default class WebService {
    /**
     * Returns the auhorization token
     * This method is overriden in LogPad App modality
     * @returns {Q.Promise<string>}
     */
    static getAuthorizationToken () {
        return Q('');
    }

    /**
     * Does the AJAX RESTful call to the Server.
     * @param {Object} ajaxConfig The data required to make AJAX call.
     * @param {Object} payload - The payload to be sent with the AJAX call.
     * @returns {Q.Promise<*>}
     * @example this.transmit({
     *      type: 'GET',
     *      uri: '/subjects/12345678'
     *  }, {});
     */
    transmit (ajaxConfig, payload = {}) {
        let authorization,
            deferred = Q.defer(),
            timeout = (LF.StudyDesign.transmissionTimeout || LF.CoreSettings.transmissionTimeout) * 1000;

        if (ajaxConfig.type == null) {
            throw new Error('Missing Argument: type is null or undefined.');
        } else if (ajaxConfig.uri == null) {
            throw new Error('Missing Argument: uri is null or undefined.');
        }

        COOL.getClass('WebService', WebService).getAuthorizationToken()
        .then((authorizationToken) => {
            let storedProc = payload && payload.StoredProc ? ` with stored procedure "${payload.StoredProc}"` : '',
                syncLevel = payload && payload.level ? ` for syncLevel "${payload.level}"` : '';

            if (ajaxConfig.auth || ajaxConfig.auth === '') {
                authorization = ajaxConfig.auth;
            } else {
                authorization = authorizationToken;
            }

            logger.operational(`Attempt to transmit ${ajaxConfig.type} ${ajaxConfig.uri}${storedProc}${syncLevel} started.`, {
                storedProc: payload.StoredProc,
                syncLevel: payload.level,
                method: ajaxConfig.type,
                uri: ajaxConfig.uri
            }, 'CommStart');

            $.ajax({
                type: ajaxConfig.type,
                url: ajaxConfig.uri,
                data: ajaxConfig.type === 'GET' ? '' : JSON.stringify(payload),
                dataType: ajaxConfig.dataType || 'json',
                contentType: 'application/json',
                timeout,
                cache: false,

                // jQuery is setting the context of this... Fat arrows don't work properly.
                success: _((res, status, jqXHR) => {
                    let isSubjectActive = jqXHR.getResponseHeader('X-Device-Status') == null ? 1 : +jqXHR.getResponseHeader('X-Device-Status'),
                        syncID = jqXHR.getResponseHeader('X-Sync-ID'),
                        isDuplicate = !!parseInt(jqXHR.getResponseHeader('X-Diary-Status'), 10);

                    let path = ajaxConfig.uri.replace(/https?:\/\/[^/]+\//, '');

                    // Only log up to 100 chars of response at OPERATIONAL level;
                    // keep in mind all this data gets POST'ed back to server
                    logger.operational(`Successful request ${ajaxConfig.type} ${path}`, {
                        uri: ajaxConfig.uri,
                        method: ajaxConfig.type,
                        response: (typeof res === 'string' ? res : JSON.stringify(res)).slice(0, 100)
                    }, 'CommEnd');

                    // But log the whole thing at TRACE level (goes to console only)
                    logger.trace(`Successful request ${ajaxConfig.type} ${path}`, {
                        response: typeof res === 'string' ? res : JSON.stringify(res)
                    });

                    deferred.resolve({ res, syncID, isSubjectActive, isDuplicate });
                }).bind(this),

                // jQuery is setting the context of this... Fat arrows don't work properly.
                error: _((jqXHR, status) => {
                    let isSubjectActive = jqXHR.getResponseHeader('X-Device-Status') == null ? 1 : +jqXHR.getResponseHeader('X-Device-Status'),
                        errorCode = jqXHR.getResponseHeader('X-Error-Code'),
                        httpCode = jqXHR.status,
                        swisError = errorCode ? ` and SWIS error: ${errorCode}` : '',
                        isTimeout = (status === 'timeout');

                    logger.operational(`Unsuccessful transmission ${ajaxConfig.type} ${ajaxConfig.uri} with HTTP code: ${httpCode} ${swisError}`, {}, 'CommFail');

                    deferred.reject({ errorCode, httpCode, isSubjectActive, isTimeout });
                }).bind(this),
                beforeSend: (xhr) => {
                    this.beforeSend({ xhr, authorization, ajaxConfig });
                }
            });
        })
        .done();

        return deferred.promise;
    }

    /**
     * Set any headers required for product transmissions.
     * @param {Object} opts - Options provided from WebService#transmit
     * @param {jQuery.xhr} opts.xhr - jQuery XHR object.
     * @param {string} opts.authorization - Authorization token.
     * @param {Object} opts.ajaxConfig - Ajax configuration generated by each individual transmit method.
     */
    beforeSend ({ xhr, authorization, ajaxConfig }) {
        logger.traceEnter('beforeSend');
        if (xhr) {
            let now = new Date();

            // If set as blank it will fail!
            if (authorization !== '') {
                // Note: Due to security concerns, we shouldn't log the authorization token to the console.
                xhr.setRequestHeader('Authorization', authorization);
            }

            logger.trace(`Request Header - Clientdate: ${now}`);
            xhr.setRequestHeader('Clientdate', timeStamp(now));

            if (ajaxConfig.syncID && ajaxConfig.syncID !== 'null') {
                logger.trace(`Request Header - X-Sync-ID: ${ajaxConfig.syncID}`);
                xhr.setRequestHeader('X-Sync-ID', ajaxConfig.syncID);
            }
            if (ajaxConfig.jsonh) {
                logger.trace('Request Header - Diary-Encoding: jsonm');
                xhr.setRequestHeader('Diary-Encoding', 'jsonm');
            }
            if (ajaxConfig.unlockCode) {
                // Note: due to security concerns, we shouldn't log the unlock code to the console.
                xhr.setRequestHeader('Unlock-Code', ajaxConfig.unlockCode);
            }
        }
        logger.traceExit('beforeSend');
    }

    /**
     * Build and return URL string.
     * @param {string} fragment - URL fragment to append to the end of the string.
     * @returns {string}
     */
    buildUri (fragment) {
        let serviceBase = lStorage.getItem('serviceBase');
        let apiBase = serviceBase != null ? `${serviceBase}/api` : LF.apiBase;

        return apiBase + fragment;
    }

    /**
     * Register a SitePad device.
     * @param {Object} payload - Payload passed to the transmit method.
     * @returns {Q.Promise<Object>}
     */
    registerDevice (payload) {
        logger.trace('registerDevice', payload);
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/devices2')
        };
        logger.trace('registerDevice', payload);

        return this.transmit(ajaxConfig, payload);
    }

    /**
     * Get a list of available sites that belong to a given study.
     * @param {string} studyName The name of the study to get sites for.
     * @returns {Q.Promise<Object>} A deferred promise.
     */
    getSites (studyName) {
        let uri = this.buildUri(`/v1/sites/${studyName}`);
        logger.trace(`getSites for ${studyName}`);

        return this.transmit({
            type: 'GET',
            uri
        });
    }

    /**
     * Send the new Patient using Transmission Queue.
     * @param {string} payload - The payload for the subject assignment API.
     * @param {string} auth - Authorization token.
     * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
     */
    sendSubjectAssignment (payload, auth) {
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/subjects/'),
            auth,
            jsonh: LF.StudyDesign.jsonh !== false
        };

        payload.source = this.getSource();

        return this.transmit(ajaxConfig, payload);
    }

    /**
     * Retrieve Subject's active status
     * @param {string} setupCode The Setup code of the subject.
     * @returns {Q.Promise<Object>}
     * @example LF.webservice.getSubjectActive('12345678');
     */
    getSubjectActive (setupCode) {
        let ajaxConfig;

        logger.trace(`getSubjectActive for setupCode:${setupCode}`);

        if (setupCode == null) {
            throw new Error('Missing Argument: setupCode is null or undefined.');
        }

        ajaxConfig = {
            type: 'GET',
            uri: this.buildUri(`/v1/subjects/${setupCode}`)
        };

        return this.transmit(ajaxConfig);
    }

    /**
     * Retrieve Subject's security question
     * @param {string} setupCode The Setup code of the subject.
     * @returns {Q.Promise<Object>}
     * @example LF.webservice.getSubjectQuestion('12345678');
     */
    getSubjectQuestion (setupCode) {
        let ajaxConfig;
        logger.trace(`getSubjectQuestion for ${setupCode}`);

        if (setupCode == null) {
            throw new Error('Missing Argument: setupCode is null or undefined.');
        }

        ajaxConfig = {
            type: 'GET',
            uri: this.buildUri(`/v1/devices?UserID=${setupCode}`)
        };

        return this.transmit(ajaxConfig);
    }

    /**
     * Do subject sync with Middle-Tier and get all non-DCF data back, and DCF data which are changed in SW.
     * @param {string} setupCode The Setup code of the subject.
     * @param {string} auth AJAX authorization token.
     * @returns {Q.Promise<Object>}
     * @example LF.webservice.doSubjectSync('12345678', '12345678:3bb7b3b1631b95534:4f4-17bd-38134c');
     */
    doSubjectSync (setupCode, auth) {
        let ajaxConfig;

        logger.trace(`doSubjectsync for ${setupCode}`);

        if (setupCode == null) {
            throw new Error('Missing Argument: setupCode is null or undefined.');
        }

        ajaxConfig = {
            type: 'GET',
            uri: this.buildUri(`/v1/subjects/${setupCode}`),
            auth,
            syncID: localStorage.getItem('PHT_syncID')
        };

        return this.transmit(ajaxConfig);
    }

    /**
     * Get subject data from Middle-Tier and get all non-DCF data back, and DCF data which are changed in SW.
     * @param {string} krpt - The krpt of the subject.
     * @param {string} auth - AJAX authorization token.
     * @returns {Q.Promise<Object>}
     * @example webservice.getSingleSubject('12345678', '12345678:3bb7b3b1631b95534:4f4-17bd-38134c');
     */
    getSingleSubject (krpt, auth) {
        let ajaxConfig;

        logger.trace(`getSingleSubject for ${krpt}`);

        if (krpt == null) {
            throw new Error('Missing Argument: krpt is null or undefined.');
        }

        ajaxConfig = {
            type: 'GET',
            uri: this.buildUri(`/v1/subjects/${krpt}`),
            auth
        };

        return this.transmit(ajaxConfig, {});
    }

    /**
     * Get all subject data for a site from Middle-Tier and get all non-DCF data back, and DCF data which are changed in SW.
     * @param {Object} params - Parameters passed into the method.
     * @param {string} params.auth AJAX authorization token.
     * @returns {Q.Promise<Array>}
     * @example LF.webservice.getAllSubjects({ auth: '12345678:3bb7b3b1631b95534:4f4-17bd-38134c' });
     */
    getAllSubjects (params = {}) {
        let ajaxConfig = {
                type: 'GET',
                uri: this.buildUri('/v1/subjects'),
                auth: params.auth || ''
            },
            convertSubjectIDFormat = function (patientID, siteCode) {
                let idFormat = LF.StudyDesign.participantSettings.participantIDFormat,
                    patientPortion = LF.StudyDesign.participantSettings.participantNumberPortion,
                    sitePortion = LF.StudyDesign.participantSettings.siteNumberPortion,
                    formattedSite,
                    formattedPatient,
                    fullFormatted;

                formattedSite = zeroPad(siteCode, sitePortion[1] - sitePortion[0]);
                formattedPatient = zeroPad(patientID, patientPortion[1] - patientPortion[0]);
                fullFormatted = '';

                // Walk the string and build our code from the idFormat with patient and site ranges spliced in.
                for (let i = 0, len = idFormat.length; i < len; ++i) {
                    if (i >= sitePortion[0] && i < sitePortion[1]) {
                        fullFormatted += formattedSite.charAt(i - sitePortion[0]);
                    } else if (i >= patientPortion[0] && i < patientPortion[1]) {
                        fullFormatted += formattedPatient.charAt(i - patientPortion[0]);
                    } else {
                        fullFormatted += idFormat.charAt(i);
                    }
                }

                return fullFormatted;
            };

        logger.trace('getAllSubjects');

        return Q.all([
            Sites.fetchCollection(),
            Subjects.fetchCollection(),
            this.transmit(ajaxConfig, {})
        ])
        // eslint-disable-next-line consistent-return
        .spread((sites, subjects, { res: incomingSubjects }) => {
            let site = sites.at(0),
                filteredSubjects = [];

            if (incomingSubjects.length) {
                for (let i = 0, len = incomingSubjects.length; i < len; i++) {
                    let incomingSubject = incomingSubjects[i],
                        subject = subjects.findWhere({ krpt: incomingSubject.krpt });

                    incomingSubject.subject_active = 1;
                    incomingSubject.site_code = site.get('siteCode');
                    incomingSubject.subject_number = incomingSubject.subjectId.substring(
                        LF.StudyDesign.participantSettings.participantNumberPortion[0],
                        LF.StudyDesign.participantSettings.participantNumberPortion[1]
                    );
                    incomingSubject.subject_id = convertSubjectIDFormat(incomingSubject.subject_number, incomingSubject.site_code);
                    incomingSubject.service_password = incomingSubject.password;
                    incomingSubject.subject_password = incomingSubject.clientPassword;
                    incomingSubject.secret_answer = incomingSubject.securityAnswer;

                    // If the incoming subject exists in the client and the phaseStartDateTZOffset is greater than
                    // the incoming timestamp then we ignore phase data for the subject. See DE19851
                    if (subject && new Date(subject.get('phaseStartDateTZOffset')) > new Date(incomingSubject.phaseStartDate)) {
                        delete incomingSubject.phase;
                    } else {
                        incomingSubject.phase = JSON.parse(incomingSubject.phase);
                        incomingSubject.phaseStartDateTZOffset = incomingSubject.phaseStartDate;
                    }

                    if (incomingSubject.setupCode) {
                        incomingSubject.setupCode = `${incomingSubject.setupCode}`;
                    }

                    if (incomingSubject.securityQuestion != null) {
                        incomingSubject.secret_question = `${incomingSubject.securityQuestion}`;
                    }

                    filteredSubjects.push(filterCastModelData(incomingSubject, 'Subject'));
                }
            }

            return { res: filteredSubjects };
        })
        .catch((err) => {
            err && logger.error('Error while syncing Subjects', err);
            return Q.reject(err);
        });
    }

    /**
     * Query the subject Data and always gets the data back if it is non-DCF data.
     * The Middle-Tier will return only data changed in SW if it is DCF data.
     * @param {string} setupCode The Setup code of the subject.
     * @param {string []} queryList An array of strings listing the subject's query fields.
     * @param {string} auth AJAX authorization token.
     * @returns {Q.Promise<Object>}
     * @example LF.webservice.querySubjectData('12345678', ['Phase', 'LogLevel', 'SiteCode'],
     *          '12345678:3bb7b3b1631b95534:4f4-17bd-38134c');
     */
    querySubjectData (setupCode, queryList, auth) {
        let ajaxConfig,
            index = 1,
            query;

        logger.trace(`querySubjectData for ${setupCode} list:${queryList}`);
        if (setupCode == null) {
            throw new Error('Missing Argument: setupCode is null or undefined.');
        }

        if (queryList == null) {
            throw new Error('Missing Argument: queryList is null or undefined.');
        }

        query = queryList.length ? `?fields=${queryList[0]}` : '';

        for (; index < queryList.length; index++) {
            query += `,${queryList[index]}`;
        }

        ajaxConfig = {
            type: 'GET',
            uri: this.buildUri(`/v1/subjects/${setupCode}${query}`),
            auth,
            syncID: localStorage.getItem('PHT_syncID')
        };

        return this.transmit(ajaxConfig);
    }

    /**
     * Updates the Subject's Data.
     * @param {string} deviceID The device ID for the subject.
     * @param {Object} subjectData The data needs to be updated.
     * @param {string} [auth] AJAX authorization token.
     * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
     * @example LF.webservice.updateSubjectData('4f4-17bd-38134c', {
     *  question  : 0,
     *  answer    : 'abcd',
     *  password  : 'qqqq'
     *  }, '12345678:3bb7b3b1631b95534:4f4-17bd-38134c');
     */
    updateSubjectData (deviceID, subjectData, auth) {
        let ajaxConfig;

        logger.trace(`updateSubjectData for deviceID:${deviceID}`, subjectData);
        if (deviceID == null) {
            throw new Error('Missing Argument: deviceID is null or undefined.');
        }

        ajaxConfig = {
            type: 'PUT',
            uri: this.buildUri(`/v1/devices/${deviceID}`),
            auth
        };

        return this.transmit(ajaxConfig, subjectData);
    }

    /**
     * Used to set the subject password, secret question and secret answer for the first time.
     * @param {Object} payload - Payload configuration
     * @param {string} payload.U Setup Code
     * @param {string} payload.W Password
     * @param {string} payload.Q Secret Question
     * @param {string} payload.A Secret Answer
     * @param {string} payload.O Client Name
     * @returns {Q.Promise<{ res, syncId, isSubjectActive, isDuplicate }>} Q promise
     */
    setSubjectData (payload) {
        logger.trace('setSubjectData', payload);

        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/devices/')
        };

        payload.O = this.getSource();

        return this.transmit(ajaxConfig, payload);
    }

    /**
     * Resets the subject password and secret QA with unlock code.
     * @param {Object} payload - Payload to be sent with the API request
     * @param {number} payload.setupCode - Setup Code a.k.a pin
     * @param {string} payload.clientPassword - Password to be set
     * @param {number} payload.regDeviceId - regDeviceId returned by Netpro during registration
     * @param {number} payload.clientId - clientId returned by Netpro during registration
     * @param {Array} payload.challengeQuestions - an array of an object for secret question and answers to be set
     * @param {Object} payload.challengeQuestions[0] - An object literal for secret question and answer
     * @param {number} payload.challengeQuestions[0].question - Secret question
     * @param {string} payload.challengeQuestions[0].answer - Secret answer
     * @param {string} krpt - krpt of the subject to change the credentials of
     * @param {string} unlockCode - Daily unlock code
     * @returns {Q.Promise<{ res, syncId, isSubjectActive, isDuplicate }>}
     */
    resetSubjectCredentials (payload, krpt, unlockCode) {
        logger.trace('resetSubjectCredentials', payload);

        payload.source = this.getSource();

        let ajaxConfig = {
            type: 'POST',
            unlockCode,
            uri: this.buildUri(`/v1/subjects/${krpt}/credentials`)
        };

        return this.transmit(ajaxConfig, payload);
    }

    /**
     * Resets the subject password
     * @param {Object} payload - Payload to be sent with the API request
     * @param {string} payload.clientPassword - Password to be set
     * @param {Array} payload.challengeQuestions - an array of an object for secret question and answers to be set
     * @param {Object} payload.challengeQuestions[0] - An object literal for secret question and answer
     * @param {number} payload.challengeQuestions[0].question - Secret question
     * @param {string} payload.challengeQuestions[0].answer - Secret answer
     * @param {string} krpt - krpt of the subject to change the credentials of
     * @returns {Q.Promise<{ res, syncId, isSubjectActive, isDuplicate }>}
     */
    resetSubjectPassword (payload, krpt) {
        logger.trace('resetSubjectPassword', payload);

        payload.source = this.getSource();

        let ajaxConfig = {
            type: 'PUT',
            uri: this.buildUri(`/v1/subjects/${krpt}/credentials`)
        };

        return this.transmit(ajaxConfig, payload);
    }

    /**
     * Gets subject setup code
     * @param {Object} krpt krpt of the subject of which setup code is to be returned
     * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
     */
    getSetupCode (krpt) {
        let ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin')
            },
            payload = {
                StoredProc: 'LPA_getSetupCode',
                Params: [krpt]
            },
            handleResult = ({ res }) => {
                if (!res) {
                    return Q.reject(`No setup code could be found for the krpt: ${krpt}`);
                }

                return Q(res);
            };

        return this.transmit(ajaxConfig, payload)
        .then(handleResult);
    }

    /**
     * Gets country code
     * @param {string} siteDom - The 'site_ID' attribute of the assigned Site. e.g. site.get('site_id").
     * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
     */
    getCountryCode (siteDom) {
        let ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/getDataClin')
            },
            payload = {
                StoredProc: 'NP_GetCountryCodeByKrDom',
                Params: [siteDom]
            },
            handleResult = ({ res }) => {
                if (!res) {
                    return Q.reject(`No country code could be found for the krdom: ${siteDom}`);
                }

                return Q(res);
            };

        return this.transmit(ajaxConfig, payload).then(handleResult);
    }

    /**
     * Get the device ID for the subject.
     * @param {Object} subjectData - The Subject's data used to retrieve the device ID.
     * @returns {Q.Promise<{ res, syncId, isSubjectActive, isDuplicate }>} Q promise
     * @example LF.webservice.getDeviceID({
     *     U: '12345678',
     *     W: 'qqqq'
     * })
     * .then(onSuccess)
     * .catch(onError);
     * @example LF.webservice.getDeviceID({
     *     U: '12345678',
     *     W: 'qqqq',
     *     code: 123456
     * });
     * .then(onSuccess)
     * .catch(onError);
     */
    getDeviceID (subjectData) {
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/devices'),
            unlockCode: subjectData.code ? subjectData.code : false
        };

        subjectData.O = this.getSource();

        logger.trace('getDeviceID for subjectData', subjectData);

        if (subjectData.code) {
            subjectData.code = undefined;
        }

        return this.transmit(ajaxConfig, subjectData);
    }

    /**
     * Send the completed Diary using Transmission Queue.
     * @param {Object} diary - The completed diary to be sent.
     * @param {string} auth - The authentication token
     * @param {boolean} jsonh - The value indicating whether jsonh compression is being used or not
     * @returns {Q.Promise<{ res, syncId, isSubjectActive, isDuplicate }>} Q promise
     * @example LF.webservice.sendDiary({
     *  "id"        : 1,
     *  "SU"        :'Meds',...,
     *  "Answers"   :[{
     *      "response"  : "1",
     *      "SW_Alias"  : "Meds.0.MEDS_Q_1",..., {...},{...}]
     *  }, true);
     */
    sendDiary (diary, auth, jsonh) {
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/diaries/'),
            jsonh
        };

        diary.source = this.getSource();

        // TODO: This looks like to be cecking if it is logpad. Change to isLogPad() or
        // better refocator it so that there is no need to check the running product
        if (!lStorage.getItem('apiToken')) {
            ajaxConfig.auth = auth;
        }

        return this.transmit(ajaxConfig, diary);
    }

    /**
     * Logs to be sent.
     * @param {Object} logs Logs generated to be sent.
     * @returns {Q.Promise<void>}
     * @example LF.webservice.sendLogs({
     *  "id"        : 1,...,
     *  "params"   :[{...},{...}]
     *  });
     */
    sendLogs (logs) {
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/logs/')
        };

        return this.transmit(ajaxConfig, logs);
    }

    /**
     * Gets last diary data from server by sending the stored procedure name and the required parameters
     * @param {Object} params Includes the authentication token
     * @returns {Q.Promise<void>}
     */
    syncLastDiary (params = {}) {
        logger.trace('syncLastDiary');
        let ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            },
            krpt = params.krpt || localStorage.getItem('krpt'),
            scheduleStart = new Date(parseTime('00:00', true)).toISOString(),
            unscheduledSuList = (LF.StudyDesign.lastDiarySyncUnscheduledSUList || []).join(',').match(/.{1,36}/g),
            payload = {
                StoredProc: 'lastDiaryTableSync_XML',
                Params: [krpt, scheduleStart].concat(unscheduledSuList)
            },
            handleResult = ({ res }) => {
                let data,
                    parseResult = (result) => {
                        let xml = $($.parseXML(result)).children()[0],
                            dataJson = convertXmlToJson(xml);

                        // DE19248: res = '<Results/>' instead of 'null' therefore dataJson = {}
                        return dataJson.diary || [];
                    };

                try {
                    data = (res !== 'null') ? parseResult(res) : [];
                } catch (err) {
                    logger.error(`Error converting lastDiary XML data to JSON. res: ${res}, err: ${err.toString()}`);
                }

                if (_.isArray(data)) {
                    if (params && params.activation) {
                        data = _.reject(data, item => item.deleted === '1');
                    }

                    _.each(data, (item) => {
                        let SU = item.SU,
                            questionnaire = LF.StudyDesign.questionnaires.where({ SU })[0];

                        if (params && params.activation) {
                            delete item.deleted;
                        }

                        if (item.role) {
                            item.role = JSON.stringify(item.role);
                        }

                        item.krpt = krpt;
                        item.questionnaire_id = questionnaire ? questionnaire.get('id') : SU;
                        delete item.SU;
                    });
                } else {
                    throw new Error('Incorrect result type: Result type should be an array');
                }

                return { res: data };
            };

        return this.transmit(ajaxConfig, payload)
        .then(handleResult);
    }

    /**
     * Send new user data.
     * @param {Object} params - Parameters passed into the method.
     * @param {string} params.user_type type of the user ['admin' || null]
     * @param {string} params.user_name user's name
     * @param {string} params.language user's language
     * @param {string} params.password user's password
     * @param {string} params.key salt for the user's password hash
     * @param {string} params.role role of the new user
     * @param {string} params.syncLevel level of synchronization
     * @param {string} params.syncValue value of the sync level
     * @param {string} [params.auth] authorization token for the webservice.
     * @returns {Q.promise<res, {syncId, isSubjectActive, isDuplicate}>} q promise
     * @example LF.webservice.addUser({
     *    auth      : subject.getAuth(),
     *    userType  : user.get('user_type'),
     *    userName  : user.get('user_name'),
     *    language  : user.get('language'),
     *    password  : user.get('password'),
     *    key       : user.get('key'),
     *    syncLevel : role.get('syncLevel'),
     *    syncValue : getSyncValue()
     *  });
     */
    addUser (params = {}) {
        logger.trace('addUser', params);
        let ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            },
            payload = {
                StoredProc: 'LPA_AddUser',
                Params: [
                    params.userType,
                    params.username,
                    params.language,
                    params.password,
                    params.salt,
                    params.secretQuestion || '',
                    params.secretAnswer || '',
                    params.role,
                    params.syncLevel,
                    params.syncValue,
                    params.active
                ]
            };

        return this.transmit(ajaxConfig, payload);
    }

    /**
     * Update existing user.
     * @param {Object} params - Parameters passed into the method.
     * @param {number} params.id - The ID of the user.
     * @param {string} params.userType - type of the user ['admin' || null]
     * @param {string} params.username - user's name
     * @param {string} params.language - user's language
     * @param {string} params.role - role of the new user
     * @param {string} params.syncLevel - level of synchronization
     * @param {string} params.syncValue - value of the sync level
     * @param {string} [params.auth] - authorization token for the webservice.
     * @returns {Q.promise<res, {syncId, isSubjectActive, isDuplicate}>} q promise
     * @example LF.webservice.updateUser({
     *    userId: user.get('userId'),
     *    userType: user.get('userType'),
     *    username: user.get('username'),
     *    role: user.get('role'),
     *    language: user.get('language'),
     *    active: user.get('active'),
     *    syncLevel,
     *    syncValue
     *  });
     */
    updateUser (params = {}) {
        logger.trace('updateUser', params);

        let ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            },
            payload = {
                StoredProc: 'synd_UpdateUser',
                Params: [
                    params.userId,
                    params.userType,
                    params.username,
                    params.language,
                    params.role,
                    params.syncLevel,
                    params.syncValue,
                    params.active
                ]
            };

        return this.transmit(ajaxConfig, payload);
    }

    /**
     * Update non-subject User's password and/or security question and answer
     * @param {Object} params - Parameters passed into the method.
     * @param {string} [params.auth] authorization token for the webservice.
     * @param {string} params.userId unique id of user to update
     * @param {string} params.password new password we want to send
     * @param {string} params.salt salt of the new password
     * @param {string} params.secretQuestion secretQuestion of user to update
     * @param {string} params.secretAnswer secretAnswer of user to update
     * @returns {Q.promise<res, {syncId, isSubjectActive, isDuplicate}>} q promise
     * @example LF.webservice.updateUserCredentials({
     *    userId         : '2',
     *    password       : 'newPassword'
     *    salt           : 'jash2u329udisf93',
     *    secretQuestion : '0',
     *    secretAnswer   : 'newAnswer'
     *  });
     */
    updateUserCredentials (params = {}) {
        let user = new LF.Model.User({ id: params.id });

        // Fetch the user by ID, fetch added as fix for DE18864.
        return user.fetch()
        .then(() => {
            logger.trace('updateUserCredentials');
            let ajaxConfig = {
                    type: 'POST',
                    uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                    auth: params.auth || null
                },
                payload = {
                    StoredProc: 'LPA_UpdateUserCredentials',
                    Params: [
                        params.userId || user.get('userId'),
                        params.password,
                        params.salt,
                        params.secretQuestion,
                        params.secretAnswer
                    ]
                };
            return this.transmit(ajaxConfig, payload);
        });
    }

    /**
     * Update Get all users that have been added/updated since PHT_lastUpdatedUsers or 0
     * @param {Object} params - Parameters passed into the method.
     * @param {string} params.auth - Authorization token.
     * @returns {Q.promise}
     * @example LF.webservice.syncUsers(params);
     */
    syncUsers (params = {}) {
        logger.trace('syncUsers', params);
        let getData,
            newLastUpdated,
            lastUpdated = lStorage.getItem('lastUpdatedUsers') || null,
            syncLevels = LF.StudyDesign.roles.getSyncLevels();

        getData = (level, value) => {
            let ajaxConfig = {
                    type: 'POST',
                    uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                    auth: params.auth || null
                },
                payload = {
                    level,
                    StoredProc: 'LPA_SyncUsers_XML',
                    Params: [lastUpdated, level, value]
                };

            return this.transmit(ajaxConfig, payload)
            // eslint-disable-next-line consistent-return
            .then(({ res }) => {
                // Stored Proc returns string 'null' if no users are found
                if (res && res !== 'null') {
                    let i,
                        len,
                        xml,
                        result,
                        users,
                        filteredUsers = [];

                    try {
                        // The actual code is the else block. If block is a workaround for unit tests.
                        if (typeof res !== 'string') {
                            result = res;
                        } else {
                            xml = $($.parseXML(res)).children()[0];
                            result = convertXmlToJson(xml);
                        }

                        users = result.user;
                        newLastUpdated = result.lastUpdated;
                    } catch (e) {
                        logger.error('User data retrieved is not a valid XML', res);
                        // eslint-disable-next-line no-throw-literal
                        throw '';
                    }

                    if (users) {
                        for (i = 0, len = users.length; i < len; i++) {
                            filteredUsers.push(filterCastModelData(users[i], 'User'));
                        }
                    }

                    return filteredUsers;
                }
            });
        };

        if (syncLevels.length === 0) {
            logger.operational('No SyncLevels found, skipping sync.');
            return Q({});
        }

        return Q.all(_.map(syncLevels, (syncLevel) => {
            return UserSync.getValue(syncLevel)
            .then((syncVal) => {
                if (syncVal || syncVal === '') {
                    return getData(syncLevel, syncVal);
                }

                if (syncVal === false) {
                    logger.operational(`No value for syncLevel: ${syncLevel}. Skipping sync`);
                }
                return Q.reject();
            });
        }))
        .then((users) => {
            lStorage.setItem('lastUpdatedUsers', newLastUpdated);

            return { res: _(users).chain().flatten().compact().value() };
        })
        .catch((err) => {
            logger.error('Error while syncing users', err);
            return Q.reject(err);
        });
    }

    /**
     * Gets admin password hash from server by sending the stored procedure name and the required parameters
     * @param {Object} params - Parameters passed into the method.
     * @param {string} params.auth - Authorization token.
     * @returns {Q.Promise<string>}
     */
    getAdminPassword (params = {}) {
        logger.trace('getAdminPassword');
        let ajaxConfig = {
            type: 'POST',
            uri: this.buildUri('/v1/SWAPI/GetDataClin'),
            auth: params.auth || null
        };

        return Q(JSON.parse(localStorage.getItem('PHT_site')))
        .then((siteData) => {
            let payload = {
                StoredProc: 'LPA_getRoleAccessCodes_XML',
                Params: [siteData.krdom]
            };

            LF.StudyDesign.useAcccessCodesFor1dot10 && payload.Params.push(1);

            return payload;
        })
        .then(payload => this.transmit(ajaxConfig, payload))
        .then(({ res }) => {
            let xml,
                adminRes;

            // The actual code is the else block. If block is a workaround for unit tests.
            if (typeof res !== 'string') {
                adminRes = res[0];
            } else {
                xml = $($.parseXML(res)).children()[0];
                adminRes = convertXmlToJson(xml).accessCode[0];
            }

            return { res: adminRes };
        })
        .catch((err) => {
            err && logger.error('Error retrieving admin password', err);
            return Q.reject(err);
        });
    }

    /**
     * Gets krdom from server by sending the stored procedure name and the required parameters
     * @param {Object} params - Parameters passed into the method.
     * @param {string} params.auth - Optional authorization token.
     * @returns {Q.Promise<string>}
     */
    getkrDom (params = {}) {
        logger.trace('getkrDom');
        let ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            },
            payload = {
                StoredProc: 'LPA_getKrdom_XML',
                Params: [localStorage.getItem('krpt')]
            };

        return this.transmit(ajaxConfig, payload)
        .then(({ res }) => {
            let xml,
                siteRes;

            // The actual code is the else block. If block is a workaround for unit tests.
            if (typeof res !== 'string') {
                siteRes = res[0];
            } else {
                xml = $($.parseXML(res)).children()[0];
                siteRes = convertXmlToJson(xml).site[0];
            }

            return { res: siteRes };
        })
        .catch((err) => {
            err && logger.error('Error retrieving krdom', err);
            return Q.reject(err);
        });
    }

    /**
     * Sends Edit Patient Diary data
     * @param {Object} params data to be sent
     * @param {Object} auth the authentication token
     * @returns {Q.Promise<res, { syncId, isSubjectActive, isDuplicate }>} Q promise
     */
    sendEditPatient (params, auth) {
        let ajaxConfig = {
            type: 'PUT',
            uri: this.buildUri(`/v1/subjects/${params.K}`),
            auth,
            jsonh: LF.StudyDesign.jsonh !== false
        };

        params.source = this.getSource();

        return this.transmit(ajaxConfig, params);
    }

    /**
     * Send krpt, krDom, lastUpdated, deviceId and get all visits and forms status for that site.
     * @param  {Object} params parameters
     * @param  {string} [params.auth] auth token
     * @param  {string} [params.krDom] site krDom
     * @param  {string} [params.lastUpdated] last update timestamp
     * @param  {string} [params.krpt] subject SW ID
     * @param  {string} [params.deviceId] device SW ID
     * @returns {Q.Promise}
     */
    syncUserVisitsAndReports (params = {}) {
        const ajaxConfig = {
                type: 'POST',
                uri: this.buildUri('/v1/SWAPI/GetDataClin'),
                auth: params.auth || null
            },
            krpt = params.krpt || null,
            lastUpdated = params.lastUpdated || null,
            deviceId = params.deviceId || lStorage.getItem('deviceId');

        const handleResult = ({ res }) => {
            let result;

            try {
                result = !res ? [] : convertXmlToJson($($.parseXML(res)).children()[0]);
            } catch (err) {
                logger.error('Error parsing Form result.', err);
            }

            return {
                res: {
                    subjects: result.subject || [],
                    lastRefreshTime: result.lastUpdated
                }
            };
        };

        return Sites.fetchFirstEntry()
        .then((site) => {
            const payload = {
                StoredProc: 'synd_SyncVisitsAndForms',
                Params: [params.krDom || site.get('site_id'), deviceId, krpt, lastUpdated]
            };
            return this.transmit(ajaxConfig, payload);
        })
        .then(handleResult);
    }

    /**
     * Get the time in utc from the server.
     * @param  {Object} params parameters
     * @returns {Q.Promise<Object>} Q Promise
     */
    getServerTime (params = {}) {
        const ajaxConfig = {
            type: 'GET',
            uri: this.buildUri('/v1/timezones/utc'),
            auth: params.auth || null
        };

        return this.transmit(ajaxConfig, params);
    }

    /**
     * Get the internet connection status by connecting to third party api.
     * @returns {Q.Promise<Object>} Q Promise
     */
    getInternetConnectionStatus () {
        const ajaxConfig = {
            type: 'GET',
            uri: LF.StudyDesign.tabletSetupConnectionCheckURL
        };

        return this.transmit(ajaxConfig);
    }
}

COOL.add('WebService', WebService);
