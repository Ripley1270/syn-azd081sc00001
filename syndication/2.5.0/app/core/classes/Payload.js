import COOL from 'core/COOL';
import Logger from 'core/Logger';

/**
 * Form Input
 * @typedef {Object} Payload~FormInput
 * @property {string} diary_id - The ID of the form as defined by the study design.
 * @property {string} SU - The signing unit of the form.
 * @property {string} completed - The completed date/time of the questionnaire. e.g. '2012-07-25T17:39:43Z'
 * @property {string} report_date - A formatted start date string (yyyy-mm-dd). ex. '2015-01-15'
 * @property {number} completed_tz_offset - The timezone offset at the time of questionnaire completion. e.g. -4
 * @property {string} phase - The phase of the subject at questionnaire completion. e.g. 10.
 * @property {number} phaseStartDateTZOffset -The timezone offset of the phase start date. e.g. -4
 * @property {string} change_phase - Determines if this diary should change the subject's phase. e.g. 'false'
 * @property {string} device_id - The ID of the device the questionnaire was completed on.
 * @property {string} core_version - The core version of the application at the time of questionnaire completion. e.g. '2.0.0'
 * @property {string} study_version - The study version at time of completion. e.g. '00.01'
 * @property {string} battery_level - The battery level at the time the questionnaire was taken.
 * @property {string} sig_id - A generated identifier that differs by product.
 * @property {Array<Payload~AnswerInput>} Answers - Answer records associated with the form.
 * @property {Payload~InkInput} ink - User signature.
 */

/**
 * Form Payload
 * @typedef {Object} Payload~Form
 * @property {string} I - The ID of the form as defined by the study design.
 * @property {string} U - The signing unit of the form.
 * @property {string} C - The completed date/time of the questionnaire. e.g. '2012-07-25T17:39:43Z'
 * @property {string} R - A formatted start date string (yyyy-mm-dd). ex. '2015-01-15'
 * @property {number} O - The timezone offset at the time of questionnaire completion. e.g. -4
 * @property {string} P - The phase of the subject at questionnaire completion. e.g. 10.
 * @property {number} T -The timezone offset of the phase start date. e.g. -4
 * @property {string} B - Determines if this diary should change the subject's phase. e.g. 'false'
 * @property {string} D - The ID of the device the questionnaire was completed on.
 * @property {string} E - The core version of the application at the time of questionnaire completion. e.g. '2.0.0'
 * @property {string} V - The study version at time of completion. e.g. '00.01'
 * @property {string} L - The battery level at the time the questionnaire was taken.
 * @property {string} J - A generated identifier that differs by product.
 * @property {Array<Payload~AnswerInput>} A - Answer records associated with the form.
 * @property {Payload~InkInput} [N] - User signature.
 */

/**
 * Edit Patient Input
 * @typedef {Object} Payload~EditPatientInput
 * @property {string} kprt - The KRPT of the target patient.
 * @property {string} ResponsibleParty - The party reponsible for the edits to the patient record. e.g. 'SysAdmin'
 * @property {string} dateStarted - The start date/time of the edit. e.g. '2012-07-25T17:39:43Z'
 * @property {string} dateCompleted - The completed date/time of the edit. e.g. '2012-07-25T17:39:43Z'
 * @property {string} reportDate - A formatted start date string (yyyy-mm-dd). ex. '2015-01-15'
 * @property {string} phase - The phase of the subject at edit completion. e.g. '10'.
 * @property {number} phaseStartDate -The timezone offset of the phase start date. e.g. -4
 * @property {string} coreVersion - The core version of the application at the time of edit completion. e.g. '2.0.0'
 * @property {string} studyVersion - The study version at time of completion. e.g. '00.01'
 * @property {string} sigId - A generated identifier that differs by product.
 * @property {string} batteryLevel - The battery level at the time the edits were made.
 * @property {Array<Payload~AnswerInput>} answers - Edits made to the patient record.
 * @property {Payload~InkInput} [ink] - Signature of the user that made the edits.
 */

/**
 * Edit Patient Payload
 * @typedef {Object} Payload~EditPatient
 * @property {string} K - The KRPT of the target patient.
 * @property {string} M - The party reponsible for the edits to the patient record. e.g. 'SysAdmin'
 * @property {string} S - The start date/time of the edit. e.g. '2012-07-25T17:39:43Z'
 * @property {string} U - The signing unit of the form. e.g. 'Assignment'
 * @property {string} C - The completed date/time of the edit. e.g. '2012-07-25T17:39:43Z'
 * @property {string} R - A formatted start date string (yyyy-mm-dd). ex. '2015-01-15'
 * @property {string} P - The phase of the subject at edit completion. e.g. '10'.
 * @property {string} E - The core version of the application at the time of edit completion. e.g. '2.0.0'
 * @property {string} V - The study version at time of completion. e.g. '00.01'
 * @property {string} L - The battery level at the time the edits were made.
 * @property {string} J - A generated identifier that differs by product.
 * @property {number} T -The timezone offset of the phase start date. e.g. -4
 * @property {Array<Payload~Answer>} A - Edits made to the patient record.
 * @property {Payload~Ink} [N] - Signature of the user that made the edits.
 */

/**
 * Subject Assignment Input
 * @typedef {Object} Payload~SubjectAssignmentInput
 * @property {string} kprt - The KRPT of the target subject.
 * @property {string} ResponsibleParty - The party reponsible for creating the new subject. e.g. 'SysAdmin'
 * @property {string} dateStarted - The start date/time of the assignment. e.g. '2012-07-25T17:39:43Z'
 * @property {string} dateCompleted - The completed date/time of the assignment. e.g. '2012-07-25T17:39:43Z'
 * @property {string} reportDate - A formatted start date string (yyyy-mm-dd). ex. '2015-01-15'
 * @property {string} subjectAssignmentPhase - The starting phase for the new subject. e.g. '10'.
 * @property {number} phaseStartDate -The timezone offset of the phase start date. e.g. -4
 * @property {string} coreVersion - The core version of the application at the time of assignment completion. e.g. '2.0.0'
 * @property {string} studyVersion - The study version at time of assignment. e.g. '00.01'
 * @property {string} sigId - A generated identifier that differs by product.
 * @property {string} batteryLevel - The battery level at the time the edits were made.
 * @property {Array<Payload~AnswerInput>} customSysVars - Custom patient data.
 * @property {Payload~InkInput} [ink] - Signature of the user creating the subject.
 * @property {string} initials - The subject's initials. 'ABC'
 * @property {string} patientId - The ID of the patient. e.g. '0100'
 * @property {string} enrollDate - The date the subject was enrolled in the study. e.g. '2015-12-08T13:05:36.000-05:00'
 * @property {string} TZValue - StudyWorks TimeZone ID. e.g. '21'
 * @property {string} language - The subject's language. e.g. 'en-US'
 * @property {string} affidavit - The ID of the affidavit displayed on the assignment form. e.g. 'SignatureAffidavit'
 * @property {string} SPStartDate - SitePad Start data of the subject.
 */

/**
 * Subject Assignment Payload
 * @typedef {Object} Payload~SubjectAssignment
 * @property {string} K - The KRPT of the target subject.
 * @property {string} M - The party reponsible for creating the new subject. e.g. 'SysAdmin'
 * @property {string} U - Signing Unit of the form. Always 'Assignment'
 * @property {string} S - The start date/time of the assignment. e.g. '2012-07-25T17:39:43Z'
 * @property {string} C - The completed date/time of the assignment. e.g. '2012-07-25T17:39:43Z'
 * @property {string} R - A formatted start date string (yyyy-mm-dd). ex. '2015-01-15'
 * @property {string} P - The starting phase for the new subject. e.g. '10'.
 * @property {number} T -The timezone offset of the phase start date. e.g. -4
 * @property {string} E - The core version of the application at the time of assignment completion. e.g. '2.0.0'
 * @property {string} V - The study version at time of assignment. e.g. '00.01'
 * @property {string} J - A generated identifier that differs by product.
 * @property {string} L - The battery level at the time the edits were made.
 * @property {Array<Payload~AnswerInput>} A - Subject data such as initials, language, etc...
 * @property {Payload~InkInput} [N] - Signature of the user creating the subject.
 */

/**
 * Ink Payload
 * @typedef {Object} Payload~Ink
 * @property {Array<Number>} D - The captured signature data.
 * @property {number} X - The width of the signature canvas.
 * @property {number} Y - The height of the signature canvas.
 * @property {string} H - The author of the signature.
 */

/**
 * Ink Input
 * @typedef {Object} Payload~InkInput
 * @property {Array<Number>} signatureData - The captured signature data.
 * @property {number} xSize - The width of the signature canvas.
 * @property {number} ySize - The height of the signature canvas.
 * @property {string} author - The author of the signature.
 */

/**
 * Answer Payload
 * @typedef {Object} Payload~Answer
 * @property {string} G - The user's response.
 * @property {string} F - The SW Alias of the question answered.
 * @property {string} Q - The ID of the question answered.
 */

/**
 * Answer Input
 * @typedef {Object} Payload~AnswerInput
 * @property {string} response - The user's response.
 * @property {string} SW_Alias - The SW Alias of the question answered.
 * @property {string} question_id - The ID of the question answered.
 */

/**
 * Reset Password Payload
 * @typedef {Object} Payload~ResetPassword
 * @property {string} W - The user's new password.
 * @property {string} Q - The user's selected security question.
 * @property {string} A - The user's answer to the selected security question.
 */

/**
 * Reset Password Input
 * @typedef {Object} Payload~ResetPasswordInput
 * @property {string} password - The user's new password.
 * @property {string} secret_question - The user's selected security question.
 * @property {string} secret_answer - The user's answer to the selected security question.
 */

/**
 * Reset Secret Question Payload
 * @typedef {Object} Payload~ResetSecretQuestion
 * @property {string} Q - The user's selected security question.
 * @property {string} A - The user's answer to the selected security question.
 */

/**
 * Reset Secret Question Input
 * @typedef {Object} Payload~ResetSecretQuestionInput
 * @property {string} secret_question - The user's selected security question.
 * @property {string} secret_answer - The user's answer to the selected security question.
 */

const logger = new Logger('core/Payload');

/**
 * Utility class to generate HTTP request payloads.  All input and output should be JSON.
 */
export default class Payload {
    /**
     * Generate a form transmission payload.
     * @param {Payload~FormInput} form - Form data to format.
     * @returns {Payload~Form}
     */
    static questionnaire (form) {
        let payload = {
                I: form.diary_id,
                U: form.SU,
                S: form.started,
                C: form.completed,
                R: form.report_date,
                O: form.completed_tz_offset,
                P: form.phase,
                T: form.phaseStartDateTZOffset,
                B: form.change_phase,
                D: form.device_id,
                E: form.core_version,
                V: form.study_version,
                L: form.battery_level,
                J: form.sig_id
            },
            useJSONH = LF.StudyDesign.jsonh !== false;

        logger.traceEnter('Payload#questionnaire');

        payload.A = _.map(form.Answers, (answer) => {
            let answerPayload = this.answer(answer);

            if (answer.type) {
                answerPayload.T = answer.type;
                useJSONH = false;
            }

            return answerPayload;
        });

        logger.trace(`Setting payload.A: ${payload.A}`);

        if (form.ink) {
            let ink = JSON.parse(form.ink);
            payload.N = this.ink(ink);
        }

        // compress answers collection with JSONH
        if (useJSONH) {
            payload.A = this.pack(payload.A);
        }

        logger.traceExit('Payload#questionnaire');

        return { payload, useJSONH };
    }

    /**
     * Generates an array of answers.
     * @param {Array<Payload~AnswerInput>} answerInput - Object to format.
     * @returns {Array<Payload~Answer>}
     */
    static answers (answers) {
        return _.map(answers, answer => this.answer(answer));
    }

    /**
     * Generates an answer payload.
     * @param {Payload~AnswerInput} answerInput - Object to format.
     * @returns {Payload~Answer}
     */
    static answer ({ response, SW_Alias, question_id }) {
        return {
            G: response,
            F: SW_Alias,

            // Custom SysVars don't have a question_id so we pass an empty string.
            Q: question_id || ''
        };
    }

    /**
     * Generate a signature Payload.
     * @param {Payload~InkInput} inkInput - User's signature data.
     * @returns {Payload~Ink}
     */
    static ink ({ signatureData, xSize, ySize, author }) {
        return {
            D: signatureData,

            // DE21158 - Unable to transmit subject assignment form using eCoa Web app.
            // xSize and ySize were being transmitted with a decimal. e.g. 192.0102
            // StudyWorks was rejecting the transmissions based on this.
            X: Math.floor(xSize),
            Y: Math.floor(ySize),
            H: author
        };
    }

    /**
     * Generate the payload for the Edit Patient form.
     * @param {Payload~EditPatientInput} form - Edit Patient form data.
     * @returns {Payload~EditPatient}
     */
    static editPatient (form) {
        let payload = {
            K: form.krpt,
            M: form.ResponsibleParty,
            U: 'Assignment',
            S: form.dateStarted,
            C: form.dateCompleted,
            R: form.reportDate,
            P: form.phase,
            E: LF.coreVersion,
            V: LF.StudyDesign.studyVersion,
            T: form.phaseStartDate,
            L: form.batteryLevel,
            J: form.sigID
        };

        payload.A = this.answers(form.answers);

        if (form.ink) {
            payload.N = this.ink(form.ink);
        }

        payload.A = this.pack(payload.A);

        return payload;
    }

    /**
     * Generates a reset password payload.
     * @param {Payload~ResetPasswordInput} params - Reset password parameters.
     * @returns {Payload~ResetPassword}
     */
    static resetPassword (params) {
        return {
            W: params.password,
            Q: params.secret_question,
            A: params.secret_answer
        };
    }

    /**
     * Generates a reset secret (security) question payload.
     * @param {Payload~ResetSecretQuestionInput} params - Reset secret question parameters.
     * @returns {Payload~ResetSecretQuestion}
     */
    static resetSecretQuestion (params) {
        return {
            Q: params.secret_question,
            A: params.secret_answer
        };
    }

    /**
     * Compress the provided JSON if the study is configured to.
     * @param {Array} target - The target JSON to compress.
     * @returns {Array} The compressed JSON.
     */
    static pack (target) {
        // compress answers collection with JSONH
        if (LF.StudyDesign.jsonh !== false) {
            logger.trace('Remapped after JSONH.pack');

            return JSONH.pack(target);
        }

        return target;
    }

    /**
     * Generates subject assignment payload.
     * @param {Payload~SubjectAssignmentInput} form - Data from the subject assignment form.
     * @returns {Payload~SubjectAssignment}
     */
    static subjectAssignment (form) {
        let payload = {
            K: form.krpt,
            M: form.responsibleParty,
            U: 'Assignment',
            S: form.dateStarted,
            C: form.dateCompleted,
            R: form.reportDate,
            P: form.subjectAssignmentPhase,
            E: form.coreVersion || LF.coreVersion,
            V: LF.StudyDesign.studyVersion,
            T: form.phaseStartDate,
            L: form.batteryLevel,
            J: form.sigID,
            A: this.answers([{
                response: form.initials,
                SW_Alias: 'PT.Initials'
            }, {
                response: form.patientId,
                SW_Alias: 'PT.Patientid'
            }, {
                response: form.enrollDate,
                SW_Alias: 'PT.EnrollDate'
            }, {
                response: form.TZValue,
                SW_Alias: 'SU.TZValue'
            }, {
                response: form.language,
                SW_Alias: 'Assignment.0.Language'
            }, {
                response: '',
                SW_Alias: form.affidavit,
                question_id: 'AFFIDAVIT'
            }, {
                response: form.SPStartDate,
                SW_Alias: 'PT.SPStartDate'
            }])
        };

        if (form.ink) {
            payload.N = this.ink(form.ink);
        }

        if (form.customSysVars) {
            _.each(form.customSysVars, (sysVar) => {
                payload.A.push(this.answer(sysVar));
            });
        }

        payload.A = this.pack(payload.A);

        return payload;
    }
}

COOL.add('Payload', Payload);
