/* eslint-disable */
// This file isn't process via babelify. eslint is disabled until file is updated.

/** #depends application.js
 * @file eSense utility functions
 * @author <a href="mailto:dyildiz@phtcorp.com">Dogus Yildiz</a>
 * @version 1.8.0
 */

/**
 * Returns if the eSense plugin exists or not
 * @returns {Boolean} Returns true if there exists the eSense plugin. Else returns false;
 */
LF.Wrapper.Utils.eSense.pluginExists = function () {
    return !!window.plugin && !!window.plugin.eSense;
};

/**
 * Checks if the eSense plugin exists and calls the appropriate callback.
 * If the plugin doesn't exist it will log an error and show plugin missing message
 * @param {Function} onPluginExists This callback is called if the plugin exists
 * @param {Function} onPluginMissing This callback is called if the plugin is missing
 */
LF.Wrapper.Utils.eSense.checkPlugin = function (onPluginExists, onPluginMissing) {

    onPluginExists = onPluginExists || $.noop;
    onPluginMissing = onPluginMissing || $.noop;

    if (LF.Wrapper.Utils.eSense.pluginExists()) {
        onPluginExists();
    } else {
        LF.Actions.notify({ message: 'PLUGIN_MISSING' }, function () {
            LF.Actions.removeMessage()
            .then(onPluginMissing)
            .done();
        });
    }
};

/**
 * Checks if the eSense API is supported on this platform. If not supported displays a notification.
 * @param {Function} onSupported The callback function to continue app life cycle if eSense API is supported
 * @param {Function} onNotSupported The callback function to continue app life cycle if eSense API is NOT supported
 */
LF.Wrapper.Utils.eSense.checkSupport = function (onSupported, onNotSupported) {
    onSupported = onSupported || $.noop;
    onNotSupported = onNotSupported || function () {
        LF.Actions.notify({ message: 'DIARY_REQUIRES_E_SENSE_SUPPORT' }, function () {
            LF.Actions.removeMessage();

            localStorage.setItem('questionnaireToDashboard', true);
            LF.router.navigate('dashboard', true);
        });
    };

    if (LF.Wrapper.Utils.eSense.pluginExists() && plugin.eSense.isSupported()) {
        onSupported();
    } else {
        onNotSupported();
    }
};

/**
 * Checks the paired devices to see if a specific type of eSense device is paired with the app
 * @param {String} type The type of the eSense device to be checked
 * @param {Function} onPaired The callback function to be called if there exists a paired eSense device. eSense device object is passed to this callback
 * @param {Function} onNotPaired The callback function to be called if there is no eSense device paired.
 */
LF.Wrapper.Utils.eSense.checkPaired = function (type, onPaired, onNotPaired) {

    onPaired = onPaired || $.noop;
    onNotPaired = onNotPaired || function () {
        LF.Actions.notify({ message: 'E_SENSE_NOT_PAIRED' }, function () {

            LF.Actions.removeMessage();

            localStorage.setItem('questionnaireToDashboard', true);
            LF.router.navigate('dashboard', true);
        });
    };

    LF.Wrapper.Utils.eSense.getPairedDevice(type, function (device) {
        if (!!device) {
            onPaired(device);
        } else {
            onNotPaired();
        }
    });
};

/**
 * Enables eSense.
 * @param {Function} onSuccess The callback function to be called if eSense is successfully enabled
 * @param {Function} onError The callback function to be called if eSense fails to be enabled
 */
LF.Wrapper.Utils.eSense.enable = function (onSuccess, onError) {

    onError = onError || function () {
        LF.Actions.notify({ message: 'ERROR_E_SENSE_ENABLE' }, function () {
            LF.Actions.removeMessage();

            localStorage.setItem('questionnaireToDashboard', true);
            LF.router.navigate('dashboard', true);
        });
    };

    LF.Wrapper.Utils.eSense.checkPlugin(function () {
        plugin.eSense.enable(onSuccess, onError);
    }, onError);
};

/**
 * Makes a connection to eSense device
 * @param {Function} onSuccess The callback function to be called after a successful connection to eSense device
 * @param {Function} onError The callback function to be called after connection attempt to eSense device fails
 * @param {Object} device Backbone model for the device to be connected
 */
LF.Wrapper.Utils.eSense.connect = function (onSuccess, onError, device) {
    var timeout = LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.apiCallTimeout') || LF.CoreSettings.eSense.apiCallTimeout;

    onError = onError || function () {
        LF.Wrapper.Utils.eSense.turnOffBluetooth(function () {
            LF.Actions.notify({
                header: 'ERROR_TITLE',
                message: ['BLUETOOTH_SYNC_FAILURE_TOP', 'BLUETOOTH_SYNC_FAILURE_MIDDLE']
            }, LF.Actions.removeMessage);
        });
    };

    LF.Wrapper.Utils.eSense.checkPlugin(function () {
        plugin.eSense.connect(function () {
            LF.Data.connectedESenseDevice = device;

            onSuccess();
        }, onError, device.get('address'), true, timeout);
    }, onError);
};

/**
 * Closes the connection with eSense device
 * @param {Function} callback The callback function to continue app life cycle after a successful or a failed disconnect
 */
LF.Wrapper.Utils.eSense.disconnect = function (callback) {

    callback = callback || $.noop;

    LF.Wrapper.Utils.eSense.checkPlugin(function () {
        plugin.eSense.disconnect(callback, callback);
    }, callback);
};

/**
 * Compares LogPad App time with eSense device time.
 * If the difference is more then the threshold, updates the time of eSense device.
 * @param {Function} onSuccess The callback function to be called after a successful check or update
 * @param {Function} onError The callback function to be called on an error case
 */
LF.Wrapper.Utils.eSense.checkUpdateTime = function (onSuccess, onError) {
    var timeout = LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.apiCallTimeout') || LF.CoreSettings.eSense.apiCallTimeout;

    onError = onError || $.noop;

    LF.Wrapper.Utils.eSense.checkPlugin(function () {
        plugin.eSense.getTime(function (result) {
            var nowISOStamp,
                now = new Date();

            if (Math.abs(now - Date.parse(result)) > LF.CoreSettings.eSense.thresholdForTimeUpdate) {
                nowISOStamp = now.ISOStamp();

                plugin.eSense.setTime(onSuccess, onError, nowISOStamp, timeout);
            } else {
                onSuccess();
            }
        }, onError, timeout);
    }, onError);
};

/**
 * Gets all records from eSense device
 * @param {Function} onResult The callback function to be called after a successful retrieval of records
 * @param {Function} onError The callback function to be called on an error case
 */
LF.Wrapper.Utils.eSense.getAllRecords = function (onResult, onError) {
    var timeout = LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.getRecordsTimeout') || LF.CoreSettings.eSense.getRecordsTimeout;

    LF.Wrapper.Utils.eSense.checkPlugin(function () {
        LF.Wrapper.Utils.eSense.checkUpdateTime(function () {
            plugin.eSense.getAllRecords(onResult, onError, timeout);
        }, onError);
    }, onError);
};

/**
 * Gets a number of latest records from eSense device
 * @param {Function} onResult The callback function to be called after a successful retrieval of records
 * @param {Function} onError The callback function to be called on an error case
 * @param {Number} numberOfRecords The number of records to be retrieved
 */
LF.Wrapper.Utils.eSense.getLatestRecords = function (onResult, onError, numberOfRecords) {
    var timeout = LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.getRecordsTimeout') || LF.CoreSettings.eSense.getRecordsTimeout;

    LF.Wrapper.Utils.eSense.checkPlugin(function () {
        LF.Wrapper.Utils.eSense.checkUpdateTime(function () {
            plugin.eSense.getLatestRecords(onResult, onError, numberOfRecords, timeout);
        }, onError);
    }, onError);
};

/**
 * Gets the records from eSense device, which recorded after a specific record
 * @param {Function} onResult The callback function to be called after a successful retrieval of records
 * @param {Function} onError The callback function to be called on an error case
 * @param {Object} lastRecord The record object, after which the records to be retrieved are recorded
 */
LF.Wrapper.Utils.eSense.getNewRecords = function (onResult, onError, lastRecord) {
    var timeout = LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.getRecordsTimeout') || LF.CoreSettings.eSense.getRecordsTimeout;

    LF.Wrapper.Utils.eSense.checkPlugin(function () {
        LF.Wrapper.Utils.eSense.checkUpdateTime(function () {
            plugin.eSense.getNewRecords(onResult, onError, lastRecord, timeout);
        }, onError);
    }, onError);
};

/**
 * Deletes all records from the eSense device
 * @param {Function} onSuccess The callback function to be called after a successful deletion of records
 * @param {Function} onError The callback function to be called on an error case
 */
LF.Wrapper.Utils.eSense.deleteRecords = function (onSuccess, onError) {
    var timeout = LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.apiCallTimeout') || LF.CoreSettings.eSense.apiCallTimeout;

    LF.Wrapper.Utils.eSense.checkPlugin(function () {
        plugin.eSense.deleteRecords(onSuccess, onError, timeout);
    }, onError);
};

/**
 * Gets the paired device data from the ESenseDevices table
 * @param {String} type The type of the eSense device
 * @param {Function} callback The callback function to continue app life cycle
 */
LF.Wrapper.Utils.eSense.getPairedDevice = function (type, callback) {
    var eSenseDevices = new LF.Collection.ESenseDevices();

    eSenseDevices.fetch({
        onSuccess: function () {
            var model = null,
                devices = eSenseDevices.match({
                    type: type
                });

            if (devices) {
                model = devices.reduce(function (device1, device2) {
                    var pairDate1 = new Date(device1.get('pairDate')),
                        pairDate2 = new Date(device2.get('pairDate'));

                    return pairDate1 > pairDate2 ? device1 : device2;
                });
            }
            callback(model);
        },
        onError : function (error) {
            callback(null);
        }
    });
};

/**
 * Gets the battery state of the eSense device
 * @param {Function} onSuccess The callback function to be called after a successful retrieval of battery state
 * @param {Function} onError The callback function to be called on an error case
 */
LF.Wrapper.Utils.eSense.getBatteryState = function (onSuccess, onError) {
    var timeout = LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.apiCallTimeout') || LF.CoreSettings.eSense.apiCallTimeout;

    onError = onError || $.noop;

    LF.Wrapper.Utils.eSense.checkPlugin(function () {
        plugin.eSense.getBatteryState(onSuccess, onError, timeout);
    }, onError);
};

/**
 * Turn Bluetooth off on the device if configured
 * @param {Function} callback The callback function to continue app life cycle after a successful or a failed disable
 */
LF.Wrapper.Utils.eSense.turnOffBluetooth = function (callback) {

    callback = callback || $.noop;

    if (LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.disableBluetooth') || LF.CoreSettings.eSense.disableBluetooth) {
        LF.Wrapper.Utils.eSense.checkPlugin(function () {
            plugin.eSense.disable(callback, callback);
        }, callback);
    } else {
        callback();
    }
};

/**
 * This function is called after pairing is complete.
 * @param {Object} device The name of the device paired
 * @param {Function} callback Callback function to continue app life cycle
 */
LF.Wrapper.Utils.eSense.onESensePaired = function (deviceName, callback) {
    var am1PlusTimeWindowSettings, command,
        timeout = LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.apiCallTimeout') || LF.CoreSettings.eSense.apiCallTimeout;

    callback = callback || $.noop;

    if (deviceName.indexOf('AM1') === 0) {
        am1PlusTimeWindowSettings = LF.Utilities.getNested('LF.StudyDesign.eSenseConfig.AM1Plus.timeWindow') || LF.CoreSettings.eSense.AM1Plus.timeWindow;
        command = {
            command: 'setTimeWindow',
            data: am1PlusTimeWindowSettings
        };

        plugin.eSense.sendCustomCommand(callback, callback, command, timeout);
    } else {
        callback();
    }
};

/**
 * Unpair with the given device
 * @param {String} address The Bluetooth address of the device to unpair with
 * @param {Function} callback Callback function to continue app life cycle
 */
LF.Wrapper.Utils.eSense.unPairDevice = function (address, callback) {

    if (address) {
        // Unpair at the OS level
        plugin.eSense.unpair(callback, callback, address);
    } else {
        callback();
    }
};
