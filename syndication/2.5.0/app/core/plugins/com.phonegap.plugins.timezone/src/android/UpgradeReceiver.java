package com.phonegap.plugins.timezone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class UpgradeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        final SharedPreferences options = context.getSharedPreferences("LogPad", Context.MODE_PRIVATE);
        options.edit().putBoolean("TIMEZONE_SET", false).commit();
    }

}
