package com.phtcorp.cordova.plugins.battery;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

public class BatteryListener extends CordovaPlugin {

    private static final String LOG_TAG = "BatteryManager";

    BroadcastReceiver receiver;

    private int batteryLevel;
    private boolean isPlugged;

    /**
     * Constructor.
     */
    public BatteryListener() {
        this.receiver = null;
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              JSONArry of arguments for the plugin.
     * @param callbackContext   The callback id used when calling back into JavaScript.
     * @return                  True if the action was valid, false if not.
     */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        if (action.equals("getBatteryInfo")) {

            // We need to listen to power events to update battery status
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
            if (this.receiver == null) {
                getBatteryInfoOnce();

                this.receiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        populateBatteryInfo(intent);
                    }
                };
                cordova.getActivity().registerReceiver(this.receiver, intentFilter);
            }

            callbackContext.success(this.getBatteryInfoJSON());

            return true;
        }

        return false;
    }

    /**
     * Stop battery receiver.
     */
    public void onDestroy() {
        removeBatteryListener();
    }

    /**
     * Stop battery receiver.
     */
    public void onReset() {
        removeBatteryListener();
    }

    /**
     * Stop the battery receiver and set it to null.
     */
    private void removeBatteryListener() {
        if (this.receiver != null) {
            try {
                this.cordova.getActivity().unregisterReceiver(this.receiver);
                this.receiver = null;
            } catch (Exception e) {
                Log.e(LOG_TAG, "Error unregistering battery receiver: " + e.getMessage(), e);
            }
        }
    }

    /**
     * Gets the battery information once.
     */
    private void getBatteryInfoOnce() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryIntent = cordova.getActivity().registerReceiver(null, intentFilter);
        populateBatteryInfo(batteryIntent);
    }

    /**
     * Populates the members with the current battery information
     *
     * @param batteryIntent the current battery information
     */
    private void populateBatteryInfo(Intent batteryIntent) {
        this.batteryLevel = batteryIntent.getIntExtra(android.os.BatteryManager.EXTRA_LEVEL, 0);
        this.isPlugged = batteryIntent.getIntExtra(android.os.BatteryManager.EXTRA_PLUGGED, -1) > 0 ? true : false;
    }

    /**
     * Creates a JSONObject with the current battery information
     *
     * @return a JSONObject containing the battery information
     */
    private JSONObject getBatteryInfoJSON() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("batteryLevel", this.batteryLevel);
            obj.put("isPlugged", this.isPlugged);
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        }
        return obj;
    }

}
