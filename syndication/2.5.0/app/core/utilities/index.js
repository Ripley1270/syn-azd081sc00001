import * as coreUtilities from './coreUtilities';
import * as languageExtensions from './languageExtensions';
import * as localizationUtils from './localizationUtils';
import * as fileUtilities from './fileUtilities';
import * as select2Configurations from './select2Configurations';

export default _.extend(coreUtilities, languageExtensions, localizationUtils, fileUtilities, select2Configurations);
