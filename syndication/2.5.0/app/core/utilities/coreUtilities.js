/* eslint-disable no-bitwise */
import * as lStorage from 'core/lStorage';
import Logger from 'core/Logger';
import TimeTravel from 'core/TimeTravel';
import COOL from 'core/COOL';

// This was blessed by Sal, since it is a simple dependency for a specific function
import emojiStrip from 'emoji-strip';

// STOP! Please do not add any more dependencies (imports) here.
// Rather, see if you can REMOVE some. Please discuss with Sal
// or Dave if you think you want to add an import here.

let logger = new Logger('Utilities');

/**
 * Contains the utility functions that will be registered to COOL.
 * @class Utilities
 */
export class Utilities {
    /**
     * Check if online using navigator.online if in browser, phonegap plugin if wrapped
     * @param {function} callback A callback function invoked upon completion.
     * @returns {Q.Promise<boolean>}
     */
    static isOnline (callback = $.noop) {
        // TODO: This function needs a good refactoring.

        let deferred = Q.defer(),
            resolve = () => {
                callback(true);
                deferred.resolve(true);
            },
            reject = () => {
                callback(false);
                deferred.resolve(false);
            };

        LF.Wrapper.exec({
            execWhenWrapped () {
                let platform = LF.Wrapper.platform;
                switch (platform) {
                    case 'windows': {
                        let connectionLevel,
                            networkInfo = Windows.Networking.Connectivity.NetworkInformation,
                            connectionProfile = networkInfo.getInternetConnectionProfile();

                        if (connectionProfile) {
                            connectionLevel = connectionProfile.getNetworkConnectivityLevel();
                            if (connectionLevel === Windows.Networking.Connectivity.NetworkConnectivityLevel.internetAccess) {
                                resolve();
                            } else {
                                reject();
                            }
                        } else {
                            reject();
                        }
                        break;
                    }
                    case 'android':
                        if (navigator && navigator.connection) {
                            LF.Wrapper.getConnectionType() === 'none' ? reject() : resolve();
                        } else {
                            (window.navigator.onLine ? resolve : reject)();
                        }
                        break;
                    default:
                        (window.navigator.onLine ? resolve : reject)();
                        break;
                }
            },
            execWhenNotWrapped: window.navigator.onLine ? resolve : reject
        });

        return deferred.promise;
    }
}

COOL.add('Utilities', Utilities);

/**
 * Determines the data type of the passed argument.
 * @param {Object} obj Object to retrieve data type.
 * @returns {string} The object type.
 * @example LF.Utilities.toType({}); // 'object'
 * @example LF.Utilities.toType([]); // 'array'
 */
export function toType (obj) {
    return {}.toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
}

LF.Utilities.toType = toType;

// @todo Do we still need this?
/**
 * Render/Refresh a jQueryMobile page.
 * @param {DOM} el The dom structure to render to the page.
 * @example LF.Utilities.page(this.el);
 */
export function page (el) {
    let $page = $('#application');

    $page.html(el);
}

LF.Utilities.page = page;

/**
 * Update the document's title
 * @param {string} docTitle The desired title.
 * @example LF.Utilities.title('Hello World');
 */
export function title (docTitle) {
    document.title = docTitle;
}

LF.Utilities.title = title;

/**
 * Set the language and direction of the html element.
 * @param {string} language The language to set. Defaults to default locale set in study design.
 * @param {string} direction The direction the language should display. 'ltr' or 'rtl'
 * @example LF.Utilities.setLanguage('en-US', 'ltr');
 */
export function setLanguage (language, direction) {
    let lang,
        dir,
        defaultDirection = LF.strings.getLanguageDirection({
            language: LF.StudyDesign.defaultLanguage,
            locale: LF.StudyDesign.defaultLocale
        });

    if (LF.security && LF.security.checkSiteContent && LF.security.checkSiteContent()) {
        lang = `${LF.StudyDesign.defaultLanguage}-${LF.StudyDesign.defaultLocale}`;
        dir = defaultDirection;
    } else {
        lang = language || `${LF.StudyDesign.defaultLanguage}-${LF.StudyDesign.defaultLocale}`;
        dir = direction || defaultDirection;
    }

    $('html').attr('lang', lang).attr('dir', dir);
}

LF.Utilities.setLanguage = setLanguage;

/**
 * Encodes a JSON object as a string.
 * @param {Object} json - The JSON to encode.
 * @returns {string} The encoded JSON.
 * @example LF.Utilities.encodeJSON({"hello" : "world" }); // "%7B%22hello%22:%22world%22%7D"
 */
export function encodeJSON (json) {
    return encodeURI(JSON.stringify(json)).replace(/'/g, '%27');
}

LF.Utilities.encodeJSON = encodeJSON;

/**
 * Decodes a encoded JSON string and returns the value.
 * @param {string} str The string to decode.
 * @returns {JSON} The decoded JSON.
 * @example LF.Utilities.decodeJSON("%7B%22hello%22:%22world%22%7D"); // {"hello" : "world" }
 */
export function decodeJSON (str) {
    return JSON.parse(decodeURI(str));
}

LF.Utilities.decodeJSON = decodeJSON;

/**
 * Filters most emoji codepoints from the given string.
 * Please see <a href="https://github.com/khalifenizar/emoji-strip">this</a> for more information about the Node package used to filter the emoji.
 * @param {string} str - THe string to filter
 * @returns {string} The filtered string
 */
export function removeEmoji (str) {
    return emojiStrip(str);
}

LF.Utilities.removeEmoji = removeEmoji;

/**
 * Check to see if answer model contains an IG
 * @param {Object} answer a widget answer model.
 * @param {string} IG an IG string
 * @returns {boolean} boolean result
 * @example
 * let results = LF.Utilities.compareAnswerIG(Data.Questionnaire.questionViews[0].widget.answers.at(0), 'NRS');
 */
export function compareAnswerIG (answer, IG) {
    let swAlias = answer.get('SW_Alias').split('.');

    return swAlias[0] === IG;
}

LF.Utilities.compareAnswerIG = compareAnswerIG;

/**
 * Check to see if answer model contains an IT
 * @param {Object} answer a widget answer model.
 * @param {string} IT an IT string
 * @returns {boolean} boolean result
 * @example
 * let results = LF.Utilities.compareAnswerIT(Data.Questionnaire.questionViews[0].widget.answers.at(0), 'PainLevel');
 */
export function compareAnswerIT (answer, IT) {
    let swAlias = answer.get('SW_Alias').split('.');

    return swAlias[2] === IT;
}

LF.Utilities.compareAnswerIT = compareAnswerIT;

/**
 * Check to see if answer model contains an IGR
 * @param {Object} answer a widget answer model.
 * @param {number} IGR an IGR number
 * @returns {boolean} boolean result
 * @example
 * let results = LF.Utilities.compareAnswerIGR(Data.Questionnaire.questionViews[0].widget.answers.at(0), 1);
 */
export function compareAnswerIGR (answer, IGR) {
    let swAlias = answer.get('SW_Alias').split('.');

    return parseInt(swAlias[1], 10) === IGR;
}

LF.Utilities.compareAnswerIGR = compareAnswerIGR;

/**
 * Check to see if answer model contains an IG and IGR
 * @param {Object} answer a widget answer model.
 * @param {string} IG an IG string
 * @param {number} IGR an IGR number
 * @returns {boolean} boolean result
 * @example
 * let results = LF.Utilities.compareAnswerByIGAndIGR(Data.Questionnaire.questionViews[0].widget.answers.at(0), 'NRS', 1);
 */
export function compareAnswerByIGAndIGR (answer, IG, IGR) {
    let swAlias = answer.get('SW_Alias').split('.');

    return swAlias[0] === IG && parseInt(swAlias[1], 10) === IGR;
}

LF.Utilities.compareAnswerByIGAndIGR = compareAnswerByIGAndIGR;

/**
 * Modify an answer model's swAlias.
 * @param {Object} answer a widget answer model.
 * @param {Object} obj an object containing swAlias overwrite properties. (IG,IGR,IT)
 * @returns {Object} a reference to answer model updated
 * @example
 * let results = LF.Utilities.answerSWAliasUpdate(Data.Questionnaire.questionViews[0].widget.answers.at(0), { IT: 'TEST_NRS_Q1' });
 */
export function answerSWAliasUpdate (answer, obj) {
    let swAlias = answer.get('SW_Alias').split('.'),
        swAliasObj = obj || {};

    swAlias[0] = swAliasObj.IG || swAlias[0];
    swAlias[1] = swAliasObj.IGR || swAlias[1];
    swAlias[2] = swAliasObj.IT || swAlias[2];

    answer.set('SW_Alias', swAlias.join('.'));

    return answer;
}

LF.Utilities.answerSWAliasUpdate = answerSWAliasUpdate;

/**
 * It gets the ID from the JQuery object, which is a review screen Item. This ID will be same
 * as configered for review screen items in screenFunction of the Review Screen.
 * This function must be used only within the Review Screen Action function, since Review
 * Screen widget send the widget object to its Action function. Suppose params, then params.item
 * is the review screen item selected, if none selected it will be null. On null this function throws an error.
 * @param {Object} reviewScreenItem review screen item. JQuery Object.
 * @returns {number} a number which is the ID for the item selected on the review screen.
 * @example
 * let id = LF.Utilities.getReviewScreenItemID(item);
 */
export function getReviewScreenItemID (reviewScreenItem) {
    if (!reviewScreenItem) {
        throw new Error('Attempt failed to get ID for the review screen item');
    } else {
        return reviewScreenItem.data('id');
    }
}

LF.Utilities.getReviewScreenItemID = getReviewScreenItemID;

/**
 * This function removes all screen ID from the Screen Stack which are associated
 * with the IG passed. The intention for this function is to be called before the Review Screen
 * is pushed on the stack. If this is called after the review screen is rendered then the screen ID
 * for the review screen will also be removed from the stack. The best place to use this function
 * is from a branching function which is called on navigation and before the new screen is rendered.
 * Also an ELF rule could be a better place for calling this function.
 * @param {string} IG an IG string.
 * @example
 * LF.Utilities.clearLoopScreenFromStack('NRS');
 */
export function clearLoopScreenFromStack (IG) {
    let view = LF.router.view(),
        loopScreens = [],
        mappedQuestions = {};

    if (view instanceof LF.View.QuestionnaireView) {
        // map all questions to their respective screen ID
        _.each(view.data.screens.models, (screen) => {
            _.each(screen.get('questions'), (question) => {
                mappedQuestions[question.id] = screen.id;
            });
        });

        // get all screens for the IG passed in
        loopScreens = view.data.questions.models.filter((question) => {
            return question.get('IG') === IG;
        }).map((question) => {
            return mappedQuestions[question.get('id')];
        });

        // remove all matching screen ID from the stack
        view.screenStack = view.screenStack.filter((screen) => {
            let index = loopScreens.indexOf(screen);
            return index === -1;
        });
    } else {
        throw new Error('Attempt failed to clear loop screens from the stack.');
    }
}

LF.Utilities.clearLoopScreenFromStack = clearLoopScreenFromStack;

/**
 * This function removes a screen ID from the Screen Stack. Removing screen ID from
 * the stack doesn't remove any answers on the screen. By removing from screen stack, we
 * are just not navigating to the screen when backing out.
 * @param {string} screenID an ID for the screen to be removed.
 * @example
 * LF.Utilities.removeScreenFromStack('NRS_S_3');
 */
export function removeScreenFromStack (screenID) {
    let screenStack,
        index,
        view = LF.router.view();

    if (view instanceof LF.View.QuestionnaireView) {
        screenStack = view.screenStack;
        index = screenStack.indexOf(screenID);
        if (index !== -1) {
            screenStack.splice(index, 1);
        }
    } else {
        throw new Error('Attempt failed to a screen from the stack.');
    }
}

LF.Utilities.removeScreenFromStack = removeScreenFromStack;

/**
 * Execute a query on a collection.
 * @param {Object[]} collection An array of object literals to query.
 * @param {Object} params The query to execute.
 * @returns {Object[]} The resulting collection of the query.
 * @example
 * let results = LF.Utilities.query([
 *     { id : 1, questionnaire_id : 'Meds', response : 0 },
 *     { id : 2, questionnaire_id : 'Meds', response : 2 },
 *     {id  : 3, questionnaire_id : 'Meds', response : 1 },
 * ], {
 *     where : { questionnaire_id : 'Meds' },
 *     limit : 2,
 *     order : { field : 'response', modifier : 'DESC' }
 * });
 * _(results).pluck('response'); // [2,1]
 */
export function query (collection, params) {
    let arrayObj = collection;
    if (params.where) {
        arrayObj = _(arrayObj).filter((obj) => {
            let matched = 0,
                length = _(params.where).size();

            _(obj).each((value, prop) => {
                if (value === params.where[prop]) {
                    matched += 1;
                }
            });

            return matched === length;
        });
    }

    if (params.order) {
        arrayObj = _(arrayObj).sortBy((obj) => {
            return obj[params.order.field];
        });

        if (params.order.modifier === 'DESC') {
            arrayObj = arrayObj.reverse();
        }
    }

    if (params.limit) {
        arrayObj = arrayObj.splice(0, params.limit);
    }

    return arrayObj;
}

LF.Utilities.query = query;

/**
 * Create a Javascript "GUID"
 * @returns {string} GUID
 * @example LF.Utilities.createGUID();
 */
export function createGUID () {
    let guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        /*eslint-disable*/
        let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        /*eslint-enable*/
        return v.toString(16);
    });

    return guid;
}

LF.Utilities.createGUID = createGUID;

/**
 * @description
 * Checks that a variable is set and not an empty string. This can be used in conditionals to
 * save some commonly used checking before trying to use a variable
 * @param {string} value - The target string to check.
 * @example LF.Utilities.safeCheck('');
 * @returns {boolean} true if the parameter is set and not null and not false and not an empty string
 */
export function safeCheck (value) {
    return !!(typeof value !== 'undefined' && value !== null && value);
}

LF.Utilities.safeCheck = safeCheck;

/**
 * Check if the application is receiving GET parameters and assigns them to local storage variables
 * @example ?wrapped=android&setup_code=12345678 -> localStorage.setItem('url_wrapped', 'android');
 *          and localStorage.setItem('url_setup_code', 12345678);
 */
export function storeUrlParams () {
    let search = window.location.search.replace(/^\?/, ''),
        params = search.split('&');

    _(params).each((param) => {
        let keyval = param.split('=');

        // if (keyval[0] === 'trainer') LF.Trainer.trainerMode(true);
        if (safeCheck(keyval[0])) {
            localStorage.setItem(`url_${keyval[0]}`, keyval[1]);
        }
    });
}

LF.Utilities.storeUrlParams = storeUrlParams;

/**
 * Case Insensitive String compare
 * @param {string} first String to compare
 * @param {string} second String to compare with first string
 * @returns {boolean} boolean whether strings are equal
 */
export function stringCompareCI (first, second) {
    // This will currently fail in cases of some special characters such as Turkish characters: ıIiİ
    return first.toUpperCase() === second.toUpperCase();
}

LF.Utilities.stringCompareCI = stringCompareCI;

/**
 * Returns the byte length of a string, considering the UTF-8 strings.
 * @param {string} value The string value to get the byte length of
 * @returns {number} The number of bytes in a string
 */
export function getByteLength (value) {
    let stringifiedValue,
        byteLength = 0;

    // Force string type
    stringifiedValue = String(value);

    for (let i = 0; i < stringifiedValue.length; i++) {
        let c = stringifiedValue.charCodeAt(i);
        if (c < (1 << 7)) {
            byteLength += 1;
        } else if (c < (1 << 11)) {
            byteLength += 2;
        } else if (c < (1 << 16)) {
            byteLength += 3;
        } else if (c < (1 << 21)) {
            byteLength += 4;
        } else if (c < (1 << 26)) {
            byteLength += 5;
        } else if (c < (1 << 31)) {
            byteLength += 6;
        } else {
            byteLength += Number.NaN;
        }
    }

    return byteLength;
}

LF.Utilities.getByteLength = getByteLength;

/**
 * Get a regular expression for validating the input fields for special characters.
 * @returns {Object} the regular expression for validating the special characters.
 */
export function getSpecialCharacterRegExp () {
    let validInputCharacters = LF.StudyDesign.validInputCharacters || 'a-zA-Z0-9\\s\\.-';

    // eslint-disable-next-line prefer-template
    return new RegExp('^[' + validInputCharacters + ']+$', 'g');
}

LF.Utilities.getSpecialCharacterRegExp = getSpecialCharacterRegExp;

/**
 * Adds leading zero to 1 digit numbers. Used for format formating date and time.
 * @param {number|string} item The number or string to be padded with leading zero.
 * @param {number} totalLength The total length of the string to return.  Defaults to 2.
 * @returns {string} The number as a zero-padded string
 */
export function zeroPad (item, totalLength = 2) {
    let itemStr = item.toString();
    while (itemStr.length < totalLength) {
        itemStr = `0${itemStr}`;
    }

    return itemStr;
}

LF.Utilities.zeroPad = zeroPad;

/**
 * Evaluates the nested objects and returns the object value if all nested objects do exist.
 * The two params may be reversed, reads better (e.g. zip = getNested(user, 'address.zipCode'))
 * @param {string} dotPath The string representation of the nested object separated with dots (.)
 * @param {Object} base object, defaults to window
 * @returns {Object} The evaluated value of the nested object
 */
export function getNested (dotPath, base = window) {
    let baseObj = base,
        nestedObj = dotPath;
    if (typeof baseObj === 'string') {
        [baseObj, nestedObj] = [nestedObj, baseObj];
    }
    return nestedObj.split('.').reduce((o, i) => {
        return o ? o[i] : undefined;
    }, baseObj);
}

LF.Utilities.getNested = getNested;

/**
 * Returns the font family if a langugae requires a specific font
 * @param {string} code The language-locale code
 * @returns {string} returns the font family name for the langugage code
 */
export function getFontFamily (code) {
    switch (code) {
        case 'gu-IN':
            return 'Noto Sans Gujarati';
        case 'pa-IN':
            return 'Noto Sans Gurmukhi';
        default:
            return '';
    }
}

LF.Utilities.getFontFamily = getFontFamily;

/**
 * Determine if the application is running in production.
 * @param {string} [env=LF.environment] - The environment string to check.
 * @returns {boolean} True if in production, false if not.
 */
export function isProduction (env = LF.environment) {
    // Todo: more flexibility
    return env.id === 'production';
}

/**
 * Determine if time travel is configured.
 * @param {string} env - The current environment.
 * @returns {boolean}
 */
export function isTimeTravelConfigured (env = LF.environment) {
    return !!localStorage.getItem('trainer') || (env.timeTravel && !isProduction(env));
}

/**
 * Set the environment.
 * @param {string} url - The url to set.
 */
export function setEnvironmentByURL (url) {
    let link = url != null ? url.toLowerCase() : null;

    let environment = LF.StudyDesign.environments.findWhere((env) => {
        return env.get('url').toLowerCase() === link;
    });

    environment = environment != null ? environment.toJSON() : {
        id: 'anonymous',
        label: 'anonymous',
        modes: ['production'],
        url: link,
        studyDbName: 'anonymous',
        timeTravel: false
    };

    LF.environment = environment;

    if (TimeTravel) {
        // Forcibly deny timeTravel if in production mode, allow in trainer mode
        let timeTravel = isTimeTravelConfigured(environment);

        logger.trace(`calling displayTimetravel ${timeTravel}`);
        TimeTravel.displayTimeTravel(timeTravel);
    }

    if (isProduction(environment)) {
        Q.longStackSupport = false;
    }
}

/**
 * Set the service base of the NetPro API
 * @param {string} url - the URL to set.
 */
export function setServiceBase (url) {
    let link = url;
    if (link == null) {
        link = lStorage.getItem('serviceBase');
    }

    logger.trace(`setServiceBase: ${link}`);
    setEnvironmentByURL(link);
    logger.trace(`Using environment ${LF.environment}`);

    LF.environment.url != null && lStorage.setItem('serviceBase', LF.environment.url);
    logger.trace(`serviceBase=${lStorage.getItem('serviceBase')}`);
}

LF.Utilities.setServiceBase = setServiceBase;

/**
 * Restarts the application
 */
export function restartApplication () {
    // to allow unit tests to spyOn/skip
    window.location = './index.html';
}

LF.Utilities.restartApplication = restartApplication;

/**
 * Freeze an object (with Object.freeze), recursively into sub-objects
 * and arrays.
 * @param {Object} obj The object to freeze
 */
export function deepFreeze (obj) {
    if (obj instanceof Object) {
        // includes arrays
        Object.freeze(obj);
        for (let key in obj) {
            // includes array indexes
            Object.freeze(obj[key]);
        }
    }
}

LF.Utilities.deepFreeze = deepFreeze;

/**
 * Get the correct glypicon based on type.
 * @param {string} type - The type of modal.
 * @returns {string}
 */
export function getGlyphicon (type) {
    let icon,
        span,
        headerClass;

    if (!type) {
        logger.warn('modal has no \'type\' param');
    }

    switch (type) {
        case 'error':
            icon = 'minus-circle';
            headerClass = 'error';
            break;

        case 'warning':
            icon = 'exclamation-triangle';
            headerClass = 'warning';
            break;

        case 'success':
            icon = 'check-circle';
            headerClass = 'success';
            break;

        // deliberate fall through
        case 'default':
        default :
            icon = '';
            headerClass = '';
    }

    span = icon && `<span class="fa fa-${icon}"></span>`;

    return span ? [span, headerClass] : [false];
}

/**
 * Converts XML Node to JSON representation.
 * This function converts the parent xml node to an object literal which means
 * that the name if the Parent node will not be in the JSON object
 * @param {Object} node XML node as an object.
 * @returns {Object} returns JSON Object representation of the XML.
 */
export function convertXmlToJson (node) {
    let result = {},
        // eslint-disable-next-line func-names
        textNode = $(node).contents().filter(function () {
            return this.nodeType === 3;
        });

    if (textNode.length) {
        return textNode[0].nodeValue;
    }

    $.each(node.attributes, (i, attribute) => {
        result[attribute.name] = attribute.value;
    });

    $.each($(node).children(), (i, child) => {
        result[child.nodeName] = result[child.nodeName] || [];
        result[child.nodeName].push(convertXmlToJson(child));
    });

    return result;
}

/**
 * Filters a JSON object comparing the data to the schema of a given Backbone Model.
 * Also casts a string value to number if the expected data type is number.
 * @param {Object} data The JSON object to be filtered
 * @param {Object} modelName The backbone model for data to be compared with
 * @returns {Object} returns filtered JSON object
 */
export function filterCastModelData (data, modelName) {
    let filtered = {},
        schema = LF.Model[modelName].schema,
        getAttrType = (modelAttr) => {
            return modelAttr.relationship ? modelAttr.relationship.to : modelAttr.type.name;
        };

    for (let key in data) {
        // copy in properties that are defined in schema; discard others
        if (schema[key]) {
            filtered[key] = data[key];

            // cast string values to number as necessary per schema
            // getAttrType(schema[key]) is a hack because the 'type' defined in the model's schema is a native javascript data type/object
            // and gets evaluated to its Object Function so schema[key] === Number always fails
            if (typeof filtered[key] === 'string' && getAttrType(schema[key]) === 'Number') {
                filtered[key] = filtered[key] * 1;
            }
        }
    }

    return filtered;
}

// Stakutis; preserving old PDE uses....
class Log4jsSuper extends Logger {
    log (func, str) {
        // Wrap the per-type calls so logger.log(Log4js.Level.ERROR,"chicken") turns into logger.error("chicken")
        this[func](str);
    }
}

LF.Log = {
    // so that existing code like 'new LF.Log.Logger("xmit");' will work
    Logger: Log4jsSuper
};

/**
 * Build a promise chain from an array of items, and a handler function
 * that processes each item. The handler *can* return a promise, or not;
 * it becomes the body of a .then().  Each step in the .then chain is
 * invoked with the result of the previous step.  The first step is invoked
 * with the seed value, which defaults to undefined.
 *
 * Use Q.all() instead!
 *
 * promiseChain() serializes asynchronous activities where each activity depends
 * on the result of the previous activity.  Where there is no such dependency,
 * it is probably better to use Q.all(_.map(items, handler)) instead.
 *
 * @param {Array} items - Items to process asynchronously.
 * @param {function} handle - A handler function that produces result for a given item.
 *                          It is passed the previous result as the first parameter, and
 *                          the the current item as the second parameter.
 * @param {*} seed - A seed value that is passed as the 'previousRes' to the first iteration.
 * @returns {Q.Promise<*>} A Promise that resolves to the result from the final step.
 */
export function promiseChain (items, handle, seed = undefined) {
    return items.reduce((chain, item) => {
        return chain.then(prevRes => handle(prevRes, item));
    }, Q(seed));
}

LF.promiseChain = promiseChain;

/**
 * Verify password format.
 * @param {string} passwordInput The password input by the user
 * @param {Object} passwordFormat The password formatting rules to apply to this password
 * @returns {boolean} if the password is valid, return true. Otherwise false.
 * @example this.isValidPassword("Transformers");
 */
export function isValidPassword (passwordInput = '', passwordFormat = {}) {
    let expressions = {
            // Matches all lowercase characters.
            lower: new XRegExp('[a-z]', 'g'),

            // Matches all uppercase characters.
            upper: new XRegExp('[A-Z]', 'g'),

            // Matches all characters.
            alpha: new XRegExp('\\p{L}', 'g'),

            // Matches all numeric characters.
            numeric: new XRegExp('\\p{N}', 'g'),

            // Matches all special characters.
            special: new XRegExp('[\\W_]', 'g')
        },
        options = {},
        customLength,
        password = passwordInput.replace(/\0/g, ''),
        passwordFormatRules = passwordFormat;

    if (_.isEmpty(passwordFormatRules)) {
        _.defaults(options, LF.StudyDesign.defaultPasswordFormat || {});
        _.defaults(options, LF.CoreSettings.defaultPasswordFormat || {});
        passwordFormatRules = options;
    } else {
        options = passwordFormatRules;
    }

    customLength = options.custom ? options.custom.length : 0;

    // Check for whitespaces
    if (!/^\S+$/.test(password)) {
        return false;
    }

    // Check the password length
    if (password.length < options.min || password.length > options.max) {
        return false;
    }

    // Check the passwords lower, upper, alpha, numeric, and special counts.
    for (let expression in expressions) {
        if ((password.match(expressions[expression]) || []).length < options[expression]) {
            return false;
        }
    }

    // Checks if the password has any consecutive characters.
    if (!options.allowConsecutive) {
        if (/(.)\1/.test(password)) {
            return false;
        }
    }

    // Checks if the password has any characters that appear more then once.
    if (!options.allowRepeating) {
        if (/(.).*\1/.test(password)) {
            return false;
        }
    }

    // Checks all custom expressions or functions.
    for (let i = 0; i < customLength; i += 1) {
        let expression = options.custom[i];
        if (LF.Utilities.toType(expression) === 'regexp') {
            if (!expression.test(password)) {
                return false;
            }
        } else if (LF.Utilities.toType(expression) === 'function') {
            if (!expression(password)) {
                return false;
            }
        }
    }

    return true;
}

LF.Utilities.isValidPassword = isValidPassword;

/**
 * Gets the product name of the app.
 * @returns {string}
 */
export function getProductName () {
    switch (LF.appName) {
        case 'SitePad App':
            return 'sitepad';
        case 'LogPad App':
            return 'logpad';
        case 'Web App':
            // Please do not change this!
            // There are too many issues that need to be resolve first.
            return 'sitepad';
        default:
            logger.fatal(`LF.appName value is not supported by Utilities.getProductName: ${LF.appName}`);
            return '';
    }
}

LF.Utilities.getProductName = getProductName;


/**
 * Determines if the running application is the web
 * @returns {boolean}
 */
export function isWeb () {
    return getProductName() === 'web';
}

/**
 * Determines if the running application is the SitePad
 * @returns {boolean}
 */
export function isSitePad () {
    return getProductName() === 'sitepad';
}

/**
 * Determines if the running application is the LogPad.
 * @returns {boolean}
 */
export function isLogPad () {
    return getProductName() === 'logpad';
}

/**
 * Constructs StudyWorks formatted date time string
 * @param {Date} date A date object to build StudyWorks string from
 * @returns {string} a StudyWorks formatted date string dd Mmm yyyy hh:mm:ss
 * @example LF.Utilities.buildStudyWorksFormat(new Date());
 */
export function buildStudyWorksFormat (date) {
    /**
     * Array of strings for the month values which studyworks expects when accepting data
     * @readonly
     * @type String[]
     */
    let swMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    if (!(date instanceof Date)) {
        throw new Error(`Expected a Date, got ${date}`);
    }

    let swDate = zeroPad(date.getDate()),
        swMonth = swMonths[date.getMonth()],
        swYear = date.getFullYear(),
        swHours = zeroPad(date.getHours()),
        swMinutes = zeroPad(date.getMinutes()),
        swSeconds = zeroPad(date.getSeconds());

    return `${swDate} ${swMonth} ${swYear} ${swHours}:${swMinutes}:${swSeconds}`;
}

LF.Utilities.buildStudyWorksFormat = buildStudyWorksFormat;

/**
 * Get the rater training configuration for the current platform.
 * @returns {Object} rater training configuration in study-design, for the current platform.
 */
export function getRaterTrainingConfig () {
    let platform = getProductName();
    return LF.StudyDesign.raterTrainingConfig[platform];
}

LF.Utilities.getRaterTrainingConfig = getRaterTrainingConfig;

/**
 * Makes Windows have obfuscated numeric password input.
 * @param {string} inputId the id of input that needs override
 * @param {Object} parentToAppendTo A parent DOM object to append input to
 */
export function makeWindowsNumericInputCover (inputId, parentToAppendTo) {
    let passwordInput = $(`#${inputId}`),
        height = passwordInput.outerHeight(),
        parentHeight = parentToAppendTo.outerHeight(),
        left = passwordInput.siblings('span').outerWidth();

    passwordInput.prop('type', 'password');
    passwordInput.css('z-index', '1');
    parentToAppendTo.append(`<input id="numberInput${inputId}" type="number" class="form-control"/>`);
    let numberInput = $(`#numberInput${inputId}`);
    numberInput.css({
        position: 'relative',
        top: `-${height}px`,
        left: `${left}px`,
        opacity: 0,
        height: `${height}px`,
        'z-index': 2
    });
    numberInput.on('input', () => {
        passwordInput.val(numberInput.val()).trigger('input');
    });
    parentToAppendTo.height(parentHeight);
}

/**
 * Removes splashscreen.
 * @returns {Q.Promise<void>}
 */
export function removeSplashScreen () {
    return Q()
    .then(() => {
        setTimeout(() => {
            if (navigator.splashscreen) {
                navigator.splashscreen.hide();
            }
        }, 1000);
    })
    .catch((err) => {
        logger.error('Error removing splashscreen', err);
    });
}
