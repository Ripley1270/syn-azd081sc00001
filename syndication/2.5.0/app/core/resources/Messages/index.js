import setupCoreBanners from './Banners';
import setupCoreDialogs from './DialogBoxes';

export default function setupCoreMessages () {
    setupCoreDialogs();
    setupCoreBanners();
}
