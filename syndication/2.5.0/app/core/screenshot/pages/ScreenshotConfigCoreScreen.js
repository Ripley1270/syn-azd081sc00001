import Logger from 'core/Logger';
import BaseConfigScreen from './BaseConfigScreen';

const logger = new Logger('ScreenshotConfigCoreScreen');

/**
 * @class ScreenshotConfigCoreScreen
 * @description Screenshot Configuration view.
 */
export default class ScreenshotConfigCoreScreen extends BaseConfigScreen {
    /**
     * Creates an instance of ScreenshotConfigCoreScreen.
     * @param {object} options options for this object constructor
     */
    constructor (options) {
        super(options);
        this.currentSelectionName = 'pages';

        this.template = '#screenshot-template-core-screen-selection';

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = _.defaults({}, options.strings, {
            mode: 'pages'
        }, this.templateStrings);
    }

    /**
     * The unique id of the root element.
     * @type {string}
     * @readonly
     * @default 'screenshot-config-core-screens'
     */
    get id () {
        return 'screenshot-config-core-screens';
    }

    /**
     * Render the view
     * @returns {Q.Promise}
     */
    render () {
        return super.render({
            mode: this.templateStrings.mode,
            title: this.templateStrings.title
        })
        .then(() => {
            let coreScreens = _.findWhere(this.model.get('screens'), {
                name: 'page'
            }).data;

            this.createCheckboxListItems({ list: coreScreens });
        })
        .then(() => this.showPrevCheckedItems())
        .catch((err) => {
            logger.error('An error occurred rendering the screenshot view', err);
        });
    }

    /**
     * Render the checkbox list.
     * @param {Object} options
     * @param {string[]} options.list - List of screens to display
     * @returns {Q.Promise}
     */
    createCheckboxListItems ({ list }) {
        // Each screen is an item in the router object.
        const screenList = _.chain(list)
            .reject((screen, key) => key === '*path')
            .map((screen) => {
                const values = screen.split('#');
                return {
                    id: screen,
                    displayValue: `${values[0]}#<br />${values[1]}`
                };
            })
            .value();
        return super.createCheckboxListItems({
            list: screenList,
            displayProp: 'displayValue',
            valueProp: 'id'
        });
    }
}
