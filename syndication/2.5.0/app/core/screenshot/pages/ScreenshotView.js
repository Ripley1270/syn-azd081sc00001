import NotifyView from 'core/views/NotifyView';
import Subjects from 'core/collections/Subjects';
import BaseConfigScreen from './BaseConfigScreen';

import Logger from 'core/Logger';
import { restartApplication } from 'core/utilities';

const logger = new Logger('ScreenshotView');

/**
 * @class ScreenshotView
 * @description Screenshot Language Selection view.
 */
export default class ScreenshotView extends BaseConfigScreen {
    constructor (options) {
        super(options);
        this.currentSelectionName = 'languages';
        this.template = '#screenshot-template';


        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = _.defaults({}, options.strings, {
            header: 'APPLICATION_HEADER'
        });
    }

    get id () {
        return 'screenshot-config';
    }

    /**
    * The view's events.
    * @readonly
    * @enum {Event}
    */
    get events () {
        return {
            'click .check-all': 'selectAll',

            'click .uncheck-all': 'deselectAll',

            /** Event handlers for checkbox selection */
            'click li.list-item > span': 'handleSelection',

            'change input[type=checkbox]': 'handleSelection',

            /** Click event for next button */
            'click #next': 'nextHandler',

            /** Click event for back button */
            'click #back': 'back'
        };
    }

    set events (events = {}) {
        return events;
    }

    /**
     * Restarts application, navigating to code entry.
     * @param {Event} e Event object.
     */
    back () {
        // DE18816 - Logpad Only - Navigation was broken when clicking the back button.
        // A 'screenshot' variable is set in localStorage when submitting the CodeEntryView with a Study URL of 'screenshot'.
        // This value needs to be removed before restarting the application, so the correct view will display.
        localStorage.removeItem('screenshot');
        localStorage.removeItem('krpt');

        // DE17565 - A fake subject is created when screenshot mode is initialized.
        // This subject has to be removed for the LPA or the application will attempt
        // to navigate to the login view.
        Subjects.clearStorage()
        .finally(() => {
            restartApplication();
        })
        .done();
    }

    /**
     * Go to the given next screen.
     * @param {string} url - The url we are navigating to.
     */
    next (url) {
        this.navigate(url);
    }

    /**
     * Determine where to go next, or display an error message.
     * @returns {Q.Promise|undefined}
     */
    nextHandler () {
        let languages = this.findCheckedItems();

        if (!languages.length) {
            return new NotifyView().show({
                header: '',
                body: 'Please select one or more languages',
                ok: 'OK',
                type: 'error'
            });
        }

        this.model.setSelection(this.currentSelectionName, languages);

        return this.next('screenshot_mode');
    }

    /**
     * Render the view
     * @return {Q.Promise}
     */
    render () {
        return super.render({
            title: 'Take Screenshots',
            mode: 'language'
        })

        // add check for new strings here
        .then(() => this.addLanguagesList())
        .then(() => this.showPrevCheckedItems())
        .catch((err) => {
            err && logger.error('An error occurred rendering the screenshot view', err);
        });
    }

    /**
     * Add the list of languages
     * @returns {Q.Promise}
     */
    addLanguagesList () {
        let langcodes = _.uniq(LF.strings.map((m) => {
                return `${m.attributes.language}-${m.attributes.locale}`;
            })),
            unorderedList = $('<div>');

        // Display the languages as checkbox options.
        return Q.all(langcodes.map((langcode) => {
            return LF.getStrings(langcode.toUpperCase(), (langName) => {
                let listItem = $(`<li class="list-item"><input type="checkbox"><span>${langName}</span></li>`)
                    .attr('data-value', langcode);

                return unorderedList.append(listItem);

                // deal w/ multiple namespaces
            }) || Q(unorderedList.append($(`<li class="list-item"><input type="checkbox"><span>${langcode.toUpperCase}</span></li>`)
                    .attr('data-value', langcode)));
        }))
        .then(() => {
            this.renderListItems(unorderedList);
        })
        .catch((err) => {
            logger.error('Error occurred rendering languages', err);
        });
    }
}
