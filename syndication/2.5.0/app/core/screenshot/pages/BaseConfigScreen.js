import PageView from 'core/views/PageView';
import Logger from 'core/Logger';
import NotifyView from 'core/views/NotifyView';

const logger = new Logger('ScreenshotConfigBaseScreen');

export default class BaseConfigScreen extends PageView {
    constructor (options) {
        super(options);

        /**
         * Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = _.defaults({}, options.strings, {
            header: 'APPLICATION_HEADER',
            title: 'Take Screenshots'
        });
    }

    get events () {
        return {
            'click .check-all': 'selectAll',
            'click .uncheck-all': 'deselectAll',

            // Click event for next button
            'click #next': 'nextHandler',

            // Click event for back button
            'click #back': 'back',

            // Event handlers for checkbox selection
            'click li.list-item > span': 'handleSelection',

            'change input[type=checkbox]': 'handleSelection'
        };
    }

    set events (events = {}) {
        return events;
    }

    /**
     * Set the model based on current selection
     */
    setModelSelected () {
        let selectedItems = this.findCheckedItems();
        this.model.setSelection(this.currentSelectionName, selectedItems);
    }

    /**
     * Navigate back to the previous screen.
     */
    back () {
        this.setModelSelected();
        this.model.prevMode();
        this.navigate('screenshot_next_mode');
    }

    /**
     * Go to the given next screen.
     * @param {string} url - The url we are navigating to.
     */
    next (url) {
        this.model.nextMode();
        this.navigate(url);
    }

    /**
     * Determine where to go next, or display an error message.
     * @returns {Q.Promise|undefined}
     */
    nextHandler () {
        let items = this.findCheckedItems();

        if (!items.length) {
            return new NotifyView().show({
                header: '',
                body: 'Please select one or more items',
                ok: 'OK',
                type: 'error'
            });
        }

        this.model.setSelection(this.currentSelectionName, items);

        return this.next('screenshot_next_mode');
    }

    /**
     * If we are navigating back to this screen, display what was chosen previously.
     * @returns {Q.Promise}
     */
    showPrevCheckedItems () {
        return Q()
        .then(() => {
            let itemsChecked = this.model.getSelection(this.currentSelectionName);

            if (itemsChecked && itemsChecked.length) {
                itemsChecked.forEach((item) => {
                    const element = this.$checkboxGroup.find(`li.list-item[data-value="${item}"]`);

                    element && this.selectItem(element);
                });
            }
        })
        .catch(e => logger.error(e));
    }

    /**
     * Get the selected items.
     * @returns {string[]} The selected Items.
     */
    findCheckedItems () {
        let selectedAnswers = this.$el.find('li.checked');

        return _.map(selectedAnswers, answer => $(answer).data('value'));
    }

    /**
     * Render the view
     * @returns {Q.Promise}
     */
    render (strings = {}) {
        return this.buildHTML(strings)
        .then(() => {
            this.page();
            this.delegateEvents();

            this.$checkboxContainer = this.$('.screenshot-checkbox-container');
            this.$checkboxGroup = this.$('#screenshot-checkbox-group');
            this.$screenshotBtnContainer = this.$('.screenshot-btn-container');
        })
        .catch((err) => {
            logger.error('An error occurred rendering the screenshot view', err);
        });
    }

    /**
     * Render the checkbox list.
     * @param {Object} options
     * @param {string[]} options.list - List of screens to display
     * @param {string} [options.displayProp] - Property name on the item to use to display to the screen
     * @param {string} [options.valueProp] - Property name on the item to use to store internally
     * @returns {Q.Promise}
     */
    createCheckboxListItems ({ list, displayProp = null, valueProp = null }) {
        const unorderedList = list.reduce((el, item) => {
            const name = displayProp ? item[displayProp] : item;
            const value = valueProp ? item[valueProp] : name;
            const id = item.id || '';
            const listItem = $(`<li class="list-item"><input type="checkbox"><span>${name}</span></li>`);

            listItem.attr('data-template-data', JSON.stringify(item))
            .attr('id', id)
            .attr('data-value', value);

            return el.append(listItem);
        }, $('<div>'));

        return Q()
        .then(() => this.renderListItems(unorderedList));
    }

    /**
     * Render the list to the screen.
     * @param {any} list - jQuery object of the list to display.
     */
    renderListItems (list) {
        this.$checkboxGroup.html(list.html());
    }

    /**
     * React to input event to select a list item.
     * @param {Object} e - input event
     */
    handleSelection (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        let $parentListItem = $(e.target).closest('li');
        $parentListItem.hasClass('checked') ? this.deselectItem($parentListItem) : this.selectItem($parentListItem);
    }

    /**
     * Select the item.
     * @param {any} listItem - jQuery object for the item to select
     */
    selectItem (listItem) {
        let itemObj = listItem;
        itemObj.context || (itemObj = $(itemObj));
        itemObj.not('.disabled')
            .addClass('checked')
            .attr('selected', true)
            .children('input:not(:disabled)')
            .attr('checked', true);
    }

    /**
     * Deselect the item.
     * @param {any} listItem - jQuery object for the item to deselect
     */
    deselectItem (listItem) {
        let itemObj = listItem;
        itemObj.context || (itemObj = $(itemObj));
        itemObj.not('.disabled')
            .removeClass('checked')
            .attr('selected', false)
            .children('input:not(:disabled)')
            .removeAttr('checked');
    }

    /**
     * Checks all checkboxes
     */
    selectAll () {
        this.$checkboxGroup
            .find('li.list-item').not('.disabled')
            .each((idx, listItem) => {
                this.selectItem(listItem);
            });
    }

    /**
     * Unchecks all checkboxes
     */
    deselectAll () {
        this.$checkboxGroup
            .find('li.list-item').not('.disabled')
            .each((idx, listItem) => {
                this.deselectItem(listItem);
            });
    }
}
