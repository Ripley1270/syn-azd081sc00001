import DropDownListDetector from './detectDropdownList';
import DynamicTextDetector, { addDynamicTextTriggerRule } from './detectDynamicText';
import ScrollingDetector from './detectScrolling';
import ParamFunctionDetector from './detectParamFunction';

/**
 * Creates an array of instantiated detectors, then runs them in sequence.
 * @param {Object} options - options
 * @param {function} options.render - The function that will render the screen.
 * @param {function} options.takeScreenshot - The function that will take the screenshots.
 * @param {function} [options.cleanup] - A function run to cleanup after rendering and taking screenshot.
 * @param {object[]} [options.detectors] - The detectors to run. Defaults to all of the current detectors.
 * @returns {Q.Promise<any>}
 */
export const runDetectors = (options = {}) => {
    _.defaults(options, {
        render: $.noop,
        takeScreenshot: $.noop,
        cleanup: $.noop,
        detectors: [],
        defaultDetectorClasses: [
            ParamFunctionDetector,
            DynamicTextDetector,
            DropDownListDetector,
            ScrollingDetector
        ]
    });
    const { render, takeScreenshot, cleanup, detectors } = options;

    // So that Dynamic Text functions are not run while checking other detectors.
    addDynamicTextTriggerRule({
        id: 'BlankDynamicText',
        salience: 2,
        action: () => ({ text: '' })
    });

    if (!detectors.length) {
        detectors.push(..._.map(options.defaultDetectorClasses, (Detector) => {
            return new Detector(render, takeScreenshot, cleanup);
        }));
    }

    const detectorFound = detectorList => _.contains(_.pluck(detectorList, 'isDetected'), true);

    return detectors.reduce((detectionChain, detector) => {
        return detectionChain
        .then(() => {
            return detector.setupDetectionListener ? detector.setupDetectionListener() : Q();
        })
        .then(() => render())
        .then(() => detector.run())
        .then(() => !detectorFound([detector]) && cleanup());
    }, Q())
    .then(() => {
        // if no detectors were found, render, take a screenshot, and cleanup
        if (detectorFound(detectors)) {
            return Q();
        }

        return Q()
        .then(() => render())
        .then(() => takeScreenshot())
        .then(() => cleanup());
    });
};
