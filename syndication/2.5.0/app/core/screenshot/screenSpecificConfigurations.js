/**
 * Special configurations for specific screens. Such as default string values for values that may not exist in screenshot mode.
 */
export default {
    diaries: {
        // Views that have a cancel button need to be rendered differently. Maybe we need a way to detect it instead of hardcoding.
        diariesWithCancel: [
            'Edit_Patient',
            'Edit_User',
            'Activate_User',
            'Deactivate_User',
            'New_Patient',
            'Skip_Visits'
        ]
    },

    pages: {
        about: {
            stringValues: {
                activationCodeValue: '0',
                subjectIDValue: '000',
                logLevelValue: 'ERROR',
                siteCodeValue: '000',
                studyURLValue: 'http://'
            }
        },
        login: {
            stringValues: {
                siteCode: '000',
                subjectNumber: '000'
            }
        },
        reactivation: {
            stringValues: {
                setupCode: '000000000'
            }
        },
        activation: {
            stringValues: {
                setupCode: '000000000'
            }
        }
    }
};
