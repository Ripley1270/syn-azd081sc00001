import ELF from 'core/ELF';
import Logger from 'core/Logger';
import { eCoaDB, LogDB } from 'core/dataAccess';

let logger = new Logger('uninstall');

let collections = [
    'ActiveAlarm',
    'Answer',
    'Dashboard',
    'DatabaseVersion',
    'ESenseDevice',
    'LastDiary',
    'Site',
    'StoredSchedule',
    'Subject',
    'SubjectAlarm',
    'Transmission',
    'User'
];

/**
 * @memberOf ELF.actions
 * @method uninstall
 * @description
 * Uninstall the application
 * @param {Array<string>} [input] - Additional storage objects to clear.
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'uninstall' }]
 */
export function uninstall (input = []) {
    logger.info('******************** UNINSTALL ******************** ');

    logger.traceEnter('Uninstall');
    LF.security.pauseSessionTimeOut();

    logger.trace('Wiping various local storages');
    localStorage.clear();

    logger.trace('Wiping databases');

    // db.drop() is currently broken/problematic.
    // instead, simply clear the databases
    let promise = Q();

    // Merge the core collections with any provided ones.
    collections = _.union(collections, input);

    collections.forEach((db) => {
        // If this collection doesn't already exist in the data access layer, create it.
        if (!eCoaDB[db]) {
            eCoaDB.collection(db);
        }
    });

    // If this collection doesn't already exist in the data access layer, create it.
    if (!LogDB.Log) {
        LogDB.collection('Log');
    }

    [eCoaDB, LogDB].forEach((db) => {
        for (let store in db) {
            if (db[store].clear) {
                promise = promise.then(() => db[store].clear());
            }
        }
    });

    return promise.tap(() => {
        logger.trace('Uninstall complete');
        logger.traceExit('Uninstall');
    });
}

ELF.action('uninstall', uninstall);

// @todo remove
LF.Actions.uninstall = uninstall;
