/* eslint-disable */
// Seems like this file should be defunct.
import Data from '../Data';
import ELF from '../ELF';

// TODO: This action is not being used anywhere and also it will fail for both LogPad and SitePad due to 
// the navigation to a url which is not supported in the routers
export default function editQuestionnaire (input, done) {
    Data.Questionnaire = { };

    LF.router.navigate('questionnaire/' + input.questionnaire + '/edit/' + input.dashboardId , true);

    done();
};

ELF.action('editQuestionnaire', editQuestionnaire);