import ELF from 'core/ELF';
import { MessageRepo } from 'core/Notify';
import * as MessageHelpers from 'core/resources/Messages/MessageHelpers';
import Logger from 'core/Logger';
import Spinner from 'core/Spinner';

const logger = new Logger('Confirm');

/**
 * @memberOf ELF.actions
 * @method confirm
 * @description
 * Displays a confirmation dialog pop-up.
 * @param {Object} params - Dialog configuration provided by a {@link ELF~ActionObject}.
 * @param {string} params.key - Key, or ID, of the confirmation dialog to display.
 * @param {Function} [callback=false] The callback function invoked when the the user's input is submitted.
 * @returns {Q.Promise<boolean>}
 * @example
 * resolve: [{
 *     action: 'confirm',
 *     data: { key: 'BACKOUT_CONFIRMATION' }
 * }]
 * @example
 * import { confirm } from 'core/actions/confirm';
 *
 * confirm.then(() => {
 *     // User clicked 'OK'...
 * }, then () => {
 *     // User clicked 'Cancel'...
 * });
 */

/**
 * @memberOf ELF.expressions
 * @method confirm
 * @description
 * Displays a confirmation dialog pop-up.
 * @param {Object} params - Dialog configuration provided by a {@link ELF~ExpressionObject}.
 * @param {string} params.key - Key, or ID, of the confirmation dialog to display.
 * @param {Function} [callback=false] The callback function invoked when the the user's input is submitted.
 * @return {Q.Promise<boolean>}
 * @example
 * evaluate: { expression: 'confirm', input: { key: 'LOGOUT_CONFIRM' } },
 */
export function confirm (params, callback = false) {
    const dialogVal = params.dialog || (MessageRepo.Dialog && MessageRepo.Dialog[params.key]);

    if (!dialogVal) {
        if (params.key) {
            logger.warn(`Dialog "${params.key}" not found in registry.`);

            callback && callback(true);
            return Q(true);
        }

        logger.info('Confirm dialog being shown without being registered, message will not be available for screenshots.');

        const dialog = MessageHelpers.confirmDialogCreator(params);

        return Spinner.hide()
        .then(() => {
            return dialog();
        })
        .then(() => {
            callback && callback(true);
        });
    }

    return Spinner.hide()
    .then(() => MessageRepo.display(dialogVal, params.options))
    .then(() => {
        callback && callback(true);
        return true;
    })
    .catch(() => {
        // DE16990 - Removed Q.reject(false).  It wasn't being returned, so remained unhandled.
        callback && callback(false);

        return false;
    });
}

ELF.action('confirm', confirm);

// Adding this as an expression as well...
ELF.expression('confirm', confirm);

// @todo remove
LF.Actions.confirm = confirm;
