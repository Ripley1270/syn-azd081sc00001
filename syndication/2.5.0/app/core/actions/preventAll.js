import ELF from 'core/ELF';

/**
 * @memberOf ELF.actions
 * @method preventAll
 * @description
 * Prevents the default functionality and any more rules from executing.
 * <pre><code>
 * { preventDefault: true, stopRules: true }
 * </code></pre>
 * @return {Q.Promise<ELF~ActionResult>}
 * @example
 * resolve: [{
 *     action: 'preventAll'
 * }]
 */
export default function preventAll () {
    return Q({ preventDefault: true, stopRules: true });
}

ELF.action('preventAll', preventAll);
