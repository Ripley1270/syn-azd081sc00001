/* eslint-disable no-shadow */
import Logs from 'core/collections/Logs';
import ELF from 'core/ELF';

/**
 * @memberOf ELF.actions
 * @method deleteLogs
 * @description
 * Deletes all the log entries included in the batch log provided.
 * @param {LF.Model.BatchLog} deleteBatch BatchLog containing logs to be deleted
 * @param {Function} callback Callback function to be invoked when the process has completed.
 */
export function deleteLogs (deleteBatch, callback) {
    let deleteLength,
        deleteLogs = new Logs(),
        step = (counter) => {
            // Test if logs.at(counter).id is zero, if so, skip
            if (deleteLogs.at(counter).id !== 0) {
                deleteLogs.destroy(deleteLogs.at(counter).id,
                    () => {
                        if (deleteLogs.length > counter) {
                            step(counter);
                        } else {
                            callback ? callback() : $.noop();
                        }
                    }
                );
            } else { // eslint-disable-next-line no-lonely-if
                if (deleteLogs.length > counter + 1) {
                    step(counter + 1);
                } else {
                    callback ? callback() : $.noop();
                }
            }
        };

    deleteLogs = deleteBatch.attributes.params;
    deleteLength = deleteLogs.length;

    if (deleteLength) {
        step(0);
    }
}

ELF.action('deleteLogs', deleteLogs);

LF.Actions.deleteLogs = deleteLogs;
