/**
 * Checks the repeating indicator schedule.
 * @param {LF.Model.Schedules} schedule object containing schedule configuration.
 * @param {LF.Collection.Dashboards} completedQuestionnaires list of all completed diaries.
 * @param {Object} parentView The view to append this scheduled item to.
 * @param {Object} context an object that contains subject model and visit model (sitepad only)
 * @param {Function} callback A callback function invoked upon.
 */
LF.Schedule.schedulingFunctions.checkRepeatByDateIndicatorAvailability = (schedule, completedQuestionnaires, parentView, context, callback) => {
    if (!parentView) {
        callback(false);
        return;
    }

    let nowUTCMillis = new Date().getTime(),
        start = LF.Utilities.parseTime(schedule.get('scheduleParams').startAvailability, true),
        end = LF.Utilities.parseTime(schedule.get('scheduleParams').endAvailability, true),
        customClassname = schedule.get('scheduleParams').customClassname,
        model = parentView.indicators.get(schedule.get('target').id);

    if (nowUTCMillis >= start && nowUTCMillis < end) {
        if (customClassname) {
            model.set({ className: customClassname });
        }

        callback(true);
    } else {
        callback(false);
    }
};

export default LF.Schedule.schedulingFunctions.checkRepeatByDateIndicatorAvailability;
