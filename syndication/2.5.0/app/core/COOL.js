import Logger from 'core/Logger';
let logger = new Logger('COOL');

// registry data structire is as follows:
// { SomeKey: { classDefinition: <function>, singleton: <boolean>, instance: <Object>|undefined}, ... }
let registry = {};

/**
 * Class Oriented Object Locator
 *
 * This is the Service Locator pattern.
 *
 * Basically, a bit of inversion-of-control.  Allow engineers to create upperlevel classes
 * that likely subclass core classes, and then 'register' those by key.  The system (core)
 * will "new" by that key and get an instance of the registered class without need to know
 * what it is or what it offers beyond to the base implementation it depends on.
 */
export default class COOL {
    /**
     * Register a service.
     * @static
     * @param {string} key - The key for the service to be registered with.
     * @param {Object} srv - The service to register.
     * @returns {Object} The registered service.
     * @example
     * COOL.service('Transmit', { ... });
     */
    static service (key, srv) {
        let previous = registry[key];

        registry[key] = { key, service: srv, singleton: true };

        if (previous) {
            logger.trace(`Replaced service ${key}.`);
        } else {
            logger.trace(`Registered service ${key}.`);
        }

        return registry[key];
    }

    /**
     * Fetch a service by key.
     * @static
     * @param {string} key - The key of the service to get.
     * @param {Object} defaultService - If no service exists, return this service.
     * @returns {Object} The service requested, or the default provided.
     * @example
     * import Transmit from 'core/transmit';
     * let srv = COOL.getService('Transmit', Transmit);
     */
    static getService (key, defaultService) {
        if (!defaultService) {
            logger.error(`Service ${key} not registered and no default was supplied.`);

            return null;
        }

        let registered = registry[key];

        if (registered) {
            logger.trace(`Found service ${key}.`);

            return registered.service;
        }

        logger.trace(`Service ${key} not registered; returning supplied default.`);

        return defaultService;
    }

    /**
     * Adds a new service function to the global object and COOL, by using COOL's own 'service' method.
     * @param {Object|function} customServiceFunctionObj - The function object defining the service being added.
     */
    static addService (customServiceFunctionObj) {
        let newServiceFunction;

        if (customServiceFunctionObj === null) {
            logger.error('COOL.addService: Service not defined. No service added.');
            return;
        }

        newServiceFunction = { customServiceFunctionObj };

        // While we eventually want to remove references to globals across the app, for now we need to
        // guarantee the new service updates. Reviewed with C.H.
        LF.Transmit = _.extend({}, LF.Transmit, newServiceFunction);
        this.service('Transmit', LF.Transmit);
    }

     /*
     * Determines if the provided class is a valid class/constructor function.
     * @static
     * @param {(Object|function)} classDefinition - The class/function to check.
     * @returns {boolean} True if valid, false if not.
     */
    static isValidConstructor (classDefinition) {
        if (typeof classDefinition === 'object' && _.isEmpty(classDefinition)) {
            logger.error('It looks like we may have this situation: http://stackoverflow.com/a/30390378/1092324');
        }

        return classDefinition instanceof Function;
    }

    /*
     * Register a class (by its constructor), under some key.
     * @static
     * @param {string} key - The key of the class to register.
     * @param {(Object|function)} classDefinition - The class or constructor function to register.
     * @returns {(Object|function)} The registered class.
     * @example
     * class WebService { };
     * COOL.register('WebService', WebService);
     */
    static register (key, classDefinition) {
        let previous = registry[key];

        if (!COOL.isValidConstructor(classDefinition)) {
            logger.fatal(`Attempted to register ${key} with something that is not a constructor: ${classDefinition}`);
            return previous;
        }

        registry[key] = { classDefinition, singleton: false, instance: undefined };

        if (previous) {
            logger.trace(`Replaced ${key} ${previous.name} with ${classDefinition.name}`);
        } else {
            logger.trace(`Registered ${key} as ${classDefinition.name}`);
        }

        return previous ? previous.classDefinition : undefined;
    }

    /**
     * Alias for COOL.register.
     * @static
     * @param {string} key - The key of the class to register.
     * @param {(Object|function)} classDefinition - The class or constructor function to register.
     * @returns {(Object|function)} The registered class.
     * @example
     * class WebService {};
     * COOL.add('WebService', WebService);
     */
    static add (key, classDefinition) {
        return COOL.register(key, classDefinition);
    }

    /**
     * Alias for COOL.new, but without providing options.
     * @static
     * @param {string} key - The key of the registered class to instantiate.
     * @param {(Object|function)} defaultClass - The default class to instantiate if none is registered.
     * @returns {Object} The instantiated class.
     * @example
     * let webService = COOL.getInstance('WebService');
     */
    static getInstance (key, defaultClass) {
        // TODO evolve to handle singletons etc.
        return COOL.new(key, defaultClass);
    }

    /*
     * Caller can pass in the string-key of a class already added to COOL,
     * and if not defined in cool, the 'default' (a class constructor) will be used instead.
     * @static
     * @param {string} key - The key of the class to instantiate.
     * @param {(Object|function)} defaultClass - The default class to instantiate if none is registered.
     * @param {Object} [options] - Options passed into the constructor function.
     * @returns {Object} The instantiated class.
     * @example
     * COOL.new('WebService', WebService, { ... });
     */
    static new (key, defaultClass, options) {
        let registration = registry[key];
        let classDefinition = registration ? registration.classDefinition : undefined;

        if (!defaultClass) {
            logger.error(`No default class definition was supplied. Key: ${key}`);
            return undefined;
        }

        if (!registration) {
            if (!COOL.isValidConstructor(defaultClass)) {
                logger.fatal(`Nothing registered for ${key}, and supplied default is not a valid constructor: ${defaultClass}`);
                return undefined;
            }

            classDefinition = defaultClass;
            logger.trace(`Instantiating supplied default class ${defaultClass.name} for ${key}`);
        } else {
            logger.trace(`Instantiating a ${key} of type ${classDefinition.name}`);
        }

        // eslint-disable-next-line new-cap
        return new classDefinition(options);
    }

    /*
     * This method does not instantiate the requested service, it only returns its class (constructor).
     * This is outside the Service Locator pattern. In general, only the 'new' method should be used.
     * @static
     * @param {string} key - The key of the registered class to get.
     * @param {(Object|function)} defaultClass - A default class/constructor function to provide.
     * @returns {(Object|function)} The requested class/constructor function.
     * @example
     * import WebService from 'core/classes/WebService';
     * let WService = COOL.getClass('WebService', WebService);
     * let service = new WebService();
     */
    static getClass (key, defaultClass) {
        let registration = registry[key];

        if (registration) {
            logger.trace(`Found ${key} ${registration.classDefinition.name}`);
            return registration.classDefinition;
        } else if (defaultClass) {
            if (!COOL.isValidConstructor(defaultClass)) {
                logger.error(`Key ${key} not registered, and supplied default looks to be invalid: ${defaultClass}`);
            }

            logger.trace(`Key ${key} not registered; returning supplied default ${defaultClass.name}`);
            return defaultClass;
        }

        logger.trace(`Key ${key} not registered and no default was supplied.`);
        return undefined;
    }

    /**
     * This is a helper method intended to be used by a developer on the dev console.
     * The method returns the class hierarchy of a class registered in COOL.
     * @param {string} key - The key of the registered class to get the hieararchy of.
     * @returns {string} The hierarchy of the class in a string format (<class name> <-- <super class name> <-- <super class name of the super class>).
     */
    static getClassHierarchy (key) {
        let classDefinition = COOL.getClass(key),
            getSuperClassNames = (definition) => {
                if (!definition.name) {
                    return [];
                }

                let superClass = Object.getPrototypeOf(definition),
                    names = getSuperClassNames(superClass);

                names.push(definition.name);
                return names;
            },
            superClassNames = getSuperClassNames(classDefinition);

        superClassNames.reverse();

        return superClassNames.join(' --> ');
    }
}
