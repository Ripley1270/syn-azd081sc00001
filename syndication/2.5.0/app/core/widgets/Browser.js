import WidgetBase from './WidgetBase';

const DEFAULT_TEMPLATE = 'DEFAULT:Browser';
const DEFAULT_LOAD_ERROR_MESSAGE = 'BROWSER_LOAD_ERROR';
const ANSWER_VALUE = '1';

class Browser extends WidgetBase {
    /**
     * The default template.
     * @returns {string}
     */
    get defaultTemplate () {
        return DEFAULT_TEMPLATE;
    }

    get defaultLoadErrorMessage () {
        return DEFAULT_LOAD_ERROR_MESSAGE;
    }

    /**
     * Constructs the browser widget.
     * @param {Object} options - The widget options
     * @param {Object} options.model - The Backbone model class containing the widget attributes.
     * @param {string} options.model.url - The URL to set as the iframe source.
     * @param {string|RegExp} [options.model.browserCloseURLPattern] - URL pattern (string or regex) which,
     *  when accessed by the browser, will signal the app to close it
     * @param {string} [options.model.className] {dev-only} - The CSS classname applied to the widget container element.
     * @param {Object} options.model.templates - {dev-only} Collection of templates to be used to render the widget.
     * @param {string<translated>} options.model.linkText - The key for the NLS text string to be used as the link text for opening the new browser window.
     * @param {string<translated>} [options.model.loadErrorMessage=BROWSER_LOAD_ERROR] - The string to be used when a load error has occurred.
     * @param {string} options.model.id {dev-only} - The ID of the widget.
     * @param {string} [options.model.templates.container=DEFAULT:Browser] {dev-only} - The container used to render the iframe.
     */
    constructor (options) {
        super(options);

        this.validateModel();

        /**
         * Indicates if the widget is completed.  This will be set to true when the provided URL renders.
         * @type Boolean
         * @default true
         */
        this.completed = false;

        this.events = {
            'click a': 'respond'
        };
    }

    /**
     * Override delegate events to also bind event functions to "this".
     */
    delegateEvents () {
        super.delegateEvents();
        this.loadStop = _.bind(this.loadStop, this);
        this.loadError = _.bind(this.loadError, this);
        this.browserExit = _.bind(this.browserExit, this);
    }

    /**
     * Renders the widget.
     * @returns {Q.Promise}
     */
    render () {
        /**
         * The templates used by the widget.
         * @type Object
         */
        this.templates = this.model.get('templates') || { container: this.defaultTemplate };

        /**
         * The text to use for the link to open the browser.
         * @type String
         */
        this.linkText = this.model.get('linkText');

        /**
         * The URL to provide to the iframe as its src attribute.
         * @type String
         */
        this.url = this.model.get('url');

        /**
         * The ID of the widget.
         * @type String
         */
        this.id = this.model.get('id');

        /**
         * The 404 message for when a site is not available.
         * @type String
         */
        this.loadErrorMessage = this.model.get('loadErrorMessage') || this.defaultLoadErrorMessage;

        /**
         * The result of the load error message after translation
         * @type String
         */
        this.loadErrorMessageString = null;

        /**
         * Handle to the inappbrowser when it is opened.
         * @type Object
         */
        this.browser = null;

        /**
         * The ID applied to the anchor element.
         * @type String
         */
        this.linkID = `${this.id}_a`;

        let toTranslate = {
            linkText: this.linkText,
            loadErrorMessage: this.loadErrorMessage
        };

        return this.i18n(toTranslate, $.noop, { namespace: this.question.getQuestionnaire().id })
        .then((strings) => {
            this.loadErrorMessageString = strings.loadErrorMessage;
            return new Q.Promise((resolve) => {
                let $a;
                let divEl = this.renderTemplate(this.templates.container, {
                    linkText: strings.linkText,
                    id: this.linkID
                });

                this.$el.empty();
                this.$el.append(divEl);

                this.$el.appendTo(this.parent.$el);
                this.$el.trigger('create');

                $a = this.$(`#${this.linkID}`);
                $a.on('click', () => {
                    this.trigger('response');
                });

                $a.ready(() => {
                    resolve();
                });
            });
        })
        .then(() => {
            this.delegateEvents();
        });
    }

    /**
     * Validate the model, specifically the URL attribute.
     * If this does not exist, throw an exception.
     */
    validateModel () {
        if (!this.model.has('url')) {
            throw new Error('Browser Widget must have a URL attribute defined.');
        }
    }

    /**
     * URL is loaded.  On Android this seems to fire for 404 pages as well.
     * @param {Object} event object containing url of browser.
     */
    loadStop (event) {
        let browserCloseURLPattern = this.model.get('browserCloseURLPattern');

        if (browserCloseURLPattern && event.url.match(browserCloseURLPattern)) {
            if (this.browser) {
                this.browser.close();
            }
        } else {
            this.markCompleted()
            .done();
        }
    }

    /**
     * Error has occurred (404 or other).  Close browser and set status to false.
     */
    loadError () {
        if (this.browser && this.loadErrorMessageString) {
            this.$('.Browser-Error').html(this.loadErrorMessageString);
            this.browser.close();
            this.browser = null;
        }
    }

    /**
     * After browser has exited.  De-reference browser and remove answers if it happened by error.
     */
    browserExit () {
        // If browser is already null, it was closed in an error event.
        if (this.browser === null) {
            // Remove answers so the control does not think we are done
            this.removeAllAnswers();
            this.completed = false;
        } else {
            this.browser = null;
        }
    }

    /**
     * Mark the widget as completed, and store an answer of 1.
     * @returns {Q.Promise<void>}
     */
    markCompleted () {
        // If the user successfully gets to a page, consider the question "done"
        this.completed = true;

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        return this.respondHelper(this.answer, ANSWER_VALUE);
    }

    /**
     * Handles the user's interact with the widget.
     * @returns {Q.Promise}
     */
    respond () {
        // If we are running on a device, us the InAppBrowser
        if (LF.Wrapper.isWrapped) {
            this.browser = cordova.InAppBrowser.open(this.url, '_blank', 'location=no,EnableViewPortScale=yes');
            this.browser.addEventListener('loadstop', this.loadStop);
            this.browser.addEventListener('loaderror', this.loadError);
            this.browser.addEventListener('exit', this.browserExit);
            return Q();
        }

        this.browser = window.open(this.url, '_blank', 'location=no,EnableViewPortScale=yes');

        // For browser security reasons, we can't monitor load status using window.open.  Mark it complete here.
        return this.markCompleted();
    }
}

window.LF.Widget.Browser = Browser;
export default Browser;
