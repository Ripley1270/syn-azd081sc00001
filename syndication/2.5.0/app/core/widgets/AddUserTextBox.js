import TextBox from './TextBox';
import { Banner } from 'core/Notify';

const defaultInput = 'DEFAULT:TextBox',
    lfKeyCode = 10,
    crKeyCode = 13;

/**
 * Extension of TextBox widget for Add User.
 */
export default class AddUserTextBox extends TextBox {
    /**
     * Get the input template
     */
    get input () {
        return defaultInput;
    }

    /**
     *  Constructor
     *  @param {Object} options The options for the widget
     */
    constructor (options) {
        super(options);

        this.validation = this.model.get('validation');
        this.isUserTextBox = true;
        this.events = {
            'keypress input[type=text]': 'keyPressed',
            'input input[type=text]': 'sanitize',
            'focus input[type=text]': 'onFocus'
        };
    }

    /**
     *  Responds to a key press event.
     *  @param {Event} e Event data
     *  @returns {boolean|undefined} false or nothing.
     */
    keyPressed (e) {
        let keyCode = e.which || e.keyCode;

        if ((keyCode === lfKeyCode) || (keyCode === crKeyCode)) {
            $('input').blur();
            e.preventDefault();
            return false;
        }
        super.keyPressed(e);
        return undefined;
    }

    /**
     *  Handles user input on the widget.
     *  @param {Event} e Event data
     */
    respond (e) {
        // DE17147 - A value of ' ' is trimmed to a length of 0.
        // This triggered the remove answer condition below.
        // This condition was hoisted to the top of the method to ensure the answer record
        // exists prior to accessing it.
        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        let validateRegex = this.answer.get('validateRegex'),
            field = this.model.get('field'),
            value = this.$(e.target).val().trim(),
            responseString = JSON.stringify({ [field]: value }),
            comp = typeof this.isValid === 'boolean' ? this.completed : true;

        if (typeof value !== 'string' || value.length === 0) {
            this.completed = false;
            this.removeAnswer(this.answer);
            return;
        }

        if (!(validateRegex instanceof RegExp)) {
            this.completed = comp;
        } else {
            this.completed = validateRegex.test(value) && comp;
        }
        if (this.completed) {
            this.validation.answers = this.questionnaire.answers;
        }

        this.respondHelper(this.answer, responseString, this.completed);
    }

    /**
     * Focus handler
     */
    onFocus () {
        this.$el.find('input').removeClass('ui-state-error-border');
        Banner.closeAll();
    }

    /**
      * clears the textbox and removes the answer
      */
    clear () {
        this.completed = false;
        this.removeAnswer(this.answer);
        $(`#${this.model.get('id')}`).val('');
    }
}

window.LF.Widget.AddUserTextBox = AddUserTextBox;
