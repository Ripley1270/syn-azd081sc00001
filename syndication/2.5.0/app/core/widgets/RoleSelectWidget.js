import SelectWidgetBase from './SelectWidgetBase';
import CurrentContext from 'core/CurrentContext';
import { isSitePad, getProductName, getNested } from 'core/utilities';

const INPUT = 'DEFAULT:SelectWidget';

export default class RoleSelectWidget extends SelectWidgetBase {
    get input () {
        return INPUT;
    }

    /**
     *  Constructor
     *  @param {Object} options The class options.
     */
    constructor (options) {
        // noinspection JSAnnotator
        this.options = options;
        super(options);
        this.events = {
            'change select': 'onRoleChange',
            'click select': 'respond'
        };
    }

    /**
     * Extension of SelectWidgetBase for a particular instance of a Select Widget control.
     * @param {String} divToAppendTo the select ID that the options should be appended to
     * @return {Q.Promise<void>}
     */
    renderOptions (divToAppendTo) {
        let roleModels = LF.StudyDesign.roles.models,
            permissions = _.where(roleModels, {
                id: CurrentContext().role
            })[0],
            permissionsList = permissions.get('addPermissionsList'),
            answer = this.answer ? JSON.parse(this.answer.get('response'))[this.model.get('field')] : false,
            template = _.template('<option id="{{ roleId }}" value="{{ roleId }}">{{ displayName }}</option>'),
            roles;

        // If the permissions list contains 'ALL'...
        if (_.contains(permissionsList, 'ALL')) {
            // Reject those that do not belong in the product, the subject role,
            // and if is the product LogPad App, the admin role.
            roles = _.reject(roleModels, role =>
                (_.indexOf(role.get('product'), getProductName()) === -1) || role.get('id') === 'subject' || (!isSitePad() && role.get('id') === 'admin'));
        } else {
            // Determine if there is a target user.
            let targetUser = getNested('parent.parent.user', this);

            // Otherwise, display those that are in the permissions list,
            // or if there is a target user and the answer matches the role id.
            roles = _.filter(roleModels, (role) => {
                return _.contains(permissionsList, role.get('id')) || (targetUser && answer === role.get('id'));
            });
        }

        let addItemPromiseFactories = roles.map((role) => {
            return () => {
                return LF.strings.display(role.get('displayName'))
                .then((displayName) => {
                    let resource = { roleId: role.get('id'), displayName },
                        el = template(resource);

                    this.$(divToAppendTo).append(el);
                });
            };
        });
        return addItemPromiseFactories.reduce(Q.when, Q());
    }

    /**
     * the handler for the change event
     * @param  {Event} e The event args.
     */
    onRoleChange (e) {
        let language = _.filter(this.questionnaire.getQuestions(), qview => $(qview.el).hasClass(this.model.get('params').language))[0].widget,
            passwordFields = _.filter(this.questionnaire.getQuestions(), qview => $(qview.el).hasClass(this.model.get('params').password));

        for (let i = 0; i < passwordFields.length; i++) {
            let pass = $(passwordFields[i].widget.$el[0]),
                $target = $(pass.find('input')),
                parent = $target.parent();
            passwordFields[i].widget.clear();
            if (parent.hasClass('has-feedback')) {
                $target.siblings('.form-control-feedback').remove();
                parent.removeClass('has-feedback has-success has-warning has-error');
            }
        }

        language.refreshLanguages($(`#${this.model.id}`).val());
        this.questionnaire.data.newUserRole = this.$(e.target).val();
        this.respond(e);
    }
}

window.LF.Widget.RoleSelectWidget = RoleSelectWidget;
