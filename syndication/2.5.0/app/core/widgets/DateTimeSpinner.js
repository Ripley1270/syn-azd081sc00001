import DateTimeSpinnerMixin from './mixin/DateTimeSpinnerMixin';
import TextBoxCustomInputBase from './TextBoxCustomInputBase';

/**
 * Variation of Interval Spinner for Date and Time.
 * Docs to be produced when widget is validated.
 */
class DateTimeSpinner extends TextBoxCustomInputBase {
    /**
     * Construct a DateTimeSpinner by mixing TextBoxCustomInputBase with DateTimeSpinnerMixin.
     * @param {Object} options options for this widget
     */
    constructor (options) {
        options.model.set('modalMixin', DateTimeSpinnerMixin);
        return super(options);
    }
}

window.LF.Widget.DateTimeSpinner = DateTimeSpinner;
export default DateTimeSpinner;
