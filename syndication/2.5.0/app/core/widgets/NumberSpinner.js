import TextBoxCustomInputBase from './TextBoxCustomInputBase';
import NumberSpinnerMixin from './mixin/NumberSpinnerMixin';

/**
 * The NumberSpinner input control implements the BaseSpinner interface, to create a numeric control.
 * <ul>
 *      <li>NumberSpinnerInput is the input.</li>
 *      <li>Spinners are injected into containers with
 *           the 'number-spinner-container' CSS class in the modal template.</li>
 *      <li>Creating an array to send to spinners splits the string on '.'
 *              and sends an array of size 2 to a decimal spinner.</li>
 *      <li>Values are concatenated together when returning from the spinner.</li>
 * </ul>
 * <p><i>Note: 3 things passed in from study-design.js must be arrays in this widget.  They depend
 * on there being the same number of HTML elements with the class of 'number-spinner-container' in the modal template.
 * Each index of these arrays will apply to each spinner, respectively, based on the order they occur in the template.
 * <ul>
 *     <li>inputOptions</li>
 *     <li>templates.spinnerTemplates</li>
 *     <li>templates.spinnerItemTemplates</li>
 * </ul></i>
 */
class NumberSpinner extends TextBoxCustomInputBase {
    // Disabling ESLint for the next line.  The parser seems to be a bit broken, and it thinks that
    // @param {boolean} [options.model.inputOptions[].showButtons]
    // is a duplicate param to:
    // @param {boolean} [options.model.showButtons=false]

    // eslint-disable-next-line valid-jsdoc
    /**
     * Construct the number spinner
     * @param {Object} options options for this widget (see super class for option definitions).
     * @param {Object} options.model The model containing the widget configuration.
     *  This represents the "widget" node in the study-design for this question.
     * @param {string<translated>} options.model.label {dev-only} The text label for the spinner
     * @param {string<translated>} options.model.modalTitle The title string for the modal dialog.
     * @param {number} [options.model.min] The overall min value for combined spinners in this control.
     *  (Note, min still needs to be set to reasonable levels for each spinner instance)
     * @param {number} [options.model.max] The overall max value for combined spinners in this control.
     *  (Note, max still needs to be set to reasonable levels for each spinner instance)
     * @param {boolean} [options.model.embedded=false] Whether or not to embed spinner on page (no modal).
     * @param {boolean} [options.model.showButtons=false] Whether or not to show buttons for each spinner.
     *  Can be overridden per-spinner in inputOptions.
     * @param {string} [options.model.buttonLayout=vertical] (horizontal|vertical) Button layout for spinner
     * (vertical: north/south or horizontal: east/west).  Can be overridden per-spinner in inputOptions.
     * @param {Array<Object>} options.model.inputOptions Array of options for each spinner in the template.
     * @param {number} options.model.inputOptions[].min min value for this spinner instance.
     * @param {number} options.model.inputOptions[].max max value for this spinner instance.
     * @param {number} options.model.inputOptions[].precision number of decimal places to show in this spinner instance.
     * @param {boolean} [options.model.inputOptions[].showLeadingZeros=false] {dev-only}
     *  whether or not to show leading zeroes in this spinner instance.
     * @param {boolean} [options.model.inputOptions[].showButtons] {dev-only}
     *  whether or not to show buttons (lets you override the default behavior for an individual spinner).
     * @param {string} [options.model.inputOptions[].buttonLayout] {dev-only} (horizontal|vertical)
     *  button layout for an input option (lets you override the default behavior for an individual spinner).
     * @param {number} options.model.inputOptions[].step step intervals for this spinner instance.
     * @param {Object} [options.model.inputOptions[].labels] Similar to "labels" in the root,
     * but used to override things like upButtonText/downButtonText for a specific input (spinner).
     * Defaults to using the widget's default, but can be specified here if they need to be different.
     * @param {string} options.model.id {dev-only} The ID property for this widget.
     * @param {string} [options.model.type=NumberSpinner] {dev-only} The widget type.
     * @param {Object} [options.model.labels={labelOne: '', labelTwo: '', upButtonText: '', downButtonText: ''}]
     * {dev-only} The label strings.  Used to replace the label placeholders in the modal dialog template.
     * Note:  upButtonText and downButtonText are applied to the up and down buttons in the spinnerInput objects.
     * The text can be overridden in inputOptions by supplying another
     * labels object there.  If left as default, upButtonText and downButtonText are "+" and "-" glyphs.
     * @param {string} [options.model.okButtonText=OK] {dev-only} OK button text.
     * @param {string} [options.model.className=NumberSpinner] {dev-only} The CSS classname for the widget.
     */
    constructor (options) {
        options.model.set('modalMixin', NumberSpinnerMixin);
        return super(options);
    }
}

window.LF.Widget.NumberSpinner = NumberSpinner;
export default NumberSpinner;
