/* eslint-disable max-lines */

import ELF from 'core/ELF';
import Sound from 'core/classes/Sound';
import Answers from 'core/collections/Answers';
import { MessageRepo, Banner } from 'core/Notify';
import NotifyView from 'core/views/NotifyView';
import Logger from 'core/Logger';
import ComponentView from 'core/views/ComponentView';
import { createGUID } from 'core/utilities';

const logger = new Logger('WidgetBase');

/**
 * Base class for all widgets
 */
export default class WidgetBase extends ComponentView {
    /**
     * Construct a new widget
     * @param {Object} options widget options
     * @param {Element} options.parent parent element for this widget
     * @param {boolean} options.mandatory whether or not this widget is mandatory
     * @param {Widget} options.model model for widget params
     * @param {Object} options.screen screen object
     */
    constructor (options) {
        let className,
            swAlias,
            answers;

        super(options);

        /**
         * Handles all application sounds.
         * @type  LF.Class.Sound
         */
        this.sound = new Sound();

        className = this.model.get('className') || this.className;

        this.options = options;
        this.parent = this.options.parent;
        this.answers = new Answers();
        this.mandatory = this.options.mandatory || false;
        this.completed = false;
        this.paramsSetup = false;
        this.uniqueKey = createGUID();

        LF.Widget.ParamFunctions.subject = this.questionnaire.subject;

        // If the questionnaire is in edit mode...
        if (this.questionnaire.editMode) {
            let curIGR = this.parent.ig;
            let IGRNumber = this.questionnaire.data.currentIGR[curIGR];
            let id = this.getQuestion().model.get('id');
            swAlias = `${this.getQuestion().model.get('IG')}.${IGRNumber}.${(this.getQuestion().model.get('IT'))}`;

            // FIXME: This is a quick last minute fix for DE21835 in final validation.
            // This is an ugly fix that aims to reduce the regression required
            if (this.question.model.get('localOnly')) {
                answers = this.questionnaire.data.answers.match({
                    SW_Alias: swAlias,
                    question_id: id
                });
            } else {
                answers = this.questionnaire.data.answers.match({
                    SW_Alias: swAlias
                });
            }

            if (answers) {
                this.answers.add(answers);

                this.answers.each((answer) => {
                    this.respondHelper(answer, answer.get('response'), true);
                });
            }
        }

        // Set the class name of the widget
        this.$el.attr('class', className);
    }

    /**
     * Get questionnaire object
     * @type {Questionnaire} questionnaire object
     */
    get questionnaire () {
        return this.question.getQuestionnaire();
    }

    /**
     * Get string representation of this widget
     * @returns {string}
    */
    toString () {
        return `${this.constructor.name}:${this.id}`;
    }

    /**
     * This function returns a flag which indicates whether the widget needs different css
     * when rendered along with other widgets on one screen. Default value is false
     * e.g. VVAS, IMAGE and EQ5D widgets do need to apply different css when they are used
     * with other widgets on one screen
     * @returns {boolean} - false by default
     */
    get needMultipleWidgetSupport () {
        return false;
    }


    /**
     * This function pre-processes a widget value (typically before next or previous)
     * @returns {Q.Promise<void>} promise resolving when operation is complete.
     */
    processValue () {
        return Q();
    }

    /**
     * Override backbone delegateEvents.
     * Also include cleaning up if div used for identifying this widget is removed.
    */
    delegateEvents () {
        super.delegateEvents();

        if (this.$el) {
            this.destroy = _.bind(this.destroy, this);
            this.$(`#${this.model.get('id')}`).on('remove', this.destroy);
        }
    }

    /**
     * Override backbone undelegateEvents.
     * Also remove our custom event handler for destroying the widget.
    */
    undelegateEvents () {
        super.undelegateEvents();

        if (this.$el) {
            this.$(`#${this.model.get('id')}`).off('remove', this.destroy);
        }
    }

    /**
     * This function takes care of any shutdown of our widget
     */
    destroy () {
        this.undelegateEvents();

        // Call to remove/cleanup the entire widget.
        this.remove();
    }

    /**
     * This function is a workaround for an MS Edge issue.
     * See https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/4983269/
     * @param {Event} e - The event args
     * @returns {number} - The appropriate pageX for the platform.
     */
    getPageX (e) {
        let ev = this.getClickOrTouchEvent(e);
        let pageX = ev.pageX;

        if (ev.clientX != null && window.scrollX != null) {
            pageX = ev.clientX + window.scrollX;
        }

        return pageX;
    }

    /**
     * Get touch event or regular click event from event object.
     * @param {Event} e event object
     * @returns {Event}
     */
    getClickOrTouchEvent (e) {
        return e.touches ? e.touches[e.touches.length - 1] : e;
    }

    /**
     * This function is a workaround for an MS Edge issue.
     * See https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/4983269/
     * @param {Event} e - The event args
     * @returns {number} - The appropriate pageY for the platform.
     */
    getPageY (e) {
        let ev = this.getClickOrTouchEvent(e);
        let pageY = ev.pageY;

        if (ev.clientY != null && window.scrollY != null) {
            pageY = ev.clientY + window.scrollY;
        }

        return pageY;
    }

    /**
     * Iterates through the widget's model looking for functions used to set
     * the model attributes.
     * @returns {Q.Promise}
     */
    setupModel () {
        if (
            this.model.has('paramFunctions') &&
            (this.completed === false || localStorage.getItem('screenshot') === 'true')
        ) {
            let paramFunctions = this.model.get('paramFunctions');
            for (let i = 0; i < paramFunctions.length; i++) {
                this.model.set(paramFunctions[i].id, paramFunctions[i]);
            }
        }

        // This is a temporary workaround for deprecating the answerFunc attribute for multiple choice widgets.
        if (this.model.has('answerFunc')) {
            logger.warn('answerFunc has been deprecated.  ' +
                'Please set the answers attribute to be your answers setup function.');

            // Copy to answers:
            this.model.set('answers', this.model.get('answerFunc'));

            // Remove the answerFunc:
            this.model.unset('answerFunc', { silent: true });
        }

        // This is a temporary workaround for deprecating the configFunc attribute for multiple choice widgets.
        if (this.model.has('configFunc') && !this.paramsSetup) {
            logger.warn('configFunc has been deprecated.  Please set individual attributes to functions.');

            // Copy to configuration (for time widgets and things that use 'configuration' model property):
            this.model.set('configuration', this.model.get('configFunc'));

            // DO NOT remove configFunc from model.  Just escape if you run into it in processAttributes.
        }

        let attrs = this.model.attributes;

        // Iterate through the attributes.  If any of the attributes are functions, add them to the promise factory,
        // which will be executed at the end of the function:
        let aFactory = this.processAttributes(attrs);

        // Reduce the array and execute the promises:
        return aFactory.reduce(Q.when, Q())
        .then(() => {
            this.paramsSetup = true;
        });
    }

    /**
     * Process the given array of model attributes, running any functions
     * to set the attribute value, or recursing on any attribute that is an object.
     * We'll let any errors from the functions bubble up the core, which will halt the rendering of the widget.
     * @param {Array} attrs - Array of attributes to parse
     * @param {string} [namespace=''] - dot-delimited path to the current point in the object.  Used to create a unique
     * string for caching functions.
     * @returns {Array} - An array of Promises
     */
    processAttributes (attrs, namespace = '') {
        let aFactory = [],
            attrKeys = _.keys(attrs);

        this.cachedFunctions = this.cachedFunctions || {};
        for (let i = 0; i < attrKeys.length; i++) {
            let key = attrKeys[i],
                fullKeyPath = namespace === '' ? key : `${namespace}.${key}`,
                attribute = attrs[key];
            if (!(key === 'validationFunc' || key === 'configFunc' || key === 'paramFunctions')) {
                let runner;
                if (this.paramsSetup) {
                    // If the screen has already been rendered,
                    // look for a cached function and run it if it exists
                    // Only run cached functions,
                    // in case objects and other functions were stored in the model by the widget
                    runner = this.cachedFunctions[fullKeyPath];
                } else if (typeof attribute === 'function') {
                    runner = attribute;
                } else if (typeof attribute === 'string') {
                    runner = LF.Widget.ParamFunctions[attribute];
                } else if (attribute && typeof attribute.evaluate === 'function') {
                    runner = attribute.evaluate;
                } else if (attribute && typeof attribute.evaluate === 'string') {
                    runner = LF.Widget.ParamFunctions[attribute.evaluate];
                }

                if (typeof runner === 'function') {
                    // cache the property in cachedFunctions object to be run if the screen is re-rendered.
                    this.cachedFunctions[fullKeyPath] = runner;
                    aFactory.push(() => {
                        return Q()
                        .then(() => {
                            if (attribute.screenshots) {
                                return ELF.trigger('PARAMFUNCTION:Trigger', { key, paramFunction: attribute })
                                .then((res) => {
                                    if (res.value) {
                                        this.answer = null;
                                        this.removeAllAnswers();
                                        return res.value;
                                    }
                                    return runner.call(this);
                                });
                            }
                            return runner.call(this);
                        })
                        .then((val) => {
                            attrs[key] = val;
                        });
                    });
                } else if (attribute && (_.isArray(attribute) || attribute.constructor === Object)) {
                    // use constructor to ensure that this is a simple object,
                    // not a constructed model or RegExp or something
                    // which may happen to the model when returning to a screen that has already been rendered.
                    aFactory = aFactory.concat(this.processAttributes(attribute, fullKeyPath));
                }
            }
        }

        return aFactory;
    }

    /**
     * Gets the question (parent) of this widget.
     * @returns {QuestionView}
     */
    getQuestion () {
        return this.question;
    }

    /**
     * gets the parent question of this widget
     * @returns {*}
     */
    get question () {
        return this.parent;
    }

    /**
     * fired when value is set. saves the UI Control value into the answer associated with the widget
     * @abstract
     * @returns {Q.Promise<void>} promise resolved when complete.
     */
    respond () {
        logger.error('unexpected call to WidgetBase.respond().  Should be overridden by extending class.');
        return Q();
    }

    /**
     * Responsible for displaying the widget.
     * @abstract
     * @returns {Q.Promise<void>} resolved when draw is complete
     */
    render () {
        logger.error('unexpected call to WidgetBase.render().  Should be overridden by extending class.');
        return Q();
    }

    /**
     * Respond to user input and answer the question.
     * @param {Object} model The answer record to modify.
     * @param {string} [response=null] The response to be stored.
     * @param {boolean} [completed=true] True if the response should set the widget to a completed state.
     * @returns {Q.Promise<void>} promise resolved when complete.
     * @example LF.Widget.Base.prototype.respond.call(this, this.answer, "25", true);
     * @protected
     */
    respondHelper (model, response = null, completed = true) {
        // Set the answers numeric value to the target's value.
        model.set({ response });

        this.saveAnswerType(model);

        // prepopulate widget internal answer
        this.answer = model;

        if (model.get('question_id') === 'AFFIDAVIT') {
            if (this.isAffidavitAnswered()) {
                logger.operational(`Diary ${model.get('questionnaire_id')} signed.`, {
                    diary: model.get('questionnaire_id')
                });
            } else {
                logger.operational(`Diary ${model.get('questionnaire_id')} unsigned.`, {
                    diary: model.get('questionnaire_id')
                });
            }
        }

        this.completed = completed;

        if (this.completed && this.skipped) {
            this.skipped = false;
        }

        this.trigger('response');

        return ELF.trigger(`QUESTIONNAIRE:Answered/${this.getQuestion().getQuestionnaire().id}/${this.question.id}`, {
            questionnaire: this.questionnaire.id,
            screen: this.options.screen,
            question: this.question.id,
            answer: this.answers
        }, this.question.getQuestionnaire());
    }

    /**
     * Validate the widget.
     * @returns {boolean} Returns true if the widget is valid, otherwise false
     * @example this.validate();
     */
    validate () {
        return !this.mandatory || this.completed;
    }

    /**
     * Figures out which validation errors have failed, and throws an error for the proper one.
     * @returns {Q.Promise<boolean>} true if an error message was actually displayed here.
     */
    displayError () {
        // eslint-disable-next-line consistent-return
        return Q.Promise((resolve) => {
            let validationErrors = this.model.attributes.validationErrors,
                currentError = null,
                notifyStrings = {};

            if (this.validate()) {
                resolve(false);
            }

            // Validation errors not in the model.  Create an empty array so it will act like it didn't find one.
            if (!_.isArray(validationErrors)) {
                validationErrors = [];
            }

            for (let i = 0, length = validationErrors.length; i < length; ++i) {
                // Must be an actual boolean false.  Ignore undefined.
                if (this[validationErrors[i].property] === false) {
                    currentError = validationErrors[i];
                    break;
                }
            }

            // Error doesn't match any of our passed in validation errors.  Throw a generic message instead.
            if (currentError === null) {
                logger.warn('REQUIRED_ANSWER QuestionnaireID: ' +
                    `${this.getQuestion().getQuestionnaire().id}, QuestionID: ${this.getQuestion().id}`);
                return MessageRepo.display(MessageRepo.Banner.REQUIRED_ANSWER)
                .then(() => {
                    resolve(true);
                });
            }

            notifyStrings.header = {
                key: currentError.header || 'VALIDATION_HEADER',
                namespace: this.questionnaire.id
            };

            notifyStrings.body = {
                key: currentError.errString,
                namespace: this.questionnaire.id
            };

            notifyStrings.ok = {
                key: currentError.ok || 'VALIDATION_OK',
                namespace: this.questionnaire.id
            };

            logger.error(currentError.errString);

            LF.getStrings(notifyStrings)
            // eslint-disable-next-line consistent-return
            .then((strings) => {
                let dialog,
                    banner;

                /**
                 * Get the error message
                 * @param {string} type type of error
                 * @returns {string} error message for this type.
                 */
                const getErrorMessage = (type) => {
                    const { messageKey, errString } = currentError;

                    // Can either pass a custom message key or use ${errString}_${questionnaire.id}
                    const key = messageKey || `${errString}_${this.questionnaire.id}`;

                    return MessageRepo[type] && MessageRepo[type][key];
                };

                switch (currentError.errorType) {
                    case 'alert':
                        // eslint-disable-next-line no-alert
                        alert(strings.body);
                        break;
                    case 'popup':
                        dialog = getErrorMessage('Dialog');

                        // Since the strings are configured in the study,
                        //  core may or may not have a registered message here.
                        if (dialog) {
                            return MessageRepo.display(dialog);
                        }

                        return new NotifyView().show(strings);
                    default:
                        banner = getErrorMessage('Banner');

                        // Since the strings are configured in the study,
                        //  core may or may not have a registered message here.
                        if (banner) {
                            return MessageRepo.display(banner);
                        }

                        return Banner.show({
                            text: strings.body,
                            type: 'error'
                        });
                }
            })
            .then(() => {
                resolve(true);
            });
        });
    }

    /**
     * Set a record as skipped.
     * @param {string} swAlias Skipped IT
     * @returns {boolean} Returns true if the skipped record is set, otherwise false.
     */
    skip (swAlias) {
        if (this.completed) {
            return false;
        }

        this.skipped = new (this.question.getQuestionnaire()).Answer();

        try {
            this.skipped.set({
                question_id: this.question.model.get('id'),
                questionnaire_id: this.question.getQuestionnaire().model.get('id'),
                response: '1',
                SW_Alias: swAlias
            });
        } catch (err) {
            // eslint-disable-next-line no-console
            console.log(err);
        }

        return true;
    }

    /**
     * Returns the value to be set as Type
     * @returns {null}
     */
    getAnswerType () {
        return null;
    }

    /**
     * Sets the answer type.
     * @param {Answer} model - The answer model
     */
    saveAnswerType (model) {
        let type = this.getAnswerType();

        if (type != null) {
            model.set({ type });
        }
    }

    /**
     * Add an answer record to the collection.
     * @param {Object} [params] An object literal
     * @example this.addAnswer();
     * @returns {Object}
     */
    addAnswer (params = {}) {
        let model,
            IG = this.question.model.get('IG'),
            IT = params.IT || this.question.model.get('IT'),
            swAlias;

        if (this.question.model.get('id') === 'AFFIDAVIT') {
            swAlias = params.krSig || this.question.model.get('krSig');
        } else {
            swAlias = `${IG}.${this.question.getQuestionnaire().getCurrentIGR(IG)}.${IT}`;
        }

        try {
            let questionnaire = this.question.getQuestionnaire();

            model = new questionnaire.Answer({
                user_id: LF.security.activeUser ? LF.security.activeUser.get('id') : null,
                question_id: this.question.model.get('id'),
                questionnaire_id: questionnaire.model.get('id'),
                response: params.response || null,
                SW_Alias: swAlias,
                localOnly: this.question.model.get('localOnly') || false
            });

            this.saveAnswerType(model);
            this.answers.add(model);
            this.questionnaire.data.answers.add(model);
        } catch (e) {
            // eslint-disable-next-line no-console
            console.log(e);
        }
        return model;
    }

    /**
     * remove an answer record from the collections.
     * @param {Object} model An answer model to be removed from the collection.
     * @example this.removeAnswer(model);
     */
    removeAnswer (model) {
        this.answers.remove(model);
        this.questionnaire.data.answers.remove(model);
        this.completed = false;
    }

    /**
     * remove all answer records from the collections.
     */
    removeAllAnswers () {
        for (let i = this.answers.length - 1; i >= 0; i--) {
            this.removeAnswer(this.answers.at(i));
        }
    }

    /**
     * Save the entire answer collection to the database
     * @param {number} dashboardId - the id of the dashboard record of the questionnaire
     * @returns {Q.Promise<void>}
     */
    save (dashboardId) {
        // TODO: This method only saves the skipped answers. This is inconsistent with how the rest
        //       of the answers are saved. Needs refactoring.

        // If a skipped record exists, save it.
        if (this.skipped) {
            return this.skipped.save({ dashboardId });
        }

        return Q();
    }

    /**
     * If the widget is used as an affidavit, returns whether the affidavit is answered or not
     * @returns {boolean} Returns true if the affidavit is answered
     * @example this.isAffidavitAnswered();
     * Moved the decision making logic to the widget space where it makes more sense.
     * By doing so every widget can implement their own logic by overriding this function.
     * The behaviour in this function is the behaviour of the CheckBox widget/affidavit
     * and it was being used as the default behaviour for all widgets/affidavits so it is kept that way.
     */
    isAffidavitAnswered () {
        let res = this.answers.toJSON();
        return res && res[0].response === '1';
    }

    /**
     * An alias for LF.templates.display. See {@link core/collections/Templates Templates} for details.
     * @param {string} name The name of the template to render.
     * @param {Object} options The options passed to the template.
     * @returns {string} The rendered template.
     */
    renderTemplate (name, options) {
        return LF.templates.display(name, options);
    }
}

WidgetBase.prototype.tagName = 'div';
WidgetBase.prototype.className = 'widget';

window.LF.Widget.WidgetBase = WidgetBase;
