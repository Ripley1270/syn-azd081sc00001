import WidgetBase from './WidgetBase';

const defaultImageTemplate = 'DEFAULT:ImageTemplate',
    defaultImageFolder = 'IMAGE_FOLDER',
    defaultSingleSelect = false,
    defaultOptions = [{
        value: '0',
        css: {
            fill: '#FFFFFF'
        }
    }, {
        value: '1',
        css: {
            fill: '#0000FF'
        }
    }];

/**
 * This widget allows for creating a single-select or multi-select SVG image.  On a high level, the SVG image can be configured to do two things:
 * <ol>
 *  <li>Make Paths that are selectable.  To do this, give paths an ID that passes the nodeIDRegex regular expression.  This
 *      will make selecting an item act accordingly with the answers model, and apply the styles in selectedCSS to the node.</li>
 *  <li>Make TextAreas that translated text can go into.  To do this, make a rect with a specific ID in the location that you want
 *      text.  Pass any references to that text as key value pairs (key is the ID, value is the text lookup) into imageText.</li>
 */
class ImageWidget extends WidgetBase {
    get defaultImageTemplate () {
        return defaultImageTemplate;
    }
    get defaultImageFolder () {
        return defaultImageFolder;
    }
    get defaultSingleSelect () {
        return defaultSingleSelect;
    }
    get defaultOptions () {
        return defaultOptions;
    }

    /**
     * Construct the image widget
     * @param {Object} options options for this widget (see super class for option definitions).
     * @param {Object} options.model The model containing the widget configuration.  This represents the "widget" node in the study-design for this question.
     * @param {boolean} [options.model.singleSelect=false] Whether or not the control is single select.  <ul><li>True will act as a radio-button (returning the node ID as the single answer).</li>
     *              <li>False will act like a checkbox.  Each node translates to an IT (by pre-pending "{QuestionID}_"), and those all are entered as separate answers to the collection,
     *              with responses of 1 or 0.</li></ul>
     * @param {Array} [options.model.options] array of options, usually defaulting to an array of 2... a selected (value 0) option and an unselected (value 1) option.
     *      This is also where CSS for different selected states is defined.  Making this array larger than 2 items will do nothing in single select mode, but will
     *      make click events cycle through the available options in multi-select mode.
     * @param {string} [options.model.options[0].css.fill=#FFFFFF] Style fill color for an unselected node.
     * @param {string} [options.model.options[0].value=0] {dev-only} value for an unselected node.
     * @param {string} [options.model.options[1].css.fill=#0000FF] Style fill color for a selected node.
     * @param {string} [options.model.options[1].value=1] {dev-only} value for a selected node.
     * @param {Object} [options.model.imageText] object of key-value pairs.  Keys are rect IDs in the SVG file, and the values of these are the lookups for text in the translated text area.
     * @param {Object} options.model.imageName {dev-only} Image name of the SVG inside the configured imageFolder.
     * @param {string} options.model.nodeIDRegex {dev-only} regular expression.  When this expression passes with a path ID in the SVG file, this indicates that it is selectable.
     * @param {string} [options.model.imageFolder=IMAGE_FOLDER] {dev-only} Image folder to be used by the config.  This key is looked up as translated text, and defaults to the standard image folder.
     * @param {Object} [options.model.templates] {dev-only} templates for this widget.  The only template to be set is the imageTemplate, which defaults to DEFAULT:ImageTemplate.
     */
    constructor (options) {
        super(options);

        /**
         * List of the widgets events
         * @readonly
         * @enum {Event}
         */
        this.events = {
        };

        this.$svgPaths = null;

        this.completed = false;
    }

    /**
     * Setup all options.
     * 1. Default to the default array if nothing is supplied.
     * 2. Extend the default array into passed in objects (in case a param is missing)
     * 3. Validate options (verify values are 0 and 1 for single select).
     */
    setupOptions () {
        this.model.set('options', this.model.get('options') || this.defaultOptions);

        // Default what we can with options.
        let curOptions = this.model.get('options');
        if (curOptions.length < 2) {
            throw new Error('Options array must have at least two options.');
        }

        // Extend options in case only CSS or only value was supplied.
        for (let i = 0; i < this.defaultOptions.length; ++i) {
            // Empty CSS should not override the default.
            if (_.keys(curOptions[i].css).length === 0) {
                delete curOptions[i].css;
            }
            _.defaults(curOptions[i], this.defaultOptions[i]);
        }

        // Validate that there is an option for 0
        if (this.getOptionIndex('0') === -1) {
            throw new Error('Options array must have an element with value=0 (for an unselected value).');
        }

        if (this.model.get('singleSelect') && this.getOptionIndex('1') === -1) {
            throw new Error('Options array must have an element with value=1 in singleSelect mode (1 is selected and 0 is unselected)');
        }
    }

    /**
     * Setup all model parameters.
     * @returns {Q.Promise<void>} promise resolving when setup is complete.
     */
    setupModelParams () {
        return Q()
        .then(() => {
            this.model.set('singleSelect',
                typeof this.model.get('singleSelect') === 'boolean' ? this.model.get('singleSelect') : this.defaultSingleSelect);
            this.model.set('imageText', this.model.get('imageText') || {});

            // Setup and validate options.
            this.setupOptions();

            if (!this.model.get('imageName')) {
                throw new Error('Missing property "imageName" in ImageWidget.');
            }

            if (!(this.model.get('nodeIDRegex') instanceof RegExp)) {
                throw new Error('Configuration error.  nodeIDRegex must supplied as a regular expression, used to test if nodes are selectable.');
            }

            this.model.set('imageFolder', this.model.get('imageFolder') || this.defaultImageFolder);

            let templates = this.model.get('templates') || {};
            templates.imageTemplate = templates.imageTemplate || this.defaultImageTemplate;
            this.model.set('templates', templates);
        });
    }

    /**
     * Register click events on our SVG Paths.
     */
    registerClickEvents () {
        if (this.$svgPaths) {
            this.$svgPaths.on('click', this.respond);
        }
    }

    /**
     * Unregister click events on our SVG Paths.
     */
    unRegisterClickEvents () {
        if (this.$svgPaths) {
            this.$svgPaths.off('click', this.respond);
        }
    }

    /**
     * Override of delegateEvents to add custom events.
     */
    delegateEvents () {
        super.delegateEvents();
        this.respond = _.bind(this.respond, this);
        this.destroy = _.bind(this.destroy, this);
        if (this.$el) {
            this.$el.on('remove', this.destroy);
        }
        this.jQueryNodeFilter = _.bind(this.jQueryNodeFilter, this);
        this.registerClickEvents();
    }

    /**
     * Override of undelegateEvents to remove all events, including custom ones.
     */
    undelegateEvents () {
        this.unRegisterClickEvents();
        if (this.$el) {
            this.$el.off('remove', this.destroy);
        }
        super.undelegateEvents();
    }

    /**
     * Filter the jquery values
     * @param {number} index index passed by jQuery filter function.  Unused, required for polymorphism.
     * @param {Element} element HTML element passed to the jQuery filter function
     * @returns {boolean} whether or not it matches our regex
     */
    jQueryNodeFilter (index, element) {
        return this.model.get('nodeIDRegex').test(element.id);
    }

    /**
     * Sort the jquery values
     * @param {Element} elementA first element to compare
     * @param {Element} elementB second element to compare
     * @returns {number} 1, 0, or -1 for comparison result.
     */
    jQueryNodeSort (elementA, elementB) {
        let aID = $(elementA).attr('id'),
            bID = $(elementB).attr('id');

        if (aID > bID) {
            return 1;
        } else if (bID > aID) {
            return -1;
        }
        return 0;
    }

    /**
     * Build the widget HTML
     * @returns {Q.Promise<void>} promise resolving when HTML is added to the DOM.
     */
    buildHTML () {
        return Q()
        .then(() => {
            $('#questionnaire').addClass('ImageWidget');
        })
        .then(() => {
            return this.i18n({ imageFolder: this.model.get('imageFolder') },
                () => null,
                { namespace: this.getQuestion().getQuestionnaire().id }
            );
        })
        .then((strings) => {
            let content = this.renderTemplate(this.model.get('templates').imageTemplate, {
                imageFolder: strings.imageFolder,
                imageName: this.model.get('imageName')
            });

            this.$el.html(content);
        })
        .then(() => {
            // suppress a context menu click (or long press) by returning false: DE19476
            $(this.$('object.imageSvg')[0].contentDocument).contextmenu(() => false);
        });
    }

    /**
     * Create a text node inside a rect in the SVG.  Rect nodes act as placeholders for new text nodes.
     * Overlay new text nodes on the rects and render the text in them.
     * @param {Element} $rect SVG node on which to overlay the text.
     * @param {string} text text to write in the new text node.
     * @returns {jQuery} jQuery lookup for the text node.
     */
    createTextNodeInRect ($rect, text) {
        let $textNode = $(document.createElementNS('http://www.w3.org/2000/svg', 'text'));

        // Eventually allow other justifications, but also figure out how to measure them.
        $textNode.attr('x', parseFloat($rect.attr('x')) + (parseFloat($rect.attr('width')) / 2));
        $textNode.attr('y', parseFloat($rect.attr('y')) + (parseFloat($rect.attr('height')) / 2));
        $textNode.attr('height', parseFloat($rect.attr('height')));
        $textNode.attr('width', parseFloat($rect.attr('width')));
        $textNode.attr('text-anchor', 'middle');
        $textNode.attr('alignment-baseline', 'central');

        $textNode.append(document.createTextNode(text));
        $rect.parent().append($textNode);
        return $textNode;
    }

    /**
     * Inject all SVG text from the 'imageText' object passed into the model.
     * @returns {Q.Promise<void>} promise resolving when all text has been injected.
     */
    injectSVGText () {
        return Q()
        .then(() => {
            return this.i18n(this.model.get('imageText'),
                () => null,
                { namespace: this.getQuestion().getQuestionnaire().id }
            );
        })
        .then((strings) => {
            let $allSvgNodes = $(this.$('object.imageSvg')[0].contentDocument).find('path, rect');

            _.map(strings, (value, key) => {
                this.createTextNodeInRect($($allSvgNodes.filter(`#${key}`).get(0)), value);
            });
        });
    }

    /**
     * Retrieve an index for the option with a particular value.
     * @param {string} answerValue value to retrieve
     */
    getOptionIndex (answerValue) {
        let options = this.model.get('options');
        return _.findIndex(options, (option) => {
            return option.value === answerValue;
        });
    }

    /**
     * Get the index of the next available option, after the selected value.
     * @param {string} answerValue value to iterate
     */
    getNextOptionIndex (answerValue) {
        return (this.getOptionIndex(answerValue) + 1) % this.model.get('options').length;
    }

    /**
     * Parse answers collection and update the UI to match (should happen on load of the widget)
     * @returns {Q.Promise<void>} promise resolving when this is done.
     */
    parseAnswers () {
        // If no SVGPaths, we can't setup answers.  This is dependent on render being finished, which is why it is
        // chained to render.
        if (this.$svgPaths === null) {
            return Q();
        }

        return Q()
        .then(() => {
            let options = this.model.get('options');

            if (!this.model.get('singleSelect')) {
                if (this.answers.size() !== 0) {
                    this.$svgPaths.each((index, item) => {
                        let $el = $(item),
                            selectedOption = options[this.getOptionIndex(this.answers.at(index).get('response'))];

                        $el.data('state', selectedOption.value);
                        $el.css(selectedOption.css);
                    });
                } else {
                    this.$svgPaths.each((index, item) => {
                        let $el = $(item),
                            unselectedOption = options[this.getOptionIndex('0')];
                        $el.data('state', unselectedOption.value);
                        $el.css(unselectedOption.css);
                        this.addAnswer({
                            IT: this.generateITFromNodeID($(item).attr('id')),
                            response: unselectedOption.value
                        });
                    });
                }
            } else {
                this.$svgPaths.each((index, item) => {
                    let $el = $(item),
                        unselectedOption = options[this.getOptionIndex('0')];
                    $el.data('state', unselectedOption.value);
                    $el.css(unselectedOption.css);
                });

                if (this.answers.size() !== 0) {
                    this.$svgPaths.filter(`#${this.answers.at(0).get('response')}`).each((index, item) => {
                        let $el = $(item),
                            selectedOption = options[this.getOptionIndex('1')];
                        $el.data('state', selectedOption.value);
                        $el.css(selectedOption.css);
                    });
                }
            }
        });
    }

    /**
     * Responsible for displaying the widget.
     * @returns {Q.Promise<void>} fulfilled when widget is finished being rendered
     */
    render () {
        let that = this;

        return that.setupModelParams()
        .then(() => {
            return that.buildHTML();
        })
        .then(() => {
            return Q.Promise((resolve) => {
                that.$el.appendTo(that.getQuestion().$el)
                    .ready(() => {
                        let svgLoaded;
                        svgLoaded = () => {
                            this.$('object.imageSvg').off('load', svgLoaded);

                            // unassign variable to remove references.
                            svgLoaded = null;
                            resolve();
                        };
                        this.$('object.imageSvg').on('load', svgLoaded);
                    });
            });
        })
        .then(() => {
            return this.injectSVGText();
        })
        .then(() => {
            that.$svgPaths = $(this.$('object.imageSvg')[0].contentDocument)
                .find('path')
                .filter(this.jQueryNodeFilter)
                .sort(this.jQueryNodeSort);
            that.delegateEvents();
        })
        .then(() => {
            return this.parseAnswers();
        });
    }

    /**
     * Generate a node ID from the IT.  IT will typically be prepended with the question ID, so remove the prepended string.
     * This function is intended to be overridden if it makes sense to have a different conversion method for IT/Node ID.
     * @param {string} it IT to convert.
     * @returns {string} result of the conversion.
     */
    generateNodeIDFromIT (it) {
        return it.replace(`${this.getQuestion().id}_`, '');
    }

    /**
     * Generate an IT from the node ID.  IT will typically be prepended with the question ID, so add the prepending string.
     * This function is intended to be overridden if it makes sense to have a different conversion method for IT/Node ID.
     * @param {string} id ID to convert.
     * @returns {string} result of the conversion.
     */
    generateITFromNodeID (id) {
        return `${this.getQuestion().id}_${id}`;
    }

    /**
     * Handle a single select from the user.  Select the selected node and unselect the other nodes.
     * @param {jQuery} $selectedNodes jQuery lookup for selected node (called selectedNodes, since jQuery contains an array).
     * @returns {Q.Promise<void>} promise resolving after respondHelper is finished.
     */
    handleSingleSelect ($selectedNodes) {
        let options = this.model.get('options'),
            selectedOption = options[this.getOptionIndex('1')],
            unselectedOption = options[this.getOptionIndex('0')];

        this.completed = $selectedNodes && $selectedNodes.length !== 0;

        if (!this.completed) {
            this.removeAllAnswers();
            return Q.resolve();
        }

        let $selectedNode = $($selectedNodes.last());

        if (!this.answers.size()) {
            this.answer = this.addAnswer();
        }

        $selectedNode.data('state', selectedOption.value);
        $selectedNode.css(selectedOption.css);

        // jscs:disable requireArrowFunctions
        // eslint-disable-next-line func-names
        this.$svgPaths.filter(`[id!=${$selectedNode.attr('id')}]`).each(function () {
            let $el = $(this);
            $el.data('state', unselectedOption.value);
            $el.css(unselectedOption.css);
        });
        // jscs:enable requireArrowFunctions

        return this.respondHelper(this.answer, $selectedNode.attr('id'), this.completed);
    }

    /**
     * Handle a multi select from the user.  Toggle the clicked nodes response (1/0) without affecting other selected nodes.
     * @param {jQuery} [$selectedNodes=$()] jQuery lookup for selected node (called selectedNodes, since jQuery contains an array).
     * Defaults to empty jQuery selection.
     * @returns {Q.Promise<void>} promise resolving after respondHelper is finished.
     */
    handleMultiSelect ($selectedNodes = $()) {
        let responses = [],
            completed = false,
            options = this.model.get('options');

        if ($selectedNodes.length > 0) {
            $selectedNodes.each((index, item) => {
                let $selectedNode = $(item),
                    selectedOption = options[this.getNextOptionIndex($selectedNode.data('state'))];

                $selectedNode.data('state', selectedOption.value);
                $selectedNode.css(selectedOption.css);
            });
        }

        // jscs:disable requireArrowFunctions
        // eslint-disable-next-line func-names
        this.$svgPaths.each(function () {
            let state = $(this).data('state');
            responses.push($(this).data('state').toString());
            completed = completed || (state > 0);
        });
        // jscs:enable requireArrowFunctions

        if (completed && this.skipped) {
            this.skipped = false;
        }

        let answerArray = [];
        this.answers.each((answer, index) => {
            answerArray.push(this.respondHelper(answer, responses[index], completed));
        });
        return Q.allSettled(answerArray);
    }

    /**
     * Respond to a click event by forwarding to "handleSingleSelect" or "handleMultiSelect".
     * @param {Event} e event object from click event.
     * @returns {Q.Promise<void>} promise resolving after respond is finished.
     */
    respond (e) {
        let $selectedNodes = $(e.target),
            singleSelect = this.model.get('singleSelect');

        return singleSelect ? this.handleSingleSelect($selectedNodes) : this.handleMultiSelect($selectedNodes);
    }

    /**
     * Handler to tear down this widget.
     */
    destroy () {
        $('#questionnaire').removeClass('ImageWidget');
    }
}

window.LF.Widget.ImageWidget = ImageWidget;

export default ImageWidget;
