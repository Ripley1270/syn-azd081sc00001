/**
 * Widget Validation Function for a Edit Patient Id Textbox Widget
 * @param {Object} answer the answer object from the widget
 * @param {Object} params the validation object form the widget model
 * @param {Function} callback The callback function to called upon completion.
 */
import NotifyView from 'core/views/NotifyView';

/**
 * @param {*} answer The answer
 * @param {*} params The params
 * @param {*} callback The function callback
 */
export function checkEditPatientID (answer, params, callback) {
    let response = answer.get('response').replace('*', ''),
        namespace = this.questionnaire.id,
        errorPopup = (errorString) => {
            let notifyStrings = {};

            notifyStrings.header = {
                key: errorString.header || 'VALIDATION_HEADER',
                namespace
            };

            notifyStrings.body = {
                key: errorString.errString,
                namespace
            };

            notifyStrings.ok = {
                key: errorString.ok || 'VALIDATION_OK',
                namespace
            };

            LF.getStrings(notifyStrings)
            .then((strings) => {
                (new NotifyView()).show(strings);
            });

            $(`#${answer.attributes.question_id}`).find('input').addClass('ui-state-error-border');
        };

    LF.Collection.Subjects.fetchCollection()
    .then((subjects) => {
        let subjectExists = subjects.find(user => user.get('subject_id') === response),
            identicalId = this.parent.parent.subject.get('subject_id') === response;
        if (!LF.Widget.ValidationFunctions.checkPatientRange(response)) {
            errorPopup(params.errorStrings.isInRange);
            callback(false);
        } else {
            (subjectExists && !identicalId) && errorPopup(params.errorStrings);
            callback(!subjectExists || identicalId);
        }
    });
}

LF.Widget.ValidationFunctions.checkEditPatientID = checkEditPatientID;

export default checkEditPatientID;
