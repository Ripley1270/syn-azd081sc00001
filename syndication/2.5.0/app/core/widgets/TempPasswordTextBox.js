import AddUserTextBox from './AddUserTextBox';
import { isValidPassword } from '../utilities';
import { makeWindowsNumericInputCover, getNested } from '../utilities/coreUtilities';

const defaultInput = 'DEFAULT:LogpadPasswordTextBox',
    keyCodeLF = 10,
    keyCodeCR = 13;

/**
  * Defines a text box for entering user information.
  */
export default class TempPasswordTextBox extends AddUserTextBox {
    /**
     * Get default input template
     * @type {string}
     */
    get input () {
        return defaultInput;
    }

    /**
     * Constructor
     * @param {Object} options The options for the widget.
     */
    constructor (options) {
        super(options);
        this.events = {
            'input input': 'keyPressed',
            'focus input': 'onFocus'
        };
        this.hasError = false;
        this.isValid = true;
    }

    /**
     *  Responsible for displaying the widget
     * @returns {Q.Promise<void>}
     */
    render () {
        let templates = this.model.get('templates') || {},
            textBox = templates.input || this.input,
            toTranslate = {};

        this.$el.empty();

        // Optionally populate the toTranslate object.
        this.model.get('label') && (toTranslate.label = this.model.get('label'));
        this.model.get('placeholder') && (toTranslate.placeholder = this.model.get('placeholder'));

        return this.i18n(toTranslate)
        .then((strings) => {
            let wrapperElement,
                labelElement,
                textBoxElement;

            wrapperElement = this.renderTemplate(templates.wrapper || this.wrapper);

            // If a template label is configured, render it.
            if (strings.label) {
                labelElement = this.renderTemplate(templates.label || this.label, {
                    link: this.model.get('id'),
                    text: strings.label
                });
            }
            let inputType = this.model.get('getRoleInputType') || 'text',
                obfuscateInput = getNested('model.attributes.configuration.obfuscate', this);

            if (inputType === 'text' && obfuscateInput) {
                inputType = 'password';
            }
            textBoxElement = this.renderTemplate(textBox, {
                id: this.model.get('id'),
                placeholder: strings.placeholder || '',
                name: `${this.model.get('id')}-${this.uniqueKey}`,
                className: this.model.get('className'),
                maxLength: this.model.get('maxLength'),
                inputType
            });

            // Append the wrapper to the local DOM and find where to append other elements.
            let $wrapper = this.$el.append(wrapperElement).find('[data-container]');

            if (labelElement) {
                $wrapper.append(labelElement);
            }

            // Add the textBox to the wrapper, then add the wrapper to the widget's DOM.
            $wrapper.append(textBoxElement)
                .appendTo(this.$el);

            // Append the widget to the parent element and trigger a create event.
            this.$el.appendTo(this.parent.$el)
                .trigger('create');

            this.value = this.displayText;

            // If the widget has a disabled property, disable the input element.
            if (this.model.get('disabled')) {
                this.$(`#${this.model.get('id')}`).attr('disabled', 'disabled').attr('readonly', 'readonly');
            }

            this.delegateEvents();

            if (this.displayText.length > 0) {
                // Trigger the key-up event so that the answer is properly formatted:
                this.$(`#${this.model.get('id')}`).trigger('input');
            }

            if (LF.Wrapper.platform === 'windows' && inputType === 'number' && obfuscateInput) {
                makeWindowsNumericInputCover(this.model.get('id'), $wrapper);
            }
        });
    }

    /**
     * Responds to a key press event.
     * @param {Event} e Event data
     */
    keyPressed (e) {
        let keyCode = e.which || e.keyCode;

        if ((keyCode === keyCodeCR) || (keyCode === keyCodeLF)) {
            super.keyPressed(e);
        } else {
            let $target = $(e.target),
                passwordValue = $target.val(),
                nonDateboxInputs = $(':input').not('[data-role = "datebox"]'),
                confirmationTextBox = $(nonDateboxInputs[nonDateboxInputs.index($target) + 1]),
                confirmationValue = confirmationTextBox ? confirmationTextBox.val() : '',
                parent = $target.parent('.input-group'),
                roleWidget = this.questionnaire.questionViews
                    .filter(q => q.widget.model && q.widget.model.get('field') === 'role')[0].id,
                roleAnswer = this.questionnaire.queryAnswersByID(roleWidget)[0],
                roleValue = roleAnswer ? JSON.parse(roleAnswer.get('response')).role : '',
                rolePasswordFormat = roleValue ? LF.StudyDesign.roles
                    .find({ id: roleValue }).get('passwordFormat') : '',
                defaultPasswordFormat = LF.StudyDesign.defaultPasswordFormat ?
                    _.defaults(LF.StudyDesign.defaultPasswordFormat, LF.CoreSettings.defaultPasswordFormat) :
                    LF.CoreSettings.defaultPasswordFormat,
                passwordFormat = _.defaults(rolePasswordFormat, defaultPasswordFormat),
                isPasswordValid = isValidPassword(passwordValue, passwordFormat),
                // eslint-disable-next-line max-len
                icon = isPasswordValid ? '<span class="glyphicon glyphicon-ok-sign form-control-feedback" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-minus-sign form-control-feedback" aria-hidden="true"></span>',
                classesToAdd = isPasswordValid ? 'has-feedback has-success' : 'has-feedback has-error';

            if (parent.hasClass('has-feedback')) {
                $target.siblings('.form-control-feedback').remove();
                $target.removeClass('ui-state-error-border');
                $($target.parentsUntil('screen')).removeClass('ui-state-error-border');
                parent.removeClass('has-feedback has-success has-warning has-error');
            }

            this.hasError = !isPasswordValid;
            this.completed = isPasswordValid && !this.hasError;
            this.isValid = isPasswordValid || passwordValue === '';
            parent.addClass(classesToAdd).append(icon);

            if (confirmationValue !== '') {
                // eslint-disable-next-line max-len
                let confirmationWidget = this.questionnaire.questionViews[this.questionnaire.questionViews.indexOf(this.question) + 1].widget,
                    passInValue = {
                        keyCode: e.keyCode,
                        target: confirmationTextBox
                    };
                confirmationWidget.keyPressed(passInValue);
            }

            super.sanitize(e);
            parent = null;
        }
    }
}
window.LF.Widget.TempPasswordTextBox = TempPasswordTextBox;
