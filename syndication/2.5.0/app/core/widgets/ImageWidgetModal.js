import ImageWidget from './ImageWidget';
import MultipleChoiceMixin from './mixin/MultipleChoiceMixin';
import NumberSpinnerMixin from './mixin/NumberSpinnerMixin';
import Logger from 'core/Logger';
import { mix } from 'core/utilities/languageExtensions';

const logger = new Logger('ImageWidgetModal');

/**
 * This widget allows for creating a multi-select SVG image
 * (see {@link ImageWidget}) that also launches a modal dialog to allow a user
 * to select a value for that node.
 * Mixes with a modalLauncherMixin implementation.
 * @extends ImageWidget
 */
class ImageWidgetModal {
    // Disabling ESLint for the next line.  The parser seems to be a bit broken, and it thinks that
    // all my options in objects within arrays are duplicate params.

    // eslint-disable-next-line valid-jsdoc
    /**
     * Construct the image widget with a modal dialog
     * @param {Object} options options for this widget (see super class for option definitions).
     * @param {Object} options.model The model containing the widget configuration.
     * This represents the "widget" node in the study-design for this question.
     * @param {boolean} [options.model.singleSelect=false] Whether or not the control is single select.
     *              <ul><li>True will act as a radio-button (returning the node ID as the single answer).</li>
     *              <li>False will act like a checkbox.  Each node translates to an IT (by pre-pending "{QuestionID}_"),
     *                      and those all are entered as separate answers to the collection,
     *                      with responses of 1 or 0.</li></ul>
     * @param {Array} [options.model.options] array of options, usually defaulting to an array of 2...
     *      a selected (value 0) option and an unselected (value 1) option.
     *      This is also where CSS for different selected states is defined.
     *      Making this array larger than 2 items will do nothing in single select mode, but will
     *      make click events cycle through the available options in multi-select mode.
     * @param {string} [options.model.options[0].css.fill=#FFFFFF] Style fill color for an unselected node.
     * @param {string} [options.model.options[0].value=0] {dev-only} value for an unselected node.
     * @param {string} [options.model.options[1].css.fill=#0000FF] Style fill color for a selected node.
     * @param {string} [options.model.options[1].value=1] {dev-only} value for a selected node.
     * @param {string} [options.model.modalMixin=MultipleChoiceMixin] (MultipleChoiceMixin|NumberSpinnerMixin)
     * mixin class for modal input control.
     * @param {Object} [options.model.inputOptions] options for the modal widget
     * @param {string} [options.model.inputOptions.itemTemplate] {dev-only} item template for the input options
     * @param {string} [options.model.inputOptions.type] {dev-only} (MultipleChoiceInput|NumberSpinnerInput)
     * DEPRECATED (use modalMixin instead) type of modal input
     * @param {Array} [options.model.inputOptions.items] (MultipleChoiceInput ONLY)
     * Array of items to be used as options for this pushbutton
     * @param {string} [options.model.inputOptions.items[].text] (MultipleChoiceInput ONLY) text of the item
     * @param {string} [options.model.inputOptions.items[].value] (MultipleChoiceInput ONLY)
     * value of the item, typically starting with '0' to indicate an unselected node.
     * @param {number} [options.model.inputOptions.min] (NumberSpinnerInput ONLY)
     * minimum value of the spinner in the modal dialog
     * @param {number} [options.model.inputOptions.max] (NumberSpinnerInput ONLY)
     * maximum value of the spinner in the modal dialog
     * @param {boolean} [options.model.inputOptions.showButtons] {dev-only} (NumberSpinnerInput ONLY)
     * whether or not to show buttons (lets you override the default behavior for an individual spinner).
     * @param {string} [options.model.inputOptions.buttonLayout] {dev-only} (horizontal|vertical)
     * (NumberSpinnerInput ONLY) button layout for an input option
     * (lets you override the default behavior for an individual spinner).
     * @param {boolean} [options.model.showButtons=false] {dev-only} Whether or not to show buttons for each spinner,
     * if spinners are used.  Can be overridden per-spinner in inputOptions.
     * @param {string} [options.model.buttonLayout=vertical] {dev-only} (horizontal|vertical) Button layout for spinner,
     * if spinners are used (vertical: north/south or horizontal: east/west).
     * Can be overridden per-spinner in inputOptions.
     * @param {Object} [options.model.imageText] object of key-value pairs.
     * Keys are rect IDs in the SVG file, and the values of these are the lookups for text in the translated text area.
     * @param {Object} options.model.imageName {dev-only} Image name of the SVG inside the configured imageFolder.
     * @param {string} options.model.nodeIDRegex {dev-only} regular expression.
     * When this expression passes with a path ID in the SVG file, this indicates that it is selectable.
     * @param {string} [options.model.imageFolder=IMAGE_FOLDER] {dev-only} Image folder to be used by the config.
     * This key is looked up as translated text, and defaults to the standard image folder.
     * @param {Object} [options.model.templates] {dev-only} templates for this widget.
     * The only template to be set is the imageTemplate, which defaults to DEFAULT:ImageTemplate.
     * @param {Object} [options.model.labels={labelOne: '', labelTwo: '', upButtonText: '', downButtonText: ''}]
     * {dev-only} The label strings.  Used to replace the label placeholders in the modal dialog template.
     * Note:  upButtonText and downButtonText are applied to the up and down buttons in the spinnerInput objects.
     * The text can be overridden in inputOptions by supplying another
     * labels object there.  If left as default, upButtonText and downButtonText are "+" and "-" glyphs.
     */
    constructor (options) {
        // This constructor is a factory pulling in the proper mixins.
        let mixin = options.model.get('modalMixin');

        // remove modalMixin, so it is not processed by the attribute processor.
        options.model.unset('modalMixin');

        if (typeof mixin === 'string') {
            mixin = LF.Widget.Mixin[mixin];
        }

        // Temporary while inputOptions.type is deferred.  Remove later.
        let inputOptions = _.compact(_.flatten([options.model.get('inputOptions')]));
        _.each(inputOptions, (option) => {
            if (option.type) {
                // eslint-disable-next-line max-len
                logger.warn('Specifying "type" for inputOptions is deferred.  Please set "modalMixin" property of the widget instead (e.g. NumberSpinnerMixin)');
                switch (option.type) {
                    case 'NumberSpinnerInput':
                        mixin = NumberSpinnerMixin;
                        break;
                    case 'MultipleChoiceInput':
                        mixin = MultipleChoiceMixin;
                        break;
                    default:
                        break;
                }
            }
        });

        // End Temporary section.

        // Default to MultipleChoiceMixin
        /**
         * Mixed class with provided mixin
         */
        class MixedImageWidgetModal extends mix(ImageWidget)
            .with(mixin || MultipleChoiceMixin, this._imageWidgetModalHandlersMixin) {}
        return new MixedImageWidgetModal(options);
    }

    /**
     * Use a mixin method for instance classes for this class.
     * This is done because JS does not support multiple inheritance, so our
     * specific class extensions need to go here.
     * @param {class} superclass superclass for this mixin
     * @returns {class} resulting class after the mixin.
     */
    _imageWidgetModalHandlersMixin (superclass) {
        return class extends superclass {
            /**
             * Get the current value passed to modal control
             * @returns {string} value for modal
            */
            getValueForModal () {
                return this.$selectedNode ? this.$selectedNode.data('state') || '0' : '0';
            }

            /**
             * Render the control
             * @returns {Q.Promise<void>} promise resolving when render is complete.
             */
            render () {
                this.$selectedNode = null;
                return super.render()
                .then(() => {
                    return this.i18n(this.getModalTranslationsObject(),
                        () => null,
                        { namespace: this.getQuestion().getQuestionnaire().id }
                    );
                })
                .then((modalStrings) => {
                    return this.renderModal(modalStrings);
                })
                .then(() => {
                    this.delegateEvents();
                });
            }

            /**
             * Move value from modal dialog to whatever instance we have.
             * @param {boolean} [stopModal=false] whether or not to stop modal processing to propagate this value.
             * Should be true if done inside a close handler or processing an embedded spinner when exiting.
             * @returns {Q.Promise<void>} promise resolving when complete.
            */
            propagateModalValue (stopModal = false) {
                let options = this.model.get('options');

                return this.getModalValuesString(stopModal)
                .then((answer) => {
                    if (this.$selectedNode) {
                        let selectedOption = options[this.getOptionIndex(answer)];
                        this.$selectedNode.data('state', selectedOption.value);
                        this.$selectedNode.css(selectedOption.css);

                        this.$selectedNode = null;
                    }
                })
                .then(() => {
                    return this.handleMultiSelect();
                });
            }

            /**
             * Respond to a click event by launching the modal dialog.
             * @param {Event} e event object from click event.
             * @returns {Q.Promise<void>} promise resolving after respond is finished.
             */
            respond (e) {
                this.$selectedNode = $(e.target);
                return this.openDialog();
            }
        };
    }
}

window.LF.Widget.ImageWidgetModal = ImageWidgetModal;
export default ImageWidgetModal;
