import TickMarks from '../CommonProperties/TickMarks';
import { isLogPad } from 'core/utilities';

export default class EQ5DTickMarks extends TickMarks {
    constructor (vasModel) {
        super(vasModel);

        // setup the defaults for the EQ5D tick marks based on whether it's a tablet or hand-held
        this.frequency = 10;

        // FIXME: The core should not branch for modalities. Each modality should have their own extension.
        if (isLogPad()) {
            this.values.frequency = 50;
            this.displayMinorTicks = false;
        } else {
            this.values.frequency = 10;
            this.displayMinorTicks = true;
        }

        // if the configuration overrides the defaults, load them up
        if (vasModel !== undefined && vasModel.has('tickMarks')) {
            let tmpTickMarks = vasModel.get('tickMarks');

            if (tmpTickMarks.frequency !== undefined) {
                this.frequency = tmpTickMarks.frequency;
            }

            if (tmpTickMarks.displayMinorTicks !== undefined) {
                this.displayMinorTicks = tmpTickMarks.displayMinorTicks;
            }

            if (tmpTickMarks.values !== undefined && tmpTickMarks.values.frequency !== undefined) {
                this.values.frequency = tmpTickMarks.values.frequency;
            }
        }
    }
}
