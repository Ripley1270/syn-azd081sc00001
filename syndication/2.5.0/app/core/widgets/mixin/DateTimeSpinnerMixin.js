import BaseIntervalSpinnerMixin from './BaseIntervalSpinnerMixin';
import { mix } from 'core/utilities/languageExtensions';

const defaultModalTemplate = 'DEFAULT:DateTimeSpinnerModal',
    defaultDisplayFormat = 'LLL',
    defaultStorageFormat = 'DD MMM YYYY HH:mm:ss';

/**
 * A variation of the BaseIntervalSpinnerMixin that allows only date, defaulting formats and templates.
 * @interface
 * @param {class} superclass superclass with which to mix
 */
function DateTimeSpinnerMixin (superclass) {
    return class extends mix(superclass).with(BaseIntervalSpinnerMixin) {
        /**
         * default modal template
         * @type {string}
         */
        get defaultModalTemplate () {
            return defaultModalTemplate;
        }

        /**
         * Default display format for the date.
         * @type {string}
         */
        get defaultDisplayFormat () {
            return defaultDisplayFormat;
        }

        /**
         * Default storage format
         * @type {string}
         */
        get defaultStorageFormat () {
            return defaultStorageFormat;
        }

        /**
         * Verify all necessary containers exist.  Throw error if not.
         * @throws {Error} error if this control does not contain all date/time related input containers
         */
        validateUI () {
            if (this.yearIndex === null || this.monthIndex === null || this.dayIndex === null ||
                this.hourIndex === null || this.minuteIndex === null) {
                throw new Error('Invalid template for DateTimeSpinner widget. ' +
                'Expected day + month + year + hour + minute containers');
            }
        }
    };
}

window.LF.Widget.Mixin.DateTimeSpinnerMixin = DateTimeSpinnerMixin;
export default DateTimeSpinnerMixin;
