/* eslint-disable max-lines */
import Logger from 'core/Logger';

const logger = new Logger('ModalLauncherMixin'),
    defaultOkButtonText = 'MODAL_OK_TEXT',
    mouseDownEvents = ['MSPointerDown', 'mousedown', 'pointerdown', 'touchstart'],
    mouseUpEvents = ['MSPointerUp', 'mouseup', 'pointerup', 'touchend'];

/**
 * Mixin for allowing a view/object to launch a modal dialog.
 * Assumes that the superclass is a backbone View.
 * @param {Backbone.View} superclass super class to be extended with this mixin.
 * @returns {Object} resulting class, superclass extended with this mixin.
 */
function ModalLauncherMixin (superclass) {
    return class extends superclass {
        /**
         * default modal template
         * @type {string}
         * @abstract
         */
        get defaultModalTemplate () {
            throw new Error('Unimplemented defaultModalTemplate getter in a ModalLauncher implementation.');
        }

        /**
         * default input template
         * @type {string}
         */
        get defaultInputTemplate () {
            throw new Error('Unimplemented defaultInputTemplate getter in a ModalLauncher implementation.');
        }

        /**
         * Default text lookup for OK button.
         * @type {string}
         */
        get defaultOkButtonText () {
            return defaultOkButtonText;
        }

        /**
         * Getter for modal jQuery selector
         * @type {jQuery}
         */
        get $modal () {
            return this._$modal || null;
        }

        /**
         * Setter for modal jQuery selector
         * @param {jQuery} $value value to be set
         * @type {jQuery}
         */
        set $modal ($value) {
            this._$modal = $value;
        }

        /**
         * current modal value
         * @type {string}
         */
        get modalValue () {
            throw new Error('Must define a modalValue getter for retrieving current modal value');
        }

        /**
         * Method to transform a value to its storage format.
         * Base implementation does nothing, but can be overridden
         * if storage format must differ from the display
         * @param {string} val value to transform
         * @returns {string} value transformed to storage format
         */
        displayValueToStorageFormat (val) {
            return val;
        }

        /**
         * Method to transform a stored value to its display format (shown to user).
         * Base implementation does nothing, but can be overridden
         * if storage format must differ from the display
         * @param {string} val value to transform
         * @returns {string} value transformed to storage format
         */
        storageValueToDisplayFormat (val) {
            return val;
        }

        /**
         * Method to parse a value into an array for the individual inputs values.
         * i.e. splitting a string, date/time values, etc.
         * @abstract
         */
        getInputValuesArray () {
            throw new Error('Must define getInputValuesArray() in ModalLauncherMixin implementation');
        }

        /**
         * Method to get the entire value to pass to the modal dialog.
         * Will be split by something like getInputValuesArray
         */
        getValueForModal () {
            throw new Error('Must define getValueForModal() in ModalLauncherMixin implementation');
        }

        /**
         * Method to convert the form elements/widgets in the modal dialog
         *      into a string.
         * @abstract
         * @param {boolean} stop stop any processing on the modal and immediately get the value
         */
        getModalValuesString () {
            throw new Error('Unimplemented getModalValuesString() method in a ModalLauncher implementation');
        }

        /**
         * Method to inject modal inputs into the modal dialog.
         * Override with specific functionality if needed.
         * @abstract
         * @param {Object} strings key/value pairs of translated strings.
         */
        injectModalInputs () {
            throw new Error('injectModalInputs method is required for ModalLauncherMixin implemenation');
        }

        /**
         * Move value from modal dialog to whatever instance we have.
         * @abstract
         * @param {boolean} [stopModal=false] whether or not to stop modal processing to propagate this value.
         * Should be true if done inside a close handler or processing an embedded spinner when exiting.
         * @returns {Q.Promise<void>} promise resolving when complete.
        */
        propagateModalValue () {
            throw new Error('propagateModalValue must be implemented by mixer of ModalLauncherMixin class');
        }

        /**
         * Get array of spinner templates from the model.  Fill with defaults where undefined
         * @param {number} index index of our current spinner
         * @returns {string} template name for the spinner at this index.
         */
        getInputTemplate (index) {
            let templatesObj = this.model.get('templates') || {};

            // formerly called spinnerTemplates... now is just inputTemplates.
            if (_.isArray(templatesObj.spinnerTemplates)) {
                logger.warn('Use of spinnerTemplates is deprecated.  Please use "inputTemplates" instead.');
                templatesObj.inputTemplates = templatesObj.spinnerTemplates;
            }

            if (_.isArray(templatesObj.inputTemplates) && templatesObj.inputTemplates.length > index) {
                return templatesObj.inputTemplates[index];
            }
            return this.defaultInputTemplate;
        }

        /**
         * Overridden to have handler for modal dialog.  Doesn't work as a regular event
         * (in the events object) so create and destroy manually
         */
        delegateEvents () {
            super.delegateEvents();
            this.addCustomEvents();
        }

        /**
         * Overridden to also cancel custom events.
         */
        undelegateEvents () {
            super.undelegateEvents();
            this.removeCustomEvents();
        }

        /**
         * Add custom events that could not be set the Backbone way
         */
        addCustomEvents () {
            (super.addCustomEvents || $.noop).call(this);
            if (this.$modal) {
                this.dialogClosing = _.bind(this.dialogClosing, this);
                this.dialogHidden = _.bind(this.dialogHidden, this);
                this.modalMouseDown = _.bind(this.modalMouseDown, this);
                this.handleMouseUp = _.bind(this.handleMouseUp, this);
                this.modalValueChanged = _.bind(this.modalValueChanged, this);
                this.$modal.on('click', '[data-dismiss]', this.dialogClosing);
                this.$modal.on('hidden.bs.modal', this.dialogHidden);

                for (let i = 0; this.inputs && i < this.inputs.length; ++i) {
                    this.listenTo(this.inputs[i].model, 'change:value', this.modalValueChanged);
                }
            }

            if (this.$dialog) {
                _.each(mouseDownEvents, (eventName) => {
                    this.$dialog.on(eventName, this.modalMouseDown);
                });
            }

            if (this.$win) {
                _.each(mouseUpEvents, (eventName) => {
                    this.$win.on(eventName, this.handleMouseUp);
                });
            }
        }

        /**
         * Remove custom events that could not be set the Backbone way
         */
        removeCustomEvents () {
            if (this.$modal) {
                this.$modal.off('click', '[data-dismiss]', this.dialogClosing);
                this.$modal.off('hidden.bs.modal', this.dialogHidden);

                if (this.inputs) {
                    _.each(this.inputs, (input) => {
                        this.stopListening(input.model, 'change:value');
                    });
                }
            }

            if (this.$dialog) {
                _.each(mouseDownEvents, (eventName) => {
                    this.$dialog.off(eventName, this.modalMouseDown);
                });
            }

            if (this.$win) {
                _.each(mouseUpEvents, (eventName) => {
                    this.$win.off(eventName, this.handleMouseUp);
                });
            }
            (super.removeCustomEvents || $.noop).call(this);
        }

        /**
         * Render the modal dialog.
         * @param {Object} textStrings Object of text strings to translate.
         * @returns {Q.Promise<jQuery>} promise resolving when modal has been rendered.
         */
        renderModal (textStrings) {
            let modalTemplate = (this.model.get('templates') || {}).modal || this.defaultModalTemplate,

                // Default common strings for modal templates to blank
                strings = _.defaults(textStrings,
                    {
                        modalTitle: '&nbsp;',
                        okButtonText: '&nbsp;',
                        labelOne: '&nbsp;',
                        labelTwo: '&nbsp;'
                    });

            // Fill out blanks for labels that are defaulted
            _.each(this.model.get('labels') || this.defaultLabels, (value, key) => {
                if (value === '') {
                    strings[key] = '&nbsp;';
                }
            });

            // Iterate labels for each input, and create keys for them
            let inputOptions = _.compact(_.flatten([this.model.get('inputOptions')]));

            _.each(inputOptions, (inputOption, index) => {
                _.each(inputOption.labels || {}, (value, key) => {
                    if (value === '') {
                        strings[`input_${index}_${key}`] = '&nbsp;';
                    } else {
                        this.model.get('labels')[`input_${index}_${key}`] = value;
                    }
                });
            });

            this.inputs = [];

            return Q()
            .then(() => {
                this.$modal = $(this.renderTemplate(modalTemplate, strings));
                this.$dialog = this.$modal.find('.modal-dialog');

                // reference to window for global event handling (mouseup)
                this.$win = $(window);
                return this.injectModalInputs(strings);
            })
            .then(() => {
                this.getModalContainer().append(this.$modal.get(0));

                if (this.model.get('embedded')) {
                    return this.embedModal();
                }
                return Q();
            });
        }

        /**
         * Get the modal container
         * @returns {jQuery} jQuery collection containing the first modal container element, or <body> if there is none.
         */
        getModalContainer () {
            let ret = $('#modalContainer').first();
            if (ret.length === 0) {
                ret = $('body');
            }
            return ret;
        }

        /**
         * Clone our strings object.  Remove ones that are for other indexes,
         * and override model-level strings with index-specific ones, if they exist.
         * @param {Object} strings key/value pairs of strings.
         * Keys are formatted as input_${index}_key for labels defined in the actual input options
         * (which are more specific and will override the ones defined in "labels" in the root)
         * @param {number} index index of the control for which to set strings
         * @returns {Object} new string object, set up to pass into our index
         */
        getStringsForInputIndex (strings, index) {
            let cloneStrings = _.clone(strings);
            _.each(strings, (val, key) => {
                if (/^input_/.test(key)) {
                    let keyParts = key.split('_'),
                        keyPartIndexIndex = 1,
                        keyPartNameIndex = 2;

                    if (keyParts[keyPartIndexIndex] === index.toString()) {
                        cloneStrings[keyParts[keyPartNameIndex]] = val;
                    }
                    delete cloneStrings[key];
                }
            });
            return cloneStrings;
        }

        /**
         * Refresh the spinners' UI and set them to our current values (from parsing the textbox).
         * @returns {Q.Promise<void>} promise resolving when all spinners are refreshed with values set.
         */
        refreshInputs () {
            let valArray = this.getInputValuesArray(),
                setValuePromiseFactories = [];

            for (let i = 0; i < this.inputs.length; ++i) {
                setValuePromiseFactories.push(() => {
                    return this.inputs[i].show()
                    .then(() => {
                        logger.trace(`refreshInputs() -> Setting value to ${valArray[i]}`);
                        return this.inputs[i].setValue(valArray[i]);
                    })
                    .then(() => {
                        this.inputs[i].pushValue();
                    });
                });
            }

            // Order may matter here.  Set them in order just in case it does.
            return setValuePromiseFactories.reduce(Q.when, Q());
        }

        /**
         * Add reveal CSS class to modal content.
         * This prevents some of the artifacts from appearing before the entire content is ready.
         */
        revealModalContent () {
            this.$dialog.addClass('reveal');
        }

        /**
         * Dialog has been opened.
         * @abstract
         * @param {Event} e The event args
         * Overrideable hook for after the dialog is fully opened.
         * @returns {Q.Promise<void>}
         */
        dialogOpened (e) {
            let ret = Q()
                .then(() => {
                    return this.refreshInputs();
                }).then(() => {
                    return this.revealModalContent();
                });

            // If called directly from an event handler, close the promise,
            // and return undefined (value of ret.done())
            //  Otherwise return the promise to be handled by the caller.
            return e instanceof $.Event ? ret.done() : ret;
        }

        /**
         * Handler for when the dialog is hidden.  This can be because of an OK or a cancel.
         * Calls out to the onHidden handler for each input class.
         */
        dialogHidden () {
            return this.refreshInputs()
            .then(() => {
                _.each(this.inputs, (input) => {
                    _.bind(input.onHidden || $.noop, input)();
                });
            });
        }

        /**
         * Handle mouse down event anywhere on modal.  Add class to modal backdrop to remove pointer events.
         * This way sliding out of the modal and mousing up does not fire a click event on the backdrop,
         * causing a cancel.
         * @param {Event} e event handler, used to stop propagation.
         * @returns {boolean} false
         */
        modalMouseDown (e) {
            if (this.model.get('embedded')) {
                // keep event going
                return true;
            }

            if (this.$modal) {
                this.$modal.addClass('nocancel');
            }

            // if we are in the modal body, but not some other element, stop the event from bubbling.
            if ($(e.target).hasClass('modal-dialog')) {
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
            return true;
        }

        /**
         * Remove class added by modalMouseDown during any mouse up event.
         */
        handleMouseUp () {
            if (this.$modal) {
                this.$modal.removeClass('nocancel');
            }
        }

        /**
         * Get object containing strings that should be translated for the modal dialog.
         * By default gets an OK button... and also any text coming from any modal inputs "items" array.
         * @returns {Object} object containing key/value pairs of text to translate.
         */
        getModalTranslationsObject () {
            let toTranslate = {
                okButtonText: this.model.get('okButtonText') || this.defaultOkButtonText
            };

            this.model.has('modalTitle') && (toTranslate.modalTitle = this.model.get('modalTitle'));

            _.each(_.compact(_.flatten([this.model.get('inputOptions')])), (input) => {
                _.each(input.items || [], (item) => {
                    toTranslate[item.text] = item.text;
                });
            });

            _.each(this.model.get('labels') || this.defaultLabels, (value, key) => {
                if (value !== '') {
                    toTranslate[key] = value;
                }
            });

            return toTranslate;
        }

        /**
         * Launch the dialog.
         * @param {Event} e The event args
         * @returns {Q.Promise<void>} undefined if called from an event handler.
         * Otherwise, returns a promise, resolving when the dialog is fully opened.
         * After opened, this function will launch the afterDialogOpened hook.
         */
        openDialog (e) {
            // jscs:disable requireArrowFunctions
            // eslint-disable-next-line func-names
            this.$el.find('input[type=tel], input[type=text]').each(function () {
                this.blur();
            });
            // jscs:enable requireArrowFunctions

            let ret = new Q.Promise((resolve) => {
                /**
                 * Event handler for event where modal is shown.  Resolves outer promise.
                 */
                let isShown = () => {
                    this.$modal && this.$modal.off('shown.bs.modal', isShown);
                    resolve();
                };
                if (this.$modal) {
                    this.$modal.on('shown.bs.modal', isShown);
                    this.$modal.modal('show');
                } else {
                    resolve();
                }
            })
            .then(() => {
                return this.dialogOpened();
            });


            // If called directly from an event handler, close the promise,
            // and return undefined (value of ret.done())
            //  Otherwise return the promise to be handled by the caller.
            return e instanceof $.Event ? ret.done() : ret;
        }

        /**
         * Overrideable hook for when the dialog is closing.
         * @param {Event} e The event args
         * @returns {Q.Promise<void>} the promise if not coming from an event handler
         *      , or undefined if it is.
         */
        dialogClosing (e) {
            let chain = this.propagateModalValue(true);

            // If called directly from an event handler, close the promise,
            // and return undefined (value of ret.done())
            //  Otherwise return the promise to be handled by the caller.
            return e instanceof $.Event ? undefined : chain;
        }

        /**
         * Destroy and de-reference inputs
         */
        removeInputs () {
            _.each(this.inputs, (item) => {
                item.destroy();
            });
            this.inputs && this.inputs.splice(0, this.inputs.length);
        }

        /**
         * Remove and de-reference modal dialog.
         */
        removeModal () {
            if (this.$modal) {
                this.$modal.modal('hide');
                this.$modal.remove();
                this.$modal = null;
                this.$dialog = null;
            }
        }

        /**
         * Destroy the UI for this widget or view, and call to remove the modal.
         */
        destroy () {
            // If the parent view implements a destroy method, run it, otherwise do nothing.
            (super.destroy || $.noop).call(this);

            // May get called by widget's or parent view's destroy
            // but must be called by this view regardless, so call it here anyway
            this.undelegateEvents();
            this.removeInputs();
            this.removeModal();
            this.$win = null;
        }

        /**
         * Note, the following method has been copied from WidgetBase
         * so that the superclass can just be a View if necessary (not a widget)
         */
        /**
         * An alias for LF.templates.display. See {@link core/collections/Templates Templates} for details.
         * @param {string} name The name of the template to render.
         * @param {Object} options The options passed to the template.
         * @returns {string} The rendered template.
         */
        renderTemplate (name, options) {
            return LF.templates.display(name, options);
        }

        /**
         * Hide normal input fields and embed the modal control in the
         * ".embedded-control-container" within the widget template
         * @returns {Q.Promise<void>} promise resolving when process is complete.
         */
        embedModal () {
            this.$el.find('input[type=tel], input[type=text]').each((index, element) => {
                let $element = $(element);
                if ($element.attr('type') !== 'hidden') {
                    $element.data('oldType', $element.attr('type'));
                    $element.attr('type', 'hidden');
                }
            });
            this.$el.find('.embedded-control-container').append(this.$modal.find('.modal-body'));
            return this.refreshInputs()
            .then(() => {
                // Manually kick off process to set spinner limits,
                // scroll to correct location for value, and sync up text box.
                // Subsequent calls will come from an event handler.
                return this.modalValueChanged();
            });
        }

        /**
         * Event handler for when any input value has changed.
         * @returns {Q.Promise<void>} promise resolving when action is complete.
        */
        modalValueChanged () {
            if (!this.model.get('embedded')) {
                return Q();
            }

            return this.propagateModalValue(false);
        }

        /**
         * Process our value by retrieving our modal values strings.
         * @returns {Q.Promise<void>} promise resolving when completed.
        */
        processValue () {
            return Q().then(() => {
                if (this.model.get('embedded')) {
                    return this.propagateModalValue(true);
                }
                return Q();
            }).then(() => {
                // run super processValue
                // more specific functionality (above) is done first
                if (super.processValue) {
                    return super.processValue();
                }
                return Q();
            });
        }
    };
}

export default ModalLauncherMixin;
