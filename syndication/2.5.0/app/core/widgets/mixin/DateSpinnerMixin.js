import BaseIntervalSpinnerMixin from './BaseIntervalSpinnerMixin';
import { mix } from 'core/utilities/languageExtensions';

const defaultModalTemplate = 'DEFAULT:DateSpinnerModal',
    defaultDisplayFormat = 'LL',
    defaultStorageFormat = 'DD MMM YYYY';

/**
 * A variation of the BaseIntervalSpinnerMixin that allows only date, defaulting formats and templates.
 * @interface
 * @param {class} superclass superclass with which to mix
 */
function DateSpinnerMixin (superclass) {
    return class extends mix(superclass).with(BaseIntervalSpinnerMixin) {
        /**
         * default modal template
         * @type {string}
         */
        get defaultModalTemplate () {
            return defaultModalTemplate;
        }

        /**
         * default display format
         * @type {string}
         */
        get defaultDisplayFormat () {
            return defaultDisplayFormat;
        }

        /**
         * default storage format
         * @type {string}
         */
        get defaultStorageFormat () {
            return defaultStorageFormat;
        }

        /**
         * default initial date
         * For the date spinner, returns a floor of current date.
         * @type {Date}
         */
        get defaultInitialDate () {
            let dt = new Date();

            // Use floor of initial date.
            dt.setHours(0, 0, 0, 0);
            return dt;
        }

        /**
         * Verify all necessary containers exist.
         * For the date spinner, this means that no hour or minute containers are passed into the template.
         * @throws {Error} error if no year, month, or day containers, or if hour or minute containers exist.
         */
        validateUI () {
            if (this.yearIndex === null && this.monthIndex === null && this.dayIndex === null) {
                throw new Error('Invalid template for DateSpinner widget. Expected day + month + year containers');
            }
            if (this.hourIndex !== null || this.minuteIndex !== null) {
                throw new Error('Invalid template for DateSpinner widget. Hour and minute containers must not exist. ' +
                    'Use TimeSpinner or DateTimeSpinner if you wish to include these.');
            }
        }
    };
}

window.LF.Widget.Mixin.DateSpinnerMixin = DateSpinnerMixin;
export default DateSpinnerMixin;
