/* eslint-disable max-lines */
import BaseSpinnerMixin from './BaseSpinnerMixin';
import ValueSpinnerInput from '../input/ValueSpinnerInput';
import { toStrictRTL } from 'core/utilities/localizationUtils';
import Logger from 'core/Logger';
import { mix, dateOrNull } from 'core/utilities/languageExtensions';

/* globals _ */

const logger = new Logger('IntervalSpinnerBase'),
    { Model } = Backbone,
    defaultInputTemplate = 'DEFAULT:NumberSpinnerControl',
    defaultSpinnerItemTemplate = 'DEFAULT:NumberItemTemplate',
    defaultClassName = 'DateSpinner',
    defaultStorageLocale = 'en-US',
    defaultNumberOfSpinnerItems = 2.5,
    defaultDateFormat = 'LL',
    defaultTimeFormat = 'LT',
    defaultDateTimeFormat = 'LLL',
    defaultDateYearRange = 30,
    hoursPerMeridian = 12,
    maxHoursInDay = 23,
    maxHoursInMeridian = 11,
    maxMinutesInHour = 59,
    maxSecondsInMinute = 59,
    numberOfMonths = 12,
    maxDaysInMonth = 31,

    // Date where each date part has a unique value.
    //  This is used to generate a test date that can be displayed, reordered for RTL,
    //  and then re-populated with the date pattern parts (e.g. DD, MMM, etc.)
    dateWithUniqueParts = '08 Nov 1970 23:24';

/**
 * Enum for flow options.
 * @enum {Number}
 */
// eslint-disable-next-line no-unused-vars
const FlowOptions = {
    /**
     * <b>1.</b> Will use LTR if the user's language is LTR.
     * In RTL, will evaluate the display of a sample date, inspect the order,
     * and mirror it, so that it will appear properly if BDO is forced to RTL.
     * Then all the separated DIVs will appear in the same order HTML would have ordered them.
     */
    calculate: 1,

    /**
     * <b>2.</b> Align spinners and separators in the flow of the document.
     * Note, this will <b>NOT</b> always be the same as when printed in HTML or in a textbox,
     * especially when RTL and LTR characters are mixed.  For an exact representation of printed value,
     * use <i>calculate</i>
     */
    natural: 2,

    /**
     * <b>3.</b> Force an RTL flow of the flexbox elements.
     */
    rtl: 3,

    /**
     * <b>4.</b> Force an LTR flow of the flexbox elements.
     */
    ltr: 4
};

/**
 * A variation of the spinner control that allows for a date and time spinner.
 * Only difference in spinners for "date", "date-time" and "time" are the Modal Templates
 * @interface
 * @param {class} superclass superclass with which to mix
 */
function BaseIntervalSpinnerMixin (superclass) {
    return class extends mix(superclass).with(BaseSpinnerMixin) {
        /**
         * default modal template
         * @type {string}
         */
        get defaultModalTemplate () {
            throw new Error('Invalid Interval Spinner Implementation.  A Modal Template is required');
        }

        /**
         * default display template
         * @type {string}
         */
        get defaultDisplayFormat () {
            throw new Error('Invalid Interval Spinner Implementation.  A Display Format is required');
        }

        /**
         * default storage template
         * @type {string}
         */
        get defaultStorageFormat () {
            throw new Error('Invalid Interval Spinner Implementation.  A Storage Format is required');
        }

        /**
         * default date format
         * @type {string}
         */
        get defaultDateFormat () {
            return defaultDateFormat;
        }

        /**
         * default time format
         * @type {string}
         */
        get defaultTimeFormat () {
            return defaultTimeFormat;
        }

        /**
         * default datetime format
         * @type {string}
         */
        get defaultDateTimeFormat () {
            return defaultDateTimeFormat;
        }

        /**
         * default storage locale
         * @type {string}
         */
        get defaultStorageLocale () {
            return defaultStorageLocale;
        }

        /**
         * default spinner template
         * @type {string}
         */
        get defaultInputTemplate () {
            return defaultInputTemplate;
        }

        /**
         * default class name
         * @type {string}
         */
        get defaultClassName () {
            return defaultClassName;
        }

        /**
         * default spinner item template
         * @type {string}
         */
        get defaultSpinnerItemTemplate () {
            return defaultSpinnerItemTemplate;
        }

        /**
         * default year
         * @type {number}
         */
        get defaultYear () {
            return this.model.get('initialDate').getFullYear();
        }

        /**
         * default month
         * @type {number}
         */
        get defaultMonth () {
            return this.model.get('initialDate').getMonth();
        }

        /**
         * default day
         * @type {number}
         */
        get defaultDay () {
            return this.model.get('initialDate').getDate();
        }

        /**
         * default hour
         * @type {number}
         */
        get defaultHour () {
            return this.model.get('initialDate').getHours();
        }

        /**
         * default minute
         * @type {number}
         */
        get defaultMinute () {
            return this.model.get('initialDate').getMinutes();
        }

        /**
         * default initial date
         * @type {Date}
         */
        get defaultInitialDate () {
            return this.constructedDate;
        }

        /**
         * default min date
         * @type {Date}
         */
        get defaultMinDate () {
            let tempDt = this.constructedDate;
            return new Date(tempDt.setFullYear(tempDt.getFullYear() - defaultDateYearRange));
        }

        /**
         * default max date
         * @type {Date}
         */
        get defaultMaxDate () {
            let tempDt = this.constructedDate;
            return new Date(tempDt.setFullYear(tempDt.getFullYear() + defaultDateYearRange));
        }

        /**
         * current year value
         * @type {number}
         */
        get year () {
            let ret = null;
            if (typeof this.yearIndex === 'number') {
                ret = this.inputs[this.yearIndex].value;
            }
            ret = parseInt(ret, 10);
            return isNaN(ret) === false ? ret : this.defaultYear;
        }

        /**
         * current month value
         * @type {number}
         */
        get month () {
            let ret = null;
            if (typeof this.monthIndex === 'number') {
                ret = this.inputs[this.monthIndex].value;
            }
            ret = parseInt(ret - 1, 10);

            // ret becomes -1 when null.  So check for -1 instead of NaN.
            return ret !== -1 || ret === undefined ? ret : this.defaultMonth;
        }

        /**
         * current day value
         * @type {number}
         */
        get day () {
            let ret = null;
            if (typeof this.dayIndex === 'number') {
                ret = this.inputs[this.dayIndex].value;
            }
            ret = parseInt(ret, 10);
            return isNaN(ret) === false ? ret : this.defaultDay;
        }

        /**
         * current hour value
         * @type {number}
         */
        get hour () {
            let ret = null;
            if (typeof this.hourIndex === 'number') {
                ret = this.inputs[this.hourIndex].value;
            }
            ret = parseInt(ret, 10);
            if (typeof this.meridianIndicatorIndex === 'number') {
                ret += hoursPerMeridian * parseInt(this.inputs[this.meridianIndicatorIndex].value, 10);
            }
            return isNaN(ret) === false ? ret : this.defaultHour;
        }

        /**
         * current minute value
         * @type {number}
         */
        get minute () {
            let ret = null;
            if (typeof this.minuteIndex === 'number') {
                ret = this.inputs[this.minuteIndex].value;
            }
            ret = parseInt(ret, 10);
            return isNaN(ret) === false ? ret : this.defaultMinute;
        }

        /**
         * Input indexes in the order at which to evaluate them for testing.
         * @returns {Array} input indexes sorted from largest to smallest, for evaluating.
         */
        get inputEvaluateOrder () {
            let order = [];
            !_.isNull(this.yearIndex) && order.push(this.yearIndex);
            !_.isNull(this.monthIndex) && order.push(this.monthIndex);
            !_.isNull(this.dayIndex) && order.push(this.dayIndex);
            !_.isNull(this.meridianIndicatorIndex) && order.push(this.meridianIndicatorIndex);
            !_.isNull(this.hourIndex) && order.push(this.hourIndex);
            !_.isNull(this.minuteIndex) && order.push(this.minuteIndex);
            return order;
        }

        /**
         * Get the date part by its input index.
         * @param {number} index index to check against available date part indexes.
         * @returns {string|null} string representing date part (e.g. "month"), or null if it can't find any.
         */
        getDatePartByIndex (index) {
            switch (index) {
                case this.yearIndex:
                    return 'year';
                case this.monthIndex:
                    return 'month';
                case this.dayIndex:
                    return 'day';
                case this.hourIndex:
                    return 'hour';
                case this.minuteIndex:
                    return 'minute';
                case this.meridianIndicatorIndex:
                    return 'meridianIndicator';
                default:
                    return null;
            }
        }

        /**
         * getter for constructed date.
         * Creates a new Date() so that the object is cloned each time, and cannot be modified.
         * @type {Date}
         */
        get constructedDate () {
            return new Date(this._constructedDate);
        }

        /**
         * Verify that the UI is acceptable for this flavor of IntervalSpinner (i.e. date/time/datetime).
         * Returns nothing, but throws an error if the control is not configured correctly.
         * @throws {Error} error if control is not configured correctly
         * @abstract
         */
        validateUI () {
            throw new Error('IntervalSpinnerBase.validateUI() must be overridden by subclass');
        }

        /**
         * Get the storage value of the current date
         * @returns {string} value in display format.
        */
        get modalValue () {
            let dt = this.getDate(),
                storageFormat = this.model.get('storageFormat'),
                storageLocale = this.model.get('storageLocale');

            return moment(dt).locale(storageLocale).format(storageFormat);
        }

        /**
         * Construct the interval spinner
         * @param {Object} options options for this widget (see super class for option definitions).
         */
        constructor (options) {
            super(options);

            /**
             * Date that the widget was constructed.  This is used as the basis for default dates/times, etc.
             * so that they will be consistent regardless of when the getters are called.
             * @private
             * @type {Date}
             */
            this._constructedDate = new Date();

            this.yearIndex = null;
            this.monthIndex = null;
            this.dayIndex = null;
            this.hourIndex = null;
            this.minuteIndex = null;
            this.meridianIndicatorIndex = null;

            this.model.set('initialDate', dateOrNull(this.model.get('initialDate')) || this.defaultInitialDate);
            this.model.set('minDate', dateOrNull(this.model.get('minDate')) || this.defaultMinDate);
            this.model.set('maxDate', dateOrNull(this.model.get('maxDate')) || this.defaultMaxDate);

            this.model.set('displayFormat', this.model.get('displayFormat') || this.defaultDisplayFormat);
            this.model.set('storageFormat', this.model.get('storageFormat') || this.defaultStorageFormat);
            this.model.set('storageLocale', this.model.get('storageLocale') || this.defaultStorageLocale);

            this.model.set('dateFormat', this.model.get('dateFormat') || this.defaultDateFormat);
            this.model.set('timeFormat', this.model.get('timeFormat') || this.defaultTimeFormat);
            this.model.set('dateTimeFormat', this.model.get('dateTimeFormat') || this.defaultDateTimeFormat);

            let flow = this.model.get('flow');
            if (typeof flow === 'string') {
                flow = FlowOptions[flow.toLowerCase()];
            } else if (typeof flow !== 'number' && typeof flow !== 'undefined') {
                throw new Error('Flow is invalid.  Accepts only numbers and strings.');
            }

            this.model.set('flow', flow || FlowOptions.calculate);

            this.moment = moment();

            let localeInfo = [];
            if (LF.Preferred.language) {
                localeInfo.push(LF.Preferred.language);
            }
            if (LF.Preferred.locale) {
                localeInfo.push(LF.Preferred.locale);
            }

            this.moment.locale(localeInfo.join('-'));
        }

        /**
         * Transform value in displayFormat to storageFormat
         * @param {string} val value to transform
         * @returns {string} value in storageFormat
         */
        displayValueToStorageFormat (val) {
            let displayFormat = this.model.get('displayFormat'),
                displayLocale = this.moment.locale(),
                storageFormat = this.model.get('storageFormat'),
                storageLocale = this.model.get('storageLocale');

            if (val !== '') {
                return moment(val, displayFormat, displayLocale).locale(storageLocale).format(storageFormat);
            }
            return '';
        }

        /**
         * Transform value in displayFormat to storageFormat
         * @param {string} val value to transform
         * @returns {string} value in storageFormat
         */
        storageValueToDisplayFormat (val) {
            let displayFormat = this.model.get('displayFormat'),
                displayLocale = this.moment.locale(),
                storageFormat = this.model.get('storageFormat'),
                storageLocale = this.model.get('storageLocale');

            if (val !== '' && val !== null && val !== undefined) {
                return moment(val, storageFormat, storageLocale).locale(displayLocale).format(displayFormat);
            }
            return '';
        }

        /**
         * Get the array of input values from the current value to pushed forward.
         * @returns {Array<number>} the array to be passed along to our inputs.
         */
        getInputValuesArray () {
            let valArray = [],
                defaultVal = this.model.get('initialDate') || '',
                storageVal,
                val,
                storageFormat = this.model.get('storageFormat'),
                storageLocale = this.model.get('storageLocale');

            storageVal = this.getValueForModal();
            if (storageVal === '') {
                storageVal = moment(defaultVal).locale(storageLocale).format(storageFormat);
            }
            val = moment(storageVal, storageFormat, storageLocale).toDate();

            if (typeof this.yearIndex === 'number') {
                valArray[this.yearIndex] = val.getFullYear();
            }

            if (typeof this.monthIndex === 'number') {
                valArray[this.monthIndex] = val.getMonth() + 1;
            }

            if (typeof this.dayIndex === 'number') {
                valArray[this.dayIndex] = val.getDate();
            }

            if (typeof this.hourIndex === 'number') {
                valArray[this.hourIndex] = val.getHours();
            }

            if (typeof this.meridianIndicatorIndex === 'number') {
                if (valArray[this.hourIndex] >= hoursPerMeridian) {
                    valArray[this.hourIndex] = valArray[this.hourIndex] % hoursPerMeridian;
                    valArray[this.meridianIndicatorIndex] = 1;
                } else {
                    valArray[this.meridianIndicatorIndex] = 0;
                }
            }

            if (typeof this.minuteIndex === 'number') {
                valArray[this.minuteIndex] = val.getMinutes();
            }

            return valArray;
        }

        /**
         * Update divs containing meridian indicators with strings
         * that correspond to our time now and our time if we were
         * to adjust 12 hours
         */
        setMeridianDisplayValues () {
            let curTime = moment(this.getDate()),
                lowTime,
                highTime,
                $meridianContainer,
                meridianFormat;

            if (typeof this.meridianIndicatorIndex !== 'number') {
                return;
            }

            $meridianContainer = $(this.inputs[this.meridianIndicatorIndex].parent);

            meridianFormat = $meridianContainer.data('format') || 'A';

            if (curTime.hours() < hoursPerMeridian) {
                lowTime = curTime;
                highTime = moment(curTime).add(hoursPerMeridian, 'h');
            } else {
                highTime = curTime;
                lowTime = moment(curTime).add(-hoursPerMeridian, 'h');
            }

            $meridianContainer.find('.item').each((index, element) => {
                let $element = $(element);
                switch ($element.data('value')) {
                    case 0:
                        $element.html(lowTime.locale(this.moment.locale()).format(meridianFormat));
                        break;
                    case 1:
                        $element.html(highTime.locale(this.moment.locale()).format(meridianFormat));
                        break;

                    // No default
                }
            });
        }

        /**
         * Test all values in an index.
         * Currently just returns true for "year", since it is never conditionally filtered.
         * @param {number} index index of spinner to test
         * @returns {boolean} true if all values for this spinner meet criteria.  False otherwise.
         */
        testAllValuesForIndex (index) {
            return index === this.yearIndex;
        }

        /**
         * Test a particular value in an index.
         * @param {number} customIndex index to test
         * @param {number} customValue value to test
         * @returns {boolean} true if this index supports having this valid (is valid and is in range)
         */
        testValue (customIndex, customValue) {
            return this.testDate(this.getDatePartByIndex(customIndex), customValue);
        }

        /**
         * Test that the date is valid and within range with the custom date part set to a custom value.
         * @param {string} customProperty name of the date part
         * @param {number} customValue value to test
         * @returns {boolean} true if date is valid and is in range.
         */
        testDate (customProperty, customValue) {
            let testDt = this.getDate(customProperty, customValue),
                minDt = new Date(testDt),
                maxDt = new Date(testDt);

            // Determine min and max date/times for range checks
            // noinspection FallThroughInSwitchStatementJS
            switch (customProperty) {
                case 'month':
                    minDt.setDate(1);
                    maxDt.setMonth(maxDt.getMonth() + 1);
                    maxDt.setDate(0);
                // eslint-disable-next-line no-fallthrough
                case 'day':
                    minDt.setHours(0);
                    maxDt.setHours(maxHoursInDay);
                // eslint-disable-next-line no-fallthrough
                case 'meridianIndicator':
                    if (customProperty === 'meridianIndicator') {
                        if (customValue === 1) {
                            minDt.setHours(hoursPerMeridian);
                            maxDt.setHours(maxHoursInDay);
                        } else {
                            minDt.setHours(0);
                            maxDt.setHours(maxHoursInMeridian);
                        }
                    }
                // eslint-disable-next-line no-fallthrough
                case 'hour':
                    minDt.setMinutes(0);
                    maxDt.setMinutes(maxMinutesInHour);
                // eslint-disable-next-line no-fallthrough
                case 'minute':
                    minDt.setSeconds(0);
                    maxDt.setSeconds(maxSecondsInMinute);
                    break;

                // No default
            }

            // Months are not the same.  Day is past max day for the month.
            // Also check if our time is in the specified range.
            if (
                (
                    testDt.getMonth() !== this.month &&
                    customProperty === 'day'
                ) ||
                (
                    testDt.getHours() !== customValue &&
                    customProperty === 'hour'
                ) ||
                minDt.getTime() > (new Date(this.model.get('maxDate'))).getTime() ||
                maxDt.getTime() < (new Date(this.model.get('minDate'))).getTime()
            ) {
                return false;
            }
            return true;
        }

        /**
         * Get date from spinners.  Optionally allows a custom property to perform tests.  Short circuits after the
         * custom property is applied (for instance if the property is 'month', it will not set days, hours, or minutes
         * and will default them to 1, 0, and 0.
         * @param {string} customProperty Name of the date part.
         * @param {number} customValue The custom value to test
         * @returns {Date} date object with custom value set in the context of the current value
         */
        getDate (customProperty = null, customValue = null) {
            if (this.inputs.length === 0) {
                // Return null... no spinners yet, so no date.
                return null;
            }
            let m = moment('1970-01-01', null, 'en');

            m.year(customProperty === 'year' ? customValue : this.year);
            if (customProperty === 'year') {
                return m.toDate();
            }

            m.month(customProperty === 'month' ? customValue - 1 : this.month);
            if (customProperty === 'month') {
                return m.toDate();
            }

            m.date(customProperty === 'day' ? customValue : this.day);
            if (customProperty === 'day') {
                return m.toDate();
            }

            m.hour(customProperty === 'hour' ? customValue : this.hour);
            if (customProperty === 'hour') {
                return m.toDate();
            }

            m.minute(customProperty === 'minute' ? customValue : this.minute);

            return m.toDate();
        }

        /**
         * Get year items for the input control.  Array of values and formatted strings.
         * @param {string} format format for this date part.
         * @returns {Array<Object>} array of "value"/"text" items to be passed to the input control.
         */
        getYearItemsArray (format) {
            let minYear = (new Date(this.model.get('minDate'))).getFullYear(),
                maxYear = (new Date(this.model.get('maxDate'))).getFullYear(),
                items = [];

            for (let i = minYear; i <= maxYear; ++i) {
                let m = moment(`01 Jan ${i}`, 'DD MMM YYYY', 'en-US');
                m.locale(this.moment.locale());
                items.push({
                    value: i,
                    text: m.format(format)
                });
            }
            return items;
        }

        /**
         * Get month items for the input control.  Array of values and formatted strings.
         * @param {string} format format for this date part.
         * @returns {Array<Object>} array of "value"/"text" items to be passed to the input control.
         */
        getMonthItemsArray (format) {
            let items = [],
                m = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
            m.locale(this.moment.locale());

            for (let i = 0; i < numberOfMonths; ++i) {
                items.push({
                    value: i + 1,
                    text: m.format(format)
                });
                m.add(1, 'M');
            }
            return items;
        }

        /**
         * Get day items for the input control.  Array of values and formatted strings.
         * @param {string} format format for this date part.
         * @returns {Array<Object>} array of "value"/"text" items to be passed to the input control.
         */
        getDayItemsArray (format) {
            let items = [];
            let m = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
            m.locale(this.moment.locale());

            for (let i = 1; i <= maxDaysInMonth; ++i) {
                // pad only if necessary
                items.push({
                    value: i,
                    text: m.format(format)
                });
                m.add(1, 'd');
            }
            return items;
        }

        /**
         * Get hour items for the input control.  Array of values and formatted strings.
         * @param {string} format format for this date part.
         * @returns {Array<Object>} array of "value"/"text" items to be passed to the input control.
         */
        getHourItemsArray (format) {
            let items = [];
            let m = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
            m.locale(this.moment.locale());

            for (let i = 0; i <= maxHoursInDay; ++i) {
                // pad only if necessary
                items.push({
                    value: i,
                    text: m.format(format)
                });
                m.add(1, 'h');
            }
            return items;
        }

        /**
         * Get minute items for the input control.  Array of values and formatted strings.
         * @param {string} format format for this date part.
         * @returns {Array<Object>} array of "value"/"text" items to be passed to the input control.
         */
        getMinuteItemsArray (format) {
            let items = [];
            let m = moment('01 Jan 1970', 'DD MMM YYYY', 'en-US');
            m.locale(this.moment.locale());

            for (let i = 0; i <= maxMinutesInHour; ++i) {
                // pad only if necessary
                items.push({
                    value: i,
                    text: m.format(format)
                });
                m.add(1, 'm');
            }
            return items;
        }

        /**
         * Get minute items for the input control.  Array of values and formatted strings.
         * @param {string} format format for this date part.
         * @returns {Array<Object>} array of "value"/"text" items to be passed to the input control.
         */
        getMeridiemItemsArray () {
            // value changed handler will set text for the meridian displays based on exact time of day.
            // No need to do it here, just make an array of size 2
            return [{
                value: 0,
                text: ''
            }, {
                value: 1,
                text: ''
            }];
        }

        /**
         * Override set input limits to also change the meridian display.
         * @returns {Q.Promise<void>} promise resolving when input limits are set
         */
        setInputLimits () {
            return super.setInputLimits()
            .then(() => {
                return this.setMeridianDisplayValues();
            });
        }

        /**
         * Take a date format, execute any momentjs locale macros, and separate into usable date parts (and static text)
         * @param {string} format format compatible with momentjs
         * @returns {Array<string>} array with date parts and static text separated
         */
        parseDateParts (format) {
            let dateParts = [],
                i = 0,
                part;
            while (i < format.length) {
                let token = format.charAt(i),
                    acceptableChars = {};
                switch (token) {
                    case 'D':
                        // Allow 'Do' which is an ordinate day
                        acceptableChars.o = true;
                    // eslint-disable-next-line no-fallthrough
                    case 'M':
                    case 'Y':
                    case 'H':
                    case 'h':
                    case 'm':
                    case 'A':
                    case 'd':
                    case 'a':
                        acceptableChars[token] = true;
                        part = '';
                        while (acceptableChars[format.charAt(i)]) {
                            part += format.charAt(i);
                            ++i;
                        }
                        dateParts.push(part);

                        break;
                    case 'L':
                    case 'T':
                        part = '';
                        while (format.charAt(i) === 'L' || format.charAt(i) === 'T') {
                            part += format.charAt(i);
                            ++i;
                        }

                        dateParts = dateParts.concat(
                            this.parseDateParts(this.moment.localeData().longDateFormat(part))
                        );
                        break;
                    default: {
                        // Not a date or time formatter.  Make a node for all the extraneous chars (part of formatting)
                        let validDateChars = /[DMYHhmAa]/;
                        part = '';

                        while (!validDateChars.test(format.charAt(i)) && i < format.length) {
                            part += format.charAt(i);
                            ++i;
                        }
                        dateParts.push(part);
                        break;
                    }
                }
            }
            return dateParts;
        }

        /**
         * Generate a test date and force strict RTL on it.
         * Then take deconstructed date parts to reinsert formatted sections.
         * This will result in a new dateParts array that will exactly match the flow of the RTL language,
         * when placed into separate spans (with bdo dir set to RTL)
         * @param {Array<string>} dateParts array of date parts to process.
         * @returns {Array<string>} dateParts reordred (in RTL) to work with a strict RTL document flow.
         */
        datePartsWithDocumentFlow (dateParts) {
            if (LF.strings.getLanguageDirection() !== 'rtl' || this.model.get('flow') !== FlowOptions.calculate) {
                return dateParts;
            }

            let oldFormat = dateParts.join(''),
                displayLocale = this.moment.locale();

            let testMoment = moment(dateWithUniqueParts).locale(displayLocale),
                newFormat = toStrictRTL(testMoment.format(oldFormat));

            // Find where date parts are being used, and put them back.
            _.each(dateParts, (part) => {
                let formatted = testMoment.format(part);
                if (formatted !== part) {
                    newFormat = newFormat.replace(formatted, part);
                    newFormat = newFormat.replace(formatted.split('').reverse().join(''), part);
                }
            });

            // Now test date should be a new format string, forcing strict RTL for all characters
            return this.parseDateParts(newFormat);
        }

        /**
         * Find any jQuery containers for dates, times, and date-times.
         * Replace the contents with our custom formats (if they are currently empty).
        */
        buildDynamicTemplate () {
            this.$modal.find('.date-container, .time-container, .date-time-container').each((index, element) => {
                let $element = $(element);

                // eslint-disable-next-line max-len
                if ($element.children('.day-spinner-container, .month-spinner-container, .year-spinner-container, .hour-spinner-container, .minute-spinner-container, .meridian-indicator-spinner-container').length !== 0) {
                    // Date or time children inside here already.  Don't build a template, but do check other elements
                    return true;
                }

                let format = $element.data('format');
                if (!format && $element.hasClass('date-container')) {
                    format = this.model.get('dateFormat');
                } else if (!format && $element.hasClass('time-container')) {
                    format = this.model.get('timeFormat');
                } else if (!format && $element.hasClass('date-time-container')) {
                    format = this.model.get('dateTimeFormat');
                }

                // use "expanded" date format for "LL/LT", etc., defaulting to the literal passed in format.
                let dateparts = this.parseDateParts(format);
                dateparts = this.datePartsWithDocumentFlow(dateparts);

                for (let i = 0; i < dateparts.length; ++i) {
                    let datePart = dateparts[i];
                    switch (datePart.substring(0, 1)) {
                        case 'M':
                            $element.append(`<div class="month-spinner-container" data-format="${datePart}"></div>`);
                            break;
                        case 'D':
                            $element.append(`<div class="day-spinner-container" data-format="${datePart}"></div>`);
                            break;
                        case 'Y':
                            $element.append(`<div class="year-spinner-container" data-format="${datePart}"></div>`);
                            break;
                        case 'H':
                        case 'h':
                            $element.append(`<div class="hour-spinner-container" data-format="${datePart}"></div>`);
                            break;
                        case 'm':
                            $element.append(`<div class="minute-spinner-container" data-format="${datePart}"></div>`);
                            break;
                        case 'A':
                            // eslint-disable-next-line max-len
                            $element.append(`<div class="meridian-indicator-spinner-container" data-format="${datePart}"></div>`);
                            break;
                        default:
                            datePart = datePart.replace(/\s+/g, '&nbsp;');
                            datePart = datePart.replace(/[[\]]/g, '');
                            $element.append(`<div class="separator">${datePart}</div>`);
                            break;
                    }
                }

                // keep iterating loop
                return true;
            });
        }

        /**
         * Set the flow style class for the modal dialog
         */
        setModalFlow () {
            let className;
            switch (this.model.get('flow')) {
                case FlowOptions.calculate:
                    className = 'flow-calculate';
                    break;
                case FlowOptions.natural:
                    className = 'flow-natural';
                    break;
                case FlowOptions.ltr:
                    className = 'flow-ltr';
                    break;
                case FlowOptions.rtl:
                    className = 'flow-rtl';
                    break;
                default:
                    logger.error('Flow provided is not a member of the enumerable.  ' +
                    `Value provided was ${this.model.get('flow')}`);
            }
            this.$modal.find('.modal-horizontal-container').addClass(className);
            this.$el.find('.modal-horizontal-container').addClass(className);
        }

        /**
         * Place modal inputs into place
         * @param {Object} strings key/value pair of strings for input control to use
         */
        injectModalInputs (strings) {
            if (this.model.has('spinnerInputOptions')) {
                logger.warn('"spinnerInputOptions" is deprecated.  Use "inputOptions" instead.');
                this.model.set('inputOptions', this.model.get('spinnerInputOptions'));
            }

            let spinnerInputOptions = _.compact(_.flatten([this.model.get('inputOptions')]));

            this.buildDynamicTemplate();
            this.setModalFlow();

            let i = -1;
            this.$modal.find('.year-spinner-container').each((index, element) => {
                let spinnerTemplate,
                    spinnerItemTemplate,
                    $element = $(element);

                i++;
                this.yearIndex = i;
                spinnerTemplate = this.getInputTemplate(i);
                spinnerItemTemplate = this.getSpinnerItemTemplate(i);

                let items = this.getYearItemsArray($element.data('format') || 'YYYY');

                this.inputs[i] = new ValueSpinnerInput(
                    _.extend({
                        model: new Model(),
                        parent: element,
                        template: spinnerTemplate,
                        itemTemplate: spinnerItemTemplate,
                        numItems: defaultNumberOfSpinnerItems,
                        items,
                        showButtons: this.model.get('showButtons') || false,
                        buttonLayout: this.model.get('buttonLayout') || 'vertical',
                        strings: this.getStringsForInputIndex(strings, i)
                    }, spinnerInputOptions[i] || {}
                    )
                );
            });

            this.$modal.find('.month-spinner-container').each((index, element) => {
                let spinnerTemplate,
                    spinnerItemTemplate,
                    $element = $(element);

                i++;
                this.monthIndex = i;
                spinnerTemplate = this.getInputTemplate(i);
                spinnerItemTemplate = this.getSpinnerItemTemplate(i);

                let items = this.getMonthItemsArray($element.data('format') || 'MMM');

                this.inputs[i] = new ValueSpinnerInput(
                    _.extend({
                        model: new Model(),
                        parent: element,
                        template: spinnerTemplate,
                        itemTemplate: spinnerItemTemplate,
                        numItems: defaultNumberOfSpinnerItems,
                        items,
                        showButtons: this.model.get('showButtons') || false,
                        buttonLayout: this.model.get('buttonLayout') || 'vertical',
                        strings: this.getStringsForInputIndex(strings, i)
                    }, spinnerInputOptions[i] || {}
                    )
                );
            });

            this.$modal.find('.day-spinner-container').each((index, element) => {
                let spinnerTemplate,
                    spinnerItemTemplate,
                    $element = $(element);

                i++;
                this.dayIndex = i;
                spinnerTemplate = this.getInputTemplate(i);
                spinnerItemTemplate = this.getSpinnerItemTemplate(i);

                let items = this.getDayItemsArray($element.data('format') || 'DD');

                this.inputs[i] = new ValueSpinnerInput(
                    _.extend({
                        model: new Model(),
                        parent: element,
                        template: spinnerTemplate,
                        itemTemplate: spinnerItemTemplate,
                        numItems: defaultNumberOfSpinnerItems,
                        items,
                        showButtons: this.model.get('showButtons') || false,
                        buttonLayout: this.model.get('buttonLayout') || 'vertical',
                        strings: this.getStringsForInputIndex(strings, i)
                    }, spinnerInputOptions[i] || {}
                    )
                );
            });

            this.$modal.find('.hour-spinner-container').each((index, element) => {
                let spinnerTemplate,
                    spinnerItemTemplate,
                    $element = $(element);

                i++;
                this.hourIndex = i;
                spinnerTemplate = this.getInputTemplate(i);
                spinnerItemTemplate = this.getSpinnerItemTemplate(i);

                let items = this.getHourItemsArray($element.data('format') || 'hh');

                this.inputs[i] = new ValueSpinnerInput(
                    _.extend({
                        model: new Model(),
                        parent: element,
                        template: spinnerTemplate,
                        itemTemplate: spinnerItemTemplate,
                        numItems: defaultNumberOfSpinnerItems,
                        items,
                        showButtons: this.model.get('showButtons') || false,
                        buttonLayout: this.model.get('buttonLayout') || 'vertical',
                        strings: this.getStringsForInputIndex(strings, i)
                    }, spinnerInputOptions[i] || {}
                    )
                );
            });

            this.$modal.find('.minute-spinner-container').each((index, element) => {
                let spinnerTemplate,
                    spinnerItemTemplate,
                    $element = $(element);

                i++;
                this.minuteIndex = i;
                spinnerTemplate = this.getInputTemplate(i);
                spinnerItemTemplate = this.getSpinnerItemTemplate(i);

                let items = this.getMinuteItemsArray($element.data('format') || 'mm');

                this.inputs[i] = new ValueSpinnerInput(
                    _.extend({
                        model: new Model(),
                        parent: element,
                        template: spinnerTemplate,
                        itemTemplate: spinnerItemTemplate,
                        numItems: defaultNumberOfSpinnerItems,
                        items,
                        showButtons: this.model.get('showButtons') || false,
                        buttonLayout: this.model.get('buttonLayout') || 'vertical',
                        strings: this.getStringsForInputIndex(strings, i)
                    }, spinnerInputOptions[i] || {}
                    )
                );
            });

            this.$modal.find('.meridian-indicator-spinner-container').each((index, element) => {
                let spinnerTemplate,
                    spinnerItemTemplate;

                i++;
                this.meridianIndicatorIndex = i;
                spinnerTemplate = this.getInputTemplate(i);
                spinnerItemTemplate = this.getSpinnerItemTemplate(i);

                // Update hour spinner to be 12 hour now.
                let hourItems = this.inputs[this.hourIndex].model.get('items');
                hourItems.splice(hoursPerMeridian, hoursPerMeridian);
                this.inputs[this.hourIndex].model.set('items', hourItems);

                let items = this.getMeridiemItemsArray();

                this.inputs[i] = new ValueSpinnerInput(
                    _.extend({
                        model: new Model(),
                        parent: element,
                        template: spinnerTemplate,
                        itemTemplate: spinnerItemTemplate,
                        numItems: defaultNumberOfSpinnerItems,
                        items,
                        showButtons: this.model.get('showButtons') || false,
                        buttonLayout: this.model.get('buttonLayout') || 'vertical',
                        strings: this.getStringsForInputIndex(strings, i)
                    }, spinnerInputOptions[i] || {}
                    )
                );
            });

            this.validateUI();
        }
    };
}

// Exports
export { FlowOptions };
export default BaseIntervalSpinnerMixin;
