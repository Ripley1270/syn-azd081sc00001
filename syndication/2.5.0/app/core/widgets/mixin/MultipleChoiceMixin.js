import ModalLauncherMixin from './ModalLauncherMixin';
import { mix } from 'core/utilities/languageExtensions';
import MultipleChoiceInput from '../input/MultipleChoiceInput';

const defaultModalTemplate = 'DEFAULT:MultipleChoiceModal',
    defaultInputTemplate = 'DEFAULT:MultipleChoiceInputControl';

let { Model } = Backbone;

/**
 * Mixin for allowing a view/object to launch a modal dialog.
 * Assumes that the superclass is a backbone View.
 * @param {Backbone.View} superclass super class to be extended with this mixin.
 * @returns {Object} resulting class, superclass extended with this mixin.
 */
function MultipleChoiceMixin (superclass) {
    return class extends mix(superclass).with(ModalLauncherMixin) {
        /**
         * default modal template
         * @type {string}
         * @abstract
         */
        get defaultModalTemplate () {
            return defaultModalTemplate;
        }

        /**
         * default input template
         * @type {string}
         * @abstract
         */
        get defaultInputTemplate () {
            return defaultInputTemplate;
        }

        /**
         * Retrieves current modal value
         * @type {string}
         */
        get modalValue () {
            let valueArray = [];
            for (let i = 0; i < this.inputs.length; ++i) {
                valueArray.push(this.inputs[i].itemDisplayValueFunction(this.inputs[i].value));
            }
            if (isNaN(parseFloat(_.first(valueArray)))) {
                return '';
            }

            return valueArray.reduce((x, y) => x.toString() + y.toString());
        }

        /**
         * Method to convert the form elements/widgets in the modal dialog into a string.
         * @abstract
         * @param {boolean} stop stop any processing on the modal and immediately get the value
         * @returns {Q.Promise<string>} promise resolving with a string of the final value of this modal.
         */
        getModalValuesString () {
            return Q()
            .then(() => {
                for (let i = 0; i < this.inputs.length; ++i) {
                    this.inputs[i].pushValue();
                }
            })
            .then(() => {
                return this.modalValue;
            });
        }

        /**
         * Method to inject modal inputs into the modal dialog.
         * @param {Object} strings key/value pairs of translated strings.
         */
        injectModalInputs (strings) {
            // Ensure inputOptions is an array
            let inputOptions = _.compact(_.flatten([this.model.get('inputOptions')]));

            this.$modal.find('.modal-input-container').each((i, elem) => {
                let template = this.getInputTemplate(i);
                this.inputs[i] = new MultipleChoiceInput(
                    _.extend({
                        model: new Model(),
                        parent: elem,
                        callerId: this.model.get('id'),
                        inputIndex: i,
                        strings: this.getStringsForInputIndex(strings, i),
                        template
                    }, inputOptions[i] || {})
                );
            });
        }

        /**
         * Get array of input values
         * @returns {Array} array of values for inputs
         */
        getInputValuesArray () {
            // No reason for this not to be an array of 1.
            // Nothing to split values on in a normal multiple choice control.
            return [this.getValueForModal()];
        }
    };
}

window.LF.Widget.Mixin.MultipleChoiceMixin = MultipleChoiceMixin;
export default MultipleChoiceMixin;
