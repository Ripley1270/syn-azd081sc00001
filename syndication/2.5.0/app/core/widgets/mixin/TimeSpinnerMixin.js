import BaseIntervalSpinnerMixin from './BaseIntervalSpinnerMixin';
import { mix } from 'core/utilities/languageExtensions';

const defaultModalTemplate = 'DEFAULT:TimeSpinnerModal',
    defaultDisplayFormat = 'LT',
    hoursPerMeridian = 12,
    defaultStorageFormat = 'HH:mm:ss';

/**
 * A variation of the BaseIntervalSpinnerMixin that allows only date, defaulting formats and templates.
 * @interface
 * @param {class} superclass superclass with which to mix
 */
function TimeSpinnerMixin (superclass) {
    return class extends mix(superclass).with(BaseIntervalSpinnerMixin) {
        /**
         * Override value getter.  Transform to storage value.
         * A little more complicated than the date and date/time one, because, as a time-only, this only works
         * if it is bound to a full date.  So bind to our default "base" date with a time.
         * Bind to the base date, and not "today", because an hour will be missing on the spring ahead DST date.
         * @returns {string} value parsed as the storage value.
         */
        get value () {
            let val;

            if (this.model && this.$(`#${this.model.get('id')}`).length > 0) {
                val = this.$(`#${this.model.get('id')}`).val();
            } else if (this.model) {
                val = this.model.get('defaultVal');
            } else {
                return null;
            }

            if (val === '') {
                return val;
            }

            let baseDtString,
                valMoment,
                storageFormat = this.model.get('storageFormat'),
                displayFormat = this.model.get('displayFormat'),
                displayLocale = this.moment.locale(),
                storageLocale = this.model.get('storageLocale');

            baseDtString = moment(this.model.get('defaultVal')).locale(displayLocale).format('YYYY-MM-DD');
            valMoment = moment(`${baseDtString} ${val}`, `YYYY-MM-DD ${displayFormat}`, displayLocale);
            return valMoment.locale(storageLocale).format(storageFormat);
        }

        /**
         * Override value getter.  Transform to display value.
         * A little more complicated than the date and date/time one, because, as a time-only, this only works
         * if it is bound to a full date.
         * Bind to the base date, and not "today", because an hour will be missing on the spring ahead DST date.
         * @param {string} val The value.
         */
        set value (val) {
            let formattedVal = val;

            if (val !== '') {
                let baseDtString,
                    valMoment,
                    storageFormat = this.model.get('storageFormat'),
                    displayFormat = this.model.get('displayFormat'),
                    displayLocale = this.moment.locale(),
                    storageLocale = this.model.get('storageLocale');

                baseDtString = moment(this.model.get('defaultVal')).locale(storageLocale).format('YYYY-MM-DD');
                valMoment = moment(`${baseDtString} ${val}`, `YYYY-MM-DD ${storageFormat}`, storageLocale);
                formattedVal = valMoment.locale(displayLocale).format(displayFormat);
            }

            this.$(`#${this.model.get('id')}`).val(formattedVal);
        }

        /**
         * Override spinner values array.  Just a time is not a valid date.
         * It needs to go along with the date... use our base date for this.
         * @returns {Array}
         */
        getSpinnerValuesArray () {
            let valArray = [],
                defaultVal = this.model.get('defaultVal'),
                storageFormat = this.model.get('storageFormat'),
                storageLocale = this.model.get('storageLocale'),
                timeVal,
                baseDt,
                dtVal;

            timeVal = this.value;
            if (timeVal === '' || timeVal === undefined) {
                timeVal = moment(defaultVal, null, storageLocale).format(storageFormat);
            }

            baseDt = moment(this.model.get('defaultVal'), null, storageLocale).format('YYYY-MM-DD');
            dtVal = moment(`${baseDt} ${timeVal}`, `YYYY-MM-DD ${storageFormat}`, storageLocale).toDate();

            if (typeof this.hourIndex === 'number') {
                valArray[this.hourIndex] = dtVal.getHours();
            }

            if (typeof this.meridianIndicatorIndex === 'number') {
                if (valArray[this.hourIndex] >= hoursPerMeridian) {
                    valArray[this.hourIndex] = valArray[this.hourIndex] % hoursPerMeridian;
                    valArray[this.meridianIndicatorIndex] = 1;
                } else {
                    valArray[this.meridianIndicatorIndex] = 0;
                }
            }

            if (typeof this.minuteIndex === 'number') {
                valArray[this.minuteIndex] = dtVal.getMinutes();
            }

            return valArray;
        }

        /**
         * default modal template
         * @returns {string}
         */
        get defaultModalTemplate () {
            return defaultModalTemplate;
        }

        /**
         * default display format
         * @returns {string}
         */
        get defaultDisplayFormat () {
            return defaultDisplayFormat;
        }

        /**
         * default storage format
         * @returns {string}
         */
        get defaultStorageFormat () {
            return defaultStorageFormat;
        }

        /**
         * default min date
         * @returns {Date}
         */
        get defaultMinDate () {
            return new Date('01 Jan 1970 00:00');
        }

        /**
         * default max date
         * @returns {Date}
         */
        get defaultMaxDate () {
            return new Date('01 Jan 1970 23:59:59');
        }

        /**
         * default initial date
         * @returns {Date}
         */
        get defaultInitialDate () {
            let dt = new Date('01 Jan 1970 00:00'),
                curDt = new Date();
            dt.setHours(curDt.getHours());
            dt.setMinutes(curDt.getMinutes());
            return dt;
        }

        /**
         * Verify all necessary containers exist.
         */
        validateUI () {
            if (this.hourIndex === null || this.minuteIndex === null) {
                throw new Error('Invalid template for TimeSpinner widget. Expected hour & minute containers');
            }
            if (this.yearIndex !== null || this.monthIndex !== null || this.dayIndex !== null) {
                throw new Error('Invalid template for TimeSpinner widget. ' +
                    'Day, month, and year containers must not exist. ' +
                    'Use DateSpinner or DateTimeSpinner if you wish to include these.');
            }
        }
    };
}

window.LF.Widget.Mixin.TimeSpinnerMixin = TimeSpinnerMixin;
export default TimeSpinnerMixin;
