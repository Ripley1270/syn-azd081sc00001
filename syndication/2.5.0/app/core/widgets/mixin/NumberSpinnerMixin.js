import BaseSpinnerMixin from './BaseSpinnerMixin';
import NumberSpinnerInput from '../input/NumberSpinnerInput';
import { mix } from 'core/utilities/languageExtensions';
import Logger from 'core/Logger';

const logger = new Logger('NumberSpinnerMixin');

let { Model } = Backbone;

const defaultModalTemplate = 'DEFAULT:NumberSpinnerModal',
    defaultInputTemplate = 'DEFAULT:NumberSpinnerControl',
    defaultSpinnerItemTemplate = 'DEFAULT:NumberItemTemplate',
    defaultClassName = 'NumberSpinner';

/**
 * The NumberSpinner input control implements the BaseSpinner interface, to create a numeric control.
 * <ul>
 *      <li>NumberSpinnerInput is the input.</li>
 *      <li>Spinners are injected into containers with the 'number-spinner-container'
 *          CSS class in the modal template.</li>
 *      <li>Creating an array to send to spinners splits the string on '.'
 *          and sends an array of size 2 to a decimal spinner.</li>
 *      <li>Values are concatenated together when returning from the spinner.</li>
 * </ul>
 * <p><i>Note: 3 things passed in from study-design.js must be arrays in this widget.  They depend
 * on there being the same number of HTML elements with the class of 'number-spinner-container' in the modal template.
 * Each index of these arrays will apply to each spinner, respectively, based on the order they occur in the template.
 * <ul>
 *     <li>inputOptions</li>
 *     <li>templates.spinnerTemplates</li>
 *     <li>templates.spinnerItemTemplates</li>
 * </ul></i>
 * @param {function} superclass super-class for our mixin to extend.
 * @returns {function} mixin method.
 */
function NumberSpinnerMixin (superclass) {
    return class extends mix(superclass).with(BaseSpinnerMixin) {
        /**
         * default modal template
         * @returns {string}
         */
        get defaultModalTemplate () {
            return defaultModalTemplate;
        }

        /**
         * default spinner template
         * @returns {string}
         */
        get defaultInputTemplate () {
            return defaultInputTemplate;
        }

        /**
         * default spinner item template
         * @returns {string}
         */
        get defaultSpinnerItemTemplate () {
            return defaultSpinnerItemTemplate;
        }

        /**
         * default class name.
         */
        get defaultClassName () {
            return defaultClassName;
        }

        /**
         * Get display value string
         * @returns {string} value
        */
        get modalValue () {
            return this.getNumericValue().toString();
        }

        /**
         * Get the array of input values from the textbox entry.
         * @returns {Array<number>} the array to be passed along to our inputs.
         */
        getInputValuesArray () {
            let valArray,
                defaultVal = this.model.get('defaultVal'),
                val;

            val = this.getValueForModal();

            if (val === '' || val === undefined) {
                val = defaultVal !== undefined ? defaultVal.toString() : '';
            }

            valArray = val.split('.');

            if (valArray.length > 1) {
                valArray[1] = parseFloat(`.${valArray[1]}`);
            }

            return valArray;
        }

        /**
         * Gets the numeric value for these spinners.
         * Has optional params for custom indexes and values.
         * Those can be used to test the state of the spinners with
         * a different value selected (for instance, to evaluate overall min/max values for each spinner)
         * @param {Array<Object>} [customValues=[]] Custom values
         * @param {number} customValues[].index index of a custom value
         * @param {number} customValues[].value index of a custom value
         * @param {number} [customValue=null] The custom value
         * @returns {number} number representing all the values
         * of the numeric spinners in the modal dialog, concatenated.
         */
        getNumericValue (customValues = []) {
            let valueArray = [];
            if (this.inputs.length === 0) {
                // Return null... no inputs yet, so no value.
                return null;
            }

            for (let i = 0; i < this.inputs.length; ++i) {
                let val = this.inputs[i].itemDisplayValueFunction(this.inputs[i].value);
                if (val === null) {
                    // Escape.  Spinner is still working
                    return '';
                }
                valueArray.push(val);
            }
            _.each(customValues, (customValue) => {
                let index = customValue.index,
                    value = customValue.value;
                valueArray[index] = this.inputs[index].itemDisplayValueFunction(value);
            });

            if (isNaN(parseFloat(valueArray[0]))) {
                return '';
            }
            return parseFloat(valueArray.reduce((x, y) => x.toString() + y.toString()));
        }

        /**
         * Place inputs into place.  For the NumberSpinner implementation, they go into elements
         * with the CSS class 'number-spinner-container'.
         * @param {Object} strings key value pair of strings to use within control
         */
        injectModalInputs (strings) {
            if (this.model.get('spinnerInputOptions')) {
                logger.warn('"spinnerInputOptions" is deprecated.  Use "inputOptions" instead.');
                this.model.set('inputOptions', this.model.get('spinnerInputOptions'));
            }

            let foundSpinner = false,
                spinnerInputOptions = _.compact(_.flatten([this.model.get('inputOptions')]));

            this.$modal.find('.number-spinner-container').each((i, element) => {
                let spinnerTemplate = this.getInputTemplate(i),
                    spinnerItemTemplate = this.getSpinnerItemTemplate(i);

                // For Item, delay evaluate, so we can replace placeholders with actual values and display values

                foundSpinner = true;
                this.inputs[i] = new NumberSpinnerInput(
                    _.extend({
                        model: new Model(),
                        parent: element,
                        template: spinnerTemplate,
                        itemTemplate: spinnerItemTemplate,
                        showButtons: this.model.get('showButtons') || false,
                        buttonLayout: this.model.get('buttonLayout') || 'vertical',
                        strings: this.getStringsForInputIndex(strings, i)
                    }, spinnerInputOptions[i] || {})
                );
            });

            if (!foundSpinner) {
                throw new Error(`Invalid template for NumberSpinner widget.  Expected an element with
                    "number-spinner-container" class`);
            }
        }

        /**
         * Verify that possible numeric values with our custom properties
         * fall in range with our overall max and min.
         * @param {number} customIndex custom index to test
         * @param {number} customValue custom value to test for this index.
         * @returns {boolean} true if there are possible values with this indexes value, that fall within our max/min
        */
        testValue (customIndex, customValue) {
            let currentCustom = {
                index: customIndex,
                value: customValue
            };
            let customMinValues = [currentCustom],
                customMaxValues = [currentCustom];

            for (let i = customIndex + 1; i < this.inputs.length; ++i) {
                customMinValues.push({
                    index: i,
                    value: this.inputs[i].model.get('min')
                });
                customMaxValues.push({
                    index: i,
                    value: this.inputs[i].model.get('max')
                });
            }

            let minValue = this.getNumericValue(customMinValues),
                maxValue = this.getNumericValue(customMaxValues),
                minAllowed = parseFloat(this.model.get('min')),
                maxAllowed = parseFloat(this.model.get('max')),
                greaterThanMin =
                        isNaN(minAllowed) ||
                        maxValue >= minAllowed,

                lessThanMax =
                        isNaN(maxAllowed) ||
                        minValue <= maxAllowed
                        ;

            return greaterThanMin && lessThanMax;
        }

        /**
         * Test all values.  This is an optimization over testing individual values for each spinner.
         * If this returns true, no more tests will be required to hide items in this spinner.
         * @param {number} index index at which to test all values
         * @returns {boolean} whether or not all values in this spinner pass.
         */
        testAllValuesForIndex (index) {
            let spinner = this.inputs[index],
                min = spinner.itemValueFunction(spinner.model.get('min')),
                max = spinner.itemValueFunction(spinner.model.get('max'));

            return this.testValue(index, min) && this.testValue(index, max);
        }
    };
}

window.LF.Widget.Mixin.NumberSpinnerMixin = NumberSpinnerMixin;
export default NumberSpinnerMixin;
