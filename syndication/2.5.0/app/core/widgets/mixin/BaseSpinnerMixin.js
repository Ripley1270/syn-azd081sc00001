/* eslint-disable max-lines */
import ModalLauncherMixin from './ModalLauncherMixin';
import { mix } from 'core/utilities/languageExtensions';

const defaultClassName = 'Spinner',
    defaultLabels = { labelOne: '', labelTwo: '' },
    embeddedValueStopDelay = 350;

/**
 * BaseSpinner widget.  This is an interface for spinners.
 * It is essentially a textbox that launches a modal dialog (think "keyboard") with an
 * array of spinners.  It then calls a function to determine the value string from the array.<br /><br />
 * Note: 3 things passed in from study-design.js must be arrays in this widget.  They depend
 * on there being the same number of HTML elements in the modal template that have spinners injected into them
 * (specific implementations will specify how this is done).
 * Each index of these arrays will apply to each spinner, respectively, based on the order they occur in the template.
 * <ul>
 *     <li>inputOptions</li>
 *     <li>templates.spinnerTemplates</li>
 *     <li>templates.spinnerItemTemplates</li>
 * </ul>
 * @param {class} superclass superclass of this mixin.
 * @returns {class} superclass mixed with our class extension.
 */
function BaseSpinnerMixin (superclass) {
    return class extends mix(superclass).with(ModalLauncherMixin) {
        /**
         * default spinner item template
         * @type {string}
         */
        get defaultSpinnerItemTemplate () {
            throw new Error('Unimplemented defaultSpinnerItemTemplate getter in a BaseSpinner implementation.');
        }

        /**
         * Return indexes of inputs in order of evaluation
         * (during setLimits, etc. largest to smallest)
         * @type {Array<number>}
         */
        get inputEvaluateOrder () {
            // default implementation, return in ascending order.
            // Will work for number spinner and most applications.
            return _.keys(this.inputs).map(val => parseInt(val, 10));
        }

        /**
         * default class name for this widget, unless overridden by model.
         * @type {string}
         */
        get defaultClassName () {
            return defaultClassName;
        }

        /**
         * default labels for modal dialog (keys to be translated)
         * @type {Object}
         */
        get defaultLabels () {
            return defaultLabels;
        }

        /**
         * Modal value has changed.  This runs setInputLimits.
         * Then defers to super to finish publishing the value, if necessary.
         * @returns {Q.Promise<void>} promise resolving when complete
        */
        modalValueChanged () {
            return this.setInputLimits()
            .then(() => {
                super.modalValueChanged();
            });
        }

        /**
         * Get the string value for the text box from an array of spinner values.
         * @param {boolean} [stop=true] whether or not to stop spinning during value processing.
         * @returns {Promise<string>} promise returning a string to be used for our textbox.
         */
        getModalValuesString (stop = true) {
            stop && this.stopSpinners();

            return this.setInputLimits()
            .then(() => {
                return this.modalValue;
            });
        }

        /**
         * Pause spinners and push their values to the model.
         * Should be done anytime a modal is closed or we are processing our value (during "Next" button).
        */
        stopSpinners () {
            for (let i = 0; i < this.inputs.length; ++i) {
                this.inputs[i].stop();
                this.inputs[i].pushValue();
            }
        }

        /**
         * Override function from ModalLauncherMixin.
         * Perform normal value processing, but delay if embedded
         * and currently spinning when starting this process.
         * @override
         * @returns {Q.Promise<void>} promise resolving when completed.
        */
        processValue () {
            if (!this.model.get('embedded')) {
                // By default, do no special behavior.
                return super.processValue();
            }

            // For embedded control, compare initial value to final value.
            // Delay if different, to make it obvious to the user what they selected
            // before advancing.
            let initialValue = this.modalValue;
            return super.processValue()
            .then(() => {
                return initialValue !== this.modalValue ? Q.delay(embeddedValueStopDelay) : Q();
            });
        }

        /**
         * Refresh the inputs' UI and set them to our current values (from parsing the textbox).
         * @returns {Q.Promise<void>} promise resolving when all spinners are refreshed with values set.
         */
        refreshInputs () {
            // Allocate an inputlimits promise, to make the control skip limit validation.
            // This avoids race conditions while the control updates itself.
            // We will enforce input limits at the end just in case.
            this.inputLimitsPromise = Q();
            return super.refreshInputs()
            .then(() => {
                this.inputLimitsPromise = false;
                return this.setInputLimits();
            });
        }

        /**
         * Get array of spinner item templates from the model.  Fill with defaults where undefined
         * @param {number} index index of our current spinner
         * @returns {string} item template name for the spinner at this index.
         */
        getSpinnerItemTemplate (index) {
            let modelTemplates = (this.model.get('templates') || {}).spinnerItemTemplates || [];
            return modelTemplates[index] || this.defaultSpinnerItemTemplate;
        }

        /**
         * Figure out the max and min of each spinner, based on the current values of the other spinners
         * disable options we can't use
         * @returns {Q.Promise<void>} promise resolving when input limits are set
         */
        setInputLimits () {
            if (this.inputLimitsPromise || this.inputs.length === 0) {
                return this.inputLimitsPromise || Q();
            }

            for (let i = 0; i < this.inputs.length; ++i) {
                if (!this.inputs[i].isRendered || this.inputs[i].value === null) {
                    // Some input is in flux.  Wait for all of them to finish adjusting the limits.
                    return this.inputLimitsPromise || Q();
                }
            }

            let adjustInputPromiseFactories = [];

            // Start from largest value and work way to smallest.
            _.each(this.inputEvaluateOrder, (iNdx) => {
                adjustInputPromiseFactories.push(() => {
                    return this.adjustInput(iNdx);
                });
            });

            this.inputLimitsPromise = adjustInputPromiseFactories.reduce(Q.when, Q())
            .then(() => {
                this.inputLimitsPromise = false;
            });
            return this.inputLimitsPromise;
        }

        /**
         * Test all values in an index for optimization
         * (if there is a quick way to do so).
         * Optionally overridden by subclass.
         * @abstract
         * @param {number} index index of spinner to test
         * @returns {boolean} true if all values for this spinner meet criteria.  False otherwise.
         */
        testAllValuesForIndex () {
            return false;
        }

        /**
         * Test the overall value of this control if the index specified
         * contains a specific value.  If valid within our constraints, return true.
         * Optionally overridden by subclass.
         * @abstract
         * @param {number} customIndex index of spinner to test
         * @param {number|string} customValue value to test in the context of the spinner.
         * @returns {boolean} true if all values for this spinner meet criteria.  False otherwise.
         */
        testValue () {
            return true;
        }

        /**
         * Adjust the spinner at this index.  Find its allowable min/max values and hide ones out of range.
         * @param {number} index spinner index
         * @returns {Q.Promise<void>} promise resolving when adjustment is finished.
         */
        adjustInput (index) {
            let spinner = this.inputs[index],
                lastItemIndex = spinner.$items.length - 1,

                // first actual item (index 0 has an empty item)
                adjustSpinnerPromises = [],
                lastEventWasHide = true,
                oldValue = spinner.itemValueFunction(spinner.value),
                firstIndexValue = _.first(this.inputs).value,
                allRangeValuesValid = this.testAllValuesForIndex(index) && (firstIndexValue !== '' || index === 0),
                valueWhenFinished = null;

            if (allRangeValuesValid) {
                spinner.unHideAll();
                valueWhenFinished = oldValue;
            }

            // Don't do for loop if all range values are considered to be valid.
            if (!allRangeValuesValid) {
                /**
                 * Factory for hiding an item.
                 * @param {number|string} itemValue value of item to hide
                 * @returns {Q.Promise<void>} promise resolving when hide is finished.
                 */
                const hideItemFnFactory = itemValue => () => {
                        lastEventWasHide = true;
                        return this.inputs.length > 0 ?
                            spinner.hideItem(itemValue, false, false) :
                            Q();
                    },

                    /**
                     * Factory for unhiding an item.
                     * @param {number|string} itemValue value of item to show
                     * @returns {Q.Promise<void>} promise resolving when unhide is finished.
                     */
                    unHideItemFnFactory = itemValue => () => {
                        if (lastEventWasHide) {
                            // When switching from hiding to unhiding,
                            // check to make sure all our items are not display:none
                            // If they are, the scroll position will be off,
                            // and this always means we should scroll to the max visible value.
                            // (since we are iterating this list backwards,
                            // the user has previously selected something that is larger and out of range)
                            if (valueWhenFinished === null) {
                                valueWhenFinished = itemValue;
                            }
                            lastEventWasHide = false;
                        } else if (itemValue >= oldValue || oldValue === '') {
                            valueWhenFinished = itemValue;
                        }

                        return this.inputs.length > 0 ?
                            spinner.unHideItem(itemValue, false, false) :
                            Q();
                    };

                // Iterate backwards.  The reason for this is that unHideItem() will determine
                //      whether or not it needs to scroll based on the current scroll position.
                //      The current scroll position is altered by adding things that take up space,
                //      so new unhidden items need to be at a lower scroll position (0) to work.
                //      so that unHideItem knows that it should adjust this offset accordingly.
                for (let itemNdx = lastItemIndex; itemNdx >= 1; --itemNdx) {
                    let itemValue = spinner.itemValueFunction($(spinner.$items[itemNdx]).data('value')),
                        shouldHide = !this.testValue(index, itemValue) ||
                                        (firstIndexValue === '' && index !== 0);
                    if (shouldHide) {
                        adjustSpinnerPromises.push(hideItemFnFactory(itemValue));
                    } else {
                        adjustSpinnerPromises.push(unHideItemFnFactory(itemValue));
                    }
                }
            }


            if (firstIndexValue !== '') {
                spinner.hideItem('', false, false);
                if (valueWhenFinished === '') {
                    valueWhenFinished = spinner.itemValueFunction(spinner.model.get('min'));
                }
            }

            adjustSpinnerPromises.push(() => {
                if (oldValue === '' && _.first(this.inputs).value === '') {
                    valueWhenFinished = '';
                }
                return spinner.setValue(valueWhenFinished);
            });
            adjustSpinnerPromises.push(() => {
                spinner.pushValue();
                spinner.scroller.refresh();
                return Q();
            });

            return adjustSpinnerPromises.reduce(Q.when, Q());
        }

        /**
         * Destroy our class.
         * @param {boolean} [retainPromise=false] whether to return promise, instead of just calling it "done()"
         * Destroy is not typically asynchronous, so this one will run in a new promise thread and call done().
         * This is just there for testing purposes.
         * @returns {Q.Promise<void>|undefined} a promise, or undefined (if retainPromise is false)
        */
        destroy (retainPromise = false) {
            let promise = (this.inputLimitsPromise || Q())
            .then(() => {
                super.destroy();
            });
            if (retainPromise) {
                return promise;
            }
            promise.done();
            return undefined;
        }
    };
}

export default BaseSpinnerMixin;
