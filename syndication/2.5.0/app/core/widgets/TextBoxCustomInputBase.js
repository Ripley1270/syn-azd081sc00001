import TextBox from './TextBox';
import { mix } from 'core/utilities/languageExtensions';

const defaultWrapperTemplate = 'DEFAULT:FormGroup',
    defaultClassName = 'Modal',
    defaultLabelTemplate = 'DEFAULT:Label',
    defaultInputTemplate = 'DEFAULT:TextBoxWithEmbeddedControl',
    defaultLabels = {};

/**
 * Textbox that allows mixing with a custom input mixin.
 */
class TextBoxCustomInputBase {
    /**
     * A textbox that allows for a custom input type.
     * Mix with your favorite ModalLauncherMixin (i.e. NumberSpinnerMixin, DateSpinnerMixin) to specify the input type.
     * Input will launch in a modal dialog unless "embedded" is specified.
     * Then it is embedded in place of the then hidden form input.
     * @param {Object} options options for this textbox.  See options for each superclass for parameters.
     */
    constructor (options) {
        // This constructor is a factory pulling in the proper mixins.
        let mixin = options.model.get('modalMixin');

        // remove modalMixin, so it is not processed by the attribute processor.
        options.model.unset('modalMixin');

        if (typeof mixin === 'string') {
            mixin = LF.Widget.Mixin[mixin];
        }

        if (typeof mixin !== 'function') {
            throw new Error('Configuration Error:  TextBoxCustomInputBase requires a modalMixin property.');
        }

        /**
         * Mixed TextBox with our mixin object.
         */
        class MixedTextBoxCustomInputBase extends mix(TextBox).with(mixin, this._textBoxModalHandlersMixin) {}
        return new MixedTextBoxCustomInputBase(options);
    }

    /**
     * Use a mixin method for instance classes for this class.
     * This is done because JS does not support multiple inheritance, so our
     * specific class extensions need to go here.
     * This also helps control the order (these functions override the modalLauncher mixin versions)
     * @param {Class} superclass superclass for this mixin
     * @returns {Class} resulting class after the mixin.
     */
    _textBoxModalHandlersMixin (superclass) {
        return class extends superclass {
            /**
             * Use this constructor to add an openDialog event.
             * @param {Object} options options passed to the widget
             */
            constructor (options) {
                super(options);

                _.extend(this.events, {
                    'focus input[type=tel], input[type=text]': 'openDialog'
                });
            }

            /**
             * default class name for this widget, unless overridden by model.
             * @type {string}
             */
            get defaultClassName () {
                return defaultClassName;
            }

            /**
             * default labels for modal dialog (keys to be translated)
             * @type {Object}
             */
            get defaultLabels () {
                return defaultLabels;
            }

            /**
             * default template for the wrapper of this widget, unless overidden by model.
             * @type {string}
             */
            get defaultWrapperTemplate () {
                return defaultWrapperTemplate;
            }

            /**
             * default template for the label of this widget, unless overidden by model.
             * @type {string}
             */
            get defaultLabelTemplate () {
                return defaultLabelTemplate;
            }

            /**
             * default template for the input of this widget, unless overidden by model.
             * @type {string}
             */
            get input () {
                return defaultInputTemplate;
            }

            /**
             * Override value getter to change to storage format
             * when it is being pulled out to save.
             * @type {string}
             */
            get value () {
                return this.displayValueToStorageFormat(super.value);
            }

            /**
             * Defer value setter to super
             * Even though this is defined in TextBox, it must be redefined here
             * since we overrode the getter.
             * @param {string} val value to set
             * @type {string}
             */
            set value (val) {
                super.value = this.storageValueToDisplayFormat(val);
            }

            /**
             * Gets the text to display within the UI.  This property allows what is saved to be different than what is
             * displayed
             * @returns {string} the text to display
             */
            get displayText () {
                return this.answer ? this.answer.get('response') : null;
            }

            /**
             * Get the value to be split and pushed to the modal.
             * @returns {string} value for the modal.
            */
            getValueForModal () {
                return this.value;
            }

            /**
             * Render this textbox control with modal input.
             * @returns {Q.Promise<void>} promise resolving when render is complete.
             */
            render () {
                return super.render()
                .then(() => {
                    return this.i18n(this.getModalTranslationsObject(),
                        $.noop,
                        { namespace: this.getQuestion().getQuestionnaire().id }
                    );
                })
                .then((modalStrings) => {
                    return this.renderModal(modalStrings);
                })
                .then(() => {
                    this.delegateEvents();
                });
            }

            /**
             * Move value from modal dialog to whatever instance we have.
             * @param {boolean} [stopModal=false] whether or not to stop modal processing to propagate this value.
             * Should be true if done inside a close handler or processing an embedded spinner when exiting.
             * @returns {Q.Promise<void>} promise resolving when complete.
            */
            propagateModalValue (stopModal = false) {
                return this.getModalValuesString(stopModal)
                .then((answer) => {
                    this.value = answer;
                    this.$input.trigger('input');
                    return this.respond();
                });
            }
        };
    }
}

LF.Widget.TextBoxCustomInputBase = TextBoxCustomInputBase;
export default TextBoxCustomInputBase;
