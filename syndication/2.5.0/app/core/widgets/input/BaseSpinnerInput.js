/* eslint-disable max-lines */
import { createGUID } from 'core/utilities';
import BaseInput from './BaseInput';

/* global IScroll*/

// quarter second for an entire item height (i.e. 8th of a second for half item height)
const autoScrollMilliseconds = 250,
    repeatButtonDelay = 75,
    repeatButtonStartDelay = 750,
    upButtonTextDefault = '<span class="glyphicon glyphicon-plus"></span>',
    downButtonTextDefault = '<span class="glyphicon glyphicon-minus"></span>',
    defaultDeceleration = 0.0006,
    mouseDownEvents = ['MSPointerDown', 'mousedown', 'pointerdown', 'touchstart'],
    mouseUpEvents = ['MSPointerUp', 'mouseup', 'pointerup', 'touchend'],
    fewSpinnerItemsThreshold = 2,
    floatHalf = 2.0,
    floatOne = 1.0,
    intHalf = 2,
    keyCodeSpace = 32,
    keyCodeCR = 13,
    keyCodeZero = 0,

    // length - 2, because we want to skip the current event in the queue
    previousSpinnerEventIndex = 2,
    spinnerEventMin = 2;

/**
 * An input control that works as a spinner.
 * This is essentially a scrollable area that keeps track of a "value" property, which is the item that is in the
 * center of the area.
 *
 * This scrolls with momentum and also allows selecting an item by clicking it.
 */
export default class BaseSpinnerInput extends BaseInput {
    /**
     * Items cache.
     * @type {jQuery|null}
     */
    get $items () {
        if (this._$items === null && this.$itemContainer) {
            this._$items = this.$itemContainer.find('.item');

            // One-time cache of items to reverse lookup objects.
            this._$items.each((index, element) => {
                let val = this.itemValueFunction($(element).data('value'));
                this._indexValueMap[index] = val;
                this._valueIndexMap[val] = index;
            });
        }
        return this._$items;
    }

    /**
     * Whether or not a finger or mouse is currently down.  If so, cancel any end events.
     * NOTE:  Array is searched starting at second to last item, because this will be called during another event
     *              , and we want to ignore the ongoing event.
     * @type {boolean}
     */
    get mouseDown () {
        for (let i = this.spinnerEventQueue.length - previousSpinnerEventIndex; i >= 0; --i) {
            switch (this.spinnerEventQueue[i]) {
                case 'mousedown':
                    return true;
                case 'mouseup':
                    return false;

                // No default
            }
        }
        return false;
    }

    /**
     * Whether or not we are currently in a scroll event
     * NOTE:  Array is searched starting at second to last item, because this will be called during another event
     *              , and we want to ignore the ongoing event.
     * @type {boolean}
     */
    get inScroll () {
        for (let i = this.spinnerEventQueue.length - previousSpinnerEventIndex; i >= 0; --i) {
            switch (this.spinnerEventQueue[i]) {
                case 'scrollStart':
                    return true;
                case 'scrollEnd-success':
                    return false;

                // No default
            }
        }
        return false;
    }

    /**
     * Whether or not a scroll started from the user (if false during a scroll event, we are in an auto-scroll).
     * NOTE:  Array is searched starting at second to last item, because this will be called during another event
     *              , and we want to ignore the ongoing event.
     * @type {boolean}
     */
    get userInitiatedScroll () {
        // If no scroll events at all return false
        if (this.spinnerEventQueue.length === 0) {
            return false;
        }

        for (let i = this.spinnerEventQueue.length - previousSpinnerEventIndex; i >= 0; --i) {
            switch (this.spinnerEventQueue[i]) {
                case 'beforeScrollStart':
                    return true;
                case 'scrollEnd-success':
                    return false;

                // No default
            }
        }
        if (this.spinnerEventQueue.length < spinnerEventMin) {
            return true;
        }
        return false;
    }

    /**
     * Get previous spinner event.
     * NOTE:  The item returned is the second to last item, because this will be called during another event
     *              , and we want to ignore the ongoing event.
     * @type {string|null}
     */
    get previousSpinnerEvent () {
        if (this.spinnerEventQueue.length >= spinnerEventMin) {
            return this.spinnerEventQueue[this.spinnerEventQueue.length - previousSpinnerEventIndex];
        }
        return null;
    }

    /**
     * Construct the spinner input object.
     * @param {Object} options options for this spinner.
     * @param {Model} options.model Backbone model to bind attributes.
     * @param {Element} options.parent parent element for our input control.
     * @param {string} options.itemTemplate template for an individual item in our input control.  (Note: this is the
     * lookup key to for template resources.)
     * @param {string} options.template template for our entire input control.
     * @param {string} [options.deceleration] deceleration of our spinner.
     * @param {string} [options.autoScrollMilliseconds] number of milliseconds required to do an auto scroll (i.e. after
     * a click, or after scrolling approximately to the location of an item).
     * @param {string} [options.numItems] number of items to be displayed.  Passing a number here will change the
     * height of the item-container element to accomodate N number of elements to be displayed at a time.
     * Otherwise, the height remains whatever it was from CSS.
     * @param {boolean} [options.showButtons=false] whether or not to show buttons above spinners.
     * Note, this will also force "numItems" to be 1.
     */
    constructor (options) {
        super(options);

        // Parameter validation
        if (!options.parent) {
            throw new Error('Invalid configuration for Spinner.  "parent" is undefined.');
        }
        if (!options.itemTemplate) {
            throw new Error('Invalid configuration for Spinner.  "itemTemplate" is undefined.');
        }

        this.parent = options.parent;

        this.model.set('itemTemplate', options.itemTemplate);
        this.model.set('deceleration', options.deceleration || defaultDeceleration);
        this.model.set('showButtons', options.showButtons || false);
        this.model.set('buttonLayout', options.buttonLayout || 'vertical');
        this.model.set('numItems', this.model.get('showButtons') ? 1 : options.numItems || null);
        this.model.set('autoScrollMilliseconds',
            typeof options.autoScrollMilliseconds === 'number' ?
                options.autoScrollMilliseconds :
                autoScrollMilliseconds
        );

        this.model.set('value', options.value || null);

        let strings = this.model.get('strings') || {};
        strings.upButtonText = strings.upButtonText || upButtonTextDefault;
        strings.downButtonText = strings.downButtonText || downButtonTextDefault;
        this.model.set('strings', strings);

        /**
         * IScroll object.
         * @type {IScroll}
         */
        this.scroller = null;

        /**
         * Whether or not the widget was previously rendered.  Decides whether or not to do full render, or just
         * refreshing the currently selected value on show().
         * @type {boolean}
         */
        this.isRendered = false;

        this._renderPromise = Q.Promise.resolve();

        /**
         * Item container JQuery selector.
         * @type {jQuery}
         */
        this.$itemContainer = null;

        /**
         * jQuery selector for buttons
         * @type {jQuery}
         */
        this.$spinnerBtn = null;

        /**
         * Reference to window
         * @type {jQuery}
         */
        this.$win = $(window);

        /**
         * Keep track of spinner events that have occurred.
         * @type {Array}
         */
        this.spinnerEventQueue = [];

        /**
         * Item cache
         * @private
         */
        this._$items = null;

        /**
         * Value to item map.  Reverse lookup of value to HTML element.
         */
        this._valueIndexMap = {};

        /**
         * Index to value map.  Reverse lookup of index to value.
         */
        this._indexValueMap = {};

        /**
         * Selected item by this control
         * @type {Element|null}
         */
        this.selectedItem = null;

        /**
         * Direction of the currently depressed button (null if none)
         * @type {string|null}
         */
        this.buttonPressedDirection = null;

        /**
         * GUID token for the current button down event.
         * This way, "hold" events can be cancelled if the button event was replaced.
         * @type {string|null}
         */
        this.buttonEventToken = null;
    }

    /**
     * Parse the display value (shown in spinner) from the actual value that gets stored.
     * Default is for them to be identical.  One can override if deemed necessary
     * For instance, a number should do some fixed precision, and an item spinner (e.g. "Sunday-Saturday")
     * should do some sort of lookup based on a numeric value.
     * @param {string|number} value The value
     * @returns {string}
     */
    itemDisplayValueFunction (value) {
        return value;
    }

    /**
     * Parse the item value (stored value in spinner) from the value used to establish the template.
     * Default is for them to be identical.  One reason to override this is to create a fixed precision for numeric
     * values so that roundoff error mess up the appearance in the textbox in the NumberSpinner control.
     * @param {string|number} value The value
     * @returns {string}
     */
    itemValueFunction (value) {
        return value;
    }

    /**
     * Set our values.  Values can be passed into the model in a way that makes sense to the varation of the spinner.
     * @abstract
     */
    setValues () {
        throw new Error('setValues() is not defined in a BaseSpinnerInput extension.');
    }

    /**
     * Render the spinner.
     * @returns {Q.Promise<jQuery>} promise containing this.$el for convenience, in case the caller is interested
     * in listening to ready().
     */
    render () {
        return super.render()
        .then(() => {
            return Q.Promise((resolve) => {
                this.$el.ready(() => {
                    this.$itemContainer = this.$el.find('.item-container');
                    this.$spinnerBtn = this.$('.spinner-btn');
                    this.setValues();
                })
                .ready(() => {
                    this.setHeights();
                    this.setWidths();
                })
                .ready(() => {
                    this.setEmptyAreas();
                    this.setButtonVisibility();
                    this.scroller = new IScroll(this.$itemContainer.parent()[0],
                        {
                            deceleration: this.model.get('deceleration'),
                            tap: true
                        });
                    this.clearItemCache();
                })
                .ready(() => {
                    this.delegateEvents();
                    resolve(this.$el);
                });
            });
        });
    }

    /**
     * Show or hide buttons, depending on the showButtons parameter value.
    */
    setButtonVisibility () {
        let $spinner = this.$el.children('.spinner');

        if (!this.model.get('showButtons')) {
            $spinner.removeClass('spinner-with-buttons');
            this.$spinnerBtn.hide();
        } else {
            // spinner width before buttons added.  Value actually applied, without min-width
            let spinnerWidth = $spinner.css('min-width', 0).outerWidth();
            $spinner.css('min-width', '');

            this.$spinnerBtn.show();
            $spinner.addClass('spinner-with-buttons');
            if (this.model.get('buttonLayout') === 'horizontal') {
                $spinner.addClass('east-west');

                // Set spinner width to include horizontal buttons
                $spinner.width(spinnerWidth + (this.$spinnerBtn.outerWidth() * 2));
            }
        }
    }

    /**
     * Add custom events to our spinner.
     */
    addCustomEvents () {
        if (this.scroller) {
            this.handleScrollEnd = _.bind(this.handleScrollEnd, this);
            this.handleScrollCancel = _.bind(this.handleScrollCancel, this);
            this.handleBeforeScrollStart = _.bind(this.handleBeforeScrollStart, this);
            this.handleScrollStart = _.bind(this.handleScrollStart, this);
            this.scroller.on('scrollEnd', this.handleScrollEnd);
            this.scroller.on('scrollCancel', this.handleScrollCancel);
            this.scroller.on('beforeScrollStart', this.handleBeforeScrollStart);
            this.scroller.on('scrollStart', this.handleScrollStart);
        }
        if (this.$itemContainer) {
            this.handleTap = _.bind(this.handleTap, this);
            this.handleMouseDown = _.bind(this.handleMouseDown, this);
            this.handleMouseUp = _.bind(this.handleMouseUp, this);
            this.handleButtonDown = _.bind(this.handleButtonDown, this);
            this.handleButtonKeyPress = _.bind(this.handleButtonKeyPress, this);
            this.increment = _.bind(this.increment, this);
            this.decrement = _.bind(this.decrement, this);
            this.performButtonAction = _.bind(this.performButtonAction, this);

            this.$itemContainer.on('tap', this.handleTap);

            _.each(mouseDownEvents, (eventName) => {
                this.$itemContainer.on(eventName, this.handleMouseDown);
            });

            _.each(mouseUpEvents, (eventName) => {
                this.$win.on(eventName, this.handleMouseUp);
            });
        }
        if (this.$spinnerBtn) {
            _.each(mouseDownEvents, (eventName) => {
                this.$spinnerBtn.on(eventName, this.handleButtonDown);
            });

            this.$spinnerBtn.on('keypress', this.handleButtonKeyPress);
        }
    }

    /**
     * Removing custom events from our spinner.
     */
    removeCustomEvents () {
        if (this.scroller) {
            this.scroller.off('scrollEnd', this.handleScrollEnd);
            this.scroller.off('beforeScrollStart', this.handleBeforeScrollStart);
            this.scroller.off('scrollStart', this.handleScrollStart);
            this.scroller.off('scrollCancel', this.handleScrollCancel);
        }

        // cancel any repeat button timeouts
        this.buttonPressedDirection = null;

        if (this.$itemContainer) {
            this.$itemContainer.off('tap', this.handleTap);

            _.each(mouseDownEvents, (eventName) => {
                this.$itemContainer.off(eventName, this.handleMouseDown);
            });

            _.each(mouseUpEvents, (eventName) => {
                this.$win.off(eventName, this.handleMouseUp);
            });
        }
        if (this.$spinnerBtn) {
            _.each(mouseDownEvents, (eventName) => {
                this.$spinnerBtn.off(eventName, this.handleButtonDown);
            });

            this.$spinnerBtn.off('keypress', this.handleButtonKeyPress);
        }
    }

    /**
     * Handles a mousedown event on a spinner button.
     * Launches perFormButtonAction which allows the buttons to act as repeat buttons.
     * @param {Event} e event object forwarded by jQuery
     * @returns {Q.Promise<void>} promise resolving when action is finished.
     */
    handleButtonDown (e) {
        e.preventDefault();

        // Handle dup mouse-down style events by checking the button pressed direction (removed on mouseup)
        if (this.buttonPressedDirection === null) {
            let $btn = $(e.target).closest('button');
            this.buttonEventToken = createGUID();
            this.buttonPressedDirection = $btn.attr('data-dir') || null;
            return this.performButtonAction(0, this.buttonEventToken);
        }

        return Q();
    }

    /**
     * Handle a keypress on a spinner button.  Allow certain keys to perform increment/decrement operations.
     * @param {Event} e event object forwarded by jQuery
     * @returns {Q.Promise<void>} promise resolving when action is finished.
     */
    handleButtonKeyPress (e) {
        if (e.keyCode === keyCodeSpace || e.keyCode === keyCodeZero || e.keyCode === keyCodeCR) {
            let $btn = $(e.target).closest('button');
            switch ($btn.attr('data-dir')) {
                case 'up':
                    return this.increment();
                case 'down':
                    return this.decrement();
                default:
                    return Q();
            }
        }
        return Q();
    }

    /**
     * Get the delay for the next number iteration while mouse is down.
     * @param {number} itemIterationCount the iteration number for this mousedown session.
     * In case we want to accelerate as the number climbs.
     * @returns {number} calculated delay
     */
    calculateButtonDownDelay (itemIterationCount = 0) {
        return itemIterationCount === 0 ? repeatButtonStartDelay : repeatButtonDelay;
    }

    /**
     * Perform a click action on a button.
     * @param {number} itemIterationCount How many values have been moved since the button has been down.
     * (On initial press it should be 0)
     * @param {string} buttonEventToken unique identifier for mouse event.
     * If this is different than this.buttonEventToken before the end of the event,
     * the timeout should be cancelled.
     * @returns {Q.Promise<Object>} promise resolving when action is finished.
     * Handle to the next call is passed as nextCallPromise (for testing purposes).
     */
    performButtonAction (itemIterationCount, buttonEventToken) {
        let fn = null;

        // depending on the direction, check that our next available value is there,
        // and that our event token matches the currently active one.
        if (
            this.buttonPressedDirection === 'up' &&
            this.getNextAvailableValue() !== null &&
            this.buttonEventToken === buttonEventToken
        ) {
            fn = this.increment;
        } else if (
            this.buttonPressedDirection === 'down' &&
            this.getPreviousAvailableValue() !== null &&
            this.buttonEventToken === buttonEventToken
        ) {
            fn = this.decrement;
        }

        if (fn !== null) {
            this.$spinnerBtn.each((index, element) => {
                let $element = $(element);
                if ($element.attr('data-dir') === this.buttonPressedDirection) {
                    $element.addClass('active');
                    return false;
                }
                return true;
            });

            return fn()
            .then(() => {
                let ret = {};
                ret.nextCallPromise = Q()
                    .delay(this.calculateButtonDownDelay(itemIterationCount))
                    .then(() => {
                        return this.performButtonAction(itemIterationCount + 1, buttonEventToken);
                    });
                return ret;
            });
        }

        // If the event token is the same and it gets here, it means our target button is now disabled.
        if (this.buttonEventToken === buttonEventToken) {
            this.$spinnerBtn.removeClass('active');
        }
        return Q({ nextCallPromise: Q() });
    }

    /**
     * Go to previous available value.
     * @returns {Q.Promise<void>} promise resolving when setValue is finished.
     */
    decrement () {
        let previousValue = this.getPreviousAvailableValue();
        if (previousValue !== null) {
            // try to take care of some of the flashing.  Set this to selected before doing the scroll.
            $(this.$items[this._valueIndexMap[previousValue]]).addClass('selected');
            return this.setValue(previousValue, true);
        }
        return Q();
    }

    /**
     * Go to next available value.
     * @returns {Q.Promise<void>} promise resolving when setValue is finished.
     */
    increment () {
        let nextValue = this.getNextAvailableValue();
        if (nextValue !== null) {
            // try to take care of some of the flashing.  Set this to selected before doing the scroll.
            $(this.$items[this._valueIndexMap[nextValue]]).addClass('selected');
            return this.setValue(nextValue, true);
        }
        return Q();
    }

    /**
     * Based on current value, find the next available
     * @returns {string|null} value if there is a next available.  Null if there is none.
     */
    getNextAvailableValue () {
        let curValIndex = this._valueIndexMap[this.itemValueFunction(this.value)];
        for (let i = curValIndex + 1; i < this.$items.length; ++i) {
            if ($(this.$items[i]).css('display') !== 'none') {
                return this._indexValueMap[i];
            }
        }
        return null;
    }

    /**
     * Based on current value, find the previous available
     * @returns {string|null} value if there is a previous available.  Null if there is none.
     */
    getPreviousAvailableValue () {
        let curValIndex = this._valueIndexMap[this.itemValueFunction(this.value)];
        for (let i = curValIndex - 1; i >= 0; --i) {
            if ($(this.$items[i]).css('display') !== 'none') {
                return this._indexValueMap[i];
            }
        }
        return null;
    }

    /**
     * Cache heights of item and field.
     * If the user passed in a set number of items to be shown at a time, set a new field height.
     * Assumes all items are the same height (so ensure item template CSS guarantees this).
     */
    setHeights () {
        let $firstItem = this.$items.first(),
            numItems = this.model.get('numItems');
        this.itemHeight = $firstItem.outerHeight();

        if (numItems) {
            this.fieldHeight = this.itemHeight * numItems;
            this.$el.children('.spinner').children().not('button').first().height(this.fieldHeight);
            if (numItems <= fewSpinnerItemsThreshold) {
                this.$el.children('.spinner').addClass('few-spinner-items');
            }
        } else {
            this.fieldHeight = this.$el.children('.spinner').children().not('button').first().height();
        }
    }

    /**
     * Auto set widths for this control if necessary.
     * This ensures any large values are still contained.
     * Use "min-width" CSS on .spinner to control what the default width should be.
     */
    setWidths () {
        this.$itemContainer.css('width', 'auto')
        .ready(() => {
            this.$el.children('.spinner').width(this.$itemContainer.outerWidth());
            this.$itemContainer.css('width', '');
        });
    }

    /**
     * Creates buffer zones at the top and bottom of the spinner, so that the first and last object can be in the
     * middle of the scrollable area, and can be selected.
     */
    setEmptyAreas () {
        let bufferHeight = (this.fieldHeight / floatHalf) - (this.itemHeight / floatHalf);

        // remove old buffers.
        this.$itemContainer.find('.buffer-item').remove();
        this.$itemContainer.prepend(`<div class="buffer-item" style="height:${bufferHeight}px;">&nbsp;</div>`);
        this.$itemContainer.append(`<div class="buffer-item" style="height:${bufferHeight}px;">&nbsp;</div>`);
    }

    /**
     * Item comparitor.  Compare item at current index to another value.  Used during binary searches.
     * @param {number} currentIndex current index to compare to a value.
     * @param {string|number} value value to compare.
     * @returns {number} -1, 0, or 1, depending on the comparison result.
     */
    itemValueComparison (currentIndex, value) {
        let valueA = value,
            valueB = this.itemValueFunction($(this.$items[currentIndex]).data('value'));

        // Blank should be first item.  First 2 cases are to coerce the search that direction.
        if (valueA === '' && valueB !== '') {
            return -1;
        } else if (valueB === '' && valueA !== '') {
            return 1;
        } else if (valueA < valueB) {
            return -1;
        } else if (valueA > valueB) {
            return 1;
        }
        return 0;
    }

    /**
     * Get an item by its value.  First, attempt to do a binary search.  If that is unsuccessful (i.e. items are
     * not sorted), then fall back to a jQuery selector (iterative scan).
     * If includeHiddenItems is true, search both $hiddenItems as well as $items before trying a scan of both.
     * @param {number|string} value value to search for
     * @returns {Element|null} element found or null
     */
    getItemByValue (value) {
        if (this.$items[this._valueIndexMap[value]]) {
            return this.$items[this._valueIndexMap[value]];
        }

        // If selectedItem is null, we are either spinning (and forced to choose an answer because the widget is
        // closing), or we are finished spinning.
        // Either way, find item in the middle, set it as the new value, and update the UI.
        // Perform binary search to determine which element we want to select.
        let minIndex = 0,
            maxIndex = this.$items.length - 1,
            currentIndex;
        while (minIndex <= maxIndex) {
            // eslint-disable-next-line no-bitwise
            currentIndex = (minIndex + maxIndex) / intHalf | 0;
            let comp = this.itemValueComparison(currentIndex, value);
            if (comp < 0) {
                maxIndex = currentIndex - 1;
            } else if (comp > 0) {
                minIndex = currentIndex + 1;
            } else {
                return this.$items[currentIndex];
            }
        }

        // If it gets here, we need to get it based on a selector.
        let retValue = null;

        this.$items.filter(`.item[data-value='${value}']`).each((index, elem) => {
            retValue = elem;

            // break out since we found one
            return false;
        });

        // Will be null if everything fell through.
        return retValue;
    }

    /**
     * Remove item by value.  Optionally rescroll so our position remains the same
     * if it was above the scroll and we are setting it to display:none.
     * @param {number|string} value value to remove.
     * @returns {Q.Promise<void>} promise resolving when operation is complete.
     */
    hideItem (value) {
        let item = this.getItemByValue(value);

        $(item).css('display', 'none');
        return Q();
    }

    /**
     * Unhide item.  Optionally rescroll so our position remains the same
     * if it was above the scroll and used to be display:none.
     * @param {number|string} value value of item to unhide.
     * @returns {Q.Promise<void>} promise resolving when operation is complete.
     */
    unHideItem (value) {
        let item = this.getItemByValue(value);

        $(item).css('display', '');
        return Q();
    }

    /**
     * Unhide all items for this control.  Used when a global test of the items to ensure they are all valid
     * (faster than unhiding them individually)
    */
    unHideAll () {
        this.$items.css('display', '');
    }

    /**
     * Stop scrolling.  Usually used when the window is closed so we can calculate a value.
     */
    stop () {
        this.scroller.scrollBy(0, 0, 0);
    }

    /**
     * Update the UI for the selected item (give it "selected" css class).
    */
    updateSelectedItemUI () {
        if (!this.selectedItem) {
            this.pushValue(false);
            return;
        }

        if (!this.selectedItem.classList.contains('selected')) {
            this.selectedItem.classList.add('selected');
        }

        // clear spinnerEventQueue, since we're setting the UI and are done with user interaction for the moment.
        this.spinnerEventQueue.splice(0, this.spinnerEventQueue.length);
    }

    /**
     * Set the value.  Also calls out to update the UI.
     * @param {string|number} val The value
     * @param {boolean} [immediate=true] ignore scroll milliseconds, and do the scroll in 0.
     * @returns {Promise<void>} promise resolving when value is unhidden and set.
     */
    setValue (val, immediate = true) {
        this.value = val;
        return this.unHideItem(val, false)
        .then(() => {
            let selectedItem = this.getItemByValue(val);

            // save some time by setting the selectedItem and readying the UI here.
            // so that pullValue() will not need to look it up.
            if (this.selectedItem) {
                this.selectedItem.classList.remove('selected');
            }
            this.selectedItem = selectedItem;
            this.pullValue(immediate);
        });
    }

    /**
     * Pull the value from the "value" property, and use it to update the UI.
     * @param {boolean} [immediate=true] ignore scroll milliseconds, and do the scroll in 0.
     */
    pullValue (immediate = true) {
        let value = this.value;

        if (value === null || value === undefined) {
            if (this.selectedItem) {
                this.selectedItem.classList.remove('selected');
            }
            this.selectedItem = null;
            return;
        }

        if (!(this.selectedItem && $(this.selectedItem).data('value') === this.value)) {
            if (this.selectedItem) {
                this.selectedItem.classList.remove('selected');
            }
            this.selectedItem = this.getItemByValue(value);
        }

        if (this.selectedItem !== null) {
            let $item = $(this.selectedItem);
            let topPos = $item.position().top,
                height = $item.height(),
                curScrollY = this.getScrollY(),
                targetScrollTop;

            targetScrollTop = parseInt(
                -Math.round(topPos - ((this.fieldHeight / floatHalf) - (height / floatHalf))), 10
            );
            if (targetScrollTop !== curScrollY) {
                // calculate an auto scroll time... less time for a shorter scroll distance
                let difference = Math.abs(targetScrollTop - curScrollY),
                    scrollTime = immediate ? 0 :
                                this.model.get('autoScrollMilliseconds') *
                                Math.min(difference * floatOne / height, 1);
                this.scroller.scrollTo(0, targetScrollTop, scrollTime);
            } else {
                this.updateSelectedItemUI();
            }
        }

        // disable any spinner buttons that are no longer accessible
        this.$spinnerBtn.each((iNdx, element) => {
            let $element = $(element),
                dir = $element.data('dir');

            let enabled = (dir === 'up' && !_.isNull(this.getNextAvailableValue())) ||
                            (dir === 'down' && !_.isNull(this.getPreviousAvailableValue()));
            if (enabled) {
                if (element.hasAttribute('disabled')) {
                    $element.removeAttr('disabled');

                    // click is to remove "active" status.
                    // For some reason, using a keyboard makes the status active when it is disabled,
                    // so when it re-enables, it looks active (even though it shouldn't be)
                    $element.trigger('click');
                }
            } else {
                $element.attr('disabled', 'disabled');
            }
        });
    }

    /**
     * Get logical Y scrolling value.
     * Eliminate error from bouncing by enforcing the highest and lowest bounds allowed by the scroller.
     * @returns {number} scroll Y value, within the bounds of our scroller.
     */
    getScrollY () {
        if (!this.scroller) {
            return null;
        }

        // maxScrollY is negative, so it should be smaller or equal to this.scroller.y
        return Math.min(Math.max(this.scroller.y, this.scroller.maxScrollY), 0);
    }

    /**
     * Comparison function that determines an item's position, relative to the selected item.
     * For a spinner, an item is considered selected if it is in the middle of the field.
     * @param {number} index index of the element to compare
     * @returns {number|null} comparison number (null if the item is set to not display).
     * (-1 if above than the selected item in the list, 1 if it is below, and 0 if this item is selected).
     */
    itemPositionComparison (index) {
        let $item = $(this.$items[index]),
            relY,
            topPos,
            height;

        if ($item.css('display') === 'none') {
            return null;
        }

        relY = this.fieldHeight / floatHalf;
        topPos = $item.position().top + this.getScrollY();
        height = $item.height();

        if (topPos + height <= relY) {
            // scroll is too low (item too high)... return 1 to find a higher indexed item
            return 1;
        } else if (topPos > relY) {
            // scroll is too high (item too low)... return -1 to find a lower indexed item
            return -1;
        }

            // if it gets here, item is right in range on the middle of the scroller.  Return 0 for equality.
        return 0;
    }

    /**
     * Push the current selected value into the "value" property.  And call "pullValue()" if the UI isn't updated yet.
     * If "selectedItem" exists, use that (came from a click of a specific item).
     * @param {boolean} [immediate=true] ignore scroll milliseconds, and do the scroll in 0.
     */
    pushValue (immediate = true) {
        if (this.selectedItem === null) {
            // If selectedItem is null, we are either spinning (and forced to choose an answer because the widget is
            // closing), or we are finished spinning.
            // Either way, find item in the middle, set it as the new value, and update the UI.
            // Perform binary search to determine which element we want to select.
            let minIndex = 0,
                maxIndex = this.$items.length - 1,
                currentIndex,
                selectedIndex = null,
                comp = null;
            while (minIndex <= maxIndex) {
                // eslint-disable-next-line no-bitwise
                currentIndex = (minIndex + maxIndex) / intHalf | 0;
                comp = null;

                // Next section is a bit of wiggling.
                //  If the comparison function returns null, this means that the position was not calculable,
                //  which means that it is display:none.  Not to fear, just keep moving higher until you find one
                //  that is calculable... then go down until you find one.
                do {
                    comp = this.itemPositionComparison(currentIndex);
                } while (comp === null && ++currentIndex <= maxIndex);

                if (comp === null) {
                    // eslint-disable-next-line no-bitwise
                    currentIndex = (minIndex + maxIndex) / intHalf | 0;
                }
                while (comp === null && --currentIndex >= minIndex) {
                    comp = this.itemPositionComparison(currentIndex);
                }

                // Null at this point means all items within this range are disabled.
                if (comp === null) {
                    break;
                }

                if (comp < 0) {
                    maxIndex = currentIndex - 1;
                } else if (comp > 0) {
                    minIndex = currentIndex + 1;
                } else {
                    selectedIndex = currentIndex;
                    break;
                }
            }

            if (selectedIndex !== null) {
                currentIndex = selectedIndex;

                // Now that we know the position of the selected item, choose the next one after it that is not
                // disabled.  If none, go the other direction.
                do {
                    if ($(this.$items[currentIndex]).css('display') !== 'none') {
                        this.selectedItem = this.$items[currentIndex];
                    }
                } while (this.selectedItem === null && ++currentIndex < this.$items.length);

                if (this.selectedItem === null) {
                    currentIndex = selectedIndex;
                }
                while (this.selectedItem === null && --currentIndex >= 0) {
                    if ($(this.$items[currentIndex]).css('display') !== 'none') {
                        this.selectedItem = this.$items[currentIndex];
                    }
                }
            }
        }
        if (this.selectedItem !== null) {
            this.value = $(this.selectedItem).data('value');
        }
        this.pullValue(immediate);
    }

    /**
     * Event after the scroll has ended.
     */
    handleScrollEnd () {
        this.spinnerEventQueue.push('scrollEnd');

        // Maybe just tapping or changing directions.  Wait it out.
        if (this.mouseDown) {
            return;
        }

        if (!this.userInitiatedScroll) {
            // Auto scroll to snap.  Style when finished.
            this.updateSelectedItemUI();
            this.spinnerEventQueue.push('scrollEnd-success');
            return;
        }

        this.pushValue(false);
        this.spinnerEventQueue.push('scrollEnd-success');
    }

    /**
     * Handle when scroll is cancelled.  When this occurs, mouse is always up,
     * so set that and forward to handleScrollEnd
     */
    handleScrollCancel () {
        this.spinnerEventQueue.push('scrollCancel');
        this.handleScrollEnd();
    }

    /**
     * Event when a scroll is about to be started by a user.  Updates the userInitiatedScroll property so we know
     * this is not an auto-scroll to center the selected value
     */
    handleBeforeScrollStart () {
        this.spinnerEventQueue.push('beforeScrollStart');
    }

    /**
     * If the user started this scroll... set our value and selectedItem to null, awaiting a new value.
     */
    handleScrollStart () {
        this.spinnerEventQueue.push('scrollStart');

        if (!this.userInitiatedScroll) {
            return;
        }

        this.value = null;
        this.pullValue();
    }

    /**
     * Handle mouse down event on the scroller.
     * @returns {boolean} true
    */
    handleMouseDown () {
        this.spinnerEventQueue.push('mousedown');
        return true;
    }

    /**
     * Handle mouse up event on the scroller.
     * @returns {boolean} true
    */
    handleMouseUp () {
        this.spinnerEventQueue.push('mouseup');

        if (this.previousSpinnerEvent === 'scrollEnd') {
            // Must have finished scrolling, then lifted finger (possible on Edge).
            //  Set an answer.
            this.handleScrollEnd();
        }

        // cancel any repeat button timeouts on any mouse up event
        if (!_.isNull(this.buttonEventToken)) {
            this.buttonPressedDirection = null;
            this.buttonEventToken = null;
            this.$spinnerBtn.removeClass('active');
        }

        return true;
    }

    /**
     * Handle tap of an element.  Make that element the "selectedItem" and call pushValue() to push it into the value
     * property (and update the UI).
     * @param {Object} e event object, contains the target of our click.
     */
    handleTap (e) {
        this.spinnerEventQueue.push('tap');

        // A tap when scrolling in a user initiated scroll should stop the scroller.  Fire the scrollend event.
        if (this.inScroll) {
            // False because we just tapped.
            this.handleScrollEnd();
            return;
        }

        // jscs:disable requireArrowFunctions
        $(e.target).closest('.item').each((index, element) => {
            this.value = $(element).data('value');
            this.pullValue(false);

            // Do not keep iterating after finding one item node.
            return false;
        });
        // jscs:enable requireArrowFunctions
    }

    /**
     * clear our cache (jQuery selector) of items.
     * Set private value to null so getter will recalculate next time
     */
    clearItemCache () {
        this._$items = null;
    }

    /**
     * Shutdown this control.  Destroy references and prepare for garbage collection.
    */
    destroy () {
        super.destroy();
        this.clearItemCache();
        this.$itemContainer = null;
        this.$spinnerBtn = null;
        this.buttonEventToken = null;
        this.buttonPressedDirection = null;
        this.$win = null;
        if (this.scroller !== null) {
            this.scroller.destroy();
            this.scroller = null;
        }
    }
}
