import BaseSpinnerInput from './BaseSpinnerInput';

const defaultMin = 0,
    defaultMax = 100,
    defaultStep = 1,
    defaultShowLeadingZeros = false,
    defaultPrecision = 0,
    defaultIncludeBlankValue = true;

/**
 * Defines options for the SpinnerInput itself.
 */
export class NumberSpinnerInputOptions {
    /**
     * Construct a SpinnerOptions class
     * @param {Object} options The options used to construct the class.
     */
    constructor (options) {
        /**
         * minimum value for the spinner
         * @type {number}
         * @default 0
         */
        this.min = options.min;

        /**
         * maximum value for the spinner
         * @type {number}
         * @default 100
         */
        this.max = options.max;

        /**
         * step value for the spinner
         * @type {number}
         * @default 1
         */
        this.step = options.step;

        /**
         * precision (number of decimal places to show)
         * @type {number}
         * @default 0
         */
        this.precision = options.precision;

        /**
         * whether or not transform our value to not show leading 0's.
         * If false, rendering of the item template will remove string of 0's in the beginning of the string,
         * but will not remove a 0 that is also the last 0 in the string.
         * (e.g. "0.123" => ".123", "000" => "0", "001234" => "1234")
         * @type {boolean}
         * @default false
         */
        this.showLeadingZeros = options.showLeadingZeros;

        /**
         * whether or not to include a blank default value in the spinner
         * @type {boolean}
         * @default true
         */
        this.includeBlankValue = options.includeBlankValue;
    }
}

/**
 * Extension of spinner input that accomodates the NumberSpinner.
 * Notable differences is that the model is expected to have a min, max, and step for setValues().
 * @param {NumberSpinnerInputOptions} options  spinner input options passed to this control.
 */
export default class NumberSpinnerInput extends BaseSpinnerInput {
    /**
     * Construct NumberSpinnerInput
     * @param {NumberSpinnerInputOptions} options  spinner input options passed to this control.
     */
    constructor (options) {
        super(options);

        this.model.set('min', options.min || defaultMin);
        this.model.set('max', options.max || defaultMax);
        this.model.set('step', options.step || defaultStep);
        this.model.set('showLeadingZeros', options.showLeadingZeros || defaultShowLeadingZeros);
        this.model.set('precision', options.precision || defaultPrecision);
        this.model.set(
            'includeBlankValue',
            typeof options.includeBlankValue === 'boolean' ? options.includeBlankValue : defaultIncludeBlankValue
        );
    }

    /**
     * Set display value for the number, based on precision and whether or not we should show leading zeros.
     * @param {string|number} value The value
     * @returns {string}
     */
    itemDisplayValueFunction (value) {
        if (typeof value === 'number') {
            return NumberSpinnerInput.getNumericDisplayValue(
                value,
                this.model.get('precision'),
                this.model.get('showLeadingZeros')
            );
        }
        return value;
    }

    /**
     * Call BaseSpinnerInput.setValue.
     * Verify numeric value or make it blank.
     * @override BaseSpinnerInput.setValue.
     * @param {string|number} val The value
     * @param {boolean} [immediate=true] ignore scroll milliseconds, and do the scroll in 0.
     * @returns {Promise<void>} promise resolving when value is unhidden and set.
     */
    setValue (val, immediate = true) {
        let value = isNaN(parseFloat(val)) ? '' : val;
        return super.setValue(value, immediate);
    }

    /**
     * Set stored value for the number, based on precision and whether or not we should show leading zeros.
     * @param {string|number} value The value
     * @returns {string|number}
     */
    itemValueFunction (value) {
        if (!isNaN(parseFloat(value))) {
            return parseFloat(parseFloat(value).toFixed(this.model.get('precision')));
        }
        return '';
    }

    /**
     * Get a display value for this number.  Allow options for showing leading zeros and setting the decimal precision.
     * @param {number} numericVal value to convert
     * @param {number} [precision=0] number of decimal places to show
     * @param {boolean} [showLeadingZeros=false] whether or not to show leading zeroes in the response.
     * (note, the value of "0" is still displayed, as long as "0" is at the end of the string.
     * (i.e. "0" => "0"... "000" => "0").
     * @returns {string}
     */
    static getNumericDisplayValue (numericVal, precision = 0, showLeadingZeros = false) {
        let returnVal = parseFloat(numericVal).toFixed(precision).toString();

        if (!showLeadingZeros) {
            /**
             * Function to replace leading zeroes
             * @param {string} a full match (including zeroes) - unused
             * @param {string} b match of actual numeric value without zeroes.
             * @returns {string} numeric value without zeroes.
             */
            let leadingZeroReplaceFn = (a, b) => {
                return b;
            };
            returnVal = returnVal.replace(/^0+([^$]+)$/, leadingZeroReplaceFn);
        }
        return returnVal;
    }

    /**
     * Set values of our spinner based on min, max, and step passed into our control
     */
    setValues () {
        // Initalize with a blank value.  Will be removed as soon as something else is selected.
        let fullItemString = '',
            template,
            i,
            min = this.model.get('min'),
            max = this.model.get('max'),
            step = parseFloat(this.model.get('step')),
            precision = this.model.get('precision'),
            itemTemplate = this.model.get('itemTemplate'),
            itemTemplateFactory;

        itemTemplateFactory = LF.templates.getTemplateFromKey(itemTemplate);

        if (this.model.get('includeBlankValue')) {
            fullItemString += itemTemplateFactory({ value: '', displayValue: '&nbsp;' });
        }

        // Add range of values if min/max/step are defined.
        for (i = parseFloat(min); i <= parseFloat(max); i = parseFloat((i + step).toFixed(precision))) {
            template = itemTemplateFactory(
                { value: this.itemValueFunction(i), displayValue: this.itemDisplayValueFunction(i) }
            );
            fullItemString += template;

            // on the last iteration of this loop, i represents the max value,
            // as far as the step can go... so that can be considered our new max.
            this.model.set('max', i);
        }

        this.$itemContainer.append(fullItemString);
    }
}
