const { View } = Backbone;

/**
 * Interface for "Input" Controls.  These are used as modal/embedded dialog controls.
*/
class BaseInput extends View {
    /**
     * Value of the control.  Interfaces with value in the model.
     * @type {string}
     */
    get value () {
        if (this.model) {
            return this.model.get('value');
        }
        return null;
    }

    /**
     * Value of the control.  Interfaces with value in the model.
     * @param {string} val value to set
     */
    set value (val) {
        this.model.set('value', val);
    }

    /**
     * Constructs the Input Control
     * @param {Object} options options for this control.
     * @param {Object} options.strings Key/value pairs of translated strings for pushbuttons.
     * @param {string} options.callerId ID of the caller, to be used by the pushbutton item group.
     */
    constructor (options) {
        super(options);

        if (!options.template) {
            throw new Error('Invalid configuration for Input class.  "template" is undefined.');
        }

        this.parent = options.parent;

        this.model.set('strings', options.strings || {});
        this.model.set('callerId', options.callerId);
        this.model.set('template', options.template);

        this._renderPromise = Q();

        this.isRendered = false;
    }

    /**
     * Show our modal control.  Do nothing if it has already been rendered.
     * @returns {Promise<{jQuery}>} promise containing this.$el for convenience, in case the caller is interested
     * in listening to ready().
     */
    show () {
        if (!this.isRendered) {
            this._renderPromise = this.render();
        } else {
            this._renderPromise = this._renderPromise.then(() => {
                return this.$el;
            });
        }
        return this._renderPromise;
    }

    /**
     * Render our control
     * @returns {Q.Promise<Element>} promise resolving when render is complete.
     * Contains element in case caller is interested in chaining ready()
     */
    render () {
        this.isRendered = true;
        return Q()
        .then(() => {
            // Set button text params to what is passed in or defaults.
            let strings = this.model.get('strings');

            this.$el.append(LF.templates.display(this.model.get('template'), strings));
            $(this.parent).append(this.$el);
            return this.$el;
        });
    }

    /**
     * Method used when hidden.
     * @abstract
    */
    onHidden () {
        // Optionally overridden by subclass
    }

    /**
     * Method used to pull the value from the model and update the UI
     * @abstract
    */
    pullValue () {
        throw new Error('Unimplemented pullValue function');
    }

    /**
     * Method used to take value from UI and push it into the model.
     * @abstract
     */
    pushValue () {
        throw new Error('Unimplemented pushValue function');
    }

    /**
     * Overridden to have handlers for scroller as well.
     * (in the events object) so create and destroy manually
     */
    delegateEvents () {
        super.delegateEvents();
        this.addCustomEvents();
    }

    /**
     * Add custom events for delegation (events that can't be done the bootstrap way).
     * Also used to bind functions that can be called by event handlers.
    */
    addCustomEvents () {
        this.destroy = _.bind(this.destroy, this);
    }

    /**
     * Overridden to remove our custom delegated events.
     */
    undelegateEvents () {
        super.undelegateEvents();
        this.removeCustomEvents();
    }

    /**
     * Remove custom events
     * @abstract
    */
    removeCustomEvents () {
        // Custom event removal code can be added by subclasses here
    }

    /**
     * Shutdown this control.  Destroy references and prepare for garbage collection.
    */
    destroy () {
        this.undelegateEvents();
    }
}

export default BaseInput;
