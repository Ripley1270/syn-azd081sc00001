import TimeSpinnerMixin from './mixin/TimeSpinnerMixin';
import TextBoxCustomInputBase from './TextBoxCustomInputBase';

/**
 * Variation of Interval Spinner for Time only.
 * Docs to be produced when widget is validated.
 */
class TimeSpinner extends TextBoxCustomInputBase {
    /**
     * Construct a TimeSpinner by mixing TextBoxCustomInputBase with TimeSpinnerMixin
     * @param {Object} options options for this widget
     */
    constructor (options) {
        options.model.set('modalMixin', TimeSpinnerMixin);
        return super(options);
    }
}

window.LF.Widget.TimeSpinner = TimeSpinner;
export default TimeSpinner;
