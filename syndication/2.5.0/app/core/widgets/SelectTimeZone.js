import SelectWidgetBase from './SelectWidgetBase';
import TimeZoneUtil from 'core/classes/TimeZoneUtil';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';

let logger = new Logger('SelectTimeZone');

export default class SelectTimeZone extends SelectWidgetBase {
    /**
     * @property {string} input - The input template to use when rendering the widget.
     * @default 'DEFAULT:SelectWidget'
     */
    get input () {
        return 'DEFAULT:SelectWidget';
    }

    /**
     * Resolve any dependencies required for the time zones to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return Q.Promise((resolve) => {
            LF.Wrapper.exec({
                execWhenWrapped: () => {
                    return TimeZoneUtil.getAllTimeZones()
                    .then((timezones) => {
                        this.timeZones = TimeZoneUtil.filterTimeZones(timezones);
                    })
                    .catch((err) => {
                        const { Dialog } = MessageRepo;
                        this.timeZones = [];
                        logger.error('Error occurred while obtaining the time zone list.', err);
                        return MessageRepo.display(Dialog && Dialog.TIME_ZONE_ERROR);
                    })
                    .done(resolve);
                },
                execWhenNotWrapped: () => {
                    // This code is a temporary fix to unblock devs working on browser.
                    // TODO: We need a proper way to handle workflow of setting timezone in browser
                    this.timeZones = [{
                        id: 'America/New_York',
                        displayName: '(GMT-05:00) America/New_York',
                        offset: '-18000000'
                    }];
                    resolve();
                }
            });
        });
    }

    /**
     * Initializes the select2 plugin on our select box.
     * @param {Object} [options={}] - Optional parameters to pass into select2.
     * @returns {Q.Promise<jQuery>} A promise resolving with a jQuery selector for element, for chaining purposes.
     */
    initializeSelect2 (options = {}) {
        let optParams = _.extend(options, {
            dir: 'ltr',

            // DE17530 - Empty <option> selectable at top of list.
            placeholder: ''
        });

        return super.initializeSelect2(optParams);
    }

    /**
     * Extension of SelectWidgetBase for a particular instance of a Select Widget control.
     * @param {string} divToAppendTo - The selected ID that the options should be appended to.
     * @returns {Q.Promise<void>}
     */
    renderOptions (divToAppendTo) {
        let template = _.template(
            `<option id='{{ id }}'
                 value='${JSON.stringify({ id: '{{ id }}', offset: '{{ offset }}' })}'>
                 {{ displayName }}
            </option>`);

        $(divToAppendTo).empty();

        // In order for the placeholder option to work, <select> requires the first
        // child element to be an empty <option>
        $(divToAppendTo).append('<option></option>');

        return this.resolve()
        .then(() => {
            this.timeZones.forEach((timezone) => {
                let el = template(timezone);

                this.$(divToAppendTo).append(el);
            });
        });
    }

    /**
     * Respond handler for this widget. Checks if the selection is valid before calling the respond
     * method of the base class.
     * @param {Object} e Event object.
     */
    respond (e) {
        let value = e.target ? this.$(e.target).val() : e.value;

        !!value && super.respond(e);
    }
}

window.LF.Widget.SelectTimeZone = SelectTimeZone;
