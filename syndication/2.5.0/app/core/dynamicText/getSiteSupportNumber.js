import Sites from 'core/collections/Sites';

let siteSupportNumberFn = (delimiter) => {
    let supportOptions = LF.StudyDesign.supportOptions,
        supportOptionsCountryMap = LF.StudyDesign.supportOptionsCountryMap;
    return Sites.fetchFirstEntry()
    .then((site) => {
        let supportText = supportOptionsCountryMap[site.get('countryCode')];
        let numbers = [];

        if (!supportText) {
            numbers.push('SUPPORT_NUMBER_NOT_FOUND');
        } else {
            _.each(supportOptions, (item) => {
                if (item.text === supportText) {
                    numbers.push(item.number);
                }
            });
        }
        return LF.strings.display(_.flatten(numbers), $.noop);
    })
    .then((strings) => {
        return strings.join(delimiter);
    });
};

export const getSiteSupportNumber = {
    id: 'getSiteSupportNumber',
    evaluate: () => {
        return siteSupportNumberFn(', ');
    },
    screenshots: {
        values: ['867-5309']
    }
};

export const getSiteSupportNumberWithBreaks = {
    id: 'getSiteSupportNumberWithBreaks',
    evaluate: () => {
        return siteSupportNumberFn('<br />');
    },
    screenshots: {
        values: ['867-5309']
    }
};
