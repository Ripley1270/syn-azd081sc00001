import Data from 'core/Data';

/**
 * Get and translate the questionnare title.
 * Should be able to use the same function for screenshots.
 * @returns {Q.Promise<string>} Promise that resolves with the translated string of the questionnaire name.
 */
const getQuestionnaireTitle = () => {
    const displayName = Data.Questionnaire.model.get('displayName') || 'DISPLAY_NAME';

    return LF.getStrings(displayName, $.noop, {
        namespace: Data.Questionnaire.id
    });
};

/**
 * Get the name of the current questionnaire.
 */
export const questionnaireTitle = {
    id: 'questionnaireTitle',
    evaluate: () => getQuestionnaireTitle(),
    screenshots: {
        getValues: () => getQuestionnaireTitle()
    }
};
