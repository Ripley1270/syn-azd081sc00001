/**
 * Always set the alarm.
 * @param {LF.Model.Schedules} schedule object containing schedule configuration.
 * @param {Object} alarmParams alarm object containing alarm configuration.
 * @param {LF.Collection.Dashboards} completedQuestionnaires list of all completed diaries.
 * @param {Object} parentView The view to append this scheduled item to.
 * @param {function} callback A callback function invoked upon.
 */
LF.Schedule.alarmFunctions.addAlwaysAlarm = (schedule, alarmParams, completedQuestionnaires, parentView, callback) => {
    callback(true);
};

export default LF.Schedule.alarmFunctions.addAlwaysAlarm;
