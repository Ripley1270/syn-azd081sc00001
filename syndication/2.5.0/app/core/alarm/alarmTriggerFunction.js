/**
 * A trigger function that is called when an alarm is fired.
 * @param {String} id - the id of the alarm that was triggered.
 * @param {String} state - 'foreground' or 'background'
 * @param {Object} params - parameters that were set with the alarm
 */
// eslint-disable-next-line no-unused-vars
LF.Schedule.alarmFunctions.alarmTriggerFunction = (id, state, params) => {
    // var logger = (Log4js.loggers['alarmTriggerFunction.alarmTriggerFunction'] ? Log4js.loggers['alarmTriggerFunction.alarmTriggerFunction'] : new LF.Log.Logger('alarmTriggerFunction.alarmTriggerFunction'));

    //  logger.log(Log4js.Level.OPERATIONAL, 'Alarm Trigger Function for ID ' + id + ': Params Data - ' + params.testData);
};

export default LF.Schedule.alarmFunctions.alarmTriggerFunction;
