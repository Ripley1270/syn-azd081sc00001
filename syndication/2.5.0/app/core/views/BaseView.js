import COOL from 'core/COOL';
import Logger from 'core/Logger';

// Imported so LF.displayStrings is defined.
import 'core/collections/Languages';

// All templates are stored here in memory as they're fetched from the DOM.
// Subsequent renders are made faster by this.
let templateCache = {};

const logger = new Logger('BaseView');

/**
 * Base view for all Syndication views.
 * @class BaseView
 * @extends {Backbone.View}
 */
export default class BaseView extends Backbone.View {
    constructor (options) {
        super(options);

        /**
         * A list of events applied to the view's DOM.
         * @type Object
         */
        this.events = this.events || {};
    }

    /**
     * Determines the root element of the view.
     * @returns {string}
     */
    get tagName () {
        return 'div';
    }

    /**
     * Default resolve method for in cases where a view doesn't have one defined.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return Q();
    }

    /**
     * Render the view. Unlike the Backbone.View method it overrides, this
     * method is expected to return a Promise which resolves when the view
     * is rendered to it's virtual DOM.
     * @abstract
     * @returns {Q.Promise<*>}
     */
    render () {
        throw new Error('must be implemented by subclass!');
    }

    /**
     * Remove the view and prevent zombies(living dead event listeners).
     * @example this.close();
     */
    close () {
        this.undelegateEvents();
        this.remove();
    }

    /**
     * @returns {Backbone.View<TModel>}
     * Remove the view from the DOM.
     */
    remove () {
        this.clearSelectors();
        this.undelegateEvents();

        return super.remove();
    }


    /**
     * Find and return an underscore template.
     * @param {string=} selector The selector for the template to fetch and wrap.
     * @returns {_.TemplateExecutor}
     */
    getTemplate (selector) {
        let selectorValue = selector || this.template;

        // If there is no selector, throw an error.
        if (!selectorValue) {
            throw new Error('No template selector provided.');
        }

        // If the template is cached, use it.
        if (templateCache[selectorValue]) {
            return templateCache[selectorValue];
        }

        let template = _.template($(selectorValue).html());
        templateCache[selectorValue] = template;

        return template;
    }

    /**
     * Clears the private template cache. Use more for unit testing.
     */
    clearTemplateCache () {
        templateCache = {};
    }

    /**
     * Build the view's HTML.
     * @param {object} dynamicStrings Any strings that are not to be translated, but are pulled from other sources. ex. A site's ID.
     * @returns {Q.Promise} The translated strings.
     */
    buildHTML (dynamicStrings = {}) {
        let template = this.getTemplate();

        this.templateStrings = this.templateStrings || {};

        // Before constructing the view, remove any existing event listeners.
        this.undelegateEvents();

        return this.i18n(this.templateStrings)
            .then((translated) => {
                let strings = _.extend(translated, dynamicStrings, { key: this.key }),
                    compiled;

                // Try and compile the template with the translated and dynamic strings.
                try {
                    compiled = template(strings);
                } catch (e) {
                    logger.error(e);
                    return Q.reject(e.message);
                }

                // Pass the combined strings into the template, and set it as the view's HTML.
                this.$el.html(compiled);

                // Build any predefined selectors.
                this.buildSelectors();

                // Ensure that all defined event listeners are once again enabled.
                this.delegateEvents();

                return strings;
            });
    }

    /**
     * Generate shortcut selectors from this.selectors. Called with buildHTML().
     * @example
     * this.selectors = { submit : '#submit' };
     * this.buildSelectors();
     * this.$submit.attr('disabled', 'disabled');
     */
    buildSelectors () {
        let selectors = this.selectors || {};
        _(selectors).forEach((value, name) => {
            this[`$${name}`] = this.$(value);
        });
    }

    /**
     * Clear any generated shortcut selectors. Called on view removal.
     * @example
     * this.clearSelectors();
     */
    clearSelectors () {
        let selectors = this.selectors || {};
        _(selectors).forEach((value, name) => {
            this[`$${name}`] = null;
        });
    }

    /**
     * An alias for LF.strings.display. See {@link core/collections/Languages Languages} for details.
     * @param {object|string} strings The translations keys to translate.
     * @param {function} callback A callback function invoked when the strings have been translated.
     * @param {object} options Options passed into the translation.
     * @returns {Q.Promise}
     */
    i18n (strings, callback = $.noop, options = undefined) {
        return LF.strings.display(LF.strings, strings, callback, options);
    }

    /**
     * Returns a jQuery object based on the provided selector.
     * @param {string|object} selector The selector of the element to get.
     * @returns {Object}
     */
    getElement (selector) {
        return typeof selector === 'string' ? this.$(selector) : selector;
    }

    /**
     * Enable an element (scoped to the view).
     * @param {string|object} selector Either a string selector, or a jQuery object, of the element to enable.
     * @returns {object} The enabled element.
     */
    enableElement (selector) {
        let $ele = this.getElement(selector);

        return $ele.removeAttr('disabled');
    }

    /**
     * Disable an element (scoped to the view).
     * @param {string|object} selector Either a string selector, or a jQuery object of the element to disable.
     * @returns {object} The disabled element.
     */
    disableElement (selector) {
        let $ele = this.getElement(selector);

        return $ele.attr('disabled', 'disabled');
    }

    /**
     * Determines if an element is disabled.
     * @param {string|object} selector Either a string selector, or a jQuery object of the element to check.
     * @returns {Boolean}
     */
    isDisabled (selector) {
        let $ele = this.getElement(selector);

        return !!$ele.attr('disabled');
    }

    /**
     * A shortcut for enableElement.
     * @param {string|object} button Either a string selector, or a jQuery object of the button to enable.
     * @returns {object} The enabled button.
     */
    enableButton (button) {
        return this.enableElement(button);
    }

    /**
     * A shortcut for disableButton.
     * @param {string|object} button Either a string selector, or a jQuery object of the button to disable.
     * @returns {object} The disabled button.
     */
    disableButton (button) {
        return this.disableElement(button);
    }

    /**
     * Show an element in the view.
     * @param {string|object} selector Either a string selector, or a jQuery object of the element to show.
     * @returns {JQuery} The shown element.
     */
    showElement (selector) {
        let $ele = this.getElement(selector);

        return $ele.removeClass('hidden');
    }

    /**
     * Hide an element in the view.
     * @param {string|object} selector Either a string selector, or a jQuery object of the element to hide.
     * @returns {object} The hidden element.
     */
    hideElement (selector) {
        let $ele = this.getElement(selector);

        return $ele.addClass('hidden');
    }
}

/*
 * Due to ES6's spec, you cannot set properties outside of the constructor!
 * This creates a condition, where class properties will be overwritten by the super class.
 * Since ES6 classes are syntactic sugar onto of prototypal inheritance, we still have direct access to the prototype chain...
 * This isn't the cleanest solution, but it provides what we need.
 */
BaseView.prototype.id = 'page';

COOL.add('BaseView', BaseView);
