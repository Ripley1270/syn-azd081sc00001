import PasswordBaseView from 'core/views/PasswordBaseView';
import templates from 'core/resources/Templates';

import Data from 'core/Data';
import Logger from 'core/Logger';

let logger = new Logger('ChangeTemporaryPasswordView');

export default class ChangeTemporaryPasswordView extends PasswordBaseView {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,(Object|string)>} templateStrings - Strings to fetch in the render function
         * @type Object
         */
        this.templateStrings = {
            activatedElsewhere: 'DEACTIVATED_LABEL',
            header: 'APPLICATION_HEADER',
            title: 'CHANGE_PASSWORD',
            newPassword: 'ENTER_NEW_PASSWORD',
            back: 'BACK',
            confirmPassword: 'CONFIRM_PASSWORD',
            submit: 'OK',
            submitIcon: {
                key: 'OK',
                namespace: 'ICONS'
            },
            pwdRules: 'PASSWORD_RULES'
        };
    }

    /**
     * @property {string} id - The default id of the root element
     * @readonly
     * @default 'change-temporary-password-page'
     */
    get id () {
        return 'change-temporary-password-page';
    }

    /**
     * @property {string} template - Id of template to render.
     * @readonly
     * @default '#change-temporary-password-template'
     */
    get template () {
        return '#change-temporary-password-template';
    }

    resolve () {
        return super.resolve()
        .then(() => {
            // Initialize
            this.cacheTemplate(this.templateSrc, 'ChangeTemporaryPasswordView');
            this.fetchData(['Subjects'], () => {
                // @todo remove
            });
        });
    }

    /**
     * Navigates back to the toolbox view.
     */
    back () {
        this.navigate('login', true);
        localStorage.removeItem('PHT_changePassword');
        localStorage.removeItem('Reset_Password_Secret_Question');
    }

    /**
     * Renders the view.
     * @returns {Object} this ({@link LF.View.ChangeTemporaryPasswordView})
     */
    render () {
        return Q()
        .then(() => {
            if (LF.StudyDesign.showPasswordRules) {
                return this.i18n('PASSWORD_RULES')
                .then(text => templates.display('DEFAULT:PasswordRules', { text }));
            }
            return Q();
        })
        .then((passwordRules = '') => {
            return this.buildHTML({
                key: this.key,
                inputType: this.getRoleInputType(this.user && this.user.get('role')),
                passwordRules
            }, true);
        });
    }

    /**
     * Transmit all records to the web service once the password is saved locally.
     * @example this.onPasswordSaved();
     */
    onPasswordSaved () {
        let user = this.user;

        logger.operational(`Password set by ${user.get('role')} ${user.get('username')}`, {
            username: user.get('username'),
            role: user.get('role')
        });

        LF.security.pauseSessionTimeOut();
        try {
            if (LF.StudyDesign.askSecurityQuestion) {
                localStorage.setItem('Reset_Password_Secret_Question', true);
                this.navigate('reset_secret_question', true);
            } else {
                localStorage.removeItem('PHT_changePassword');

                user.changeCredentials(Data.changeCredentialParams)
                .then(() => LF.security.login(this.user.id, $('#txtNewPassword').val()))
                .then(() => {
                    Data.changeCredentialParams = undefined;
                    this.navigate('dashboard', true);
                });
            }
        } finally {
            LF.security.restartSessionTimeOut();
        }
    }
}

window.LF.View.ChangeTemporaryPasswordView = ChangeTemporaryPasswordView;
