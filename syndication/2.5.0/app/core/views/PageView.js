import Data from 'core/Data';
import Sound from 'core/classes/Sound';
import Spinner from 'core/Spinner';
import { MessageRepo } from 'core/Notify';
import { confirm } from 'core/actions/confirm';
import notify from 'core/actions/notify';
import Logger from 'core/Logger';
import ComponentView from 'core/views/ComponentView';

// Imported so LF.displayStrings is defined.
import 'core/collections/Languages';

let { history } = Backbone,
    { defer } = Q;

const logger = new Logger('PageView');

export default class PageView extends ComponentView {
    get id () {
        return this._id || 'page';
    }

    set id (value) {
        this._id = value;
    }

    get tagName () {
        return this._tagName || 'div';
    }

    set tagName (value) {
        this._tagName = value;
    }

    // noinspection JSDuplicatedDeclaration
    /**
     * @param {Backbone.ViewOptions=} options Backbone view options
     */
    constructor (options) {
        super(options);

        /**
         * The default pageType of a page view.
         * @readonly
         * @type String
         * @default 'page'
         */
        this.pageType = 'page';

        /**
         * A list of events applied to the view's DOM.
         * @type Object
         */
        this.events = this.events || {};

        /**
         * Attributes for the page view
         * @enum {String}
         * @default <ul>
         *     <li>data-theme: a</li>
         *     <li>lang: en-US</li>
         *     <li>dir: ltr</li>
         *     <li>screen: auto-gen</li>
         * </ul>
         */
        this.attributes = {
            /** screen dimension */
            screen: `${screen.width}x${screen.height}`
        };

        /**
         * Template content to render
         * @type String
         * @readonly
         * @default ''
         */
        this.templateContent = '';

        /**
         * Responsible for finding and playing sounds.
         * @readonly
         * @type Object
         * @default new {@link LF.Class.Sound}
         */
        this.sound = new Sound();

        this.spinner = Spinner;

        this.data = Data;

        /**
         * Scoped notify action
         * @type notify
         */
        this.notify = notify;

        /**
         * Scoped confirm action
         * @type confirm
         */
        this.confirm = confirm;

        // This will log the name of the actual view that is being loaded, not this base class
        // However, the logger name will be this base class.
        logger.traceSection(this.constructor.name);

        /**
         * @property {string} key A randomly generated alphanumeric key generated as each page is constructed.
         */
        this.key = (() => {
            let chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz',
                length = chars.length,
                output = '';

            for (let i = 0; i < 10; i += 1) {
                let value = Math.floor(Math.random() * length);
                output += chars.substring(value, value + 1);
            }

            return output;
        })();
    }

    toString () {
        return `${this.constructor.name}:${this.id}`;
    }

    // noinspection JSDuplicatedDeclaration,JSCheckFunctionSignatures
    /**
     * Render the page. Unlike the Backbone.View method it overrides, this
     * method is expected to return a Promise which resolves when the page
     * is rendered.
     *
     * @abstract
     * @returns {Q.Promise<*>}
     */
    render () {
        throw new Error('must be implemented by subclass!');
    }

    /**
     * Remove the view and prevent zombies(living dead event listeners).
     * @example this.close();
     */
    close () {
        $('#noty_top_layout_container').css('display', 'none');
        this.undelegateEvents();
        this.remove();
    }

    // noinspection JSUnusedLocalSymbols
    /**
     * Disables the entire view and displays a message.
     * @returns {Q.Promise<void>}
     */
    displayMessage () {
        this.spinner.show();
        return Q();
    }

    // noinspection JSUnusedLocalSymbols
    /**
     *
     * Removes the loading message and enables the view.
     * @returns {Q.Promise<void>}
     */
    removeMessage () {
        this.spinner.hide();
        return Q();
    }

    /**
     * Fetches any data required for the view to render.
     * @param {String[]} collections An array of strings listing the collections to fetch.
     * @param {Function} onSuccess A callback function invoked upon success.
     * @returns {Q.Promise<void>}
     * @example
     * this.fetchData([
     *     'Subjects'
     * ], function () {
     *     // Data fetched...
     * });
     */
    fetchData (collections, onSuccess) {
        let deferred = defer(),
            length = collections.length,
            step = (counter) => {
                let item = collections[counter],
                    tempModel;

                // Define a new model based on the model type and data.
                tempModel = new LF.Collection[item]();

                // noinspection JSUnusedLocalSymbols
                tempModel.fetch()
                .then(() => {
                    Data[item] = tempModel;

                    if ((counter + 1) === length) {
                        (onSuccess || $.noop)();
                        deferred.resolve();
                    } else {
                        step(counter + 1);
                    }
                });
            };

        // Begin stepping through the data sets.
        step(0);

        return deferred.promise;
    }

    /**
     * Retry the transmission action based on the user input.
     * @param {Object} params An object that contains the message and connectivity status.
     * @param {Function} callback A callback function to continue remaining actions.
     * @returns {Q.Promise<void>}
     */
    transmitRetry (params, callback = $.noop) {
        const { Dialog } = MessageRepo;
        let resume = (result) => {
            if (!result) {
                callback(false);
                return Q(false);
            }

            // Let the user know that we are retrying by making sure that
            // the please wait message displays for at least 1 sec
            return this.spinner.show()
            .delay(1000)
            .then(() => {
                return this.triggerTransmitRule();
            })
            .then(() => this.spinner.hide())
            .then(() => callback());
        };

        LF.security.restartSessionTimeOut();

        if (params.isOnline) {
            // if the device has connectivity, but study was not reachable
            // noinspection JSValidateTypes
            return this.notify({
                dialog: Dialog && Dialog[params.message],
                options: { httpRespCode: params.httpRespCode || '' }
            })
            .then(callback);
        }
        const dialog = (() => {
            if (Dialog && Dialog[`CONFIRM_${params.message}`]) {
                return Dialog[`CONFIRM_${params.message}`];
            }
            return Dialog && Dialog[params.message];
        })();

            // Device has no internet connection
            // noinspection JSValidateTypes
        return this.confirm({
            dialog,
            options: { httpRespCode: params.httpRespCode || '' }
        })
        .then(res => resume(res));
    }

    /**
     * Calls the transmit rule.
     * Abstract method. Expected to be overriden by the sub class when needed.
     */
    triggerTransmitRule () {
        // Do nothing
    }

    /**
     * Display a view.
     * @example
     * this.page();
     */
    page () {
        let $page = $('#application');

        $page.html(this.el);
        $page = null;
    }

    renderIntoView () {
        this.page();
    }

    /**
     * Set the title of the window.
     * @param {string} title The title to set.
     */
    title (title) {
        document.title = title;
    }

    /**
     * An alias for Backbone.history.navigate (or LF.router.navigate).
     * @param {string} fragment See Backbone docs.
     * @param {object|boolean} options See Backbone docs.
     * @param {object} flashParams The parameters to be passed to the navigated controller function
     * @example this.navigate(login, true);
     */
    navigate (fragment, options = true, flashParams) {
        LF.router.flash(flashParams).navigate(fragment, options);
    }

    /**
     * An alias for Backbone.history.loadUrl(). Essentially reloads the route without navigating away, or refreshing the page.
     * @example this.reload();
     */
    reload () {
        history.loadUrl(undefined);
    }

    /**
     * Default resolve method for in cases where a view doesn't have one defined.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return Q();
    }


    /**
     * Clear an input of any validation state.
     * @param {string|object} input Either a string selector, or a jQuery object of the input to clear state.
     * @returns {object} The cleared input element.
     */
    clearInputState (input) {
        let $input = this.getElement(input),
            $parent = this.getValidParent($input),
            $icon = $input.siblings('.form-control-feedback');

        // If the input has feedback...
        if ($parent.hasClass('has-feedback')) {
            // Remove the icon.
            $icon.remove();

            // Remove any state-related classes from the input.
            $parent.removeClass('has-feedback has-success has-warning has-error');
        }

        $parent = null;
        $icon = null;

        return $input;
    }

    /**
     * Validate an input element within the view.
     * @param {string|object} input Either a string selector, or a jQuery object of the input to validate.
     * @param {RegExp} expression A regular expression used to validate the input element's value.
     * @param {string} errorKey A translation key to display in an invalid state.
     * @returns {boolean} True if valid, false if not.
     */
    validateInput (input, expression, errorKey) {
        let $input = this.getElement(input),
            value = $input.val();

        $input = null;

        // Test the value from the input against the provided expression.
        if (expression.test(value)) {
            // The input is valid...
            this.inputSuccess(input);

            if (errorKey != null) {
                // Remove any existing help text.
                this.removeHelpText(input);
            }

            return true;
        }

        // The field is not valid...
        this.inputError(input);

        if (errorKey != null) {
            // If there is an error key provided, display help text below the input element.
            this.addHelpText(input, errorKey);
        }

        return false;
    }

    /**
     * Add help text to an input element.
     * @param {string|object} input Either a string selector, or a jQuery object of the input to add the help text to.
     * @param {string} key A translation key for the help text to add.
     * @returns {object} The input the help text was added to.
     */
    addHelpText (input, key) {
        let $input = this.getElement(input),
            $parent = this.getValidParent($input),
            exists = $parent.has('.help-block').length;

        if (!exists) {
            this.i18n(key)
            .then((text) => {
                $parent.append(`<p class="help-block">${text}</p>`);
                $parent = null;
            })
            .done();
        } else {
            $parent = null;
        }

        return $input;
    }

    /**
     * Remove help text from an input element.
     * @param {string|object} input Either a string selector, or a jQuery object of the input to remove the help text from.
     * @returns {object} The input the help text was removed from.
     */
    removeHelpText (input) {
        let $input = typeof input === 'string' ? this.$(input) : input,
            $parent = this.getValidParent($input);

        // Remove any prior text.
        $parent.children('.help-block').remove();
        $parent = null;

        return $input;
    }

    /**
     * Select correct parent for target element
     * @param  {string|object} input Either a string selector, or a jQuery object of the input whose parent is needed.
     * @return {object} The parent object of the input element
     */
    getValidParent (input) {
        let $input = typeof input === 'string' ? this.$(input) : input,
            $parent = $input.parent('.input-group');

        return $parent.length ? $parent : $input.closest('.form-group');
    }

    /**
     * Set an input element to a valid state.
     * @param {string|object} input Either a string selector, or a jQuery object of the input to set to a valid state.
     * @returns {object} The provided input element.
     */
    inputSuccess (input) {
        let $input = typeof input === 'string' ? this.$(input) : input,
            $parent = this.getValidParent($input),
            icon = '<span class="glyphicon glyphicon-ok-sign form-control-feedback" aria-hidden="true"></span>';

        this.clearInputState(input);
        $parent.addClass('has-feedback has-success').append(icon);
        $parent = null;

        return $input;
    }

    /**
     * Set an input element to an invalid state.
     * @param {string|object} input Either a string selector, or a jQuery object of the input to set to an invalid state.
     * @returns {object} The provided input element.
     */
    inputError (input) {
        let $input = this.getElement(input),
            $parent = this.getValidParent($input),
            icon = '<span class="glyphicon glyphicon-minus-sign form-control-feedback" aria-hidden="true"></span>';

        this.clearInputState(input);
        $parent.addClass('has-feedback has-error').append(icon);
        $parent = null;

        return $input;
    }

    /**
     * Displays an error banner with the error icon next to input text box.
     * @param {String} elementName The name of the element to show the error icon, preceded with a #
     * @param {String} errorKey The resource key for the error string that will be displayed on the banner
     * @return {Q.Promise<null>}
     */
    showInputError (elementName, errorKey) {
        this.inputError(elementName);
        logger.error(errorKey);

        return Q()
        .then(() => {
            const banner = (() => {
                if (MessageRepo.Banner[errorKey]) {
                    return MessageRepo.Banner[errorKey];
                }
                return MessageRepo.Banner[`INPUT_ERROR_${errorKey}`];
            })();

            return MessageRepo.display(banner);
        })
        .catch((err) => {
            logger.error('Error displaying message', err);
            return Q.reject(err);
        });
    }

    /**
     * We don't want the soft-keyboard 'go' or 'enter' to do an html submit of the page!
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     * @returns {boolean}
     */
    skipSubmit (e) {
        e.preventDefault();
        return false;
    }

    /**
     * Hides keyboard from the active element, when enter key is pressed.
     * @param {Event} evt Event data.
     */
    hideKeyboardOnEnter (evt) {
        if (evt.keyCode === 13 && document.activeElement) {
            if (document.activeElement.tagName === 'INPUT') {
                document.activeElement.setSelectionRange(0, 0);
            }

            document.activeElement.blur();
        }
    }

    /**
     * Gets the preferred type of input for a current role.
     * @param {String} roleId The role to get the type for.
     * @example this.getRoleInputType('subject');
     * @returns {String} Input type string
     */
    getRoleInputType (roleId = 'subject') {
        if (LF.Wrapper.isBrowser()) {
            return 'password';
        }
        let roleSettings = LF.StudyDesign.roles.get(roleId);
        return roleSettings ? roleSettings.get('passwordInputType') : 'password';
    }

    /**
     * Build the view's HTML.
     * @param {object} dynamicStrings Any strings that are not to be translated, but are pulled from other sources. ex. A site's ID.
     * @param {boolean} renderToView If set to true, calls page() to render the view to the DOM.
     * @returns {Q.Promise} The translated strings.
     */
    buildHTML (dynamicStrings = {}, renderToView = false) {
        // DE15951 - Extend default events onto the current event list.
        this.events = _.extend(this.events, {
            'keypress input': 'hideKeyboardOnEnter',
            submit: 'skipSubmit'
        });

        return super.buildHTML(dynamicStrings, renderToView);
    }
}

/*
 * Due to ES6's spec, you cannot set properties outside of the constructor!
 * This creates a condition, where class properties will be overwritten by the super class.
 * Since ES6 classes are syntactic sugar onto of prototypal inheritance, we still have direct access to the prototype chain...
 * This isn't the cleanest solution, but it provides what we need.
 */

window.LF.View.PageView = PageView;
