import ModalView from './ModalView';

export default class NotifyView extends ModalView {
    constructor (options = {}) {
        super({
            onHide: options.onHide || $.noop,

            /**
             * @property {Object} events A list of DOM events.
             * @readonly
             */
            events: {
                'click #ok': 'resolve',
                'click #close': 'resolve',
                'hidden.bs.modal': 'teardown'
            }
        });

        /**
         * @property {string} template A selector that points to the template, within the DOM, to render.
         */
        this.template = '#notify-template';

        /**
         * Creates an internal, de-bounced show method to prevent the same notification from being rendered twice.
         */
        this._show = _.debounce(opts => super.show(opts), 500);
    }

    /**
     * Show the notification dialog.
     * @param {Object} options Strings to pass into the modal's template.
     * @param {string} options.header The header text to display.
     * @param {string} options.body The body text to display.
     * @param {string} options.ok The OK button text.
     * @param {string} options.type The type of notification to display.
     * @returns {Q.Promise<void>}
     */
    show (options) {
        this.deferred = Q.defer();

        this._show(options);

        return this.deferred.promise;
    }

    /**
     * Invoked when the user accepts the confirmation.
     */
    resolve () {
        this.deferred.resolve(true);
        this.hide();
    }
}
