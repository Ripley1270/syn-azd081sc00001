import PageView from 'core/views/PageView';
import COOL from 'core/COOL';
import Logs from 'core/collections/Logs';
import * as Utilities from 'core/utilities/index';

export default class ConsoleLogView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {string} id - The ID of template to render.
         * @readonly
         * @default '#console-log-tpl'
         */
        this.template = '#console-log-tpl';

        /**
         * @property {Object} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #back': 'back'
        };

        /**
         * @property {Object} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            title: 'CONSOLE_LOG',
            back: 'BACK'
        };
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'console-log'
     */
    get id () {
        return 'console-log';
    }

    /**
     *
     * @param @logs - Array of log objects
     * @param @container - Data table target container
     * @returns Data Table with Log Data
     */

    logViewer (logs, container) {
        let pageID = this.el.id,

            /**
             *
             * @param pageId - ID of the current page
             * @param dtContainer - DOM element containing the DataTable
             */

            calculateScrollYHeight = (pageId, dtContainer) => {
                let navBar = $(`#${pageId} > nav.navbar`).outerHeight(),
                    tableSearchButtonsHeight = $('#table-search-buttons').outerHeight(),
                    dtWrapper = $(`#${dtContainer[0].id}_wrapper`),
                    dataTables_scrollHeadTable = dtWrapper.find('.dataTables_scrollHead table.dataTable').outerHeight(),
                    tablePaginationRow = dtWrapper.find('.dataTables_paginate').outerHeight(),
                    totalToSubtractFromWindowHeight =
                        navBar + tableSearchButtonsHeight + dataTables_scrollHeadTable + tablePaginationRow + 30;

                return $(window).height() - totalToSubtractFromWindowHeight;
            },

            /*
             * Adjusts the height of the datatable so that it can be contained within the page.
             * @adjustableContainer - DOM element which is the outer container of the datatable.
             * @scrollHeight - number for height
             **/
            adjustScrollYHeight = (adjustableContainer, scrollHeight) => {
                let dataTables_scrollBody = adjustableContainer.parents('.dataTables_wrapper')
                    .find('.dataTables_scrollBody');

                dataTables_scrollBody.css('max-height', scrollHeight);
            };

        let head = `<thead>
                        <tr>                        
                            <th class="all">Log Message</th>
                            <th class="all">Time</th>
                            <th>Level</th>
                            <th>Location</th>
                        </tr>
                    </thead>`;
        container.append(head);

        logs.map((log) => {
            let logDate = new Date(log.attributes.clientTime);
            let formattedDate = moment(logDate).format('DDMMMYYYY, HH:mm:ss');
            let message = log.attributes.message;

            // let id = log.attributes.id;
            let level = log.attributes.level;
            let loc = log.attributes.location;
            let row = `<tr>
                        <td>${message}</td>
                        <td>${formattedDate}</td>
                        <td>${level}</td>
                        <td>${loc}</td>
                      </tr>`;
            return container.append(row);
        });

        let ConsoleLogTable = container.DataTable({
            dom: '<"#dt-head"<"dt-display"l><"#table-search-buttons"fr>>t<i><p>',

            // Order by date descending
            order: [[1, 'desc']],
            language: {
                lengthMenu: 'Display _MENU_ log entries',
                zeroRecords: 'No logs were found',
                info: 'Showing _START_ to _END_ log entries',
                infoEmpty: 'No logs available',
                infoFiltered: '</br>(filtered from _MAX_ total log entries)',
                paginate: {
                    next: '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>',
                    previous: '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>'
                }
            },
            buttons: [
                // The Export Excel is not working on Android/mobile devices and will need to
                // be postponed, until I can create a fix.
                // { extend: 'excelHtml5', text: '<i class="fa fa-lg fa-file-excel-o"></i>' },
                { extend: 'colvis', text: '<i class="fa fa-lg fa-columns"></i>' }
            ],
            responsive: true,
            scrollY: $(window).height() * 0.6,
            scrollCollapse: true,
            fnDrawCallback: (oSettings) => {
                /*
                 * If there are less records(log entries) than there is to show on the page
                 * hide the pagination.
                 */
                if (oSettings._iDisplayLength > oSettings.fnRecordsDisplay()) {
                    $(oSettings.nTableWrapper).find('.dataTables_paginate').hide();
                } else {
                    $(oSettings.nTableWrapper).find('.dataTables_paginate').show();
                }
            },
            columnDefs: [
                { width: '35%', targets: 0 }
            ]
        });

        /*
         * Handle closing the column visibility modal when navigating
         * outside page, usually on session timeout.
         */
        $(window).one('hashchange', () => {
            let dtBackground = $('.dt-button-background');
            if (dtBackground.length > 0) {
                dtBackground.click();
            }

            /*
             *   DE23504 - this prevents the fixed header of the DataTable
             *   from being created when leaving the page.
             */
            ConsoleLogTable.fixedHeader.disable();
        });

        adjustScrollYHeight(container, calculateScrollYHeight(pageID, container));

        $(window).resize(() => {
            adjustScrollYHeight(container, calculateScrollYHeight(pageID, container));
        });
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise<void>}
     */
    render () {
        Utilities.setLanguage('en-US', 'ltr');
        return this.buildHTML({}, true)
            .then(() => Logs.fetchCollection())
            .then((logs) => {
                let container = $('#user-console-log');
                this.logViewer(logs, container);
            });
    }
}
COOL.add('ConsoleLogView', ConsoleLogView);
