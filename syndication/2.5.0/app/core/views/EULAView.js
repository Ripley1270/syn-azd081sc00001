import PageView from 'core/views/PageView';
import Logger from 'core/Logger';
import COOL from 'core/COOL';
import EULA from 'core/classes/EULA';

// eslint-disable-next-line no-unused-vars
const logger = new Logger('EULAView');

// Default event handler.
const dfltEvent = { preventDefault: $.noop };

/**
 * View for EULA page, user's acceptance is required for activation.
 * @class EULAView
 * @extends PageView
 */
export default class EULAView extends COOL.getClass('PageView', PageView) {
    constructor (options) {
        super(options);

        /**
         * @property {Object<string,string>} templateStrings - Strings to fetch prior to render.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            title: 'EULA',
            text: 'EULA_TEXT',
            accept: 'ACCEPT',
            decline: 'DECLINE'
        };

        /**
         * @property {Object<string,string>} events - End User License Agreements view's event list.
         * @readonly
         */
        this.events = {
            'click #back': 'back',
            'click #next': 'next'
        };
    }

    /**
     * @property {string} template - ID of template to render.
     * @readonly
     * @default '#end-user-license-template'
     */
    get template () {
        return '#end-user-license-template';
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'end-user-license-view'
     */
    get id () {
        return 'end-user-license-view';
    }

    /**
     * Navigate to the next view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    next (evt = dfltEvent) {
        evt.preventDefault();

        EULA.accept();
    }

    /**
     * Navigate back to the previous view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    back (evt = dfltEvent) {
        evt.preventDefault();

        EULA.decline();
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise<Object>}
     */
    render () {
        return this.buildHTML({}, true).tap(() => {
            window.scrollTo(0, 0);

            // This will ensure they any extended events are delegated.
            this.delegateEvents();
        });
    }
}

COOL.add('EULAView', EULAView);
