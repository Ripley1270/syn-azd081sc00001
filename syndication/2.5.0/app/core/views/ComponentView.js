import Logger from 'core/Logger';
import Transmissions from 'core/collections/Transmissions';
import { isLogPad } from 'core/utilities';

let templateCache = {};

const logger = new Logger('ComponentView');


/**
 * Base class for any component (PageView, WidgetBase, etc.).
 * Contains shared helper functions and things that apply to all component views.
 */
export default class ComponentView extends Backbone.View {
    get id () {
        return this._id || 'component';
    }

    set id (value) {
        this._id = value;
    }

    get tagName () {
        return this._tagName || 'div';
    }

    set tagName (value) {
        this._tagName = value;
    }

    /**
     * Cache the template content in templateContent
     * using the View name as the key in the caching object.
     * @example this.cacheTemplate();
     * @param {string} source The ID associated with the template.
     * @param {string} view The name of the View associated with the template.
     */
    cacheTemplate (source, view) {
        !!LF.cachedTemplates || (LF.cachedTemplates = {});
        !!LF.cachedTemplates[view] || (LF.cachedTemplates[view] = $(source).html());
        this.templateContent = LF.cachedTemplates[view];
    }

        /**
     * Returns a jQuery object based on the provided selector.
     * @param {string|object} selector The selector of the element to get.
     * @returns {Object}
     */
    getElement (selector) {
        return typeof selector === 'string' ? this.$(selector) : selector;
    }

    /**
     * Enable an element (scoped to the view).
     * @param {string|object} selector Either a string selector, or a jQuery object, of the element to enable.
     * @returns {object} The enabled element.
     */
    enableElement (selector) {
        let $ele = this.getElement(selector);
        return $ele.removeAttr('disabled').removeAttr('readonly');
    }

    /**
     * Disable an element (scoped to the view).
     * @param {string|object} selector Either a string selector, or a jQuery object of the element to disable.
     * @returns {object} The disabled element.
     */
    disableElement (selector) {
        let $ele = this.getElement(selector);
        return $ele.attr('disabled', 'disabled').attr('readonly', 'readonly');
    }

    /**
     * Determines if an element is disabled.
     * @param {string|object} selector Either a string selector, or a jQuery object of the element to check.
     * @returns {Boolean}
     */
    isDisabled (selector) {
        let $ele = this.getElement(selector);

        return !!$ele.attr('disabled');
    }

    /**
     * A shortcut for enableElement.
     * @param {string|object} button Either a string selector, or a jQuery object of the button to enable.
     * @returns {object} The enabled button.
     */
    enableButton (button) {
        return this.enableElement(button);
    }

    /**
     * A shortcut for disableButton.
     * @param {string|object} button Either a string selector, or a jQuery object of the button to disable.
     * @returns {object} The disabled button.
     */
    disableButton (button) {
        return this.disableElement(button);
    }

    /**
     * Show an element in the view.
     * @param {string|object} selector Either a string selector, or a jQuery object of the element to show.
     * @returns {JQuery} The shown element.
     */
    showElement (selector) {
        let $ele = this.getElement(selector);

        return $ele.removeClass('hidden');
    }

    /**
     * Hide an element in the view.
     * @param {string|object} selector Either a string selector, or a jQuery object of the element to hide.
     * @returns {object} The hidden element.
     */
    hideElement (selector) {
        let $ele = this.getElement(selector);

        return $ele.addClass('hidden');
    }

    /**
     * Find and return an underscore template.
     * @param {string=} selector The selector for the template to fetch and wrap.
     * @returns {_.TemplateExecutor}
     */
    getTemplate (selector) {
        let pattern = selector;
        pattern = pattern || this.template;

        // If there is no selector, throw an error.
        if (!pattern) {
            throw new Error('No template selector provided.');
        }

        // If the template is cached, use it.
        if (templateCache[pattern]) {
            return templateCache[pattern];

        // Otherwise, find and create the template.
        }
        let template = _.template($(pattern).html());
        templateCache[pattern] = template;
        return template;
    }

    /**
     * Clears the private template cache. Use more for unit testing.
     */
    clearTemplateCache () {
        templateCache = {};
    }

    /**
     * Clear any generated shortcut selectors. Called on view removal.
     * @example
     * this.clearSelectors();
     */
    clearSelectors () {
        let selectors = this.selectors || {};
        _(selectors).forEach((value, name) => {
            this[`$${name}`] = null;
        });
    }

    /**
     * An alias for LF.strings.display. See {@link core/collections/Languages Languages} for details.
     * @param {Object|string} strings The translations keys to translate.
     * @param {function} callback A callback function invoked when the strings have been translated.
     * @param {Object} options Options passed into the translation.
     * @returns {Q.Promise}
     */
    i18n (strings, callback = $.noop, options = undefined) {
        return LF.strings.display(strings, callback, options);
    }

    /**
     * Build the view's HTML.
     * @param {object} dynamicStrings Any strings that are not to be translated, but are pulled from other sources. ex. A site's ID.
     * @param {boolean} renderToView If set to true, calls page() to render the view to the DOM.
     * @returns {Q.Promise} The translated strings.
     */
    buildHTML (dynamicStrings = {}, renderToView = false) {
        let template = this.getTemplate();

        this.templateStrings = this.templateStrings || {};

        // Before constructing the view, remove any existing event listeners.
        this.undelegateEvents();

        return this.i18n(this.templateStrings)
        .then((translated) => {
            let strings = _.extend(translated, dynamicStrings, { key: this.key }),
                compiled;

            // Try and compile the template with the translated and dynamic strings.
            try {
                compiled = template(strings);
            } catch (e) {
                logger.error(e);
                return Q.reject(e.message);
            }

            // Pass the combined strings into the template, and set it as the view's HTML.
            this.$el.html(compiled);

            // Build any predefined selectors.
            this.buildSelectors();

            // If renderToView is set to true, render the view to the page.
            if (renderToView) {
                this.renderIntoView();
            }

            // Ensure that all defined event listeners are once again enabled.
            this.delegateEvents();

            return strings;
        });
    }

    /**
     * Generate shortcut selectors from this.selectors. Called with buildHTML().
     * @example
     * this.selectors = { submit : '#submit' };
     * this.buildSelectors();
     * this.$submit.attr('disabled', 'disabled');
     */
    buildSelectors () {
        let selectors = this.selectors || {};
        _(selectors).forEach((value, name) => {
            this[`$${name}`] = this.$(value);
        });
    }

    renderIntoView () {
        throw new Error('renderIntoView not implemented by ComponentView subclass');
    }

    // noinspection JSDuplicatedDeclaration,JSCheckFunctionSignatures
    /**
     * @returns {Backbone.View<TModel>}
     * Remove the view from the DOM.
     */
    remove () {
        this.clearSelectors();
        this.undelegateEvents();
        return super.remove();
    }

    /**
     * Gets the number of pending reports that should be displayed
     * @param  {Object} [filter] - transmission items to be included or excluded from display
     * @example getPendingReportCount({
     *      exclude: ['resetPassword'],
     *      include ['transmitEditPatient', 'updateUserCredentials']
     *  })
     * @return {Q.Promise<number>}
     */
    getPendingReportCount (filter = {}) {
        let transmissionTypes = {
                core: [],
                logpad: [
                    'resetPassword',
                    'resetSecretQuestion',
                    'transmitQuestionnaire',
                    'updateUserCredentials',
                    'transmitUserData'
                ],
                sitepad: [
                    'transmitUserData',
                    'transmitSubjectAssignment',
                    'transmitEditPatient',
                    'transmitQuestionnaire',
                    'createFirstUser',
                    'sitepadPasswordSecretQuestion',
                    'transmitResetCredentials',
                    'transmitResetPassword',
                    'updateUserCredentials',
                    'transmitUserEdit'
                ]
            },

            filterTransmission = (models) => {
                return _.reject(models, (model) => {
                    return model.get('hideCount') === true;
                });
            },

            collection = new Transmissions(),

            getCount = (models) => {
                return _.countBy(models, model => model.get('method'));
            },

            addUpCount = (param) => {
                let total = 0,
                    typesToDisplay = transmissionTypes[isLogPad() ? 'logpad' : 'sitepad'],
                    filterTypesToDisplay = (types) => {
                        let transmissionKeys = Object.keys(LF.Transmit),
                            filterOut = filter.exclude,
                            filterIn = filter.include,
                            filterTypes = types,

                            // performs the filtering of transmission
                            execute = (transmissionFilter, op) => {
                                let testValidKeys = (filtr) => {
                                        let validKeys = _.intersection(transmissionKeys, filtr),
                                            invalidKeys = validKeys.length === filtr.length ? [] : _.difference(filtr, validKeys);
                                        return { validKeys, invalidKeys };
                                    },
                                    keys = testValidKeys(transmissionFilter);

                                // add or remove valid transmission types from the types to display
                                if (keys.validKeys && keys.validKeys.length) {
                                    filterTypes = (op === 'out' ? _.difference : _.union)(filterTypes, keys.validKeys);
                                }

                                // log error if invalid transmission type was passed into filter object
                                if (keys.invalidKeys && keys.invalidKeys.length) {
                                    logger.error('Invalid transmission item(s)', keys.invalidKeys);
                                }
                            };


                        if (filterOut && !_.isEmpty(filterOut)) {
                            execute(filterOut, 'out');
                        }

                        if (filterIn && !_.isEmpty(filterIn)) {
                            execute(filterIn, 'in');
                        }

                        return filterTypes;
                    };

                // if filters were passed in, run this to perform filtering
                if (!_.isEmpty(filter)) {
                    typesToDisplay = filterTypesToDisplay(typesToDisplay);
                }

                _.each(typesToDisplay, (type) => {
                    total = param[type] ? total + param[type] : total;
                });

                return total;
            };

        return collection.fetch()
        .then(() => filterTransmission(collection.models))
        .then(models => getCount(models))
        .then(param => addUpCount(param))
        .catch(err => logger.error('Error getting pending transmission count', err));
    }
}
