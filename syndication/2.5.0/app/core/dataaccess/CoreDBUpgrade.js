import Subjects from 'core/collections/Subjects';

/**
 * The list of study upgrade configurations.
 * You must include custom function per version upgrade.
 * Supports use of callback or returning a promise.
 * @example
 * LF.CoreDBUpgrade = [{
 *     version: 1,
 *     upgradeFunction (callback) {
 *         callback(isCompleted);
 *     }
 *  }, {
 *     version: 2.5,
 *     upgradeFunction () {
 *         return Q.delay(300)
 *         .then(() => true);
 *     }
 * }];
 * @type {*[]}
 */
const CoreDBUpgrade = [{
    // Let's assign the release version as the db version. It will make it easier to understand.
    version: 2.5,
    upgradeFunction () {
        // Fetch all subjects and save again. This will encrypt the fields that were not previously encrypted.
        return Subjects.fetchCollection({ onExceptionKeepOriginal: true })
        .then((subjects) => {
            return Q.all(subjects.map(subject => subject.save()));
        });
    }
}];

LF.CoreDBUpgrade = CoreDBUpgrade;
export default CoreDBUpgrade;
