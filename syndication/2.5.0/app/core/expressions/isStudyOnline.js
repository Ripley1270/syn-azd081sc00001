import ELF from '../ELF';
import Spinner from 'core/Spinner';
import COOL from 'core/COOL';
import WebService from 'core/classes/WebService';

/**
 * Check if we can get a response from the study API for getSubjectActive
 * @memberOf ELF.expressions
 * @param {function} callback A callback function invoked upon completion.
 * @returns {Q.Promise<void>}
 */
function isStudyOnline (callback = $.noop) {
    // TODO: Moved this function from utilities as is. It will be refactored in the project's main stream.
    // @todo check xhr status...
    // We don't really care what the response is, only that there is one.
    return COOL.new('WebService', WebService).getSubjectActive(1)
    .then(() => callback(true))
    .catch(err => callback(false, err));
}

/**
 * Determines if the study is online.
 * @param {Object} input - ELF context.
 * @param {function} done - Invoked upon completion of the expression.
 */
export function studyOnline (input, done) {
    Spinner.show()
    .then(() => {
        isStudyOnline((status, code) => {
            input.httpRespCode || (input.httpRespCode = code);

            Spinner.hide()
            .done();

            done(status);
        });
    })
    .done();
}

ELF.expression('isStudyOnline', studyOnline);
