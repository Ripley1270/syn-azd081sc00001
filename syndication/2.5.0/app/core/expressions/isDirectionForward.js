import ELF from 'core/ELF';

/**
 * @memberOf ELF.expressions
 * @method isDirectionForward
 * @description
 * Determines if the navigation direction is forward.
 * This should only be used in the context of a questionnaire,
 * when a QUESTIONNAIRE:Navigate event is triggered.
 * @param {object} input - Event data passed along by the triggered event.
 * @param {string} input.direction - The direction of the navigation. e.g. 'next'
 * @returns {Q.Promise<boolean>}
 * @example
 * trigger: 'QUESTIONNAIRE:Navigate/Activate_User/ACTIVATE_USER_S_1',
 * evaluate: 'isDirectionForward'
 */
export default function isDirectionForward (input) {
    return Q(input.direction === 'next');
}

ELF.expression('isDirectionForward', isDirectionForward);
