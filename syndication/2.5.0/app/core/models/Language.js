import Base from './Base';

/**
 * A model defining a language resource.
 * @class Language
 * @extends Base
 * @example
 * let model = new Language({
 *     namespace: 'DAILY',
 *     language: 'en',
 *     locale: 'US',
 *     resources: {
 *        QUESTION_1: 'What is your favorite color?'
 *     }
 * });
 */
export default class Language extends Base {
    /**
     * @property {Object} schema - The model's schema used for validation and storage construction.
     * @static
     * @readonly
     */
    static get schema () {
        return {
            // As its name implies, used to namespace the language resources.
            // Namespaces can point a Questionnaire's ID. e.g. 'DAILY'
            namespace: {
                type: String,
                required: true
            },

            // ISO 639-1 format. e.g. 'en'
            language: {
                type: String,
                required: true
            },

            // ISO 3166-1 alpha-2 format. e.g. 'US'
            locale: {
                type: String,
                required: false
            },

            // Used for display purposes.
            // e.g. 'English (US)'
            localized: {
                type: String,
                required: false
            },

            // A map of translation keys to translations.
            // Each key should be defined in all capital letters spaced with an underscore.
            // e.g. { "CANCEL_UPDATE": "Cancel Update" }
            resources: {
                type: Object,
                required: true
            },

            // Date/time format configurations.
            dates: {
                type: Object,
                required: false
            }
        };
    }
}

// @todo Phase out LF namespace...
window.LF.Model.Language = Language;
