import Base from './Base';

/**
 * A model that defines a screen configuration.
 * @class Models.Screen
 * @extends Models.Base
 * @description
 * Screens are direct children of a Questionnaire view and are configured in the study design.
 * Each screen can be configured with any number of questions for display.
 * Access to each screen can be limited by role via the accessRoles property.
 * @example
 * let model = Screen({
 *     id: 'DailyScreen01',
 *     questions: [{ id: 'DailyQuestion01': mandatory: true }],
 *     className: 'daily-screen',
 *     template: 'DAILY:Screen',
 *     excludeFromTranscriptionViaPaper: true,
 *     accessRoles: ['site']
 * });
 */
export default class Screen extends Base {
    /**
     * The model's schema used for validation.
     * @readonly
     * @type {Object}
     * @memberof Models.Screen
     * @property {Models.SchemaType} id - {@link Models.Screen.id}
     * @property {Models.SchemaType} className - {@link Models.Screen.className}
     * @property {Models.SchemaType} questions - {@link Models.Screen.questions}
     * @property {Models.SchemaType} disableBack - {@link Models.Screen.disableBack}
     * @property {Models.SchemaType} template - {@link Models.Screen.template}
     * @property {Models.SchemaType} excludeFromTranscriptionViaPaper - {@link Models.Screen.excludeFromTranscriptionViaPaper}
     * @property {Models.SchemaType} accessRoles - {@link Models.Screen.accessRoles}
     */
    static get schema () {
        return {
            /**
             * The unique identifier (ID) of the screen.
             * @typedef {string} Models.Screen.id
             * @example { id: 'WELCOME_S_01' }
             */
            id: {
                type: String,
                required: true
            },

            /**
             * A list of questions to display on the screen.
             * @typedef {Object<Array>} Models.Screen.questions
             * @property {string} Models.Screen.questions[].id - The ID of the question to display.
             * @property {boolean} Models.Screen.questions[].mandatory - Determines if the question can be skipped or not.
             * @property {boolean} [Models.Screen.questions[].excludeFromTranscriptionViaPaper] - Determines if the question should be excluded from transcription via paper.
             * @property {number} [Models.Screen.questions[].transcriptionViaPaperOrder] - Determines the display order of the question on the screen.
             * @example
             * questions: [{
             *      id: 'QUESTION_01',
             *      mandatory: true,
             *      excludeFromTranscriptionViaPaper: false,
             *      transcriptionViaPaperOrder: 1
             * }]
             */
            questions: {
                relationship: {
                    to: 'Questions',
                    via: 'id',
                    multiplicity: '1..*'
                }
            },

            /**
             * An optional class to append to the root DOM node of the screen view.
             * @typedef {string} Models.Screen.className
             * @example { className: 'welcome-s-1' }
             */
            className: {
                type: String,
                required: false
            },

            /**
             * If set to true, will hide the back button on the screen.
             * @typedef {boolean} Models.Screen.disableBack
             * @example { disableBack: true }
             */
            disableBack: {
                type: Boolean,
                required: false
            },

            /**
             * An optional template override.
             * @typedef {string} Models.Screen.template
             * @example { template: 'DEFAULT:Screen' }
             */
            template: {
                type: String,
                required: false
            },

            /**
             * Determines if the screen should be excluded from transcription via paper.
             * @typedef {boolean} Models.Screen.excludeFromTranscriptionViaPaper
             * @example { excludeFromTranscriptionViaPaper: true }
             */
            excludeFromTranscriptionViaPaper: {
                type: Boolean,
                required: false
            },

            /**
             * Configure the roles that have access to the questionnaire.
             * @typedef {string<Array>} Models.Screen.accessRoles
             * @example { accessRoles: ['site', 'subject'] }
             */
            accessRoles: {
                relationship: {
                    to: 'Roles',
                    multiplicity: '0..*'
                }
            }
        };
    }

    /**
     * @memberOf Models.Screen
     * @property {Object} defaults - Default attributes of the screen model.
     * @property {Models.Screen.className} defaults.className='screen' - {@link Models.Screen.className}
     * @property {Models.Screen.template} defaults.template='DEFAULT:Screen' - {@link Models.Screen.template}
     * @property {Models.Screen.excludeFromTranscriptionViaPaper} defaults.excludeFromTranscriptionViaPaper=false - {@link Models.Screen.excludeFromTranscriptionViaPaper}
     * @readonly
     */
    get defaults () {
        return {
            className: 'screen',
            template: 'DEFAULT:Screen',
            excludeFromTranscriptionViaPaper: false
        };
    }
}

window.LF.Model.Screen = Screen;
