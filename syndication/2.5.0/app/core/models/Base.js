// TODO: REMOVE THIS and all references to it
// will need to update many unit tests.
let unitTestCompatibility = () => {
    return window.LF && LF.StudyDesign && LF.StudyDesign.oldStyleValidationErrorMessages;
};

/**
 * @classdesc The base model for all LogPad LF Models. Not a standalone model.
 * @memberof Models
 * @extends Backbone.Model
 */
class Base extends Backbone.Model {
    /**
     * Creates an instance of Base.
     * @param {Object} options - The options object to pass to Backbone.model.
     * @example export default class ModelName extends Base { }
     */
    constructor (options) {
        super(options);

        /**
         * @member {string} Models.Base#modelName - The name to use in storage, taken from constructor.name.
         * @example class Visit extends Base{} // this.modelName = "Visit"
         */
        this.modelName = this.constructor.name;
    }

    /**
     * @property {string} idAttribute - The model's attribute to use as an ID.
     * @readonly
     */
    get idAttribute () {
        return 'id';
    }

    /**
     * @property {Object} schema - The model's schema used for validation. If not implemented by extending class this will throw an error.
     * @readonly
     * @static
     */
    static get schema () {
        throw new Error(`No schema defined for Model ${this.name}`);
    }

    /**
     * @property {Object} schema - The model's schema used for validation.
     * @readonly
     */
    get schema () {
        return this.constructor.schema;
    }

    // TODO: this is not being called from anywhere.
    // It should be called once per loaded Model class. How?
    static validatePropDef (propDef) {
        if (!propDef.name) {
            throw new Error('Property in schema is missing name');
        }
        if (propDef.relationship) {
            let relSpec = propDef.relationship;
            if (!relSpec.to) {
                throw new Error(`Property ${propDef.name}: relationship.to not specified`);
            }

            // Can't do this; introduces circular dependencies
            // if (!relSpec.to instanceof Base) {
            //     let toName = relSpec.to && relSpec.to.name ? relSpec.to.name : relSpec.to.toString();
            //     throw new Error(`Property ${propDef.name}: relationship.to is not a Base collection class: ${toName}`);
            // }
            try {
                Base.getMultiplicityBounds(relSpec);
            } catch (e) {
                throw new Error(`Property ${propDef.name}: ${e}`);
            }
        }
    }

    /**
     * Get the multiplicity bounds of a relationship.
     * @param {Models.Relationship} relSpec A relationship specification.
     * @returns {Number[]} e.g [0, Infinity]
     * @example
     * this.getMultiplicityBounds({
     *    to: 'Roles',
     *    multiplicity: '0..*',
     * });
     */
    static getMultiplicityBounds (relSpec) {
        // (Not sure this should be allowed, but if it is:)
        // If no multiplicity specified, then not bounded, same as '0..*'
        if (!relSpec.multiplicity) {
            return [0, Infinity];
        } else if (relSpec.multiplicity === '*') {
            // '*' is same as '0..*'
            return [0, Infinity];
        } else if (_.isNumber(relSpec.multiplicity)) {
            // special special case: n is same as 'n..n'
            return [relSpec.multiplicity, relSpec.multiplicity];
        }

        try {
            let [lowerBound, upperBound] = relSpec.multiplicity.split('..');

            lowerBound = parseInt(lowerBound, 10);
            if (upperBound !== undefined) {
                if (upperBound === '*') {
                    upperBound = Infinity;
                } else {
                    upperBound = parseInt(upperBound, 10);
                }
            } else {
                // 'n' same as as 'n..n'
                upperBound = lowerBound;
            }
            if (_.isNaN(lowerBound) || _.isNaN(upperBound)) {
                throw NaN;
            }
            if (upperBound < lowerBound) {
                // TODO - Should this throw an error?
                // eslint-disable-next-line no-throw-literal
                throw '>';
            }
            return [lowerBound, upperBound];
        } catch (e) {
            throw new Error(`Invalid multiplicity spec: ${relSpec.multiplicity}`, e);
        }
    }

    /**
     * Format own name as ModelName[modelId] or ModelName[<#cid>] if no id
     * @returns {String}
     */
    toString () {
        return `${this.modelName}[${this.id === undefined ? `<#${this.cid}>` : this.id}]`;
    }

    /**
     * Generates a display name for the property.
     * @param {String} propName The name of the property.
     * @returns {String}
     */
    displayName (propName) {
        return `${this.toString()}.${propName}`;
    }

    validateRelationshipSpec (propName, propDef, propVal, violations) {
        let displayName = this.displayName(propName),
            violation = (str) => {
                // a place to put a debugger breakpoint for all violations
                violations.push(str);
            };

        let relSpec = propDef.relationship;

        let [lowerBound, upperBound] = Base.getMultiplicityBounds(relSpec);
        if (upperBound === 1) {
            if (relSpec.embedded) {
                propDef.type = Object;
            } else {
                propDef.type = propDef.type || String;
            }
        } else {
            propDef.type = Array;
        }

        if (lowerBound !== 0) {
            propDef.required = true;
        }

        if (propVal == null) {
            if (propDef.required) {
                violation(`${displayName} (rel to ${relSpec.to}) is required (${relSpec.multiplicity}), but is ${propVal}`);
                return;
            }

            // either null or undefined are acceptable for all 0.. rels
            // but we need an empty array below.
            if (propDef.type === Array) {
                // eslint-disable-next-line no-param-reassign
                propVal = [];
            }
        }

        // Array props should already be array; for others, convert to
        // array of one element.
        let arrayizedVal = propDef.type === Array ? propVal : [propVal];
        let actualCount = arrayizedVal.length;
        if (actualCount < lowerBound) {
            violation(`${displayName} too few; minimum ${lowerBound}, actual ${actualCount}`);
        } else if (actualCount > upperBound) {
            violation(`${displayName} too many; maximum ${upperBound}, actual ${actualCount}`);
        }
    }

    /**
     * Validate the model based on the defined schema.
     * @param {object} attributes The model's attributes.
     * @returns {null|Error}
     */
    validate (attributes) {
        let violations = [],
            violation = (str) => {
                // a place to put a debugger breakpoint for all violations
                violations.push(str);
            };

        // _(attributes).each((attrValue, attrName) => {
        //     if (this.schema[attrName] == null && this.modelName !== 'Widget') {
        //         violation(`Attribute: ${attrName} not defined in schema.`);
        //     }
        // });

        // Note: looping only over schema def; any additional
        // attributes found on the object not defined in schema are
        // not checked in any way.
        _(this.schema).each((propDef, propName) => {
            // TODO: do this only once per class
            // Fill in some defaults
            let extendedPropDef = $.extend(true, {}, {
                type: String,
                required: false
            }, propDef);

            let propVal = attributes[propName];

            // Note: id is undefined when saving a new record
            let displayName = this.displayName(propName);

            if (extendedPropDef.relationship) {
                // Note: side effects of following call relied upon later
                this.validateRelationshipSpec(propName, extendedPropDef, propVal, violations);
            }

            if (propVal === null || propVal === undefined) {
                // If the value is null/undefined, but required...
                if (extendedPropDef.required) {
                    // TODO: some unit tests look for this error string exactly. Fix later.
                    if (unitTestCompatibility()) {
                        violation(`Attribute: ${propName} must not be null or undefined.`);
                    } else {
                        violation(`${displayName} is required but is ${propVal}`);
                    }
                }
            } else if (extendedPropDef.type instanceof Array) {
                // TODO: validate e.g. ['string','array']
            } else {
                // If the value is of an invalid length...
                if (extendedPropDef.width && propVal && (propVal.toString()).length > extendedPropDef.width) {
                    // TODO: some unit tests look for this error string exactly. Fix later.
                    if (unitTestCompatibility()) {
                        violation(`Attribute: ${propName} Invalid Length`);
                    } else {
                        violation(`${displayName} value exceeds max width (${extendedPropDef.width}) ('${propVal}')`);
                    }
                }

                // If the value is of an invalid data type...
                // TODO support typing as in instanceof. But note: 3 instanceof Number gives false
                if (extendedPropDef.type && propVal.constructor !== extendedPropDef.type) {
                    // TODO: some unit tests look for this error string exactly. Fix later.
                    if (unitTestCompatibility()) {
                        violation([`Attribute: ${propName}.`,
                            'Invalid DataType.',
                            // eslint-disable-next-line no-new-wrappers
                            `Expected ${new String(extendedPropDef.type.name).toLowerCase()}.`,
                            // eslint-disable-next-line no-new-wrappers
                            `Received ${new String(propVal.constructor.name).toLowerCase()}.`
                        ].join(' '));
                    } else {
                        violation([
                            `${displayName} invalid value type;`,
                            `expected ${extendedPropDef.type.name},`,
                            `received ${propVal.constructor.name} (${propVal})`
                        ].join(' '));
                    }
                }
            }
        });

        if (violations.length) {
            //  There may be multiple violations, but we can only return a single Error.
            //  So construct an Error object with a concatenation of all messages.
            return new Error(violations.join('\n'));
        }
        return null;
    }
}

export default Base;
