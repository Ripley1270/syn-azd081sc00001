cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/org.apache.cordova.device/www/device.js",
        "id": "org.apache.cordova.device.device",
        "clobbers": [
            "device"
        ]
    },
    {
        "file": "plugins/de.appplant.cordova.plugin.local-notification/www/local-notification.js",
        "id": "de.appplant.cordova.plugin.local-notification.LocalNotification",
        "clobbers": [
            "plugin.notification.local"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.network-information/www/network.js",
        "id": "org.apache.cordova.network-information.network",
        "clobbers": [
            "navigator.connection",
            "navigator.network.connection"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.network-information/www/Connection.js",
        "id": "org.apache.cordova.network-information.Connection",
        "clobbers": [
            "Connection"
        ]
    },
    {
        "file": "plugins/com.phonegap.plugins.barcodescanner/www/barcodescanner.js",
        "id": "com.phonegap.plugins.barcodescanner.BarcodeScanner",
        "clobbers": [
            "cordova.plugins.barcodeScanner"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.plugins.androidpreferences/www/androidpreferences.js",
        "id": "org.apache.cordova.plugins.androidpreferences.androidpreferences",
        "clobbers": [
            "plugin.androidpreferences"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.splashscreen/www/splashscreen.js",
        "id": "org.apache.cordova.splashscreen.SplashScreen",
        "clobbers": [
            "navigator.splashscreen"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.screenshot/www/Screenshot.js",
        "id": "org.apache.cordova.screenshot.screenshot",
        "merges": [
            "navigator.screenshot"
        ]
    },
	{
        "file": "plugins/com.phtcorp.cordova.plugins.battery/www/battery.js",
        "id": "com.phtcorp.cordova.plugins.battery.battery",
        "clobbers": [
            "plugin.battery"
        ]
    },
    {
        "file": "plugins/com.simonmacdonald.imei/www/imei.js",
        "id": "com.simonmacdonald.imei.imei",
        "clobbers": [
            "plugin.imei"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "org.apache.cordova.device": "0.2.4",
    "de.appplant.cordova.plugin.local-notification": "0.7.3dev",
    "org.apache.cordova.network-information": "0.2.6",
    "com.phonegap.plugins.barcodescanner": "1.0.1",
    "org.apache.cordova.plugins.androidpreferences": "1.0.0",
    "org.apache.cordova.splashscreen": "0.2.7",
    "org.apache.cordova.screenshot": "0.1.0"
}
// BOTTOM OF METADATA
});