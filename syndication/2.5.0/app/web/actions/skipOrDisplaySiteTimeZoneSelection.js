import COOL from 'core/COOL';
import Sites from 'core/collections/Sites';
import Transmit from 'web/transmit';
import * as lStorage from 'core/lStorage';
import { navigateTo } from 'core/actions/navigateTo';

/**
 * After sucessful login this action will be called to determine
 * if Set Site Time Zone Screen should be displayed or skipped
 * @memberOf ELF.actions/web
 * @method skipOrDisplaySiteTimeZoneSelection
 * @returns {Q.Promise<void>}
 * @example
 * resolve: [{ action: 'skipOrDisplaySiteTimeZoneSelection' }]
 */
export default function skipOrDisplaySiteTimeZoneSelection () {
    return Q.all([Sites.fetchFirstEntry(), COOL.getService('Transmit', Transmit).getSiteTimeZone()])
    .spread((site, tz) => {
        if (tz.res) {
            lStorage.setItem('tzSwId', tz.res);

            return site.save({ timezone: tz.res })
            .then(() => {
                navigateTo('home');
            });
        }
        return Q.reject(false);
    })
    .catch(() => {
        navigateTo('set-site-time-zone');
        return Q.resolve({ preventActions: true });
    });
}

ELF.action('skipOrDisplaySiteTimeZoneSelection', skipOrDisplaySiteTimeZoneSelection);
