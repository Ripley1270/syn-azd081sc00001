import SitePadContextSwitchingView from 'sitepad/views/ContextSwitchingView';
import COOL from 'core/COOL';
import * as lStorage from 'core/lStorage';
import TranscriptionUtility from 'web/classes/TranscriptionUtility';
import Logger from 'core/Logger';

const logger = new Logger('ContextSwitchingView');

/**
 * Entry point for context switch functionality.
 * @class ContextSwitchingView
 * @extends SitePadContextSwitchingView
 */
export default class ContextSwitchingView extends SitePadContextSwitchingView {
    constructor (options = {}) {
        super(options);

        /**
         * @method back function called when back button is clicked
         */
        this.back = _.debounce((evt) => {
            evt.preventDefault();

            if (typeof this.backArrow === 'function') {
                this.backArrow();
            } else {
                logger.error('Error navigating back. backArrow is not a function.');
            }
        }, 1000, true);

        /**
         * @method forward function called when forward button is clicked
         */
        this.forward = _.debounce((evt) => {
            evt.preventDefault();

            if (typeof this.forwardArrow === 'function') {
                this.forwardArrow();
            } else {
                logger.error('Error navigating back. forwardArrow is not a function.');
            }
        }, 1000, true);
    }

    /**
     * Mixes in transcription workflow to the navigate method upon login attempt.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    login (evt) {
        ELF.trigger('WEB:PreventOffline', {}, this)
        .then((flags) => {
            if (flags.preventDefault) {
                return;
            }

            this.navigate = TranscriptionUtility.navigate(this.navigate);
            super.login(evt);
        })
        .done();
    }

    /**
     * Invokes the forgot password workflow.
     */
    forgotPassword () {
        // DE21169 - core/views/ForgotPasswordView resolves the user record by pulling this value from lStorage.
        // sitepad/views/ContextSwitchingView#render method invokes web/classes/Session#logout, which clears the User_Login value.
        // Prior to invoking the forgot password workflow, the User_Login value needs to be set.
        lStorage.setItem('User_Login', this.user.get('id'));

        super.forgotPassword();
    }
}

COOL.add('ContextSwitchingView', ContextSwitchingView);
