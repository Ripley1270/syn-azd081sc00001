import * as DateTimeUtil from 'core/DateTimeUtil';
import COOL from 'core/COOL';
import Screen from 'core/models/Screen';

import SitePadQuestionnaireView from 'sitepad/views/SitePadQuestionnaireView';

/**
 * Enables transcription via paper on a questionnaire.
 * @class PaperQuestionnaireView
 * @extends SitePadQuestionnaireView
 */
export default class PaperQuestionnaireView extends COOL.getClass('SitePadQuestionnaireView', SitePadQuestionnaireView) {
    /**
     * @property {boolean} transcriptionViaPaper - Flag used to determine if the questionnaire is being completed as a transcription via paper.
     */
    get transcriptionViaPaper () {
        return true;
    }

    /**
     * Finds and prepares all screen data.
     * @returns {Screens}
     * @example this.prepScreens();
     */
    prepSingleScreen () {
        let screenIds = this.model.get('screens');

        // Loop through each screen and get a list of questions to display.
        let questions = _(screenIds).chain()

            // Reject all screens marked to be excluded from paper mode.
            .reject((id) => {
                let screen = LF.StudyDesign.screens.get(id);

                return screen.get('excludeFromTranscriptionViaPaper');
            })

            // Reduce all screens to a union of question IDs. e.g. ['Daily_Q_1', 'Daily_Q_2']
            .reduce((memo, id) => {
                let screen = LF.StudyDesign.screens.get(id),

                    // Clone the screens questions to ensure we don't modify the master copy.
                    questionConfigs = _.clone(screen.get('questions'));

                // Set each question configuration's mandatory property as false to
                // remove any validation.
                questionConfigs.forEach((question) => {
                    question.mandatory = false;
                });

                return memo.concat(questionConfigs);
            }, [])

            // Reject any Questions marked with excludeFromTranscriptionViaPaper.
            .reject((question) => {
                return question.excludeFromTranscriptionViaPaper;
            })

            // Sort the questions by the transcriptionViaPaperOrder property. lowest-to-highest
            .sortBy((question) => {
                return question.transcriptionViaPaperOrder;
            })

            .value();

        let id = `${this.model.get('id')}_S_1`;
        let screen = new Screen({
            id,
            className: id,
            questions
        });

        this.data.screens = [screen];

        return this.data.screens;
    }

    /**
     * Set the report date. Invoked by ELF rule upon completion of the injected report date screen.
     * @returns {Q.Promise<void>}
     */
    setReportDate () {
        let screens = this.data.screens,
            currentScreen = screens[this.screen].get('id');

        // Only change the report date when navigating away from the transcription screen.
        if (currentScreen === 'TRANSCRIPTION_REPORT_DATE_S_1') {
            let answer = this.answers.findWhere({ SW_Alias: 'TRANSCRIPTION_REPORT_DATE.0.DATE_TIME' }),
                dt = new Date(answer.get('response'));

            // StudyWorks - Report Start Date and Time.
            // Transmitted as 'S'
            this.data.dashboard.set('started', dt.ISOStamp());

            // StudyWorks - Report Date
            // Transmitted as 'R'
            this.data.dashboard.set('report_date', DateTimeUtil.convertToDate(dt));
        }

        return Q();
    }

    /**
     * Override validation of the single screen.
     * @returns {boolean} true if valid, false if not.
     */
    validateScreens () {
        let screen = this.data.screens[this.screen];

        if (screen.get('id') === `${this.model.get('id')}_S_1`) {
            return true;
        }

        return super.validateScreens();
    }

    /**
     * Overrides prepScreens to inject the Report Date screen.
     * @returns {Screens}
     */
    prepScreens () {
        // Convert the questionnaire to display on a single page.
        this.prepSingleScreen();

        // Inject the questionnaire report date screen.
        this.data.screens.unshift(LF.StudyDesign.screens.get('TRANSCRIPTION_REPORT_DATE_S_1'));

        return this.data.screens;
    }
}

COOL.add('PaperQuestionnaireView', PaperQuestionnaireView);
