import SitePadSecretQuestionView from 'sitepad/views/SecretQuestionView';
import COOL from 'core/COOL';

export default class SecretQuestionView extends SitePadSecretQuestionView {
    /**
     * Go to the next screen.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     */
    next (e) {
        ELF.trigger('WEB:PreventOffline', {}, this)
        .then((flags) => {
            if (flags.preventDefault) {
                return;
            }
            super.next(e);
        })
        .done();
    }
}

COOL.add('SecretQuestionView', SecretQuestionView);
