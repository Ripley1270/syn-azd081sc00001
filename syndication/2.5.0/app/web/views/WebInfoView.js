import SitePadInfoView from 'sitepad/views/InfoView';
import * as lStorage from 'core/lStorage';

/**
 * Extend Web Info class to use the NetProURL.
 */
export default class WebInfoView extends SitePadInfoView {
    /**
     * Creates the parameters for the template
     * @returns {Q.Promise<Object>}
     */
    getTemplateParameters () {
        return super.getTemplateParameters()
        .then((parameters) => {
            parameters.studyURL = lStorage.getItem('NetProURL');

            return parameters;
        });
    }
}
