import PageView from 'core/views/PageView';
import WebService from 'web/classes/WebService';
import { MessageRepo } from 'core/Notify';
import Sites from 'core/collections/Sites';
import COOL from 'core/COOL';
import * as lStorage from 'core/lStorage';
import Logger from 'core/Logger';
import ELF from 'core/ELF';
const eventDefault = { preventDefault: $.noop };
const logger = new Logger('ReactivationView');
import CurrentContext from 'core/CurrentContext';

export default class UnlockCodeView extends PageView {
    constructor (options) {
        super(options);

        /**
         * @property {string} template - A selector that points to the template for the view.
         * @readonly
         * @default '#unlock-code-tpl'
         */
        this.template = '#unlock-code-tpl';

        /**
         * @property {Object<string,string>} events - A list of DOM events.
         * @readonly
         */
        this.events = {
            'click #change-site': 'changeSite',
            'click #submit': 'submitHandler',
            'input #unlock-code': 'blindValidate'
        };

        /**
         * @property {Object<string,string>} templateStrings - A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = {
            header: 'APPLICATION_HEADER',
            tagline: 'ENTER_UNLOCK_CODE',
            unlockCode: 'STARTUP_UNLOCK_CODE',
            enterUnlockCode: 'ENTER_UNLOCK_CODE',
            helpText: 'UNLOCK_CODE_HELP',
            site: 'SITE',
            changeSite: 'CHANGE_SITE',
            submit: 'SUBMIT',
            unlockAttemptExeeded: 'UNLOCK_ATTEMPT_EXCEEDED'
        };
    }

    /**
     * @property {string} id - The ID of the view.
     * @readonly
     * @default 'unlock-code-view'
     */
    get id () {
        return 'unlock-code-view';
    }

    /**
    * @property {Object<string,string>} selectors - A list of selectors to populate.
    */
    get selectors () {
        return {
            unlockCode: '#unlock-code',
            btnChangeSite: 'button#change-site',
            btnSubmit: 'button#submit',
            form: '#unlockCodeForm',
            unlockAttemptExeeded: '#unlock-attempt-exeeded'
        };
    }

    /**
     * Resolve any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        lStorage.setItem('isActivation', true);

        return Sites.fetchFirstEntry()
        .then((site) => {
            /**
             * @property {Site} site The site the device is registered to.
             */
            this.site = site;
        });
    }

    /**
     * Render the view to the DOM.
     * @returns {Q.Promise<void>}
     */
    render () {
        let preferredLanguage = lStorage.getItem('preferredLanguageLocale');
        if (preferredLanguage) {
            // DE22806 fix
            CurrentContext().setContextLanguage(preferredLanguage);
        }
        return this.buildHTML({ siteCode: this.site.get('siteCode') }, true)
        .then(() => this.initForm());
    }

    /**
     * Prevents the form from being sumitted via traditional means.
     */
    initForm () {
        this.$form.submit((e) => {
            e.preventDefault();
            return false;
        });
    }

    /**
     * Validate that some value is entered for Unlock Code.
     * @param {ChangeEvent} [evt] - A input ChangeEvent.
     */
    blindValidate (evt = eventDefault) {
        let unlockCode = this.$unlockCode.val();

        evt.preventDefault();

        this.clearInputState(this.$unlockCode);

        if (!unlockCode.length) {
            this.disableButton(this.$btnSubmit);
        } else {
            this.enableButton(this.$btnSubmit);
        }
    }

    /**
     * Navigate back to the site selection view.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    changeSite (evt = eventDefault) {
        evt.preventDefault();

        ELF.trigger('APPLICATION:Uninstall', {}, this)
        .done();
    }

    /**
     * Handles failed unlock attempts
     * @param {number} incorrectUnlockCode - The incorrect unlock code that has failed
     * @returns {Q.Promise}
     */
    handleFailedAttempt (incorrectUnlockCode) {
        logger.error(`Failed unlock attempt for site: ${this.site.get('siteCode')}. unlock code: ${incorrectUnlockCode}`);

        this.inputError(this.$unlockCode);
        this.disableButton(this.$btnSubmit);

        MessageRepo.display(MessageRepo.Banner.INCORRECT_UNLOCK_CODE);

        // While not required, here for eslint consistent-return rule.
        return Q();
    }

    /**
     * Handle a click on the submit button. Abstracted for testing and to prevent
     * swallowed errors.
     * @param {(MouseEvent|TouchEvent)} evt - A mouse or touch event.
     */
    submitHandler (evt = eventDefault) {
        evt.preventDefault();

        this.submit()
        .done();
    }

    /**
     * Submit the form and navigate to the next view.
     * @returns {Q.Promise<void>}
     */
    submit () {
        let unlockCode = this.$unlockCode.val();
        const siteCode = this.site.get('siteCode'),
            webService = COOL.new('WebService', WebService),
            failedAttempt = (incorrectUnlockCode) => {
                logger.error(`Failed unlock attempt for site: ${this.site.get('siteCode')}. unlock code: ${incorrectUnlockCode}`);

                this.inputError(this.$unlockCode);
                this.disableButton(this.$btnSubmit);

                MessageRepo.display(MessageRepo.Banner.INCORRECT_UNLOCK_CODE);

                // While not required, here for eslint consistent-return rule.
                return Q();
            },
            lockedOut = () => {
                return Q()
                .then(() => MessageRepo.display(MessageRepo.Dialog.SITE_IS_LOCKED_OUT))
                .then(() => ELF.trigger('APPLICATION:Uninstall', {}, this))
                .catch((err) => {
                    logger.error('An error occurred.', err);
                });
            };

        return this.spinner.show()
        .then(() => webService.validateUnlockCode({ unlockCode, siteCode }))
        .tap(() => this.spinner.hide())
        .then(({ res }) => {
            if (!res) {
                return failedAttempt(unlockCode);
            }

            if (res.locked) {
                return lockedOut();
            }

            lStorage.removeItem('LockedOutSites');
            this.navigate('setup-user');

            // While not required, here for eslint consistent-return rule.
            return Q();
        }).catch((err) => {
            this.spinner.hide();
            logger.error('An error occurred.', err);
        });
    }
}

COOL.add('UnlockCodeView', UnlockCodeView);
