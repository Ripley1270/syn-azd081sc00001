import BaseQuestionnaireView from 'core/views/BaseQuestionnaireView';
import Sites from 'core/collections/Sites';
import COOL from 'core/COOL';
import WebService from 'web/classes/WebService';
import Logger from 'core/Logger';
import { MessageRepo } from 'core/Notify';

const logger = new Logger('WebFirstSiteUserQuestionnaireView');

export default class WebFirstSiteUserQuestionnaireView extends BaseQuestionnaireView {
    /**
     *
     * @param {Object} options
     * Required properties
     * id {string} - the ID of the questionnaire
     * Optional properties
     * showCancel {boolean} - Indicates whether to display the cancel button
     */
    constructor (options = {}) {
        super(options);

        /**
         * Id of template to render
         * @type String
         * @readonly
         * @default '#questionnaire-template'
         */
        this.template = '#first-site-user-questionnaire-template';
    }


    get defaultRouteOnExit () {
        return 'unlock-code';
    }

    get defaultFlashParamsOnExit () {
        return {};
    }

    /**
     * Performs the standard screen, Question and affidavit prep work.
     * @returns {Q.Promise<void>} resolved when complete
     */
    resolve () {
        return Q.Promise((resolve, reject) => {
            let setSite = Sites.fetchFirstEntry()
            .then((site) => {
                /**
                 * @property {Site} site The site the device is registered to.
                 */
                this.site = site;
            });
            this.prepScreens();
            this.prepQuestions();

            return this.prepAnswerData(this.id, this.ordinal)
            .then(setSite)
            .then(() => this.open())
            .then(() => resolve())
            .catch(e => reject(e));
        });
    }


    /**
     * Renders the view.
     * @param {string=} id - screen id to render (TODO: if omitted does what?)
     * @returns {Q.Promise<void>}
     */
    render (id) {
        return super.render(id)
        .then(this.setUnlockCodeHandler());
    }

    /**
     * Move to the next screen.
     * @returns {Q.Promise<void>}
     */
    next () {
        let unlockCodeWidget = $('#ADD_USER_W_8'),
            unlockCodeVal = $('#ADD_USER_W_8').val();

        const siteCode = this.site.get('siteCode'),
            webService = COOL.new('WebService', WebService),
            failedAttempt = (incorrectUnlockCode) => {
                logger.error(`Failed unlock attempt for site: ${this.site.get('siteCode')}. unlock code: ${incorrectUnlockCode}`);

                this.inputError(unlockCodeWidget);

                MessageRepo.display(MessageRepo.Banner.INCORRECT_UNLOCK_CODE);

                // While not required, here for eslint consistent-return rule.
                return Q().reject();
            },
            lockedOut = () => {
                return Q()
                .then(() => MessageRepo.display(MessageRepo.Dialog.SITE_IS_LOCKED_OUT))
                .then(() => ELF.trigger('APPLICATION:Uninstall', {}, this))
                .catch((err) => {
                    logger.error('An error occurred.', err);
                });
            };

        return this.spinner.show()
        .then(() => webService.validateUnlockCode({ unlockCode: unlockCodeVal, siteCode }))
        .tap(() => this.spinner.hide())
        .then(({ res }) => {
            if (!res) {
                return failedAttempt(unlockCodeVal);
            }

            if (res.locked) {
                return lockedOut();
            }

            this.inputSuccess(unlockCodeWidget);

            // While not required, here for eslint consistent-return rule.
            return Q();
        })
        .then(() => {
            return super.next();
        }).catch((err) => {
            this.spinner.hide();
            logger.error('An error occurred.', err);
        });
    }

    /*
     * Sets the callback handler for handling the unlock code validation.
     * @returns {Q.Promise<void>}
     */
    setUnlockCodeHandler () {
        let unlockCodeWidget = $('#ADD_USER_W_8');
        unlockCodeWidget.on('input', () => {
            this.clearInputState(unlockCodeWidget);
            return Q();
        });
    }
}
