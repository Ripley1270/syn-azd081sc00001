// This file is used to bootstrap views into COOL.
// See web/classes/Startup.js

import './AccountConfiguredView';
import './ContextSwitchingView';
import './InactiveTabView';
import './LoginView';
import './QuestionnaireCompletionView';
import './SecretQuestionView';
import './SetPasswordView';
import './SetSiteTimeZoneView';
import './SettingsWebView';
import './SiteAssignmentView';
import './TranscriptionQuestionnaireView';
import './UnlockCodeView';
import './UnsupportedBrowserView';
import './WebAboutView';
import './WebAccountConfiguredView';
import './WebEULAView';
import './WebFormGatewayView';
import './WebQuestionnaireCompletionView';
import './WebQuestionnaireView';
