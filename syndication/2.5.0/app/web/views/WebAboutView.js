import SitePadAboutView from 'sitepad/views/AboutView';
import WebInfoView from 'web/views/WebInfoView';
import * as lStorage from 'core/lStorage';

/**
 * Web-specific implementation of the About view
 */
export default class WebAboutView extends SitePadAboutView {
    /**
     * Create instance of InfoView component (or inherited class) to use with this control.
     * @override Return WebInfoView instead.
     * @returns {InfoView} instance of InfoView
     */
    constructInfoView () {
        return new WebInfoView();
    }

    /**
     * Renders the component
     * @returns {Promise<void>} A Promise
     */
    render () {
        return super.render()
        .then(() => {
            let mode = lStorage.getItem('mode');
            if (mode === 'web') {
                // Hide the network panel
                this.hideElement('#network-parent');

                // Hide timezone panel
                this.hideElement('#timezone-parent');

                // Enabled the visit button and set the link
                this.showElement('#btn-visit');
                this.showElement('#btn-dcr');
            }
        });
    }

    /**
     * Open a new browser window and navigate to the ERT GSSO site
     * @param {Event} e jQuery event argument
     */
    goToGSSO (e) {
        e.target.blur();
        window.open('https://gsso.ert.com/gsso/idp/', '_blank', 'location=no,EnableViewPortScale=yes');
    }

    /**
     * Goes back to the previous view
     */
    back () {
        this.navigate('settings');
    }
}
