import SitePadLogin from 'sitepad/views/LoginView';
import * as lStorage from 'core/lStorage';
import ELF from 'core/ELF';
import CurrentContext from 'core/CurrentContext';
import { MessageRepo } from 'core/Notify';
import Logger from 'core/Logger';

const logger = new Logger('WebLoginView');

export default class LoginView extends SitePadLogin {
    constructor (options = {}) {
        super(options);

        // DE20498 - End the user's current session when the login view is displayed.
        LF.security.logout();

        /**
         * @property {object} events - A list of DOM events.
         * @readonly
         */
        this.events = _.defaults({
            'input #user': 'validate',
            'change #role': 'roleChanged'
        }, this.events);

        /**
         * @property {object} templateStrings A list of resource keys to translate and render to the UI.
         */
        this.templateStrings = _.extend(this.templateStrings, {
            role: 'ROLES',
            selectSite: 'SITE_SELECTION',
            invalidPass: 'INCORRECT_USERNAME_PWD'
        });

        /**
         * @property {boolean} hideSiteSelectionLink - Determines if the site selection link should be hidden.
         */
        this.hideSiteSelectionLink = !!options.hideSiteSelectionLink;

        if (!this.hideSiteSelectionLink) {
            this.events['click #select-site'] = 'selectSite';
        }
    }

    /**
     * @property {Object<string,string>} selectors - A list of selectors to generate.
     */
    get selectors () {
        return _.defaults({
            role: '#role'
        }, super.selectors);
    }

    /**
     * Resolves any dependencies required for the view to render.
     * @returns {Q.Promise<void>}
     */
    resolve () {
        return super.resolve()
            .then(() => {
            // core/views/LoginView#resolve filters and assigns a default user.
            // Since we don't have a dropdown list here, we need to clear it out.
                delete this.user;

                let lang = lStorage.getItem('preferredLanguageLocale');

                if (lang) {
                    return CurrentContext().setContextLanguage(lang);
                }

                // Here for eslint consistent-return rule.
                return Q();
            });
    }

    /**
     * Render the login view to the DOM.
     * @returns {Q.Promise<void>}
     */
    render () {
        return super.render()
            .then(() => {
                if (this.hideSiteSelectionLink) {
                    this.hideElement(this.$('#select-site'));
                }

                this.enableElement(this.$password);

                return this.renderRoles();
            });
    }

    /**
     * Render the available roles to the view.
     * @returns {Q.Promise<void>}
     */
    renderRoles () {
        let roles = LF.StudyDesign.roles.getProductRoles(),
            data = [];

        // Translate a role's displayName.
        let translate = (role) => {
            return this.i18n(role.get('displayName'))
                .then((text) => {
                    data.push({ id: role.get('id'), text });
                });
        };

        // Render the select element.
        let render = (input = []) => {
            this.$role.select2({
                escapeMarkup: markup => markup,
                minimumResultsForSearch: Infinity,
                data: input,
                width: ''
            });
        };

        return roles.reduce((chain, role) => {
            return chain.then(() => translate(role));
        }, Q())
            .then(() => {
                return render(data);
            });
    }

    /**
     * @override
     * Alter render users to do nothing.  We now have a textbox, not a select list.
     * If there is only one option for a user with the current filter,
     * write it in and disable the field.
     */
    renderUsers () {
        return this.users.fetch()
            .then(() => {
                return this.filterSortUsers();
            })
            .then((users) => {
                if (users.length === 1 && users[0].get('role') === 'subject') {
                    this.$user.val(users[0].get('username'));
                    this.disableElement(this.$user);
                    return this.userChanged();
                }
                return Q();
            });
    }

    /**
     * Invoked when the user input element trigger's a change event.
     */
    userChanged () {
        return this.validate();
    }

    /**
     * Invoked when the user changes the selected role.
     */
    roleChanged () {
        logger.trace(`Role changed to ${this.$role.val()}`);

        this.$password.val('');
        this.clearInputState(this.$password);
        this.disableButton(this.$login);
    }

    /**
     * Validate the login form.
     * @example this.validate();
     */
    validate () {
        let username = this.$user.val(),
            password = this.$password.val();

        this[username ? 'enableButton' : 'disableButton'](this.$forgotPassword);

        if (username && password) {
            this.enableButton(this.$login);
        } else {
            this.disableButton(this.$login);
        }
    }

    /**
     * Handles the user's click on the forgot password link.
     */
    forgotPassword () {
        let username = this.$user.val(),
            role = this.$role.val(),
            user = this.users.findWhere({ username, role, active: 1 });

        // If the user has forgotten their password, we should clear
        // any password they've entered, along with the current state of
        // the input element.
        this.$password.val('');
        this.clearInputState(this.$password);
        this.disableButton(this.$login);

        if (user) {
            this.user = user;

            // core/views/ForgotPasswordView resolves the user record by pulling this value
            // from localStorage.
            lStorage.setItem('User_Login', user.get('id'));

            // Invoke core forgot password workflow.
            CurrentContext().setContextByUser(this.user)
            .then(() => super.forgotPassword())
            .done();
        } else {
            MessageRepo.display(MessageRepo.Banner.INVALID_USERNAME);
        }
    }

    /**
     * Login to the application.
     * @param {(MouseEvent|TouchEvent)} e - A mouse or touch event.
     * @returns {Q.Promise<void>}
     */
    login (e) {
        let username = this.$user.val(),
            role = this.$role.val();

        this.disableButton(this.$login);
        return this.filterSortUsers()
            .then((users) => {
                this.user = _.find(users, (user) => {
                    return user.get('username') === username && user.get('role') === role;
                });

                if (this.user) {
                    lStorage.setItem('User_Login', this.user.get('id'));
                    return CurrentContext().setContextByUser(this.user);
                }

                lStorage.removeItem('User_Login');

                // Expected by eslint for consistent-return
                return Q();
            })
            .then(() => {
                if (this.user) {
                    return LF.security.checkUserLockout(this.user);
                }
                return Q();
            })
            .then(() => super.login(e, () => {
                return this.render()
                .then(() => {
                    this.$user.val(username);
                    this.$role.val(role).trigger('change.select2');
                    this.validate();
                });
            }));
    }

    /**
     * Handle the site selection link event.
     * This triggers an ELF action to ensure the user wants to change sites.
     * @returns {Q.Promise<void>}
     */
    selectSite () {
        return ELF.trigger('SITEASSIGNMENT:ConfirmChange');
    }
}
