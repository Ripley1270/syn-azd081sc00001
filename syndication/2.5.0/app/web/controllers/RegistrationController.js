import BaseController from 'core/controllers/BaseController';
import State from 'web/classes/State';
import Logger from 'core/Logger';
import SiteAssignmentView from 'web/views/SiteAssignmentView';
import UnlockCodeView from 'web/views/UnlockCodeView';
import WebEULAView from 'web/views/WebEULAView';
import WebFirstSiteUserQuestionnaireView from 'web/views/WebFirstSiteUserQuestionnaireView';
import ELF from 'core/ELF';

let logger = new Logger('RegistrationController');

// Note: Do not import & extend sitepad/controllers/RegistrationController.
// It imports a decent chunk of code we don't want included in our runtime.
export default class RegistrationController extends BaseController {
    /**
     * Route based on state of application.
     */
    route () {
        const { activated, locked, endUserLicenseAgreements, registered } = State.states;

        switch (State.get()) {
            case activated:
                this.navigate('login');
                break;
            case locked:
                this.unlockCode();
                break;
            case endUserLicenseAgreements:
                this.eula();
                break;
            case registered:
                ELF.trigger('APPLICATION:Uninstall', {}, this)
                .done();
                break;

            // TODO: new is a keyword, we should avoid using it.
            case State.states.new:
            default:
                this.siteAssignment();
        }
    }

    /**
     * Verify the current state before displaying the view.  If the target state
     * doesn't match the current one, navigate to the base route.
     * @param {string} state - The target state to verify.
     * @param {string} strView - The name of the view to render.
     * @param {Object} clsView - The default class to use in case one isn't registered with COOL.
     * @param {Object} options - Options to pass to the constructor.
     * @returns {Q.Promise<void>}
     */
    verifyAndGo (state, strView, clsView, options) {
        if (state === State.get()) {
            return this.go(strView, clsView, options);
        }

        return Q()
        .finally(() => this.navigate(''));
    }

    /**
     * Display the SiteAssignmentView.
     */
    siteAssignment () {
        this.go('SiteAssignmentView', SiteAssignmentView);
    }

    /**
     * Display the UnlockCodeView.
     */
    unlockCode () {
        // DE20704 - Verify that the application is in the correct state before displaying the view.
        this.verifyAndGo(State.states.locked, 'UnlockCodeView', UnlockCodeView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the End User License Agreement (EULA).
     */
    eula () {
        // DE20704 - Verify that the application is in the correct state before displaying the view.
        this.verifyAndGo(State.states.endUserLicenseAgreements, 'WebEULAView', WebEULAView)
        .catch(e => logger.error(e))
        .done();
    }

    /**
     * Display the first user creation questionnaire.
     */
    setupUser () {
        this.go('WebFirstSiteUserQuestionnaireView', WebFirstSiteUserQuestionnaireView, {
            id: 'First_Site_User'
        })
        .catch(e => logger.error(e))
        .done();
    }
}
