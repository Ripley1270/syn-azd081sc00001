import TextBox from 'core/widgets/TextBox';

/**
 * Password TextBox for replacing signatures on the web.
 * @class PasswordWidget
 * @extends TextBox
 */
export default class PasswordWidget extends TextBox {
    /**
     * Determines if the affidavit is answered and valid.
     * @returns {boolean} True if valid, false if not.
     */
    isAffidavitAnswered () {
        if (this.value) {
            try {
                let user = LF.security.activeUser,
                    userPassword = user.get('password'),
                    isAdminUser = user.get('userType') === 'Admin',
                    hash;

                if (isAdminUser) {
                    hash = (userPassword.length > 32) ? hex_sha512(user.get('salt') + this.value) : hex_md5(user.get('salt') + this.value);
                } else {
                    hash = (userPassword.length > 32) ? hex_sha512(this.value + user.get('salt')) : hex_md5(this.value + user.get('salt'));
                }

                return hash === userPassword;
            } catch (e) {
                return false;
            }
        }

        return false;
    }
}

window.LF.Widget.PasswordWidget = PasswordWidget;
