import BaseStartup from 'core/classes/BaseStartup';
import trial from 'core/index';
import COOL from 'core/COOL';
import ELF from 'core/ELF';
import Logger from 'core/Logger';
import * as lStorage from 'core/lStorage';
import * as utils from 'core/utilities';
import { uninstall } from 'core/actions/uninstall';
import URLParser from 'web/classes/URLParser';
import UserAgentParser from 'web/classes/UserAgentParser';
import TabManager from 'web/classes/TabManager';
import { MessageRepo } from 'core/Notify';

// The following components are included to be bootstrapped into COOL.
// Order is important!
import 'sitepad/transmit';
import 'sitepad/classes/SitePadPayload';
import SitepadDatabaseVersionManager from 'sitepad/classes/SitepadDatabaseVersionManager';

import setupSitepadMessages from 'sitepad/resources/Messages';

import Router from 'web/classes/Router';
import RegistrationController from 'web/controllers/RegistrationController';
import ApplicationController from 'web/controllers/ApplicationController';
import SettingsController from 'web/controllers/SettingsController';
import setupWebMessages from 'web/resources/Messages';

// Importing the following to be bootstrapped into COOL.
import 'web/classes/WebStudyDesign';
import 'web/views/WebAccountConfiguredView';

let logger = new Logger('Startup');

/**
 * The class that manages the application startup process for Web modality
 */
export default class Startup extends BaseStartup {
    /**
     * Setup the application's router.
     * @param {Object} [options={}] - Options passed into the Router's constructor.
     * @returns {Q.Promise<void>}
     */
    backboneStartup (options = {}) {
        logger.traceEnter('backboneStartup');

        return Q.Promise((resolve) => {
            const registration = new RegistrationController();
            const application = new ApplicationController();
            const settings = new SettingsController();
            const study = new trial.assets.StudyController();

            const routerOpts = _.extend({}, options.routerOpts, {
                controllers: _.extend({}, options.controllers, {
                    application, registration, settings, study
                })
            });

            LF.router = new Router(routerOpts);

            resolve();
        })
        .catch((err) => {
            logger.error('Error on Backbone startup.', err);
        })
        .finally(() => {
            logger.traceExit('backboneStartup');
        });
    }

    /**
     * Runs a browser check against the configured list
     * @returns {Q.Promise<void>}
     */
    checkBrowser () {
        const isValid = UserAgentParser.isBrowserSupported(navigator.userAgent);

        if (!isValid) {
            const ua = UserAgentParser.parse(navigator.userAgent),
                message = `Unsupported browser detected: ${ua.browser.name}`;

            LF.router.navigate('unsupported-browser', true);
            return Q.reject(new Error(message));
        }

        return Q();
    }

    /**
     * Start Backbone.history
     */
    startUI () {
        logger.traceEnter('startUI');

        Backbone.history.start({ pushState: false, silent: true });

        // hack so backbone things route it changing.  Unfortunately it sets fragment to ''
        // if silent: true, even though it never actually navigated anywhere.
        Backbone.history.fragment = undefined;
        logger.traceExit('startUI');
    }

    /**
     * Setup the environment for the web modality.
     * @returns {Q.Promise<void>}
     */
    setupEnvironment () {
        logger.traceEnter('setupEnvironment');

        lStorage.setItem('mode', 'web');

        let currentEnvironment = lStorage.getItem('environment');
        logger.trace(`Current environment: ${currentEnvironment}`);

        let parsed = URLParser.parse(window.location.href);
        let [study, env] = parsed.pathname.replace(/\//g, ' ').trim().split(' ');

        logger.trace(`Study path: ${study}`);
        logger.trace(`Environment path: ${env}`);

        let matched = _.findWhere(trial.assets.studyDesign.environments, { id: env });

        if (matched == null) {
            let dflt = trial.assets.studyDesign.defaultEnvironment || 'production';

            logger.trace(`No environment matched. Resolving to default: ${dflt}.`);

            matched = _.findWhere(trial.assets.studyDesign.environments, { id: dflt });
        }

        if (currentEnvironment == null || (currentEnvironment === matched.id && lStorage.getItem('NetProURL') === matched.url && lStorage.getItem('studyDbName') === matched.studyDbName)) {
            lStorage.setItem('NetProURL', matched.url);

            // Set the service base to the Proxy API.
            utils.setServiceBase('api/v1/proxy');

            lStorage.setItem('environment', matched.id);
            lStorage.setItem('studyDbName', matched.studyDbName);

            // This token is used only for Web APIs to help identify which configuration should be used with each request.
            lStorage.setItem('webToken', `${study}:${env}`);

            logger.traceExit('setupEnvironment');
            return Q();
        }
        logger.warn('Current environment doesn\'t match that of the URL!');

        // If the environment has changed, we need to clear the application data.
        return uninstall(['UserVisit', 'UserReport'])
        .finally(() => {
            logger.traceExit('setupEnvironment');

            window.location.href = '';
        });
    }

    /**
     * When the application is loaded for the first time, there should be no valid user sessions.
     * @returns {Q.Promise<void>}
     */
    checkSession () {
        logger.traceEnter('checkSession');

        if (LF.security) {
            logger.trace('Ending invalid user session...');

            LF.security.logout(false);
        }

        logger.traceExit('checkSession');

        // This isn't really needed as this method is invoked late in the promise chain.
        // However, we want to ensure that if this is moved to the top of the stack, it will still work.
        return Q();
    }

    /**
     * Setup and check the current tab session.
     * @returns {Q.Promise<void>}
     */
    checkTab () {
        let tab = new COOL.new('TabManager', TabManager);

        // DE22423 - Check for duplicate tabs. e.g. User right-clicks tab and selectes "Duplicate"
        // A hidden form field is being used to track tab duplicate state.  See app/web/index.ejs
        tab.checkDuplicates();

        if (!tab.isActiveTab()) {
            return ELF.trigger('APPLICATION:InactiveTab', {}, this)
            .then(() => {
                // We need to reject the startup promise chain to prevent any further actions from being taken.
                return Q.reject('Current tab is inactive.');
            });
        }

        return Q();
    }

    /**
     * Track the current page in the navigation.
     * Each next page will be assigned a number.
     * If the user clicks the back button, it will
     * detect an already defined page number and present
     * a notification that Back Button is not allowed
     */
    trackNavigationAndPreventBrowserBackButton () {
        window.history.pushState({ currentPage: null }, null, '');
        $(window).on('popstate', (e) => {
            if (e.originalEvent.state !== null) {
                MessageRepo.display(MessageRepo.Banner.BACK_BUTTON_NOTIFICATION).done();
            }
            window.history.pushState({ currentPage: location.href }, document.title, location.href);
            window.history.pushState({ currentPage: location.href }, document.title, location.href);
        });
    }

    /**
     * Startup the application.
     * @returns {Q.Promise<void>}
     */
    startup () {
        setupSitepadMessages();
        setupWebMessages();
        this.trackNavigationAndPreventBrowserBackButton();

        return this.preSystemStartup()
        .then(() => this.cordovaStartup())
        .then(() => this.jQueryStartup())
        .then(() => this.studyStartup())
        .then(() => SitepadDatabaseVersionManager.createDatabaseVersions())
        .then(() => this.backboneStartup())
        .then(() => this.setupEnvironment())
        .then(() => this.setupUsersAndContext())
        .then(() => this.startUI())
        .then(() => this.checkBrowser())
        .then(() => this.checkTab())
        .then(() => this.checkSession())
        .then(() => ELF.trigger('APPLICATION:Loaded'))
        .then((res) => {
            // TODO remove the usage of this flag.
            localStorage.removeItem('questionnaireCompleted');

            if (!res.preventDefault) {
                LF.router.navigate('', true);
            }
        })
        .catch(err => err && logger.error('Error Loading Application', err));
    }
}

COOL.add('WebStartup', Startup);
