import SitePadStudyDesign from 'sitepad/classes/SitePadStudyDesign';
import COOL from 'core/COOL';

/**
 * Extended Study Design functionality for the Web app.
 * @class WebStudyDesign
 * @extends SitePadStudyDesign
 */
export default class WebStudyDesign extends COOL.getClass('StudyDesign', SitePadStudyDesign) {
    constructor (design) {
        super(design);

        // Any forms we want to exclude from using the password signature.
        const exclude = ['First_Site_User', 'Deactivate_User'];

        this.questionnaires.forEach((questionnaire) => {
            let { id, affidavit } = questionnaire.toJSON();

            if (affidavit && exclude.indexOf(id) === -1) {
                let isSignatureAffidavit = this.affidavits.find((aff) => {
                    // If the affidavit IDs match, and the widget type is SignatureBox, we need to replace the affidavit.
                    return aff.get('id') === affidavit && aff.get('widget').type === 'SignatureBox';
                });

                if (isSignatureAffidavit) {
                    // Determine the ID of the password affidavit to use.  P_ indicates a subject-based form.
                    let affidavitId = affidavit.indexOf('P_') === 0 ? 'P_PasswordAffidavit' : 'PasswordAffidavit';

                    // US8188 - We need the retain the text of the original affidavit and display it on the password affidavit for each questionnaire.
                    // To do so, we'll dynamically create unique password affidavits.  We'll exclude the SignatureAffidavit and P_SignatureAffidavit id's
                    // from this list.
                    if (['SignatureAffidavit', 'P_SignatureAffidavit'].indexOf(isSignatureAffidavit.get('id')) !== -1) {
                        questionnaire.set('affidavit', affidavitId);
                    } else {
                        let pwdAffidavit = this.affidavits.findWhere({ id: affidavitId })
                            .clone(),
                            newAffidavitId = `${pwdAffidavit.get('id')}_${isSignatureAffidavit.get('id')}`;

                        // Create an ID for our new affidavit.
                        pwdAffidavit.set('id', newAffidavitId);

                        // Copy over the text from the signature affidavit to the cloned password affidavit.
                        pwdAffidavit.set('text', isSignatureAffidavit.get('text'));

                        // Set the new affidavit to the target questionnaire.
                        questionnaire.set('affidavit', newAffidavitId);

                        // Add the new Password Affidavit to the affidavits collection.
                        this.affidavits.add(pwdAffidavit);
                    }
                }
            } else if (id === 'First_Site_User') {
                // This is a unique case where we cannot use
                questionnaire.set('affidavit', 'SiteCheckBoxAffidavit');
            } else if (id === 'Deactivate_User') {
                questionnaire.set('affidavit', 'DeactivateUserAffidavitPassword');
            }
        });

        this.roles.forEach((role) => {
            let affidavit = role.get('defaultAffidavit');

            if (affidavit) {
                let isSignatureAffidavit = this.affidavits.find((aff) => {
                    // If the affidavit IDs match, and the widget type is SignatureBox, we need to replace the affidavit.
                    return aff.get('id') === affidavit && aff.get('widget').type === 'SignatureBox';
                });

                if (isSignatureAffidavit) {
                    // Replace the signature affidavit with a password affidavit.  P_ indicates a subject-based form.
                    role.set('defaultAffidavit', affidavit.indexOf('P_') === 0 ? 'P_PasswordAffidavit' : 'PasswordAffidavit');
                }
            }
        });
    }
}

COOL.add('StudyDesign', WebStudyDesign);
