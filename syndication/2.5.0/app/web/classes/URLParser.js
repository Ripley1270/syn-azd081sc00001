/**
 * @typedef {Object} URLParser~Parsed
 * @property {string} protocol - URL protocol. ex. 'http'
 * @property {string} host - The host portion of the URL. ex. 'localhost:3000'
 * @property {string} hostname - The hostname of the URL. ex. 'localhost'
 * @property {string} port - The port of the URL. ex. 3000
 * @property {string} pathname - The pathname of the URL. ex. '/ecoa-study/ust'
 * @property {string} hash - The hash portion of the route. ex. #login
 * @property {string} search - The query string portion of the URL. ex. '?user=1'
 * @property {Object} seachObject - The query string broken into an object. ex. { user: 1 }
 */

/**
 * Parses URLs.
 * @class URLParser
 */
export default class URLParser {
    /**
     * Parse the provided URL.
     * @param {string} [url=window.location.href] - The URL to parse.
     * @returns {URLParser~Parsed}
     */
    static parse (url = window.location.href) {
        let parser = document.createElement('a'),
            searchObject = {},
            queries,
            split;

        parser.href = url;
        queries = parser.search.replace(/^\?/, '').split('&');

        for (let i = 0; i < queries.length; i++) {
            split = queries[i].split('=');
            searchObject[split[0]] = split[1];
        }

        return {
            // e.g. 'http'
            protocol: parser.protocol.replace(':', ''),

            // e.g. 'localhost:3000'
            host: parser.host,

            // e.g. 'localhost'
            hostname: parser.hostname,

            // e.g. '3000'
            port: parser.port,

            // e.g. '/users/1'
            pathname: parser.pathname,

            // e.g. '?user=1'
            search: parser.search,

            // e.g. '#login'
            hash: parser.hash,

            // { user: '1' }
            searchObject
        };
    }
}
