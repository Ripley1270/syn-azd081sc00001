// Sets the report date in transcription mode.
export default {
    id: 'saveReportDate',
    trigger: 'QUESTIONNAIRE:Navigate',
    evaluate: ['AND', 'isTranscriptionMode', 'isDirectionForward'],
    resolve: [{ action: 'setReportDate' }]
};
