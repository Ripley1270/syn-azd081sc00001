// Uninstall the application data when triggered.
const uninstall = {
    id: 'uninstall',
    trigger: [
        'APPLICATION:Uninstall',
        'APPLICATION:DecryptionError',
        'APPLICATION:EncryptionError'
    ],
    deferSubsequentEvals: 5000,
    resolve: [
        { action: 'clearTabState' },
        { action: 'uninstall', data: ['UserReport', 'UserVisit'] },
        { action: 'refresh' }
    ]
};

export default uninstall;
