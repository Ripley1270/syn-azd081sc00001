// Display a  notification to ensure the user has enabled popups.
const eConsentNotification = {
    id: 'eConsentNotification',
    trigger: 'ECONSENT:Open',
    salience: 2,
    resolve: [{
        action: 'notify',
        data: { key: 'ECONSENT_NOTIFICATION' }
    }]
};

export default eConsentNotification;
