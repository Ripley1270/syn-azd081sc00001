// Workflow logic for session timeout in the web app.
// It runs the same actions as it would for unsigned affidavit on SitePad.
const affidavitSessionTimeout = {
    id: 'affidavitSessionTimeout',
    trigger: 'QUESTIONNAIRE:SessionTimeout',
    salience: 99,
    resolve: [
        { action: 'logout' },

        // Prevent all other rules and functionality from executing.
        { action: 'preventAll' }
    ]
};

export default affidavitSessionTimeout;
